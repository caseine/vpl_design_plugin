/*
 * @author : Yvan Maillot (yvan.maillot@uha.fr)
 */
package edu.uha.miage;

import caseine.tags.*;


@RelativeEvaluation
@ClassTestPriority(1)
public class Point {

    @ToDo("1.01. Déclarer x et y, les coordonnées cartésiennes de ce point")
    @ToCheck(value="x ou y : déclaration incorrecte ou manquante", priority = 1, grade = 1)
    @GetterToCheck(priority = 6, grade = 1)
    @SetterToCheck(priority = 7, grade = 1)

    private double x, y;

    @ToCheck(priority = 2, grade = 1)
    @ToDo("1.02. Écrire le constructeur aux coordonnées cartésiennes")
    public Point(double x, double y) {
        this.x = x;
        this.y = y;
    }

    @ToCheck(priority = 3, grade = 1)
    @ToDo("1.03. Écrire un getter pour x")
    public double getX() {
        return x;
    }

    @ToDo("1.04. Écrire un getter pour y")
    @ToCheck(priority = 3, grade = 1)
    public double getY() {
        return y;
    }

    @ToDo("1.05. Écrire un setter pour x")
    @ToCheck(priority = 4, grade = 1)
    public void setX(double x) {
        this.x = x;
    }

    @ToDo("1.06. Écrire un setter pour y")
    @ToCheck(priority = 4, grade = 1)
    public void setY(double y) {
        this.y = y;
    }

    @ToDo("1.07. public toString() qui retourne par exemple (2.5, 3.2)")
    @Override
    @ToCheck(priority = 5, grade = 1)
    public String toString() {
        return "(" + x + ", " + y + ")";
    }
}
