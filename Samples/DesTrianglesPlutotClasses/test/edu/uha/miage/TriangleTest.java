/*
 * @author : Yvan Maillot (yvan.maillot@uha.fr)
 */
package edu.uha.miage;

import caseine.format.javajunit.Grade;
import caseine.reflect.ReflectUtilities;
import caseine.tags.ClassTestPriority;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import org.junit.Test;
import static org.junit.Assert.*;


@ClassTestPriority(6)
public class TriangleTest {

    private Constructor<?> cpoint = null;
    private Constructor<?> csegment = null;
    private Constructor<?> ctriangle = null;
    private Field fx = null, fy = null;
    private Field fp1 = null, fp2 = null;
    private Field fs1 = null, fs2 = null, fs3 = null;

    private void getConstructorAndFields() {

        try {
            cpoint = Point.class.getDeclaredConstructor(double.class, double.class);
        } catch (NoSuchMethodException | SecurityException | IllegalArgumentException ex) {
            fail("Constructeur Point(double x, double y) absent ou mal signé");
        }

        try {
            csegment = Segment.class.getDeclaredConstructor(Point.class, Point.class);
        } catch (NoSuchMethodException | SecurityException | IllegalArgumentException ex) {
            fail("Constructeur Segment(Point p1, Point p2) absent ou mal signé");
        }

        try {
            ctriangle = Triangle.class.getDeclaredConstructor(Point.class, Point.class, Point.class);
        } catch (NoSuchMethodException | SecurityException | IllegalArgumentException ex) {
            fail("Constructeur Triangle(Point p1, Point p2, Point p3) absent ou mal signé");
        }

        try {
            fx = Point.class.getDeclaredField("x");
            fx.setAccessible(true);
            fy = Point.class.getDeclaredField("y");
            fy.setAccessible(true);
        } catch (NoSuchFieldException | SecurityException ex) {
            fail("Attribut x ou y de Point absent ou mal signé");
        }

        try {
            fp1 = Segment.class.getDeclaredField("p1");
            fp1.setAccessible(true);
            fp2 = Segment.class.getDeclaredField("p2");
            fp2.setAccessible(true);
        } catch (NoSuchFieldException | SecurityException ex) {
            fail("Attribut p1 ou p2 de Segment absent ou mal signé");
        }

        try {
            fs1 = Triangle.class.getDeclaredField("s1");
            fs1.setAccessible(true);
            fs2 = Triangle.class.getDeclaredField("s2");
            fs2.setAccessible(true);
            fs3 = Triangle.class.getDeclaredField("s3");
            fs3.setAccessible(true);
        } catch (NoSuchFieldException | SecurityException ex) {
            fail("Attribut s1 ou s2 ou s3 de Triangle absent ou mal signé");
        }

    }

    @Test
    @Grade(6)
    public void testConstructor() {
        System.out.println("Test constructeur Triangle");
        getConstructorAndFields();
        if (fx != null && fy != null
                && cpoint != null && csegment != null && ctriangle != null
                && fp1 != null && fp2 != null
                && fs1 != null && fs2 != null && fs3 != null) {

            try {
                double x1 = -2.5, y1 = -2;
                double x2 = -20.5, y2 = -20;
                double x3 = 2.5, y3 = -10;
                for (int i = 0; i < 10; ++i) {
                    Point p1 = (Point) cpoint.newInstance(x1, y1);
                    Point p2 = (Point) cpoint.newInstance(x2, y2);
                    Point p3 = (Point) cpoint.newInstance(x3, y3);
                    Triangle t = (Triangle) ctriangle.newInstance(p1, p2, p3);

                    // Segment 1
                    assertEquals("Revoir le constructeur de Triangle",
                            fx.getDouble(fp1.get(fs1.get(t))),
                            fx.getDouble(p1),
                            1e-6);
                    assertEquals("Revoir le constructeur de Triangle",
                            fy.getDouble(fp1.get(fs1.get(t))),
                            fy.getDouble(p1),
                            1e-6);

                    assertEquals("Revoir le constructeur de Triangle",
                            fx.getDouble(fp2.get(fs1.get(t))),
                            fx.getDouble(p2),
                            1e-6);
                    assertEquals("Revoir le constructeur de Triangle",
                            fy.getDouble(fp2.get(fs1.get(t))),
                            fy.getDouble(p2),
                            1e-6);

                    // Segment 2
                    assertEquals("Revoir le constructeur de Triangle",
                            fx.getDouble(fp1.get(fs2.get(t))),
                            fx.getDouble(p2),
                            1e-6);
                    assertEquals("Revoir le constructeur de Triangle",
                            fy.getDouble(fp1.get(fs2.get(t))),
                            fy.getDouble(p2),
                            1e-6);

                    assertEquals("Revoir le constructeur de Triangle",
                            fx.getDouble(fp2.get(fs2.get(t))),
                            fx.getDouble(p3),
                            1e-6);
                    assertEquals("Revoir le constructeur de Triangle",
                            fy.getDouble(fp2.get(fs2.get(t))),
                            fy.getDouble(p3),
                            1e-6);

                    // Segment 3
                    assertEquals("Revoir le constructeur de Triangle",
                            fx.getDouble(fp1.get(fs3.get(t))),
                            fx.getDouble(p3),
                            1e-6);
                    assertEquals("Revoir le constructeur de Triangle",
                            fy.getDouble(fp1.get(fs3.get(t))),
                            fy.getDouble(p3),
                            1e-6);

                    assertEquals("Revoir le constructeur de Triangle",
                            fx.getDouble(fp2.get(fs3.get(t))),
                            fx.getDouble(p1),
                            1e-6);
                    assertEquals("Revoir le constructeur de Triangle",
                            fy.getDouble(fp2.get(fs3.get(t))),
                            fy.getDouble(p1),
                            1e-6);

                    x1 += 0.5;
                    y1 += 0.5;
                    x2 += 0.5;
                    y2 += 0.5;
                    x3 += 0.5;
                    y3 += 0.5;
                }
            } catch (SecurityException | InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
                fail("Revoir le constructeur de Point ");
            }
        }
    }

    @Test
    @Grade(3)
    public void testSetP1() {
        System.out.println("Test setP1 Triangle");
        getConstructorAndFields();

        Method setP1 = null;
        try {
            setP1 = Triangle.class.getDeclaredMethod("setP1", Point.class);
        } catch (NoSuchMethodException | SecurityException ex) {

        }

        if (fx != null && fy != null
                && setP1 != null
                && cpoint != null && csegment != null && ctriangle != null
                && fp1 != null && fp2 != null
                && fs1 != null && fs2 != null && fs3 != null) {

            try {
                double x1 = -2.5, y1 = -2;
                double x2 = -20.5, y2 = -20;
                double x3 = 2.5, y3 = -10;

                Point p1 = (Point) cpoint.newInstance(x1, y1);
                Point p2 = (Point) cpoint.newInstance(x2, y2);
                Point p3 = (Point) cpoint.newInstance(x3, y3);

                Triangle t = (Triangle) ctriangle.newInstance(p1, p2, p3);

                for (double x = -5; x <= 5; x += 0.5) {
                    for (double y = -5; y <= 5; y += 0.5) {
                        Point p = (Point) cpoint.newInstance(x, y);
                        setP1.invoke(t, p);

                        // Segment 1
                        assertEquals("Revoir setP1",
                                fx.getDouble(fp1.get(fs1.get(t))),
                                fx.getDouble(p),
                                1e-6);
                        assertEquals("Revoir setP1",
                                fy.getDouble(fp1.get(fs1.get(t))),
                                fy.getDouble(p),
                                1e-6);
                        // Segment 2

                        assertEquals("Revoir setP1",
                                fx.getDouble(fp2.get(fs3.get(t))),
                                fx.getDouble(p),
                                1e-6);
                        assertEquals("Revoir setP1",
                                fy.getDouble(fp2.get(fs3.get(t))),
                                fy.getDouble(p),
                                1e-6);
                    }
                }

            } catch (SecurityException | InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
                fail("Revoir setP1 ");
            }
        }
    }
    
    
    @Test
    @Grade(3)
    public void testSetP2() {
        System.out.println("Test setP2 Triangle");
        getConstructorAndFields();

        Method setP2 = null;
        try {
            setP2 = Triangle.class.getDeclaredMethod("setP2", Point.class);
        } catch (NoSuchMethodException | SecurityException ex) {

        }

        if (fx != null && fy != null
                && setP2 != null
                && cpoint != null && csegment != null && ctriangle != null
                && fp1 != null && fp2 != null
                && fs1 != null && fs2 != null && fs3 != null) {

            try {
                double x1 = -2.5, y1 = -2;
                double x2 = -20.5, y2 = -20;
                double x3 = 2.5, y3 = -10;

                Point p1 = (Point) cpoint.newInstance(x1, y1);
                Point p2 = (Point) cpoint.newInstance(x2, y2);
                Point p3 = (Point) cpoint.newInstance(x3, y3);

                Triangle t = (Triangle) ctriangle.newInstance(p1, p2, p3);

                for (double x = -5; x <= 5; x += 0.5) {
                    for (double y = -5; y <= 5; y += 0.5) {
                        Point p = (Point) cpoint.newInstance(x, y);
                        setP2.invoke(t, p);

                        // Segment 1
                        assertEquals("Revoir setP2",
                                fx.getDouble(fp1.get(fs2.get(t))),
                                fx.getDouble(p),
                                1e-6);
                        assertEquals("Revoir setP2",
                                fy.getDouble(fp1.get(fs2.get(t))),
                                fy.getDouble(p),
                                1e-6);
                        // Segment 2

                        assertEquals("Revoir setP2",
                                fx.getDouble(fp2.get(fs1.get(t))),
                                fx.getDouble(p),
                                1e-6);
                        assertEquals("Revoir setP2",
                                fy.getDouble(fp2.get(fs1.get(t))),
                                fy.getDouble(p),
                                1e-6);
                    }
                }

            } catch (SecurityException | InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
                fail("Revoir setP2 ");
            }
        }
    }

    
    @Test
    @Grade(3)
    public void testSetP3() {
        System.out.println("Test setP3 Triangle");
        getConstructorAndFields();

        Method setP3 = null;
        try {
            setP3 = Triangle.class.getDeclaredMethod("setP3", Point.class);
        } catch (NoSuchMethodException | SecurityException ex) {

        }

        if (fx != null && fy != null
                && setP3 != null
                && cpoint != null && csegment != null && ctriangle != null
                && fp1 != null && fp2 != null
                && fs1 != null && fs2 != null && fs3 != null) {

            try {
                double x1 = -2.5, y1 = -2;
                double x2 = -20.5, y2 = -20;
                double x3 = 2.5, y3 = -10;

                Point p1 = (Point) cpoint.newInstance(x1, y1);
                Point p2 = (Point) cpoint.newInstance(x2, y2);
                Point p3 = (Point) cpoint.newInstance(x3, y3);

                Triangle t = (Triangle) ctriangle.newInstance(p1, p2, p3);

                for (double x = -5; x <= 5; x += 0.5) {
                    for (double y = -5; y <= 5; y += 0.5) {
                        Point p = (Point) cpoint.newInstance(x, y);
                        setP3.invoke(t, p);

                        // Segment 1
                        assertEquals("Revoir setP3",
                                fx.getDouble(fp1.get(fs3.get(t))),
                                fx.getDouble(p),
                                1e-6);
                        assertEquals("Revoir setP3",
                                fy.getDouble(fp1.get(fs3.get(t))),
                                fy.getDouble(p),
                                1e-6);
                        // Segment 2

                        assertEquals("Revoir setP3",
                                fx.getDouble(fp2.get(fs2.get(t))),
                                fx.getDouble(p),
                                1e-6);
                        assertEquals("Revoir setP3",
                                fy.getDouble(fp2.get(fs2.get(t))),
                                fy.getDouble(p),
                                1e-6);
                    }
                }

            } catch (SecurityException | InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
                fail("Revoir setP3 ");
            }
        }
    }


    @Test
    @Grade(5)
    public void testToString() {
        System.out.println("Test toString");

        testConstructor();
        if (fx != null && fy != null && cpoint != null) {

            try {
                double x1 = -2.5, y1 = -2;
                double x2 = 2.5, y2 = 2;
                double x3 = 5.5, y3 = -20;
                for (int i = 0; i < 10; ++i) {
                    Point p1 = (Point) cpoint.newInstance(x1, y1);
                    Point p2 = (Point) cpoint.newInstance(x2, y2);
                    Point p3 = (Point) cpoint.newInstance(x3, y3);
                    Triangle t = (Triangle) ctriangle.newInstance(p1, p2, p3);
                    String value = (String) ReflectUtilities.getFromMethod(Triangle.class, t, "toString");
                    String vref = "<[(" + x1 + ", " + y1 + ") ; (" + x2 + ", " + y2 + ")], [(" + x2 + ", " + y2 + ") ; (" + x3 + ", " + y3 + ")], [(" + x3 + ", " + y3 + ") ; (" + x1 + ", " + y1 + ")]>";
                    assertEquals("Revoir toString()", vref.trim().replaceAll("[\\s]*", ""), value.trim().replaceAll("[\\s]*", ""));
                    x1 += 0.5;
                    y1 += 0.5;
                    x2 += 0.5;
                    y2 += 0.5;
                    x3 += 0.5;
                    y3 += 0.5;
                }
            } catch (SecurityException | InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException ex) {
                fail("Revoir toString()");
            }
        }
    }

}
