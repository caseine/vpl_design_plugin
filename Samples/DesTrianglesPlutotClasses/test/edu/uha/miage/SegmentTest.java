/*
 * @author : Yvan Maillot (yvan.maillot@uha.fr)
 */
package edu.uha.miage;

import caseine.format.javajunit.Grade;
import caseine.reflect.ReflectUtilities;
import caseine.tags.ClassTestPriority;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import org.junit.Test;
import static org.junit.Assert.*;


@ClassTestPriority(4)
public class SegmentTest {

    private Constructor<?> cpoint = null;
    private Constructor<?> csegment = null;
    private Field fx = null, fy = null, fp1 = null, fp2 = null;

    private void getConstructorAndFields() {

        try {
            cpoint = Point.class.getDeclaredConstructor(double.class, double.class);
        } catch (NoSuchMethodException | SecurityException | IllegalArgumentException ex) {
            fail("Constructeur Point(double x, double y) absent ou mal signé");
        }

        try {
            csegment = Segment.class.getDeclaredConstructor(Point.class, Point.class);
        } catch (NoSuchMethodException | SecurityException | IllegalArgumentException ex) {
            fail("Constructeur Segment(Point p1, Point p2) absent ou mal signé");
        }

        try {
            fx = Point.class.getDeclaredField("x");
            fy = Point.class.getDeclaredField("y");
        } catch (NoSuchFieldException | SecurityException ex) {
            fail("Attribut x ou y de Point absent ou mal signé");
        }

        try {
            fp1 = Segment.class.getDeclaredField("p1");
            fp2 = Segment.class.getDeclaredField("p2");
        } catch (NoSuchFieldException | SecurityException ex) {
            fail("Attribut p1 ou p2 de Segment absent ou mal signé");
        }

    }

    @Test
    @Grade(1)
    public void testConstructor() {
        System.out.println("Test constructeur Segment");
        getConstructorAndFields();
        if (fx != null && fy != null && cpoint != null && csegment != null && fp1 != null && fp2 != null) {

            fx.setAccessible(true);
            fy.setAccessible(true);
            fp1.setAccessible(true);
            fp2.setAccessible(true);
            
            try {
                double xa = -2.5, ya = -2;
                double xb = -20.5, yb = -20;
                for (int i = 0; i < 10; ++i) {
                    Point pa = (Point) cpoint.newInstance(xa, ya);
                    Point pb = (Point) cpoint.newInstance(xb, yb);
                    Segment s = (Segment) csegment.newInstance(pa, pb);
                    
                    
                    assertEquals("Revoir le constructeur de Segment",
                            fx.getDouble(fp1.get(s)),
                            fx.getDouble(pa),
                            1e-6);
                    assertEquals("Revoir le constructeur de Segment",
                            fy.getDouble(fp1.get(s)),
                            fy.getDouble(pa),
                            1e-6);
                    assertEquals("Revoir le constructeur de Segment",
                            fx.getDouble(fp2.get(s)),
                            fx.getDouble(pb),
                            1e-6);
                    assertEquals("Revoir le constructeur de Segment",
                            fy.getDouble(fp2.get(s)),
                            fy.getDouble(pb),
                            1e-6);
                    xa += 0.5;
                    ya += 0.5;
                    xb += 0.5;
                    yb += 0.5;
                }
            } catch (SecurityException | InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
                fail("Revoir le constructeur de Point ");
            }
        }
    }

    @Test
    @Grade(5)
    public void testToString() {
        System.out.println("Test toString");

        testConstructor();
        if (fx != null && fy != null && cpoint != null) {

            try {
                double xa = -2.5, ya = -2;
                double xb = 2.5, yb = 2;
                for (int i = 0; i < 10; ++i) {
                    Point p1 = (Point) cpoint.newInstance(xa, ya);
                    Point p2 = (Point) cpoint.newInstance(xb, yb);
                    Segment s = (Segment) csegment.newInstance(p1, p2);
                    String value = (String) ReflectUtilities.getFromMethod(Segment.class, s, "toString");
                    String vref = "[("+xa+", "+ya+") ; ("+xb+", "+yb+")]";
                    assertEquals("Revoir toString()", vref.trim().replaceAll("[\\s]*", ""), value.trim().replaceAll("[\\s]*", ""));
                    xa += 0.5;
                    ya += 0.5;
                    xb += 0.5;
                    yb += 0.5;
                }
            } catch (SecurityException | InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException ex) {
                fail("Revoir toString()");
            }
        }
    }

}
