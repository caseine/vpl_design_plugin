package number;


import caseine.tags.ToCheck;
import caseine.tags.ToCompare;
import caseine.tags.ToDo;
import caseine.tags.ToDoIn;



/**
 * A class representing a triplet of three integers
 * @author hadrien cambazard
 */
public class IntegerTriplet {

    @ToDo("Déclarer l'attribut a de type int")
    @ToCheck(value = "l'attribut a", priority=1)
    private int a;
    @ToDo("Déclarer l'attribut b de type int")
    @ToCheck(value = "l'attribut b", priority=1)
    private int b;
    @ToDo("Déclarer l'attribut c de type int")
    @ToCheck(value = "l'attribut c", priority=1)
    private int c;

    /**
     * Constructor using three values
     */
    @ToDo("Définir le constructeur qui affecte les trois attributs")
    @ToCheck(value = "le constructeur", priority=2)
    public IntegerTriplet(int a, int b, int c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }

    /**
     * @return the sum of the elements in the set
     */
    @ToDoIn("Ecrire une méthode publique sum() qui retourne la somme des trois attributs")
    @ToCompare(priority = 4)
    public int sum() {
        return a+b+c;
    }

    /**
     * @return the average value of the set
     */
    @ToDo("Ecrire une méthode publique average() qui retourne la moyenne des trois attributs")
    @ToCheck(value = "la méthodes average()", priority=3)
    @ToCompare(priority = 4)
    public double average() {
        return (a+b+c)/3.0;
    }

    /**
     * @return the string obtained by concatenating the three integers
     */
    @ToDo("Ecrire une méthode publique concatenate() qui retourne la chaine obtenue par concaténation des trois entiers")
    @ToCheck(value = "la méthodes concatenate()", priority=3)
    @ToCompare(priority = 4)
    public String concatenate() {
        return ""+a+b+c;
    }
}
