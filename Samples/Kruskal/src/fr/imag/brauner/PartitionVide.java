package fr.imag.brauner;

/**
 *
 * Structure Partition non optimisée
 * pour gérer des partitions d'un ensemble à {@code n} éléments
 *
 * @author Nadia Brauner
 */
public class PartitionVide {
    private int[] parent ;

    /**
     * Initialise une structure Partition avec {@code n} éléments
     * La partition est composée de singletons
     *
     * @param n nombre d'éléments dans la partition
     * @throws IllegalArgumentException si le paramètre {@code n} est négatif
     */
    public PartitionVide(int n) {
        // TODO
    }

    /**
     * @return la référence de la classe de l'élément {@code i}
     * @throws IllegalArgumentException si le paramètre {@code i} n'est pas valide
     */
    public int trouver(int i){
        // TODO
        return -1 ;
    }


    /**
     * Reunit les classes d'equivalence de {@code i} et {@code j}
     *
     * @param i
     * @param j
     *
     * @throws IllegalArgumentException si l'un des paramètres {@code i} ou {@code j} n'est pas valide
     */
    public void unir(int i , int j){
        //TODO

    }

    /**
     * indique si {@code i} et {@code j} sont dans la même composante connexe
     *
     * @param i
     * @param j
     *
     * @return true si {@code i} et {@code j} sont dans la même composante connexe
     * @throws IllegalArgumentException si l'un des paramètres {@code i} ou {@code j} n'est pas valide
     */
    public boolean sontConnectes(int i, int j){
        // TODO
        return false ;
    }

    /**
     * vérifie que {@code i} est un paramètre valide
     *
     * @param i
     *
     * @throws IllegalArgumentException sauf si 0 <= {@code i} < n
     *
     */
    private void validateIndex(int i) {
        //TODO
    }
}
