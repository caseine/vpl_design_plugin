package fr.imag.brauner;

import caseine.tags.ClassTestPriority;
import caseine.tags.ToDoIn;

/**
 *
 * Structure Partition non optimisée
 * pour gérer des partitions d'un ensemble à {@code n} éléments
 *
 * @author Nadia Brauner
 */
@ClassTestPriority(1)
public class Partition {
	
    private int[] parent ;

    /**
     * Initialise une structure Partition avec {@code n} éléments
     * La partition est composée de singletons
     *
     * @param n nombre d'éléments dans la partition
     * @throws IllegalArgumentException si le paramètre {@code n} est négatif
     */
    public Partition(int n) {
       if (n<1) throw new IllegalArgumentException("Le nombre d'éléments doit etre strictement positif");

        this.parent = new int[n] ;

        for(int i=0 ; i<n ; i++){
            parent[i] = i ;
        }
    }

    /**
     * TODO 1.1
     * @return la référence de la classe de l'élément {@code i}
     * @throws IllegalArgumentException si le paramètre {@code i} n'est pas valide
     */
    @ToDoIn("1.1")
    public int trouver(int i){
        verifieIndex(i);

        while(i != parent[i]){
            i=parent[i] ;
        }
        return i ;
    }


    /**
     * Reunit les classes d'equivalence de {@code i} et {@code j}
     *
     * @param i
     * @param j
     *
     * @throws IllegalArgumentException si l'un des paramètres {@code i} ou {@code j} n'est pas valide
     */
    @ToDoIn
    public void unir(int i , int j){
        //validateIndex(i);
        //validateIndex(j);
        if (trouver(i) == trouver(j))
            throw new IllegalArgumentException(String.format("%d et %d sont déjà unis",i,j));
        parent[trouver(j)] = trouver(i) ;
    }

    /**
     * indique si {@code i} et {@code j} sont dans la même composante connexe
     *
     * @param i
     * @param j
     *
     * @return true si {@code i} et {@code j} sont dans la même composante connexe
     * @throws IllegalArgumentException si l'un des paramètres {@code i} ou {@code j} n'est pas valide
     */
    @ToDoIn
    public boolean sontConnectes(int i, int j){
        verifieIndex(i);
        verifieIndex(j);

        return (trouver(i) == trouver(j)) ;
    }

    /**
     * vérifie que {@code i} est un paramètre valide
     *
     * @param i
     *
     * @throws IllegalArgumentException sauf si 0 <= {@code i} < n
     *
     */
    @ToDoIn
    private void verifieIndex(int i) {
        if (i < 0 || i >= parent.length)
            throw new IllegalArgumentException("le sommet " + i + " n'est pas entre 0 et " + (parent.length-1));
    }
}
