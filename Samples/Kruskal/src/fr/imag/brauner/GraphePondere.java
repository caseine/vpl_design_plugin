package fr.imag.brauner;

import caseine.tags.ClassTestPriority;
import java.io.*;
import java.util.*;

import caseine.tags.ToDoIn;

/**
 * Classe qui implémente des graphes non orientés pondérés
 *
 * @author Nadia Brauner
 */
@ClassTestPriority(2)
public class GraphePondere {

    private final int n; // nombre de sommets. Ne peut etre assigné qu'une fois
    private int m ; // nombre d'arêtes du graphe

    private ArrayList<LinkedList<Arete>> adjacence; // listes d'arêtes incidentes à chaque sommet

    /**
     * Initialise l'instance de graphe avec {@code n} sommets
     *
     * @param n le nombre de sommets du graphe. {@code n} doit etre strictement positif.
     * @throws IllegalArgumentException si {@code n} est négatif ou nul
     */
    public GraphePondere(int n) {
        if (n < 1)
            throw new IllegalArgumentException("le nombre de sommets doit etre strictement positif");
        this.n = n;
        this.m = 0 ;
        this.adjacence = new ArrayList<LinkedList<Arete>>(n);
        for (int i = 0; i < n; i++)
            adjacence.add(new LinkedList<Arete>());
    }

    /**
     * Construit un graphe à partir d'un flux de données (InputStream)
     *
     * @param input données du graphe à construire
     * @return un graphe
     * @throws GrapheReaderException en cas d'erreur de lecture de {@code input}
     */
    public static GraphePondere read(final InputStream input) throws GrapheReaderException {
        return new GPReader().read(input);
    }

    /**
     * renvoie le nombre de sommets du graphe
     *
     * @return le nombre de sommets (toujours positif)
     */
    public int getN() {
        return this.n;
    }

    /**
     * renvoie le nombre d'aretes du graphe
     * @return le nombre d'aretes
     */
    public int getM() {
        return this.m;
    }

    /**
     * ajoute l'arête {@code e} au graphe <BR>
     * On peut rajouter une arête déjà existante ou faire des boucles !
     *
     * @param e arête à rajouter au graphe
     * @throws IllegalArgumentException si {@code e} n'est pas une arête valide
     */
    public void ajouteArete(Arete e){

        verifieSommet(e.unSommet());
        verifieSommet(e.autreSommet(e.unSommet()));

        this.adjacence.get(e.unSommet()).add(e);
        this.adjacence.get(e.autreSommet(e.unSommet())).add(e);
        m++ ;
    }

    /**
     * vérifie que le sommet {@code v} est un sommet valide du graphe
     *
     * @param v sommet du graphe à vérifier
     * @throws IllegalArgumentException si le sommet n'est pas valide
     */
    private void verifieSommet(int v) {
        if (v >= getN() || v < 0)
            throw new IllegalArgumentException(String.format("Le sommet %d n'est pas valide", v));
    }

    /**
     * Renvoie la liste des voisins du sommet {@code v}
     * Cette liste ne peut pas être modifiée
     *
     * @param v sommet du graphe
     * @return la liste des voisins du sommet {@code v}
     * @throws IllegalArgumentException si le sommet n'est pas valide
     */
    public List<Arete> voisins(int v) {
        verifieSommet(v);

        return Collections.unmodifiableList(adjacence.get(v));
    }


    /**
     * @return un arbre couvrant de poids min avec l'algorithme de Kruskal
     */
    @ToDoIn
    public GraphePondere kruskal(){
        GraphePondere lArbre = new GraphePondere(n) ;
        Partition uf = new Partition(n) ;
        Arete[] lesAretes = listeAretes();

        Arrays.sort(lesAretes) ;

        //for (int i=0 ; i<lesAretes.length ; i++){
          //  Arete e = lesAretes[i];
        for (Arete e:lesAretes){
            int u = e.unSommet() ;
            int v = e.autreSommet(u) ;
            if (!uf.sontConnectes(u,v)) {
                lArbre.ajouteArete(e);
                uf.unir(u, v);
            }
        }
        return lArbre ;
    }


    // <editor-fold desc="utils à ne pas copier dans caseine">
    /**
     *
     * @return un tableau qui contient toutes les arêtes du graphe
     */
    public Arete[] listeAretes(){
        Arete[] listeAretes = new Arete[m*2] ;
        int compteur = 0 ;

        for(LinkedList<Arete> courant:adjacence){
            for (Arete a:courant) {
                listeAretes[compteur] = a ;
                compteur++;
            }
        }
        return listeAretes ;
    }



    /**
     *
     * @return la somme des poids de toutes les arêtes du graphe
     */
    public double poidsTotal(){
        Arete[] lesAretes = listeAretes();

        double s =0 ;

        for(int i = 0 ; i< lesAretes.length ; i++){
            s+= lesAretes[i].getPoids();
        }
        return s/2 ;
    }


    /**
     *
     * @return le nombre de composantes connexes
     */
    public int nbCompo(){
        Partition uf = new Partition(n) ;

        for (int courant=0 ; courant<n ; courant++){
            for (int j =0 ; j<adjacence.get(courant).size(); j++){
                int voisinCourant = (int) adjacence.get(courant).get(j).autreSommet(courant);
                uf.unir(courant,voisinCourant) ;
            }
        }

        int s=1 ;
        for (int i =1 ; i<n ; i++){
            if (!uf.sontConnectes(i-1,i)){
                s++ ;
                uf.unir(i-1,i) ;
            } ;
        };
        return s ;
    }

    /**
     *
     * @return true si this est un arbre
     */
    public boolean estArbre(){

        return ((listeAretes().length)/2==n-nbCompo()) ;
    }

    // </editor-fold>

    private  static final class GPReader {

        public GraphePondere read(final InputStream input) throws GrapheReaderException {
            try {
                final BufferedReader reader = new BufferedReader(new InputStreamReader(input)); {

                final GraphePondere g;
                String line;

                String[] items = readNextLine(reader);

                if ((items == null) || (items.length < 2) || !"w".equals(items[0]))
                    throw new GrapheReaderException("il manque la ligne w",null);

                g = new GraphePondere(toInt(items[2]));


                while (items != null) {
                    items = readNextLine(reader);
                    if ((items != null) && (items.length > 0) && "e".equals(items[0])) {
                        g.ajouteArete(new Arete(toInt(items[1]) , toInt(items[2]) , Double.parseDouble(items[3])));
                    }
                }
                return g;
            }
            } catch (IOException e) {
                throw new GrapheReaderException(e.getMessage(),e);
            }
        }

        private String[] readNextLine(BufferedReader reader) throws IOException {
            String line = reader.readLine();
            while ((line != null) && line.startsWith("c")) {
                line = reader.readLine();
            }
            return (line == null) ? null : line.split("\\s+");
        }


        private static int toInt(String s) throws GrapheReaderException {
            try {
                return Integer.parseInt(s);
            } catch (NumberFormatException e) {
                throw new GrapheReaderException(String.format("'%1$s' n'est pas un entier", s),e) ;
            }
        }
    }

    public static class GrapheReaderException extends Exception {

        public GrapheReaderException(String message, Throwable cause) {
            super(message, cause);
        }

    }

}
