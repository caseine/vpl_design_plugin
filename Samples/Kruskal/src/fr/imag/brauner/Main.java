package fr.imag.brauner;

import java.io.InputStream;

public class Main {

    public static void main(String[] args) {
        try {
            InputStream input = Main.class.getResourceAsStream("maison.txt");
            
            System.out.println("Input= " + input);
       
            GraphePondere g1 = GraphePondere.read(input);

            GraphePondere couvrant_g1 = g1.kruskal();
            
            System.out.println(couvrant_g1);
        }
        catch(Exception e) {
            e.printStackTrace();
        }
    }
}

