package fr.imag.brauner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.InputStream;
import java.lang.reflect.Method;

import org.junit.BeforeClass;
import org.junit.Test;

import caseine.format.javajunit.Grade;
import caseine.tags.ClassTestPriority;

/**
 * @author Nadia Brauner
 */
@ClassTestPriority(2)
public class Arbre_test {

    @BeforeClass
    public static void printTest() {
        for (final Method m : Arbre_test.class.getMethods()) {
            System.out.printf(
                    " \n Method = %1$s",
                    m.getName());
        }
    }

    public static double poidsTotal(GraphePondere g) {
        Arete[] lesAretes = listeAretes(g);
        double s = 0;
        for (int i = 0; i < lesAretes.length; i++) {
            s += lesAretes[i].getPoids();
        }
        return s / 2;
    }

    /**
     *
     * @return un tableau qui contient toutes les aretes du graphe
     */
    public static Arete[] listeAretes(GraphePondere g) {
        Arete[] listeAretes = new Arete[g.getM() * 2];
        int compteur = 0;

        for (int i = 0; i < g.getN(); i++) {
            for (Arete a : g.voisins(i)) {
                listeAretes[compteur] = a;
                compteur++;
            }
        }
        return listeAretes;
    }

    @Test
    @Grade(5)
    public void maisonArbre() throws Exception {
        InputStream input = Arbre_test.class.getResourceAsStream("maison.txt");
        if (input != null) {
            GraphePondere g = GraphePondere.read(input);
            GraphePondere couvrant_g = g.kruskal();
            assertEquals("Sur maison.txt, le graphe calculé n'est pas un arbre", (listeAretes(couvrant_g).length) / 2, couvrant_g.getN() - 1);
        } else {
            fail("Fichier absent");
        }
    }


    @Test
    @Grade(5)
    public void maisonUnitaireArbre()throws Exception {
        InputStream input = Arbre_test.class.getResourceAsStream("maisonUnitaire.txt");
        GraphePondere g = GraphePondere.read(input);
        GraphePondere couvrant_g = g.kruskal() ;
        assertEquals("Sur maisonUnitaire.txt, le graphe calculé n'est pas un arbre", listeAretes(couvrant_g).length/2,couvrant_g.getN()-1);
    }

    @Test
    @Grade(5)
    public void maisonPoids() throws Exception {
        InputStream input = Arbre_test.class.getResourceAsStream("maison.txt");
        GraphePondere g = GraphePondere.read(input);
        GraphePondere couvrant_g = g.kruskal() ;
        assertEquals("Le poids attendu pour la maison est 6", 6.0,poidsTotal(couvrant_g),0.001);
    }


    @Test
    @Grade(5)
     public void maisonUnitairePoids() throws Exception {
        InputStream input = Arbre_test.class.getResourceAsStream("maisonUnitaire.txt");
        GraphePondere g = GraphePondere.read(input);
        GraphePondere couvrant_g = g.kruskal() ;
        assertEquals("Le poids attendu pour la maison unitaire est 4",4.0,poidsTotal(g.kruskal()),0.001);
    }

    @Test
    @Grade(5)
    public void sansAreteForet() {
        GraphePondere g = new GraphePondere(4);
        GraphePondere couvrant_g = g.kruskal() ;
        assertEquals("Le graphe est composé de 4 sommets isolés", (listeAretes(couvrant_g).length)/2,couvrant_g.getN()-4);
    }

    @Test
    @Grade(5)
    public void sansAretePoids() {
        GraphePondere g = new GraphePondere(4);
        GraphePondere couvrant_g = g.kruskal() ;
        assertEquals("Le graphe ne contient pas d'arêtes", 0.0,poidsTotal(couvrant_g),0.001);
    }

    @Test
    @Grade(5)
    public void graphe8Arbre() throws Exception {
        InputStream input = Arbre_test.class.getResourceAsStream("test8.txt");
        GraphePondere g = GraphePondere.read(input);
        GraphePondere couvrant_g = g.kruskal() ;
        assertEquals("le graphe calcule n'est pas un arbre", listeAretes(couvrant_g).length/2,couvrant_g.getN()-1);
    }

    @Test
    @Grade(5)
    public void graphe8Poids() throws Exception {
        InputStream input = Arbre_test.class.getResourceAsStream("test8.txt");
        GraphePondere g = GraphePondere.read(input);
        assertEquals("Les poids ne sont pas entiers",1.81,poidsTotal(g.kruskal()),0.001);
    }

    @Test
    @Grade(5)
    public void graphe250Arbre() throws Exception {
        InputStream input = Arbre_test.class.getResourceAsStream("test250.txt");
        GraphePondere g = GraphePondere.read(input);
        GraphePondere couvrant_g = g.kruskal() ;
        assertTrue("le graphe calculé n'est pas un arbre",(listeAretes(couvrant_g).length)/2==couvrant_g.getN()-1);
    }

    @Test
    @Grade(5)
    public void graphe250Poids() throws Exception {
        InputStream input = Arbre_test.class.getResourceAsStream("test250.txt");
        GraphePondere g = GraphePondere.read(input);
        assertTrue("Attention plus gros graphe",Math.abs(poidsTotal(g.kruskal())-10.46351)<0.00001);
    }

    @Test
    @Grade(5)
    public void nonConnexeForet() throws Exception {
        InputStream input = Arbre_test.class.getResourceAsStream("testNonConnexe.txt");
        GraphePondere g = GraphePondere.read(input);
        GraphePondere couvrant_g = g.kruskal() ;
        assertTrue("le graphe calcule n'est pas une foret",(listeAretes(couvrant_g).length)/2==couvrant_g.getN()-2);
    }

    @Test
    @Grade(5)
    public void nonConnexePoids() throws Exception {
        InputStream input = Arbre_test.class.getResourceAsStream("testNonConnexe.txt");
        GraphePondere g = GraphePondere.read(input);
        assertEquals("Attention le graphe n'est pas connexe",9.0,poidsTotal(g.kruskal()),0.001);
    }

    @Test
    @Grade(5)
    public void grosArbre() throws Exception {
        System.out.println(Arbre_test.class.getResource("testGros.txt"));
        InputStream input = Arbre_test.class.getResourceAsStream("testGros.txt");
        GraphePondere g = GraphePondere.read(input);
        GraphePondere couvrant_g = g.kruskal() ;
        assertEquals("le graphe calculé n'est pas une forêt",(listeAretes(couvrant_g).length)/2,couvrant_g.getN()-1);
    }

    @Test
    @Grade(10)
    public void grosPoids() throws Exception {
        InputStream input = Arbre_test.class.getResourceAsStream("testGros.txt");
        GraphePondere g = GraphePondere.read(input);
        assertTrue("Attention le graphe n'est pas connexe", Math.abs(poidsTotal(g.kruskal())-20.7732)<0.00001);
    }
}
