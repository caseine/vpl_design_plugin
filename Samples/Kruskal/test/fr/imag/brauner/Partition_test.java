package fr.imag.brauner;


import static org.junit.Assert.assertTrue;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import caseine.format.javajunit.Grade;
import caseine.tags.ClassTestPriority;

/**
 * @author Nadia Brauner
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@ClassTestPriority(1)
public class Partition_test {


    @Test
    @Grade(5)
    public void trouver() {
        int n = 5;

        Partition p = new Partition(n);
        for (int i = 0; i < n; i++)
            assertTrue("test trouver juste apres l'initialisation", p.trouver(i) == i);
    }

    @Test
    @Grade(5)
    public void trouver_unir() {
        Partition p = new Partition(4);
        p.unir(0, 1);
        String s = "problème avec trouver ou unir";
        assertTrue(s, p.trouver(0) == p.trouver(1));
        assertTrue(s, p.trouver(0) != p.trouver(2));
        p.unir(2, 1);
        assertTrue(s, p.trouver(1) == p.trouver(2));
        assertTrue(s, p.trouver(0) == p.trouver(2));
        assertTrue(s, p.trouver(0) == p.trouver(1));
        assertTrue(s, p.trouver(0) != p.trouver(3));
    }


    @Test
    @Grade(5)
    public void partition_complet() {
        Partition p = new Partition(5);
        String s = "test complet de partition";
        assertTrue(s, !p.sontConnectes(0, 1) && !p.sontConnectes(1, 2) && !p.sontConnectes(0, 2));
        p.unir(0, 1);
        p.unir(1, 2);
        p.unir(2, 3);
        assertTrue(s, p.sontConnectes(0, 1) && p.sontConnectes(1, 2) && p.sontConnectes(2, 3));
        assertTrue(s, p.sontConnectes(0, 3) && !p.sontConnectes(0, 4));

        Partition p1 = new Partition(5);
        assertTrue(s, !p1.sontConnectes(0, 1) && !p1.sontConnectes(1, 2) && !p1.sontConnectes(0, 2));
        p1.unir(1, 2);
        p1.unir(1, 3);
        p1.unir(1, 4);
        assertTrue(s, p1.sontConnectes(1, 4) && p1.sontConnectes(1, 2) && p1.sontConnectes(2, 3));
        assertTrue(s, p1.sontConnectes(4, 3) && !p1.sontConnectes(0, 4));
    }


    @Test(expected = IllegalArgumentException.class)
    @Grade(2)
    public void exception_constructeur() {
        Partition p = new Partition(-1);
    }

    @Test
    @Grade(1)
    public void exception_unir() {
        Partition p = new Partition(3);
        boolean leve = false;
        try {
            try {
                p.unir(-1, 0);
            } catch (IllegalArgumentException e) {
                leve = true;
            }
            try {
                p.unir(3, 0);
            } catch (IllegalArgumentException e) {
            }
            try {
                p.unir(0, -1);
            } catch (IllegalArgumentException e) {
            }
            try {
                p.unir(0, 3);
            } catch (IllegalArgumentException e) {
            }
        } catch(Exception e)
        {leve = false;}
        assertTrue("La méthode unir ne lève pas correctement les exceptions", leve);

    }


    @Test
    @Grade(1)
    public void exception_trouver() {
        Partition p = new Partition(3);
        boolean leve = false;
        try {
            p.trouver(3);
        } catch (IllegalArgumentException e) {
            leve = true;
        } catch (Exception e) {
            leve = false;
        }
        try {
            p.trouver(-1);
            leve = false;
        } catch (IllegalArgumentException e) {
        } catch (Exception e) {
            leve = false;
        }
        assertTrue("La méthode trouver ne lève pas correctement les exceptions", leve);
    }

    @Test
    @Grade(1)
    public void exception_sont_connectes() {
        Partition p = new Partition(3);
        boolean leve = false;
        try {
            try {
                p.sontConnectes(0, 3);
            } catch (IllegalArgumentException e) {
                leve = true;
            }
            try {
                p.sontConnectes(-1, 0);
                leve = false;
            } catch (IllegalArgumentException e) {
            }
            try {
                p.sontConnectes(3, 0);
                leve = false;
            } catch (IllegalArgumentException e) {
            }
            try {
                p.sontConnectes(0, -1);
                leve = false;
            } catch (IllegalArgumentException e) {
            }
        } catch (Exception e) {
            leve = false;
        }
        assertTrue("la méthode sont_connectes ne lève pas correctement les exceptions", leve);
    }

    @Test
    @Grade(5)
    public void exception_deja_unis() {
        int taille = 5;
        int i = 1;
        int j = 2;
        Partition p = new Partition(taille);
        p.unir(i, j);
        boolean leve = false;
        try {
            try {
                p.unir(i, j);
            } catch (IllegalArgumentException e) {
                leve = true;
            }
            try {
                p.unir(i, i);
                leve = false;
            } catch (IllegalArgumentException e) {
            }
        } catch (Exception e) {
            leve = false;
        }
        assertTrue("la méthode unir() accepte d'unir des éléments qui sont déjà dans la même partition", leve);
    }

}
