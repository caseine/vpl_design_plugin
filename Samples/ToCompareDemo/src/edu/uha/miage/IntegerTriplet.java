package edu.uha.miage;

import caseine.tags.RelativeEvaluation;
import caseine.tags.ToCompare;
import caseine.tags.ToDoIn;
import java.util.Scanner;

/**
 * Classe de démonstration du tag @Compare
 *
 * @author Yvan Maillot <yvan.maillot@uha.fr>
 */
@RelativeEvaluation
public class IntegerTriplet {

    private final int a;
    private final int b;
    private final int c;

    public IntegerTriplet(int a, int b, int c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }

    /* 
    
        L'énoncé de l'exercice pourrait être "écrire un getter pour l'attribut a"
    
        Dans ce cas de déclaration de @ToCompare, toutes les valeurs par défaut
        sont données aux propriétés :
      
            1. la propriété priority est 0. Ce test sera fait avant ceux de 
               priorités supérieurs.

            2. la propriété value est "". Un message standard sera présenté à 
               l'étudiant en cas d'échec.

            3. la propriété grade est 0. Cette évaluation ne compte pas dans la note.

            4. C'est la génération automatique des jeux de tests qui est choisie. 
               En l'occurrence, 100 entiers sont choisis au hasard pour a et le
               résultat de la méthode getA() de l'étudiant est comparé à celui
               de la méthode de référence.

               Il n'y aucune précaution à prendre ici. Toutes les valeurs traitées
               sont des types primitifs et 100 entiers au hasard sont largement 
               suffisants. 10 seraient suffisants... 
     */
    @ToDoIn("1.00a Very easy : returns a")
    @ToCompare
    public int getA() {
        return a;
    }

    // Même chose que pour a, mais avec moins de tests cette fois.
    @ToDoIn("1.00b Very easy : returns b")
    @ToCompare(numberOfAleaTestSets = 10)
    public int getB() {
        return b;
    }

    // Même chose que pour b, mais avec un message adapté
    @ToDoIn("1.00c Very easy : returns c")
    @ToCompare(
            value = "Votre getter pour c est incorrect.",
            numberOfAleaTestSets = 10
    )
    public int getC() {
        return c;
    }

    /* 
        L'énoncé de l'exercice pourrait être 
    
            "Une méthode qui retourne la somme des attributs de cette classe."
    
        Dans ce cas
      
            1. la propriété priority est 1.
      
            2. la propriété value est "". Un message standard sera présenté à 
                l'étudiant en cas d'échec.
      
            3. la propriété grade est 1. Cette évaluation comptera dans la note,
                pour un poids de 1.
      
            4. C'est la génération automatique des jeux de tests qui est choisie. 
                En l'occurrence, 10 entiers sont choisis au hasard pour a, b, c et
                le résultat de la méthode sum() de l'étudiant est comparé à celui
                de la méthode de référence.
      
                Il n'y aucune précaution à prendre ici. Toutes les valeurs traitées
                sont des types primitifs et 10 triplets d'entiers au hasard sont 
                largement suffisants.
     */
    @ToDoIn("1.01 Returns the sum of a, b, c")
    @ToCompare(
            priority = 1,
            grade = 1,
            numberOfAleaTestSets = 10
    )
    public int sum() {
        return a + b + c;
    }

    /*     
        Même chose que pour la somme mais c'est la moyenne qu'on demande cette fois
        avec une autre priorité et un autre grade.
    
        Toutefois, si le cas présent c'est suffisant, c'est un peu dangereux 
        dans le cas général car les comparaisons se font entre des valeurs de 
        type double. Ça marche dans ce cas car il est fort probable que la 
        moyenne correctement calculée par l'étudiant sera rigoureusement la même 
        (sans erreur d'arrondie) que celle de référence.

        Un autre test est montré plus loin pour la méthode averageSafer afin de
        montrer comment générer des tests pour des comparaisons spécifiques entre
        valeurs de type double.
     */
    @ToDoIn("1.02 Returns the average of a, b, c")
    @ToCompare(
            priority = 2,
            grade = 2,
            numberOfAleaTestSets = 10
    )
    public double average() {
        return (a + b + c) / 3.0;
    }

    /* 
        Un exemple similaire aux deux précédents qui montre que ça fonctionne 
        aussi quand les retours sont de type String.
    
        Le champ value est rempli pour fournir à l'étudiant un message d'aide.
     */
    @ToDoIn("1.03 Returns the concatenation of a, b, c")
    @ToCompare(
            value = "La concatenation s'est mal passée. Indice : l'ordre des opérations peut avoir une importance.",
            priority = 3,
            grade = 2,
            numberOfAleaTestSets = 10)
    public String concatenate() {
        return "" + a + b + c;
    }

    /* 
        Un exemple similaire aux trois précédents qui fonctionne encore avec une
        génération automatique de jeux de test.
     */
    @ToDoIn("1.04 Returns the min of a, b, c")
    @ToCompare(priority = 4, grade = 3, numberOfAleaTestSets = 10)
    public int min() {
        if (a < b) {
            if (a < c) {
                return a;
            } else {
                return c;
            }
        } else {
            if (b < c) {
                return b;
            } else {
                return c;
            }
        }
    }

    /* 
        Méthode qui retourne vrai si les 3 attributs a, b, c et les trois 
        paramètres d, e, f sont ordonnées.
    
        Voici un premier exemple de question que la génération aléatoire de jeux
        de test NE permet PAS de résoudre. 
    
        Générer aléatoirement un jeu de test signifie tirer au hasard 6 entiers
        et comparer le résultat de la méthode de l'étudiant sur ces 6 entiers
        avec celui de la méthode de référence sur ces 6 mêmes entiers.
        Or, il est fort probable que ces 6 entiers ne soient jamais ordonnées.
        En tout cas, la probabilité qu'ils le soient est faible.
        Donc, si l'étudiant a simplement laissé return false; le test réussira
        le plus souvent.                       

        Une solution pour s'en sortir est de générer suffisant de jeux de tests
        aléatoire. Mais ce n'est pas très efficace, il reste une incertitude.

        C'est ce que tente de faire cet exemple. 500 est un cas limite. Parfois
        return false; réussit le test, parfois non.

        Ce n'est pas une bonne solution. 
    
        L'exemple suivant : areSortedSafer montre une meilleure solution où les  
        jeux de sont choisis par l'enseignant.
     */
    @ToDoIn("1.05 Returns a b c d e f are sorted")
    @ToCompare(
            priority = 5,
            grade = 3,
            numberOfAleaTestSets = 500)
    public boolean areSorted(int d, int e, int f) {
        return a <= b && b <= c && c <= d && d <= e && e <= f;
    }

    /* Voici donc ce qu'il faudrait plutôt faire : produire explicitement des
       jeux de test pertinents (aléatoires ou non). 
    
       Certains qui échouent. 
    
       D'autres qui réussissent.
    
       Pour ça, la propriété testSetsMethodName = "edu.uha.miage.IntegerTripletTest.setTestsAreSorted"
    
       Cela signifie que la méthode
    
       public static List<ToCompareProducer.InstanceAndParameters> setTestsAreSorted()
    
       doit exister dans la classe public edu.uha.miage.IntegerTripletTest
    
       Cette méthode retourne une liste (List<ToCompareProducer.InstanceAndParameters>)
    
       qui une liste de jeux de tests chacun d'eux étant décrit par la classe
    
       ToCompareProducer.InstanceAndParameters
    
       Voir cette méthode.

     */
    @ToDoIn("1.06 Returns a b c d e f are sorted")
    @ToCompare(
            priority = 6,
            grade = 3,
            testSetsMethodName = "edu.uha.miage.IntegerTripletTest.setTestsAreSorted"
    )
    public boolean areSortedSafer(int d, int e, int f) {
        return a <= b && b <= c && c <= d && d <= e && e <= f;
    }

    /* Un autre exemple d'exercice qui nécessite l'emploi de jeux de tests.
    
       La méthode à réaliser doit retourner la somme de cet IntegerTriplet avec
       un autre passé en paramètre.
    
       La génération automatique de jeux de tests ne peut pas fonctionner parce                      
       les paramètres ainsi que le retour de la méthode à écrire ne sont pas de   
       type primitif.                                                                   
    
       Il faut générer des jeux de test dans une méthode. C'est fait dans la 
       méthode setTestsAdd() de la edu.uha.miage.IntegerTripletTest.classe 
    
       Voir cette méthode.

     */
    @ToDoIn("1.07 Returns an IntegerTriplet which the sum between this and it")
    @ToCompare(
            priority = 7,
            grade = 4,
            testSetsMethodName = "edu.uha.miage.IntegerTripletTest.setTestsAdd"
    )
    public IntegerTriplet add(IntegerTriplet it) {
        return new IntegerTriplet(it.a + a, it.b + b, it.c + c);
    }

    /* Maintenant que nous savons générer nos propres jeux de tests. Nous 
       pouvons sur le problème évoqué plus haut de la méthode average.
    
       Le retour de cette méthode est de type double. Il n'est recommandé de 
       comparer exactement deux doubles. Il faut le faire selon une approximation.
    
       Pour mettre en oeuvre ce mécanisme, il faut générer des jeux de tests et
       utiliser la propriété comparatorMethodName de @ToCompare.
    
       La propriété comparatorMethodName est une String qui représente le nom 
       pleinement qualifié d'une méthode accessible.
    
       Cette méthode, que l'enseignant doit écrire, sera utilisée pour comparer
       les résultats de l'étudiant avec ceux de la référence.
    
        Dans notre exemple, la méthode 
    
        testSetsMethodName = "edu.uha.miage.IntegerTripletTest.setTestsAverage",
    
        est utilisée pour les jeux de tests.
    
        et la méthode 
    
        comparatorMethodName = "edu.uha.miage.IntegerTripletTest.averageComparator"
    
        est utilisée pour effectuer leur comparaison.
    
        Voir ces deux méthodes et leurs explications.
    
        Par ailleurs, comme j'utilise les getters et sum dans la méthode de
        comparaison. J'aimerais m'assurer que ceux-ci ont été réalisés 
        correctement. C'est ce que me permet de faire la propriété 
        requiersUnitTestsBefore

     */
    @ToDoIn("1.08 Returns an IntegerTriplet which the sum between this and it")
    @ToCompare(
            priority = 8,
            grade = 4,
            testSetsMethodName = "edu.uha.miage.IntegerTripletTest.setTestsAleaABC",
            comparatorMethodName = "edu.uha.miage.IntegerTripletTest.averageComparator",
            // Le succés de ce test passe par ceux des tests de priorités 0 et 1
            // C'est-à-dire les getters et la somme.
            requiersUnitTestsBefore = {"0", "1"}
    )
    public double averageSafer() {
        return sum() / 3.0;
    }

    /*
        L'énoncé pourrait être "afficher à l'écran les attributs a, b et c, dans 
        cet ordre, sur une ligne, séparés par un espace et terminé par un saut 
        de ligne.
    
        La différence avec les exemples précédents est l'affichage à l'écran.
    
        Si l'on utilise la propriété testSetsMethodName, il est possible de 
        préciser que la comparaison se fait aussi entre les sorties 
        standards.
    
        Parce que l'on peut utiliser la méthode edu.uha.miage.IntegerTripletTest.setTestsPrintABC()
        déjà écrite pour average, il n'y a rien à écrire de plus.
     */
    @ToDoIn("1.09 Print a b c")
    @ToCompare(
            priority = 9,
            grade = 1,
            // Une méthode de générations de a b c (la même que celle utilisée pour average)
            testSetsMethodName = "edu.uha.miage.IntegerTripletTest.setTestsAleaABC",
            // Pour tenir compte de la sortie standard
            checkStdOut = true
    )
    public void print() {
        System.out.println(a + " " + b + " " + c);
    }

    /*
        L'évaluation de la méthode précédente présente un inconvénient. Elle 
        n'est pas assez souple. Si l'étudiant oublie le saut de ligne à la fin
        ou si au contraire il place un espace de trop, le test échoue.
    
        La rigueur de temps en temps peut avoir des vertues. Mais on peut aussi
        parfois vouloir être plus souple.
    
        @ToCompare le permet grace à la propriété comparatorMethodName.
    
        Par exemple 
    
        comparatorMethodName = "edu.uha.miage.IntegerTripletTest.printComparator"
    
        signifie que la méthode suivante doit être accessible 
    
        public static String printCompare(caseine.publication.producers.ToCompareProducer.ResultForComparison rfc)
    
        Elle est à observer dans edu.uha.miage.IntegerTripletTest.
     */
    @ToDoIn("1.10 Print a b c plus souple")
    @ToCompare(
            priority = 10,
            grade = 1,
            // Une méthode de générations de a b c (la même que celle utilisée 
            // pour average et print)
            testSetsMethodName = "edu.uha.miage.IntegerTripletTest.setTestsAleaABC",
            // Pour tenir compte de la sortie standard
            checkStdOut = true,
            // Pour comparer 
            comparatorMethodName = "edu.uha.miage.IntegerTripletTest.printComparator"
    )
    public void softerPrint() {
        System.out.println(a + " " + b + " " + c);
    }

    /*
        L'énoncé pourrait être 
    
        Écrire une méthode qui retourne un IntegerTriplet dont les paramètres a 
        b et c sont saisis au clavier.
    
        La particularité ici est l'utilisation de l'entrée standard.
    
        
     */
    @ToDoIn("1.11 IntegerTripletReader")
    @ToCompare(
            value = "Revoir la méthode read()",
            priority = 11,
            grade = 1,
            // Une méthode de génération de TU qui intègre l'entrée standard.
            testSetsMethodName = "edu.uha.miage.IntegerTripletTest.setTestsRead"/*,
            // Pour comparer 
            comparatorMethodName = "edu.uha.miage.IntegerTripletTest.readComparator"*/
    )
    public static IntegerTriplet read() {
        try ( Scanner in = new Scanner(System.in)) {

            System.out.print("Entrez a puis b puis c : ");

            return new IntegerTriplet(in.nextInt(), in.nextInt(), in.nextInt());
        }
    }

    /*
    
    @ToDoIn("1.08 Return a random double number that belongs to [min(a, b, c), max(a, b, c)]")
    @ToCompare(priority = 7, grade = 5,
            testSetsMethodName = "number.IntegerTripletTest.setTestsRandomPrint",
            comparatorMethodName = "number.IntegerTripletTest.randomComparator")
    public double random() {
        Random random = new Random();
        int min = min();
        int max = Math.max(Math.max(a, b), c);
        return min + random.nextInt(max - min) + random.nextDouble();
    }

    @ToDoIn("1.10 Read a b c and prints them")
    @ToCompare(priority = 9, grade = 5,
            testSetsMethodName = "number.IntegerTripletTest.setTestsInOut",
            checkStdOut = true)
    public static void inout() {
        Scanner in = new Scanner(System.in);
        do {
            System.out.print("Entrez trois entiers : ");
            IntegerTriplet it = new IntegerTriplet(in.nextInt(), in.nextInt(), in.nextInt());
            it.print();
            System.out.print("Encore ? (Oui ou Non) : ");
        } while ("OUI".equals(in.next().toUpperCase()));

        System.out.println("Fin");
    }*/
}
