/*
 * Creative commons CC BY-NC-SA 2020 Yvan Maillot <yvan.maillot@uha.fr>
 *
 *     Share - You can copy and redistribute the material in any medium or format
 * 
 *     Adapt - You can remix, transform, and build upon the material 
 * 
 * Under the following terms :
 * 
 *     Attribution - You must give appropriate credit, provide a link to the license, 
 *     and indicate if changes were made. You may do so in any reasonable manner, 
 *     but not in any way that suggests the licensor endorses you or your use. 
 * 
 *     NonCommercial — You may not use the material for commercial purposes. 
 * 
 *     ShareAlike — If you remix, transform, or build upon the material, 
 *     you must distribute your contributions under the same license as the original. 
 * 
 * Notices:    You do not have to comply with the license for elements of 
 *             the material in the public domain or where your use is permitted 
 *             by an applicable exception or limitation. 
 * 
 * No warranties are given. The license may not give you all of the permissions 
 * necessary for your intended use. For example, other rights such as publicity, 
 * privacy, or moral rights may limit how you use the material. 
 * 
 * See <https://creativecommons.org/licenses/by-nc-sa/4.0/>.
 */
package edu.uha.miage;

import caseine.publication.producers.ToCompareProducer;
import caseine.publication.producers.ToCompareProducer.InstanceAndParameters;
import static caseine.publication.producers.ToCompareProducer.InstanceAndParameters.newInputParamsForStaticMethod;
import static caseine.publication.producers.ToCompareProducer.InstanceAndParameters.newInstanceParams;
import static caseine.publication.producers.ToCompareProducer.InstanceAndParameters.newMessageInstanceParams;
import caseine.publication.producers.ToCompareProducer.ResultForComparison;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Yvan Maillot <yvan.maillot@uha.fr>
 */
public class IntegerTripletTest {

    private static Random R = new Random();

    /**
     * Produit une liste de jeux de tests pertinents pour évaluer la méthode
     * areSortedSafer()
     *
     * @return une liste de jeux de tests. Chacun d'eux encapsulé dans la classe
     * ToCompareProducer.InstanceAndParameters
     *
     * Cette classe possède des fabriques (method factory) qui facilitent leur
     * construction
     */
    public static List<ToCompareProducer.InstanceAndParameters> setTestsAreSorted() {
        List<ToCompareProducer.InstanceAndParameters> setTest = new ArrayList<>(103);
        IntegerTriplet it = new IntegerTriplet(1, 2, 3);

        // Quelques jeux de tests en "dur". Les 3 premiers réussissent. Le 4ème échoue.
        setTest.add(ToCompareProducer.InstanceAndParameters.newMessageInstanceParams(
                "[1, 1, 1, 1, 1, 1] sont triés pourtant", new IntegerTriplet(1, 1, 1), 1, 1, 1));
        setTest.add(ToCompareProducer.InstanceAndParameters.newMessageInstanceParams(
                "[1, 2, 3, 4, 5, 6] sont triés pourtant", it, 4, 5, 6));
        setTest.add(ToCompareProducer.InstanceAndParameters.newMessageInstanceParams(
                "[1, 2, 3, 3, 4, 5] sont triés pourtant", it, 3, 4, 5));

        // Des jeux de tests aléatoires qui réussissent.
        // Il suffit de trier. Le message est explicite.
        for (int i = 0; i < 25; ++i) {
            int[] a = {R.nextInt(), R.nextInt(), R.nextInt(), R.nextInt(), R.nextInt(), R.nextInt()};
            Arrays.sort(a);
            setTest.add(ToCompareProducer.InstanceAndParameters.newMessageInstanceParams(
                    String.format("[%d, %d, %d, %d, %d, %d] sont triés pourtant", a[0], a[1], a[2], a[3], a[4], a[5]),
                    new IntegerTriplet(a[0], a[1], a[2]),
                    a[3],
                    a[4],
                    a[5]));
        }
        // Des jeux de tests aléatoires qui échouent.
        for (int i = 0; i < 70; ++i) {
            int[] a = {R.nextInt(), R.nextInt(), R.nextInt(), R.nextInt(), R.nextInt(), R.nextInt()};
            // Normalement ça ne devrait pas être trié. Mais pour plus de sûreté...
            while (a[0] <= a[1] && a[1] <= a[2] && a[2] <= a[3] && a[3] <= a[4] && a[4] <= a[5]) {
                for (int j = 0; j < a.length; ++j) {
                    a[j] = R.nextInt();
                }
            }
            setTest.add(ToCompareProducer.InstanceAndParameters.newMessageInstanceParams(
                    String.format("[%d, %d, %d, %d, %d, %d] ne devrait pas être triés", a[0], a[1], a[2], a[3], a[4], a[5]),
                    new IntegerTriplet(a[0], a[1], a[2]),
                    a[3],
                    a[4],
                    a[5]));
        }
        // D'autres jeux de tests aléatoires qui échouent.
        for (int i = 1; i < 6; ++i) {
            int[] a = {
                R.nextInt(50),
                50 + R.nextInt(50),
                100 + R.nextInt(50),
                150 + R.nextInt(50),
                200 + R.nextInt(50),
                250 + R.nextInt(50)
            };
            swap(a, i, i - 1);

            setTest.add(ToCompareProducer.InstanceAndParameters.newMessageInstanceParams(
                    String.format("[%d, %d, %d, %d, %d, %d] ne devrait pas être triés", a[0], a[1], a[2], a[3], a[4], a[5]),
                    new IntegerTriplet(a[0], a[1], a[2]),
                    a[3],
                    a[4],
                    a[5]));
        }
        return setTest;
    }

    /**
     * Produit une liste de jeux de tests pour évaluer la méthode add()
     *
     * C'est très simple.Il suffit de générer quelques instances de
     * IntergetTriplet.
     *
     * Dans cet exemple, aucun message n'est choisi. C'est le message standard
     * qui sera employé pour alerter l'étudiant en cas d'échec.
     *
     * @return la liste des jeux de tests
     */
    public static List<ToCompareProducer.InstanceAndParameters> setTestsAdd() {
        List<ToCompareProducer.InstanceAndParameters> setTest = new ArrayList<>(20);

        for (int i = 0; i < 20; ++i) {
            IntegerTriplet a = new IntegerTriplet(R.nextInt(), R.nextInt(), R.nextInt());
            IntegerTriplet b = new IntegerTriplet(R.nextInt(), R.nextInt(), R.nextInt());
            // a est l'instance et b le paramètre
            setTest.add(ToCompareProducer.InstanceAndParameters.newInstanceParams(a, b));
        }

        return setTest;
    }

    /**
     * Produit une liste de jeux de tests pour évaluer plusieurs méthodes qui
     * nécessitent tout simplement de générer 3 entiers
     *
     * 1. averageSafer()
     *
     * 2. printABC()
     *
     * C'est très simple.
     *
     * Dans cet exemple, aucun message n'est choisi. C'est le message standard
     * qui sera employé pour alerter l'étudiant en cas d'échec.
     *
     * @return la liste des jeux de tests
     */
    public static List<ToCompareProducer.InstanceAndParameters> setTestsAleaABC() {
        List<ToCompareProducer.InstanceAndParameters> setTest = new ArrayList<>(100);
        for (int i = 0; i < 100; ++i) {
            // Je choisis volontairement des entiers dans [0, 20]. Pourquoi pas.
            IntegerTriplet a = new IntegerTriplet(R.nextInt(21), R.nextInt(21), R.nextInt(21));
            // a est l'instance. Il n'y a pas de paramètre.
            setTest.add(ToCompareProducer.InstanceAndParameters.newInstanceParams(a));
        }
        return setTest;
    }

    /**
     * Cette méthode est utilisée pour comparer le résultat de averageSafer() de
     * l'étudiant avec celui de référence.Elle doit retourner "OK" si la
     * comparaison est satisfaisante ou autre chose en cas d'échec.
     *
     * Dans ce cas, ce peut être un message à l'étudiant.
     *
     * Cette méthode reçoit un objet de type
     * ToCompareProducer.ResultForComparison qui fournit de nombreuses
     * informations pour réaliser cette comparaison.
     *
     * getMessage() : l'éventuel message du jeu de tests
     *
     * getInst() : l'éventuelle instance sur laquelle s'applique la méthode
     * testée
     *
     * getParams() : les éventuels paramètres de la méthode testée
     *
     * getInput() : l'éventuel flux d'entrée qui a servi à l'exécution de la
     * méthode testée.
     *
     * getRefResult() : l'éventuel retour de la méthode de référence
     *
     * getStudentResult() : l'éventuel retour de la méthode de l'étudiant
     *
     * getRefStdOutput() : l'éventuelle sortie d'affichage de la méthode de
     * référence
     *
     * getStudentStdOutput() : l'éventuelle sortie d'affichage de la méthode de
     * l'étudiant
     *
     * getRefThrown() : l'éventuelle exception lancée par la méthode de
     * référence
     *
     * getStudentThrown() : l'éventuelle exception lancée par la méthode de
     * l'étudiant
     *
     *
     * @param rfc un objet qui contient de nombreuses données pour comparer les
     * résultats.
     * @return "OK", en cas de succès, ou un message à l'étudiant, en cas
     * d'échec.
     */
    public static String averageComparator(ToCompareProducer.ResultForComparison rfc) {
        // Notre cas demande simplement de comparer le résultat de l'étudiant 
        // avec celui de la référence. Mais comme ce sont des doubles, nous le 
        // faisons à une approximation près.

        // Si les valeurs sont suffisamment proches, on retour "OK".
        // Sinon je décide de retourner un message précis pour aider l'étudiant.
        double stdResult = (double) rfc.getStudentResult();
        double refResult = (double) rfc.getRefResult();
        IntegerTriplet it = (IntegerTriplet) rfc.getInst();
        final double EPSILON = 1e-6;

        if (Math.abs(stdResult - refResult) < EPSILON) {
            return "OK";
        } else {
            String result = String.format("average of %d %d %d\n"
                    + "expected : \n%f\n"
                    + "bus was : \n%f", it.getA(), it.getB(), it.getC(), refResult, stdResult);
            return result;
        }

    }
    
    /**
     * Méthode à écrire par l'enseignant qui sera appelée lors de l'évaluation
     * de read().
     * 
     * Le paramètre ResultForComparison contient le nécessaire pour comparer
     * le travail de l'étudiant avec celui de la référence.
     * 
     * La méthode doit retourner "OK" en cas d'égalité, ou autre chose, comme un
     * message d'aide, en cas d'échec.
     * d'échec.
     * 
     * @param rfc
     * @return OK si succès ou autre chose sinon
     
    public static String printComparator(ResultForComparison rfc) {
        // Récupération de l'étudiant
        IntegerTriplet stdResult = (IntegerTriplet) rfc.getStudentResult();
        IntegerTriplet refResult = (IntegerTriplet) rfc.getRefResult();
        

        
        // Pour commencer, plus de souplesse avec les séparateurs, on laisse
        // à l'étudiant de choisir la nature et la quantité des séparateurs.
        
        stdResult = stdResult.replaceAll("\\s+", " ");
        
        // Et puis pour être vraiment, cherchons tout simplement trois entiers
        // quelque part noyés dans stdResult
        
        Pattern pattern = Pattern.compile("[^0-9-]*([0-9-]+)[^0-9-]+([0-9-]+)[^0-9-]+([0-9-]+)[^0-9-]*");
        
        Matcher matcher = pattern.matcher(stdResult);
        
        if (matcher.matches()) {
            int stdA = Integer.parseInt(matcher.group(1));
            int stdB = Integer.parseInt(matcher.group(2));
            int stdC = Integer.parseInt(matcher.group(3));
            
            if (stdA == a && stdB == b && stdC == c) {
                return "OK";
            } else {
                return "On attendait quelque chose comme : \n"
                        + rfc.getRefStdOutput()
                        + "Mais on a eu : \n"
                        + rfc.getStudentStdOutput();
            }
            
        } else {
            return "Il n'y a pas trois entiers affichés à l'écran :\n" + rfc.getStudentStdOutput();
        }
        
        // De cette façon, l'étudiant peut proposer toutes sortes d'affichage
        // Par exemple (bien sûr) 
        //    System.out.println(a + " " + b + " " + c);
        // Par exemple aussi
        //    System.out.println(a + "   " + b + " " + c);
        // et aussi 
        //    System.out.print(a + " " + b + " " + c);
        // et aussi 
        //    System.out.print(a + "\t" + b + " " + c);
        // et même 
        //    System.out.println("a = " + a);
        //    System.out.println("b = " + b);
        //    System.out.println("c = " + c);
        
        // Bon, il y a des limites, par exemple :
        //    System.out.println("Attribut N°1 " + a);
        //    System.out.println("Attribut N°2 " + b);
        //    System.out.println("Attribut N°3 " + c);
        // ne marchera pas...
        
    }*/

    /**
     * Produit une liste de jeux de tests pour évaluer read()
     * 
     * Il faut produire des flux de trois entiers
     *
     * @return la liste des jeux de tests
     */
    public static List<ToCompareProducer.InstanceAndParameters> setTestsRead() {
        List<ToCompareProducer.InstanceAndParameters> setTest = new ArrayList<>(100);
        for (int i = 0; i < 100; ++i) {
            setTest.add(InstanceAndParameters.newInputParamsForStaticMethod(
                    (R.nextInt(101)-2)+ " " + (R.nextInt(101)-2) + " " + (R.nextInt(101)-2)));
        }
        return setTest;
    }
    
    /**
     * Méthode à écrire par l'enseignant qui sera appelée lors de l'évaluation
     * de softerPrint().
     * 
     * Le paramètre ResultForComparison contient le nécessaire pour comparer
     * le travail de l'étudiant avec celui de la référence.
     * 
     * La méthode doit retourner "OK" en cas d'égalité, ou autre chose, comme un
     * message d'aide, en cas d'échec.
     * d'échec.
     * 
     * @param rfc
     * @return OK si succès ou autre chose sinon
     */
    public static String printComparator(ResultForComparison rfc) {
        // Récupération de la sortie standard de l'étudiant
        String stdResult = rfc.getStudentStdOutput();
        
        // Récupération de l'instance qui a servi à l'affichage
        IntegerTriplet it = (IntegerTriplet) rfc.getInst();
        int a = it.getA();
        int b = it.getB();
        int c = it.getC();
        
        // Pour commencer, plus de souplesse avec les séparateurs, on laisse
        // à l'étudiant de choisir la nature et la quantité des séparateurs.
        
        stdResult = stdResult.replaceAll("\\s+", " ");
        
        // Et puis pour être vraiment, cherchons tout simplement trois entiers
        // quelque part noyés dans stdResult
        
        Pattern pattern = Pattern.compile("[^0-9-]*([0-9-]+)[^0-9-]+([0-9-]+)[^0-9-]+([0-9-]+)[^0-9-]*");
        
        Matcher matcher = pattern.matcher(stdResult);
        
        if (matcher.matches()) {
            int stdA = Integer.parseInt(matcher.group(1));
            int stdB = Integer.parseInt(matcher.group(2));
            int stdC = Integer.parseInt(matcher.group(3));
            
            if (stdA == a && stdB == b && stdC == c) {
                return "OK";
            } else {
                return "On attendait quelque chose comme : \n"
                        + rfc.getRefStdOutput()
                        + "Mais on a eu : \n"
                        + rfc.getStudentStdOutput();
            }
            
        } else {
            return "Il n'y a pas trois entiers affichés à l'écran :\n" + rfc.getStudentStdOutput();
        }
        
        // De cette façon, l'étudiant peut proposer toutes sortes d'affichage
        // Par exemple (bien sûr) 
        //    System.out.println(a + " " + b + " " + c);
        // Par exemple aussi
        //    System.out.println(a + "   " + b + " " + c);
        // et aussi 
        //    System.out.print(a + " " + b + " " + c);
        // et aussi 
        //    System.out.print(a + "\t" + b + " " + c);
        // et même 
        //    System.out.println("a = " + a);
        //    System.out.println("b = " + b);
        //    System.out.println("c = " + c);
        
        // Bon, il y a des limites, par exemple :
        //    System.out.println("Attribut N°1 " + a);
        //    System.out.println("Attribut N°2 " + b);
        //    System.out.println("Attribut N°3 " + c);
        // ne marchera pas...
        
    }

    /*

    public static ToCompareProducer.InstanceAndParameters[] setTestsAdd() {
        IntegerTriplet it1 = new IntegerTriplet(1, 2, 3);
        IntegerTriplet it2 = new IntegerTriplet(4, 7, -5);
        ToCompareProducer.InstanceAndParameters[] testSets = {
            newMessageInstanceParams("Revoir add", it2, it1)
        };
        return testSets;
    }


    public static List<ToCompareProducer.InstanceAndParameters> setTestsInOut() {
        List<ToCompareProducer.InstanceAndParameters> setTestInOut = new LinkedList<>();
        /*setTestInOut.add(new InstanceAndParameters(
                "1 2 3 Oui 4 5 6 Non",
                "1 2 3 Oui 4 5 6 Non\n",
                (Object) null));*//*
        setTestInOut.add(newInputParamsForStaticMethod("1 2 3 Oui 4 5 6 Non"));
        return setTestInOut;
    }

    public static List<ToCompareProducer.InstanceAndParameters> setTestsAreSorted() {
        List<ToCompareProducer.InstanceAndParameters> setTestAreSorted = new LinkedList<>();
        IntegerTriplet it = new IntegerTriplet(1, 2, 3);

        setTestAreSorted.add(ToCompareProducer.InstanceAndParameters.newMessageInstanceParams(
                "[1, 1, 1, 1, 1, 1] sont triés pourtant", new IntegerTriplet(1, 1, 1), 1, 1, 1));
        setTestAreSorted.add(ToCompareProducer.InstanceAndParameters.newMessageInstanceParams(
                "[1, 2, 3, 4, 5, 6] sont triés pourtant", it, 4, 5, 6));
        setTestAreSorted.add(ToCompareProducer.InstanceAndParameters.newMessageInstanceParams(
                "[1, 2, 3, 3, 4, 5] sont triés pourtant", it, 3, 4, 5));
        setTestAreSorted.add(ToCompareProducer.InstanceAndParameters.newMessageInstanceParams(
                "[1, 2, 3, 7, 5, 9] ne sont pas triés pourtant", it, 7, 6, 8));

        for (int i = 0; i < 100; ++i) {
            int[] a = {R.nextInt(), R.nextInt(), R.nextInt(), R.nextInt(), R.nextInt(), R.nextInt()};
            Arrays.sort(a);
            setTestAreSorted.add(ToCompareProducer.InstanceAndParameters.newMessageInstanceParams(
                    String.format("[%d, %d, %d, %d, %d, %d] sont triés pourtant", a[0], a[1], a[2], a[3], a[4], a[5]),
                    new IntegerTriplet(a[0], a[1], a[2]),
                    a[3],
                    a[4],
                    a[5]));
        }

        //for (int i = 0; i < 100; ++i) {
        int[] a = {R.nextInt(), R.nextInt(), R.nextInt(), R.nextInt(), R.nextInt(), R.nextInt()};

        setTestAreSorted.add(ToCompareProducer.InstanceAndParameters.newMessageInstanceParams(
                String.format("[%d, %d, %d, %d, %d, %d] appelez votre enseignant, vous avez gagné le Jackpot", a[0], a[1], a[2], a[3], a[4], a[5]),
                new IntegerTriplet(a[0], a[1], a[2]),
                a[3],
                a[4],
                a[5]));
        //}
        return setTestAreSorted;
    }

    public static List<ToCompareProducer.InstanceAndParameters> setTestsRandomPrint() {

        List<ToCompareProducer.InstanceAndParameters> setTests = new LinkedList<>();

        IntegerTriplet it;
        for (int i = 0; i < 10; ++i) {
            it = new IntegerTriplet(R.nextInt(100), R.nextInt(100), R.nextInt(100));
            setTests.add(newInstanceParams(it));
        }
        return setTests;
    }

    private static double lastA = Double.NEGATIVE_INFINITY;
    private static double lastB = Double.NEGATIVE_INFINITY;

    public static String randomComparator(ToCompareProducer.ResultForComparison rfc) {
        IntegerTriplet it = (IntegerTriplet) rfc.getInst();
        int min = Math.min(it.a, Math.min(it.b, it.c));
        int max = Math.max(it.a, Math.max(it.b, it.c));
        double refResult = (double) rfc.getRefResult();
        double studentResult = (double) rfc.getStudentResult();
        boolean changeOfA = refResult != lastA;
        boolean changeOfB = studentResult != lastB;
        lastA = refResult;
        lastB = studentResult;
        return min <= refResult && refResult <= max && min <= studentResult && studentResult <= max && changeOfA && changeOfB?"OK":"KO";
    }

    /*
     * public static String itsName(Object inst, Object[] params, Object oref,
     * Object ototest, String stdOutRef, String stdOutToTest) où
     * <ul>
     * <li>inst est l'instance sur laquelle s'applique la méthode (ou null si
     * static)</li>
     * <li>params contient les paramètres envoyés à la méthode.</li>
     * <li>oref est le résultat de la méthode de référence</li>
     * <li>ototest est le résultat de la méthode à tester</li>
     * <li>stdOutRef est la sortie standard de la méthode de référence</li>
     * <li>stdOutToTest est la sortie standard de la méthode à tester</li>
     * <li>Le retour est "OK" si le résultat de l'étudiant est satisfaisant,
     * sinon il contient un message qui sera affiché à l'étudiant en cas
     * d'échec.</li>
     * </ul>
     *//*
    public static String testPrint(ToCompareProducer.ResultForComparison rfc) {
        IntegerTriplet inst = (IntegerTriplet) rfc.getInst();
        String stdOutRef = rfc.getRefStdOutput();
        String stdOutToTest = rfc.getStudentStdOutput();

        String regex = String.format("%d.+%d.+%d", inst.a, inst.b, inst.c);
        Pattern pattern = Pattern.compile(regex);
        return (pattern.matcher(stdOutRef).find() && pattern.matcher(stdOutToTest).find())?"OK":String.format("KO => found %s but expected %s", stdOutToTest, stdOutRef);
    }*/

    private static void swap(int[] a, int i, int j) {
        int tmp = a[i];
        a[i] = a[j];
        a[j] = tmp;
    }
}
