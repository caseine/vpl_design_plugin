/*
 * @author : Yvan Maillot (yvan.maillot@uha.fr)
 */
package cam;

import caseine.tags.ToCheck;
import caseine.tags.ToDo;

@ToDo("Écrire une classe autonome")
public class ClasseAutonome {
    @ToCheck("votre classe. Elle n'est pas autonome")
    public static void main(String[] args) {
        
    }
}
