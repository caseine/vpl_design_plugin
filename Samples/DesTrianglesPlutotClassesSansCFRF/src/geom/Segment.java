/*
 * @author : Yvan Maillot (yvan.maillot@uha.fr)
 */
package geom;

import caseine.tags.ToDo;

public class Segment {
    @caseine.tags.ToCheck(priority = 20)
    @caseine.tags.GetterToCheck(priority = 26)
    @caseine.tags.SetterToCheck(priority = 27)
    @ToDo("1. Déclarer les attributs p1 et p2, les extrémités de ce segment")
    private Point p1, p2;

    @caseine.tags.ToCheck(priority = 21)
    @ToDo("2. Écrire le constructeur attendant les extrémités de ce segment")
    public Segment(Point p1, Point p2) {
        this.p1 = p1;
        this.p2 = p2;
    }

    @ToDo("3. Écrire un getter pour chaque extrémité")
    @caseine.tags.ToCheck(priority = 22)
    public Point getP1() {
        return p1;
    }

    @ToDo
    @caseine.tags.ToCheck(priority = 23)
    public void setP1(Point p1) {
        this.p1 = p1;
    }

    @ToDo("4. Écrire un setter pour chaque extrémité")
    @caseine.tags.ToCheck(priority = 22)
    public Point getP2() {
        return p2;
    }

    @ToDo
    @caseine.tags.ToCheck(priority = 23)
    public void setP2(Point p2) {
        this.p2 = p2;
    }
    
    @ToDo("5. Écrire getLongueur() qui retourne la longueur de ce segment")
    @caseine.tags.ToCheck(priority = 24)
    @caseine.tags.ToCompare(priority = 28)
    public double getLongueur() {
        return Math.sqrt((p2.getX()-p1.getX())*(p2.getX()-p1.getX())+(p2.getY()-p1.getY())*(p2.getY()-p1.getY()));
    }
    
    @caseine.tags.ToCheck(priority = 25)
    @caseine.tags.ToCompare(priority = 28)
    @Override
    @ToDo("6. Redéfinir toString() la comme demandé dans l'énoncé, Par exemple [(1.2, 3.4) ; (4.5, 5.6)]")
    public String toString() {
        return "["+p1+" ; "+p2+"]";
    }
}
