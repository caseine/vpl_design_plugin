/*
 * @author : Yvan Maillot (yvan.maillot@uha.fr)
 */
package geom;

import caseine.tags.*;

public class Point {

    @ToDo("1. Déclarer x et y, les coordonnées cartésiennes de ce point")
    @ToCheck(value="x ou y : déclaration incorrecte ou manquante", priority = 1)
    @caseine.tags.GetterToCheck(priority = 6)
    @caseine.tags.SetterToCheck(priority = 7)

    private double x, y;

    @caseine.tags.ToCheck(priority = 2)
    @caseine.tags.ToDo("2. Écrire le constructeur aux coordonnées cartésiennes")
    public Point(double x, double y) {
        this.x = x;
        this.y = y;
    }

    @caseine.tags.ToCheck(priority = 3)
    @caseine.tags.ToDo("3. Écrire un getter pour x")
    public double getX() {
        return x;
    }

    @caseine.tags.ToDo("5. Écrire un setter pour x")
    @caseine.tags.ToCheck(priority = 4)
    public void setX(double x) {
        this.x = x;
    }

    @caseine.tags.ToDo("4. Écrire un getter pour y")
    @caseine.tags.ToCheck(priority = 3)
    public double getY() {
        return y;
    }

    @caseine.tags.ToDo("6. Écrire un setter pour y")
    @caseine.tags.ToCheck(priority = 4)
    public void setY(double y) {
        this.y = y;
    }

    @caseine.tags.ToDoIn(value = "7. public toString() qui retourne par exemple (2.5, 3.2)")
    @Override
    @caseine.tags.ToCheck(priority = 5)
    @caseine.tags.ToCompare(priority = 10)
    public String toString() {
        return "(" + x + ", " + y + ")";
    }
}
