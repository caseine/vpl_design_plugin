/*
 * @author : Yvan Maillot (yvan.maillot@uha.fr)
 */
package geom;

import caseine.tags.ToDo;
import caseine.tags.ToDoIn;

public class Triangle {

    @caseine.tags.ToCheck(priority = 30)
    @ToDo("1. Les trois segments du triangle : c1, c2, c3")
    private Segment c1, c2, c3;

    @caseine.tags.ToCheck(priority = 31)
    @ToDo("2. Écrire le constructeur avec ses trois sommets")
    public Triangle(Point p1, Point p2, Point p3) {
        this.c1 = new Segment(p1, p2);
        this.c2 = new Segment(p2, p3);
        this.c3 = new Segment(p3, p1);
    }

    @caseine.tags.ToCheck(priority = 32)
    @ToDo("3. getP1() qui retourne le premier sommet de ce triangle")
    public Point getP1() {
        return c1.getP1();
    }

    @caseine.tags.ToCheck(priority = 33)
    @ToDo("6. setP1() qui met à jour le premier sommet de ce triangle")
    public void setP1(Point p1) {
        this.c1 = new Segment(p1, c2.getP1());
        this.c3 = new Segment(c3.getP1(), p1);
    }

    @ToDo("4. getP2() qui retourne le deuxième sommet de ce triangle")
    @caseine.tags.ToCheck(priority = 32)
    public Point getP2() {
        return c2.getP1();
    }

    @ToDo("7. setP2() qui met à jour le deuxième sommet de ce triangle")
    @caseine.tags.ToCheck(priority = 33)
    public void setP2(Point p2) {
        this.c1 = new Segment(c1.getP1(), p2);
        this.c2 = new Segment(p2, c3.getP1());
    }

    @ToDo("5. getP3() qui retourne le troisième sommet de ce triangle")
    @caseine.tags.ToCheck(priority = 32)
    public Point getP3() {
        return c3.getP1();
    }

    @ToDo("8. setP3() qui met à jour le troisième sommet de ce triangle")
    @caseine.tags.ToCheck(priority = 33)
    public void setP3(Point p3) {
        this.c2 = new Segment(c2.getP1(), p3);
        this.c3 = new Segment(p3, c1.getP1());
    }

    @ToDoIn("5. Retourner le périmètre de ce triangle")
    @caseine.tags.ToCheck(priority = 34)
    @caseine.tags.ToCompare(priority = 37)
    public double getPerimetre() {
        return c1.getLongueur() + c2.getLongueur() + c3.getLongueur();
    }

    @ToDoIn("6. Retourner le barycentre de ce triangle")
    @caseine.tags.ToCheck(priority = 34)
    @caseine.tags.ToCompare(priority = 37)
    public Point getBaryCentre() {
        Point a = c1.getP1();
        Point b = c2.getP1();
        Point c = c3.getP1();

        return new Point((a.getX() + b.getX() + c.getX()) / 3, (a.getY() + b.getY() + c.getY()) / 3);
    }

    @caseine.tags.ToCheck(priority = 34)
    @ToDoIn("7. Retourner la surface de ce triangle")
    @caseine.tags.ToCompare(priority = 37)
    public double getSurface() {
        double a = c1.getLongueur();
        double b = c2.getLongueur();
        double c = c3.getLongueur();
        double p = (a + b + c) / 2;
        return Math.sqrt(p * (p - a) * (p - b) * (p - c));
    }

    @Override
    @caseine.tags.ToCheck(priority = 34)
    @ToDoIn("La méthode toString()")
    @caseine.tags.ToCompare(priority = 38)
    public String toString() {
        return "<" + c1 + ", " + c2 + ", " + c2 + ">";
    }
}
