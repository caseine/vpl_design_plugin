/*
 * @author : Yvan Maillot (yvan.maillot@uha.fr)
 */
package main;

import caseine.tags.ToDoIn;
import java.io.IOException;
import univ.Etudiant;
import univ.Personne;
import univ.Prof;


public class Main {

    public static void main(String[] args) throws IOException {
        Personne lucky = new Personne("Luke", "Lucky", true);
        Personne ma = new Personne("Dalton", "Ma", false);

        System.out.println("\n--- Quelque part dans le farwest ---- \n");

        lucky.ditBonjourA(ma); // Ce simple salut doit entraîner la discussion suivante :
        /*
        " Bonjour, je suis M. Lucky Luke (26 ans) " dit Luke Lucky. 
        " Bonjour M. Lucky Luke (26 ans).
           Moi c'est Mme Dalton Ma (98 ans) " répond Ma Dalton.
        " Comment allez-vous ? " continue Ma Dalton.
        " Ca va bien. Merci " termine Luke Lucky.
         */
        
        /*System.out.println("\n--- Pendant ce temps là, à l'est ---- \n");

        Etudiant skell = new Etudiant("Maillot", "Simon", true, "Skell", "M2 MIAGE");
        Etudiant bisou = new Etudiant("Girardot", "Céline", false, "Cel", "M1 MIAGE");
        skell.ditBonjourA(bisou);

        System.out.println("\n--- À quelques pas de là ---- \n");

        Prof bruno = new Prof("Adam", "Bruno", true, "les bases de données");
        Prof yvan = new Prof("Maillot", "Yvan", true, "Java");
        yvan.ditBonjourA(bruno);

        System.out.println("\n--- Plus tard ---- \n");

        yvan.ditBonjourA(skell);

        System.out.println("\n--- Au même moment ---- \n");
            
        skell.ditBonjourA(bruno);*/

    }
}
