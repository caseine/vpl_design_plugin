/*
 * @author : Yvan Maillot (yvan.maillot@uha.fr)
 */
package univ;

import caseine.tags.ToCheck;
import caseine.tags.ToDo;
import caseine.tags.ToDoIn;

/**
 * Un étudiant est une personne. Mais il est affublé d'un surnom (par exemple
 * poussin) et suit une année de formation (par exemple L3 MIAGE). 
 * 
 * Il faudra donc rajouter, surnom et annee, deux attributs de type String
 * (en respectant scrupuleusement l'orthographe et la casse).
 * 
 * Il faudra créer un constructeur qui initialise un étudiant comme une personne
 * avec en plus, surnom et annee.
 * 
 * Il faudra créer les getters pour surnom (getSurnom()) et annee (getAnnee())
 *
 * @author yvan
 */
// Il serait intéressant d'ajouter un tag qui supprime extends Personne
public class Etudiant extends Personne {
    @ToCheck(value = "Revoir les attributs", priority = 9)
    // @GetterToCheck (Marche pas bien sur String)
    @ToDo("Les attributs surnon et annee")
    private final String surnom, annee; // Exemple "L3 MIAGE"
    
    @ToCheck(value = "Revoir le constructeur", priority = 10)
    @ToDo("Le constructeur comme une personne mais avec un surnom et l'année en plus")
    public Etudiant(String nom, String prenom, boolean homme, String surnom, String annee) {
        super(nom, prenom, homme);
        this.annee = annee;
        this.surnom = surnom;
    }

    @ToDo("Les getters")
    @ToCheck(value = "Revoir getSurnom()", priority = 11)
    public String getSurnom() {
        return surnom;
    }

    @ToDo
    @ToCheck(value = "Revoir getAnnee()", priority = 12)
    public String getAnnee() {
        return annee;
    }

    @Override
    @ToDoIn("Redéfinition de presentation()")
    protected String presentation(boolean courte) {
       if (courte) 
           return surnom;
       else 
           return getPrenom() + " " + getNom() + ", étudiant en " + getAnnee();
    } 

    @Override
    @ToDoIn("Redéfinition de bonjour() pour un étudiant")
    protected String bonjour() {
        return "Salut, Gros";
    }

    @Override
    @ToCheck
    @ToDoIn("Redéfinition de reponseAuBonjourDe() pour un étudiant")
    protected String reponseAuBonjourDe(Personne personne) {
        return "Salut, Gros";
    }

    @ToCheck
    @ToDoIn("Redéfinition de etVous() pour un étudiant")
    @Override
    protected String etVous(Personne personne) {
        return "Wesh, Gros";
    }

    @Override
    @ToCheck
    @ToDoIn("Redéfinition de caVA() pour un étudiant")
    protected String caVa(Personne personne) {
        return "Bien ou quoi, Gros ?";
    }
}
