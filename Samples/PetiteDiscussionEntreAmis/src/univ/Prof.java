/*
 * @author : Yvan Maillot (yvan.maillot@uha.fr)
 */
package univ;

import caseine.tags.ToCheck;
import caseine.tags.ToDo;


/**
 * Un prof est une personne. Mais, en plus, il enseigne (par exemple
 * le Java). 
 * 
 * Il faudra donc rajouter, cours, un attribut de type String
 * (en respectant scrupuleusement l'orthographe et la casse).
 * 
 * Il faudra créer un constructeur qui initialise un prof comme une personne
 * avec en plus, cours.
 * 
 * Il faudra créer le getters pour cours (getCours())
 *
 * @author yvan
 */
public class Prof extends Personne {
    @ToDo("Un prof enseigne un cours (prévoir un attribut à cet effet)")
    @ToCheck(value = "l'attribut cours", priority = 5)
    // @GetterToCheck (Marche pas bien sur String)
    private final String cours;
    
    @ToDo("Constructeur comme une personne normale avec le cours en plus")
    @ToCheck(value = "le constructeur", priority = 6)
    public Prof(String nom, String prenom, boolean homme, String cours) {
        super(nom, prenom, homme);
        this.cours = cours;
    }

    @ToDo("Les getters")
    @ToCheck(value = "getPrenom()", priority = 7)
    @Override
    public String getPrenom() {
        return super.getPrenom();
    }

    @ToDo
    @ToCheck(value = "getNom()", priority = 7)
    @Override
    public String getNom() {
        return super.getNom().toUpperCase();
    }

    @Override
    @ToDo("Redéfinir etVous() pour un prof")
    @ToCheck(value = "eVous()", priority = 8)
    protected String etVous(Personne personne) {
        return "Ça roule, Raoul.";
    }

    @Override
    @ToDo("Redéfinir caVA() pour un prof")
    @ToCheck(value = "caVa()", priority = 8)
    protected String caVa(Personne personne) {
        return "Ça get'z, " + personne.getPrenom();
    }

    @Override
    @ToDo("Redéfinir reponseAuBonjourDe() pour un prof")
    @ToCheck(value = "reponseAuBonjourDe()", priority = 8)
    protected String reponseAuBonjourDe(Personne personne) {
        return "Salut " + personne.getPrenom() + ", moi c'est " + 
                presentation(true) + " et j'enseigne " + cours;
    }

    @Override
    @ToDo("Redéfinir bonjour() pour un prof")
    @ToCheck(value = "bonjour()", priority = 8)
    protected String bonjour() {
        return "Hello, je suis " + presentation(true) + ". J'enseigne " + cours;
    }

    @Override
    @ToDo("Redéfinir presentation() pour un prof")
    @ToCheck(value = "presentation()", priority = 8)
    protected String presentation(boolean courte) {
        if (courte) {
            return super.presentation(courte);
        } else {
            return super.presentation(courte) + ". J'enseigne " + cours + ".";
        }
    }
}
