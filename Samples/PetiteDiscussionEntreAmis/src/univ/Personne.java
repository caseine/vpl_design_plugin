/*
 * @author : Yvan Maillot (yvan.maillot@uha.fr)
 */
package univ;

import caseine.tags.*;

/**
 * Une personne a un nom, un prénom et un genre (homme ou femme). Il faudra 
 * déclarer des attributs pour ça, nom et prenom de type String et homme, de 
 * type boolean (en respectant exactement cette orthographe).
 * 
 * Il faudra écrire le constructeur qui initialise ces attributs.
 * 
 * Il faudra aussi prévoir des getters pour ces attributs
 *  - getNom()
 *  - getPrenom()
 *  - isHomme()
 * 
 * Une personne aime bien se présenter. Elle le fait de deux manières, soit
 * courte (ou familière), sous longue (ou soutenue).Pour ça, il faudra écrire le 
 * corps de la méthode 
 * protected String presentation(boolean courte).
 * 
 * Une personne est polie. Quand elle rencontre une autre personne, elle dit 
 * bonjour et se présente. Il s'en suit un petit dialogue, du style :
 * 
 * " Bonjour, je suis M. Luke Lucky " dit Lucky Luke. 
 * " Bonjour Lucky Luke.
 *    Moi c'est Mme Dalton Ma " répond Ma Dalton.
 * " Comment allez-vous ? " continue Ma Dalton.
 * " Ca va bien. Merci " termine Lucky Luke.
 * 
 * Ce dialogue est établi à l'aide des quatre méthodes, mises à votre disposition, 
 * suivantes :
 *       1. public final void ditBonjourA(Personne personne)
 *       2. private void repondAuBonjourDe(Personne personne)
 *       3. private void repondAuBonjourDe(Personne personne)
 *       4. private void demandeSiCaVaA(Personne personne)
 * 
 * ELLES N'ONT PAS À ÊTRE MODIFIÉES EN QUOIQUE CE SOIT.
 * 
 * Les 4 précédentes méthodes invoquent 4 autres méthodes, également mises à 
 * votre disposition, qui retournent des chaînes de caractères.
 *
 *    1. protected String bonjour() qui retourne
 *              "Bonjour, je suis " suivi de sa présentation longue.
 * 
 *    2. protected String reponseAuBonjourDe(Personne personne) qui retourne
 *              "Bonjour " + personne.presentation(true) + ".\n   Moi c'est " + presentation();
 * 
 *    3. protected String caVa(Personne personne) qui retourne "Comment allez-vous ?";
 * 
 *    4. protected String etVous(Personne personne) qui retourne "Ca va bien. Merci"
 *  
 * VOUS POUVEZ MODIFIER LEUR CONTENU POUR VOUS AMUSER.
 * 
 * L'exécution de la classe autonome main.Main devrait engendrer le petit 
 * dialogue entre Lucky et Ma Dalton déjà évoqué. Ce dialogue peut varier en 
 * changeant le retour comme précédement expliqué.
 * 
 * D'autres dialogues sont pour le moment "en commentaires". Vous pourrez les
 * décommenter et les tester quand vous aurez réalisé l'exercice.
 * 
 * @author yvan
 */
public class Personne {
    
    /**
     * Affiche sur la sortie standard "" **B** " dit **P**. \n" où 
     *        1. **B** est le résultat de l'invocation de bonjour()
     *        2. **P** est le résultat de l'invocation de presentation(true)
     * Puis invoque personne.repondAuBonjourDe(this) pour continuer le dialogue
     * 
     * @param personne est la personne à qui s'adresse le bonjour.
     */
    public final void ditBonjourA(Personne personne) {
        System.out.printf("\" %s \" dit %s. \n", bonjour(), presentation(true));
        personne.repondAuBonjourDe(this);
    }

    /**
     * Affiche sur la sortie standard "" **R** " répond **P**. \n" où
     *        1. **R** est le résultat de l'invocation de reponseAuBonjourDe(personne)
     *        2. **P** est le résultat de l'invocation de presentation(true)
     * Puis invoque demandeSiCaVaA(personne);
     * 
     * @param personne est la personne à qui répondre
     */
    private void repondAuBonjourDe(Personne personne) {
        System.out.printf("\" %s \" répond %s.\n", reponseAuBonjourDe(personne), presentation(true));
        demandeSiCaVaA(personne);
    }
    
    /**
     * Affiche sur la sortie standard "" **C** " répond **P**. \n" où
     *        1. **C** est le résultat de l'invocation de caVa(personne)
     *        2. **P** est le résultat de l'invocation de presentation(true)
     * Puis invoque personne.demandeEtVousA(this);
     * 
     * @param personne est la personne à qui demander si ça va.
     */
    private void demandeSiCaVaA(Personne personne) {
        System.out.printf("\" %s \" continue %s.\n", caVa(personne), presentation(true));
        personne.demandeEtVousA(this);
    }

    /**
     * Affiche sur la sortie standard "" %s " termine %s.\n" où
     *        1. **V** est le résultat de l'invocation de etVous(personne)
     *        2. **P** est le résultat de l'invocation de presentation(true)
     * Et le dialogue se termine.
     * 
     * @param personne est la personne à qui on s'adresse.
     */
    private void demandeEtVousA(Personne personne) {
        System.out.printf("\" %s \" termine %s.\n", etVous(personne), presentation(true));
    }

    @ToCheck(value = "l'attribut nom", priority = 1)
    @ToDo("Déclarer nom, prenom et homme (lettre pour lettre)")
    // @GetterToCheck (marche pas bien sur String)
    private final String nom;
    @ToCheck(value = "l'attribut prenom", priority = 1)
    @ToDo// @GetterToCheck (marche pas bien sur String)
    private final String prenom;
    @ToCheck(value = "l'attribut homme", priority = 1)
    @ToDo// @GetterToCheck (marche pas bien sur boolean)
    private final boolean homme;

    @ToCheck(value = "le constructeur", priority = 2)
    @ToDo("Le constructeur")
    public Personne(String nom, String prenom, boolean homme) {
        this.nom = nom;
        this.prenom = prenom;
        this.homme = homme;
    }

    @ToDoIn("La présentation courte ou longue (lire l'énoncé)")
    /**
     * Retourne une chaîne de caractères qui constitue la présentation courte ou
     * longue de cette personne, selon l'état du paramètre courte.
     * 
     * La présentation courte est tout simplement le prénom suivi du nom
     * Par exemple, Lucky Luke
     * 
     * La présentation longue est M. ou Mme (en fonction de isHomme()) suivi du
     * nom et du prénom
     * Par exemple, M. Luke Lucky
     * 
     * ATTENTION : utilisez NÉCESSAIREMENT les getters et NON les attributs.
     * 
     * @param courte pour déterminer le type de présentation
     * 
     * @return la chaîne de présentation
     */
    protected String presentation(boolean courte) {
        if (courte) {
            return String.format("%s %s", getPrenom(), getNom());
        } else {
            return String.format("%s %s %s", (homme ? "M." : "Mme"), getNom(), getPrenom());
        }
    }

    /**
     * La présentation par défaut et la présentation longue.
     * 
     * @return la présentation longue
     */
    private String presentation() {
        return presentation(false);
    }

    /**
     * bonjour()
     * @return "Bonjour, je suis " suivi de la présentation longue de cette personne
     */
    protected String bonjour() {
        return "Bonjour, je suis " + presentation();
    }
    
    /**
     * reponseAuBonjourDe(Personne personne)
     * @param personne la personne à qui répond cette personne.
     */
    protected String reponseAuBonjourDe(Personne personne) {
        return "Bonjour " + personne.presentation(true) + ".\n   Moi c'est " + presentation();
    }

    /**
     * caVa(Personne personne)
     * @param personne la personne à qui répond cette personne.
     * @return "Comment allez-vous ?"
     */
    protected String caVa(Personne personne) {
        return "Comment allez-vous ?";
    }

    /**
     * etVous(Personne personne)
     * @param personne la personne à qui répond cette personne.
     * @return "Ca va bien. Merci"
     */
    protected String etVous(Personne personne) {
        return "Ca va bien. Merci";
    }

    @ToDo("Les getters")
    @ToCheck(value = "getNom()", priority = 4)
    public String getNom() {
        return nom;
    }

    @ToDo
    @ToCheck(value = "getPrenom()", priority = 4)
    public String getPrenom() {
        return prenom;
    }

    @ToDo
    @ToCheck(value = "isHomme()", priority = 4)
    public final boolean isHomme() {
        return homme;
    }
}
