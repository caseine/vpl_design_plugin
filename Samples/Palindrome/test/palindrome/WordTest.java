/*
 * @author : Yvan Maillot (yvan.maillot@uha.fr)
 */
package palindrome;

import org.junit.Test;
import static org.junit.Assert.*;

public class WordTest {

    private static class Pair {

        String mot;
        boolean rep;

        public Pair(String mot, boolean rep) {
            this.mot = mot;
            this.rep = rep;
        }

    }

    public WordTest() {
    }

    /**
     * Test of isPalindrome method, of class Word.
     */
    @Test
    public void testIsPalindrome() {
        System.out.println("isPalindrome");
        Pair[] pairs = {
            new Pair("ABBA", true),
            new Pair("ABCBA", true),
            new Pair("kayak", true),
            new Pair("ABBBBC", false),
            new Pair("TAGTAG", false),
            new Pair("3A11A3", true),
            new Pair("AAAAAAAAAAAA888889A", false),
            new Pair("AAAAAAAAAAAA888889AAAAAAAAAAAA", false),
            new Pair("ZaZAAAAAAAAAAAAbnAAAAAAAAAAAAZaZ", false),
            new Pair("ZaZAAAAAAAAAAAA   AAAAAAAAAAAAZaZ", true),
            new Pair("Mi Natai", false),};

        for (Pair pair : pairs) {
            assertEquals(pair.rep, new Word(pair.mot).isPalindrome());
        }
    }

}
