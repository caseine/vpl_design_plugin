/*
 * @author : Yvan Maillot (yvan.maillot@uha.fr)
 */
package palindrome;

import caseine.tags.ToDoIn;

public class Word {
   /**
     * Attribute representing the word
     */
    private String word;

    /**
     * A constructor based on a given word
     * @param word
     */
    public Word(String word) {
        this.word = word;
    }

    /**
     * @return true if this word is a Palindrome
     * To help you, have a look at the following code.
     * The method charAt of the class String allows to access the caracter at position i in a String.
     * Notice the primitive type "char" exists in java:
     * 
     * String myWord = "blabla";
     * char c1 = myWord.charAt(2);
     * char c2 = myWord.charAt(5);
     * if (c1 == c2) {
     *     // ...
     * }
     * int taille = myWord.length(); //give the number of caracters in the word
     */
    @ToDoIn("Écrire l'algo qui va bien")
    public boolean isPalindrome() {
        int n = word.length();
        boolean isPalindrome = true;
        int i = 0;
        while (isPalindrome && i <= n/2) {
            isPalindrome = (word.charAt(i) == word.charAt(n-i-1));
            i++;
        }
        return isPalindrome;
    }
   
}
