#include <iostream>
#include <random>
#include <sstream>
#include "utilities.h"
#include "ref_utilities.h"

using namespace std;

string testFonctionRechercheDuMinimum() {
 	// ToDo Write a test
	return "KO1";
}

string testFonctionRechercheDuMaximum() {
 	// ToDo Write a test
	return "KO2";
}

string testFonctionMoyenne() {
 	// ToDo Write a test
	return "KO3";
}


void allTests() {
	string choix;
	cin >> choix;

	if (choix == "Min") {
		cout << testFonctionRechercheDuMinimum();

	} else if (choix == "Max") {
		cout << testFonctionRechercheDuMaximum();

	} else if (choix == "Moyenne") {
		cout << testFonctionMoyenne();

	} else {
		cout << "KO";
	}
}

int main(int argc, char** argv) {
	allTests();
}


