#!/bin/bash
# Restore all saved files

for i in *.SAVE
do
	cp $i `basename $i .SAVE`
	rm $i
done
