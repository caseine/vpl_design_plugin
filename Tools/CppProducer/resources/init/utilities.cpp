#include <iostream>
#include <vector>
using namespace std;

/**
 * Écrire la fonction mini qui retourne un entier de valeur minimale dans le tableau t de taille n.
 * @param t un tableau d'entiers de taille n
 * @param n la taille de t
 *
 * precondition : n > 0
 *
 * @return un entier de valeur minimale dans t
 */
int mini(int t[], int n) {
	// #-#
    int m = t[0];
    for (int i = 1; i < n; ++i) {
        if (t[i] < m)
            m = t[i];
    }
    return m;
	// #=#
	// return 0;
	// #-#
}

/**
 * Écrire la fonction maxi qui retourne un entier de valeur maximal dans le vector<int>.
 * @param t un tableau d'entiers de taille n
 * @param n la taille de t
 *
 * precondition : n > 0
 *
 * @return un entier de valeur minimale dans t
 */
int maxi(vector<int> t) {
    // #-#
    int m = t[0];
    for (int i = 1; i < t.size(); ++i) {
        if (t[i] > m)
            m = t[i];
    }
    return m;
    // #=#
    // return 0;
    // #-#
}

int main() {
    cout << "Écrire dans le main vos propres tests" << endl;
    int t[] = {5, 3,4, 8};

    cout << "mini = " << mini(t, 4) << endl;

}

