//#DEL#
#ifndef _UTIL_H_
#define _UTIL_H_

#include <iostream>
#include <vector>

using namespace std;

int mini(int[], int);
int maxi(vector<int>);

#endif

// end of file