#include "cppproducerutilities.h"
#include "utilities.h"
#include "ref_utilities.h"

using namespace std;

string testFonctionRechercheDuMin() {
    /*
   * Un exemple de test unitaire pour vérifier que la fonction min de l'étudiant remplit son office
   *
   * 1. par rapport à la fonction de référence ref::min de l'enseignant
   *
   * 2. et à des jeux de tests pertinents
   */
    random_device rd;
    mt19937 gen(rd());
    // Pour tirer "au hasard" des nombres entre -10 et 10
    uniform_int_distribution<> distrib(-10, 10);
    // Pour tirer "au hasard" des nombres entre 10 et 20 (strictement positifs)
    uniform_int_distribution<> spositive_distrib(10, 20);
    // Pour tirer "au hasard" des nombres entre -10 et -20 (strictement négatifs)
    uniform_int_distribution<> snegative_distrib(-20, -10);
    // Quelques jeux de tests en "dur"
    {
        int t[] = {0, 2, 8, 9, 4, 7, 1};
        if (mini(t, 6) != 0)
            return "Revoir votre fonction min()";
    }
    {
        int t[] = {0, 1, 2, 3};
        if (mini(t, 3) != 0)
            return "Revoir votre fonction min()";
    }
    {
        int t[] = {1, 2, -3};
        if (mini(t, 3) != -3)
            return "Revoir votre fonction min()";
    }
    // Pour varier les plaisirs, quels tests tirés au hasard (mais néanmoins pertinents)
    // Test pour des tableaux de 1 à 10
    for (int n = 1; n <= 10; ++n) {
        int t[n];
        // Un tableau dont les valeurs sont dans [-10, 10]
        for (int i = 0; i < n; ++i) {
            t[i] = distrib(gen);
        }
        int min = mini(t, n);
        int ref_min = ref::mini(t, n);
        if (min != ref_min) {
            // Si différent de la fonction de référence, il y a un problème
            return "Revoir votre fonction min()";
        }
        // Un tableau dont les valeurs sont dans [10, 20]
        // Dans le cas où l'étudiant fait l'erreur de commencer avec une valeur mini égale à 0
        for (int i = 0; i < n; ++i) {
            t[i] = spositive_distrib(gen);
        }
        min = mini(t, n);
        ref_min = ref::mini(t, n);
        if (min != ref_min) {
            stringstream ss;
            ss << "Revoir votre fonction min() : minimum trouvé : " << min
               << ". C'est incorrect pour certains jeux de test";
            return ss.str();
        }
        // Un tableau dont les valeurs sont dans [-20, -10]
        // Juste comme ça
        for (int i = 0; i < n; ++i) {
            t[i] = snegative_distrib(gen);
        }
        min = mini(t, n);
        ref_min = ref::mini(t, n);
        if (min != ref_min) {
            return "Revoir votre fonction min()";
        }
    }
    // Tous les tests sont passés.
    return "OK";
}
/*
 * Cette fois on veut donner plus d'indice à l'étudiant.
 * C'est la raison des deux fonctions suivantes.
 */
/**
 * Pour envoyer un vector dans un flux afin de proposer un joli affichage des valeurs du tableau séparées deux à deux
 * par une virgule et encadré par des accolades.
 */
ostream &operator<<(ostream &os, vector<int> v) {
    os << "{";
    if (v.size() > 0) {
        os << v[0];
        for (int i = 1; i < v.size(); ++i) {
            os << ", " << v[i];
        }
    }
    os << "}";
    return os;
}

/*
 * Une fonction qui construit une chaîne à partir d'un vecteur (v), d'une valeur trouvée (found) et d'une valeur
 * attendue (expected) pour qu'en cas d'erreur l'étudiant puisse savoir le jeu de test employé et son résultat.
 */
string msg(const vector<int> &v, int found, int expected) {
    stringstream ss;
    ss << "K0 : dans " << v << endl;
    ss << "Le max devrait être " << expected << endl;
    ss << "Mais " << found << " a été trouvé" << endl;
    return ss.str();
}


/*
* Un exemple de test unitaire pour vérifier que la fonction max de l'étudiant remplit son office en utilisant les mêmes
 * jeux de tests que pour min
 * */
string testFonctionRechercheDuMax() {
    random_device rd;
    mt19937 gen(rd());
    uniform_int_distribution<> distrib(-10, 10);
    uniform_int_distribution<> spositive_distrib(10, 20);
    uniform_int_distribution<> snegative_distrib(-20, -10);
    // Quelques jeux de tests en "dur"
    {
        vector<int> v{0, 2, 8, 9, 4, 7, 1};
        int m = maxi(v);
        if (m != 9)
            return msg(v, m, 9);
    }
    {
        vector<int> v{0, 1, 2, 3};
        int m = maxi(v);
        if (m != 3)
            return msg(v, m, 3);
    }
    {
        vector<int> v{1, 2, -3};
        int m = maxi(v);
        if (m != 2)
            return msg(v, m, 2);
    }

    for (int n = 1; n <= 10; ++n) {
        {
            vector<int> v;
            for (int i = 0; i < n; ++i) {
                v.push_back(distrib(gen));
            }
            int max = maxi(v);
            int ref_max = ref::maxi(v);
            if (max != ref_max) {
                return msg(v, max, ref_max);
            }
        }
        {
            vector<int> v;
            for (int i = 0; i < n; ++i) {
                v.push_back(spositive_distrib(gen));
            }
            int max = maxi(v);
            int ref_max = ref::maxi(v);
            if (max != ref_max) {
                return msg(v, max, ref_max);
            }
        }
        {
            vector<int> v;
            for (int i = 0; i < n; ++i) {
                v.push_back(snegative_distrib(gen));
            }
            int max = maxi(v);
            int ref_max = ref::maxi(v);
            if (max != ref_max) {
                return msg(v, max, ref_max);
            }
        }
    }
    return "OK";
}


void allTests() {
    string choix;
    cin >> choix;

    if (choix == "FonctionRechercheDuMin") {
        cout << testFonctionRechercheDuMin();

    } else if (choix == "FonctionRechercheDuMax") {
        cout << testFonctionRechercheDuMax();

    } else {
        cout << "KO";
    }
}

int main(int argc, char **argv) {
    killer("Trop long : probablement une boucle infinie", 5);
    allTests();
}


