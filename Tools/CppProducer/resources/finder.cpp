//
// Created by yvan on 06/08/2020.
//
#include <fstream>
#include <iostream>
#include <string>
#include <vector>
#include <sstream>
#include <regex>

#define stdout_as_string(f, s) { streambuf *ym_2020_old; stringstream ym_2020_buf; ym_2020_old = cout.rdbuf(ym_2020_buf.rdbuf()); f; cout.rdbuf(ym_2020_old); s = ym_2020_buf.str(); }

using namespace std;

string find(string filename, string name) {
    regex e("[^\\s]*[\\s]+" + name + "[\\s]*\\(.*");
    smatch sm;
    ifstream in(filename.c_str());
    stringstream ss;
    string line;


    while (getline(in, line) && !regex_match(line, sm, e)) {
    }
    bool again = true;
    int nb = 0;

    if (sm.size() == 0)
        return "";

    ss << line << endl;

    for (string::iterator i = line.begin(), end = line.end(); i != end; ++i) {
        if (*i == '{') {
            ++nb;
        } else if (*i == '}') {
            --nb;
        }
    }
    while (nb > 0 && getline(in, line)) {
        for (string::iterator i = line.begin(), end = line.end(); i != end; ++i) {
            if (*i == '{') {
                ++nb;
            } else if (*i == '}') {
                --nb;
            }
        }
        ss << line << endl;
    }
    in.close();

    return ss.str();
}