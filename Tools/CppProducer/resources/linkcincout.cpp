/*
 * Creative commons CC BY-NC-SA 2022 Stéphane Rivière <stephane.riviere@uha.fr>
 * inspired from an idea of Yvan Maillot <yvan.maillot@uha.fr>
 *
 *     Share - You can copy and redistribute the material in any medium or format
 *
 *     Adapt - You can remix, transform, and build upon the material
 *
 * Under the following terms :
 *
 *     Attribution - You must give appropriate credit, provide a link to the license,
 *     and indicate if changes were made. You may do so in any reasonable manner,
 *     but not in any way that suggests the licensor endorses you or your use.
 *
 *     NonCommercial — You may not use the material for commercial purposes.
 *
 *     ShareAlike — If you remix, transform, or build upon the material,
 *     you must distribute your contributions under the same license as the original.
 *
 * Notices:    You do not have to comply with the license for elements of
 *             the material in the public domain or where your use is permitted
 *             by an applicable exception or limitation.
 *
 * No warranties are given. The license may not give you all of the permissions
 * necessary for your intended use. For example, other rights such as publicity,
 * privacy, or moral rights may limit how you use the material.
 *
 * See <https://creativecommons.org/licenses/by-nc-sa/4.0/>.
 */

#include"linkcincout.h"
#include <sstream>

//stream buffer of cin before linking it
std::streambuf* old_cin_buffer {nullptr};
//pointer to the istream to which cin is linked
std::istream* linked_istream {nullptr};
//pointer to the string to which cin is linked
std::string* linked_istring {nullptr};
//string buffer associated to the string to which cin is linked
std::istringstream linked_istring_buffer;

bool cin_linked {false};

void link_cin()
{
  if (!cin_linked)
  {
    cin_linked = true;
    old_cin_buffer = std::cin.rdbuf(linked_istream->rdbuf());
  }
  else
  {
    std::cin.rdbuf(linked_istream->rdbuf());
  }
}

void link_cin_to(std::string& s)
{

  linked_istring = &s;
  linked_istring_buffer.str(s);
  linked_istream = &linked_istring_buffer;
  link_cin();
}

void link_cin_to_const(const std::string& s)
{

  linked_istring = nullptr;
  linked_istring_buffer.str(s);
  linked_istream = &linked_istring_buffer;
  link_cin();
}

void link_cin_to(std::istream& ist)
{
  linked_istring = nullptr;
  linked_istream = &ist;
  link_cin();
}

void unlink_cin()
{
  if (cin_linked)
  {
    cin_linked = false;
    if (linked_istring)
    {
      auto pos {linked_istring_buffer.tellg()};
      if (pos != -1)
      {
        std::string s {linked_istring_buffer.str()};
        *linked_istring = s.substr(pos,s.length()-pos);
      }
      else
      {
        *linked_istring = "";
      }
      linked_istring_buffer.str("");
      linked_istring = nullptr;
    }
    else
    {
      linked_istream = nullptr;
    }
    std::cin.rdbuf(old_cin_buffer);
    old_cin_buffer = nullptr;
  }
}

//stream buffer of cout before linking it
std::streambuf* old_cout_buffer;
//pointer to the ostream to which cout is linked
std::ostream* linked_ostream {nullptr};
//pointer to the string to which cout is linked
std::string* linked_ostring {nullptr};
//string buffer associated to the string to which cout is linked
std::stringstream linked_ostring_buffer;

bool cout_linked {false};

void link_cout()
{
  if (!cout_linked)
  {
    cout_linked = true;
    old_cout_buffer = std::cout.rdbuf(linked_ostream->rdbuf());
  }
  else
  {
    std::cout.rdbuf(linked_ostream->rdbuf());
  }
}

void link_cout_to(std::string& s)
{
  linked_ostring = &s;
  linked_ostring_buffer.str(s);
  linked_ostream = &linked_ostring_buffer;
  link_cout();
}

void link_cout_to(std::ostream& ost)
{
  linked_ostring = nullptr;
  linked_ostream = &ost;
  link_cout();
}

void unlink_cout()
{
  if (cout_linked)
  {
    cout_linked = false;
    if (linked_ostring)
    {
      (*linked_ostring) += linked_ostring_buffer.str();
      linked_ostring_buffer.str("");
      linked_ostring = nullptr;
    }
    else
    {
      linked_ostream = nullptr;
    }
    std::cout.rdbuf(old_cout_buffer);
    old_cout_buffer = nullptr;
  }
}

