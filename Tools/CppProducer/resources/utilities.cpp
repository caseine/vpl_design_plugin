

#include "main.h"

/**
 * Écrire la fonction mini qui retourne un entier de valeur minimale du tableau t de taille n.
 * @param t un tableau d'entiers de taille n
 * @param n la taille de t
 *
 * precondition : n > 0
 *
 * @return un entier de valeur minimale dans t
 */
int mini(int t[], int n) {
    // #-#
    int min = t[0];
    for(int i = 1; i < n ; ++i) {
        if (t[i] <  min)
            min = t[i];
    }
    return min;
    // #=#
    // return 0;
    // #-#
}
