#!/bin/bash
# Copier $1 dans $2 en renommant la procédure principale
cat $1 | tr '\n' '\f' | sed s/main\\s*\(\\\([^\(\)]*\\\)\)/REPLACED_MAIN\(\\1\)/g | tr '\f' '\n'  > $2