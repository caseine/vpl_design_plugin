#!/bin/bash
# Save all c and cpp files
# and try to suppress main procedure

#for i in *.c
#do
#	cp $i $i.save
#	bash copyandhidemain.sh $i.save $i
#done

for i in *.cpp
do
	cp $i $i.SAVE
	bash copyandhidemain.sh $i.SAVE $i
done

cp TestMain.cpp.SAVE TestMain.cpp
if [ -f vpl_evaluate.cpp.SAVE ] ; then
  cp vpl_evaluate.cpp.SAVE vpl_evaluate.cpp
fi
