/*******************************************************************************
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 Camilo Sanchez (Camiloasc1) 2020 Martin Mirchev (Marti2203)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * ****************************************************************************
 */

/*
 * Creative commons CC BY-NC-SA 2020 Yvan Maillot <yvan.maillot@uha.fr>
 *
 *     Share - You can copy and redistribute the material in any medium or format
 *
 *     Adapt - You can remix, transform, and build upon the material
 *
 * Under the following terms :
 *
 *     Attribution - You must give appropriate credit, provide a link to the license,
 *     and indicate if changes were made. You may do so in any reasonable manner,
 *     but not in any way that suggests the licensor endorses you or your use.
 *
 *     NonCommercial — You may not use the material for commercial purposes.
 *
 *     ShareAlike — If you remix, transform, or build upon the material,
 *     you must distribute your contributions under the same license as the original.
 *
 * Notices:    You do not have to comply with the license for elements of
 *             the material in the public domain or where your use is permitted
 *             by an applicable exception or limitation.
 *
 * No warranties are given. The license may not give you all of the permissions
 * necessary for your intended use. For example, other rights such as publicity,
 * privacy, or moral rights may limit how you use the material.
 *
 * See <https://creativecommons.org/licenses/by-nc-sa/4.0/>.
 */

#ifndef CPPPARSERENCPP_FILEPARSER_H
#define CPPPARSERENCPP_FILEPARSER_H

#include "antlr4-runtime.h"
#include "CPP14Lexer.h"
#include "CPP14Parser.h"

#include <string>

/**
 * Parses a C++ file
 */
class FileParser {
private:
    std::string d_fileName;
    antlr4::ANTLRInputStream* input;
    antlr4::CommonTokenStream* tokens;
    CPP14Lexer* lexer;
    CPP14Parser* parser;

public:

    /**
     * Constructs un FileParser from a C++ file name
     * @param fileName
     */
    FileParser(const std::string fileName);
    ~FileParser();
    /**
     * Get a parsed function of the parsed file by using a regular expression
     * @param regex
     * @return the first encountering parsed function whose signature matches regex
     */
    CPP14Parser::FunctionDefinitionContext * functionWithRegex(std::string regex) const;
    /**
     * Get a parsed function of the parsed file with its single name
     * @param functionName the name of the function
     * @return the first parsed function whose name
     */
    CPP14Parser::FunctionDefinitionContext * function(std::string functionName) const;
    /**
     * Get a parsed function of the parsed file with its signature
     * @param functionName the name of the function
     * @param params type name of its parameters
     * @return the parsed function whose signature is defined by its name and its params
     */
    CPP14Parser::FunctionDefinitionContext * function(std::string functionName, std::vector<std::string> params) const;
    std::string functionAsStringWithRegex(std::string regex) const;
    std::string functionAsString(std::string functionName) const;
    std::string functionAsString(std::string functionName, std::vector<std::string>) const;
};


#endif //CPPPARSERENCPP_FILEPARSER_H
