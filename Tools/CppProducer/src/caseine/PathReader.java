package caseine;

import caseine.cases.VplEvaluateCases;
import caseine.exceptions.MissingVplEvaluateCaseFileException;
import caseine.exceptions.RootFileNotEmptyException;
import caseine.generators.CfRfEfFileGenerator;
import caseine.generators.UnitTestsAnalyzer;

import java.io.*;
import java.nio.file.Files;
import java.util.*;

import static caseine.FileUtils.copyAZipAndUnZipIt;

/**
 * Analyse un dossier contenant des fichiers *.cpp et *.h ainsi qu'un vpl_evaluate.cases
 * pour produire le dossier caseine-output ...
 */
public class PathReader {
    public final String TO_DEL = "//#DEL#";
    public final String NO_REF = "//#NOREF#";
    // The root of the directory to analyze
    private final File root;
    // The *.cpp files in order to generate vpl_run.sh
    private final List<File> cppsInEf = new LinkedList<>();
    // The *.cpp files in order to generate vpl_run.sh
    private final List<File> cppsInEf4Test = new LinkedList<>();
    // The root of the directory where is TestMain.cpp
    private File rootDistantTests;
    private File rootLocalTests;
    // The root of the directory cf
    private File rootCf;
    // The root of the directory rf
    private File rootRf;
    // The root of the directory ef
    private File rootEf;
    // The root of the directory ef4test
    private File rootEf4Test;
    // All files in ef are kept in order to change imports before written them
    private Map<File, String> efFiles = new HashMap<>();
    // The replacements in ef in order to change includes in ref
    private Map<File, File> changedInEf = new HashMap<>();
    // The *.h files en ef in order to generate TestMain.cpp
    private List<File> headersInEf = new LinkedList<>();
    // All files in ef4test are kept in order to change imports before written them
    private Map<File, String> ef4TestFiles = new HashMap<>();
    // The replacements in ef in order to change includes in ref
    private Map<File, File> changedInEf4Test = new HashMap<>();
    // The *.h files in ef4test order to generate TestMain.cpp
    private List<File> headersInEf4Test = new LinkedList<>();

    /**
     * @param root
     * @throws IOException
     */
    public PathReader(File root, int n) throws IOException, MissingVplEvaluateCaseFileException, RootFileNotEmptyException {
        this.root = root;
        //checkPath();
        if (n >= 0) {
            if (root.listFiles().length > 0)
                throw new RootFileNotEmptyException("The root directory of a cpp projet should be empty\n" +
                        "when using -cpp-template or -cpp-template-?");
            InputStream tin = FileUtils.class.getResourceAsStream(String.format("/cpp/default/project%d.zip", n));
            if (tin == null)
                tin = CppLauncher.class.getResourceAsStream("/cpp-templates/template-0.zip");
            FileUtils.copyAZipAndUnZipIt(tin, root, ".");
        }
        setCaseineNature(root);
        setTheScene();
        read();
        finishEf();
        finishEf4Test();
    }

    public PathReader(File root) throws IOException, MissingVplEvaluateCaseFileException, RootFileNotEmptyException {
        this(root, -1);
    }
    
    private void setCaseineNature(File root) throws IOException {
    	File caseineFile = new File(root, ".caseine");
    	if (!caseineFile.exists()) {
        	FileUtils.writeAResource(CppLauncher.class.getResourceAsStream("/.caseine"), caseineFile);    		
    	}
    } 

    private void finishEf() throws FileNotFoundException {
    }

    private void finishEf4Test() throws FileNotFoundException {
        Writers.writeVplRunSh4Test(rootEf4Test, cppsInEf4Test);
    }

    /**
     * Sets the scene to push on caseine
     *
     * @throws IOException
     */
    private void setTheScene() throws IOException {
        // Creating file tree
        File caseineOutput = new File(root, "target/caseine-output");
        caseineOutput.mkdirs();
        rootDistantTests = new File(root, "unittests");
        rootDistantTests.mkdirs();
        rootEf = new File(caseineOutput, "ef");
        rootEf.mkdirs();
        rootRf = new File(caseineOutput, "rf");
        rootRf.mkdirs();
        rootCf = new File(caseineOutput, "cf");
        rootCf.mkdirs();
        rootEf4Test = new File(caseineOutput, "ef4test");
        rootEf4Test.mkdirs();
        if (!rootDistantTests.exists()) {
            throw new IOException("Unable to create " + rootDistantTests.getAbsolutePath());
        }
        if (!rootEf.exists()) {
            throw new IOException("Unable to create " + rootEf.getAbsolutePath());
        }
        if (!rootRf.exists()) {
            throw new IOException("Unable to create " + rootRf.getAbsolutePath());
        }
        if (!rootCf.exists()) {
            throw new IOException("Unable to create " + rootCf.getAbsolutePath());
        }
        if (!rootEf4Test.exists()) {
            throw new IOException("Unable to create " + rootEf4Test.getAbsolutePath());
        }
        copyAZipAndUnZipIt(CppLauncher.class.getResourceAsStream("/antlr.zip"), rootEf4Test, "antlr");
        copyAZipAndUnZipIt(CppLauncher.class.getResourceAsStream("/cppparser.zip"), rootEf4Test, "cppparser");
    }

    private void write(File file, String contenu) throws FileNotFoundException {
        try (PrintWriter out = new PrintWriter(file)) {
            out.print(contenu);
        }
    }
    /*
        If the first or the seconde line of a file is the NO_REF Tag, this file will not be a reference
    */
    private boolean isNO_REF(File file) {
        try (java.util.Scanner in = new Scanner(file)) {
            int nb = 0;
            while(in.hasNextLine() && nb < 10) {
                if (NO_REF.equalsIgnoreCase(in.nextLine().trim().replaceAll("\\s+", "").toUpperCase()))
                    return true;
                ++nb;
            }
            return false;
        } catch (Exception e) {
            return false;
        }
    }

    /*
    If the first or the second line of a file is the TO_DEL Tag, this file is to be deleted from rf and cf, but it has to be added
    to ef.
     */
    private boolean isToDel(File file) {
        try (java.util.Scanner in = new Scanner(file)) {
            int nb = 0;
            while(in.hasNextLine() && nb < 10) {
                if (TO_DEL.equalsIgnoreCase(in.nextLine().trim().replaceAll("\\s+", "").toUpperCase()))
                    return true;
                ++nb;
            }
            return false;
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * @throws IOException
     */
    private void read() throws IOException, MissingVplEvaluateCaseFileException {
        CfRfEfFileGenerator cfRfEfFileGenerator;
        File main = null;
        for (File file : root.listFiles()) {
            // Handles headers
            if (file.getName().endsWith(".h")) {
                cfRfEfFileGenerator = new CfRfEfFileGenerator(file);
                // A file to delete is indeed in ef and not in cf nor rf
                // Otherwise it is in cf and rf and not in ef
                if (!isToDel(file)) {
                    write(new File(rootRf, file.getName()), cfRfEfFileGenerator.getRf());
                    write(new File(rootCf, file.getName()), cfRfEfFileGenerator.getCf());
                } else {
                    write(new File(rootEf, file.getName()), cfRfEfFileGenerator.getCf());
                }
                // Any file is in ef4test
                write(new File(rootEf4Test, file.getName()), cfRfEfFileGenerator.getCf());

                // Adds file to headers
                headersInEf.add(file);
                // Adds file to headers
                headersInEf4Test.add(file);
                // Files are prefixed by ref_ in ef (if they are not NO_REF)
                if (!isNO_REF(file)) {
                    File changedEfFile = new File(rootEf, "ref_" + file.getName());
                    // Map between the file and its contents
                    efFiles.put(changedEfFile, cfRfEfFileGenerator.getEf());
                    // Map between file and its corresponding changer file in ef
                    changedInEf.put(file, changedEfFile);
                    // Adds changedEfFile to headers
                    headersInEf.add(changedEfFile);
                    // Files are prefixed by ref_ in ef4Test
                    File changedEf4TestFile = new File(rootEf4Test, "ref_" + file.getName());
                    // Map between the file and its contents
                    ef4TestFiles.put(changedEf4TestFile, cfRfEfFileGenerator.getEf());
                    // Map between file and its corresponding changed file in ef
                    changedInEf4Test.put(file, changedEf4TestFile);
                    // Adds changedEfFile to headers
                    headersInEf4Test.add(changedEf4TestFile);
                }


            } else if (file.getName().endsWith(".cpp")) {
                cfRfEfFileGenerator = new CfRfEfFileGenerator(file);
                // A file to delete is indeed in ef and not in cf nor rf
                // Otherwise it is in cf and rf and not in ef
                if (!isToDel(file)) {
                    write(new File(rootRf, file.getName()), cfRfEfFileGenerator.getRf());
                    write(new File(rootCf, file.getName()), cfRfEfFileGenerator.getCf());
                } else {
                    write(new File(rootEf, file.getName()), cfRfEfFileGenerator.getCf());
                }
                write(new File(rootEf4Test, file.getName()), cfRfEfFileGenerator.getCf());

                cppsInEf.add(file);
                cppsInEf4Test.add(file);
                //if (!isMain) {
                // In ef

                if (!isNO_REF(file)) {
                    File changedEfFile = new File(rootEf, "ref_" + file.getName());
                    efFiles.put(changedEfFile, cfRfEfFileGenerator.getEf());
                    cppsInEf.add(changedEfFile);
                    changedInEf.put(file, changedEfFile);
                    // In ef4Test
                    File changedEf4TestFile = new File(rootEf4Test, "ref_" + file.getName());
                    efFiles.put(changedEf4TestFile, cfRfEfFileGenerator.getEf());
                    cppsInEf4Test.add(changedEf4TestFile);
                    changedInEf4Test.put(file, changedEf4TestFile);
                }
                //}
            }
        }
        // In ef
        for (File file : efFiles.keySet()) {
            write(file, changeImportsAndTemplates(efFiles.get(file)));
        }


        if (unitTestsArePresent()) {
            // #### Génération à partir des tests unitaires
            UnitTestsAnalyzer unitTestsAnalyzer = new UnitTestsAnalyzer(new File(rootDistantTests, "TestMain.cpp"));
            Writers.writeVplEvaluateCases(rootEf, unitTestsAnalyzer.getVplEvaluateCases());
            Writers.writeTestMainCpp(unitTestsAnalyzer.getTestMain(), headersInEf4Test, rootEf);
        } else {
            VplEvaluateCases vplEvaluateCases = new VplEvaluateCases(new File(root, "vpl_evaluate.cases"));
            // Writing vpl_evaluate.cases
            Writers.writeVplEvaluateCases(rootEf, vplEvaluateCases);
            // Writing TestMain.cpp
            Writers.writeTestMainCpp(rootDistantTests, vplEvaluateCases, headersInEf);
            FileUtils.TreeCopy(new File(rootDistantTests, "TestMain.cpp").toPath(), rootEf.toPath());

        }
        // In ef4Test
        for (File file : ef4TestFiles.keySet()) {
            write(file, changeImportsAndTemplates(ef4TestFiles.get(file)));
        }

        Writers.writeVplEvaluateSh(rootEf4Test);
        Writers.writeVplEvaluateCpp(rootEf4Test);
        Writers.writeCppProducerUtilitiesCppH(rootEf4Test);
        Writers.writeLinkCinCoutCppH(rootEf4Test);
        Writers.writeOtherShellScriptsUtilities(rootEf4Test);

        if (unitTestsArePresent()) {
            // #### Génération à partir des tests unitaires
            UnitTestsAnalyzer unitTestsAnalyzer = new UnitTestsAnalyzer(new File(rootDistantTests, "TestMain.cpp"));
            Writers.writeVplEvaluateCases(rootEf4Test, unitTestsAnalyzer.getVplEvaluateCases());
            Writers.writeTestMainCpp(unitTestsAnalyzer.getTestMain(), headersInEf4Test, rootEf4Test);

        } else {
            VplEvaluateCases vplEvaluateCases4Test = new VplEvaluateCases(new File(root, "vpl_evaluate.cases"));
            Writers.writeVplEvaluateCases(rootEf4Test, vplEvaluateCases4Test);
            Writers.writeTestMainCpp(rootDistantTests, vplEvaluateCases4Test, headersInEf4Test, rootLocalTests);
            File localTestMain = new File(rootLocalTests, "TestMain.cpp");
            FileUtils.TreeCopy(localTestMain.toPath(), rootEf4Test.toPath());
            Files.deleteIfExists(localTestMain.toPath());
            Files.deleteIfExists(rootLocalTests.toPath());
        }
    }

    private boolean unitTestsArePresent() {
        return true;
    }

    private String changeImportsAndTemplates(String s) {
        for (File old : changedInEf.keySet()) {
            // Pour changer les includes. Par exemple si exemple.h devient ref_exemple.h
            // #include "exemple.h" doit devenir #include "ref_exemple.h"
            s = s.replaceAll(old.getName(), changedInEf.get(old).getName());

            //s = s.replaceAll(old.getName().toUpperCase().replaceAll("\\.", "_"), changes.get(old).getName().toUpperCase().replaceAll("\\.", "_"));

        }
        // Pour #define et #ifndef
        s = s.replaceAll("#ifndef\\s+_*(.*)", "#ifndef _REF_$1");
        s = s.replaceAll("#define\\s+_*(.*)", "#define _REF_$1");
        return s;
    }

    public static void main(String[] args) throws IOException, MissingVplEvaluateCaseFileException, RootFileNotEmptyException {
        new PathReader(new File("/home/yvan/tmp/c++/exo2"), -1);
        //new PathReader(new File("/home/yvan/Development/GitLabCaseine/git-lab-caseine/SDD-L2-UHA/PetitsExos"));
        //new PathReader(new File("/home/yvan/Development/GitLabCaseine/git-lab-caseine/SDD-L2-UHA/Chapitre1"));
    }
}
