package caseine;

import caseine.cases.VplEvaluateCases;
import caseine.generators.TestMainCppGenerator;

import java.io.*;
import java.net.URISyntaxException;
import java.util.List;

import static caseine.FileUtils.writeAResource;

public class Writers {

    private static final String ROOT_ANTLR = "/home/caseineTest/CppProducer/antlr";
    private static final String ROOT_PARSER = "/home/caseineTest/CppProducer/cppparser";


    /**
     * Pour générer le fichier vpl_run.sh qui
     * - sauvegarde tous les fichiers cpp et camoufle le "main" (saveallccppfiles.sh)
     * - copie le fichier ../ef/TestMain.cpp dans le répertoire courant (il contient les tests de l'évaluation)
     * - tente de compiler tous les fichiers de l'étudiant avec le TestMain.cpp et les libs cppparser et antlr en l'exécutable vpl_execution
     * - si la compilation réussie, vpl_execution est lancé pour chaque entrée du vpl_evaluate.cases
     * - à la fin, tous les fichiers cpp sont restaurés.
     * @param rootEf4Test
     * @param cpps
     * @throws FileNotFoundException
     */
    public static void writeVplRunSh4Test(File rootEf4Test, List<File> cpps) throws FileNotFoundException {
        File vplRunSh = new File(rootEf4Test, "vpl_run.sh");
        StringBuilder sb = new StringBuilder();

        try (PrintWriter out = new PrintWriter(vplRunSh)) {
            out.println("#!/bin/bash");
            out.println("# Modifier by Yvan Maillot <yvan.maillot@uha.fr>");
            out.println();
            out.println("bash saveallccppfiles.sh");
            out.println("cp ../ef/TestMain.cpp .");
            out.print(sb.toString());
            sb.append("if g++ -w -fno-diagnostics-color -o vpl_execution")
                    .append(' ')
                    .append("-I./antlr/include/antlr4-runtime")
                    .append(' ')
                    .append("-I./cppparser/include")
                    .append(' ')
                    .append("-I./cppparser/include/generated/")
                    .append(' ')
                    .append("cppproducerutilities.cpp")
                    .append(' ')
                    .append("linkcincout.cpp");
            for (File file : cpps) {
                sb.append(' ').append(file.getName());
            }
            sb.append(" TestMain.cpp cppparser/lib/libCppParserLib.a  antlr/lib/libantlr4-runtime.a -lm -lutil -lpthread").append('\n');
            out.println(sb);
            out.println("then");
            out.println("    echo '================================='");
            out.println("    for i in `cat vpl_evaluate.cases | grep \"input =\" | sed s/\"input =\"//g`");
            out.println("    do");
            out.println("      echo $i");
            out.println("      ./vpl_execution <<< $i");
            out.println("      echo");
            out.println("      echo '-------------------------------'");
            out.println("    done");
            out.println("    echo '================================='");
            out.println("else");
            out.println("  echo KO");
            out.println("fi");
            out.println("bash restoreallsavedfiles.sh");
            out.println("");
        }
    }


    /*public static void writeAResource(String resourcePathName, File toWrite) throws FileNotFoundException {

        try (
                PrintWriter out = new PrintWriter(toWrite);
                Scanner in = new Scanner(Writers.class.getResourceAsStream(resourcePathName));
        ) {
            while (in.hasNextLine()) {
                out.println(in.nextLine());
            }
        }
    }*/

    public static void writeVplEvaluateSh(File rootEf) throws IOException {
        writeAResource(CppLauncher.class.getResourceAsStream("/vpl_evaluate.sh"), new File(rootEf, "vpl_evaluate.sh"));
    }

    public static void writeVplEvaluateCpp(File rootEf) throws IOException {
        writeAResource(CppLauncher.class.getResourceAsStream("/vpl_evaluate.cpp"), new File(rootEf, "vpl_evaluate.cpp"));
    }

    public static void writeCppProducerUtilitiesH(File root) throws IOException {
        writeAResource(CppLauncher.class.getResourceAsStream("/cppproducerutilities.h"), new File(root, "cppproducerutilities.h"));
    }

    public static void writeCppProducerUtilitiesCppH(File root) throws IOException {
        writeAResource(CppLauncher.class.getResourceAsStream("/cppproducerutilities.cpp"), new File(root, "cppproducerutilities.cpp"));
        writeAResource(CppLauncher.class.getResourceAsStream("/cppproducerutilities.h"), new File(root, "cppproducerutilities.h"));
    }

    public static void writeLinkCinCoutH(File root) throws IOException {
        writeAResource(CppLauncher.class.getResourceAsStream("/linkcincout.h"), new File(root, "linkcincout.h"));
    }

    public static void writeLinkCinCoutCppH(File root) throws IOException {
        writeAResource(CppLauncher.class.getResourceAsStream("/linkcincout.cpp"), new File(root, "linkcincout.cpp"));
        writeAResource(CppLauncher.class.getResourceAsStream("/linkcincout.h"), new File(root, "linkcincout.h"));
    }

    public static void writeOtherShellScriptsUtilities(File rootEf) throws IOException {
        writeAResource(CppLauncher.class.getResourceAsStream("/restoreallsavedfiles.sh"), new File(rootEf, "restoreallsavedfiles.sh"));
        writeAResource(CppLauncher.class.getResourceAsStream("/saveallccppfiles.sh"), new File(rootEf, "saveallccppfiles.sh"));
        writeAResource(CppLauncher.class.getResourceAsStream("/copyandhidemain.sh"), new File(rootEf, "copyandhidemain.sh"));
    }

    public static void writeTestMainCpp(String content, List<File> headers, File dest) throws FileNotFoundException {
        try (PrintWriter out = new PrintWriter(new File(dest, "TestMain.cpp"))) {
            out.println(content);
        }
    }

    public static void writeTestMainCpp(File root, VplEvaluateCases vplEvaluateCases, List<File> headers, File dest) throws FileNotFoundException {
        TestMainCppGenerator testMainCppGenerator = new TestMainCppGenerator(root, headers, vplEvaluateCases);
        try (PrintWriter out = new PrintWriter(new File(dest, "TestMain.cpp"))) {
            out.println(testMainCppGenerator);
        }
    }

    public static void writeTestMainCpp(File root, VplEvaluateCases vplEvaluateCases, List<File> headers) throws FileNotFoundException {
        writeTestMainCpp(root, vplEvaluateCases, headers, root);
    }

    public static void writeVplEvaluateCases(File root, VplEvaluateCases vplEvaluateCases) throws FileNotFoundException, UnsupportedEncodingException {
        writeVplEvaluateCases(root, vplEvaluateCases.toStringProportional());
    }

    public static void writeVplEvaluateCases(File root, String vplEvaluateCases) throws FileNotFoundException {
        try (PrintWriter out = new PrintWriter(new File(root, "vpl_evaluate.cases"))) {
            out.println(vplEvaluateCases);
        }
    }

    public static void main(String[] args) throws IOException, URISyntaxException {
        File root = new File(Writers.class.getResource("/").toURI());
        System.out.println(root.exists());
        for (File file : root.listFiles()) {
            System.out.println(file);
        }
    }
}
