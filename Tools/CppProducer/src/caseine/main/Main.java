package caseine.main;

import caseine.Writers;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Arrays;

import static caseine.FileUtils.TreeCopy;


public class Main {
private static void setTheSceneForTest() throws IOException {
        // Creating file tree
        File caseineOutput = new File("/home/yvan/tmp/truc", "target/caseine-output");
        caseineOutput.mkdirs();
        File rootEf = new File(caseineOutput, "ef");
        rootEf.mkdirs();
        File rootRf = new File(caseineOutput, "rf");
        rootRf.mkdirs();
        File rootCf = new File(caseineOutput, "cf");
        rootCf.mkdirs();
        File rootEf4Test = new File(caseineOutput, "ef4test");
        rootEf4Test.mkdirs();
        if (!rootEf.exists()) {
            throw new IOException("Unable to create " + rootEf.getAbsolutePath());
        }
        if (!rootRf.exists()) {
            throw new IOException("Unable to create " + rootRf.getAbsolutePath());
        }
        if (!rootCf.exists()) {
            throw new IOException("Unable to create " + rootCf.getAbsolutePath());
        }
        if (!rootEf4Test.exists()) {
            throw new IOException("Unable to create " + rootEf4Test.getAbsolutePath());
        }
        // copying libs
        try {
            TreeCopy(new File(Writers.class.getResource("/antlr").toURI()).toPath(), rootEf.toPath());
            TreeCopy(new File(Writers.class.getResource("/cppparser").toURI()).toPath(), rootEf.toPath());
            TreeCopy(new File(Writers.class.getResource("/antlr").toURI()).toPath(), rootEf4Test.toPath());
            TreeCopy(new File(Writers.class.getResource("/cppparser").toURI()).toPath(), rootEf4Test.toPath());
        } catch (Exception ex) {
            System.err.println(Arrays.toString(ex.getStackTrace()));
            throw new IOException("Unable to create " + ex);
        }
        //Files.copy(new File("antlr").toPath(), rootEf.toPath());
        //Files.copy(new File("cppparser").toPath(), rootEf.toPath());
        //Files.copy(new File("antlr").toPath(), rootEf4Test.toPath());
        //Files.copy(new File("cppparser").toPath(), rootEf4Test.toPath());
    }

    public static void main(String[] args) throws IOException, URISyntaxException {
        setTheSceneForTest();
    }
}
