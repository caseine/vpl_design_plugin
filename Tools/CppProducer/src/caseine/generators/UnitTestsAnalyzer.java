package caseine.generators;

import caseine.cases.VplEvaluateCases;

import java.io.*;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class UnitTestsAnalyzer {
    private final File unitTestsFile;
    private String contentWithoutComment;
    private String contentWithoutBloc;
    private String allContent;
    private Map<String, Double> testUnitaireGrade = new LinkedHashMap<>();

    private String vplEvaluateCases;

    private String allTestsAndMain;

    private String testMain;

    private double total;
    private double noteMaxi = 100;

    public UnitTestsAnalyzer(File unitTestsFile) throws IOException {
        this.unitTestsFile = unitTestsFile;
        readFile();

        contentWithoutBloc = removeBlocs(contentWithoutComment);
        if (contentWithoutBloc.replaceAll("\\s+", " ").matches("main\\s*\\(.*\\)"))
            throw new RuntimeException("function main is not allowed in TestMain.cpp");
        findUnitTests();
        setVplEvaluateCases();
        setAllTestsAndMain();
        setTestMain();
    }

    private void findUnitTests() {
        total = 0;
        Pattern pattern = Pattern.compile("string\\s+test(.*)\\((.*)\\)");
        Matcher matcher = pattern.matcher(contentWithoutBloc);
        while (matcher.find()) {
            Double grade = getDouble(matcher.group(2));
            testUnitaireGrade.put(matcher.group(1), grade);
            if (grade != null)
                total += grade;
        }
    }

    private Double getDouble(String group) {
        if (group == null)
            return null;
        String[] varValue = group.split("=");
        if (varValue.length == 2)
            try {
                return Double.parseDouble(varValue[1].trim());
            } catch (NumberFormatException ex) {
                return null;
            }
        return null;
    }

    private void setVplEvaluateCases() {
        StringBuilder stringBuilder = new StringBuilder();
        for(String id : testUnitaireGrade.keySet()) {
            stringBuilder.append("case = ").append(id).append('\n');
            Double grade = testUnitaireGrade.get(id);
            if (grade != null) {
                stringBuilder.append("grade reduction = ").append((grade / total)*noteMaxi).append('\n');
            }
            stringBuilder.append("input = ").append(id).append('\n');
            stringBuilder.append("output = OK").append('\n').append('\n');
        }
        vplEvaluateCases = stringBuilder.toString();
    }

    private void setAllTestsAndMain() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("int main(int argc, char** argv) {").append('\n');
        stringBuilder.append('\t').append("killer(\"Trop long : probablement une boucle infinie\", 5);").append('\n');
        stringBuilder.append('\t').append("string choix;").append('\n');
        stringBuilder.append('\t').append("cin >> choix;").append('\n');

        String[] ids = testUnitaireGrade.keySet().toArray(new String[0]);
        if (ids.length > 0) {
            stringBuilder.append('\t').append("if (choix == ").append('"').append(ids[0]).append('"').append(") {").append('\n');
            stringBuilder.append('\t').append('\t').append("cout << test").append(ids[0]).append("();").append('\n');
            for (int i = 1; i < ids.length; ++i) {

                stringBuilder.append('\t').append("} else if (choix == ").append('"').append(ids[i]).append('"').append(") {").append('\n');
                stringBuilder.append('\t').append('\t').append("cout << test").append(ids[i]).append("();").append('\n');
            }
            stringBuilder.append('\t').append("} else {").append('\n');
            stringBuilder.append('\t').append('\t').append("cout << \"KO\";").append('\n');
            stringBuilder.append('\t').append("}").append('\n');
        } else {
            stringBuilder.append('\t').append("cout << \"KO\";").append('\n');
        }
        stringBuilder.append("}").append('\n');

        allTestsAndMain = stringBuilder.toString();
    }

    public String getVplEvaluateCases() {
        return vplEvaluateCases;
    }

    private int skipBloc(String code, int p) {
        int nestedRank = 0;
        if (code.charAt(p++) == '{')
            ++nestedRank;

        while (nestedRank > 0) {
            int c = code.charAt(p++);
            if (c == '}')
                --nestedRank;
            else if (c == '{')
                ++nestedRank;
        }
        return p;
    }

    private String removeBlocs(String code) throws IOException {

        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < code.length(); ++i) {
            if (code.charAt(i) == '{')
                i = skipBloc(code, i);
            stringBuilder.append(code.charAt(i));
        }

        return stringBuilder.toString();
    }

    private void readFile() throws IOException {
        StringBuilder sbcwc = new StringBuilder();
        StringBuilder sbtotal = new StringBuilder();
        try (BufferedReader in = new BufferedReader(new FileReader(unitTestsFile))) {
            int c;
            while ((c = in.read()) != -1) {
                if (c == '/') {
                    String line = in.readLine();
                    sbtotal.append((char) c).append(line).append('\n');
                    if (line.length() == 0 || line.charAt(0) != '/') {
                        sbcwc.append((char) c).append(line).append('\n');
                    }
                } else {
                    sbtotal.append((char) c);
                    sbcwc.append((char) c);
                }
            }
        }
        allContent = sbtotal.toString();
        contentWithoutComment = sbcwc.toString();
    }

    public String getAllContent() {
        return allContent;
    }

    public String getAllTestsAndMain() {
        return allTestsAndMain;
    }

    public String getTestMain() {
        return testMain;
    }

    public Map<String, Double> getTestUnitaireGrade() {
        return testUnitaireGrade;
    }

    private void setTestMain() {
        StringBuilder stringBuilder = new StringBuilder(getAllContent());
        stringBuilder.append(getAllTestsAndMain());
        testMain = stringBuilder.toString();
    }

    public static void main(String[] args) throws IOException {
        UnitTestsAnalyzer unitTestsAnalyzer = new UnitTestsAnalyzer(new File("/home/yvan/tmp/c++/exo0/unittests/TestMain.cpp"));
        System.out.println(unitTestsAnalyzer.getTestMain());
        System.out.println("***************");
        System.out.println(unitTestsAnalyzer.getVplEvaluateCases());
    }
}
