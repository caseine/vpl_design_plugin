package caseine.generators;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Scanner;

/**
 * Reads a special header or source cpp file and prepare it to be written by 3 ways in cf, rf and ef caseine paths
 * <p>
 * Such a file may contain 2 kinds of patterns : PATTERN_REMOVE ("//#-#") and PATTERN_COPYING ("//#=#")
 * <p>
 * An area to remove is limited by 2 PATTERN_REMOVE, the first at its beginning and the second at its ending.
 * A PATTERN_COPYING may be existed or not. If yes, it should be between a beginning PATTERN_REMOVE and an ending
 * PATTERN_REMOVE.
 * <p>
 * Ef (execution files)
 * <p>
 * In ef, the whole file is written, and it is nested in the ref namespace.
 * <p>
 * Rf (required files)
 * <p>
 * In rf, the patterns and the area between the beginning PATTERN_REMOVE and PATTERN_COPYING (or the ending
 * PATTERN_REMOVE, if PATTERN_COPYING is missing) are removed.
 * <p>
 * Cf (corrected files)
 * <p>
 * In cf, the patterns and the area between PATTERN_COPYING et the ending PATTERN_REMOVE are removed. If PATTERN_COPYING
 * is missing, only the patterns are removed.
 * <p>
 * These contents are available by the methods getRf(), getEf() and getCf()
 */
public class CfRfEfFileGenerator {
    private File headerOrSourceFile;
    private String PATTERN_REMOVE = "//#-#";
    private String PATTERN_COPYING = "//#=#";
    private boolean removing = false;
    private boolean copying = false;

    private StringBuilder rf = new StringBuilder();
    private StringBuilder cf = new StringBuilder();
    private StringBuilder ef = new StringBuilder();
    private int indexOfNamespaceRef = 0;

    /**
     * Create a CfRfEfFileGenerator in order to generate ef, rf and cf files from headerOrSourceFile
     *
     * @param headerOrSourceFile the file whose images you want in cf, rf and ef.
     * @throws IOException if headerOrSourceFile does not exist or is unreadable
     */
    public CfRfEfFileGenerator(File headerOrSourceFile) throws IOException {
        this.headerOrSourceFile = headerOrSourceFile;
        analyse();
    }

    public static void main(String[] args) throws IOException {
        CfRfEfFileGenerator cfRfEfFileGenerator = new CfRfEfFileGenerator(new File("utilities.cpp"));
        System.out.println("---------CF--------------------");
        System.out.println(cfRfEfFileGenerator.getCf());
        System.out.println("---------RF--------------------");
        System.out.println(cfRfEfFileGenerator.getRf());
        System.out.println("---------EF--------------------");
        System.out.println(cfRfEfFileGenerator.getEf());
    }

    private String removeOrCopy(Scanner in, String line) {
        if (!line.replaceAll("[\\s]+", "").startsWith(PATTERN_REMOVE)) {
            ef.append('\t').append(line).append('\n');
            rf.append(line).append('\n');
            cf.append(line).append('\n');
            line = in.nextLine();
        }
        while (in.hasNextLine()) {
            if (line.trim().startsWith("#endif"))
                // #endif marque la fin dans tous les cas
                return line;
            else if (line.trim().startsWith("#"))
                // Les #include et autres # doivent figurer hors du namespace
                ef.insert(indexOfNamespaceRef, line + "\n");
            else
                ef.append('\t').append(line).append('\n');
            if (line.replaceAll("[\\s]+", "").startsWith(PATTERN_REMOVE)) {
                removing = !removing;
                if (!removing) {
                    copying = false;
                }
            } else if (line.replaceAll("[\\s]+", "").startsWith(PATTERN_COPYING)) {
                copying = true;
            } else {
                if (!removing) {
                    rf.append(line).append('\n');
                    cf.append(line).append('\n');
                } else {
                    if (copying) {
                        rf.append(line.replaceFirst("\\/\\/\\s*", "")).append('\n');
                    } else {
                        cf.append(line).append('\n');
                    }
                }
            }
            line = in.nextLine();
        }
        return line;
    }

    private void analyse() throws FileNotFoundException {
        try (Scanner in = new Scanner(this.headerOrSourceFile)) {
            String line = "";
            // Passage et copie des lignes vides, des directives du préprocesseur (#...), des commentaires qui
            // commencent par // et des commentaires entre /* */ (sauf que pour l'instant, il faut que /* débute une
            // ligne et */ en termine une.
            while (in.hasNextLine() && !line.replaceAll("[\\s]+", "").startsWith(PATTERN_REMOVE)
                    && ("".equalsIgnoreCase(line.trim()) || line.trim().startsWith("#")
                    || line.trim().startsWith("//")
                    || line.trim().startsWith("/*"))) {
                if (line.trim().startsWith("/*")) {
                    while (!line.endsWith("*/")) {
                        ef.append(line).append('\n');
                        rf.append(line).append('\n');
                        cf.append(line).append('\n');
                        line = in.nextLine();
                    }
                }
                if (!line.trim().replaceAll("\s*", "").toUpperCase().startsWith("//#NOREF#")) {
                    ef.append(line).append('\n');
                    rf.append(line).append('\n');
                    cf.append(line).append('\n');
                }
                line = in.nextLine();
            }
            // Il peut y avoir encore des directives de compilation après le #-#, qu'il ne faut pas mettre dans
            // namespace ref {

            //removeOrCopy(in, line);

            // Tout ce qui suit devrait pouvoir se trouver dans namespace ref {
            indexOfNamespaceRef = ef.length();
            ef.append("namespace ref {").append('\n');


            line = removeOrCopy(in, line);

            /*if (!line.replaceAll("[\\s]+", "").startsWith(PATTERN_REMOVE)) {
                ef.append('\t').append(line).append('\n');
                rf.append(line).append('\n');
                cf.append(line).append('\n');
                line = in.nextLine();
            }
            while (in.hasNextLine()) {

                ef.append('\t').append(line).append('\n');
                if (line.replaceAll("[\\s]+", "").startsWith(PATTERN_REMOVE)) {
                    removing = !removing;
                    if (!removing) {
                        copying = false;
                    }
                } else if (line.replaceAll("[\\s]+", "").startsWith(PATTERN_COPYING)) {
                    copying = true;
                } else {
                    if (!removing) {
                        rf.append(line).append('\n');
                        cf.append(line).append('\n');
                    } else {
                        if (copying) {
                            rf.append(line.replaceFirst("\\/\\/\\s*", "")).append('\n');
                        } else {
                            cf.append(line).append('\n');
                        }
                    }
                }
                line = in.nextLine();
            }*/
            ef.append("}").append('\n');
            if (!line.replaceAll("[\\s]+", "").startsWith(PATTERN_REMOVE)) {
                rf.append(line).append('\n');
                cf.append(line).append('\n');
                ef.append(line).append('\n');
            }
            String last = "";

            while (in.hasNextByte()) {
                last += (char)in.nextByte();
            }
            if (!last.replaceAll("[\\s]+", "").startsWith(PATTERN_REMOVE)) {

                rf.append(last);
                cf.append(last);
                ef.append(last);
            }
        }

    }

    public String getRf() {
        return rf.toString();
    }

    public String getCf() {
        return cf.toString();
    }

    public String getEf() {
        return ef.toString();
    }
}
