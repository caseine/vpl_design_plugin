package caseine.generators;

import caseine.exceptions.WrongNameException;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TestMainCppAnalyzer {
    private File testMainCpp;
    private Set<String> headers = new LinkedHashSet<>();
    private Set<String> usings = new LinkedHashSet<>();
    private Map<String, String> sign2content = new LinkedHashMap<>();
    private Map<String, String> input2call = new LinkedHashMap<>();
    private String main;

    public TestMainCppAnalyzer(File testMainCpp) throws WrongNameException, FileNotFoundException, NoSuchElementException {
        this.testMainCpp = testMainCpp;
        if (!testMainCpp.getName().startsWith("TestMain.c")) {
            throw new WrongNameException("The file name analyzed by TestMainCppAnalyzer should begin by TestMain.c");
        }

        if (!testMainCpp.exists()) {
            throw new FileNotFoundException(testMainCpp.getName() + " should exist");
        }
        analyze();
    }

    private void analyze() throws FileNotFoundException, NoSuchElementException {
        String line;
        try (Scanner in = new Scanner(testMainCpp)) {
            if (in.hasNextLine()) {
                line = in.nextLine().trim();
                // Skip all empty lines, all comments et all #something
                while (line.isEmpty() || line.startsWith("//") || line.startsWith("#")) {
                    // A line beginning by # is an header which is stored
                    if (line.startsWith("#")) {
                        headers.add(line);
                    }
                    line = in.nextLine().trim();
                }

                // Skip all empty lines, all comments et all using somthing
                while (line.isEmpty() || line.startsWith("//") || line.startsWith("using")) {
                    // A line beginning by using is an "using thing" which is stored
                    if (line.startsWith("using")) {
                        usings.add(line);
                    }
                    line = in.nextLine().trim();
                }
                // All theses things should be at the beginning of TestMain

                // Finding void allTests() (but that means that void allTests() should begin a new line
                while (!line.trim().startsWith("void allTests()")) {

                    while (!line.trim().startsWith("string test") && !line.trim().startsWith("void allTests()")) {
                        line = in.nextLine();
                    }
                    if (line.trim().startsWith("string test")) {
                        String key = line;
                        line = in.nextLine();
                        StringBuilder sb = new StringBuilder();
                        while (!line.trim().startsWith("string test") && !line.trim().startsWith("void allTests()")) {
                            if (!line.isEmpty())
                                sb.append(line).append('\n');
                            line = in.nextLine();
                        }
                        sign2content.put(key, sb.toString());
                    }
                }

                Pattern patternInput = Pattern.compile("\"(.*)\"");
                Pattern patternCase = Pattern.compile("test(.*)\\(");
                String key = null;

                while (in.hasNextLine()) {
                    line = in.nextLine().trim();
                    if (line.contains("if")) {
                        Matcher matcher = patternInput.matcher(line);
                        if (matcher.find()) {
                            key = matcher.group(1);

                            do {
                                line = in.nextLine();
                            } while (!line.contains("cout"));
                            matcher = patternCase.matcher(line);
                            if (matcher.find()) {
                                input2call.put(key, matcher.group(1));
                            }
                        }
                    }
                }

            }
        }
    }

    public Set<String> getHeaders() {
        return headers;
    }

    public Set<String> getUsings() {
        return usings;
    }

    public Map<String, String> getSign2content() {
        return sign2content;
    }

    public Map<String, String> getInput2call() {
        return input2call;
    }

    public String toString() {

        StringBuilder sb = new StringBuilder();

        for (String imp : headers) {
            sb.append(imp).append('\n');
        }
        sb.append('\n');
        for (String us : usings) {
            sb.append(us).append('\n');
        }
        sb.append('\n');
        for (String entete : sign2content.keySet()) {
            sb.append(entete).append('\n').append(' ').append(sign2content.get(entete)).append('\n');
        }
        sb.append('\n');
        sb.append("void allTests() {").append('\n');
        sb.append("\tstring choix;").append('\n');
        sb.append("\tcin >> choix;").append('\n');
        sb.append('\n');

        if (!input2call.isEmpty()) {
            sb.append('\t');

            for (String choice : input2call.keySet()) {
                sb.append(String.format("if (choix == \"%s\") {", choice)).append('\n');
                sb.append(String.format("\t\tcout << test%s();", input2call.get(choice))).append('\n');
                sb.append('\n');
                sb.append("\t} else ");
            }
            sb.append("{").append('\n');
            sb.append("\t\tcout << \"KO\";").append('\n');
            sb.append("\t}").append('\n');
            sb.append("}").append('\n');
        }

        sb.append('\n');


        return sb.toString();
    }

    public static void main(String[] args) throws WrongNameException, FileNotFoundException {
        //TestMainCppAnalyzer testMainCppAnalyzer = new TestMainCppAnalyzer(new File("TestMain.cpp"));
        //System.out.println(testMainCppAnalyzer);
    }
}
