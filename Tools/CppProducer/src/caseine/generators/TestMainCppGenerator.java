package caseine.generators;

import caseine.Writers;
import caseine.cases.Case;
import caseine.cases.VplEvaluateCases;
import caseine.exceptions.WrongNameException;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

/**
 * Generates a basis of TestMain.cpp file that has to be completed by the user.
 */
public class TestMainCppGenerator {

    private Set<String> headers = new LinkedHashSet<>();
    private Set<String> usings = new LinkedHashSet<>();
    private Map<String, String> sign2content = new LinkedHashMap<>();
    private Map<String, String> input2call = new LinkedHashMap<>();
    private String main;
    private File testMainCppFile;

    public TestMainCppGenerator(File root, List<File> headers, VplEvaluateCases vplEvaluateCases) {
        testMainCppFile = new File(root, "TestMain.cpp");
        this.headers.add("#include \"cppproducerutilities.h\"");
        this.headers.add("#include \"linkcincout.h\"");
        for (File header : headers) {

            this.headers.add(String.format("#include \"%s\"", header.getName()));
        }

        usings.add("using namespace std;");

        for (Case c : vplEvaluateCases) {
            sign2content.put(
                    String.format("string test%s() {", c.getTheCase().replaceAll("[\\s]+", "")),
                    "\t// ToDo Write a test\n" +
                            "\treturn \"KO\";\n" +
                            "}\n"
            );
        }

        for (Case c : vplEvaluateCases) {
            input2call.put(c.getTheInput(), c.getTheCase().replaceAll("[\\s]+", ""));
        }

        main = "int main(int argc, char** argv) {\n" +
                "\tkiller(\"Trop long : probablement une boucle infinie\", 5);\n" +
                "\tallTests();\n" +
                "}\n";

        try {
            TestMainCppAnalyzer testMainCppAnalyzer = new TestMainCppAnalyzer(new File(root, "TestMain.cpp"));
            usings.addAll(testMainCppAnalyzer.getUsings());
            this.headers.addAll(testMainCppAnalyzer.getHeaders());
            input2call.putAll(testMainCppAnalyzer.getInput2call());
            sign2content.putAll(testMainCppAnalyzer.getSign2content());
        } catch (WrongNameException | FileNotFoundException e) {

        }
    }

    private void writeFinderFunction(StringBuilder sb) {

        try (
                Scanner in = new Scanner(TestMainCppAnalyzer.class.getResourceAsStream("/finder.cpp"));
        ) {
            while (in.hasNextLine()) {
                sb.append(in.nextLine()).append('\n');
            }
        }
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();

        for (String imp : headers) {
            sb.append(imp).append('\n');
        }
        sb.append('\n');
        for (String us : usings) {
            sb.append(us).append('\n');
        }
        sb.append('\n');

        // writeFinderFunction(sb);

        for (String entete : sign2content.keySet()) {
            sb.append(entete).append('\n').append(' ').append(sign2content.get(entete)).append('\n');
        }
        sb.append('\n');

        sb.append("void allTests() {").append('\n');
        sb.append("\tstring choix;").append('\n');
        sb.append("\tcin >> choix;").append('\n');
        sb.append('\n');

        if (!input2call.isEmpty()) {
            sb.append('\t');

            for (String choice : input2call.keySet()) {
                sb.append(String.format("if (choix == \"%s\") {", choice)).append('\n');
                sb.append(String.format("\t\tcout << test%s();", input2call.get(choice))).append('\n');
                sb.append('\n');
                sb.append("\t} else ");
            }
            sb.append("{").append('\n');
            sb.append("\t\tcout << \"KO\";").append('\n');
            sb.append("\t}").append('\n');
            sb.append("}").append('\n');
        }

        sb.append('\n');
        sb.append(main).append('\n');


        return sb.toString();
    }
}
