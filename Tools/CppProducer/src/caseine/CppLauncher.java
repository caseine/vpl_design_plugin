/*
 * Creative commons CC BY-NC-SA 2020 Yvan Maillot <yvan.maillot@uha.fr>
 *
 *     Share - You can copy and redistribute the material in any medium or format
 *
 *     Adapt - You can remix, transform, and build upon the material
 *
 * Under the following terms :
 *
 *     Attribution - You must give appropriate credit, provide a link to the license,
 *     and indicate if changes were made. You may do so in any reasonable manner,
 *     but not in any way that suggests the licensor endorses you or your use.
 *
 *     NonCommercial — You may not use the material for commercial purposes.
 *
 *     ShareAlike — If you remix, transform, or build upon the material,
 *     you must distribute your contributions under the same license as the original.
 *
 * Notices:    You do not have to comply with the license for elements of
 *             the material in the public domain or where your use is permitted
 *             by an applicable exception or limitation.
 *
 * No warranties are given. The license may not give you all of the permissions
 * necessary for your intended use. For example, other rights such as publicity,
 * privacy, or moral rights may limit how you use the material.
 *
 * See <https://creativecommons.org/licenses/by-nc-sa/4.0/>.
 */
package caseine;

import caseine.exceptions.MissingVplEvaluateCaseFileException;
import caseine.exceptions.RootFileNotEmptyException;

import java.io.File;
import java.io.IOException;

/*
 * @author Yvan Maillot <yvan.maillot@uha.fr>
 */
public class CppLauncher {
    public static void launch(String file, int n) throws IOException, RootFileNotEmptyException, MissingVplEvaluateCaseFileException {
        File root = new File(file);
        if (!root.exists())
            root.mkdir();
        new PathReader(root, n);
        System.out.println("A template cpp project was created at " + file);
    }

    public static void launch(String file) throws IOException, MissingVplEvaluateCaseFileException, RootFileNotEmptyException {
        File root = new File(file);
        if (!root.exists())
            new PathReader(root, 0);
        else
            new PathReader(root);
    }
}
