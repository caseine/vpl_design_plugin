package caseine.exceptions;

public class RootFileNotEmptyException extends Exception {
    public RootFileNotEmptyException(String msg) {
        super(msg);
    }
}
