package caseine.exceptions;

public class WrongNameException extends Exception {
    public WrongNameException(String msg) {
        super(msg);
    }
}
