package caseine.exceptions;

public class MissingVplEvaluateCaseFileException extends Exception {
    public MissingVplEvaluateCaseFileException(String msg) {
        super(msg);
    }
}
