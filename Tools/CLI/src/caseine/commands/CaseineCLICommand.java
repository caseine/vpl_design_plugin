package caseine.commands;
/*
 * Copyright 2019: Christophe Saint-Marcel
 * This software is part of the Caseine project.
 * This software was developped with the support of the following organizations:
 * Université Grenoble Alpes
 * Institut Polytechnique de Grenoble
 *
 *
 * This file is part of the VPL Design Tool.
 * The VPL Design Tool is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The VPL Design Tool is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the VPL Design Tool.  If not, see <https://www.gnu.org/licenses/>.
 */

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.concurrent.Callable;
import java.util.stream.Collectors;

import caseine.CaseineCommand;
import caseine.CaseineProperties;
import caseine.IDE;
import caseine.LANG;
import caseine.exceptions.TestDirectoryMissingException;
import caseine.exceptions.UnitTestsFileMissingException;
import caseine.project.*;
import picocli.CommandLine;
import picocli.CommandLine.Command;
import picocli.CommandLine.IVersionProvider;
import picocli.CommandLine.Option;
import picocli.jansi.graalvm.AnsiConsole;
import vplwsclient.exception.MoodleWebServiceException;
import vplwsclient.exception.VplConnectionException;
import vplwsclient.exception.VplException;


/**
 * {@link IVersionProvider} implementation that returns version information from a {@code /version.txt} file in the classpath.
 */
class PropertiesVersionProvider implements IVersionProvider {
    public String[] getVersion() throws Exception {
        URL url = getClass().getResource("/version.properties");
        if (url == null) {
            return new String[]{"No version.properties file found in the classpath"};
        }
        CaseineProperties properties = new CaseineProperties();
        properties.load(url.openStream());
        return new String[]{
                properties.getProperty("caseine.version")
        };
    }
}

@Command(name = "caseine", mixinStandardHelpOptions = true, versionProvider = PropertiesVersionProvider.class, description = "Caseine commands to manage your VPLs")
public class CaseineCLICommand implements Callable<Integer> {
    private static String command = "caseine";

    private static final String VPLID_0 = "0";

    private static final String CASEINE_VPLID = "CASEINE_VPLID";

    private static final String CASEINE_TOKEN = "CASEINE_TOKEN";

    @Option(names = {"-p", "--path"}, paramLabel = "project_path", description = "The project path (current directory by default).")
    private String projectPath = ".";

    @Option(names = "--gen", description = "Generates a template for your lab. Should be used with --lang")
    private boolean generate = false;

    @Option(names = "--doc", description = "Push the documentation of the lab. Note that when using --push with or without --doc, the documentation is pushed if it exists or pulled if not.")
    private boolean doc = false;

    @Option(names = {"-l", "--lang"}, paramLabel = "PROJECT_LANGUAGE", description = "Generates template for your language [java, python, cpp, no-specific-language]. Should be used with --gen. no-specific-language is not by default to be sure")
    private String lang = null;

    @Option(names = {"-i", "--ide"}, paramLabel = "PROJECT_IDE", description = "Generates template for your IDE [eclipse].")
    private String ide = null;

    @Option(names = {"--mvn"}, description = "Generates maven nature.")
    private boolean mvn = false;

    @Option(names = "--local", description = "Generates locally the Caseine stuff for your lab.")
    private boolean local = false;

    @Option(names = "--push", description = "Pushes the lab to the server. By default, settings are pushed too. But you can use it with --no-settings to avoid pushing settings.")
    private boolean push = false;

    @Option(names = {"-v", "--vplId"}, paramLabel = "CASEINE_VPLID", description = "The Virtual Lab Identifier (required with --push only the first time or after --clean).", defaultValue = VPLID_0)
    private String vplId;

    @Option(names = {"-u", "--url"}, paramLabel = "CASEINE_URL", description = "The caseine server, default server is https://moodle.caseine.org/.", defaultValue = "https://moodle.caseine.org/webservice/rest/server.php")
    private String url;

    @Option(names = {"-ns", "--no-settings"}, description = "Do not push settings to the server when using --push. Otherwise, settings are pushed by default when using --push")
    boolean noSettings = false;

    @Option(names = {"-s", "--settings"}, description = "Push settings and only settings to the server. Cannot be used with --push.")
    boolean settings = false;

    @Option(names = {"-t", "--token"}, paramLabel = "CASEINE_TOKEN", description = "Your token to authenticate you to the plateform (required with --push unless environment variable CASEINE_TOKEN is set)")
    private String token = null;

    @Option(names = "--clean", description = "Cleans locally the Caseine stuff for your lab.")
    private boolean clean = false;

    @Option(names = {"--no-local", "-nl"}, description = "Do not (re)generate the local files when using --push. For instance if you have change something under target directory. Should be used with --push")
    private boolean noLocal = false;

    @Option(names = "-java-template-1", description = "Generation of a java template with 1 class in 1 file and no test.", defaultValue = "false")
    private boolean java_template_1;

    @Option(names = "-java-template-2", description = "Generation of a java template with a class Point, most of tags and a JUnit test.", defaultValue = "false")
    private boolean java_template_2;

    @Option(names = "-java-template-3", description = "Generation of a quite big java template to show what we can do", defaultValue = "false")
    private boolean java_template_3;

    @Option(names = "-cpp-template-1", description = "Generation of a cpp template with 5 functions using the parser and stdoutput in 1 file.", defaultValue = "false")
    private boolean cpp_template_1;

    @Option(names = "-cpp-template-2", description = "Generation of a cpp template with 5 functions using the parser and stdoutput in 3 files.", defaultValue = "false")
    private boolean cpp_template_2;

    @Option(names = "-cpp-template-3", description = "Generation of a cpp template with 5 functions with no signature. The student has to find them from link errors.", defaultValue = "false")
    private boolean cpp_template_3;

    @Option(names = "-python-template-1", description = "Generation of a python template with 1 file.", defaultValue = "false")
    private boolean python_template_1;

    @Option(names = "-python-template-2", description = "Generation of a python template with 2 files.", defaultValue = "false")
    private boolean python_template_2;

    @Option(names = "-python-template-3", description = "Generation of a python template quite hard.", defaultValue = "false")
    private boolean python_template_3;

    @Option(names = {"-no-specific-language-template-1", "-nslt-1"}, description = "Generation of a simple prolog vpl.", defaultValue = "false")
    private boolean nolang_template_1;

    @Option(names = {"-no-specific-language-template-2", "-nslt-2"}, description = "Generation of a simple bash script vpl.", defaultValue = "false")
    private boolean nolang_template_2;

    @Option(names = {"-no-specific-language-template-3", "-nslt-3"}, description = "Generation of a simple sql vpl", defaultValue = "false")
    private boolean nolang_template_3;

    public CaseineCLICommand() {
        if (token == null) {
            // Try to get it in env
            token = System.getenv(CASEINE_TOKEN);
        }
    }

    @Override
    public Integer call() {
        // Check inputs, write errors and exit if needed
        checkInputs();
        int result = 0;
        if (clean) {
            result = clean();
        } else if (doc) {
            result = documentation(vplId);
        } else if (generate) {
            result = generate(url, token, vplId, lang, ide, mvn, templateNumber());
        } else if (local) {
            result = local(vplId);
        } else if (push) {
            result = push(noLocal);
        } else if (settings) {
            result = updateSettings(vplId);
        } else if (lang != null) {
            System.err.println("--lang should be used with --gen");
        } else {
            // new CommandLine(this).usage(System.out);
            System.err.println(command + " is a wrong command");
            System.err.println("Use caseine --help to see the available commands");
        }

        return result;
    }

    private int templateNumber() {
        if (java_template_1 || cpp_template_1 || python_template_1 || nolang_template_1) {
            return 1;
        }
        if (java_template_2 || cpp_template_2 || python_template_2 || nolang_template_2) {
            return 2;
        }
        if (java_template_3 || cpp_template_3 || python_template_3 || nolang_template_3) {
            return 3;
        }
        return 0;
    }


    private void checkInputs() {
        if (java_template_1 || java_template_2 || java_template_3) {
            lang = "java";
            generate = true;
        }
        if (cpp_template_1 || cpp_template_2 || cpp_template_3) {
            lang = "cpp";
            generate = true;
        }
        if (python_template_1 || python_template_2 || python_template_3) {
            lang = "python";
            generate = true;
        }
        if (nolang_template_1 || nolang_template_2 || nolang_template_3) {
            lang = "no-specific-language";
            generate = true;
        }
        if (mvn) {
            lang = "java";
            generate = true;
        }
        if (!new File(projectPath, ".caseine").exists() && !generate) {
            System.err.println("The project is not a caseine one. You should use --gen to generate a caseine project");
            System.exit(0);
        }
        int exclusivesParameters = checkExclusivesParameters();
        if (exclusivesParameters > 1) {
            System.err.println("You can't use -no-specific-language-template-x -java-template-x, -python-template-x, -cpp-template-x, --local, --push, --doc or --clean together");
            System.exit(0);
        }

        if (generate && lang == null) {
            System.err.println("--gen and --lang must be used together and --lang must be followed by one of: " + Arrays.stream(LANG.values()).map(LANG::getCommand).collect(Collectors.joining(" ")));
            System.exit(0);
        }

        if (ide != null && Arrays.stream(IDE.values()).map(x -> x.getCommand()).noneMatch(x -> x.equals(ide))) {
            System.err.print("--ide or i must be followed by one of: ");
            Arrays.stream(IDE.values()).forEach(c -> System.err.print(c.getCommand() + " "));
            System.err.println();
            System.exit(0);
        }

        if (generate && Arrays.stream(LANG.values()).map(x -> x.getCommand()).noneMatch(x -> x.equals(lang))) {
            System.err.println("Sorry, the language " + lang + " is not supported yet.");
            System.err.print("--lang or -l must be followed by one of: ");
            Arrays.stream(LANG.values()).forEach(c -> System.err.print(c.getCommand() + " "));
            System.err.println();
            System.exit(0);
        }
        if (noSettings && settings) {
            System.err.println("--settings and --no-settings cannot be used together");
            System.exit(0);
        }
        if (noSettings && !push) {
            System.err.println("--no-settings can only be used with --push");
            System.exit(0);
        }
    }

    private int checkExclusivesParameters() {
        int result = 0;
        boolean template = false;
        if (java_template_1) {
            result++;
            template = true;
        }
        if (java_template_2) {
            result++;
            template = true;
        }
        if (java_template_3) {
            result++;
            template = true;
        }
        if (cpp_template_1) {
            result++;
            template = true;
        }
        if (cpp_template_2) {
            result++;
            template = true;
        }
        if (cpp_template_3) {
            result++;
            template = true;
        }
        if (nolang_template_1) {
            result++;
            template = true;
        }
        if (nolang_template_2) {
            result++;
            template = true;
        }
        if (nolang_template_3) {
            result++;
            template = true;
        }
        if (local) {
            result++;
        }
        if (push) {
            result++;
        }
        if (doc) {
            result++;
        }
        if (clean) {
            result++;
        }
        if (!template && generate) {
            result++;
        }
        return result;
    }

    private Integer nature() {
        int result = -1;
        try {
            CaseineCommand.nature(Paths.get(projectPath).toAbsolutePath().toString(), vplId);
            result = 0;
        } catch (CaseineProjectAlreadyExistingException | IOException | NotACaseineProjectException e) {
            System.err.println(e.getMessage());
        }
        if (result == 0) {
            System.out.println("The project is now a caseine one");
        }
        return result;
    }

    private Integer documentation(String vplId) {
        int result = -1;
        try {
            CaseineCommand.documentation(Paths.get(projectPath).toAbsolutePath().toString(), vplId, url, token, noSettings);
            result = 0;
        } catch (VplConnectionException | MoodleWebServiceException | IOException | NotACaseineProjectException e) {
            System.err.println(e.getMessage());
        }
        return result;
    }


    private Integer updateSettings(String vplId) {
        int result = -1;
        if (!vplId.equals(VPLID_0)) {
	        try {
	            CaseineCommand.udpateSettings(Paths.get(projectPath).toAbsolutePath().toString(), vplId, url, token, noSettings);
	            result = 0;
	        } catch (VplConnectionException | MoodleWebServiceException | IOException | NotACaseineProjectException e) {
	            System.err.println(e.getMessage());
	        }
        }
        return result;
    }

    private Integer generate(String url, String token, String vplId, String lang, String ide, boolean mvn, int template) {
        try {
            CaseineCommand.generate(url, token, Paths.get(projectPath).toAbsolutePath().toString(), vplId, lang, ide, mvn, template, noSettings, noLocal);
            if (new File(projectPath).exists()) {
                System.out.println("The project has been successfully generated in " + projectPath);
                return 0;
            } else {
                System.err.println("The project has not been generated");
                return -1;
            }
        } catch (CaseineProjectAlreadyExistingException | IOException | BadIDEException | TestDirectoryMissingException
                 | FileMissingException | UnitTestsFileMissingException e) {
            System.err.println(e.getMessage());
            return -1;
        }
    }

    /**
     * Generates the caseine local files. The project will be compiled.
     *
     * @param vplId
     * @return 0 if ok
     */
    private Integer local(String vplId) {
        int result = -1;
        try {
            guardForMvnProjects();
            CaseineCommand.local(Paths.get(projectPath).toAbsolutePath().toString(), vplId, null);
            if (new File(projectPath + "/target/caseine-output").exists()) {
                result = 0;
            } else {
                result = -1;
            }
        } catch (IllegalArgumentException iae) {
            if (".".equals(projectPath)) {
                System.err.println(
                        "Local path does not seem to contain a caseine project, change root or apply the option --path");
            } else {
                System.err.println("Path \"" + projectPath + "\" does not seem to contain a caseine project");
            }
        } catch (IOException ioe) {
            System.err.println(ioe.getMessage());
        } catch (ClassNotFoundException cnfe) {
            System.err.println("Missing class: " + cnfe.getMessage());
            System.err.println(cnfe.getCause());
        } catch (NotACaseineProjectException | MavenProjectException e) {
            System.err.println(e.getMessage());
        }
        if (result == 0) {
            System.out.println("The template has been successfully generated in " + projectPath + "/target/caseine-output");
        } else {
            System.err.println("The template has not been generated in " + projectPath + "/target/caseine-output");
        }
        return result;
    }

    private Integer push() {
        return push(false);
    }

    private Integer push(boolean noLocal) {
        int result = -1;
        if (token == null) {
            System.err.println("Token must be set! Run with option --token or set environment variable CASEINE_TOKEN");
        } else {
            if (vplId.equals(VPLID_0)) {
                // Try to get it in env
                try {
                    vplId = System.getenv(CASEINE_VPLID);
                    if (vplId == null) {
                        vplId = VPLID_0;
                    } else {
                        int vplIdAsInt = Integer.parseInt(System.getenv(CASEINE_VPLID));
                        if (vplIdAsInt <= 0) {
                            System.err.println("VplId must be a positive integer!");
                        }
                    }
                } catch (NumberFormatException nfe) {
                    System.err.println("VplId must be an integer!");
                }
            }
            try {
                guardForMvnProjects();
                CaseineCommand.push(Paths.get(projectPath).toAbsolutePath().toString(), vplId, url, token, null, noSettings, noLocal);
                result = 0;
            } catch (IllegalArgumentException iae) {
                if ("".equals(projectPath)) {
                    System.err.println(
                            "Local path does not seem to contain a caseine project, change root or apply the option --path");
                } else {
                    System.err.println("Path \"" + projectPath + "\" does not seem to contain a caseine project");
                }
            } catch (IOException | MavenProjectException | NotACaseineProjectException | NothingPushedException e) {
                System.err.println(e.getMessage());
            } catch (ClassNotFoundException cnfe) {
                System.err.println("Missing class: " + cnfe.getMessage());
                System.err.println(cnfe.getCause());
            } catch (VplException e) {
                System.err.println(e.getMessage());
            } catch (VPLIDMissingException e) {
                System.err.println(
                        "VplId must be set! Run with option --vplId or set environment variable CASEINE_VPLID");
            }
            if (result == 0) {
                System.out.println("The project has been successfully pushed");
            }

        }
        return result;
    }

    private void guardForMvnProjects() throws MavenProjectException {
        String path = Paths.get(projectPath).toAbsolutePath().toString();
        File fPath = new File(path + File.separator + "pom.xml");
        if (fPath.exists()) {
            throw new MavenProjectException();
        }
    }

    private Integer clean() {
        int result = 0;
        try {
            CaseineCommand.clean(Paths.get(projectPath).toAbsolutePath().toString());
        } catch (IOException | NotACaseineProjectException e) {
            System.err.println(e.getMessage());
            result = -1;
        }
        if (result == 0) {
            System.out.println("The project has been successfully cleaned");
        }
        return result;
    }

    public static void main(String... args) {
        command = "caseine " + String.join(" ", args);
        int exitCode;
        try (AnsiConsole ansi = AnsiConsole.windowsInstall()) {
            exitCode = new CommandLine(new CaseineCLICommand()).execute(args);
        } catch (Exception e) {
            e.printStackTrace();
            exitCode = 1;
        }
        System.exit(exitCode);
    }

}