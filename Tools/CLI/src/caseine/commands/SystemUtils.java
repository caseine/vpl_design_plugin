package caseine.commands;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

public class SystemUtils {

	private static String OS = System.getProperty("os.name").toLowerCase();
	
	public static boolean isWindows() {

		return (OS.indexOf("win") >= 0);

	}

	public static boolean isMac() {

		return (OS.indexOf("mac") >= 0);

	}

	public static boolean isUnix() {

		return (OS.indexOf("nix") >= 0 || OS.indexOf("nux") >= 0 || OS.indexOf("aix") > 0 );
		
	}

	
	// from w w w . j a v a 2 s . c o m
	static public String sysExec(String command) {
		String result = new String();
		try {
			Runtime runtime = Runtime.getRuntime();
			Process proc = runtime.exec(command);
			/*
			 * Put a BufferedReader on the command output
			 */
			InputStream inputstream = proc.getInputStream();
			InputStreamReader inputstreamreader = new InputStreamReader(inputstream);
			BufferedReader bufferedreader = new BufferedReader(inputstreamreader);
			/*
			 * Read the command output
			 */
			String line;
			while ((line = bufferedreader.readLine()) != null) {
				result += line;
				result += "\n";
			}
			/*
			 * Check for command failure
			 */
			try {
				if (proc.waitFor() != 0) {
					System.err.println("exit value = " + proc.exitValue());
				}
			} catch (InterruptedException e) {
				System.err.println(e);
			}
		} catch (Throwable t) {
			t.printStackTrace();
		}
		return result;
	}

}
