/*
 * Copyright 2019: Christophe Saint-Marcel
 * This software is part of the Caseine project.
 * This software was developped with the support of the following organizations:
 * Université Grenoble Alpes
 * Institut Polytechnique de Grenoble
 * 
 * 
 * This file is part of the VPL Design Tool.
 * The VPL Design Tool is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * The VPL Design Tool is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with the VPL Design Tool.  If not, see <https://www.gnu.org/licenses/>.
 */
package caseine.goals;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/*
 * Copyright 2001-2005 The Apache Software Foundation.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.plugins.annotations.ResolutionScope;
import org.apache.maven.project.MavenProject;

import caseine.CaseineCommand;
import caseine.project.MavenProjectException;
import caseine.project.VPLIDMissingException;
import vplwsclient.exception.VplException;

/**
 * This command is ran when you use the ">mvn deploy" command
 * @author christophe
 *
 */
@Mojo(name = "push-moodle", requiresDependencyResolution = ResolutionScope.COMPILE)
public class PushMoodleMojo extends AbstractMojo {

    @Parameter(property = "url", defaultValue = "")
    private String url;

    @Parameter(property = "token", defaultValue = "")
    private String token;

    @Parameter(property = "vplid", defaultValue = "0")
    private String vplid;

    @Parameter(defaultValue = "${project.basedir}", readonly = true)
    private File basedir;
    
	@Parameter(defaultValue = "${project}", required = true, readonly = true)
	private MavenProject project;

	@Parameter(property = "no-settings", defaultValue = "false", required = false)
	private String noSettings;

	@Parameter(property = "no-local", defaultValue = "false", required = false)
	private boolean noLocal;

	private boolean isNoSettings() {
		return !noSettings.toLowerCase().equals("false");
	}

    public void execute() throws MojoExecutionException {

        getLog().info("Server: " + url + ", vplId: " + vplid);
        getLog().info("No Settings: " + noSettings);

        if (!vplid.equals("0")) {
        	URLClassLoader contextClassLoader = null;
            try {
    		    Set<URL> urls = new HashSet<>();
    		    List<String> elements = project.getCompileClasspathElements();
    		    for (String element : elements) {
    		        urls.add(new File(element).toURI().toURL());
    		    }
    		    contextClassLoader = URLClassLoader.newInstance(
    		    		urls.toArray(new URL[0]),
    		            CaseineCommand.class.getClassLoader());

    		    getLog().info("Location: " + basedir.getAbsolutePath());
                CaseineCommand.push(basedir.getAbsolutePath(), vplid, url, token, contextClassLoader, isNoSettings(), noLocal);
			} catch (IOException | VplException | MavenProjectException| VPLIDMissingException e) {
				getLog().error(e);
			} catch (ClassNotFoundException e) {
				getLog().error("No class found, try to compile the lab ...");
				getLog().error(e.getCause());
			} catch (Exception e) {
				getLog().warn(e.getMessage());
			} finally {
				if (contextClassLoader != null) {
					try {
						contextClassLoader.close();
					} catch (IOException e) {
						getLog().error(e.getMessage());
					}
				}
			}
       }
    }
}
