/*
 * Copyright 2021: Christophe Saint-Marcel
 * This software is part of the Caseine project.
 * This software was developped with the support of the following organizations:
 * Université Grenoble Alpes
 * Institut Polytechnique de Grenoble
 * 
 * 
 * This file is part of the VPL Design Tool.
 * The VPL Design Tool is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * The VPL Design Tool is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with the VPL Design Tool.  If not, see <https://www.gnu.org/licenses/>.
 */
package caseine.goals;

import java.io.File;
import java.io.IOException;

import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;

import caseine.mutants.Generate;

@Mojo(name = "mutate")
public class GenerateMojo extends AbstractMojo {

    @Parameter(property = "class", required = true)
    private String className;

    @Parameter(property = "test", required = false)
    private String testName;

    @Parameter(property = "nb", defaultValue = "10")
    private Integer numberOfMutants;

    @Parameter(defaultValue = "${project.basedir}", readonly = true)
	private File basedir;

	public void execute() throws MojoExecutionException {

		try {
			getLog().info("Location: " + basedir.getAbsolutePath());
			Generate.publish(basedir.getAbsolutePath(), className, testName, numberOfMutants);
		} catch (IOException e) {
			getLog().warn(e.getMessage());
		} catch (ClassNotFoundException e) {
			getLog().error(e.getMessage());
		}	
	}
}