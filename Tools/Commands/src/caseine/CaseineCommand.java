/*
 * Copyright 2019: Christophe Saint-Marcel
 * This software is part of the Caseine project.
 * This software was developped with the support of the following organizations:
 * Université Grenoble Alpes
 * Institut Polytechnique de Grenoble
 * 
 * 
 * This file is part of the VPL Design Tool.
 * The VPL Design Tool is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * The VPL Design Tool is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with the VPL Design Tool.  If not, see <https://www.gnu.org/licenses/>.
 */
package caseine;

import java.io.IOException;

import caseine.exceptions.TestDirectoryMissingException;
import caseine.exceptions.UnitTestsFileMissingException;
import caseine.project.*;
import vplwsclient.exception.MoodleWebServiceException;
import vplwsclient.exception.VplConnectionException;
import vplwsclient.exception.VplException;

/**
 * The caseine core command for the vpl design tool. It can be used to generate locally the caseine structure
 * and remotely the VPL files( Requested Files, Execution Files, Corrected Files).
 */
public class CaseineCommand {

	
	/**
	 * Generates the project template.
	 * @param url
	 * @param projectPath
	 * @param vplId
	 * @param lang
	 * @param ide
	 * @param mvn 
	 * @throws CaseineProjectAlreadyExistingException
	 * @throws IOException
	 * @throws BadIDEException 
	 * @throws UnitTestsFileMissingException 
	 * @throws FileMissingException 
	 * @throws TestDirectoryMissingException 
	 */
    public static void generate(String url, String token, String projectPath, String vplId, String lang, String ide, boolean mvn, int template, boolean settings, boolean noLocal) throws CaseineProjectAlreadyExistingException, IOException, BadIDEException, TestDirectoryMissingException, FileMissingException, UnitTestsFileMissingException {
    	
    	CaseineProject proj = null;
    	switch(lang) {
    		case "python":
    			proj = new CaseinePythonProject(projectPath, vplId, url, token, settings, noLocal);
    			break;
    		case "cpp":
    			proj = new CaseineCppProject(projectPath, vplId, url, token, settings, noLocal);
    			break;
    		case "java":
				proj = new CaseineJavaProject(projectPath, vplId, ide, url, token, settings, noLocal);
    			break;
    		default:
				proj = new CaseineCommonProject(projectPath, vplId, url, token, settings, noLocal);
    	}
    	proj.generate(mvn, template);
    }

	/**
	 * Generates the local templates.
	 * 
	 * @param projectPath the project location
	 * @param vplId
	 * @param cl an optional classloader to provide dependencies
	 * @throws IOException if ioException
	 * @throws ClassNotFoundException if the class is not found
	 * @throws NotACaseineProjectException 
	 * @throws MavenProjectException 
	 */
	public static void local(String projectPath, String vplId, ClassLoader cl) throws IOException, ClassNotFoundException, NotACaseineProjectException, MavenProjectException {

		CaseineProject proj = CaseineProjectLoader.getProject(projectPath, vplId, null, null, null, false, false);
	    
    	proj.local(cl);        	
 	}
	
	/**
	 * Publishes the templates to the remote caseine server.
	 * 
	 * @param projectPath the local path
	 * @param vplId the id of the VPL
	 * @param url optionnal url 
	 * @param token your token to be authenticated
	 * @param cl an optional classloader to provide dependencies
	 * @throws IOException if something wrong
	 * @throws ClassNotFoundException if something wrong
	 * @throws VplException if something wrong
	 * @throws VPLIDMissingException 
	 * @throws MavenProjectException 
	 * @throws NotACaseineProjectException 
	 * @throws NothingPushedException 
	 */
	public static void push(String projectPath, String vplId, String url, String token, ClassLoader cl, boolean noSettings, boolean noLocal)
			throws IOException, ClassNotFoundException, VplException, VPLIDMissingException, MavenProjectException, NotACaseineProjectException, NothingPushedException {
		
		CaseineProject proj = CaseineProjectLoader.getProject(projectPath, vplId, null, url, token, noSettings, noLocal);

		proj.push(cl);
	}

	/**
	 * Cleans the local templates.
	 * 
	 * @param projectPath the project location
	 * @throws IOException if an error occurs with the deleted files
	 * @throws NotACaseineProjectException 
	 */
	public static void clean(String projectPath) throws IOException, NotACaseineProjectException {
		
		CaseineProject proj = CaseineProjectLoader.getProject(projectPath, null, null, null, null, false, false);
		
		proj.clean();
	}

	public static void nature(String projectPath, String vplId) throws CaseineProjectAlreadyExistingException, IOException, NotACaseineProjectException {
		
		CaseineProject proj = CaseineProjectLoader.getProject(projectPath, vplId, null, null, null, false, false);
		
		proj.nature();		
	}

	public static void documentation(String projectPath, String vplId, String url, String token, boolean settings) throws VplConnectionException, MoodleWebServiceException, IOException, NotACaseineProjectException {

		CaseineProject proj = CaseineProjectLoader.getProject(projectPath, vplId, null, url, token, settings, false);
		
		proj.documentation();		
	}
	public static void udpateSettings(String projectPath, String vplId, String url, String token, boolean settings) throws VplConnectionException, MoodleWebServiceException, IOException, NotACaseineProjectException {

		CaseineProject proj = CaseineProjectLoader.getProject(projectPath, vplId, null, url, token, settings, false);

		proj.updateSettings();
	}
	
	/*
	 * @param projectPath the project location
	 * @return True if the Caseine project is generated locally
	 */
	public static boolean isGenerated(String projectPath) throws NotACaseineProjectException {
		
		CaseineProject proj = CaseineProjectLoader.getProject(projectPath, null, null, null, null, true, false);
		
		return proj.isGenerated();
	}

}
