package caseine.project;

public class BadIDEException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4108518785668130506L;

	public BadIDEException(String message) {
		super(message);
	}
}
