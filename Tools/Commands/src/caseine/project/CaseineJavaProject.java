/*
 * Copyright 2019: Christophe Saint-Marcel
 * This software is part of the Caseine project.
 * This software was developped with the support of the following organizations:
 * Université Grenoble Alpes
 * Institut Polytechnique de Grenoble
 * 
 * 
 * This file is part of the VPL Design Tool.
 * The VPL Design Tool is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * The VPL Design Tool is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with the VPL Design Tool.  If not, see <https://www.gnu.org/licenses/>.
 */
package caseine.project;

import java.io.*;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

import caseine.LANG;
import caseine.CaseineCommand;
import caseine.CaseineProperties;
import caseine.exceptions.TestDirectoryMissingException;
import caseine.exceptions.UnitTestsFileMissingException;
import caseine.publication.Publisher;
import vplwsclient.FileUtils;

import static caseine.FileUtils.copyAZipAndUnZipIt;

public class CaseineJavaProject extends CaseineCommonProject {

	private Properties defaultProperties = new Properties();

	private String ide;
	// To adjust the max files value
	private static int MULTIPLICATIVE_MAX_FILES_VALUE = 2;
	private static int ADDITIVE_MAX_FILES_VALUE = 10;

	/**
	 * 
	 * @param projectPath
	 * @param vplId
	 * @param ide
	 * @param url
	 * @param token
	 */
	public CaseineJavaProject(String projectPath, String vplId, String ide, String url, String token, boolean noSettings, boolean noLocal) {
		super(projectPath, vplId, url, token, noSettings, noLocal);
		this.ide = ide;


		defaultProperties.setProperty("CASEINE_VPL_TYPE", LANG.JAVA.name());
		defaultProperties.setProperty("CASEINE_TOKEN", "<env>");
		defaultProperties.setProperty("WS.run", "1");
		defaultProperties.setProperty("WS.evaluate", "1");
		defaultProperties.setProperty("WS.automaticgrading", "1");
	}

	/**
	 * Generates the project template.
	 * 
	 * @param mvn True if we must generate the maven pom.xml
	 * @param template
	 * 
	 * @throws CaseineProjectAlreadyExistingException
	 * @throws BadIDEException
	 * @throws IOException
	 */
	@Override
	public void generate(boolean mvn, int template) throws CaseineProjectAlreadyExistingException, BadIDEException,
			IOException, TestDirectoryMissingException, FileMissingException, UnitTestsFileMissingException {

		if (isCaseine()) {
			throw new CaseineProjectAlreadyExistingException("A project already exists in " + projectPath);
		}
		File root = new File(projectPath);
		File src = new File(projectPath + File.separator + PATH_SRC);
		if (!src.exists()) {
			src.mkdirs();
		}

		try {
			copyAZipAndUnZipIt(FileUtils.class.getResourceAsStream(String.format("/java/default/project%d.zip", template)), root, ".");
		} catch (Exception e) {
			System.err.println("Template not found");
			System.exit(1);
		}

		if (ide != null) {
			try {
				copyAZipAndUnZipIt(CaseineCommand.class.getResourceAsStream("/java/" + ide + "/project0.zip"),
						root, ".");
			} catch (Exception e) {
				caseine.FileUtils.clean(root.toPath());
				throw new BadIDEException("ide not supported: " + ide);
			}
		}

		fillCaseine(defaultProperties);
		//Fills the .caseine
		fillCaseine(vplId);
		// Fills the .project with the project name
		fillCaseineProjectFile();
		
		if (mvn) {
			String pom = projectPath + File.separatorChar + "pom.xml";
			caseine.FileUtils.writeAResource(CaseineCommand.class.getResourceAsStream("/java/mvn/pom.xml"),
					new File(pom));
			caseine.FileUtils.fillTemplate(pom, "${vpl.name}", getProjectName());
			String vplId = getVplId();
			if (vplId != null && !"<env>".equals(vplId)) {
				caseine.FileUtils.fillTemplate(pom, "${vpl.id}", vplId);				
			}
			// Finally, remove the caseine project file which is replaced by the pom.xml!
			if (!caseineFile.delete()) {
				log.severe(".caseine file not deleted");
			}
		} else {
			String libPathName = projectPath + File.separatorChar + "resources" + File.separatorChar + "lib";
			File libPath = new File(libPathName);
			if (!libPath.exists()) {
				libPath.mkdirs();
			}
			// Adds the plugin to the lib
			caseine.FileUtils.writeAResource(CaseineCommand.class.getResourceAsStream("/" + PLUGIN_LIB),
					new File(libPathName + File.separatorChar + PLUGIN_LIB));			
		}

	}

	/**
	 * Generates the local caseine templates into caseine-output.
	 * 
	 * @param cl an optional classloader to provide dependencies
	 * @throws IOException for IO issues
	 * @throws ClassNotFoundException if the class is not found
	 * @throws MavenProjectException if it is a maven nature of project
	 */
	public void local(ClassLoader cl) throws IOException, ClassNotFoundException, MavenProjectException {

		// Clean to be confident about the result
		clean();

		// Add existing libraries to the classpath
		List<File> jarFiles = FileUtils.listFiles(projectPath + PATH_RESOURCES_LIB);

		URL[] urls = new URL[jarFiles.size() + 1];
		for (int i = 0; i < jarFiles.size(); i++) {
			urls[i] = jarFiles.get(i).toURI().toURL();
		}

		urls[jarFiles.size()] = new File(projectPath + getPATH_BIN() + "classes").toURI().toURL();
		
		ClassLoader localCL = cl == null? CaseineCommand.class.getClassLoader() : cl;
		URLClassLoader ucl = new URLClassLoader(urls, localCL);

		Publisher pbsher = new Publisher(new File(projectPath + PATH_SRC).toPath(),
				new File(projectPath + PATH_CLASSES).toPath(),
				new File(projectPath + PATH_TEST).toPath(), new File(projectPath + PATH_RESOURCES_SRC).toPath(),
				new File(projectPath + PATH_RESOURCES_TEST).toPath(), new File(projectPath + getPATH_OUTPUT()).toPath(),
				ucl);


		try {
			pbsher.publishAll();
			fillDocCaseineFile(pbsher, projectPath);
		} catch (ClassNotFoundException cnfe) {
			// Clean to prevent push of a bad caseine-output at next time
			clean();
			throw cnfe;
		} catch (NullPointerException ex) {
			throw new NullPointerException("IN " + projectPath + "<-->");
		}

		// Copy the plugin as a lib (temporary solution ?!?)
		File libEFDirectory = new File(projectPath + getPATH_EXECUTION_FILES() + "lib");
		if (!libEFDirectory.exists()) {
			// Try in lib
		}
		
		libEFDirectory.mkdir();
		
		InputStream pluginStream = CaseineCommand.class.getResourceAsStream("/caseine.vpl.tools.plugin.jar");
		if (pluginStream != null) {
			caseine.FileUtils.writeAResource(pluginStream,
					Paths.get(libEFDirectory.getAbsolutePath(), PLUGIN_LIB).toFile());			
			
		} else {
			Files.copy(new FileInputStream(new File(projectPath + getPATH_BIN() + "lib" + File.separatorChar + PLUGIN_LIB)),
					Paths.get(libEFDirectory.getAbsolutePath(), PLUGIN_LIB));			
		}


		// Copy the optional libraries (under resources/lib)
		File libRFDirectory = new File(projectPath + getPATH_REQUESTED_FILES() + "lib");
		libRFDirectory.mkdir();
		File libResourceDirectory = new File(projectPath + PATH_RESOURCES_LIB);
		if (libResourceDirectory.exists()) {
			for (int i = 0; i < jarFiles.size(); i++) {
				if (jarFiles.get(i).getName().equals(PLUGIN_LIB)) {
					Files.copy(new FileInputStream(jarFiles.get(i)),
							Paths.get(libEFDirectory.getAbsolutePath(), jarFiles.get(i).getName()),
							StandardCopyOption.REPLACE_EXISTING);
				} else {
					Files.copy(new FileInputStream(jarFiles.get(i)),
							Paths.get(libRFDirectory.getAbsolutePath(), jarFiles.get(i).getName()), 
							StandardCopyOption.REPLACE_EXISTING);
				}
			}
		}

		File casesFile = new File(projectPath + getPATH_EXECUTION_FILES() + "vpl_evaluate.cases");
		
		if (!casesFile.createNewFile()) {
			log.severe("vpl_evaluate.cases cannot be created");
		}
		
		// List<String> dirs = new ArrayList<>();

		// listDirectory(new File(projectPath + PATH_EXECUTION_FILES), "", dirs);
		// Iterator<String> iter = dirs.iterator();
		Iterator<String> iter = pbsher.iterator();

		try (BufferedWriter writer = new BufferedWriter(new FileWriter(casesFile))) {
			writer.write("JunitFiles = ");
			while (iter.hasNext()) {
				String classes = iter.next();
				writer.write(classes + ", ");
			}
			writer.close();
		}

		for (Path p : pbsher.getPathToRemove()) {
			Path toRemove = new File(projectPath + getPATH_REQUESTED_FILES(), p.toString()).toPath();
			Files.deleteIfExists(toRemove);
		}
		String requestedTestPath = projectPath + getPATH_REQUESTED_FILES() + File.separator + "test";
		for (Path p : pbsher.getPathToTest()) {
			Path toTest = new File(projectPath + getPATH_REQUESTED_FILES(), p.toString()).toPath();
			Path toTest2 = new File(requestedTestPath, p.toString()).toPath();
			if (!toTest2.getParent().toFile().exists()) {
				toTest2.getParent().toFile().mkdirs();
			}
			Files.move(toTest, toTest2);
		}

		String correctedTestPath = projectPath + getPATH_CORRECTED_FILES() + File.separator + "test";
		for (Path p : pbsher.getPathToTest()) {
			Path toTest = new File(projectPath + getPATH_CORRECTED_FILES(), p.toString()).toPath();
			Path toTest2 = new File(correctedTestPath, p.toString()).toPath();
			if (!toTest2.getParent().toFile().exists()) {
				toTest2.getParent().toFile().mkdirs();
			}
			Files.move(toTest, toTest2);
		}

	}

	public static void fillDocCaseineFile(Publisher pbsher, String projectPath) throws IOException, ClassNotFoundException {
		CaseineProperties properties = new CaseineProperties();
		try (FileInputStream inputStream = new FileInputStream(new File(projectPath, ".caseine"))) {
			properties.load(inputStream);
			properties.setProperty("WS.grade", "%s".formatted(pbsher.getGrade()));
			properties.setProperty("WS.run", "1");
			properties.setProperty("WS.evaluate", "1");
			properties.setProperty("WS.basedon", (pbsher.isGraphical()?"19024":"864"));
			properties.setProperty("WS.automaticgrading", "1");
			properties.setProperty("WS.maxexetime", (pbsher.isGraphical()?"300":"32"));
			properties.setProperty("WS.maxfiles", "%s".formatted(pbsher.getMaxfiles()*MULTIPLICATIVE_MAX_FILES_VALUE+ADDITIVE_MAX_FILES_VALUE));
		} catch (Exception ex) {
			System.err.println("Unable to read .caseine");
		}
		try (OutputStream outputStream = new FileOutputStream(new File(projectPath, ".caseine"))) {
			properties.store(outputStream, dotCaseineHeaderComment);
		} catch (Exception ex) {
			System.err.println("Unable to write .caseine");
		}
	}
}
