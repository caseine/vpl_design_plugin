package caseine.project;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.List;
import java.util.Objects;
import java.util.Properties;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.json.JsonObject;

import caseine.NoTypeLauncher;
import caseine.LANG;
import org.apache.maven.model.Model;
import org.apache.maven.model.io.xpp3.MavenXpp3Reader;
import org.codehaus.plexus.util.xml.pull.XmlPullParserException;

import caseine.CaseineProperties;
import caseine.exceptions.TestDirectoryMissingException;
import caseine.exceptions.UnitTestsFileMissingException;
import vplwsclient.FileUtils;
import vplwsclient.RestJsonMoodleClient;
import vplwsclient.RestJsonMoodleClient.VPLService;
import vplwsclient.VplFile;
import vplwsclient.exception.MoodleWebServiceException;
import vplwsclient.exception.VplConnectionException;
import vplwsclient.exception.VplException;

import static caseine.FileUtils.copyAZipAndUnZipIt;

public class CaseineCommonProject implements CaseineProject {

	private Properties defaultProperties = new Properties();

    protected static final String dotCaseineHeaderComment = "# For settings, see https://moodle.caseine.org/mod/wiki/view.php?pageid=199\n" +
            "# Writable VPL settings are:\n" +
            "# - WS.name\n" +
            "# - WS.shortdescription\n" +
            "# - WS.intro\n" +
            "# - WS.introformat\n" +
            "# - WS.startdate\n" +
            "# - WS.duedate\n" +
            "# - WS.maxfiles\n" +
            "# - WS.maxfilesize\n" +
            "# - WS.requirednet\n" +
            "# - WS.password\n" +
            "# - WS.grade\n" +
            "# - WS.visiblegrade\n" +
            "# - WS.usevariations\n" +
            "# - WS.variationtitle\n" +
            "# - WS.usableasbase\n" +
            "# - WS.basedon\n" +
            "# - WS.run\n" +
            "# - WS.debug\n" +
            "# - WS.evaluate\n" +
            "# - WS.evaluateonsubmission\n" +
            "# - WS.automaticgrading\n" +
            "# - WS.usegradefrom\n" +
            "# - WS.maxexetime\n" +
            "# - WS.restrictededitor\n" +
            "# - WS.allowsubmissionviaeditor\n" +
            "# - WS.forbiddeneditors\n" +
            "# - WS.example\n" +
            "# - WS.maxexememory\n" +
            "# - WS.maxexefilesize\n" +
            "# - WS.maxexeprocesses\n" +
            "# - WS.jailservers\n" +
            "# - WS.worktype\n" +
            "# - WS.emailteachers\n" +
            "# - WS.timemodified\n" +
            "# - WS.freeevaluations\n" +
            "# - WS.reductionbyevaluation\n" +
            "# - WS.sebrequired\n" +
            "# - WS.sebkeys\n" +
            "# - WS.runscript\n" +
            "# - WS.debugscript\n" +
            "#\n" +
            "# Writable Plugin settings are:\n" +
            "# - FILE_PATTERNS : To define which root files will be scanned and pushed (e.g. FILE_PATTERNS=.*\\.pl)\n" +
            "# - TO_DEL_PATTERN : If a file has a string in the first line that matches this pattern, it will not be in rf  (e.g. TO_DEL_PATTERN=\\s*%DEL) \n" +
            "# - REMOVE_PATTERN : Indicates the beginning or end of the zone to be deleted (e.g. REMOVE_PATTERN=^\\s*%\\s*-\\s*%)\n" +
            "# - COPYING_PATTERN : Indicates the start of the replacement zone (e.g. COPYING_PATTERN=^\\s*%\\s*\\=\\s*%)\n" +
            "# - UNCOMMENT_PATTERN : Defines how to uncomment for replacement (e.g. UNCOMMENT_PATTERN=(^\\s*)%(.*)$)\n"
            ;


    protected static final String VPLID_0 = "0";

    public static String CASEINE_VPL_ID = "CASEINE_VPL_ID";
    public static String CASEINE_VPL_TYPE = "CASEINE_VPL_TYPE";

    protected String projectPath;

    protected String vplId = VPLID_0;

    protected File caseineFile;

    protected final static String INTERNAL_FILE = ".caseine";

    protected static Logger log = Logger.getLogger(CaseineJavaProject.class.getName());

    private String url;

    private String token;

    protected static final String RF = "rf";
    protected static final String EF = "ef";
    protected static final String CF = "cf";

    /**
     * By convention sources of a CaseInE project must be here.
     */
    protected static final String PATH_SRC = File.separator + "src";

    /**
     * By convention sources of a CaseInE project must be here.
     */
    protected static final String PATH_CLASSES = File.separator + "target" + File.separator + "classes";

    /**
     * By convention sources of a CaseInE project must be here.
     */
    protected static final String PATH_DOC = File.separator + "doc";

    /**
     * By convention binaries of a CaseInE project must be here.
     */
    protected static String PATH_BIN = File.separator + "target" + File.separator;

    /**
     * By convention test sources of a CaseInE project must be here.
     */
    protected static final String PATH_TEST = File.separator + "test";
    /**
     * By convention resources of a CaseInE project must be here.
     */
    protected static final String PATH_RESOURCES_SRC = File.separator + "resources" + File.separator + "src";
    /**
     * By convention test resources of a CaseInE project must be here.
     */
    protected static final String PATH_RESOURCES_TEST = File.separator + "resources" + File.separator + "test";
    /**
     * By convention lib resources of a CaseInE project must be here.
     */
    protected static final String PATH_RESOURCES_LIB = File.separator + "resources" + File.separator + "lib";

    /**
     * Put to false in extension classes if you do not want to run local before pushing
     */
    protected boolean noLocal = false;

    protected static final String PLUGIN_LIB = "caseine.vpl.tools.plugin.jar";

    protected boolean noSettings = false;


    public CaseineCommonProject(String projectPath, String vplId, String url, String token, boolean noSettings) {
        this(projectPath, vplId, url, token, noSettings, true);
    }

    public CaseineCommonProject(String projectPath, String vplId, String url, String token, boolean noSettings, boolean noLocal) {
        this.projectPath = projectPath;

		File root = new File(projectPath);
        caseineFile = new File(projectPath + File.separator + INTERNAL_FILE);

        this.vplId = vplId;

		if (root.exists()) {
	        if (this.vplId == null || this.vplId.equals(VPLID_0)) {
	            this.vplId = getVplId();
	        } else { // Update the caseine project file
	            fillCaseine(vplId);
	        }
		}
        
		this.url = url;
        this.token = token;
        this.noSettings = noSettings;
        this.noLocal = noLocal;

        defaultProperties.setProperty("CASEINE_VPL_TYPE", LANG.NO_SP_LANG.name());
        defaultProperties.setProperty("CASEINE_TOKEN", "<env>");
        defaultProperties.setProperty("WS.run", "1");
        defaultProperties.setProperty("WS.evaluate", "1");
        defaultProperties.setProperty("WS.automaticgrading", "1");
    }

    /**
     * @return True if it is a Caseine project, Maven projects are not managed
     */
    public boolean isCaseine() {
        boolean result = false;

        if (getType() == CaseineJavaProject.ProjectType.MAVEN) {
            result = true;
        }
        if (caseineFile.exists()) {
            result = true;
        }
        return result;
    }

    public ProjectType getType() {
        ProjectType result = ProjectType.NO_SP_LANG;
        File pom = new File(projectPath + File.separator + "pom.xml");
        if (pom.exists()) {
            result = ProjectType.MAVEN;
        }
        CaseineProperties props = getProperties();
        if (props.containsProperty(CASEINE_VPL_TYPE)) {
            String type = props.getProperty(CASEINE_VPL_TYPE);
            result = ProjectType.valueOf(type);
        }
        return result;
    }

    /**
     * @return all properties in the .caseine file
     */
    public CaseineProperties getProperties() {
        CaseineProperties props = new CaseineProperties();

        if (caseineFile.exists()) {

            try (FileInputStream fis = new FileInputStream(caseineFile)) {
                props.load(fis);
            } catch (Exception e) {
                log.severe(e.getMessage());
            }
        }
        return props;
    }

    public String getVplId() {
        String result = VPLID_0;
        if (caseineFile.exists()) {
            CaseineProperties props = getProperties();
            if (props.containsProperty(CASEINE_VPL_ID)) {
                result = props.getProperty(CASEINE_VPL_ID);
            }
        } else {
            if (getType() == CaseineJavaProject.ProjectType.MAVEN) {
                // Get it in the pom.xml
                File pom = new File(projectPath + File.separator + "pom.xml");
                MavenXpp3Reader reader = new MavenXpp3Reader();
                try {
                    Model model = reader.read(new FileReader(pom));
                    Properties properties = model.getProperties();
                    result = (String) properties.get(CASEINE_VPL_ID);
                } catch (IOException | XmlPullParserException e) {
                    // Nothing found
                }
            }
        }
        return result;
    }

    private String convertFormattedDateToLongAsString(String line) {
        try {
            Pattern compiledPattern = Pattern.compile("(\\d{1,2})\\D+(\\d{1,2})\\D+(\\d{4})\\D+(\\d{1,2})\\D+(\\d{1,2})");
            Matcher matcher = compiledPattern.matcher(line);
            if (matcher.find()) {
                int day = Integer.parseInt(matcher.group(1));
                int month = Integer.parseInt(matcher.group(2));
                int year = Integer.parseInt(matcher.group(3));
                int hour = Integer.parseInt(matcher.group(4));
                int minute = Integer.parseInt(matcher.group(5));
                return "" + convertDateToLong(day, month, year, hour, minute);
            } else {
                return line;
            }
        } catch (Exception e) {
            return line;
        }
    }

    private long convertDateToLong(int day, int month, int year, int hour, int minute) {
        LocalDateTime localDateTime = LocalDateTime.of(year, month, day, hour, minute, 0);

        return localDateTime.toEpochSecond(localDateTime.atZone(ZoneId.systemDefault()).getOffset());
    }

    public void fillCaseine(Properties propsToAppend) {
        CaseineProperties props = new CaseineProperties();
        if (caseineFile.exists()) {
            try (FileInputStream fis = new FileInputStream(caseineFile)) {
                props.load(fis);
            } catch (Exception e) {
                log.severe(e.getMessage());
            }
        } else {
        	try {
				caseineFile.createNewFile();
			} catch (IOException e) {
	            log.severe(e.getMessage());
			}
        }
        propsToAppend.forEach((key, value) -> props.setProperty((String) key, (String) value));

        try (FileOutputStream fos = new FileOutputStream(caseineFile)) {
            props.store(fos, dotCaseineHeaderComment);
        } catch (Exception e) {
            log.severe(e.getMessage());
        }
    }

    public void fillCaseine(String vplId) {
        if (!Objects.equals(vplId, VPLID_0)) {
            Properties props = new Properties();
            props.setProperty(CASEINE_VPL_ID, vplId);
            fillCaseine(props);
        }
    }

    @Override
    public void generate(boolean mvn, int template) throws CaseineProjectAlreadyExistingException, BadIDEException,
            IOException, TestDirectoryMissingException, FileMissingException, UnitTestsFileMissingException {
        if (isCaseine()) {
            throw new CaseineProjectAlreadyExistingException("A project already exists in " + projectPath);
        }
        File root = new File(projectPath);

        if (!root.exists()) {
            root.mkdir();
        }

        try {
            copyAZipAndUnZipIt(FileUtils.class.getResourceAsStream(String.format("/no-sp-lang/project%d.zip", template)), root, ".");
        } catch (Exception e) {
            System.err.println("Template not found");
            System.exit(1);
        }

        fillCaseine(defaultProperties);
        // Adds vplId in the project
        fillCaseine(vplId);
        // Fills the .caseine with the project name
        fillCaseineProjectFile();
    }

    @Override
    public void local(ClassLoader cl) throws IOException, ClassNotFoundException, MavenProjectException {

        // Clean to be confident about the result
        clean();

        try {
            NoTypeLauncher.launch(Paths.get(projectPath).toAbsolutePath().toString());
        } catch (Exception ex) {
            System.err.println(ex.getMessage());
        }
    }

    /**
     * Publishes the templates to the remote caseine server.
     * The full description of the lab is synchronized.
     *
     * @param cl an optional classloader to provide dependencies
     * @throws IOException            if something wrong
     * @throws ClassNotFoundException if something wrong
     * @throws VplException           if something wrong
     * @throws VPLIDMissingException
     * @throws MavenProjectException
     * @throws NothingPushedException
     */
    public void push(ClassLoader cl) throws IOException, ClassNotFoundException, VplException, VPLIDMissingException, MavenProjectException, NothingPushedException {

        boolean somethingPushed = false;

        // Run local if needed
        if (!noLocal) local(cl);

        if (vplId != VPLID_0) {

            if (!noSettings) {
                // Synchronize noSettings
                somethingPushed = somethingPushed | updateSettings();
            }

            String[] ids = vplId.split(",");

            for (String id : ids) {

                RestJsonMoodleClient client = new RestJsonMoodleClient(id, token, url);
                client.setServerSupportsIsBinary(true);

                somethingPushed = saveFiles(client, projectPath + getPATH_REQUESTED_FILES(), VPLService.VPL_SAVE_RF, true, true);

                // Execution Files
                somethingPushed = somethingPushed | saveFiles(client, projectPath + getPATH_EXECUTION_FILES(), VPLService.VPL_SAVE_EF, true, false);

                // Corrected Files
                somethingPushed = somethingPushed | saveFiles(client, projectPath + getPATH_CORRECTED_FILES(), VPLService.VPL_SAVE_CF, true, true);
            }

            // Synchronize the documentation
            somethingPushed = somethingPushed | documentation();
            if (!somethingPushed) {
                throw new NothingPushedException("Nothing pushed on the server");
            }
        } else {
            throw new NothingPushedException("Nothing pushed on the server : a VPL_ID is needed");
        }
    }

    @Override
    public void clean() throws IOException {
        new File(projectPath + getPATH_BIN()).mkdir();
        if (new File(projectPath + getPATH_OUTPUT()).exists()) {
            Path folder = Paths.get(projectPath + getPATH_OUTPUT());
            caseine.FileUtils.clean(folder);
        }
    }


    @Override
    public boolean isGenerated() {
        return new File(projectPath + getPATH_OUTPUT()).exists();
    }

    @Override
    public void nature() throws CaseineProjectAlreadyExistingException, IOException {
        if (isCaseine()) {
            if (getType() == CaseineJavaProject.ProjectType.MAVEN) {
                throw new CaseineProjectAlreadyExistingException(
                        "Nature command is not implemented for Maven projects");
            }
            throw new CaseineProjectAlreadyExistingException("A project already exists in " + projectPath);
        }
        if (!caseineFile.createNewFile()) {
            log.severe(".caseine cannot be created");
        }
        // Adds vplId in the project
        fillCaseine(vplId);
    }


    @Override
    public boolean documentation() throws VplConnectionException, MoodleWebServiceException, IOException {

        boolean somethingPushed = false;

        if (vplId != VPLID_0) {

            String[] ids = vplId.split(",");

            for (String id : ids) {

                RestJsonMoodleClient client = new RestJsonMoodleClient(id, token, url);
                client.setServerSupportsIsBinary(true);

                // Get Format

                JsonObject introObject = client.callService("mod_vpl_get_setting", "settingname", "introformat");
                String format = introObject.getString("value");

                String filePath = projectPath + PATH_DOC;
                String fileName = filePath + File.separator + "intro";
                if (format.equals("1")) {
                    fileName += ".html";
                }

                File doc = new File(fileName);
                File docDir = new File(filePath);
                if (doc.exists()) { // Push it to the server
                    String introduction = Files.readString(Path.of(fileName));
                    client.callService("mod_vpl_set_setting", "settingname", "intro", "settingvalue", introduction);
                    somethingPushed = true;
                } else { // Get it locally
                    docDir.mkdir();
                    JsonObject intro = client.callService("mod_vpl_info");
                    String introHtml = intro.getString("intro", "A problem occured while recovering the VPL description.");
                    try (BufferedWriter writer = new BufferedWriter(new FileWriter(fileName))) {
                        writer.write(introHtml);
                    }
                }
            }
        }
        return somethingPushed;
    }

    public boolean updateSettings() throws VplConnectionException, MoodleWebServiceException, IOException {

        CaseineProperties properties = getProperties();

        if (vplId != VPLID_0) {

            String[] ids = vplId.split(",");

            for (String id : ids) {

                RestJsonMoodleClient client = new RestJsonMoodleClient(id, token, url);
                client.setServerSupportsIsBinary(true);
                properties.entrySet().stream().filter(entry -> entry.getKey().toString().startsWith("WS.")).forEach(entry ->
                {
                    try {
                        String settingName = entry.getKey().toString().substring(3);
                        String settingValue = entry.getValue().toString();
                        log.info(String.format("Trying to update setting %s with value %s", settingName, settingValue));
                        client.callService("mod_vpl_set_setting", "settingname", settingName, "settingvalue", convertFormattedDateToLongAsString(settingValue));
                    } catch (VplConnectionException | MoodleWebServiceException e) {
                        log.severe(e.getMessage());
                    }
                });
            }
        }
        return !properties.entrySet().isEmpty();
    }

    /**
     * By convention requested files generated for a CaseInE project must be here.
     */
    protected static String getPATH_REQUESTED_FILES() {
        return getPATH_OUTPUT() + File.separator + RF + File.separator;
    }

    /**
     * By convention corrected files generated for a CaseInE project must be here.
     */
    protected static String getPATH_CORRECTED_FILES() {
        return getPATH_OUTPUT() + File.separator + CF + File.separator;
    }

    /**
     * By convention execution files generated for a CaseInE project must be here.
     */
    protected static String getPATH_EXECUTION_FILES() {
        return getPATH_OUTPUT() + File.separator + EF + File.separator;
    }

    /**
     * By convention outputs of a CaseInE project must be here.
     */
    protected static String getPATH_OUTPUT() {
        return getPATH_BIN() + "caseine-output";
    }

    /**
     * By convention binaries of a CaseInE project must be generated here.
     */
    protected static String getPATH_BIN() {
        return PATH_BIN;
    }

    /**
     * @param client          the Caseine client
     * @param projectLocation the local root directory
     * @param service         the action key for the Caseine WS
     * @param override        is True if we want local files to override remote
     *                        ones.
     * @param checkMax        is True if we must check the number of files to push
     * @return true if files are saved on the server
     * @throws IOException
     * @throws VplException
     */
    private static boolean saveFiles(RestJsonMoodleClient client, String projectLocation, VPLService service,
                                     boolean override, boolean checkMax) throws IOException, VplException {
        boolean somethingPushed = false;
        List<File> files = FileUtils.listFiles(projectLocation);
        if (files == null) {
            System.out.println("Directory " + projectLocation + " not found");
        } else if (files.isEmpty()) {
            System.out.println("No files found in " + projectLocation);
        } else {
            Stream<VplFile> vplFiles = files.stream().map(file -> {
                return fileToCaseinePath(file, projectLocation);
            });
            somethingPushed = !files.isEmpty();
            JsonObject result = client.callServiceWithFiles(service.toString(), vplFiles.collect(Collectors.toList()));
            System.out.println(result == null ? "Ok" : result);
        }
        return somethingPushed;
    }

    protected void fillCaseineProjectFile() {
        try {
            caseine.FileUtils.fillTemplate(projectPath + File.separatorChar + ".project", "/*VPL_NAME*/", getProjectName());
        } catch (IOException e) {
            log.severe(e.getMessage());
        }
    }

    protected static VplFile fileToCaseinePath(File f, String projectLocation) {
        projectLocation = new File(projectLocation).getAbsolutePath();
        if (!projectLocation.endsWith(File.separator)) {
            projectLocation = projectLocation + File.separator;
        }
        String fProjectRelativeName = f.getAbsolutePath().replace(projectLocation, "").replace(File.separator, "/");
        if (fProjectRelativeName.startsWith(PATH_SRC + "/"))
            fProjectRelativeName = fProjectRelativeName.substring(PATH_SRC.length() + 1);
        try {
            return new VplFile(f, fProjectRelativeName);
        } catch (IOException e) {
            return null;
        }
    }

    public String getProjectName() {
        String projectName = null;
        File projectFile = new File(projectPath);
        if (projectPath.endsWith(".")) {
            projectName = projectFile.getParentFile().getName();
        } else {
            projectName = projectFile.getName();
        }
        return projectName;
    }

}
