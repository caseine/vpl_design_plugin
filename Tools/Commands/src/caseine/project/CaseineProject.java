/*
 * Copyright 2019: Christophe Saint-Marcel
 * This software is part of the Caseine project.
 * This software was developped with the support of the following organizations:
 * Université Grenoble Alpes
 * Institut Polytechnique de Grenoble
 * 
 * 
 * This file is part of the VPL Design Tool.
 * The VPL Design Tool is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * The VPL Design Tool is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with the VPL Design Tool.  If not, see <https://www.gnu.org/licenses/>.
 */
package caseine.project;

import java.io.IOException;

import caseine.exceptions.TestDirectoryMissingException;
import caseine.exceptions.UnitTestsFileMissingException;
import vplwsclient.exception.MoodleWebServiceException;
import vplwsclient.exception.VplConnectionException;
import vplwsclient.exception.VplException;

public interface CaseineProject {

    public enum ProjectType {
		JAVA, MAVEN, PYTHON, CPP, NO_SP_LANG
	}

	/**
	 * 
	 * @return True if it is a Caseine project
	 */
	 boolean isCaseine();

	 ProjectType getType();

	 String getVplId();

	/**
	 * Generates the project template.
	 * @param mvn True if we must generate the maven pom.xml
	 * @param template a number to define a kind of template
	 * 
	 * @throws CaseineProjectAlreadyExistingException
	 * @throws BadIDEException
	 * @throws IOException
	 * @throws TestDirectoryMissingException 
	 * @throws UnitTestsFileMissingException 
	 * @throws FileMissingException 
	 */
	 void generate(boolean mvn, int template) throws CaseineProjectAlreadyExistingException, BadIDEException, IOException, TestDirectoryMissingException, FileMissingException, UnitTestsFileMissingException;
	
	/**
	 * Generates the local caseine templates into caseine-output.
	 * 
	 * @param cl an optional classloader to provide dependencies
	 * @throws IOException if ioException
	 * @throws ClassNotFoundException if the class is not found
	 * @throws MavenProjectException 
	 */
	 void local(ClassLoader cl) throws IOException, ClassNotFoundException, MavenProjectException;


	/**
	 * Publishes the templates to the remote caseine server.
	 *
	 * @param cl an optional classloader to provide dependencies
	 * @throws IOException            if something wrong
	 * @throws ClassNotFoundException if something wrong
	 * @throws VplException           if something wrong
	 * @throws VPLIDMissingException
	 * @throws MavenProjectException
	 * @throws NothingPushedException
	 */
	void push(ClassLoader cl) throws IOException, ClassNotFoundException, VplException, VPLIDMissingException, MavenProjectException, NothingPushedException;

	/**
	 * Cleans the local templates.
	 * 
	 * @throws IOException if an error occurs with the deleted files
	 */
	 void clean() throws IOException;


	/*
	 * 
	 * @return True if the Caseine project is generated locally
	 */
	boolean isGenerated();

	/**
	 * Set the caseine nature to the project
	 * @throws CaseineProjectAlreadyExistingException
	 * @throws IOException
	 */
	void nature() throws CaseineProjectAlreadyExistingException, IOException;
	
	/**
	 * Synchronize the documentation of the VPL
	 * @return true if something is published on the server
	 */
	boolean documentation() throws VplConnectionException, MoodleWebServiceException, IOException;


	/**
	 * Update the noSettings of the VPL
	 * @return true if something is published on the server
	 */
	boolean updateSettings() throws VplConnectionException, MoodleWebServiceException, IOException;
}
