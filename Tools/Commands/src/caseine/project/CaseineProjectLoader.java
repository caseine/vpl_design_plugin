/*
 * Copyright 2019: Christophe Saint-Marcel
 * This software is part of the Caseine project.
 * This software was developped with the support of the following organizations:
 * Université Grenoble Alpes
 * Institut Polytechnique de Grenoble
 * 
 * 
 * This file is part of the VPL Design Tool.
 * The VPL Design Tool is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * The VPL Design Tool is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with the VPL Design Tool.  If not, see <https://www.gnu.org/licenses/>.
 */
package caseine.project;

/**
 * Inspects the .caseine file to load the right type of Caseine Project.
 * @author christophe
 *
 */
public class CaseineProjectLoader {
		
	/**
	 * 
	 * @return the right project
	 */
	public static CaseineProject getProject(String projectPath, String vplId, String ide, String url, String token, boolean noSettings, boolean noLocal) throws NotACaseineProjectException {
		CaseineCommonProject project = new CaseineCommonProject(projectPath, null, url, token, noSettings);
		switch(project.getType()) {
			case JAVA, MAVEN: return new CaseineJavaProject(projectPath, vplId, ide, url, token, noSettings, noLocal);
			case PYTHON: return new CaseinePythonProject(projectPath, vplId, url, token, noSettings, noLocal);
			case CPP: return new CaseineCppProject(projectPath, vplId, url, token, noSettings, noLocal);
			default: return new CaseineCommonProject(projectPath, vplId, url, token, noSettings, noLocal);
		}
	}

}
