/*
 * Copyright 2019: Christophe Saint-Marcel
 * This software is part of the Caseine project.
 * This software was developped with the support of the following organizations:
 * Université Grenoble Alpes
 * Institut Polytechnique de Grenoble
 * 
 * 
 * This file is part of the VPL Design Tool.
 * The VPL Design Tool is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * The VPL Design Tool is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with the VPL Design Tool.  If not, see <https://www.gnu.org/licenses/>.
 */
package caseine.project;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Properties;

import caseine.PythonLauncher;
import caseine.LANG;
import caseine.exceptions.TestDirectoryMissingException;
import caseine.exceptions.UnitTestsFileMissingException;
import vplwsclient.FileUtils;

import static caseine.FileUtils.copyAZipAndUnZipIt;

/**
 * The base class for python projects.
 * @author christophe
 *
 */
public class CaseinePythonProject extends CaseineCommonProject implements CaseineProject {

	private final Properties defaultProperties = new Properties();
	/**
	 * 
	 * @param projectPath the path
	 * @param vplId the identifier of the VPL
	 * @param url the caseine server URL
	 * @param token the caseine token
	 */
	public CaseinePythonProject(String projectPath, String vplId, String url, String token, boolean noSettings, boolean noLocal) {
		super(projectPath, vplId, url, token, noSettings, noLocal);
		defaultProperties.setProperty("CASEINE_VPL_TYPE", LANG.PYTHON.name());
		defaultProperties.setProperty("CASEINE_TOKEN", "<env>");
		defaultProperties.setProperty("WS.basedon", "4229");
		defaultProperties.setProperty("WS.run", "1");
		defaultProperties.setProperty("WS.evaluate", "1");
		defaultProperties.setProperty("WS.automaticgrading", "1");
	}

	@Override
	public ProjectType getType() {
		return ProjectType.PYTHON;
	}

	/**
	 * Generates the project template.
	 * 
	 * @param mvn True if we must generate the maven pom.xml
	 * @param template the number of the template
	 * 
	 * @throws CaseineProjectAlreadyExistingException if an existing project is present
	 */
	@Override
	public void generate(boolean mvn, int template) throws CaseineProjectAlreadyExistingException{

		if (isCaseine()) {
			throw new CaseineProjectAlreadyExistingException("A project already exists in " + projectPath);
		}
		File root = new File(projectPath);
		if (!root.exists()) {
			root.mkdir();
		}

		try {
			copyAZipAndUnZipIt(FileUtils.class.getResourceAsStream(String.format("/python/default/project%d.zip", template)), root, ".");
		} catch (Exception e) {
			System.err.println("Template not found");
			System.exit(1);
		}
		/*caseine.FileUtils.copyAZipAndUnZipIt(CaseineCommand.class.getResourceAsStream("/python/default/t1.zip"), root,
				".");

		boolean rootWasEmptyFirst = !Arrays.stream(root.listFiles()).filter(f -> !f.isHidden())
				.filter(f -> !"vpl_unittest.py".equals(f.getName())).findFirst().isPresent();
		if (rootWasEmptyFirst) {
			writeATemplate(root);
		} else {
				checkRoot(root);
		}*/

		fillCaseine(defaultProperties);
		// Adds vplId in the project
		fillCaseine(vplId);
		// Fills the .caseine with the project name
		fillCaseineProjectFile();

	}

	private void checkRoot(File root) throws TestDirectoryMissingException, UnitTestsFileMissingException {
		/*if (!Arrays.stream(root.listFiles()).filter(f -> f.getName().endsWith(".py")).filter(f -> !f.isHidden())
				.filter(f -> !"vpl_unittest.py".equals(f.getName())).findAny().isPresent()) {
			throw new FileMissingException("It should have at least one python file in this directory");
		}*/
		File test = new File(root, "test");
		if (test == null || !test.exists() || !test.isDirectory()) {
			throw new TestDirectoryMissingException("It should have a test directory");
		}

		if (Arrays.stream(test.listFiles()).noneMatch(f -> f.getName().endsWith(".py"))) {
			throw new UnitTestsFileMissingException("It should have at least one unit test file in test directory");
		}
	}

	/**
	 * Generates the local caseine templates into caseine-output.
	 * 
	 * @param cl an optional classloader to provide dependencies
	 * @throws IOException for IO issues
	 * @throws ClassNotFoundException if the class is not found
	 * @throws MavenProjectException if the 
	 */
	public void local(ClassLoader cl) throws IOException, ClassNotFoundException, MavenProjectException {

		// Clean to be confident about the result
		clean();

		try {
			PythonLauncher.launch(Paths.get(projectPath).toAbsolutePath().toString());
		} catch (Exception ex) {
			System.err.println(ex.getMessage());
		}
	}
}
