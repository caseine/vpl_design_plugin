/*
 * Copyright 2021: Christophe Saint-Marcel
 * This software is part of the Caseine project.
 * This software was developped with the support of the following organizations:
 * Université Grenoble Alpes
 * Institut Polytechnique de Grenoble
 * 
 * 
 * This file is part of the VPL Design Tool.
 * The VPL Design Tool is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * The VPL Design Tool is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with the VPL Design Tool.  If not, see <https://www.gnu.org/licenses/>.
 */package caseine.mutants;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.Options;

/**
 * A command to generate templates for mutation tests.
 * It generates the TestSuite and all templates of mutants.
 */
public class Generate {

	private static Logger log = Logger.getLogger(Generate.class.getName());

	/**
	 * By convention sources of a CaseInE project must be here.
	 */
	private static final String PATH_SRC = File.separator + "src";
	/**
	 * By convention test sources of a CaseInE project must be here.
	 */
	private static final String PATH_TEST = File.separator + "test";

	private static Options options;

	static {
		options = new Options();

		options.addOption("class", "Class Name", true, "");
		options.addOption("nb", "Number of mutants", true, "");
	}

	/**
	 *
	 * @param args
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception {

		CommandLineParser parser = new DefaultParser();
		CommandLine cmd = parser.parse(options, args);

		String projectPath = ".";
		String className;
		int numberOfMutants = 10;

		String[] remainingArgs = cmd.getArgs();
		if (remainingArgs != null && remainingArgs.length >= 1) {
			projectPath = remainingArgs[0];
		}
		className = cmd.getOptionValue("class");
		if (className == null) {
			System.err.println("Option 'class' is mandatory");
			return;
		}
		if (cmd.hasOption("nb")) {
			numberOfMutants = Integer.parseInt(cmd.getOptionValue("nb"));
		}

		publish(projectPath, className, null, numberOfMutants);
	}

	/**
	 * Generates the local templates.
	 * 
	 * @param projectPath the project location
	 * @param className the complete name of the class (example: "fr.uga.m1.Test")
	 * @param testClassName, can be null if convention is followed with prefix "Test"
	 * @param numberOfMutants number of mutants to generate
	 * @throws IOException if something wrong
	 * @throws ClassNotFoundException if something wrong
	 */
	public static void publish(String projectPath, String className, String testClassName, int numberOfMutants)
			throws IOException, ClassNotFoundException {
		
		File testDirectory = new File(
				projectPath + PATH_TEST + File.separator);
		if (!testDirectory.exists()) {
			testDirectory.mkdir();
		}
			
		writeTestSuite(projectPath);
		
		File src = new File(
				projectPath + PATH_SRC + File.separator + className.replace(".", File.separator) + ".java");
		if (!src.exists()) {
			log.log(Level.SEVERE, "Mutants not generated, Bad class name: " + className);
			return;
		}
		String classShortName = className;
		String pathName = "";
		int index = className.lastIndexOf(".");
		if (index != -1) {
			classShortName = className.substring(index + 1);
			pathName = className.substring(0, index + 1);
		}
		if (testClassName == null) {
			testClassName =  pathName + "Test" + classShortName;
		}
		File test = new File(
				projectPath + PATH_SRC + File.separator + testClassName.replace(".", File.separator) + ".java");
		if (!test.exists()) {
			log.log(Level.SEVERE, "Mutants not generated, bad name for class of test: " + testClassName);
			return;
		}
		for (int i = 1; i <= numberOfMutants; i++) {
			String postFix = Integer.toString(i);
			if (i < 10) {
				postFix = "0" + postFix;
			}
			postFix = "mutant_" + postFix;
			String packaging = "package " + postFix + ";"; 
			testDirectory = new File(
					projectPath + PATH_TEST + File.separator + postFix + File.separator);
			if (!testDirectory.exists()) {
				testDirectory.mkdir();
				BufferedReader reader;
				try {
					// Output file
					BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(
						    new FileOutputStream(testDirectory.toPath().resolve(src.toPath().getFileName()).toFile()), "UTF-8"));
					
					// Input file
					reader = new BufferedReader(new FileReader(src));

					// Overwrite packaging
					writer.write(packaging);
					writer.newLine();

					String line = reader.readLine();
					while (line != null) {
						if (line.contains("class")) {
							writer.newLine();							
							writer.write("import caseine.extra.utils.mutations.Mutant;");
							writer.newLine();							
							writer.newLine();							
							writer.write("@Mutant(testClass = \"" + testClassName + "\", errorMessage = \"@TODO: Define the helper to kill this mutant\")");
							writer.newLine();							
						}
						if (!line.contains("package")) {
							writer.write(line);
							writer.newLine();
						}
						// read next line
						line = reader.readLine();
					}
					reader.close();
					writer.close();
				} catch (IOException e) {
					log.log(Level.SEVERE, e.getMessage(), e);
				}		
			}
		}
	}

	private static void writeTestSuite(String projectPath) throws IOException {
		StringBuilder sb = new StringBuilder();
		sb.append("import org.junit.runners.Suite;\n");
		sb.append("import caseine.extra.utils.mutations.FactoryRunnerSuite;\n\n");
		sb.append("import org.junit.runner.RunWith;\n\n");
		sb.append("@RunWith(Suite.class)\n");
		sb.append("@Suite.SuiteClasses({FactoryRunnerSuite.class})\n");
		sb.append("public class MutationTestSuite {\n");
		sb.append("\t//nothing\n");
		sb.append("}");
        Files.write(Paths.get(projectPath + PATH_TEST + File.separator + "MutationTestSuite.java"), sb.toString().getBytes(StandardCharsets.UTF_8));			
	}
}
