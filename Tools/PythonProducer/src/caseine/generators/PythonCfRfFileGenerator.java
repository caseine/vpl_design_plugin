package caseine.generators;

import java.io.*;
import java.util.Properties;
import java.util.Scanner;

/**
 * <p>
 * Reads a special source python file and prepare it to be written by 2 ways in cf, rf caseine paths
 * </p>
 * <p>
 * Such a file may contains 2 kinds of patterns : REMOVE_PATTERN ("//#-#") and COPYING_PATTERN ("//#=#")
 * </p>
 * <p>
 * An area to remove is limited by 2 REMOVE_PATTERN, the first at its beginning and the second at its ending.
 * A COPYING_PATTERN may exist or not. If yes, it should be between an beginning REMOVE_PATTERN and a ending
 * REMOVE_PATTERN.
 * </p>
 * <p>
 * Rf (required files)
 * </p>
 * <p>
 * In rf, the patterns and the area between the beginning REMOVE_PATTERN and COPYING_PATTERN (or the ending
 * REMOVE_PATTERN, if COPYING_PATTERN is missing) are removed.
 * </p>
 * <p>
 * Cf (corrected files)
 * </p>
 * <p>
 * In cf, the patterns and the area between COPYING_PATTERN et the ending REMOVE_PATTERN are removed. If COPYING_PATTERN
 * is missing, only the patterns are removed.
 * </p>
 * <p>
 * Theses contents are available by the methods getRf() and getCf()
 * </p>
 */
public class PythonCfRfFileGenerator {
    private final File sourceFile;
    private String REMOVE_PATTERN = "^[\\s]*#[\\s]*-#";
    private String COPYING_PATTERN = "^[\\s]*#[\\s]*=#";
    private String UNCOMMENT_PATTERN = "^[\\s]*#([\\s]*)(.*)$";
    private boolean removing = false;
    private boolean copying = false;

    private StringBuilder rf = new StringBuilder();
    private StringBuilder cf = new StringBuilder();

    /**
     * Create a PythonCfRfFileGenerator in order to generate ef, rf and cf files from headerOrSourceFile
     *
     * @param sourceFile the file whose images you want in cf, rf and ef.
     * @throws IOException if headerOrSourceFile does not exist or is unreadable
     */
    public PythonCfRfFileGenerator(File sourceFile) throws IOException {
        this.sourceFile = sourceFile;
        analyse();
    }

    private void setPatterns() {
        Properties properties = new Properties();
        try (InputStream in = new FileInputStream(new File(sourceFile.getParentFile(), ".caseine"))) {
            properties.load(in);
            String removePattern = properties.getProperty("REMOVE_PATTERN");
            if (removePattern != null && !removePattern.isEmpty()) {
                REMOVE_PATTERN = removePattern;
            }
            String copyingPattern = properties.getProperty("COPYING_PATTERN");
            if (copyingPattern != null && !copyingPattern.isEmpty()) {
                COPYING_PATTERN = copyingPattern;
            }
            String uncommentPattern = properties.getProperty("UNCOMMENT_PATTERN");
            if (uncommentPattern != null && !uncommentPattern.isEmpty()) {
                UNCOMMENT_PATTERN = uncommentPattern;
            }
        } catch (IOException e) {

        }
    }

    private void analyse() throws FileNotFoundException {
        setPatterns();
        try (Scanner in = new Scanner(this.sourceFile)) {
            String line = "";
            while (in.hasNextLine()/* && !line.trim().startsWith("#")*/) {
                if (line.trim().matches(REMOVE_PATTERN))/* .startsWith(PATTERN_REMOVE)/*/ {
                    removing = !removing;
                    if (!removing) {
                        copying = false;
                    }
                } else if (line.trim().matches(COPYING_PATTERN))/*.startsWith(PATTERN_COPYING))*/ {
                    copying = true;
                } else {
                    if (!removing) {
                        rf.append(line).append('\n');
                        cf.append(line).append('\n');
                    } else {
                        if (copying) {
                            try {
                                rf.append(line.replaceAll(UNCOMMENT_PATTERN, "$1$2")).append('\n');
                            } catch (Exception e) {
                                try {
                                    rf.append(line.replaceAll(UNCOMMENT_PATTERN, "$1")).append('\n');
                                } catch (Exception e1) {
                                    rf.append(line.replaceFirst(UNCOMMENT_PATTERN, "")).append('\n');
                                }
                            }
                        } else {
                            cf.append(line).append('\n');
                        }
                    }
                }
                line = in.nextLine();
            }
            while(in.hasNextByte()) {
                rf.append(in.nextByte());
                cf.append(in.nextByte());
            }
        }

    }

    public String getRf() {
        return rf.toString();
    }

    public String getCf() {
        return cf.toString();
    }

}
