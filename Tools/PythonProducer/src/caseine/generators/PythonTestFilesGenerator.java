package caseine.generators;

import caseine.PythonWriters;
import caseine.exceptions.WrongGradeException;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 1. Copie les fichiers du dossier test dans ef en ajustant les grades.
 * 2. Génère dans ef le fichier vpl_evaluate.cases
 */
public class PythonTestFilesGenerator {
    private final File rootTestFile;
    private final File rootEf;
    private final Pattern GRADE_PATTERN = Pattern.compile("@grade\\(([^()]+)\\)");

    public PythonTestFilesGenerator(File rootTestFile, File rootEf) throws IOException, WrongGradeException {
        this.rootTestFile = rootTestFile;
        this.rootEf = rootEf;
        analyse();
    }

    private void writeEvaluateCases() throws IOException {
        StringBuilder sb = new StringBuilder();
        for (File file : rootTestFile.listFiles()) {
            if (!file.getName().equals("__pycache__"))
                sb.append(file.getName().replaceAll("\\.py$", "")).append('\n');
        }
        PythonWriters.writeAString(sb.toString(), new File(rootEf, "vpl_evaluate.cases"));
    }

    private double getTotalGrade() throws FileNotFoundException, WrongGradeException {
        double somme = 0.0;
        String erreurGrade = "";

        for (File file : rootTestFile.listFiles()) {
            if (file.isFile()) {
                try (Scanner in = new Scanner(file)) {
                    while (in.hasNextLine()) {
                        String line = in.nextLine();
                        Matcher matcher = GRADE_PATTERN.matcher(line);
                        if (matcher.find()) {
                            erreurGrade = matcher.group(1);
                            somme += Double.parseDouble(matcher.group(1));
                        }
                    }
                } catch (NumberFormatException ex) {
                    throw new WrongGradeException("grade mal écrit : " + erreurGrade + " (@grade(<ici un nombre (éventuellement décimal)>");
                }
            }
        }
        return somme;
    }

    private void putInEfWithRelativeGrade(double totalGrade) throws IOException {
        for (File file : rootTestFile.listFiles()) {
            StringBuilder sb = new StringBuilder();
            if (file.isFile()) {
                try (Scanner in = new Scanner(file)) {
                    while (in.hasNextLine()) {
                        String line = in.nextLine();
                        Matcher matcher = GRADE_PATTERN.matcher(line);
                        if (matcher.find()) {
                            double valeur = 100.0 * (Double.parseDouble(matcher.group(1)) / totalGrade);
                            line = line.replaceAll(String.valueOf(GRADE_PATTERN), "@grade(" + valeur + ")");
                        }
                        sb.append(line).append('\n');
                    }
                    while (in.hasNextByte()) {
                        sb.append(in.nextByte());
                    }
                }
            }
            PythonWriters.writeAString(sb.toString(), new File(rootEf, file.getName()));
        }
    }

    private void analyse() throws IOException, WrongGradeException {
        writeEvaluateCases();
        double totalGrade = getTotalGrade();
        putInEfWithRelativeGrade(totalGrade);
    }

}
