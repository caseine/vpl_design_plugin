package caseine.cases;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.net.URISyntaxException;
import java.util.*;

public class VplEvaluateCases implements Iterable<Case> {
    private File file;
    private List<Case> cases = new LinkedList<>();
    private List<Case> proportionalCases = new LinkedList<>();


    public VplEvaluateCases(File file) throws FileNotFoundException {
        this.file = file;
        read();
        proportionate(100);
    }

    /**
     *
     * @param line a String that must begin by "case = "
     * @param in the red stream
     * @return a String that begin by "case = " or "" and its the end of the stream
     */
    private String readACase(String line, Scanner in) {
        String theCase = line.replaceFirst("[^=]*[\\s]+=[\\s]+", "");
        String theInput = theCase.replaceAll("\\s+", "");
        String theOutput = "OK"; // Always OK
        String theGradeReduction = "0"; // 0 if missing

        while (in.hasNextLine()) {
            line = in.nextLine().trim();
            if (line.toUpperCase().startsWith("CASE")) {
                cases.add(new Case(theCase, theGradeReduction, theInput, theOutput));
                return line;
            }
            if (line.toUpperCase().startsWith("GRADE REDUCTION")) {
                theGradeReduction = line.replaceFirst("[^=]*[\\s]+=[\\s]+", "");
            }/* else if (line.toUpperCase().startsWith("INPUT")) {
                theInput = line.replaceFirst("[^=]*[\\s]+=[\\s]+", "");
            } else if (line.toUpperCase().startsWith("OUTPUT")) {
                theOutput = line.replaceFirst("[^=]*[\\s]+=[\\s]+", "");
            }*/
        }
        cases.add(new Case(theCase, theGradeReduction, theInput, theOutput));
        return "";
    }

    private void proportionate(double nm) {
        double total = 0;
        for(Case c : cases) {
            try {
                double grade = Double.parseDouble(c.getTheGradeReduction().trim().replaceAll("\\%", ""));
                total += grade;
            } catch (NumberFormatException ex) {
                double grade = 0;
            }

        }
        for(Case c : cases) {
            try {
                double grade = Double.parseDouble(c.getTheGradeReduction().trim().replaceAll("\\%", ""));
                proportionalCases.add(new Case(
                        c.getTheCase(),
                        String.format(Locale.US, "%f%%", nm*grade/total),
                        c.getTheInput(),
                        c.getTheOutput()
                ));
            } catch (NumberFormatException ex) {
                double grade = 0;
            }
            //c.setTheGradeReduction(String.format(Locale.US), "%f\\%");
        }
    }

    /*public void rewrite(double nm) throws FileNotFoundException {
        proportionate(nm);
        try (PrintWriter out = new PrintWriter(file)) {
            for (Case c : cases) {
                out.println(String.format("case = %s", c.getTheCase()));
                out.println(String.format("grade reduction = %s", c.getTheGradeReduction()));
                out.println(String.format("input = %s", c.getTheInput()));
                out.println(String.format("output = %s", c.getTheOutput()));
                out.println();
            }
        }
    }*/

    /*public void rewrite() throws FileNotFoundException {
        rewrite(100);
    }*/

    private void read() throws FileNotFoundException {
        try (Scanner in = new Scanner(file)) {
            String line = "";
            // To find the first case ....
            while (in.hasNextLine() && !line.toUpperCase().startsWith("CASE")) {
                line = in.nextLine().trim();
            }
            // To find all the remaining cases
            while (line.toUpperCase().startsWith("CASE")) {
                line = readACase(line, in);
            }
        }
    }

    public String toString(List<Case> cases) {
        StringBuilder sb = new StringBuilder();
        for(Case c : cases) {
            sb.append(c).append('\n');
        }
        return sb.toString();
    }
    public String toString() {
        return toString(cases);
    }
    public String toStringProportional() {
        return toString(proportionalCases);
    }


    @Override
    public Iterator<Case> iterator() {
        return cases.iterator();
    }

    public int size() {
        return cases.size();
    }

    public static void main(String[] args) throws URISyntaxException, FileNotFoundException {
        VplEvaluateCases vec = new VplEvaluateCases(new File(VplEvaluateCases.class.getResource("/vpl_evaluate.cases").toURI()));
        System.out.println(vec);
    }
}
