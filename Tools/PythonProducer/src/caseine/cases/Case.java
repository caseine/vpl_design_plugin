package caseine.cases;

public class Case /*implements Cloneable*/ {
    private String theCase;
    private String theGradeReduction;
    private String theInput;
    private String theOutput;

    public Case(String theCase, String theGradeReduction, String theInput, String theOutput) {
        this.theCase = theCase;
        this.theGradeReduction = theGradeReduction;
        this.theInput = theInput;
        this.theOutput = theOutput;
    }

    public String getTheCase() {
        return theCase;
    }

    public String getTheGradeReduction() {
        return theGradeReduction;
    }

    /*public void setTheGradeReduction(String theGradeReduction) {
        this.theGradeReduction = theGradeReduction;
    }*/

    public String getTheInput() {
        return theInput;
    }

    public String getTheOutput() {
        return theOutput;
    }

    public String toString() {
        return String.format(
                        "case = %s\n" +
                        "grade reduction = %s\n" +
                        "input = %s\n" +
                        "output = %s\n", theCase, theGradeReduction, theInput, theOutput);
    }

    /*@Override
    public Case clone() {
        try {
            return (Case) super.clone();
        } catch (CloneNotSupportedException e) {
            throw new IllegalStateException();
        }
    }*/
}
