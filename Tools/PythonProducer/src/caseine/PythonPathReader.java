package caseine;

import caseine.exceptions.*;
import caseine.generators.PythonCfRfFileGenerator;
import caseine.generators.PythonTestFilesGenerator;

import java.io.*;
import java.util.*;

import static caseine.FileUtils.writeAResource;


/**
 * Analyse un dossier contenant des fichiers *.py pour produire le dossier caseine-output ...
 */
public class PythonPathReader {
    public String TO_DEL = "##DEL#";
    // The root of the directory to analyze
    private final File root;
    // The *.py files in order to generate vpl_run.sh
    private final List<File> pythonsInEf = new LinkedList<>();
    // The root of the directory cf
    private File rootCf;
    // The root of the directory rf
    private File rootRf;
    // The root of the directory ef
    private File rootEf;
    // The root of the directory where to put unit tests
    private File test;
    // A template file
    private File student;
    // A test file
    private File scripttest;
    // All files in ef are kept in order to change imports before written them
    private Map<File, String> efFiles = new HashMap<>();
    // The replacements in ef in order to change includes in ref
    private Map<File, File> changedInEf = new HashMap<>();
    // Root file was empty firt
    private boolean rootWasEmptyFirst = false;

    /*private void writeATemplate() throws TestDirectoryMissingException {
        File test = new File(root, "test");
        test.mkdirs();
        if (test == null || !test.exists() || !test.isDirectory()) {
            throw new TestDirectoryMissingException("It should have a test directory");
        }
        FileUtils.copyAZipAndUnZipIt(
                FileUtils.class.getResourceAsStream(String.format("/python/default/project%d.zip", n));
                root,
                "."
        );
        System.out.println("A python project template was generated because the directory was empty");
        System.out.println("You can use it like this");
        System.out.println("You can change existing and/or add new files");
    }*/

    /**
     * @param root the root directory
     * @throws IOException if directory is wrong
     * @throws WrongGradeException grade is wrong
     * @throws RootDirectoryMissingException root missing
     *
     */
    public PythonPathReader(File root) throws
            IOException,
            WrongGradeException,
            RootDirectoryMissingException {
        if (root == null) {
            throw new RootDirectoryMissingException("The root directory is missing");
        }
        this.root = root;

        setTheScene();
        read();
    }

    private void checkRoot() throws PythonFileMissingException, UnitTestsFileMissingException, TestDirectoryMissingException {
        if (!Arrays.stream(root.listFiles())
                .filter(f -> f.getName().endsWith(".py"))
                .filter(f -> !f.isHidden())
                .filter(f -> !"vpl_unittest.py".equals(f.getName()))
                .findAny().isPresent()) {
            throw new PythonFileMissingException("It should have at least one python file in this directory");
        }
        File test = new File(root, "test");
        if (test == null || !test.exists() || !test.isDirectory()) {
            throw new TestDirectoryMissingException("It should have a test directory");
        }

        if (!Arrays.stream(test.listFiles())
                .filter(f -> f.getName().endsWith(".py"))
                .findAny().isPresent()) {
            throw new UnitTestsFileMissingException("It should have at least one unit test file in test directory");
        }
    }

    /**
     * Sets the scene to push on caseine
     *
     * @throws IOException
     */
    private void setTheScene() throws IOException {
        // Creating file tree
        File caseineOutput = new File(root, "target/caseine-output");
        caseineOutput.mkdirs();
        rootEf = new File(caseineOutput, "ef");
        rootEf.mkdirs();
        rootRf = new File(caseineOutput, "rf");
        rootRf.mkdirs();
        rootCf = new File(caseineOutput, "cf");
        rootCf.mkdirs();
        test = new File(root, "test");
        test.mkdirs();
        if (!rootEf.exists()) {
            throw new IOException("Unable to create " + rootEf.getAbsolutePath());
        }
        if (!rootRf.exists()) {
            throw new IOException("Unable to create " + rootRf.getAbsolutePath());
        }
        if (!rootCf.exists()) {
            throw new IOException("Unable to create " + rootCf.getAbsolutePath());
        }

        writeAResource(PythonPathReader.class.getResourceAsStream("/vpl_unittest.py"), new File(root, "vpl_unittest.py"));

    }

    private void write(File file, String contenu) throws FileNotFoundException {
        try (PrintWriter out = new PrintWriter(file)) {
            out.print(contenu);
        }
    }

    /*
    If the first line of a file is the TO_DEL Tag, this file is to be deleted from rf and cf but it has to be added
    to ef.
     */
    private boolean isToDel(File file) {
        try (java.util.Scanner in = new Scanner(file)) {
            if (in.hasNextLine()) {
                return TO_DEL.equals(in.nextLine().trim().replaceAll("\\s+", "").toUpperCase());
            } else {
                return false;
            }
        } catch (FileNotFoundException e) {
            return false;
        }
    }


    private void setToDel() {
        Properties properties = new Properties();
        try (InputStream in = new FileInputStream(new File(root, ".caseine"))) {
            properties.load(in);
            String toDelPattern = properties.getProperty("TO_DEL_PATTERN");
            if (toDelPattern != null && !toDelPattern.isEmpty()) {
                TO_DEL = toDelPattern;
            }
        } catch (IOException e) {

        }
    }

    private void read() throws IOException, WrongGradeException {
        setToDel();
        PythonCfRfFileGenerator pythonCfRfFileGenerator;
        File main = null;
        for (File file : root.listFiles()) {
            if (file.getName().endsWith(".py") && !file.getName().equals("vpl_unittest.py")) {
                pythonCfRfFileGenerator = new PythonCfRfFileGenerator(file);
                // A file to delete is indeed in ef and not in cf nor rf
                // Otherwise it is in cf and rf and not in ef
                if (!isToDel(file)) {
                    write(new File(rootRf, file.getName()), pythonCfRfFileGenerator.getRf());
                    write(new File(rootCf, file.getName()), pythonCfRfFileGenerator.getCf());
                } else {
                    write(new File(rootEf, file.getName()), pythonCfRfFileGenerator.getCf());
                }
            }
        }
        // In ef
        PythonTestFilesGenerator testFilesGenerator = new PythonTestFilesGenerator(test, rootEf);
    }

    public static void main(String[] args) throws IOException, WrongGradeException, TestDirectoryMissingException, PythonFileMissingException, UnitTestsFileMissingException, RootDirectoryMissingException {
        new PythonPathReader(new File("/home/yvan/tmp/mesVPLs/exobidonpython2"));
    }
}
