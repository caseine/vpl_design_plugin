package caseine.exceptions;

public class TestDirectoryMissingException extends Exception {
    public TestDirectoryMissingException(String msg) {
        super(msg);
    }
}
