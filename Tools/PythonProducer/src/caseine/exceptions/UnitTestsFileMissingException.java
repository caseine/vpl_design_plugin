package caseine.exceptions;

public class UnitTestsFileMissingException extends Exception {
    public UnitTestsFileMissingException(String msg) {
        super(msg);
    }
}
