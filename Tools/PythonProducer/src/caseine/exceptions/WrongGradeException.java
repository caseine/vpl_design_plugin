package caseine.exceptions;

public class WrongGradeException extends Exception {
    public WrongGradeException(String msg) {
        super(msg);
    }
}
