package caseine.exceptions;

public class RootDirectoryMissingException extends Exception {
    public RootDirectoryMissingException(String msg) {
        super(msg);
    }
}
