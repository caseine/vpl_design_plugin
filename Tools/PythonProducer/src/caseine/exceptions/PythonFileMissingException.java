package caseine.exceptions;

public class PythonFileMissingException extends Exception {
    public PythonFileMissingException(String msg) {
        super(msg);
    }
}
