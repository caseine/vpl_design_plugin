package caseine;

import caseine.cases.VplEvaluateCases;

import java.io.*;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.Scanner;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class PythonWriters {


    public static void writeAString(String aString, File toWrite) throws IOException {
        try (
                BufferedWriter out = new BufferedWriter(new PrintWriter(toWrite));
        ) {
            out.write(aString);
        }
    }
/*
    public static void writeAFile(File toRead, File toWrite) throws IOException {
        if (toWrite.isDirectory())
            toWrite = new File(toWrite, toRead.getName());
        try (
                BufferedWriter out = new BufferedWriter(new PrintWriter(toWrite));
                BufferedReader in = new BufferedReader(new FileReader(toRead))
        ) {
            int c = in.read();
            while (c != -1) {
                out.write(c);
                c = in.read();
            }
        }
    }

    public static void writeAResource(String resourcePathName, File toWrite) throws IOException {
        try (
                BufferedOutputStream out = new BufferedOutputStream(new FileOutputStream(toWrite));
                BufferedInputStream in = new BufferedInputStream(PythonWriters.class.getResourceAsStream(resourcePathName))
        ) {
            int c = in.read();
            while (c != -1) {
                out.write(c);
                c = in.read();
            }
        }
    }

    public static void main(String[] args) throws FileNotFoundException, URISyntaxException {
        File root = new File(PythonWriters.class.getResource("/").toURI());
        System.out.println(root.exists());
        for (File file : root.listFiles()) {
            System.out.println(file);
        }
    }*/
}
