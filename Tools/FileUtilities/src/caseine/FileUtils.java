package caseine;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.Comparator;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.logging.Logger;
import java.util.stream.Stream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

public class FileUtils {

    private static Logger log = Logger.getLogger(FileUtils.class.getName());

    public static void TreeCopy(Path source, Path target) throws IOException {
        if (!target.toFile().isDirectory() || !target.resolve(source.getFileName()).toFile().exists()) {
            Files.copy(source, target.resolve(source.getFileName()), StandardCopyOption.REPLACE_EXISTING);
        }
        if (source.toFile() != null && source.toFile().listFiles() != null) {

            for (File child : source.toFile().listFiles()) {

                TreeCopy(child.toPath(), target.resolve(source.getFileName()));
            }

        }

    }

    /**
     * @param zipPathAsStream for instance
     *                        CppLauncher.class.getResourceAsStream("/antlr.zip")
     * @param targetRoot      for instance rootEf
     * @param targetName      for instance cppparser
     */
    public static void copyAZipAndUnZipIt(InputStream zipPathAsStream, File targetRoot, String targetName) throws IOException {

        // Copying zipfile from resources to target
        File tmp = new File(targetRoot, "tmp.zip");
        // tmp.delete();
        Files.copy(zipPathAsStream, tmp.toPath());
        // Unzipping copied zipfile from targetRoot/tmp.zip to targetRoot/targetName
        unzip(targetRoot, "tmp.zip", targetRoot, targetName);
        tmp.delete();

    }

    private static void unzip(File srcRoot, String srcName, File targetRoot, String targetName) throws IOException {
        byte[] buffer = new byte[1024];

        try (ZipInputStream zis = new ZipInputStream(new FileInputStream(new File(srcRoot, srcName)))) {
            ZipEntry zipEntry = zis.getNextEntry();
            while (zipEntry != null) {
                File newFile = newFile(targetRoot, zipEntry);
                if (zipEntry.isDirectory()) {
                    if (!newFile.isDirectory() && !newFile.mkdirs()) {
                        throw new IOException("Failed to create directory " + newFile);
                    }
                } else {
                    // fix for Windows-created archives
                    File parent = newFile.getParentFile();
                    if (!parent.isDirectory() && !parent.mkdirs()) {
                        throw new IOException("Failed to create directory " + parent);
                    }

                    // write file content
                    try (FileOutputStream fos = new FileOutputStream(newFile)) {
                        int len;
                        while ((len = zis.read(buffer)) > 0) {
                            fos.write(buffer, 0, len);
                        }
                        fos.close();
                    }
                }
                zipEntry = zis.getNextEntry();
            }
            zis.closeEntry();
        }
    }

    private static File newFile(File destinationDir, ZipEntry zipEntry) throws IOException {
        File destFile = new File(destinationDir, zipEntry.getName());

        String destDirPath = destinationDir.getCanonicalPath();
        String destFilePath = destFile.getCanonicalPath();

        if (!destFilePath.startsWith(destDirPath + File.separator)) {
            throw new IOException("Entry is outside of the target dir: " + zipEntry.getName());
        }

        return destFile;
    }

    /**
     * Copy a resource in a file
     *
     * @param rin     the InputStream a the resource obtain for instance like
     *                CppProducer.class.getResourceAsStream("cpp-templates/...")
     * @param toWrite
     * @throws IOException
     */
    public static void writeAResource(InputStream rin, File toWrite) throws IOException {
        try (BufferedOutputStream out = new BufferedOutputStream(new FileOutputStream(toWrite));
             BufferedInputStream in = new BufferedInputStream(rin)) {
            int c = in.read();
            while (c != -1) {
                out.write(c);
                c = in.read();
            }
        }
    }

    /**
     * Copy a file in another file
     *
     * @param toRead  the file to copy
     * @param toWrite the file to write
     * @throws IOException
     */
    public static void fileCopy(File toRead, File toWrite) throws IOException {
        try (BufferedOutputStream out = new BufferedOutputStream(new FileOutputStream(toWrite));
             BufferedInputStream in = new BufferedInputStream(new FileInputStream(toRead))) {
            int c = in.read();
            while (c != -1) {
                out.write(c);
                c = in.read();
            }
        }
    }

    /**
     * Replace all references to the key by the value in the file named filename
     *
     * @param fileName
     * @param key      pattern to replace, all occurrences will be replaced
     * @param value
     * @throws IOException
     */
    public static void fillTemplate(String fileName, String key, String value) throws IOException {
        File eclipseProjectFile = new File(fileName);

        if (eclipseProjectFile.exists()) {
            // Updates the .project file
            int bufferSize = 1024;
            char[] buffer = new char[bufferSize];
            StringBuilder out = new StringBuilder();
            try (Reader reader = new InputStreamReader(new FileInputStream(eclipseProjectFile), StandardCharsets.UTF_8)) {
                for (int numRead; (numRead = reader.read(buffer, 0, buffer.length)) > 0; ) {
                    out.append(buffer, 0, numRead);
                }
            }
            String replacement = out.toString().replace(key, value);
            try (BufferedWriter writer = new BufferedWriter(
                    new OutputStreamWriter(new FileOutputStream(eclipseProjectFile), StandardCharsets.UTF_8))) {
                writer.write(replacement);
            }
        }
    }

    /**
     * Simply clean a folder in depth
     *
     * @param folder the folder path to clean
     * @throws IOException for IO issues
     */
    public static void clean(Path folder) throws IOException {

        Function<Path, Stream<Path>> walk = p -> {
            try {
                return Files.walk(p);
            } catch (IOException e) {
                return Stream.empty();
            }
        };

        Consumer<Path> delete = p -> {
            try {
                Files.delete(p);
            } catch (IOException e) {
            }
        };

        Files.list(folder)
                .flatMap(walk)
                .sorted(Comparator.reverseOrder())
                .forEach(delete);
    }
}
