## Written by F. Thiard
## last modified : 06/06/2019

import unittest # on importe le module unittest
from vpl_unittest import timeout,grade # spécifique à Caseine

import codes_secrets as etu # on importe le ou les modules étudiants à tester


class TestHall(unittest.TestCase):
    #les classes de test doivent hériter de unittest.TestCase. C'est elle qui définit les méthodes assertTrue,assertFailse,fail...


    #Vos méthodes de test ici (une méthode = 1 test). Elles doivent avoir "self" comme unique argument.
    #
    # /!\ LES NOMS DES METHODES DE TEST DOIVENT COMMENCER PAR TEST "test". (si ce n'est pas le cas, elles ne seront pas chargées comme test, mais peuvent servir de fonctions auxiliaires)/!\
    # Chaque méthode de test peut contenir ce que vous voulez, ainsi qu'une ou plusieurs assertions (self.assertTrue(...)...).
    # Le test passe si et seulement toutes les assertions de la méthode sont vérifiées.
    #
    # La première ligne de la docstring peut être utilisée pour nommer le test. Sinon, le nom de la méthode sera utilisé.

    longMessage = False #Facultatif : permet d'afficher uniquement le message d'erreur personnalisé en cas d'échec

    @grade(2) #spécifique à Caseine : permet d'indiquer manuellement une note.
    #Si *aucun test* n'a de note manuelle, les notes seront calculées automatiquement.
    def test_est_premier_type(self):
        """est_premier (type de retour) """ #Vous pouvez spécifier le nom affiché du test ainsi.
        for i in [2,53,111,257,13621]:
            self.assertIsInstance(etu.est_premier(i),bool,msg="La fonction est_premier doit renvoyer une valeur booléenne; True ou False")

    @grade(8)
    def test_est_premier_petits(self):
        """est_premier (petits nombres)"""
        #le paramètre msg est optionnel, et permet de définir un message d'erreur personnalisé
        self.assertTrue(etu.est_premier(53),msg="fonction est_premier incorrecte :\nest_premier(53) doit renvoyer True")
        self.assertFalse(etu.est_premier(111),msg="fonction est_premier incorrecte :\nest_premier(111) doit renvoyer False")

    @grade(8)
    def test_est_premier_grands(self):
        """est_premier (grand nombres)"""
        self.assertTrue(etu.est_premier(257),msg="fonction est_premier incorrecte:\nest_premier(257) doit renvoyer False " )
        self.assertFalse(etu.est_premier(13621),msg="fonction est_premier incorrecte:\nest_premier(13621) doit renvoyer True")

    @grade(8)
    def test_sphinx_aime_petits(self):
        """sphinx_aime (petits nombres)"""
        erreur="fonction sphinx_aime incorrecte"
        self.assertTrue(etu.sphinx_aime(5),msg=erreur)
        self.assertTrue(etu.sphinx_aime(41),msg=erreur)
        self.assertFalse(etu.sphinx_aime(21),msg=erreur)

    @grade(8)
    def test_sphinx_aime_grands(self):
        """sphinx_aime (grand nombres)"""
        erreur="fonction sphinx_aime incorrecte"
        self.assertFalse(etu.sphinx_aime(1327),msg=erreur)
        self.assertTrue(etu.sphinx_aime(16061),msg=erreur)

    #Pour limiter le temps d'execution du test. premier arguments : durée max en seconde, deuxieme argument (optionnel) : message d'erreur
    @timeout(2,"Timeout ! Peut-être une boucle infinie ?") #dans Caseine uniquement
    @grade(8)
    def test_code_hall_petits(self):
        """code hall (petits nombres)"""
        erreur="fonction code_hall incorrecte"
        self.assertEquals(etu.code_hall(15),17,msg=erreur)
        self.assertTrue(etu.code_hall(35)==41,msg=erreur) #On aurait aussi pu utiliser assertEquals comme au dessus

    @timeout(2,"Timeout ! Peut-être une boucle infinie ?")
    @grade(8)
    def test_code_hall_grands(self):
        """code hall (grands nombres)"""
        erreur="fonction code_hall incorrecte"
        self.assertEquals(etu.code_hall(13457),13679,msg=erreur)
        self.assertEquals(etu.code_hall(10343),10427,msg=erreur)

#Vous pouvez définir plusieurs classes de test par fichier, si vous le souhaitez
class TestTresor(unittest.TestCase):

    longMessage=False

    @grade(2)
    @timeout(2,"Timeout ! Peut-être une boucle infinie ?")
    def test_est_puissance2_type(self):
        """est_puissance2 (type de retour) """ #Vous pouvez spécifier le nom affiché du test ainsi.
        valeurs_possibles=[True,False]
        for i in [9,64,56,1200,1024]:
            self.assertIn(etu.est_premier(i),valeurs_possibles,msg="La fonction est_puissance_2 doit renvoyer une valeur booléenne; True ou False")

    @grade(8)
    @timeout(2,"Timeout ! Peut-être une boucle infinie ?")
    def test_est_puissance2_petits(self):
        """est_puissance2 (petits nombres)"""
        erreur="fonction est_puissance2 incorrecte"
        for n in range(2,10):
            self.assertTrue(etu.est_puissance2(2**n),msg=erreur)
        for n in range(9,56,1200):
            self.assertFalse(etu.est_puissance2(n),msg=erreur)

    @grade(8)
    @timeout(2,"Timeout ! Peut-être une boucle infinie, ou du code inefficace.")
    def test_est_puissance2_grands(self):
        """est_puissance2 (grands nombres)"""
        erreur="fonction est_puissance2 incorrecte"
        self.assertTrue(etu.est_puissance2(32768),msg=erreur)
        self.assertFalse(etu.est_puissance2(5672),msg=erreur)

    @grade(8)
    @timeout(2,"Timeout ! Peut-être une boucle infinie ?")
    def test_osiris_aime_petits(self):
        """osiris_aime (petits nombres)"""
        erreur="fonction osiris_aime incorrecte"
        self.assertTrue(etu.osiris_aime(3),msg=erreur)
        self.assertTrue(etu.osiris_aime(63),msg=erreur)
        self.assertFalse(etu.osiris_aime(31),msg=erreur)

    @grade(8)
    @timeout(2,"Timeout ! Peut-être une boucle infinie, ou du code inefficace.")
    def test_osiris_aime_grands(self):
        """osiris_aime (grands nombres)"""
        erreur="fonction osiris_aime incorrecte"
        self.assertFalse(etu.osiris_aime(32767),msg=erreur)
        self.assertTrue(etu.osiris_aime(16383),msg=erreur)

    @grade(8)
    @timeout(5,"Aïe, les 5 secondes sont écoulées ! Peut-être une boucle infinie ou du code inefficace.")
    def test_code_tresor_petits(self):
        """code_tresor (petits nombres)"""
        erreur="fonction code_tresor incorrecte"
        self.assertEquals(etu.code_tresor(20),63,msg=erreur)
        self.assertEquals(etu.code_tresor(100),1023,msg=erreur)

    @grade(8)
    @timeout(5,"Aïe, les 5 secondes sont écoulées ! Peut-être une boucle infinie ou du code inefficace.")
    def test_code_tresor_grands(self):
        """code_tresor (grands nombres)"""
        erreur="fonction code_tresor incorrecte"
        self.assertEquals(etu.code_tresor(1200),16383,msg=erreur)
        self.assertEquals(etu.code_tresor(17200),262143,msg=erreur)

