# Written by Aurelie Lagoutte
# Last update: May 5 2017


def est_premier(n):
    #-#
    i=2 # i va parcourir tous les nombres de 2 a n-1 jusqu'a trouver un diviseur de n
    while i<n: 
        if n%i==0: # on a trouve un diviseur
            return False
        i=i+1
    return True # on a fini la boucle sans trouver de diviseur
    #=#
    # # TODO 1 : Ecrire la fonction est_premier qui prend en paramètre un entier n et renvoie True si n est premier, False sinon
    #    pass
    #-#



def sphinx_aime(n):
    #-#
    if est_premier(n) and est_premier(n+2):
        return True
    else:
        return False
    #=#
    # # TODO 2 : Ecrire la fonction sphinx_aime qui prend en paramètre un entier n et renvoie True si n et n+2 sont premiers, False sinon
    #    pass
    #-#

def code_hall(borne_inf):
    #-#
    n=borne_inf 
    while not(sphinx_aime(n)):
        n=n+1
    return n
    #=#
    # # TODO 3 : Ecrire la fonction code_hall qui prend en paramètre un entier borne_inf et renvoie le plus petit entier n>=borne_inf tel que n et n+2 sont premiers
    #    pass
    #-#


def est_puissance2(n):
    #-#
    # si n est puissance de 2, on peut diviser par deux jusqu'a tomber sur 1
    while n%2==0 and n>1:
        n=n//2
    if n==1:
        return True
    else: # sinon, n a un diviseur impair >1
        return False
    #=#
    # # TODO 4 : Ecrire la fonction est_puissance2 qui prend en paramètre un entier n et renvoie True si n est une puissance de 2, False sinon
    #    pass
    #-#
    
def osiris_aime(n):
    #-#
    if est_puissance2(n+1) and n%5==3:
        return True
    else:
        return False
    #=#
    # # TODO 5 : Ecrire la fonction osiris_aime qui prend en paramètre un entier n et renvoie True si n+1 est une puissance de 2 et si n est de la forme 5k+3, False sinon
    #    pass
    #-#

def code_tresor(borne_inf):
    #-#
    n=borne_inf
    while not osiris_aime(n):
        n=n+1
    return n
    #=#
    # # TODO 6 : Ecrire la fonction code_tresor qui prend en paramètre un entier borne_inf et renvoie le plus petit entier n>=borne_inf tel que n+1 est une puissance de 2 et n est de la forme 5k+3
    #    pass
    #-#
    
if __name__=="__main__": # NE PAS SUPPRIMER CETTE LIGNE
    # Votre programme principal ne sera pas évalué.
    # Utilisez-le pour tester votre programme en faisant
    # les appels de votre choix.
    # Respectez bien ce niveau d'identation.
    print("Debut du prog. principal")
    # Vos tests ici
