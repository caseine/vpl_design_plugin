
# Mode d'emploi du plugin de conception Caseine pour python

Ce mode d'emploi accompagne le template python conçu pour faciliter 
la création et la publication d'exercices auto-évalués VPL en Python (Virtual Programming Lab) 
sur la plateforme Caseine. Ce template est généré par le plugin de conception en 
ligne de commande Caseine via la commande `caseine -python-template-3`.

**Remarque** : ce template contient un code d'exemple.

Vous pouvez également utiliser le template vierge pour commencer de zéro 
ou pour "caseinifier" un projet existant à l'aide la commande `caseine --gen --lang=python`.

## Prérequis

- Installation du plugin Caseine CLI.
- Accès à un cours sur Caseine en tant qu'enseignant.
- Interpréteur Python 3.6 ou supérieur.

## Installation et Génération

Ce template est généré par l'une des commandes suivantes :
```bash
caseine -python-template-2
```
en étant dans le répertoire où vous souhaitez créer le projet.
```bash
caseine -python-template-2 -p <chemin_du_projet>
```
Cet exemple permet de montrer la possibilité d'avoir plusieurs fichiers sources et plusieurs fichiers de tests.

Il produit une structure de base pour commencer le développement de l'exercice et un exemple.

Nous appellerons **racine** ou **racine du projet** le répertoire du projet (dans lequel se trouve ce readme).

## Structure du Template

Le template comprend plusieurs dossiers et fichiers clés où vous devrez intégrer votre code, vos tests, et l'énoncé de l'exercice.

- racine
  - .caseine
  - readme.md
  - mini.py (contenant un exercice très simple en guise d'exemple)
  - maxi.py (contenant un exercice très simple en guise d'exemple)
  - vpl_unittest.py (**qu'il ne faut pas modifier**)
- doc
  - intro.html
- test
  - testmini.py (contenant des tests unitaires pour l'auto-évaluation de mini.py)
  - testmaxi.py (contenant des tests unitaires pour l'auto-évaluation de maxi.py)
- doc (qui n'existe pas au départ)
  - intro.html
## Principe

Le principe du plugin est de rassembler au même endroit toutes les informations de votre exercice
1. la version de l'étudiant,
2. l'énoncé,
3. la solution,
4. l'auto-évaluation.

Pour finalement publier l'exercice sur Caseine depuis sa machine locale.

## Publication de l'exemple fourni sur Caseine

Pour cela, suivez les étapes suivantes :

1. Créez un VPL dans un cours sur Caseine et notez son identifiant (`vpl_id`) et votre clef (`vpl_web_service`)
   d'identification sur Caseine.
2. Utilisez la commande `caseine --push -t<vpl_web_service> -v<vpl_id>` pour la première publication.
  - l'option `-t` est inutile si la variable d'environnement `CASEINE_TOKEN` contient votre identifiant.
  - Pour les publications suivantes, l'option `-v` est inutile, car le `vpl_id` est
    enregistré automatiquement dans le fichier `.caseine`.

Et voilà, l'exemple est publié sur Caseine et prêt à être utilisé par vos étudiants. Vous pouvez
le tester et le modifier à votre guise.

## Procédure à Suivre pour modifier l'exemple

Pour concevoir un exercice auto-évalué VPL grâce au plugin, il est recommandé de suivre la procédure suivante :
- **Modifier le code source Python** dans `student.py` 
- **Ajouter des commentaires spéciaux** pour cacher des fragments de code de la solution ou les remplacer 
par d'autres morceaux de code, comme décrit dans la suite.
- **Modifier l'énoncé** soit directement dans le code dans des commentaires ou dans le fichier `intro.html` du dossier `doc`, comme décrit dans la suite.
- **Modifier des tests unitaires (pour l'auto-évaluation)** dans le fichier `test/testscript.py` comme décrit dans la suite.

### Modifier le code source Python

Vous pouvez ajouter, supprimer, modifier le code de `student.py`  à votre guise. 
Vous pouvez également ajouter de nouveaux fichiers et même supprimer `student.py` 
si vous le souhaitez.

Les fichiers sources de la solution doivent rester à la racine du projet. 

### Ajouter des commentaires spéciaux

Pour faire coexister votre solution et la version de l'étudiant, vous pouvez utiliser 
des commentaires spéciaux pour cacher certains fragments de la solution ou pour les 
remplacer par d'autres morceaux de code. 

### Commentaires spéciaux

- **Cacher du Code** : Utilisez `#-#` un pour marquer le début edu code à 
cacher, et un autre pour la fin. Le code entre les deux ne sera pas visible par les étudiants.  
    **Exemple** :  
    ```python
    def mini(a, b):
    #-#
        if a < b:
            return a
        else:
            return b
    #-#
    ```
  L'étudiant verra
    ```python
  def mini(a, b):


- **Remplacer du Code** : Pour remplacer un fragment de code par un autre, 
  - utiliser `#-#` pour marquer le début du code à cacher, 
  - puis `#=#` pour désigner où commence le code de remplacement, 
  - et termine par un dernier `#-#` pour marque la fin. 
  - Le code de remplacement doit être commenté (précédé de `#`) et sera décommenté dans la version étudiante.  
    **Exemple** :  
    ```python
    def mini(a, b):
    #-#
        if a < b:
            return a
        else:
            return b
    #=#
        # # Attention les commentaires pour les étudiants en début de ligne doivent être commentés deux fois (comme celui-là).
        # pass # TODO : écrire le code de la fonction mini à la place de pass.
    #-#
      ```
  L'étudiant verra
    ```python
    def mini(a, b):
        # Attention les commentaires pour les étudiants en début de ligne doivent être commentés deux fois (comme celui-là).
        pass # TODO : écrire le code de la fonction mini à la place de pass.
### Rédaction de l'énoncé

L'énoncé de l'exercice peut être rédigé de deux manières non exclusives :
1. soit directement dans le code avec des commentaires.  
  **Exemple** :
```python
########################################################################################
#  Écrire la fonction mini(a, b) qui retourne le plus petit des deux entiers a et b
########################################################################################
def mini(a, b):
#-#
    if a < b:
        return a
    else:
        return b
#=#
#    pass
#-#

```
2. soit dans un fichier `intro.html` dans le dossier `doc`.  
  **Exemple** :
```html
<ol>
  <li>Écrire une fonction <strong>mini(a, b)</strong> qui retourne le plus petit d'entre a et b
  <li>Écrire une fonction <strong>maxi(a, b)</strong> qui retourne le plus grand d'entre a et b
</ol>
```
Remarque : le dossier `doc` n'existe pas au départ. Il faut le créer et y ajouter le fichier `intro.html`.

## Écriture des tests d'auto-évaluation

Les tests d'auto-évaluation sont à écrire dans le fichier `testscript.py` du dossier `test`. 
Vous pouvez ajouter, supprimer, modifier les tests à votre guise et même ajouter de nouveaux fichiers.
Ce fichier existe et est pré-rempli. Il **contient des explications détaillées** sur la manière de faire 
pour évaluer le travail des étudiants.

## Publication sur Caseine

Une fois que tout est prêt, l'exercice peut être publié sur Caseine.

Pour cela, suivez les étapes suivantes :

1. Créez un VPL dans un cours sur Caseine et notez son identifiant (`vpl_id`) et votre clef (`vpl_web_service`) 
   d'identification sur Caseine.
2. Utilisez la commande `caseine --push -t<vpl_web_service> -v<vpl_id>` pour la première publication. 
   - l'option `-t` est inutile si la variable d'environnement `CASEINE_TOKEN` contient votre identifiant.
   - Pour les publications suivantes, l'option `-v` est inutile, car le `vpl_id` est 
     enregistré automatiquement dans le fichier `.caseine`.
   
Et voilà, votre exercice est publié sur Caseine et prêt à être utilisé par vos étudiants.

## Conclusion

Ce template et ce guide README visent à simplifier le processus de création et de publication d'exercices Python sur Caseine, en fournissant une structure claire et des outils pour préparer efficacement votre contenu pédagogique.

## Support

Pour de l'aide supplémentaire ou des informations, contactez Yvan Maillot.