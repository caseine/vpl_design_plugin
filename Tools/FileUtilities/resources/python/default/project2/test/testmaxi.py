"""
A Template to begin to write your own unit tests
"""
import unittest
from vpl_unittest import grade, timeout

import maxi as etu
import random as r


class Tests(unittest.TestCase):
    longMessage = False

    @grade(3)
    def testMaxi(self):
        """ Test du minimum de deux valeurs """
        for i in range(10):
            a = r.randint(-100, 100)
            b = r.randint(-100, 100)
            mab = etu.maxi(a, b)
            self.assertIsNotNone(mab, msg="La fonction ne retourne rien")
            self.assertEqual(mab, a if a > b else b)