#!/bin/bash
# $Id: prolog_run.sh,v 1.4 2012-09-24 15:13:22 juanca Exp $
# Default Prolog language run script for VPL
# Copyright (C) 2012 Juan Carlos Rodríguez-del-Pino
# License http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
# Author Juan Carlos Rodríguez-del-Pino <jcrodriguez@dis.ulpgc.es>

## mise en place des variables vpl
. common_script.sh
## compilation (=> msg pour erreur à la compilation)
check_program swipl
swipl --goal=main --stand_alone=true -q -t halt -o prog.e -c $VPL_SUBFILE0
## mise en place de l'exécutable (=> msg des erreurs à l'exécution)
cat common_script.sh > vpl_execution
echo "./prog.e" >>vpl_execution
chmod +x vpl_execution