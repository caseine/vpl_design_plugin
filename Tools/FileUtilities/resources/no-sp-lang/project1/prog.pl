% % % % % % % % % % % % % % % % % % % % % % % % %
% Programme à écrire (main) :
%   lire un entier
%   incrémenter l'entier
%   afficher le résultat
%
% le programme principal, la lecture et l'ecriture sont donnés
%
% il ne reste plus qu'à ecrire le prédicat qui fait l'incrémentation
% profil attendu :
% plusUn(Entier,Resultat)
%
% Nota bene : pour faire des calculs, vous pouvez utiliser les contraintes aritmétiques { } ex. :
% fois(X_fois_Y,X,Y) : vrai ssi X_plus_Y = X * Y
% % % % % % % % % % % % % % % % % % % % % % % % %

fois(S,X,Y):- {S=X*Y}.


  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  %%% debut de votre code ici %%%

  %-%
  plusUn(N,R) :- {R = N + 1}.
  %-%

  %%% fin de votre code  ici  %%%
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


main :-
  use_module(library(clpq)),
  writeln('Donner un entier ?'),
  readln([N|_]), !,
  plusUn(N,P),
  write('Résultat : '),
  writeln(P).