#!/bin/bash

# Demander à l'utilisateur de saisir une phrase et affiche le nombre de mots dans cette phrase
#-#
echo "Veuillez saisir une phrase :"
read phrase

# Compter le nombre de mots dans la phrase
nombre_de_mots=$(echo "$phrase" | wc -w)

# Afficher le résultat
echo "$nombre_de_mots"
#-#

