CREATE TABLE artists (
 contact_id integer PRIMARY KEY,
 name text NOT NULL
);

INSERT INTO artists VALUES
 (1,"Buddy Rich"),
 (2,"Candido"),
 (3,"Charlie Byrd"),
 (4,"Daniel");
