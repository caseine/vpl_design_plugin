
# Mode d'emploi du plugin de conception Caseine tout langage

Ce mode d'emploi accompagne le template conçu pour faciliter 
la création et la publication d'exercices auto-évalués VPL (Virtual Programming Lab) 
sur la plateforme Caseine. Ce template est généré par le plugin de conception en 
ligne de commande Caseine via la commande `caseine --gen --lang=no-specific-language`.

**Remarque** : ce template contient l'exemple d'un exercice en SQL.

## Prérequis

- Installation du plugin Caseine CLI.
- Accès à un cours sur Caseine en tant qu'enseignant.

## Installation et Génération

Ce template est généré par l'une des commandes suivantes :
```bash
caseine --gen --lang=no-specific-language
```
en étant dans le répertoire où vous souhaitez créer le projet.
```bash
caseine --gen --lang=no-specific-language -p <chemin_du_projet>
```
Il produit une structure de base pour commencer le développement de l'exercice.

Nous appellerons **racine** ou **racine du projet** le répertoire du projet (dans lequel se trouve ce readme).

## Structure du Template

Le template comprend plusieurs dossiers et fichiers clés où vous devrez intégrer votre code, vos tests, et l'énoncé de l'exercice.

- racine
  - .caseine
  - readme.md
- test
  - vpl_evaluate.cases

## Principe

Le principe du plugin est de rassembler au même endroit toutes les informations de votre exercice
1. la version de l'étudiant,
2. l'énoncé,
3. la solution,
4. l'auto-évaluation.

Pour finalement publier l'exercice sur Caseine depuis sa machine locale.

### Filtre de fichiers considérés

Il faut définir des modèles de noms de fichiers pour définir les fichiers qui seront considérés 
comme des fichiers de code source. Les modèles de noms de fichiers sont des expressions 
régulières à enregistrer dans `.caseine` associé à la clé FILE_PATTERNS, comme ci-dessous, 
par exemple pour choisir des fichiers qui se terminent pas `c` :
```bash
FILE_PATTERNS=.*c
```
Remarquez que le modèle précédent prend en compte tous les fichiers se terminant par `c`.
***Si l'on voulait considérer les fichiers qui se terminent par `.c`, il faudrait écrire :***
```bash
FILE_PATTERNS=.*\.c
```

***Plusieurs modèles peuvent être séparés par des points-virgules, par exemple :***
```bash
FILE_PATTERNS=.*\.c;.*\.h;.*\.cpp;.*\.hpp
```
### Marqueurs de délimitation des zones à cacher ou à remplacer pour l'étudiant

Le plugin Caseine CLI va analyser les fichiers identifiés par les modèles de noms de fichiers
comme décrits dans la section précédente.

Son travail va consister à chercher dans les fichiers analysés des marqueurs de délimitation.
Ces marqueurs sont des commentaires spéciaux qu'il faut définir dans le `.caseine` associés
aux clés `REMOVE_PATTERN`, `COPYING_PATTERN`, `UNCOMMENT_PATTERN`.

Leur définition dépend du langage de programmation utilisé puisqu'ils doivent être des commentaires

Le code entre les marqueurs `REMOVE_PATTERN` ne sera pas visible par les étudiants.

Entre deux marqueurs `REMOVE_PATTERN`, il peut y avoir un marqueur `COPYING_PATTERN` qui indique
que le code entre les deux marqueurs `REMOVE_PATTERN` doit être remplacé par celui
entre `COPYING_PATTERN` et `REMOVE_PATTERN`.

La partie qui est entre `COPYING_PATTERN` et `REMOVE_PATTERN` doit être commentée mais elle
sera décommentée pour l'étudiant.

Le marqueur `UNCOMMENT_PATTERN` indique comment décommenter et quoi recopier pour l'étudiant.
Il peut être écrit sous trois formes :
- avec deux groupes de capture, et les deux groupes sont recopiés
- avec un seul groupe de capture, et le groupe est recopié
- sans groupe de capture, et le premier modèle rencontré est supprimé

Par exemple, en Python, on pourrait définir les marqueurs comme suit :
```bash
REMOVE_PATTERN=#-+ 
COPYING_PATTERN=#x+ 
UNCOMMENT_PATTERN=(\s*)# (.*)
```
C'est assez délicat pour Python, car il faut tenir compte des indentations, d'où
la complexité du modèle `UNCOMMENT_PATTERN`.

### Marqueur pour l'aiguillage des fichiers

Certains fichiers peuvent nécessaires à la compilation, mais ne sont pas utiles pour l'étudiant ou 
peuvent être délibérément cachés. 

Pour cela, il faut définir un marqueur `TO_DEL_PATTERN` dans le fichier `.caseine`.

Tout fichier possédant ***en tête*** un commentaire compatible avec le marqueur `TO_DEL_PATTERN` 
sera supprimé pour l'étudiant, mais accessible à la compilation.

Par exemple, en Python, ou même en Bash, on pourrait définir le marqueur comme suit :

```bash
TO_DEL_PATTERN=\#del
```

## Procédure à Suivre

Pour concevoir un exercice auto-évalué VPL grâce au plugin, il est recommandé de suivre la procédure suivante :
- **Écrire sa solution dans votre langage** à la racine du projet dans des fichiers (autant que nécessaire).
- **Ajouter des commentaires spéciaux** pour cacher des fragments de code de la solution ou les remplacer 
par d'autres morceaux de code, comme décrit dans la suite.
- **Rédiger son énoncé** soit directement dans le code dans des commentaires ou dans le fichier `intro.html` du dossier `doc`, comme décrit dans la suite.
- **Écrire des tests unitaires (pour l'auto-évaluation)** dans le fichier `test/testscript.py` comme décrit dans la suite.

### Écriture de la Solution

Les fichiers sources de la solution doivent être placés à la racine du projet. 

### Ajouter des commentaires spéciaux

Pour faire coexister votre solution et la version de l'étudiant, vous pouvez utiliser 
des commentaires spéciaux pour cacher certains fragments de la solution ou pour les 
remplacer par d'autres morceaux de code. 

### Commentaires spéciaux

- **Cacher du Code** : Utilisez `#-#` un pour marquer le début edu code à 
cacher, et un autre pour la fin. Le code entre les deux ne sera pas visible par les étudiants.  
    **Exemple** :  
    ```python
    def mini(a, b):
    #------------------
        if a < b:
            return a
        else:
            return b
    #------------------
    ```
  L'étudiant verra
    ```python
  def mini(a, b):


- **Remplacer du Code** : Pour remplacer un fragment de code par un autre, 
  - utiliser `#-#` pour marquer le début du code à cacher, 
  - puis `#=#` pour désigner où commence le code de remplacement, 
  - et termine par un dernier `#-#` pour marque la fin. 
  - Le code de remplacement doit être commenté (précédé de `#`) et sera décommenté dans la version étudiante.  
    **Exemple** :  
    ```python
    def mini(a, b):
    #------------------
        if a < b:
            return a
        else:
            return b
    #xxxxxxxxxxxxxxxxxx
        # # Attention les commentaires pour les étudiants en début de ligne doivent être commentés deux fois (comme celui-là).
        # pass # TODO : écrire le code de la fonction mini à la place de pass.
    #------------------
      ```
  L'étudiant verra
    ```python
    def mini(a, b):
        # Attention les commentaires pour les étudiants en début de ligne doivent être commentés deux fois (comme celui-là).
        pass # TODO : écrire le code de la fonction mini à la place de pass.
### Rédaction de l'énoncé

L'énoncé de l'exercice peut être rédigé de deux manières non exclusives :
1. soit directement dans le code avec des commentaires.  
  **Exemple** :
```python
########################################################################################
#  Écrire la fonction mini(a, b) qui retourne le plus petit des deux entiers a et b
########################################################################################
def mini(a, b):
#------------------
    if a < b:
        return a
    else:
        return b
#xxxxxxxxxxxxxxxxxx
    # # Attention les commentaires pour les étudiants en début de ligne doivent être commentés deux fois (comme celui-là).
    # pass # TODO : écrire le code de la fonction mini à la place de pass.
#------------------

```
2. soit dans un fichier `intro.html` dans le dossier `doc`.  
  **Exemple** :
```html
<ol>
  <li>Écrire une fonction <strong>mini(a, b)</strong> qui retourne le plus petit d'entre a et b
  <li>Écrire une fonction <strong>maxi(a, b)</strong> qui retourne le plus grand d'entre a et b
</ol>
```
Remarque : le dossier `doc` n'existe pas au départ. Il faut le créer et y ajouter le fichier `intro.html`.

## Écriture des tests d'auto-évaluation

L'instrumentation des tests d'auto-évaluation se fait dans le dossier selon
vos besoins. 

Si vous utilisez le mécanisme de VPL, il suffit de renseigner 
le fichier `test/vpl_evaluate.cases` comme il se doit.

Dans tous les cas, le contenu du dossier `test` sera envoyé à Caseine dans les fichiers
d'exécution du VPL. Vous pouvez ajouter vos propres `vpl_run.sh`, `vpl_evaluate.sh` et
tout autre fichier nécessaire à l'exécution de votre exercice.

Vous pouvez également définir, entre autres choses, sur quoi est basée votre VPL (based on),
grâce à la clé `WS.baseon` dans le fichier `.caseine`.

## Publication sur Caseine

Une fois que tout est prêt, l'exercice peut être publié sur Caseine.

Pour cela, suivez les étapes suivantes :

1. Créez un VPL dans un cours sur Caseine et notez son identifiant (`vpl_id`) et votre clef (`vpl_web_service`) 
   d'identification sur Caseine.
2. Utilisez la commande `caseine --push -t<vpl_web_service> -v<vpl_id>` pour la première publication. 
   - l'option `-t` est inutile si la variable d'environnement `CASEINE_TOKEN` contient votre identifiant.
   - Pour les publications suivantes, l'option `-v` est inutile, car le `vpl_id` est 
     enregistré automatiquement dans le fichier `.caseine`.
   
Et voilà, votre exercice est publié sur Caseine et prêt à être utilisé par vos étudiants.

## Conclusion

Ce template et ce guide README visent à simplifier le processus de création et de publication 
d'exercices sur Caseine, 
en fournissant une structure claire et des outils pour préparer efficacement votre contenu 
pédagogique.

## Support

Pour de l'aide supplémentaire ou des informations, contactez Yvan Maillot.