/*
 * @author : Yvan Maillot (yvan.maillot@uha.fr)
 */
package edu.uha.miage;

import caseine.format.javajunit.Grade;
import caseine.reflect.ReflectUtilities;
import caseine.tags.ClassTestPriority;
import org.junit.Test;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

/**
 *
 * @author yvan
 */
@ClassTestPriority(2)
public class PointTest {

    private Constructor<?> cpoint = null;
    private Field fx = null, fy = null;

    private void getConstructorAndFields() {

        try {
            cpoint = Point.class.getDeclaredConstructor(double.class, double.class);
        } catch (NoSuchMethodException | SecurityException | IllegalArgumentException ex) {
            fail("Constructeur Point(double x, double y) absent ou mal signé");
        }

        try {
            fx = Point.class.getDeclaredField("x");
            fy = Point.class.getDeclaredField("y");
        } catch (NoSuchFieldException | SecurityException ex) {
            fail("Attribut x ou y absent ou mal signé");
        }

    }

    @Test
    @Grade(1)
    public void testConstructor() {
        System.out.println("Test constructeur Point");
        getConstructorAndFields();
        if (fx != null && fy != null && cpoint != null) {

            try {
                double x = -2.5, y = -2;
                for (int i = 0; i < 10; ++i) {
                    Point p = (Point) cpoint.newInstance(x, y);
                    fx.setAccessible(true);
                    assertEquals("Revoir le constructeur de Point",
                            x,
                            fx.getDouble(p),
                            1e-6);
                    fy.setAccessible(true);
                    assertEquals("Revoir le constructeur de Point",
                            y,
                            fy.getDouble(p),
                            1e-6);
                    x += 0.5;
                    y += 0.5;
                }
            } catch (SecurityException | InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
                fail("Revoir le constructeur de Point ");
            }
        }
    }

    @Test
    @Grade(5)
    public void testToString() {
        System.out.println("Test toString");

        testConstructor();
        if (fx != null && fy != null && cpoint != null) {

            try {
                double x = -2.5, y = -2;
                for (int i = 0; i < 10; ++i) {
                    Point p = (Point) cpoint.newInstance(x, y);
                    String value = ((String) ReflectUtilities.getFromMethod(Point.class, p, "toString")).trim().replaceAll("[\\s]*", "");
                    String vref1 = ("(" + x + ", " + y + ")").trim().replaceAll("[\\s]*", "");
                    String vref2 = String.format("(%f, %f)", x, y).trim().replaceAll("[\\s]*", "");
                    if (!value.equals(vref1) && !value.equals(vref2)) {
                        assertEquals("Revoir toString()", vref1, value);
                    }
                    x += 0.5;
                    y += 0.5;
                }
            } catch (SecurityException | InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException ex) {
                fail("Revoir toString()");
            }
        }
    }

}
