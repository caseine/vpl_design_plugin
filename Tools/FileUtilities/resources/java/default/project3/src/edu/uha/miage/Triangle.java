/*
 * @author : Yvan Maillot (yvan.maillot@uha.fr)
 */
package edu.uha.miage;

import caseine.tags.*;

/**
 *
 * @author yvan
 */

@ClassTestPriority(5)
public class Triangle {

    @ToCheck(priority = 30, grade = 1)
    @ToDo("3.01. Les trois segments du triangle : s1, s2, s3")
    private Segment s1, s2, s3;

    @ToCheck(priority = 31, grade = 5)
    @ToDo("3.02. Écrire le constructeur avec ses trois sommets")
    public Triangle(Point p1, Point p2, Point p3) {
        this.s1 = new Segment(p1, p2);
        this.s2 = new Segment(p2, p3);
        this.s3 = new Segment(p3, p1);
    }

    @ToCheck(priority = 32, grade = 2)
    @ToDo("3.03. Écrire getP1() qui retourne le premier sommet de ce triangle")
    public Point getP1() {
        return s1.getP1();
    }

    @ToDo("3.04. Écrire getP2() qui retourne le deuxième sommet de ce triangle")
    @ToCheck(priority = 32, grade = 2)
    public Point getP2() {
        return s2.getP1();
    }

    @ToDo("3.05. Écrire getP3() qui retourne le troisième sommet de ce triangle")
    @ToCheck(priority = 32, grade = 2)
    public Point getP3() {
        return s3.getP1();
    }

    @ToCheck(priority = 33, grade = 2)
    @ToDo("3.06. Écrire setP1() qui met à jour le premier sommet de ce triangle")
    public void setP1(Point p1) {
        this.s1 = new Segment(p1, s2.getP1());
        this.s3 = new Segment(s3.getP1(), p1);
    }

    @ToDo("3.07. Écrire setP2() qui met à jour le deuxième sommet de ce triangle")
    @ToCheck(priority = 33, grade = 2)
    public void setP2(Point p2) {
        this.s1 = new Segment(s1.getP1(), p2);
        this.s2 = new Segment(p2, s3.getP1());
    }

    @ToDo("3.08. Écrire setP3() qui met à jour le troisième sommet de ce triangle")
    @ToCheck(priority = 33, grade = 2)
    public void setP3(Point p3) {
        this.s2 = new Segment(s2.getP1(), p3);
        this.s3 = new Segment(p3, s1.getP1());
    }

    @ToDo("3.05. Écrire perimeter() qui retourne le périmètre de ce triangle")
    @ToCheck(value="Signature of Triangle.perimeter()", priority = 34, grade = 1)
    @ToCompare(value="Behavior of Triangle.perimeter()", priority = 35, grade = 5)
    public double perimeter() {
        return s1.length() + s2.length() + s3.length();
    }

    @ToCheck(value="Signature of Triangle.area()", priority = 36, grade = 1)
    @ToDo("3.06. Écrire area() qui retourne la surface de ce triangle")
    @ToCompare(value="Behavior of Triangle.area()", priority = 37, grade = 5)
    public double area() {
        double a = s1.length();
        double b = s2.length();
        double c = s3.length();
        double p = (a + b + c) / 2;
        return Math.sqrt(p * (p - a) * (p - b) * (p - c));
    }

    @ToDo("3.07. Écrire barycenter() qui retourne le barycentre de ce triangle")
    @ToCheck(value="Signature of Triangle.barycenter()", priority = 38, grade = 1)
    public Point barycenter() {
        Point a = s1.getP1();
        Point b = s2.getP1();
        Point c = s3.getP1();

        return new Point((a.getX() + b.getX() + c.getX()) / 3, (a.getY() + b.getY() + c.getY()) / 3);
    }

    @Override
    @ToCheck(priority = 40, grade = 1)
    @ToDo("3.08. Écrire la méthode toString() une représentation de ce triangle comme demandé dans l'énoncé, "
            + "Par exemple <[(1.2, 3.4) ; (4.5, 5.6)], [(4.5, 5.6) ; (7.5, 8.6)], [(7.5, 8.6) ; (1.2, 3.4)]>")
    public String toString() {
        return "<" + s1.toString() + ", " + s2.toString() + ", " + s3.toString() + ">";
    }
}
