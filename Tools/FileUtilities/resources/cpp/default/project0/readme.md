
# Mode d'emploi du plugin de conception Caseine pour C++

Ce mode d'emploi accompagne le template C++ conçu pour faciliter la création et la publication d'exercices auto-évalués VPL (Virtual Programming Lab) sur la plateforme Caseine. Ce template est généré par le plugin de conception en ligne de commande Caseine via la commande `caseine --gen --lang=cpp`.

**Remarque** : ce template est vierge. Il ne contient pas de code d'exemple.
Il est idéal pour un utilisateur chevronné qui souhaite partir de zéro ou pour "caseinifier" un projet existant. 

## Prérequis

- Installation du plugin Caseine CLI.
- Accès à un cours sur Caseine en tant qu'enseignant.
- Compilateur C++ conforme à la norme C++11 ou supérieure.

## Installation et Génération

Ce template est généré par l'une des commandes suivantes :
```bash
caseine --gen --lang=cpp
```
en étant dans le répertoire où vous souhaitez créer le projet.
```bash
caseine --gen --lang=cpp -p <chemin_du_projet>
```
Il produit une structure de base pour commencer le développement de l'exercice.

Nous appellerons **racine** ou **racine du projet** le répertoire du projet (dans lequel se trouve ce readme).

## Structure du Template

Le template comprend plusieurs dossiers et fichiers clés où vous devrez intégrer votre code, vos tests, et l'énoncé de l'exercice.

- racine
  - .caseine
  - readme.md
- unittests
  - TestMain.cpp
- doc (qui n'existe pas au départ)
  - intro.html
## Principe

Le principe du plugin est de rassembler au même endroit toutes les informations de votre exercice
1. la version de l'étudiant,
2. l'énoncé,
3. la solution,
4. l'auto-évaluation.

Pour finalement publier l'exercice sur Caseine depuis sa machine locale.

## Procédure à Suivre

Pour concevoir un exercice auto-évalué VPL grâce au plugin, il est recommandé de suivre la procédure suivante :
- **Écrire sa solution en C++** à la racine du projet dans des fichiers `.h` et `.cpp` (autant que nécessaire).
- **Ajouter des commentaires spéciaux** pour cacher des fragments de code de la solution ou les remplacer 
par d'autres morceaux de code, comme décrit dans la suite.
- **Rédiger son énoncé** soit directement dans le code dans des commentaires ou dans le fichier `intro.html` du dossier `doc`, comme décrit dans la suite.
- **Écrire des tests unitaires (pour l'auto-évaluation)** dans le fichier `TestMain.cpp` comme décrit dans la suite.

### Écriture de la Solution

Les fichiers sources de la solution doivent être placés à la racine du projet. 
La compilation multi-fichiers est supportée, mais seuls les fichiers `.h` et `.cpp` sont considérés.

### Ajouter des commentaires spéciaux

Pour faire coexister votre solution et la version de l'étudiant, vous pouvez utiliser 
des commentaires spéciaux pour cacher certains fragments de la solution ou pour les 
remplacer par d'autres morceaux de code. 

### Commentaires spéciaux

- **Cacher du Code** : Utilisez `// #-#` un pour marquer le début edu code à 
cacher, et un autre pour la fin. Le code entre les deux ne sera pas visible par les étudiants.  
    **Exemple** :  
    ```cpp
  int mini(int t[], int n) {
  // #-# le code ci-dessous est caché de ici
    int m = t[0];
    for (int i = 1; i < n; ++i) {
       if (t[i] < m) m = t[i];
    }
    return m;
  // #-# à là.
  }
    ```
  L'étudiant verra
    ```cpp
  int mini(int t[], int n) {
  }


- **Remplacer du Code** : Pour remplacer un fragment de code par un autre, 
  - utiliser `// #-#` pour marquer le début du code à cacher, 
  - puis `// #=#` pour désigner où commence le code de remplacement, 
  - et termine par un dernier `// #-#` pour marque la fin. 
  - Le code de remplacement doit être commenté (précédé de `//`) et sera décommenté dans la version étudiante.  
    **Exemple** :  
    ```cpp
    int mini(int t[], int n) {
    // #-# le code ci-dessous est caché de ici
       int m = t[0];
       for (int i = 1; i < n; ++i) {
          if (t[i] < m) m = t[i];
       }
       return m;
    // #=# à là, puis le code ci-dessous est décommenté et livré à l'étudiant
    // // Attention les commentaires pour les étudiants en début de ligne doivent être commentés deux fois (comme celui-là).
    // return -1; // Ceci est un exemple de code de remplacement.
    // #-#
    ```
  L'étudiant verra
    ```cpp
  int mini(int t[], int n) {
    // Attention les commentaires pour les étudiants en début de ligne doivent être commentés deux fois (comme celui-là).
    return -1; // Ceci est un exemple de code de remplacement.
  }
### Rédaction de l'énoncé

L'énoncé de l'exercice peut être rédigé de deux manières non exclusives :
1. soit directement dans le code avec des commentaires.  
  **Exemple** :
```cpp
/**
 * Écrire la fonction mini qui retourne une valeur du tableau t de taille n 
 * qui est inférieure ou égale à toutes les autres valeurs du tableau.
 *
 * @param t un tableau d'entiers de taille n
 * @param n la taille de t
 *
 * precondition : n > 0
 *
 * @return un entier de valeur minimale dans t
 */
int mini(int t[], int n) {
    // #-#
    int m = t[0];
    for (int i = 1; i < n; ++i) {
        if (t[i] < m)
            m = t[i];
    }
    return m;
    // #=#
    // return 0;
    // #-#
}
```
2. soit dans un fichier `intro.html` dans le dossier `doc`.  
  **Exemple** :
```html
<!DOCTYPE html>
<html lang="fr">
</head>
<body>

    <h1>Énoncé de l'exercice</h1>

    <p>
        Vous devez implémenter les fonctions suivantes :
    </p>
    <code>int mini(int[], int);</code><br>
    <code>int maxi(vector<int>);</code><br>
    <code>double moyenne(int t[], int n);</code><br>
    <code>void triangle(int n);</code><br>
    <code>void triangleDroite(int n);</code><br>
    <code>void carreVide(int n);</code>
    
    <p class="h2-like">
    Attention, au départ le fichier est vide. Si vous cherchez à évaluer dans cette situation, il y aura des erreurs de compilations. C'est normal. C'est à vous d'écrire les complètement fonctions pour que ça compile.
    </p>

    <h3><code>int mini(int[], int);</code></h3>
    <p>
        Cette fonction prend en entrée un tableau d'entiers et sa taille, et retourne la valeur minimale dans le tableau.
    </p>

    <h3><code>int maxi(vector<int>);</code></h3>
    <p>
        Cette fonction prend en entrée un vecteur d'entiers et retourne la valeur maximale dans le vecteur.
    </p>

    <p>etc.</p>

</body>
</html>
```
Remarque : le dossier `doc` n'existe pas au départ. Il faut le créer et y ajouter le fichier `intro.html`.

## Écriture des tests d'auto-évaluation

Les tests d'auto-évaluation sont à écrire dans le fichier `TestMain.cpp` du dossier `unittests`. 
Ce fichier existe et est pré-rempli. Il **contient des explications détaillées** sur la manière de faire 
pour évaluer le travail des étudiants.

## Publication sur Caseine

Une fois que tout est prêt, l'exercice peut être publié sur Caseine.

Pour cela, suivez les étapes suivantes :

1. Créez un VPL dans un cours sur Caseine et notez son identifiant (`vpl_id`) et votre clef (`vpl_web_service`) 
   d'identification sur Caseine.
2. Utilisez la commande `caseine --push -t<vpl_web_service> -v<vpl_id>` pour la première publication. 
   - l'option `-t` est inutile si la variable d'environnement `CASEINE_TOKEN` contient votre identifiant.
   - Pour les publications suivantes, l'option `-v` est inutile, car le `vpl_id` est 
     enregistré automatiquement dans le fichier `.caseine`.
   
Et voilà, votre exercice est publié sur Caseine et prêt à être utilisé par vos étudiants.

## Conclusion

Ce template et ce guide README visent à simplifier le processus de création et de publication d'exercices C++ sur Caseine, en fournissant une structure claire et des outils pour préparer efficacement votre contenu pédagogique.

## Support

Pour de l'aide supplémentaire ou des informations, contactez Yvan Maillot.