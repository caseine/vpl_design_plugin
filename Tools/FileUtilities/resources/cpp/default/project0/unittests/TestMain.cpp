#include "cppproducerutilities.h"
#include "linkcincout.h"
#include "utilities.h"
#include "ref_utilities.h"

using namespace std;
/*
* Ce fichier contient les tests unitaires qui évalueront le travail de l'étudiant sur Caséine.
*
* Une fonction de test unitaire
*    - doit commencer par test et
*    - doit retourne un string
*    - peut contenir un paramètre avec une valeur (int ou double) qui sert à pondérer l'exercice.
*
* La chaîne retournée doit être
*    - "OK" en cas de succès ou
*    - toute autre chaîne qui peut être un message d'aide à l'étudiant en cas d'échec.
*
* L'écriture d'un test unitaire se fait comme l'enseignant l'entend.
* Mais il dispose de quelques outils pour l'aider :
*    - des générateurs aléatoires
*    - des fonctions pour rediriger entrée et sortie standard dans des string
*    - accès aux méthodes corrigées (i.e. ref::min).
*      La correction se trouve dans l'espace de nom ref.
*
* Ce fichier peut contenir d'autres fonctions utiles (qui ne commencent pas par test) mais
* ATTENTION, il ne doit pas contenir de fonction main.
*/
/*
* Un exemple de tests unitaires pour vérifier que la fonction mini de l'étudiant remplit son office.
*
* Dans cet exemple, pour montrer cette possibilité, la méthode de référence (celle de l'enseignant), 
* ref::mini est utilisée pour être comparée à celle de l'étudiant. Ce n'est évidemment pas une obligation.
*/


