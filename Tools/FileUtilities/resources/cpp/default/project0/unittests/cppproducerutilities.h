/*
 * Creative commons CC BY-NC-SA 2020 Yvan Maillot <yvan.maillot@uha.fr>
 *
 *     Share - You can copy and redistribute the material in any medium or format
 * 
 *     Adapt - You can remix, transform, and build upon the material 
 * 
 * Under the following terms :
 * 
 *     Attribution - You must give appropriate credit, provide a link to the license, 
 *     and indicate if changes were made. You may do so in any reasonable manner, 
 *     but not in any way that suggests the licensor endorses you or your use. 
 * 
 *     NonCommercial — You may not use the material for commercial purposes. 
 * 
 *     ShareAlike — If you remix, transform, or build upon the material, 
 *     you must distribute your contributions under the same license as the original. 
 * 
 * Notices:    You do not have to comply with the license for elements of 
 *             the material in the public domain or where your use is permitted 
 *             by an applicable exception or limitation. 
 * 
 * No warranties are given. The license may not give you all of the permissions 
 * necessary for your intended use. For example, other rights such as publicity, 
 * privacy, or moral rights may limit how you use the material. 
 * 
 * See <https://creativecommons.org/licenses/by-nc-sa/4.0/>.
 */
#ifndef CPPPRODUCER_CPPPRODUCERUTILITIES_H
#define CPPPRODUCER_CPPPRODUCERUTILITIES_H

#include <thread>
#include <chrono>
#include <regex>
#include <fstream>
#include <iostream>
#include <random>
#include <string>
#include "CPP14Parser.h"
#include "FileParser.h"
#include "StringUtilities.h"
#include "FunctionParser.h"
#include "SelectionFinderVisitor.h"
#include "ParserUtilities.h"
#include "IterationFinderVisitor.h"
#include "linkcincout.h"

#define stdout_as_string(f, s) { streambuf *__old_cout__; stringstream __cout_buf__; __old_cout__ = cout.rdbuf(__cout_buf__.rdbuf()); f; cout.rdbuf(__old_cout__); s = __cout_buf__.str(); }

#define stdin_as_string(f, s) { streambuf *__old_cin__; stringstream __cin_buf__; __cin_buf__ << s; __old_cin__ = cin.rdbuf(__cin_buf__.rdbuf()); f; cin.rdbuf(__old_cin__);}

#define stdinout_as_strings(f, sin, sout) {streambuf *__old_cout__;streambuf *__old_cin__;stringstream __cout_buf__;stringstream __cin_buf__;__cin_buf__ << sin;__old_cout__ = cout.rdbuf(__cout_buf__.rdbuf());__old_cin__ = cin.rdbuf(__cin_buf__.rdbuf());f;cout.rdbuf(__old_cout__);cin.rdbuf(__old_cin__);sout = __cout_buf__.str();}


using namespace std;

string find(string filename, string name);

class killer {
public:
    killer(string msg = "Trop long : probablement une boucle infinie", int ts = 5);
private:
    void static timelimit(string msg, int ts);
};



#endif //CPPPRODUCER_CPPPRODUCERUTILITIES_H
