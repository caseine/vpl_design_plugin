/*
 * Creative commons CC BY-NC-SA 2021 Yvan Maillot <yvan.maillot@uha.fr>
 *
 *     Share - You can copy and redistribute the material in any medium or format
 *
 *     Adapt - You can remix, transform, and build upon the material
 *
 * Under the following terms :
 *
 *     Attribution - You must give appropriate credit, provide a link to the license,
 *     and indicate if changes were made. You may do so in any reasonable manner,
 *     but not in any way that suggests the licensor endorses you or your use.
 *
 *     NonCommercial — You may not use the material for commercial purposes.
 *
 *     ShareAlike — If you remix, transform, or build upon the material,
 *     you must distribute your contributions under the same license as the original.
 *
 * Notices:    You do not have to comply with the license for elements of
 *             the material in the public domain or where your use is permitted
 *             by an applicable exception or limitation.
 *
 * No warranties are given. The license may not give you all of the permissions
 * necessary for your intended use. For example, other rights such as publicity,
 * privacy, or moral rights may limit how you use the material.
 *
 * See <https://creativecommons.org/licenses/by-nc-sa/4.0/>.
 */
 /*
 Ce fichier contient un exemple de code cpp prêt à devenir un vpl.

 Les commentaires // #-# et // #=# permettent de délimiter les zones pour l'étudiants des autres.

 // si #=# est présent, il doit toujours se placer entre deux // #-#

 La partie entre // #-# et // #=# correspond à la solution (corrected files ou fichiers corrigés)
 La partie entre // #=# et // #-# correspond à la version pour l'étudiant (required files ou fichiers requis)

 Si // #=# est absent, la partie entre deux // #-# est enlevée à l'étudiant.

 Remarque : il peut y avoir des espaces entre // # - et =
 */
 #include <iostream>
#include <vector>
using namespace std;

/**
 * Écrire la fonction mini qui retourne un entier de valeur minimale du tableau t de taille n.
 * @param t un tableau d'entiers de taille n
 * @param n la taille de t
 *
 * precondition : n > 0
 *
 * @return un entier de valeur minimale dans t
 */
#include "utilities.h"

int mini(int t[], int n) {
    // #-#
    int m = t[0];
    for (int i = 1; i < n; ++i) {
        if (t[i] < m)
            m = t[i];
    }
    return m;
    // #=#
    // return 0;
    // #-#
}

/**
 * Écrire la fonction maxi qui retourne un entier de valeur maximal dans le vector<int>.
 * @param t un tableau d'entiers de taille n
 * @param n la taille de t
 *
 * precondition : n > 0
 *
 * @return un entier de valeur minimale dans t
 */
int maxi(vector<int> t) {
    // #-#
    int m = t[0];
    for (int i = 1; i < t.size(); ++i) {
        if (t[i] > m)
            m = t[i];
    }
    return m;
    // #=#
    // return 0;
    // #-#
}

double moyenne(int t[], int n) {
    // #-#
    double s = t[0];
    for (int i = 1; i < n; ++i) {
        s += t[i];
    }
    return s / n;
    // #=#
    // return 0.0;
    // #-#
}


/**
 * Écrire la procédure triangle qui affiche sur la sortie standard un "triangle rectangle" de # de taille n.
 * @param n la taille du taille
 *
 *
 * Par exemple triangle(3) doit afficher
 *
 * #
 * ##
 * ###
 *
 * precondition : n >= 0
 *
 */
void triangle(int n) {
    // #-#
    for (int i = 1; i <= n; ++i) {
        for (int j = 0; j < i; ++j) {
            cout << '#';
        }
        cout << endl;
    }
    // #=#
    // // Écrire votre code ici
    // #-#
}


/**
 * Écrire la procédure triangleDroite qui affiche sur la sortie standard un "triangle rectangle" de # de taille n dont la verticale est à droite
 * @param n la taille du taille
 *
 *
 * Par exemple triangleDroite(3) doit afficher
 *
 *   #
 *  ##
 * ###
 *
 * precondition : n >= 0
 *
 */
void triangleDroite(int n) {
    // #-#
    for (int i = 1; i <= n; ++i) {
        for (int j = 0; j < n-i; ++j) {
            cout << ' ';
        }
        for (int j = 0; j < i; ++j) {
            cout << '#';
        }
        cout << endl;
    }
    // #-#
}

/**
 * Écrire la procédure carreVide qui affiche sur la sortie standard un "carre vide" de # de taille n
 * @param n la taille du carre
 *
 *
 * Par exemple
 * carreVide(2) doit afficher
 *
 * ##
 * ##
 *
 *
 * carreVide(3) doit afficher
 *
 * ###
 * # #
 * ###
 *
 * carreVide(5) doit afficher
 *
 * #####
 * #   #
 * #   #
 * #   #
 * #####
 *
 * precondition : n >= 2
 *
 */
void carreVide(int n) {
    // #-#
    // Première ligne
    for (int c = 0; c < n; ++c) {
        cout << '#';
    }
    cout << endl;
    // Les n-2 lignes du "milieu"
    for (int r = 2; r <= n-1; ++r) {
        cout << '#';
        for (int c = 2; c <= n - 1; ++c) {
            cout << ' ';
        }
        cout << '#' << endl;
    }
    // Dernière ligne
    for (int c = 0; c < n; ++c) {
        cout << '#';
    }
    cout << endl;
    // #-#
}

