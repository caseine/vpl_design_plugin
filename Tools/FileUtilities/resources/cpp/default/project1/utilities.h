//#DEL#
/*
 * Creative commons CC BY-NC-SA 2020 Yvan Maillot <yvan.maillot@uha.fr>
 *
 *     Share - You can copy and redistribute the material in any medium or format
 *
 *     Adapt - You can remix, transform, and build upon the material
 *
 * Under the following terms :
 *
 *     Attribution - You must give appropriate credit, provide a link to the license,
 *     and indicate if changes were made. You may do so in any reasonable manner,
 *     but not in any way that suggests the licensor endorses you or your use.
 *
 *     NonCommercial — You may not use the material for commercial purposes.
 *
 *     ShareAlike — If you remix, transform, or build upon the material,
 *     you must distribute your contributions under the same license as the original.
 *
 * Notices:    You do not have to comply with the license for elements of
 *             the material in the public domain or where your use is permitted
 *             by an applicable exception or limitation.
 *
 * No warranties are given. The license may not give you all of the permissions
 * necessary for your intended use. For example, other rights such as publicity,
 * privacy, or moral rights may limit how you use the material.
 *
 * See <https://creativecommons.org/licenses/by-nc-sa/4.0/>.
 */

 /*
 Ce fichier contient le header du code cpp qui l'accompagne.

 Il commence par // # DEL # pour indiquer que l'étudiant n'en a pas besoin.

 Cependant, il est nécessaire de déclarer les fonctions de utilities.cpp pour target/caseine-output/ef/TestMain.cpp
 même si l'étudiant n'en a pas besoin.
 */
#ifndef _UTIL_H_
#define _UTIL_H_

#include <iostream>
#include <vector>

using namespace std;

int mini(int[], int);
int maxi(vector<int>);
double moyenne(int t[], int n);
void triangle(int n);
void triangleDroite(int n);
void carreVide(int n);

#endif

// end of file