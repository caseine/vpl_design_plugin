#include "cppproducerutilities.h"
#include "linkcincout.h"
#include "utilities.h"
#include "ref_utilities.h"

using namespace std;

/*
* Ce fichier contient les tests unitaires qui évalueront le travail de l'étudiant sur Caséine.
*
* Une fonction de test unitaire
*    - doit commencer par test et
*    - doit retourner un string
*    - peut contenir un paramètre avec une valeur (int ou double) qui sert à pondérer l'exercice.
*
* La chaîne retournée doit être
*    - "OK" en cas de succès ou
*    - toute autre chaîne qui peut être un message d'aide à l'étudiant en cas d'échec.
*
* L'écriture d'un test unitaire se fait comme l'enseignant l'entend.
* Mais il dispose de quelques outils pour l'aider :
*    - des générateurs aléatoires
*    - des fonctions pour rediriger entrée et sortie standard dans des stringr
*    - accès aux méthodes corrigées (i.e. ref::min).
*      La correction se trouve dans l'espace de nom ref.
*
* Ce fichier peut contenir d'autres fonctions utiles (qui ne commencent pas par test) mais
* ATTENTION, il ne doit pas contenir de fonction main.
*/

/*
* Un exemple de tests unitaires pour vérifier que la fonction mini de l'étudiant remplit son office.
*
* Dans cet exemple, pour montrer cette possibilité, la méthode de référence (celle de l'enseignant),
* ref::mini est utilisée pour être comparée à celle de l'étudiant. Ce n'est évidemment pas une obligation.
*/
string testFonctionRechercheDuMin(int grade = 1) {
    random_device rd;
    mt19937 gen(rd());
    // Pour tirer "au hasard" des nombres entre -10 et 10
    uniform_int_distribution<> distrib(-10, 10);
    // Pour tirer "au hasard" des nombres entre 10 et 20 (strictement positifs)
    uniform_int_distribution<> spositive_distrib(10, 20);
    // Pour tirer "au hasard" des nombres entre -10 et -20 (strictement négatifs)
    uniform_int_distribution<> snegative_distrib(-20, -10);
    // Quelques jeux de tests en "dur"
    {
        int t[] = {0, 2, 8, 9, 4, 7, 1};
        if (mini(t, 6) != 0)
            return "Revoir votre fonction min()";
    }
    {
        int t[] = {0, 1, 2, 3};
        if (mini(t, 3) != 0)
            return "Revoir votre fonction min()";
    }
    {
        int t[] = {1, 2, -3};
        if (mini(t, 3) != -3)
            return "Revoir votre fonction min()";
    }
    // Pour varier les plaisirs, quels tests tirés au hasard (mais néanmoins pertinents)
    // Test pour des tableaux de 1 à 10
    for (int n = 1; n <= 10; ++n) {
        int t[n];
        // Un tableau dont les valeurs sont dans [-10, 10]
        for (int i = 0; i < n; ++i) {
            t[i] = distrib(gen);
        }
        int min = mini(t, n);
        int ref_min = ref::mini(t, n);
        if (min != ref_min) {
            // Si différent de la fonction de référence, il y a un problème
            return "Revoir votre fonction min()";
        }
        // Un tableau dont les valeurs sont dans [10, 20]
        // Dans le cas où l'étudiant fait l'erreur de commencer avec une valeur mini égale à 0
        for (int i = 0; i < n; ++i) {
            t[i] = spositive_distrib(gen);
        }
        min = mini(t, n);
        ref_min = ref::mini(t, n);
        if (min != ref_min) {
            stringstream ss;
            ss << "Revoir votre fonction min() : minimum trouvé : " << min
               << ". C'est incorrect pour certains jeux de test";
            return ss.str();
        }
        // Un tableau dont les valeurs sont dans [-20, -10]
        // Juste comme ça
        for (int i = 0; i < n; ++i) {
            t[i] = snegative_distrib(gen);
        }
        min = mini(t, n);
        ref_min = ref::mini(t, n);
        if (min != ref_min) {
            return "Revoir votre fonction min()";
        }
    }
    // Tous les tests sont passés.
    return "OK";
}
/*
 * Cette fois on veut donner plus d'indice à l'étudiant.
 * C'est la raison des deux fonctions suivantes.
 */
/**
 * Pour envoyer un vector dans un flux afin de proposer un joli affichage des valeurs du tableau séparées deux à deux
 * par une virgule et encadré par des accolades.
 */
ostream &operator<<(ostream &os, vector<int> v) {
    os << "{";
    if (v.size() > 0) {
        os << v[0];
        for (int i = 1; i < v.size(); ++i) {
            os << ", " << v[i];
        }
    }
    os << "}";
    return os;
}
/*
 * Une fonction qui construit une chaîne à partir d'un vecteur (v), d'une valeur trouvée (found) et d'une valeur
 * attendue (expected) pour qu'en cas d'erreur l'étudiant puisse savoir le jeu de test employé et son résultat.
 */
string msg(const vector<int> &v, double found, double expected) {
    stringstream ss;
    ss << "K0 : dans " << v << endl;
    ss << "Le résultat devrait être " << expected << endl;
    ss << "Mais " << found << " a été trouvé" << endl;
    return ss.str();
}
/*
* C'est le deuxième test unitaire.
*
* Il vaut 1.5 fois le précédent (juste pour montrer qu'on peut utiliser des nombres à virgules)
* 
*/

string testFonctionRechercheDuMax(double grade = 1.5) {
     random_device rd;
    mt19937 gen(rd());
    uniform_int_distribution<> distrib(-10, 10);
    uniform_int_distribution<> spositive_distrib(10, 20);
    uniform_int_distribution<> snegative_distrib(-20, -10);
    // Quelques jeux de tests en "dur"
    {
        vector<int> v{0, 2, 8, 9, 4, 7, 1};
        int m = maxi(v);
        if (m != 9)
            return msg(v, m, 9);
    }
    {
        vector<int> v{0, 1, 2, 3};
        int m = maxi(v);
        if (m != 3)
            return msg(v, m, 3);
    }
    {
        vector<int> v{1, 2, -3};
        int m = maxi(v);
        if (m != 2)
            return msg(v, m, 2);
    }
    for (int n = 1; n <= 10; ++n) {
        {
            vector<int> v;
            for (int i = 0; i < n; ++i) {
                v.push_back(distrib(gen));
            }
            int max = maxi(v);
            int ref_max = ref::maxi(v);
            if (max != ref_max) {
                return msg(v, max, ref_max);
            }
        }
        {
            vector<int> v;
            for (int i = 0; i < n; ++i) {
                v.push_back(spositive_distrib(gen));
            }
            int max = maxi(v);
            int ref_max = ref::maxi(v);
            if (max != ref_max) {
                return msg(v, max, ref_max);
            }
        }
        {
            vector<int> v;
            for (int i = 0; i < n; ++i) {
                v.push_back(snegative_distrib(gen));
            }
            int max = maxi(v);
            int ref_max = ref::maxi(v);
            if (max != ref_max) {
                return msg(v, max, ref_max);
            }
        }
    }
    return "OK";
}
vector<int> asvector(int *t, int n) {
    vector<int> v;
    for (int i = 0; i < n; ++i) {
        v.push_back(t[i]);
    }
    return v;
}


string testFonctionCalculMoyenne(double grade = 1.5) {
     random_device rd;
    mt19937 gen(rd());
    uniform_int_distribution<> distrib(0, 20);
    int n = 20;
    int t[n];
    for (int k = 0; k < 5; ++k) {
        for (int i = 0; i < n; ++i) {
            t[i] = distrib(gen);
        }
        double moy = moyenne(t, n);
        double ref_moy = ref::moyenne(t, n);
        if (moy != ref_moy) {
            return msg(asvector(t, n), moy, ref_moy);
        }
    }
    return "OK";
}
/*
 * Trois exemples de tests de fonctions de "dessin" dans la console.
 * Surtout pour montrer l'emploi de stdout_as_string afin de comparer les résultats afficher dans la console
 */

string testProcedureTriangle(double grade = 3) {
     for (int i = 0; i < 10; ++i) {
        string ref, std;
        stdout_as_string(triangle(i), std);
        stdout_as_string(ref::triangle(i), ref);
        if (std != ref) {
            stringstream ss;
            ss << "KO : pour n = " << i << endl;
            ss << "Attendu : " << endl;
            ss << ref << endl;
            ss << "Mais : " << endl;
            ss << std << endl;
            return ss.str();
        }
    }
    return "OK";
}

string testProcedureTriangleHauteurDroite(double grade = 3) {
     for (int i = 0; i < 10; ++i) {
        string ref, std;
        stdout_as_string(triangleDroite(i), std);
        stdout_as_string(ref::triangleDroite(i), ref);
        if (std != ref) {
            stringstream ss;
            ss << "KO : pour n = " << i << endl;
            ss << "Attendu : " << endl;
            ss << ref << endl;
            ss << "Mais : " << endl;
            ss << std << endl;
            return ss.str();
        }
    }
    return "OK";
}

string testProcedureCarreVide(double grade = 3) {
     for (int i = 2; i < 10; ++i) {
        string ref, std;
        stdout_as_string(carreVide(i), std);
        stdout_as_string(ref::carreVide(i), ref);
        if (std != ref) {
            stringstream ss;
            ss << "KO : pour n = " << i << endl;
            ss << "Attendu : " << endl;
            ss << ref << endl;
            ss << "Mais : " << endl;
            ss << std << endl;
            return ss.str();
        }
    }
    return "OK";
}
/*
 * Pour montrer un exemple d'emploi du parser pour vérifier la qualité d'écriture d'une fonction
 *
 * Pour écrire sa fonction carreVide(n), l'étudiant peut être tenté d'écrire ceci
 *
 void carreVide(int n) {
 for (int i = 0; i < n; ++i) {
     for(int j = 0; j < n; ++j) {
         if (i == 0 || i == n-1 || j == 0 || j == n-1)
             cout << '#';
         else
             cout << ' ';
     }
     cout << endl;
 }
}
 * C'est fonctionnellement correct et en apparence "malin".
 * Mais pas tant que ça car il y a des tests inutiles.
 * La solution peut s'écrire sans l'emploi de if
 *
 * On utilise ici le parseur pour vérifier l'absence de if
 *
 */
string testQualiteCarreVide(double grade = 3) {
            // Tout d'abord on teste que la fonction fait le boulot
    if (testProcedureCarreVide() != "OK")
        return "KO";
    // Ensuite on cherche les "if" et les "switch" dans la fonction grâce au parseur
    FileParser fileParser("utilities.cpp");
    CPP14Parser::FunctionDefinitionContext *parsed = fileParser.function("carreVide");
    FunctionParser functionParser(parsed);
    if (functionParser.getSelections(true).size() != 0) {
        return "KO : il ne devrait pas y avoir de if ni de switch dans votre fonction";
    }
    return "OK";
}


string testQualiteTriangles(double grade = 3) {
           	// ToDo Write a test
       if (testProcedureTriangle() != "OK")
        return "KO";
       if (testProcedureTriangleHauteurDroite() != "OK")
        return "KO";
    // Ensuite on cherche les "if" et les "switch" dans la fonction grâce au parseur
    {
        FileParser fileParser("utilities.cpp");
        CPP14Parser::FunctionDefinitionContext *parsed = fileParser.function("triangle");
        FunctionParser functionParser(parsed);
        if (functionParser.getSelections(true).size() != 0) {
            return "KO : il ne devrait pas y avoir de if ni de switch dans la procédure triangle";
        }
    }
    {
        FileParser fileParser("utilities.cpp");
        CPP14Parser::FunctionDefinitionContext *parsed = fileParser.function("triangleDroite");
        FunctionParser functionParser(parsed);
        if (functionParser.getSelections(true).size() != 0) {
            return "KO : il ne devrait pas y avoir de if ni de switch dans la procédure triangle";
        }
    }
    return "OK";
}

