#ifndef LINKCINCOUT_H
#define LINKCINCOUT_H

/*
 * Creative commons CC BY-NC-SA 2022 Stéphane Rivière <stephane.riviere@uha.fr>
 * inspired from an idea of Yvan Maillot <yvan.maillot@uha.fr>
 *
 *     Share - You can copy and redistribute the material in any medium or format
 *
 *     Adapt - You can remix, transform, and build upon the material
 *
 * Under the following terms :
 *
 *     Attribution - You must give appropriate credit, provide a link to the license,
 *     and indicate if changes were made. You may do so in any reasonable manner,
 *     but not in any way that suggests the licensor endorses you or your use.
 *
 *     NonCommercial — You may not use the material for commercial purposes.
 *
 *     ShareAlike — If you remix, transform, or build upon the material,
 *     you must distribute your contributions under the same license as the original.
 *
 * Notices:    You do not have to comply with the license for elements of
 *             the material in the public domain or where your use is permitted
 *             by an applicable exception or limitation.
 *
 * No warranties are given. The license may not give you all of the permissions
 * necessary for your intended use. For example, other rights such as publicity,
 * privacy, or moral rights may limit how you use the material.
 *
 * See <https://creativecommons.org/licenses/by-nc-sa/4.0/>.
 */

#include<string>
#include<iostream>

/**
 * From here cin reads from the given string instead of the standard input
 * The content read will be removed from the string when unlink_cin is called
 * @param s the string from which cin reads
 */
void link_cin_to(std::string& s);

/**
 * From here cin reads from the given string instead of the standard input
 * @param s the string from which cin reads
 */
void link_cin_to_const(const std::string& s);

/**
 * From here cin reads from the given input stream
 * instead of the standard input
 * @param ist the input stream from which cin reads
 */
void link_cin_to(std::istream& ist);

/**
 * From here cin reads once again from the standard input
 */
void unlink_cin();

/**
 * From here cout writes into the given string instead of the standard ouput:
 * the given string will be updated when unlink_cout() is called
 * @param s the string to which cout writes
 */

void link_cout_to(std::string& s);

/**
 * From here cout writes into the given output stream
 * instead of the standard input
 * @param ost the output stream to which cout writes
 */

void link_cout_to(std::ostream& ost);

/**
 * From here cout writes once again into the standard input
 */

void unlink_cout();

#endif
