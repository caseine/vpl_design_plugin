/*
 * Copyright 2019: Yvan Maillot
 * This software is part of the Caseine project.
 * This software was developped with the support of the following organizations:
 * Université de Haute-Alsace
 * Institut Polytechnique de Grenoble
 * 
 * 
 * This file is part of the VPL Design Tool.
 * The VPL Design Tool is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * The VPL Design Tool is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with the VPL Design Tool.  If not, see <https://www.gnu.org/licenses/>.
 */
package caseine.reflect;

import static caseine.reflect.ReflectUtilities.randomValue;
import com.github.javaparser.ast.expr.InstanceOfExpr;
import com.github.javaparser.ast.stmt.BlockStmt;
import com.github.javaparser.ast.visitor.VoidVisitorAdapter;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * Assists in checking the correct writing of the equals and hashcode methods of
 * a class C by knowing if
 *
 *
 * <ol>
 * <li>equals method of the class C is declared <strong>with Object
 * parameter</strong></li>
 * <li>hashCode method of the class C is declared</li>
 * <li>c.equals(null) is false, where c is of type C</li>
 * <li>c.equals(c) is true, where c is of type C</li>
 * <li>equals method existe and it not use instanceOf</li>
 * </ol>
 *
 * Also allows you to retrieve parsed elements.
 *
 * Beware! this is done by trying to instantiate C and use equals on this
 * instance. So 3 and 4 are not relevant if it is unable to instantiate C.
 *
 * @param <C> The class to check
 */
/**
 * A class to check if - equals method of the class C is declared with Object
 * parameter - hashcode method of a class T is well declared - t.equals(null) is
 * false, where t is of type T - t.equals(t) is true, where t is of type T
 *
 *
 * @param <C> The class to check
 */
public class EqualChecker<C> {

    private final Class<C> checkedClass;
    private BlockStmt bodyToCheck;
    private BlockStmt bodyOfEquals;
    private BlockStmt bodyOfHashCode;
    private boolean equalsDeclared;
    private boolean hashcodeDeclared;
    private boolean ableToInstantiate;
    private boolean notEqualToNull;
    private boolean equalToItSelf;
    private boolean notUseInstanceOf;
    private Method equals;
    private Method hashCode;

    private static class EqualsVisitor extends VoidVisitorAdapter<Void> {

        private Boolean notUseInstanceOf = true;

        @Override
        public void visit(InstanceOfExpr n, Void v) {
            super.visit(n, v);
            notUseInstanceOf = false;
        }
    }

    @Deprecated
    public EqualChecker(Class<C> classToCheck, BlockStmt bodyToCheck) {
        this.checkedClass = classToCheck;
        this.bodyToCheck = this.bodyOfEquals = bodyToCheck;
        this.bodyOfHashCode = null;
        this.equalsDeclared = false;
        this.hashcodeDeclared = false;
        this.ableToInstantiate = false;
        this.notEqualToNull = false;
        this.equalToItSelf = false;
        this.notUseInstanceOf = true;

        this.equals = null;

        check();
    }

    /**
     *
     * @param classToCheck The object class of class C whose equals et hashCode
     * methods we want to check
     */
    public EqualChecker(Class<C> classToCheck) {
        this(classToCheck, null);
    }

    private <C> void check() {
        try {
            equals = checkedClass.getDeclaredMethod("equals", Object.class);
            equalsDeclared = true;
            if (this.bodyOfEquals == null) {
                bodyOfEquals = bodyToCheck = ParserWithReflectUtilities.find(equals).getBody().get();
            }
        } catch (NoSuchMethodException | SecurityException ex) {
            equalsDeclared = false;
        }

        try {
            hashCode = checkedClass.getDeclaredMethod("hashCode");
            hashcodeDeclared = true;
            if (this.bodyOfHashCode == null) {
                bodyOfHashCode = ParserWithReflectUtilities.find(hashCode).getBody().get();
            }
        } catch (NoSuchMethodException | SecurityException ex) {
            hashcodeDeclared = false;
        }

        if (equalsDeclared) {
            C obj = null;
            try {
                obj = (C) randomValue(checkedClass);
                ableToInstantiate = true;
            } catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException ex) {
                ableToInstantiate = false;
            }
            if (obj != null) {
                try {
                    notEqualToNull = !((boolean) equals.invoke(obj, (Object) null));
                } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
                    notEqualToNull = false;
                }
                try {
                    equalToItSelf = (boolean) equals.invoke(obj, obj);
                } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
                    equalToItSelf = false;
                }
            }
            if (bodyOfEquals != null) {
                EqualsVisitor equalsVisitor = new EqualsVisitor();
                bodyOfEquals.accept(equalsVisitor, null);
                notUseInstanceOf = equalsVisitor.notUseInstanceOf;
            } else {
                notUseInstanceOf = true;
            }
        }
    }

    /**
     *
     * @return Object class of C
     */
    public Class<C> getCheckedClass() {
        return checkedClass;
    }

    /**
     *
     * @return equals method with an only Object parameter exists
     */
    public boolean isEqualsDeclared() {
        return equalsDeclared;
    }

    /**
     *
     * @return hashCode method with no parameter exists
     */
    public boolean isHashcodeDeclared() {
        return hashcodeDeclared;
    }

    /**
     *
     * @return It was possible to instantiate T
     */
    public boolean isAbleToInstantiate() {
        return ableToInstantiate;
    }

    /**
     *
     * @return c.equals(null) is false, where c is of type C
     */
    public boolean isNotEqualToNull() {
        return notEqualToNull;
    }

    /**
     *
     * @return c.equals(c) is true, where c is of type C
     */
    public boolean isEqualToItSelf() {
        return equalToItSelf;
    }

    /**
     *
     * @return equals does not use instanceOf (relevant if it exists)
     */
    public boolean isNotUseInstanceOf() {
        return notUseInstanceOf;
    }

    /**
     *
     * @return the reflected object class of the clone method or null if does
     * not exist
     *
     */
    public Method getEquals() {
        return equals;
    }

    /**
     *
     * @return the reflected object class of the hashCode method or null if does
     * not exist
     *
     */
    public Method getHashCode() {
        return hashCode;
    }

    @Deprecated
    public BlockStmt getBodyToCheck() {
        return bodyToCheck;
    }

    /**
     *
     * @return The parsed body of the equals method of C is it exists or null
     */
    public BlockStmt getBodyOfEquals() {
        return bodyOfEquals;
    }

    /**
     *
     * @return The parsed body of the hashCode method of C is it exists or null
     */
    public BlockStmt getBodyOfHashCode() {
        return bodyOfHashCode;
    }

}
