/*
 * Creative commons CC BY-NC-SA 2020 Yvan Maillot <yvan.maillot@uha.fr>
 *
 *     Share - You can copy and redistribute the material in any medium or format
 *
 *     Adapt - You can remix, transform, and build upon the material
 *
 * Under the following terms :
 *
 *     Attribution - You must give appropriate credit, provide a link to the license,
 *     and indicate if changes were made. You may do so in any reasonable manner,
 *     but not in any way that suggests the licensor endorses you or your use.
 *
 *     NonCommercial — You may not use the material for commercial purposes.
 *
 *     ShareAlike — If you remix, transform, or build upon the material,
 *     you must distribute your contributions under the same license as the original.
 *
 * Notices:    You do not have to comply with the license for elements of
 *             the material in the public domain or where your use is permitted
 *             by an applicable exception or limitation.
 *
 * No warranties are given. The license may not give you all of the permissions
 * necessary for your intended use. For example, other rights such as publicity,
 * privacy, or moral rights may limit how you use the material.
 *
 * See <https://creativecommons.org/licenses/by-nc-sa/4.0/>.
 */
package caseine.reflect;

import com.github.javaparser.StaticJavaParser;
import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.body.ConstructorDeclaration;
import com.github.javaparser.ast.body.MethodDeclaration;
import com.github.javaparser.ast.body.TypeDeclaration;
import com.github.javaparser.ast.type.Type;
import com.github.javaparser.ast.visitor.VoidVisitorAdapter;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.nio.file.Path;

/*
 *
 * @author Yvan Maillot <yvan.maillot@uha.fr>
 */
public class ParserWithReflectUtilities {

    public static boolean equals(Type parsedType, Class<?> javaType) {
        if (parsedType.isPrimitiveType() || parsedType.isVoidType()) {
            return parsedType.toString().equals(javaType.toString());
        } else if (parsedType.isArrayType() && javaType.isArray()) {
            return equals(parsedType.asArrayType().getComponentType(), javaType.getComponentType());
        } else if (parsedType.isReferenceType()) {
            // java.awt.Color and Color (ans x.y.z.Color) are to be considered identical
            // just as Class, Class<?>
            String stringParsedType = parsedType.toString().replaceAll("<[^>]+>", "");
            return stringParsedType.equals(javaType.getSimpleName())
                    || stringParsedType.equals(javaType.getName());
        } else {
            return false;
        }
    }

    public static CompilationUnit parse(Class<?> aClass) {
        try {
            return StaticJavaParser.parse(new File(aClass.getName().replaceAll("\\.", "/") + ".java").toPath());
        } catch (IOException ex) {
            try {
                return StaticJavaParser.parse(new File("src/" + aClass.getName().replaceAll("\\.", "/") + ".java").toPath());
            } catch (IOException ex2) {
                return null;
            }
        }
    }

    public static CompilationUnit parse(Path p) {
        try {
            return StaticJavaParser.parse(p);
        } catch (IOException ex) {
            return null;
        }
    }

    public static class OneObjectContainer<T> {

        private T ref;

        public OneObjectContainer() {
            this(null);
        }

        public OneObjectContainer(T ref) {
            this.ref = ref;
        }

        public T get() {
            return ref;
        }

        public void set(T ref) {
            this.ref = ref;
        }

    }

    private static class ConstructorFinder extends VoidVisitorAdapter<ParserWithReflectUtilities.OneObjectContainer<ConstructorDeclaration>> {

        private Class<?>[] params;
        private String[] genericTypes = new String[0];

        public ConstructorFinder(Class<?>... params) {
            this.params = params;
        }

        public void setGenericTypes(String[] genericTypes) {
            this.genericTypes = genericTypes;
        }
        public void setGenericType(String genericType) {
            this.genericTypes = new String[1];
            genericTypes[0] = genericType;
        }

        /*public ConstructorFinder(String name) {
            this(name, new Class<?>[]{});
        }*/
        public ConstructorFinder(Constructor method) {
            this(method.getParameterTypes());
            //method.getDeclaringClass().getPackage();

        }

        @Override
        public void visit(ConstructorDeclaration n, ParserWithReflectUtilities.OneObjectContainer<ConstructorDeclaration> arg) {
            if (paramsAreSame(n)) {
                arg.set(n);
            }
        }

        private boolean paramsAreSame(ConstructorDeclaration mDecl) {
            int sizeOfParams = mDecl.getParameters().size();
            if (sizeOfParams != params.length) {
                return false;
            }
            for (int i = 0; i < sizeOfParams; ++i) {
                if (mDecl.getParameter(i).isVarArgs()) {
                    mDecl.setParameter(i, StaticJavaParser.parseParameter(mDecl.getParameter(i).toString().replace("...", "[]")));
                }
                for (String genericType : genericTypes) {
                    mDecl.setParameter(i, StaticJavaParser.parseParameter(mDecl.getParameter(i).toString().replaceAll("^"+genericType+" ", "java.lang.Object ")));

                    mDecl.setParameter(i, StaticJavaParser.parseParameter(mDecl.getParameter(i).toString().replaceAll("^"+genericType+"\\[\\] ", "java.lang.Object[] ")));

                }
                if (!ParserWithReflectUtilities.equals(mDecl.getParameter(i).getType(), params[i])) {
                    return false;
                }
            }
            return true;
        }

        private boolean paramsAreSame(MethodDeclaration mDecl) {
            int sizeOfParams = mDecl.getParameters().size();
            if (sizeOfParams != params.length) {
                return false;
            }
            for (int i = 0; i < sizeOfParams; ++i) {
                if (mDecl.getParameter(i).isVarArgs()) {
                    mDecl.setParameter(i, StaticJavaParser.parseParameter(mDecl.getParameter(i).toString().replace("...", "[]")));
                }
                if (!ParserWithReflectUtilities.equals(mDecl.getParameter(i).getType(), params[i])) {
                    return false;
                }
            }
            return true;
        }
    }

    private static class MethodFinder extends VoidVisitorAdapter<ParserWithReflectUtilities.OneObjectContainer<MethodDeclaration>> {

        private String name;
        private Class<?>[] params;

        public MethodFinder(String name, Class<?>... params) {
            this.name = name;
            this.params = params;
        }

        public MethodFinder(String name) {
            this(name, new Class<?>[]{});
        }

        public MethodFinder(Method method) {
            this(method.getName(), method.getParameterTypes());
            //method.getDeclaringClass().getPackage();

        }

        @Override
        public void visit(MethodDeclaration n, ParserWithReflectUtilities.OneObjectContainer<MethodDeclaration> arg) {
            if (name.equals(n.getNameAsString()) && paramsAreSame(n)) {
                arg.set(n);
            }
        }

        private boolean paramsAreSame(MethodDeclaration mDecl) {
            int sizeOfParams = mDecl.getParameters().size();
            if (sizeOfParams != params.length) {
                return false;
            }
            for (int i = 0; i < sizeOfParams; ++i) {
                if (mDecl.getParameter(i).isVarArgs()) {
                    mDecl.setParameter(i, StaticJavaParser.parseParameter(mDecl.getParameter(i).toString().replace("...", "[]")));
                }
                if (!ParserWithReflectUtilities.equals(mDecl.getParameter(i).getType(), params[i])) {
                    return false;
                }
            }
            return true;
        }
    }

    /**
     * Get the parsed body of a method
     *
     * @param c the class where the method is
     * @param methodName the method name
     * @param params the method parameters
     * @return the body of method in class c named methodName with params for
     * parametres or null if it does not exist.
     */
    public static MethodDeclaration find(Class<?> c, String methodName, Class<?>... params) {
        OneObjectContainer<MethodDeclaration> ooc = new OneObjectContainer<>();
        CompilationUnit cu = parse(c);
        cu.accept(new MethodFinder(methodName, params), ooc);
        return ooc.get();
    }

    /**
     * @param c the object class of the class whose the constructor body is needed
     * @param params the params of the constructor
     * @return the body of constructor in class c with params as parameters or
     * null if it does not exist.
     */
    public static ConstructorDeclaration find(Class<?> c, Class<?>... params) {
        OneObjectContainer<ConstructorDeclaration> ooc = new OneObjectContainer<>();
        CompilationUnit cu = parse(c);
        ConstructorFinder constructorFinder = new ConstructorFinder(params);

        cu.accept(constructorFinder, ooc);
        return ooc.get();
    }

    /**
     * @param c the object class of the class whose the constructor body is needed
     * @param params the params of the constructor
     * @return the body of constructor in class c with params as parameters or
     * null if it does not exist.
     */
    public static ConstructorDeclaration findWithGeneric(Class<?> c, String genericType, Class<?>... params) {
        OneObjectContainer<ConstructorDeclaration> ooc = new OneObjectContainer<>();
        CompilationUnit cu = parse(c);
        ConstructorFinder constructorFinder = new ConstructorFinder(params);
        constructorFinder.setGenericType(genericType);
        cu.accept(constructorFinder, ooc);
        return ooc.get();
    }



    public static TypeDeclaration find(Class<?> c) {
        CompilationUnit cu = parse(c);
        return cu.getPrimaryType().get();
    }

    /**
     * @param method we want the body
     * @return the body of method or null if it does not exist.
     */
    public static MethodDeclaration find(Method method) {
        return find(method.getDeclaringClass(), method.getName(), method.getParameterTypes());
        /*OneObjectContainer<MethodDeclaration> ooc = new OneObjectContainer<>();
        CompilationUnit cu = parse(method.getDeclaringClass());
        cu.accept(new MethodFinder(method), ooc);
        return ooc.get();*/
    }

    /**
     * @param constructor we want the body
     * @return the body of constructor or null if it does not exist.
     */
    public static ConstructorDeclaration find(Constructor constructor) {
        return find(constructor.getDeclaringClass(), constructor.getParameterTypes());
        /*OneObjectContainer<MethodDeclaration> ooc = new OneObjectContainer<>();
        CompilationUnit cu = parse(constructor.getDeclaringClass());
        cu.accept(new MethodFinder(constructor), ooc);
        return ooc.get();*/
    }

    /**
     * @param constructor we want the body
     * @return the body of constructor or null if it does not exist.
     */
    public static ConstructorDeclaration findWithGeneric(Constructor constructor, String genericType) {
        return findWithGeneric(constructor.getDeclaringClass(), genericType, constructor.getParameterTypes());
    }
}
