/*
 * Copyright 2019: Yvan Maillot
 * This software is part of the Caseine project.
 * This software was developped with the support of the following organizations:
 * Université de Haute-Alsace
 * Institut Polytechnique de Grenoble
 * 
 * 
 * This file is part of the VPL Design Tool.
 * The VPL Design Tool is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * The VPL Design Tool is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with the VPL Design Tool.  If not, see <https://www.gnu.org/licenses/>.
 */
package caseine.reflect;

import com.github.javaparser.ast.expr.ObjectCreationExpr;
import com.github.javaparser.ast.stmt.BlockStmt;
import com.github.javaparser.ast.visitor.VoidVisitorAdapter;
import java.lang.reflect.Method;
import java.util.Arrays;

/**
 * Assists in checking the correct writing of the clone method of a class C by
 * knowing if
 * <p>
 *
 * <ul>
 * <li>C implements or not Cloneable</li>
 * <li>clone is not declared or well declared</li>
 * <li>clone returns C</li>
 * <li>clone does not throw CloneNotSupportedException</li>
 * <li>clone does not use new</li>
 * </ul>
 *
 * Also allows you to retrieve parsed elements.
 *
 * @param <C> The class to check
 */
public class CloneChecker<C> {

    private final Class<C> checkedClass;
    private BlockStmt bodyToCheck;
    private boolean cloneDeclared;
    private boolean interfaceCloneableImplemented;
    private boolean returnsC;
    private boolean notThrowCloneNotSupportedException;
    private boolean notUseNew;
    private Method clone;

    private static class CloneVisitor extends VoidVisitorAdapter<Void> {

        private Boolean useNew = false;

        @Override
        public void visit(ObjectCreationExpr n, Void v) {
            super.visit(n, v);
            useNew = !n.getTypeAsString().contains("Exception");
        }
    }

    @Deprecated
    public CloneChecker(Class<C> classToCheck, BlockStmt bodyToCheck) {
        this.checkedClass = classToCheck;
        this.bodyToCheck = bodyToCheck;

        this.cloneDeclared = false;
        this.interfaceCloneableImplemented = false;
        this.returnsC = false;
        this.notThrowCloneNotSupportedException = false;
        this.notUseNew = false;

        this.clone = null;

        check();
    }

    /**
     *
     * @param classToCheck The object class of class C whose clone method we
     * want to check
     */
    public CloneChecker(Class<C> classToCheck) {
        this(classToCheck, null);
    }

    private void check() {
        try {
            clone = checkedClass.getDeclaredMethod("clone");
            cloneDeclared = true;
            if (bodyToCheck != null) {
                bodyToCheck = ParserWithReflectUtilities.find(clone).getBody().get();
            }
        } catch (NoSuchMethodException | SecurityException ex) {
            cloneDeclared = false;
        }

        try {
            interfaceCloneableImplemented = Arrays.stream(checkedClass.getInterfaces())
                    .anyMatch(c -> c == Cloneable.class);
        } catch (SecurityException ex) {
            interfaceCloneableImplemented = false;
        }

        if (cloneDeclared) {
            returnsC = clone.getReturnType() == checkedClass;
            notThrowCloneNotSupportedException = !Arrays.stream(clone.getExceptionTypes())
                    .anyMatch(c -> c == CloneNotSupportedException.class);

            if (bodyToCheck != null) {
                CloneVisitor cloneVisitor = new CloneVisitor();
                bodyToCheck.accept(cloneVisitor, null);
                notUseNew = !cloneVisitor.useNew;
            } else {
                notUseNew = true;
            }
        }
    }

    /**
     *
     * @return Object class of C
     */
    public Class<C> getCheckedClass() {
        return checkedClass;
    }

    /**
     *
     * @return The parsed body of the clone method of C is it exists or null
     */
    public BlockStmt getBodyToCheck() {
        return bodyToCheck;
    }

    /**
     *
     * @return The clone method is declared in the class C
     */
    public boolean isCloneDeclared() {
        return cloneDeclared;
    }

    /**
     *
     * @return C implements Cloneable
     */
    public boolean isInterfaceCloneableImplemented() {
        return interfaceCloneableImplemented;
    }

    /**
     *
     * @return clone exists and returns an objet of class C
     */
    public boolean isReturnsC() {
        return returnsC;
    }

    /**
     *
     * @return clone exists and does not throw CloneNotSupportedException
     */
    public boolean isNotThrowCloneNotSupportedException() {
        return notThrowCloneNotSupportedException;
    }

    /**
     *
     * @return clone exists and does not throw CloneNotSupportedException
     */
    public boolean isNotUseNew() {
        return notUseNew;
    }

    /**
     *
     * @return the reflected object class of the clone method or null if does not
     * exist
     */
    public Method getClone() {
        return clone;
    }

}
