/*
 * Copyright 2019: Yvan Maillot
 * This software is part of the Caseine project.
 * This software was developped with the support of the following organizations:
 * Université de Haute-Alsace
 * Institut Polytechnique de Grenoble
 *
 *
 * This file is part of the VPL Design Tool.
 * The VPL Design Tool is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The VPL Design Tool is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the VPL Design Tool.  If not, see <https://www.gnu.org/licenses/>.
 */
package caseine.reflect;

import caseine.exceptions.NoSimpleConstructor;

import java.io.*;
import java.lang.reflect.*;
import java.util.*;

/**
 * Classe utilitaires offrant de nombreuses méthodes introspectives utiles 
 *
 * @author Yvan Maillot &lt;yvan.maillot@uha.fr&gt;
 */
public class ReflectUtilities {

    private static final Random R = new Random();
    //private static Class<?> c;
    //private static Object o;
    //private static String methodName;
    //private static Object[] pv;

    /*private ReflectUtilities() {
    }*/

    public static <T> T getTA(Class<T> c, Object... pv) throws NoSuchMethodException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        Class<?>[] params = new Class<?>[pv.length / 2];
        Object[] values = new Object[pv.length / 2];
        int j = 0;
        for (int i = 0; i < pv.length; i += 2) {
            params[j] = (Class<?>) pv[i];
            values[j++] = pv[i + 1];
        }
        Constructor<T> k = c.getDeclaredConstructor(params);
        return k.newInstance(values);
    }

    public static <T> T getTA(String className, Object... pv) throws ClassNotFoundException, NoSuchMethodException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        return (T) getTA(Class.forName(className), pv);
    }

    public static <T> T get(Class<T> c, Object... pv) throws NoSuchMethodException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        Object[] coupletv = new Object[2 * pv.length];
        int k = 0;
        for (int i = 0; i < pv.length; i++) {
            if (pv[i] != null) {
                coupletv[k++] = pv[i].getClass();
                coupletv[k++] = pv[i];
            } else {
                coupletv[k++] = Object.class;
                coupletv[k++] = null;
            }
        }
        return getTA(c, coupletv);
    }

    public static <T> T get(String className, Object... pv) throws NoSuchMethodException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, ClassNotFoundException {
        return (T) get(Class.forName(className), pv);
    }

    /**
     * Retourne par introspection le résultat de l'invocation d'une méthode avec
     * ses arguments.
     *
     * @param c la classe à laquelle la méthode appartient.
     * @param o l'objet sur lequel s'application la méthode (peut-être null dans
     * le cas d'une méthode static)
     * @param methodName le nom de la méthode
     * @param pv une liste de couples (type et sa valeur)
     * @return le résultat de l'invocation
     * @throws NoSuchMethodException la méthode n'existe pas
     * @throws IllegalAccessException accès interdit
     * @throws IllegalArgumentException les types arguments de ne correspondent
     * pas
     * @throws InvocationTargetException l'invocation de la méthode a engendré
     * une exception
     */
    public static Object getFromMethodTA(Class<?> c, Object o, String methodName, Object... pv) throws NoSuchMethodException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        StringBuilder methodNameAndArgs = new StringBuilder(methodName);
        methodNameAndArgs.append("(");

        Class<?>[] params = new Class<?>[pv.length / 2];
        Object[] values = new Object[pv.length / 2];
        int k = 0;
        for (int i = 0; i < pv.length; i += 2) {
            params[k] = (Class<?>) pv[i];
            methodNameAndArgs.append(params[k].getSimpleName()).append(", ");
            values[k++] = pv[i + 1];
        }
        methodNameAndArgs.append(")");
        Method method = c.getDeclaredMethod(methodName, params);
        method.setAccessible(true);
        if (Modifier.isStatic(method.getModifiers())) {
            return method.invoke(null, values);
        } else {
            return method.invoke(o, values);
        }

    }

    public static Object getFromMethodTA(String className, Object o, String methodName, Object... pv) throws ClassNotFoundException, NoSuchMethodException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        return getFromMethodTA(Class.forName(className), o, methodName, pv);
    }

    /**
     * Retourne par introspection le résultat de l'invocation d'une méthode avec
     * ses arguments.
     *
     * @param c la classe à laquelle la méthode appartient
     * @param o l'objet sur lequel s'application la méthode
     * @param methodName le nom de la méthode
     * @param pv une liste de valeurs (dont les types sont déduits)
     * @return le résultat de l'invocation
     */
    public static Object getFromMethod(Class<?> c, Object o, String methodName, Object... pv) throws NoSuchMethodException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        Object[] coupletv = new Object[2 * pv.length];
        int k = 0;
        for (int i = 0; i < pv.length; i++) {
            if (pv[i] != null) {
                coupletv[k++] = pv[i].getClass();
                coupletv[k++] = pv[i];
            } else {
                coupletv[k++] = Object.class;
                coupletv[k++] = null;
            }
        }
        return getFromMethodTA(c, o, methodName, coupletv);
    }

    public static Object getFromMethod(String className, Object o, String methodName, Object... pv) throws NoSuchMethodException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, ClassNotFoundException {
        return getFromMethod(Class.forName(className), o, methodName, pv);
    }

    /**
     * Retourne dans une chaine de caractères le résultat à l'écran de
     * l'invocation d'une méthode avec ses arguments.
     *
     * @param c la classe à laquelle la méthode appartient
     * @param o l'objet sur lequel s'application la méthode
     * @param methodName le nom de la méthode
     * @param pv une liste de couples (type et sa valeur)
     * @return le résultat de l'invocation
     */
    public static String getFromMethodTASystemOut(Class<?> c, Object o, String methodName, Object... pv) throws FileNotFoundException, IOException, NoSuchMethodException, IllegalAccessException, InvocationTargetException {
        /*ReflectUtilities.c = c;
        ReflectUtilities.o = o;
        ReflectUtilities.methodName = methodName;
        ReflectUtilities.pv = pv;*/

        PrintStream out = System.out;
        try (ByteArrayOutputStream bos = new ByteArrayOutputStream()) {
            System.setOut(new PrintStream(bos));
            getFromMethodTA(c, o, methodName, pv);
            return bos.toString();
        } catch (IllegalArgumentException ex) {
            return "";
        } finally {
            System.setOut(out);
        }
    }

    /**
     * Retourne dans une chaine de caractères le résultat à l'écran de
     * l'invocation d'une méthode avec ses arguments.
     *
     * @param c la classe à laquelle la méthode appartient
     * @param o l'objet sur lequel s'application la méthode
     * @param methodName le nom de la méthode
     * @param sin String à envoyer dans l'entrée standard
     * @param pv une liste de couples (type et sa valeur)
     * @return le résultat de l'invocation
     */
    public static String getFromMethodTASystemInOut(Class<?> c, Object o, String methodName, String sin, Object... pv) throws FileNotFoundException, IOException, NoSuchMethodException, IllegalAccessException, InvocationTargetException {

        PrintStream out = System.out;
        InputStream in = System.in;

        try (ByteArrayOutputStream bos = new ByteArrayOutputStream()) {
            System.setOut(new PrintStream(bos));
            System.setIn(new ByteArrayInputStream(sin.getBytes()));
            getFromMethodTA(c, o, methodName, pv);
            return bos.toString();
        } catch (IllegalArgumentException ex) {
            return "";
        } finally {
            System.setOut(out);
            System.setIn(in);
        }
    }

    /**
     * Met à jour la valeur d'un attribut d'un object donné
     *
     * @param c l'objet de classe de l'objet donné
     * @param o l'objet donné
     * @param nom le nom de l'attribut
     * @param v la valeur à affecter à cet attribut
     * @throws NoSuchFieldException
     * @throws IllegalArgumentException
     * @throws IllegalAccessException
     */
    public static void setAttribut(Class<?> c, Object o, String nom, Object v) throws NoSuchFieldException, IllegalArgumentException, IllegalAccessException {

        Field fnom = c.getDeclaredField(nom);
        fnom.setAccessible(true);
        fnom.set(o, v);

    }

    public static Object getAttribut(Class<?> c, Object o, String nom) throws NoSuchFieldException, IllegalArgumentException, IllegalAccessException {

        Field fnom = c.getDeclaredField(nom);
        fnom.setAccessible(true);
        return fnom.get(o);

    }

    private static boolean checkIfMutable(Object o1, Method m) throws InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, FileNotFoundException {
        Object o2 = clone(o1);
        m.setAccessible(true);

        for (int i = 0; i < 100; i++) {
            PrintStream out = System.out;
            PrintStream err = System.err;
            System.setOut(new PrintStream("tmpout"));
            System.setErr(new PrintStream("tmperr"));
            m.setAccessible(true);
            m.invoke(o2, fillParametersArray(m));
            System.setOut(out);
            System.setErr(err);

            if (!equals(o1, o2)) {
                return true;
            }
        }
        return false;
    }

    private static boolean isASetter(Method m, Class<?> C) {
        try {
            Constructor<?> c = C.getConstructor();
            c.setAccessible(true);
            Object o = c.newInstance();
        } catch (NoSuchMethodException | SecurityException | InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
        }
        return true;
    }

    private static boolean thereIsSetter(Class<?> C) {
        return Arrays.stream(C.getMethods())
                .anyMatch(m -> m.getName().startsWith("set"))
                || Arrays.stream(C.getDeclaredMethods())
                        .filter(m -> !Modifier.isPrivate(m.getModifiers()))
                        .anyMatch(m -> m.getName().startsWith("set"));
    }

    public static boolean isMutable(Class<?> C) {
        return thereIsSetter(C)
                || Arrays.stream(C.getDeclaredFields())
                        .anyMatch(
                                p -> !Modifier.isPrivate(p.getModifiers())
                                && (!Modifier.isFinal(p.getModifiers())
                                || (!p.getType().isPrimitive()
                                && isMutable(p.getType())))
                        )
                || Arrays.stream(C.getFields())
                        .anyMatch(
                                p -> !Modifier.isPrivate(p.getModifiers())
                                && (!Modifier.isFinal(p.getModifiers())
                                || (!p.getType().isPrimitive()
                                && isMutable(p.getType())))
                        );

    }

    private static boolean checkAttributsForMutability(Class<?> c) {
        return Arrays.stream(c.getDeclaredFields())
                .anyMatch(
                        p -> !Modifier.isPrivate(p.getModifiers())
                        && (!Modifier.isFinal(p.getModifiers())
                        || (!p.getType().isPrimitive()
                        && isMutable(p.getType())))
                )
                || Arrays.stream(c.getFields())
                        .anyMatch(
                                p -> !Modifier.isPrivate(p.getModifiers())
                                && (!Modifier.isFinal(p.getModifiers())
                                || (!p.getType().isPrimitive()
                                && isMutable(p.getType())))
                        );
    }

    /*private static boolean checkIfMutable(Class<?> c) throws InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, FileNotFoundException {

        Object o1 = randomValue(c);
        for (Method m : c.getDeclaredMethods()) {
            if (!Modifier.isStatic(m.getModifiers())) {
                if (checkIfMutable(o1, m)) {
                    return true;
                }
            }
        }
        return false;
    }*/
 /*
     * Tente de dupliquer o1 dans c2. Il faut que c2 soit similaire à c1 =
     * o1.getClass()
     * <p>
     * NE FONCTIONNE PAS BIEN si c1 possède une super aux attributs non publics.
     *
     * @param o1
     * @param c2
     * @return
     * @throws InstantiationException
     * @throws IllegalAccessException
     * @throws IllegalArgumentException
     * @throws InvocationTargetException
     * @throws NoSuchMethodException
     * @throws NoSuchFieldException
     */
    public static Object clone(Object o1, Class<?> c2) throws InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, NoSuchFieldException {
        if (o1 == null) {
            return null;
        }
        if (o1.getClass() == c2)
            return o1;
        Object o2 = randomValue(c2);
        for (Field f1 : o1.getClass().getDeclaredFields()) {
            if (!Modifier.isStatic(f1.getModifiers()) || !Modifier.isFinal(f1.getModifiers())) {
                f1.setAccessible(true);
                Field f2 = c2.getDeclaredField(f1.getName());
                f2.setAccessible(true);
                if (f2.getType() == f1.getType()) {
                    f2.set(o2, f1.get(o1));
                } else {
                    f2.set(o2, clone(f1.get(o1), f2.getType()));
                }
            }
        }
        return o2;
    }



    public static Object clone(Object o) throws InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException {
        if (o == null) {
            return null;
        }
        Object co = randomValue(o.getClass());
        for (Field f : o.getClass().getDeclaredFields()) {
            f.setAccessible(true);
            f.set(co, f.get(o));
        }
        return co;
    }

    /**
     * Egalité de deux objets non nécessairement de mêmes types.
     * <p>
     * Deux objets o1 et o2 sont considérés égaux (au sens défini ICI) si
     * </p>
     * <ol>
     * <li>ils sont des wrappers de type simple ET o1.equals(o2)</li>
     * <li>OU ils sont de même type ET ils redéfissent Object::equals(Object) ET
     * o1.equals(o2)</li>
     * <li>OU
     * <ol>
     * <li>leur type ont le même nom (simple)</li>
     * <li>ET leurs attributs non statique ont les mêmes noms et sont égaux
     * entre eux (au sens défini ICI)</li>
     * </ol>
     * </li>
     * </ol>
     * <p>
     *
     * @param o1 à comparer avec o2
     * @param o2 à comparer avec o1
     * @return vrai si o1 est égal à o2 au sens défini ICI
     * @throws InstantiationException problème
     * @throws IllegalAccessException problème
     * @throws IllegalArgumentException problème
     * @throws InvocationTargetException problème
     * @throws NoSuchMethodException problème
     */
    public static boolean equals(Object o1, Object o2) throws InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException {
        if (o1 == null) {
            return o2 == null;
        }
        if (o2 == null) {
            return false;
        }
        if (o1.getClass() == o2.getClass()) {
            return o1.equals(o2);
        }
        return equals(o1, o2, false);
    }

    private static boolean equals(Object o2, Object o1, boolean isPrimitive) throws InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException {
        boolean hasEqualsMethod = false;
        try {
            o2.getClass().getDeclaredMethod("equals", Object.class);
            hasEqualsMethod = true;
        } catch (Exception ex) {

        }
        if (!o2.getClass().getSimpleName().equals(o1.getClass().getSimpleName())) {
            return false;
        } else if (o2.getClass() == o1.getClass() && hasEqualsMethod) {
            return o2.equals(o1);
        } else if (o2.getClass().isPrimitive() || isPrimitive) {
            return o2.equals(o1);
        } else {
            for (Field f1 : o2.getClass().getDeclaredFields()) {
                if (!Modifier.isStatic(f1.getModifiers())) {
                    try {
                        Field f2 = o1.getClass().getDeclaredField(f1.getName());
                        if (!Modifier.isStatic(f2.getModifiers())) {
                            f1.setAccessible(true);
                            f2.setAccessible(true);
                            if (!equals(f1.get(o2), f2.get(o1)/*, f1.getType().isPrimitive()*/)) {
                                return false;
                            }
                        } else {
                            return false;
                        }
                    } catch (NoSuchFieldException | SecurityException ex) {
                        return false;
                    }
                }
            }
            for (Field f2 : o2.getClass().getDeclaredFields()) {
                if (!Modifier.isStatic(f2.getModifiers())) {
                    try {
                        Field f1 = o1.getClass().getDeclaredField(f2.getName());
                        if (!Modifier.isStatic(f1.getModifiers())) {
                            f2.setAccessible(true);
                            f1.setAccessible(true);
                            if (!equals(f2.get(o2), f1.get(o1)/*, f1.getType().isPrimitive()*/)) {
                                return false;
                            }
                        } else {
                            return false;
                        }
                    } catch (NoSuchFieldException | SecurityException ex) {
                        return false;
                    }
                }
            }
            return true;
        }
    }

    public static boolean allParametersArePrimitive(Constructor<?> c) {
        return Arrays.stream(c.getParameterTypes())
                .allMatch(p -> p.isPrimitive());
    }

    /*
     * Retourne le premier constructeur contenant. Les constructeurs publiques
     * sont cherchés en premier Si le constructeur retourné est non-public,
     * alors il est rendu accessible.
     *
     * @param c le type de classe dont on veut un constructeur
     * @return un constructeur ou null en cas d'absence
     */
    public static Constructor<?> getOneConstructor(Class<?> c) {
        Optional<Constructor<?>> optK = Arrays.stream(c.getDeclaredConstructors())
                .filter(p -> p.getParameterCount() > 0)
                .findFirst();
        if (optK.isPresent()) {
            return optK.get();
        } else {
            optK = Arrays.stream(c.getConstructors())
                    .filter(p -> p.getParameterCount() > 0)
                    .findFirst();
            if (optK.isPresent()) {
                optK.get().setAccessible(true);
                return optK.get();
            } else {
                return null;
            }
        }
    }

    /**
     * Retourne le premier constructeur contenant que des paramètres simples.
     * Les constructeurs publiques sont cherchés en premier Si le constructeur
     * retourné est non-public, alors il est rendu accessible.
     *
     * @param c le type de classe dont on veut un constructeur
     * @return un constructeur
     * @throws NoSimpleConstructor aucun constructeur trouvé
     */
    public static Constructor<?> getOneSimpleConstructor(Class<?> c) throws NoSimpleConstructor {
        Optional<Constructor<?>> optK = Arrays.stream(c.getDeclaredConstructors())
                .filter(p -> p.getParameterCount() > 0)
                .filter(p -> allParametersArePrimitive(p))
                .findFirst();
        if (optK.isPresent()) {
            return optK.get();
        } else {
            optK = Arrays.stream(c.getConstructors())
                    .filter(p -> allParametersArePrimitive(p))
                    .findFirst();
            if (optK.isPresent()) {
                optK.get().setAccessible(true);
                return optK.get();
            } else {
                throw new NoSimpleConstructor();
            }
        }
    }

    public static Object[] fillParametersArray(Executable k) throws InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException {
        Object[] values = (Object[]) Array.newInstance(Object.class,
                k.getParameterCount());
        Class<?>[] paramtypes = k.getParameterTypes();
        for (int i = 0; i < values.length; i++) {
            if (paramtypes[i] == char.class) {
                values[i] = (char) R.nextInt();

            } else {
                Array.set(values, i, randomValue(paramtypes[i]));
            }
        }
        return values;
    }

    public static int randomInt(int min, int max, boolean notNul) {
        int r = min + R.nextInt(max - min + 1);
        if (notNul) {
            while (r == 0) {
                r = min + R.nextInt(max - min + 1);
            }
        }
        return r;
    }

    public static int randomInt(int min, int max) {
        return randomInt(min, max, false);
    }

    /*
     * Retourne un objet aléatoire en utilisant un constructeur défini ses
     * valeurs
     *
     * @param c
     * @param p
     * @return
     * @throws java.lang.NoSuchMethodException
     * @throws java.lang.InstantiationException
     * @throws java.lang.IllegalAccessException
     * @throws java.lang.reflect.InvocationTargetException
     */
    public static Object randomValue(Class<?> c, Object... p) throws NoSuchMethodException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        Class<?>[] type = new Class<?>[p.length];
        for (int i = 0; i < p.length; ++i) {
            type[i] = p[i].getClass();
        }
        Constructor<?> k = c.getConstructor(type);
        k.setAccessible(true);
        return k.newInstance(p);
    }


    public static boolean isTypesOf(Class<?> theClass, Class<?> ofType) {
        if (theClass == null || ofType == null)
            return false;
        if (theClass == ofType)
            return true;
        boolean result = false;
        for (Class<?> k : theClass.getInterfaces()) {
            result = result || isTypesOf(k, ofType);
        }
        return result || isTypesOf(theClass.getSuperclass(), ofType);
    }

    /*
     * Retourne un objet aléatoire en utilisant un constructeur défini par p
     *
     * @param c
     * @param p
     * @return
     * @throws java.lang.NoSuchMethodException
     * @throws java.lang.InstantiationException
     * @throws java.lang.IllegalAccessException
     * @throws java.lang.reflect.InvocationTargetException
     */
    public static Object randomValue(Class<?> c, Class<?>... p) throws NoSuchMethodException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        if (p.length == 0) {
            Constructor<?> k = c.getConstructor(p);
            k.setAccessible(true);
            Object o = k.newInstance(fillParametersArray(k));
            return o;
        } else {
            return randomValue(c);
        }
    }

    private static String randomString() throws InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException {
        int n = randomInt(0, 10);
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < n; ++i) {
            sb.append(randomValue(char.class));
        }
        return sb.toString();
    }

    public static Object randomValue(Class<?> c) throws InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException {
        if (c == void.class) {
            return null;

        } else if (c == boolean.class) {
            return R.nextBoolean();

        } else if (c == int.class
                || c == byte.class) {
            return 50 - R.nextInt(101);

        } else if (c == char.class) {
            return (char) ('A' + R.nextInt(26));

        } else if (c == long.class) {
            return R.nextLong();

        } else if (c == double.class) {
            return R.nextDouble();

        } else if (c == float.class) {
            return R.nextFloat();
        } else if (c == String.class) {
            return randomString();
        } else if (c.isArray()) {
            return Array.newInstance(c.getComponentType(), 0);
        } else {
            Constructor<?> k;
            try {
                k = c.getDeclaredConstructor();
            } catch (NoSuchMethodException | SecurityException ex1) {
                try {
                    k = c.getConstructor();
                } catch (NoSuchMethodException | SecurityException ex2) {
                    try {
                        k = getOneSimpleConstructor(c);
                    } catch (NoSimpleConstructor ex) {
                        k = getOneConstructor(c);
                    }
                }
            }
            if (k != null) {
                try {
                    k.setAccessible(true);
                    Object o = k.newInstance(fillParametersArray(k));
                    for (Field f : c.getDeclaredFields()) {
                        if (!Modifier.isStatic(f.getModifiers()) && !Modifier.isFinal(f.getModifiers())) {
                            f.setAccessible(true);
                            f.set(o, randomValue(f.getType()));
                        }
                    }
                    return o;
                } catch (SecurityException ex) {
                    return c;
                }
            }
            return null;
        }
    }

    /**
     * Retourne vrai l'objet de classe donné en paramètre est une exception contrôlée,
     * faux si c'est une exception non contrôlée.
     * @param crt l'exception dont on veut connaître la nature
     * @return vrai si crt est une exception contrôlée, faux si crt est une exception non contrôlée
     * @throws IllegalArgumentException si crt n'est pas une exception
     */
    public static boolean isCheckedException(Class<?> crt) throws IllegalArgumentException {
        while (crt != Object.class) {
            if (crt == RuntimeException.class) {
                return false;
            }
            if (crt == Exception.class) {
                return true;
            }
            crt = crt.getSuperclass();
        }
        throw new IllegalArgumentException("Not an exception");
    }

    public static boolean isCheckedException(Throwable cause) {
        Class<?> crt = cause.getClass();
        while (crt != Throwable.class) {
            if (crt == RuntimeException.class) {
                return false;
            }
            crt = crt.getSuperclass();
        }
        return true;
    }

    public static boolean sameResult(StringBuilder msg, Class<?> c1, Class<?> c2, String methodName, Class<?>... parameterTypes) throws NoSuchMethodException, IllegalArgumentException, IllegalAccessException, InvocationTargetException, InstantiationException, NoSuchFieldException {
        return sameResult(false, msg, c1, c2, methodName, parameterTypes);
    }

    /*
     * Retourne vrai si une méthode appliquée à deux objets différents donne le
     * même résultat.
     * <p>
     * #### LIMITATION
     * <p>
     * Cette méthode est très limitée. Elle ne fonctionne bien que si les
     * paramètres des constructeurs comme des méthodes sont de types simples.
     * <p>
     * #### LIMITATION
     *
     * @param msg message pour les tests unitaires utilisé éventuellement en cas
     * de différence
     * @param c1
     * @param c2
     * @param methodName
     * @param parameterTypes
     * @return
     * @throws NoSuchMethodException
     * @throws IllegalArgumentException
     * @throws IllegalAccessException
     * @throws InvocationTargetException
     * @throws InstantiationException
     * @throws NoSuchFieldException
     */
    public static boolean sameResult(boolean checkStdOut, StringBuilder msg, Class<?> c1, Class<?> c2, String methodName, Class<?>... parameterTypes) throws NoSuchMethodException, IllegalArgumentException, IllegalAccessException, InvocationTargetException, InstantiationException, NoSuchFieldException {

        Method m1 = c1.getDeclaredMethod(methodName, parameterTypes);
        Method m2 = c2.getDeclaredMethod(methodName, parameterTypes);
        m1.setAccessible(true);
        m2.setAccessible(true);

        Object o1;
        Object o2;

        if (Modifier.isStatic(m1.getModifiers())) {
            if (Modifier.isStatic(m2.getModifiers())) {
                o1 = o2 = null;
            } else {
                return false;
            }
        } else {
            if (!Modifier.isStatic(m2.getModifiers())) {
                o1 = ReflectUtilities.randomValue(c1);
                o2 = ReflectUtilities.clone(o1, c2);
            } else {
                return false;
            }
        }

        Object[] param = new Object[parameterTypes.length];
        for (int i = 0; i < param.length; ++i) {
            param[i] = randomValue(parameterTypes[i]);
        }
        m1.setAccessible(true);
        m2.setAccessible(true);
        Object r1 = null;
        Object r2 = null;

        String stdout1 = "";
        String stdout2 = "";

        java.io.PrintStream out;
        out = System.out;
        try (java.io.ByteArrayOutputStream bos = new java.io.ByteArrayOutputStream()) {
            System.setOut(new java.io.PrintStream(bos));
            // Invocation de la méthode 1 dont on veut le résultat
            r1 = m1.invoke(o1, param);
            stdout1 = bos.toString(); // Récupération de la sortie standard
        } catch (InvocationTargetException ex) {
        } catch (IOException e) {
        } finally {
            System.setOut(out);
        }

        out = System.out;
        try (java.io.ByteArrayOutputStream bos = new java.io.ByteArrayOutputStream()) {
            System.setOut(new java.io.PrintStream(bos));
            r2 = m2.invoke(o2, param);
            stdout2 = bos.toString(); // Récupération de la sortie standard
        } catch (InvocationTargetException ex) {
        } catch (IOException e) {
        } finally {
            System.setOut(out);
        }
        // Préparation du message en cas de différence
        msg.append("\nresult : ").append(r1).append(" <> ").append(r2);

        if (checkStdOut) {
            msg.append("\nstdout : ").append(stdout1).append(" <> ").append(stdout2);
        }
        return ReflectUtilities.equals(r1, r2) && (!checkStdOut || stdout1.equals(stdout2));

    }

    /*
     * Return the java {@link java.lang.Class} object with the specified class
     * name.This is an "extended"
     * {@link java.lang.Class#forName(java.lang.String)} operation.
     * <p>
     * + It is able to return Class objects for primitive types + Classes in
     * name space `java.lang` do not need the fully qualified name + It does not
     * throw a checked Exception
     *
     * @param className The class name, never `null`
     * @return object with the specified class name.
     * @throws ClassNotFoundException
     * @throws IllegalArgumentException if no class can be loaded
     */
    public static Class<?> parseType(final String className) throws ClassNotFoundException {
        switch (className) {
            case "boolean":
                return boolean.class;
            case "byte":
                return byte.class;
            case "short":
                return short.class;
            case "int":
                return int.class;
            case "long":
                return long.class;
            case "float":
                return float.class;
            case "double":
                return double.class;
            case "char":
                return char.class;
            case "void":
                return void.class;
            case "[I":
                return new int[]{}.getClass();
            default:
                String fqn = className.contains(".") ? className : "java.lang.".concat(className);
                try {
                    return Class.forName(fqn);
                } catch (ClassNotFoundException e) {
                    if (!className.contains(".")) {
                        return Class.forName(className);
                    } else {
                        throw e;
                    }
                }
        }
    }

    public static Class<?> getDeclaredClass(Class<?> c, String name) {
        Optional<Class<?>> optic = Arrays.stream(c.getDeclaredClasses())
                .filter(ic -> ic.getSimpleName().equals(name))
                .findFirst();
        if (optic.isPresent()) {
            return optic.get();
        } else {
            return null;
        }
    }

    public static String paramsToStringClass(Executable m) {
        StringBuilder sb = new StringBuilder();
        Class<?>[] params = m.getParameterTypes();
        for (Class<?> param : params) {
            sb.append(", ").append(param.getTypeName()).append(".class");
        }
        return sb.toString();
    }

    public static String paramsToString(Executable m) {
        StringBuilder params = Arrays.stream(m.getParameterTypes())
                .map(x -> x.getSimpleName().replaceAll("\\[\\]", "Array"))
                .collect(
                        StringBuilder::new,
                        StringBuilder::append,
                        StringBuilder::append);
        return params.toString();
    }

    /*
     * Return a string that is a sequence of statements. Each of them are a call
     * to a method built from the array argument called priorities.
     *
     * A string in "priorities" is
     *
     * - either the fully qualified name of an accessible static method with no
     * parameter and that returns void.
     *
     * For instance, "edu.uha.miage.AnUnitTestClass.aMethod" means that the
     * method "public static void edu.uha.miage.AnUnitTestClass.aMethod() {} "
     * should exist and it will be called.
     *
     * - or a string that can be parsed in a positive integer and in that case,
     * the methods that will be called is those ones in keys of "msb" of
     * priority given by this integer.
     *
     * @param msb
     * @param priorities
     * @return
     */
    public static String writePreviousUnitTestCalling(TreeMap<String, StringBuilder> msb, String[] priorities) {
        StringBuilder sb = new StringBuilder();
        sb.append("try {\n");
        for (String priority : priorities) {
            try {
                sb.append(writePreviousUnitTestCalling(msb, Integer.parseInt(priority)));
            } catch (NumberFormatException ex) {
                sb.append(writePreviousUnitTestCalling(priority));
            }
        }
        sb.append("} catch(Error e) {\n");
        sb.append("    fail(e.toString());\n");
        sb.append("}\n");
        return sb.toString();
    }

    private static StringBuilder writePreviousUnitTestCalling(String aMethodName) {
        StringBuilder sb = new StringBuilder();

        if (!aMethodName.endsWith("()")) {
            sb.append(aMethodName).append("()");
        } else {
            sb.append(aMethodName);
        }
        sb.append(";\n");

        return sb;
    }

    private static StringBuilder writePreviousUnitTestCalling(TreeMap<String, StringBuilder> msb, int priority) {
        StringBuilder sb = new StringBuilder();

        for (String name : msb.keySet()) {
            if (name.startsWith(String.format("p%06d000", priority))) {
                sb.append(name).append(";\n");
            }
        }
        /*
        sb = Arrays.stream(c.getDeclaredMethods())
                .filter(m -> m.getReturnType() == void.class)
                .filter(m -> m.getParameterCount() == 0)
                .filter(m -> m.getName().startsWith(String.format("p%06d000", priority)))
                .map(m -> "try {" +
                        String.format("ReflectUtilities.getFromMethod(%s, (Object) null, \"%s\");", c.getName()+".class", m.getName()) +
                        "} catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {" +
                        "fail(Arrays.toString(e.getStackTrace()));" +
                        "}")
                .collect(StringBuilder::new, StringBuilder::append, StringBuilder::append);*/
        return sb;
    }


    /*
    public static boolean sameResult(Object o1, Object o2, String methodName, Object... param) throws NoSuchMethodException, IllegalArgumentException, IllegalAccessException, InvocationTargetException, InstantiationException {
        Object r1 = null, r2 = null;
        try {
            r1 = getFromMethod(o1.getClass(), o1, methodName, param);
            r2 = getFromMethod(o2.getClass(), o2, methodName, param);
        } catch (InvocationTargetException ex) {
            if (r1 == null) {
                try {
                    r2 = getFromMethod(o2.getClass(), o2, methodName, param);
                } catch (InvocationTargetException ex1) {
                    return true;
                }
            } else {
                return false;
            }
        }
        if (r1 == null) {
            return r2 == null;
        } else {
            if (r2 == null) {
                return false;
            } else {
                return ReflectUtilities.equals(r1, r2);
            }
        }
    }
    public static boolean sameResult(Object o1, Object o2, String methodName, Object... param) throws NoSuchMethodException, IllegalArgumentException, IllegalAccessException, InvocationTargetException, InstantiationException {
        Object r1 = getFromMethod(o1.getClass(), o1, methodName, param);
        Object r2 = getFromMethod(o2.getClass(), o2, methodName, param);
        if (r1 == null) {
            return r2 == null;
        } else {
            if (r2 == null) {
                return false;
            } else {
                return ReflectUtilities.equals(r1, r2);
            }
        }
    }

    public static boolean sameResultTA(Object o1, Object o2, String methodName, Object... param) throws NoSuchMethodException, IllegalArgumentException, IllegalAccessException, InvocationTargetException, InstantiationException {
        Object r1 = getFromMethodTA(o1.getClass(), o1, methodName, param);
        Object r2 = getFromMethodTA(o2.getClass(), o2, methodName, param);
        return ReflectUtilities.equals(r1, r2);
    }*/
    static class A {

        double x;
    }

    static class B {

        double x;
    }
}
