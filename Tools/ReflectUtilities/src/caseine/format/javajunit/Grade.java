/*
 * Copyright 2019: Yvan Maillot
 * This software is part of the Caseine project.
 * This software was developped with the support of the following organizations:
 * Université de Haute-Alsace
 * Institut Polytechnique de Grenoble
 * 
 * 
 * This file is part of the VPL Design Tool.
 * The VPL Design Tool is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * The VPL Design Tool is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with the VPL Design Tool.  If not, see <https://www.gnu.org/licenses/>.
 */
package caseine.format.javajunit;

/**
 * Pour définir la valeur attribuée par Caséine à la réussite du test unitaire
 * annoté.
 *
 */
public @interface Grade {

    /**
     * La valeur attribuée à la réussite du test unitaire annoté.
     *
     * Potentiellement ajustée si {@link caseine.tags.RelativeEvaluation} est
     * utilisé.
     *
     * Ignorée si négative ou nulle.
     *
     * @return la valeur attribuée à la réussite du test unitaire annoté.
     */
    double value();

}
