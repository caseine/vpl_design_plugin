/*
 * Copyright 2019: Yvan Maillot
 * This software is part of the Caseine project.
 * This software was developped with the support of the following organizations:
 * Université de Haute-Alsace
 * Institut Polytechnique de Grenoble
 * 
 * 
 * This file is part of the VPL Design Tool.
 * The VPL Design Tool is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * The VPL Design Tool is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with the VPL Design Tool.  If not, see <https://www.gnu.org/licenses/>.
 */
package caseine.tests;

import caseine.tags.ToCheck;
import caseine.tags.ToDo;

public class D {
    /*@ToCheck("Inner class Apbs")
    public static class Apbs {
        
    }
    @ToCheck
    protected static class Apts {
        
    }
    @ToCheck
    private static class Apvs {
        
    }
    @ToCheck
    static class Aams {
        
    }
    @ToCheck
    public class Apb {
        
    }
    @ToCheck
    protected class Apt {
        
    }
    @ToCheck
    private class Apv {
        
    }
    @ToCheck
    class Aam {
        
    }
   
    @ToDo("1.01 Déclarer pfi, un attribut public et constant de type int ")
    @ToCheck(value = "Attribut pfi", grade = 1)
    public final int pfi = 0;
*/
    @ToDo("1.02 Déclarer IC1 une classe interne de classe privée")
    @ToCheck(value = "Inner class IC1", grade = 1)
    private static class IC1 {

        @ToDo("2.02 Déclarer pbfi, un attribut public et constant de type int ")
        @ToCheck(value = "Attribut pbfi dans IC1", grade = 1)
        public final int pbfi = 0;
    }
/*
    @ToDo("1.03 Déclarer IC2 une classe interne de visibilité amicale")
    @ToCheck(value = "Inner class IC2", grade = 1)
    class IC2 {
        @ToDo("2.02 Déclarer pvi, un attribut public et constant de type int ")
        @ToCheck(value = "Attribut pvi dans IC2", grade = 1)
        private int pvi;
        
    }*/
}


class DE {
    public class ICE {
        
    }
}
