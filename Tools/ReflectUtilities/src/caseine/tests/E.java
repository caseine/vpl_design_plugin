/*
 * Copyright 2019: Yvan Maillot
 * This software is part of the Caseine project.
 * This software was developped with the support of the following organizations:
 * Université de Haute-Alsace
 * Institut Polytechnique de Grenoble
 * 
 * 
 * This file is part of the VPL Design Tool.
 * The VPL Design Tool is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * The VPL Design Tool is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with the VPL Design Tool.  If not, see <https://www.gnu.org/licenses/>.
 */
package caseine.tests;

import caseine.tags.ToCheck;
import caseine.tags.ToDo;

public class E {

    @ToDo("1.02 Déclarer IC1 une classe interne de classe privée")
    @ToCheck(value = "Inner class IC1", grade = 1)
    private static class IC1 {

        @ToDo("2.02 Déclarer pbfi, un attribut public et constant de type int ")
        @ToCheck(value = "Attribut pbfi dans IC1", grade = 1)
        public final int pbfi = 0;
    }

}


class F {
    public class ICE {
        
    }
}
