/*
 * Copyright 2019: Yvan Maillot
 * This software is part of the Caseine project.
 * This software was developped with the support of the following organizations:
 * Université de Haute-Alsace
 * Institut Polytechnique de Grenoble
 * 
 * 
 * This file is part of the VPL Design Tool.
 * The VPL Design Tool is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * The VPL Design Tool is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with the VPL Design Tool.  If not, see <https://www.gnu.org/licenses/>.
 */
package caseine.tests;

import static caseine.tags.CheckModifier.isFinal;
import static caseine.tags.CheckModifier.isPrivate;
import static caseine.tags.CheckModifier.isProtected;
import static caseine.tags.CheckModifier.isPublic;
import static caseine.tags.CheckModifier.isStatic;

import caseine.tags.GetterToCheck;
import caseine.tags.RelativeEvaluation;
import caseine.tags.SetterToCheck;
import caseine.tags.ToCheck;
import caseine.tags.ToCompare;
import caseine.tags.ToDoIn;

@ToCheck(priority = 10)
@RelativeEvaluation
public class A {
      
    @ToCheck(modifiers = {isFinal, isPrivate, isProtected, isPublic, isStatic},
            priority = 1)
    @SetterToCheck(value ="a",
            priority = 6, grade = 1)
    private int a;
    
    @ToCheck(modifiers = {isFinal, isPrivate, isProtected, isPublic, isStatic},
            priority = 2)
    @GetterToCheck(value = "B",
            priority = 7)
    protected final char B = 'B';
    
    @ToCheck(modifiers = {isFinal, isPrivate, isProtected, isPublic, isStatic},
            priority = 3)
    @GetterToCheck(value = "C",
            priority = 8)
    static final String C = "C";
    
    @ToCheck(modifiers = {isFinal, isPrivate, isProtected, isPublic, isStatic},
            priority = 4)
    @SetterToCheck(value = "D",
            priority = 9)
    private double d = 4.0;
    
    @ToCheck(modifiers = {isFinal, isPrivate, isProtected, isPublic, isStatic},
           priority = 5)

    public A() {
        this.a = 0;
    }
    
    public int getA() {
        return a;
    }
    
    public void setA(int a) {
        this.a = a;
    }

    public char getB() {
        return B;
    }
    
    @ToDoIn
    public void testToDoIn() {
        System.out.println("ToDoIn");
    }

    public static String getC() {
        return C;
    }

    public double getD() {
        return d;
    }

    public void setD(double d) {
        this.d = d;
    }
    
    @ToCheck
    @ToCompare
    public String toString() {
        return "";
    }
     
}
