package caseine.tests.reflect_utilities;

import caseine.reflect.ReflectUtilities;

import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;

public class ForCallPreviousUnitTest {
    public void p000002000_testMethodQuelquesExpressionsBooleennesestMinusculechar() {

    }

    public void p000002000_testMethodQuelquesExpressionsBooleennesestMinusculecharBis() {

    }

    public void p000003000_testMethod() {
        try {
            ReflectUtilities.getFromMethod(caseine.tests.reflect_utilities.ForCallPreviousUnitTest.class, (Object) null, "p000003000_testMethod");
        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            fail(Arrays.toString(e.getStackTrace()));
        }
    }

    private void fail(String toString) {

    }

    public void p000004000_testMethod() {
        try {
            ReflectUtilities.getFromMethod(this.getClass(), (Object) null, "f");
        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
        }
    }
}
