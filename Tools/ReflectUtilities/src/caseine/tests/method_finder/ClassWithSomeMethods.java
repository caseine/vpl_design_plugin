package caseine.tests.method_finder;

import java.awt.*;

// Some methods to test with methodFinderTest
public class ClassWithSomeMethods {

    private static void m(int i) { System.out.println(1); }

    private void m(double d) { System.out.println(2.0); }

    private static void m(int i, double d) {System.out.println(3 + 4.0); }

    private void m(int... i) { System.out.println("Int Varargs");}

    private static void m(String s) {System.out.println("String");}

    private void m(Color s) { System.out.println("Color"); }
}
