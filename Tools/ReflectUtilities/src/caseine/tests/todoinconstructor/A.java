package caseine.tests.todoinconstructor;

import caseine.tags.ToDoInConstructor;

public class A {
    public int a;
    
    @ToDoInConstructor
    public A(int a) {
        this.a = a;
    }
    
    @ToDoInConstructor("this(...)")
    public A() {
        this(0);
    }
}
