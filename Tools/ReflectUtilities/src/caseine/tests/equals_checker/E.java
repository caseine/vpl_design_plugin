/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package caseine.tests.equals_checker;

public class E {
    private int a;



    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + this.a;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        /*if (obj == null) {
            return false;
        }*/
        if (getClass() != obj.getClass()) {
            return false;
        }
        final E other = (E) obj;
        return this.a == other.a;
    }
    
    
}
