package caseine.tags;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Pour enlever des déclarations d'implémetation ou d'extension.
 * 
 * @author Yvan Maillot &lt;yvan.maillot@uha.fr&gt;
 *
 * <h2>L'annotation &#64;caseine.tags.ImplementationToRemove</h2>
 *
 *
 * <p>
 * L'annotation &#64;ImplementationToRemove enlève des déclarations d'interfaces
 * d'une classe.
 * </p>
 ** <h3>Cible</h3>
 * <p>
 * &#64;ImplementationToRemove s'applique aux classes ou aux interfaces.
 * </p>
 *
 *
 * <h3>Formes d'emploi</h3>
 * <p>
 * &#64;ImplementationToRemove s'emploie uniquement comme une annotation
 * monovaluée : (e.g. &#64;ImplementationToRemove("Cloneable").
 *
 *
 * <h3>Effet</h3>
 * L'annotation &#64;ImplementationToRemove a pour effet de supprimer à
 * l'étudiant une ou des déclarations d'implémentation d'interface de la liste
 * des implémentations d'une classe ou une ou des déclarations d'extension
 * d'interface de la liste des extensions d'une interface.
 *
 * La valeur de l'annotation peut-être une chaîne ou un tableau de chaînes qui
 * sont le ou les noms des interfaces à enlever.
 *
 *
 * <h3>Exemple 1</h3>
 * <pre>
 * &#64;ImplementationToRemove("I1")
 * public class K implements I1, I2 {
 *
 * }
 * </pre>
 *
 * <p>
 * sera remplacé par
 * </p>
 *
 * <pre>
 * public class K implements I2 {
 *
 * }
 * </pre>
 *
 * <h3>Exemple 2</h3>
 * <pre>
 * &#64;ImplementationToRemove("I1")
 * public class K implements I1 {
 *
 * }
 * </pre>
 *
 * <p>
 * sera remplacé par
 * </p>
 *
 * <pre>
 * public class K {
 *
 * }
 * </pre>
 *
 *
 *
 * <h3>Exemple 3</h3>
 * <pre>
 * &#64;ImplementationToRemove({"I1", "I2"})
 * public class K implements I1, I2 {
 *
 * }
 * </pre>
 *
 * <p>
 * sera remplacé par
 * </p>
 *
 * <pre>
 * public class K {
 *
 * }
 * </pre>
 *
 *
 * <h2>Précautions</h2>
 * <p>
 * L'annotation &#64;ImplementationToRemove peut livrer à l'étudiant un code non
 * compilable.
 * </p>
 *
 * <p>
 * C'est normal puisque des fragments de code disparaissent. Des problèmes
 * peuvent survenir s'ils sont invoqués par ailleurs, notamment dans les tests
 * unitaires (mais pas uniquement).
 * </p>
 *
 * <p>
 * Il faut donc l'utiliser en connaissance de cause et
 * </p>
 * <ul>
 * <li>soit, enlever des fragments qui n'engendrent pas de code non
 * compilable,</li>
 * <li>soit, utiliser l'introspection dans les tests unitaires,</li>
 * <li> soit, prévenir l'étudiant.</li>
 * </ul>
 *
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface ImplementationToRemove {

    /**
     * Définit la ou les interfaces à retirer
     *
     * @return un tableau des noms des interfaces à retirer
     */
    String[] value();
}
