/*
 * Copyright 2019: Yvan Maillot
 * This software is part of the Caseine project.
 * This software was developped with the support of the following organizations:
 * Université de Haute-Alsace
 * Institut Polytechnique de Grenoble
 *
 *
 * This file is part of the VPL Design Tool.
 * The VPL Design Tool is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The VPL Design Tool is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the VPL Design Tool.  If not, see <https://www.gnu.org/licenses/>.
 */
package caseine.tags;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Pour vérifier que l'attribut annoté a un setter correct.
 *
 * @author Yvan Maillot &lt;yvan.maillot@uha.fr&gt;
 *
 * <h2>L'annotation &#64;caseine.tags.SetterToCheck</h2>
 *
 *
 * <p>
 * L'annotation &#64;SetterToCheck engendre une évaluation dont le but est de
 * vérifier l’existence du getter d'un attribut, s'il est bien signé, s'il est
 * bien formé et dans une moindre mesure son fonctionnement.
 *
 * </p>
 * * <h3>Cible</h3>
 * <p>
 * &#64;SetterToCheck ne s'applique qu'aux attributs de <strong>types
 * simples</strong>.
 * </p>
 * <h3>Formes d'emploi</h3>
 * <p>
 * &#64;SetterToCheck s'emploie
 * </p>
 * <ol>
 * <li>comme une annotation multivaluée : (e.g.  <pre>
 * &#64;SetterToCheck(
 *              grade = 1,
 *              priority = 2,
 *              requiersUnitTestsBefore = "1",
 *              value = "consigne")
 * </pre>
 * <p>
 * Voir la documentation des propriétés</p>
 * </li>
 * <li>comme une annotation monovaluée : (e.g. &#64;SetterToCheck("consigne à
 * l'étudiant")) où "consigne à l'étudiant" est le message à afficher en cas
 * d'erreur et toutes autres propriétés ont leur valeur par défaut.
 * </li>
 * <li>comme une annotation de marquage (e.g. &#64;SetterToCheck)<br>Dans ce
 * cas, toutes les propriétés ont leur valeur par défaut</li>
 * </ol>
 * <h3>Effet</h3>
 * <p>
 * L'annotation &#64;SetterToCheck a pour effet de générer une évaluation dont
 * le but est de vérifier que l’attribut annoté possède un setter
 * <ol>
 * <li>bien nommé : set suivi du nom de l'attribut avec son initiale en
 * majuscule,</li>
 * <li>bien signé : il ne retourne rien (void) et a un seul paramètre du type de l'attribut annoté,</li>
 * <li>fonctionnel : il modifie effectivement l'attribut annoté</li>
 * </ol>
 *
 * <h3>Précaution</h3>
 * <p>
 * Ne fonctionne que si l'attribut annoté est de type simple, que sa classe possède un constructeur disponible
 * publiquement et que le setter se content de modifier l'attribut annoté.
 * <p>
 *  * <h3>Utilisation de la propriété requiersUnitTestsBefore</h3>
 * <p>
 * Cette propriété est un tableau de chaînes de caractères
 * (<code>String[]</code>) dont chaque élément doit être:
 * </p>
 * <ul>
 * <li>soit une chaîne de caractères convertible en entier (e.g. "2")
 * <br>
 * Dans ce cas, cela signifie que toutes les évaluations automatiques de
 * priorité égale à cet entier converti (e.g. toutes les évaluations de priorité
 * 2) doivent réussir pour que celle-ci réussisse.</li>
 * <li>soit une chaîne de caractères qui représente le nom pleinement qualifié
 * d'un test unitaire accessible existant ou écrit par l'enseignant (e.g.
 * "edu.uha.miage.PointTest.checkOrigine").<br>
 * Dans ce cas, cela signifie que la méthode de test unitaire (e.g. public void
 * checkOrigine() de la classe public void edu.uha.miage.PointTest doit réussir
 * pour que cette évaluation réussisse.
 * </li>
 * </ul>
 *
 * <p>
 * <strong>Attention aux dépendances cycliques.</strong>
 * </p>
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface SetterToCheck {
    
    /**
     * Définit la consigne destinée à l'étudiant quand le test échoue.<br>
     * Quand la valeur par défaut est choisie, un message standard est généré.
     *
     * @return la consigne destinée à l'étudiant.
     */
    String value() default "";

    /**
     * définit l'ordre de priorité croissante du test généré dans sa classe de
     * tests unitaires
     *
     * @return l'ordre de priorité.
     */
    int priority() default 0;

    /**
     * définit le nombre de points attribués à la réussite de ce test ou une
     * valeur relative si le tag {@link caseine.tags.RelativeEvaluation} est
     * employé.<br>
     * <p>
     * Un grade négatif ou nul est ignoré.
     * </p>
     *
     * @return le grade.
     * 
     * @see caseine.tags.RelativeEvaluation
     */
    double grade() default Double.MIN_VALUE;

    /**
     * définit un tableau de chaînes de caractères qui représentent des tests
     * unitaires.
     * <p>
     * Chaque chaîne de ce tableau peut être
     * </p>
     * <ul>
     * <li>Soit convertible en entier, dans ce cas, elle représente tous des
     * tests unitaires de cette priorité.</li>
     * <li>Soit le nom pleinement qualifié d'une méthode de test unitaire.</li>
     * </ul>
     * <p>
     * Pour que le test unitaire annoté réussisse, il faut que tous les tests
     * unitaires représentés dans ce tableau réussissent.
     * </p>
     * <p>
     * <strong>Attention aux dépendances cycliques</strong>
     * </p>
     *
     *
     * @return le tableau des tests unitaires
     */
    String[] requiersUnitTestsBefore() default {};
}
