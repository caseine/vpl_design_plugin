/*
 * Copyright 2019: Yvan Maillot
 * This software is part of the Caseine project.
 * This software was developped with the support of the following organizations:
 * Université de Haute-Alsace
 * Institut Polytechnique de Grenoble
 *
 *
 * This file is part of the VPL Design Tool.
 * The VPL Design Tool is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The VPL Design Tool is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the VPL Design Tool.  If not, see <https://www.gnu.org/licenses/>.
 */
package caseine.tags;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Pour vérifier que la méthode annotée est fonctionnellement équivalente à une
 * méthode de référence sur la base de liste de jeux de tests.
 *
 * @author Yvan Maillot &lt;yvan.maillot@uha.fr&gt;
 *
 * <h2>L'annotation &#64;caseine.tags.ToCompare</h2>
 * <p>
 * Le principe consiste à comparer le résultat de la méthode de l'étudiant avec
 * celui de la méthode de la correction (de l'enseignant),
 * </p>
 * <ol>
 * <li>soit à partir de jeux de tests calculés aléatoirement.</li>
 * <li>soit à partir de jeux de tests choisis par l'enseignant. </li>
 * </ol>
 * <p>
 * <strong>Attention</strong>: la première solution est <strong>facile à
 * utiliser et commode</strong> mais aussi <strong>très limitée</strong>. D'une
 * part, il est nécessaire que les paramètres de la méthode ainsi que les
 * attribut de l'objet sur lequel elle s'applique soient de type primitif.
 * D'autre part, les jeux de tests générés ne sont pas toujours pertinents.
 * </p>
 *
 * <p>
 * <strong>Attention</strong>: la deuxième est <strong>plus compliquée</strong>
 * à mettre en place mais
 * <strong>pratiquement sans limite</strong>.
 * </p>
 *
 * <h3>Cible</h3>
 * <p>
 * &#64;ToCompare ne s'applique qu'aux méthodes.
 * </p>
 *
 * <h3>Formes d'emploi</h3>
 * <p>
 * &#64;ToCompare s'emploi comme
 * </p>
 * <ol>
 * <li><strong>une annotation multivaluée</strong>
 * (<code>&#64;ToCompare(priority = 1, grade = 1)</code>)
 * <p>
 * Les propriétés possibles son :
 * <ul>
 * <li><strong>value</strong> de type <code>String</code> : le message à
 * afficher en cas d'échec de l'évaluation.<br>
 * <i>Par défaut, un message standard est proposé.</i></li>
 * <li><strong>priority</strong> de <code>int</code> : le numéro d'ordre
 * (croissant) de traitement de l'évaluation.<br>
 * <i>Par défaut, 0.</i></li>
 * <li><strong>grade</strong> de type <code>double</code> : la valeur donnée de
 * réussite de l'évaluation (avant un éventuel ajustement avec null {@link caseine.tags.RelativeEvaluation
 * &#64;RelativeEvaluation}).<br>
 * <i>Par défaut, Double.MIN_VALUE</i></li>
 *
 * <li>
 * <strong>testSetsMethodName</strong> de type <code>String</code> :
 * <ul> <li>Si elle n'est pas renseignée (ou égale à <code>""</code>), les jeux
 * de tests sont générés aléatoirement. Dans ce cas, <strong>attention aux
 * limitations</strong>.</li>
 * <li>Sinon, elle doit représenter le nom pleinement qualifiée d'une méthode (à
 * écrire par l'enseignant) qui retourne une liste de jeux de tests à employer
 * pour l'évaluation.</li>
 * </ul>
 * <i>Par défaut <code>""</code> (i.e. elle n'est pas renseignée).</i>
 * </li>
 * <li><strong>numberOfAleaTestSets</strong> de type <code>int</code> : cette
 * propriété n'est utile que si <strong>testSetsMethodName</strong> n'est pas
 * renseignée. Elle définit le nombre de jeux de tests aléatoire générés.<br>
 * <i>Par défaut <code>100</code>.</i>
 * </li>
 * <li><strong>checkStdOut</strong> de type <code>boolean</code> : l'évaluation
 * tient compte de la sortie standard (affichage à l'écran) si
 * <code>true</code>.<br>
 * <i>Par défaut <code>false</code>.</i></li>
 * <li><strong>inputString</strong> de type <code>String</code> : le flux
 * d'entrée attendu par la méthode à évaluer (quand celle-ci lit des données au
 * clavier).<br>
 * <i>Par défaut <code>""</code>. </i></li>
 * <li><strong>requiersUnitTestsBefore</strong> de type <code>String[]</code> :
 * permet de s'assurer que des conditions préalables soient satisfaits avant de
 * lancer cette évaluation. <br>
 * <i>Par défaut <code>{}</code>.</i>
 * </li>
 * <li><strong>comparatorMethodName</strong> de type String : cette propriété
 * n'est utile que si <strong>testSetsMethodName</strong> est (correctement)
 * renseignée. Dans ce cas, elle doit représenter le nom pleinement qualifiée
 * d'une méthode (à écrire par l'enseignant) qui définit la comparaison. <br>
 * <i>Par défaut <code>""</code>.</i></li>
 * </ul>
 * </li>
 * <li><strong>une annotation monovaluée</strong>
 * (<code>&#64;ToCompare("consigne")</code>) ou <code>"consigne"</code> est une
 * consigne à l'étudiant en cas d'échec de l'évaluation.</li>
 * <li><strong>une annotation de marquage</strong> (<code>&#64;ToCompare</code>)
 * Un message standard sera donné à l'étudiant en cas d'échec de l'évaluation.
 * Pour le reste des propriétés et les effets, voir les valeurs par défaut des
 * propriétés et les explications dans la suite.</li>
 *
 * </ol>
 * <p>
 * <strong>De nombreuses explications complémentaires sont données dans la
 * suite. À lire attentivement si l'on utilise les propriétés
 * <strong>testSetsMethodName</strong> ou
 * <strong>comparatorMethodName</strong></strong>
 * </p>
 * <h3>Effets</h3>
 * <p>
 * L'annotation <code>&#64;ToCompare</code> a pour effet de générer une
 * évaluation qui compare le résultat de la méthode de l'étudiant avec celui de
 * la méthode de référence à partir de jeux de tests générés au hasard ou
 * choisis par l'enseignant.
 * </p>
 * <p>
 * La suite l'explique en détail.
 * </p>
 *
 * <h3>Utilisation de la propriété <code>testSetsMethodName</code></h3>
 * <p>
 * Cette propriété est une chaîne de caractères (<code>String</code>) qui
 * représente le nom pleinement qualifié d'une méthode. Laquelle est chargée de
 * retourner une liste de jeux de tests qui seront employés lors de
 * l'évaluation. Elle doit satisfaire les conditions suivantes :
 * </p>
 * <ul>
 * <li>être accessible, i.e. publique et dans une classe publique (en général,
 * on choisit une classe tests unitaires),</li>
 * <li>être déclarée static,</li>
 * <li>ne prendre aucun paramètre,</li>
 * <li>retourner une <i>liste de jeux de tests</i> (i.e.
 * {@code java.util.List<}{@link caseine.publication.producers.ToCompareProducer.InstanceAndParameters caseine.publication.producers.ToCompareProducer.InstanceAndParameters}{@code >}),
 * </ul>
 * <p>
 * Par exemple,
 * </p>
 * <p>
 * {@code testSetsMethodName = "edu.uha.miage.AClassTest.testSetsForAMethod"}
 * </p>
 * <p>
 * signifie que les jeux de tests sont issus de la méthode
 * </p>
 * <p>
 * <strong>{@code public static List<InstanceAndParameters> testSetsForAMethod()}</strong>
 * </p>
 * <p>
 * de la classe {@code AClassTest}.
 * </p>
 *
 *
 * <h3>Utilisation de la propriété <code>comparatorMethodName</code></h3>
 * <p>
 * Cette propriété est une chaîne de caractères (<code>String</code>) qui
 * représente le nom pleinement qualifié d'une méthode qui retourne une chaîne
 * de caractères et reçoit en paramètre un objet contenant le nécessaire pour
 * faire les comparaisons.
 * </p>
 * <p>
 * Elle doit satisfaire les conditions suivantes : </p>
 * <ul>
 * <li>être accessible, i.e. publique et dans une classe publique (en général,
 * on choisit une classe tests unitaires),</li>
 * <li>être déclarée static,</li>
 * <li>prendre un seul paramètre de type
 * {@link caseine.publication.producers.ToCompareProducer.ResultForComparison},</li>
 * <li>retourner une chaîne de caractère.</li>
 * </ul>
 * <p>
 * Par exemple,
 * </p>
 * <p>
 * {@code comparatorMethodName = "edu.uha.miage.AClassTest.aMethodComparator"}
 * </p>
 * <p>
 * représente la méthode
 * </p>
 * <p>
 * <strong>{@code public static String aMethodComparator(caseine.publication.producers.ToCompareProducer.ResultForComparison rfc)}</strong>
 * </p>
 * <p>
 * de la classe {@code AClassTest}.
 * </p>
 * <p>
 * <strong>
 * La propriété <code>comparatorMethodName</code> doit nécessairement s'utiliser
 * conjointement avec la propriété <code>testSetsMethodName</code>.
 * </strong></p>
 *
 * L'enseignant doit écrire <code>comparatorMethodName</code> de sorte qu'elle
 * retourne
 *
 * <ol>
 * <li><code>"OK"</code> si la comparaison à l'aide des données fournies par le
 * paramètre <code>ResultForComparison</code> est satisfaisante,</li>
 * <li>ou autre chose dans le cas contraire. Cet autre chose peut être un
 * message destiné à l'étudiant dont le contenu est laissé à l'appréciation de
 * l'enseignant.</li>
 * </ol>
 *
 *
 * Voir
 * <code>{@link caseine.publication.producers.ToCompareProducer.ResultForComparison ResultForComparison}</code>
 * est documentée dans la suite.
 *
 * <h3>Utilisation de la propriété <code>requiersUnitTestsBefore</code></h3>
 * <p>
 * Cette propriété est un tableau de chaînes de caractères
 * (<code>String[]</code>) dont chaque élément doit être :
 * </p>
 * <ol>
 * <li>soit une chaîne de caractères convertible en entier (e.g.
 * <code>"2"</code>).
 * <br>
 * Dans ce cas, cela signifie que toutes les évaluations automatiques de
 * priorité égale à cet entier converti (e.g. toutes les évaluations de priorité
 * 2) doivent réussir pour que celle-ci réussisse.
 * </li>
 * <li>soit une chaîne de caractères qui représente le nom pleinement qualifié
 * d'un test unitaire accessible existant ou écrit par l'enseignant (e.g.
 * <code>"edu.uha.miage.PointTest.checkOrigine"</code>).
 * <br>
 * Dans ce cas, cela signifie que la méthode de test unitaire (e.g.
 * <code>"public void checkOrigine()"</code> de la classe <code>"public void
 * edu.uha.miage.PointTest"</code>) doit réussir pour que cette évaluation
 * réussisse.
 * </li>
 * </ol>
 *
 * <p>
 * <strong>Attention aux dépendances cycliques.</strong>
 * </p>
 *
 * @see caseine.publication.producers.ToCompareProducer.InstanceAndParameters
 *
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface ToCompare {


    /**
     * Définit la consigne destinée à l'étudiant quand le test échoue.<br>
     * Quand la valeur par défaut est choisie, un message standard est généré.
     *
     * @return la consigne destinée à l'étudiant.
     */
    String value() default "";

    /**
     * définit l'ordre de priorité croissante du test généré dans sa classe de
     * tests unitaires
     *
     * @return l'ordre de priorité.
     */
    int priority() default 0;

    /**
     * définit le nombre de points attribués à la réussite de ce test ou une
     * valeur relative si le tag {@link caseine.tags.RelativeEvaluation} est
     * employé.<br>
     * <p>
     * Un grade négatif ou nul est ignoré.
     * </p>
     *
     * @return le grade.
     * 
     * @see caseine.tags.RelativeEvaluation
     */
    double grade() default Double.MIN_VALUE;

    String testSetsMethodName() default "";

    boolean checkStdOut() default false;

    String comparatorMethodName() default "";

    int numberOfAleaTestSets() default 100;

    String inputString() default "";

    /**
     * définit un tableau de chaînes de caractères qui représentent des tests
     * unitaires.
     * <p>
     * Chaque chaîne de ce tableau peut être
     * </p>
     * <ul>
     * <li>Soit convertible en entier, dans ce cas, elle représente tous des
     * tests unitaires de cette priorité.</li>
     * <li>Soit le nom pleinement qualifié d'une méthode de test unitaire.</li>
     * </ul>
     * <p>
     * Pour que le test unitaire annoté réussisse, il faut que tous les tests
     * unitaires représentés dans ce tableau réussissent.
     * </p>
     * <p>
     * <strong>Attention aux dépendances cycliques</strong>
     * </p>
     *
     *
     * @return le tableau des tests unitaires
     */
    String[] requiersUnitTestsBefore() default {};
}
