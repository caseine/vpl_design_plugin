/*
 * Copyright 2019: Yvan Maillot
 * This software is part of the Caseine project.
 * This software was developped with the support of the following organizations:
 * Université de Haute-Alsace
 * Institut Polytechnique de Grenoble
 * 
 * 
 * This file is part of the VPL Design Tool.
 * The VPL Design Tool is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * The VPL Design Tool is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with the VPL Design Tool.  If not, see <https://www.gnu.org/licenses/>.
 */
package caseine.tags;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Pour retirer tout fragment annotable et éventuellement le remplacer par autre
 * chose.
 *
 * @author Yvan Maillot &lt;yvan.maillot@uha.fr&gt;
 *
 * <h2>L'annotation &#64;caseine.tags.ToDo</h2>
 *
 *
 * <p>
 * L'annotation &#64;ToDo permet de retirer n'importe quel élément annotable et
 * éventuellement le remplacer par un autre.
 * </p>
 ** <h3>Cible</h3>
 * <p>
 * &#64;ToDo s'applique à tout élément annotable.
 * </p>
 *
 *
 * <h3>Formes d'emploi</h3>
 * <p>
 * &#64;ToDo s'emploie
 * </p>
 * <ol>
 * <li>comme une annotation multivaluée : e.g. &#64;ToDo(value = "une consigne à
 * l'étudiant", replacement="public static String emptyString{ return \"\"; }")
 * </li>
 * <li>comme une annotation monovaluée : e.g. &#64;ToDo("une consigne à
 * l'étudiant")
 * </li>
 * <li>comme une annotation de marquage : &#64;ToDo</li>
 * </ol>
 * <h3>Effet</h3>
 * L'annotation &#64;ToDo a pour effet
 * <ol>
 * <li>selon que la propriété <strong>replacement</strong> soit renseignée ou
 * non:
 * <ol>
 * <li>Si elle n'est pas renseignée (ou vide), de supprimer l'élément annoté
 * .</li>
 * <li>Si elle est renseignée (et non vide), de remplacer l'élément annoté par
 * un autre défini par la propriété <strong>replacement</strong> (cf.
 * <i>Utilisation de la propriété
 * <strong>replacement</strong> ci-après</i>).
 * </li>
 * </ol>
 * </li>
 * </ol>
 *
 * <h3>Utilisation de la propriété <strong>replacement</strong></h3>
 * <p>
 * L'usage de la propriété <strong>replacement</strong> nécessite de prendre
 * quelques précautions.
 * </p>
 * <p>
 * Si elle est présente et différente de la chaîne vide, l'élément annoté est
 * </p>
 * <ol>
 * <li>soit remplacé par le contenu d'un fichier dont le chemin est donné par la
 * valeur de la propriété <strong>replacement</strong>, s'il s'agit du chemin
 * d'un ficher accessible.</li>
 * <li>soit remplacé par cette valeur elle-même sinon.
 * </li>
 * </ol>
 * <p>
 * Si l'élément annoté possède un commentaire de Javadoc, il est rajouté à
 * l'élément de remplacement, si celui-ci le permet.
 * </p>
 * <p>
 * Si la consigne (value) est présente et différente de la chaîne vide, elle est
 * rajoutée en commentaire // TODO la consigne.
 * </p>
 * <p>
 * <strong>Attention</strong>, la consigne prévaut sur la javadoc. Il ne pas y
 * avoir à la fois commentaire et javadoc.
 * </p>
 * <p>
 * <strong>Attention</strong>, pour pouvoir déployer sur le serveur, il faut
 * absolument que le code de remplacement soit <strong>parsable</strong>.
 * </p>
 * <ul>
 * <li>Tout dépend de l'élément annoté,
 * <ul>
 * <li>si c'est une classe une autre classe (de même nom) ou même une interface
 * ou n'importe quel type de même nom.</li>
 * <li>si c'est un membre de classe, il peut être remplacé par n'importe quel
 * autre membre ou même par un bloc d'instructions.</li>
 * <li>si c'est un paramètre, il peut être remplacé par un autre paramètre</li>
 * <li>etc.</li>
 * </ul>
 * </ul>
 *
 *
 * <h3>Exemples d'annotation de marquage</h3>
 *
 * <h4>Exemple 1</h4>
 *
 * <pre>
 * &#64;ToDo
 * public class Vecteur {
 *
 *   private double x, y;
 *
 *   public Vecteur(double x, double y) {
 *      this.x = x;
 *      this.y = y;
 *   }
 *
 *   public Vecteur() {
 *      this(0.0, 0.0);
 *   }
 *
 *   public double norme() {
 *      return sqrt(x*x+y*y);
 *   }
 * }
 * </pre>
 *
 * <p>
 * sera tout bonnement effacé mais le fichier est conservé.
 * </p>
 *
 *
 * <h4>Exemple 2</h4>
 *
 * <pre>
 * public class Vecteur {
 *
 *   &#64;ToDo
 *   private double x, y;
 *
 *   public Vecteur(double x, double y) {
 *      this.x = x;
 *      this.y = y;
 *   }
 *
 *   public Vecteur() {
 *      this(0.0, 0.0);
 *   }
 *
 *   public double norme() {
 *      return sqrt(x*x+y*y);
 *   }
 * }
 * </pre>
 *
 * <p>
 * sera remplacé par
 * </p>
 * <pre>
 * public class Vecteur {
 *
 *   public Vecteur(double x, double y) {
 *      this.x = x;
 *      this.y = y;
 *   }
 *
 *   public Vecteur() {
 *      this(0.0, 0.0);
 *   }
 *
 *   public double norme() {
 *      return sqrt(x*x+y*y);
 *   }
 * }
 * </pre>
 *
 * <p>
 * <strong>Ce code n'est pas compilable</strong>
 * </p>
 *
 * <h4>Exemple 3</h4>
 *
 * <pre>
 * public class Vecteur {
 *
 *   private double x, y;
 *
 *   public Vecteur(&#64;ToDo double x, double y) {
 *      this.x = x;
 *      this.y = y;
 *   }
 *
 *   public Vecteur() {
 *      this(0.0, 0.0);
 *   }
 *
 *   public double norme() {
 *      return sqrt(x*x+y*y);
 *   }
 * }
 * </pre>
 *
 * <p>
 * sera remplacé par
 * </p>
 * <pre>
 * public class Vecteur {
 *
 *   private double x, y;
 *
 *   public Vecteur(double y) {
 *      this.x = x;
 *      this.y = y;
 *   }
 *
 *   public Vecteur() {
 *      this(0.0, 0.0);
 *   }
 *
 *   public double norme() {
 *      return sqrt(x*x+y*y);
 *   }
 * }
 * </pre>
 *
 * <p>
 * <strong>Ce code n'est pas compilable</strong>
 * </p>
 *
 *
 * <h4>Exemple 4</h4>
 *
 * <pre>
 * public class Vecteur {
 *
 *   private double x, y;
 *
 *   public Vecteur(double x, double y) {
 *      this.x = x;
 *      this.y = y;
 *   }
 *
 *   &#64;ToDo
 *   public Vecteur() {
 *      this(0.0, 0.0);
 *   }
 *
 *   public double norme() {
 *      return sqrt(x*x+y*y);
 *   }
 * }
 * </pre>
 *
 * <p>
 * sera remplacé par
 * </p>
 * <pre>
 * public class Vecteur {
 *
 *   private double x, y;
 *
 *   public Vecteur(double x, double y) {
 *      this.x = x;
 *      this.y = y;
 *   }
 *
 *   public double norme() {
 *      return sqrt(x*x+y*y);
 *   }
 * }
 * </pre>
 *
 * <p>
 * <strong>Ce code est compilable</strong>
 * </p>
 *
 *
 * <h3>Exemple d'annotation monovaluée</h3>
 * <pre>
 * &#64;ToDo("Écrire une méthode qui retourne la norme de ce vecteur")
 * public double norme() {
 *    return sqrt(x*x+y*y);
 * }
 * </pre>
 *
 * <p>
 * sera remplacé par
 * </p>
 *
 * <pre>
 * // TODO Écrire une méthode qui retourne la norme de ce vecteur
 * </pre>
 *
 * <h3>Exemple d'annotation multivaluée (sans fichier)</h3>
 *
 * <h4>Exemple 1</h4>
 *
 * <pre>
 * &#64;ToDo(value = "Cette méthode contient des erreurs. Corrigez-là !",
 *           replacement="private int norme(double x, double y) { return x*x+y*y; }")
 * public double norme() {
 *    return sqrt(x*x+y*y);
 * }
 * </pre>
 *
 * <p>
 * sera remplacé par
 * </p>
 *
 *
 * <pre>
 * // TODO Cette méthode contient des erreurs. Corrigez-là !
 * private int norme(double x, double y) {
 *    return x * x + y * y;
 * }
 * </pre>
 *
 *
 * <h4>Exemple 2</h4>
 *
 * <pre>
 * // Revoir les paramètres de ce constructeur
 * public Vecteur(@ToDo(replacement = "double _X") double x, @ToDo(replacement = "double _Y") double y) {
 *    this.x = x;
 *    this.y = y;
 * }
 * </pre>
 *
 * <p>
 * sera remplacé par
 * </p>
 *
 *
 * <pre>
 * // Revoir les paramètres de ce constructeur
 * public Vecteur(double _X, double _Y) {
 *    this.x = x;
 *    this.y = y;
 * }
 * </pre>
 *
 *
 * <h3>Exemple d'annotation multivaluée (avec fichier)</h3>
 *
 * <pre>
 * &#64;ToDo(
 *     value = "Cette classe est très mal écrite et même pas compilable.
 *              Corrigez là. Faites le nettoyage. Mais qu'elle remplisse toujours le même office.",
 *     replacement = "vecteur.txt")
 * &#64;ToCheck(grade = 1, priority = 5) 
 * public class Vecteur {
 *
 *    &#64;ToCheck(grade = 1, priority = 10)
 *    &#64;GetterToCheck(grade = 1, priority = 15)
 *    &#64;SetterToCheck(grade = 1, priority = 20)
 *    private double x, y;
 *
 *    &#64;ToCheck(grade = 1, priority = 12)
 *    public Vecteur(double x, double y) {
 *       this.x = x;
 *       this.y = y;
 *    }
 *
 *    &#64;ToCheck(grade = 1, priority = 13)
 *    public Vecteur() {
 *       this(0.0, 0.0);
 *    }
 *
 *    &#64;ToCompare(value = "Mauvais calcul de la norme", grade = 1, priority = 30)
 *    public double norme() {
 *       return sqrt(x * x + y * y);
 *    }
 *
 *    public double getX() {
 *       return x;
 *    }
 *
 *    public double getY() {
 *       return y;
 *    }
 *
 *    public void setX(double x) {
 *       this.x = x;
 *    }
 *
 *    public void setY(double y) {
 *       this.y = y;
 *    }
 * }
 * </pre>
 *
 * <p>
 * si le fichier <i><strong>vecteur.txt</strong></i> contient:
 * </p>
 *
 * <pre>
 * public class vecteur {
 *
 *    public double x, y;
 *
 *    Vecteur(double _X, double _Y) {
 *       x = _X;
 *       y = _X;
 *    }
 *
 *    Vecteur() {
 *       x = 0.0;
 *       y = 0.0;
 *    }
 *
 *    double Norme() {
 *       return x * x + y * y;
 *    }
 * }
 * </pre>
 *
 * <p>
 * sera remplacé par
 * </p>
 *
 *
 * <pre>
 * // TODO Cette classe est très mal écrite et même pas compilable. Corrigez là. Faites le nettoyage. Mais qu'elle remplisse toujours le même office.
 * public class vecteur {
 *
 *    public double x, y;
 *
 *    Vecteur(double _X, double _Y) {
 *       x = _X;
 *       y = _X;
 *    }
 *
 *    Vecteur() {
 *       x = 0.0;
 *       y = 0.0;
 *    }
 *
 *    double Norme() {
 *       return x * x + y * y;
 *    }
 * }
 * </pre>
 *
 *
 * <p>
 * <strong>Ce code n'est pas compilable</strong>
 * </p>
 * 
 *
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface ToDo {

    /**
     *
     * @return La consigne à destination de l'étudiant. Elle sera précédée de //
     * TODO ou rien si c'est la value par défaut.
     */
    String value() default "";

    String replacement() default "";
}
