/*
 * Copyright 2019: Yvan Maillot
 * This software is part of the Caseine project.
 * This software was developped with the support of the following organizations:
 * Université de Haute-Alsace
 * Institut Polytechnique de Grenoble
 * 
 * 
 * This file is part of the VPL Design Tool.
 * The VPL Design Tool is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * The VPL Design Tool is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with the VPL Design Tool.  If not, see <https://www.gnu.org/licenses/>.
 */
package caseine.tags;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Pour vider ou remplacer le contenu d'une méthode.
 * 
 * @author Yvan Maillot &lt;yvan.maillot@uha.fr&gt;
 *
 * <h2>L'annotation &#64;caseine.tags.ToDoIn</h2>
 *
 *
 * <p>
 * L'annotation &#64;ToDoIn permet de retirer le corps des méthodes qu'elle
 * annote ou de le remplacer par un autre code.
 * </p>
 ** <h3>Cible</h3>
 * <p>
 * &#64;ToDoIn ne s'applique qu'aux méthodes.
 * </p>
 *
 *
 * <h3>Formes d'emploi</h3>
 * <p>
 * &#64;ToDoIn s'emploie
 * </p>
 * <ol>
 * <li>comme une annotation multivaluée : e.g. &#64;ToDoIn(value = "une consigne
 * à l'étudiant", replacement="{ return \"\"; }")
 * </li>
 * <li>comme une annotation monovaluée : e.g. &#64;ToDoIn("une consigne à
 * l'étudiant")
 * </li>
 * <li>comme une annotation de marquage : &#64;ToDoIn</li>
 * </ol>
 * <h3>Effet</h3>
 * L'annotation &#64;ToDoIn a pour effet
 * <ol>
 * <li>dans tous les cas, de conserver la signature de la méthode,</li>
 * <li>et selon que la propriété <strong>replacement</strong> soit renseignée ou
 * non:
 * <ol>
 * <li>Si elle n'est pas renseignée (ou vide), de remplacer son contenu par une
 * instruction minimale qui garantit la compilation, suivie d'un commentaire qui
 * commence par // TODO suivi de la consigne à l'étudiant.</li>
 * <li>Si elle est renseignée (et non vide), de remplacer le corps de la méthode
 * par du code défini par la valeur de cette propriété (cf. <i>Utilisation de la
 * propriété <strong>replacement</strong> ci-après</i>).
 * </li>
 * </ol>
 * </li>
 * </ol>
 *
 * <h3>Utilisation de la propriété <strong>replacement</strong></h3>
 * <p>
 * Sans renseigner la propriété <strong>replacement</strong> (c'est-à-dire par
 * défaut), l'emploi de &#64;ToDoIn est assez naturelle.
 * </p>
 * <p>
 * En revanche, l'utilisation de la propriété <strong>replacement</strong>
 * nécessite de prendre quelques précautions :
 * </p>
 * <p>
 * Le corps de la méthode est
 * </p>
 * <ol>
 * <li>soit remplacé par le contenu d'un fichier dont le chemin est donné par la
 * valeur de la propriété <strong>replacement</strong>, s'il s'agit du chemin
 * d'un ficher accessible.</li>
 * <li>soit remplacé par cette valeur elle-même sinon.
 * </li>
 * </ol>
 * <p>
 * <strong>Attention</strong>, pour pouvoir déployer sur le serveur, il faut
 * absolument que le code de remplacement soit <strong>parsable</strong>.
 * </p>
 * <ul>
 * <li>Il doit notamment commencer par { et finir par }</li>
 * <li>S'il contient des guillemets, ils devront être invalidés dans la valeur
 * de la propriété (puisqu'elle est elle-même entre guillemets).<br>
 * Par exemple, pour produire { return ""; }, il faut écrire replacement="{
 * return \"\"; }"</li>
 * <li>Ça peut devenir compliqué s'il faut ajouter des sauts de lignes ('\n'),
 * des tabulations ('\t'), des caractères unicode, etc.<br>
 * Dans ces cas, et le plus souvent, l'emploi d'un fichier externe est
 * préférable.</li>
 * </ul>
 *
 *
 * <h3>Exemple d'annotation de marquage</h3>
 *
 *
 * <pre>
 * &#64;ToDoIn
 * public double norme() {
 *    return sqrt(x*x+y*y);
 * }
 * </pre>
 *
 * <p>
 * sera remplacé par
 * </p>
 *
 *
 * <pre>
 * public double norme() {
 *    return 0.0;
 * // TODO
 * }
 * </pre>
 *
 * <h3>Exemple d'annotation monovaluée</h3>
 * <pre>
 * &#64;ToDoIn("Retourner la norme du vecteur")
 * public double norme() {
 *    return sqrt(x*x+y*y);
 * }
 * </pre>
 *
 * <p>
 * sera remplacé par
 * </p>
 *
 * <pre>
 * public double norme() {
 *    return 0.0;
 * // TODO Retourner la norme du vecteur
 * }
 * </pre>
 *
 * <h3>Exemple d'annotation multivaluée (sans fichier)</h3>
 *
 *
 * <pre>
 * &#64;ToDoIn(value="Ce code est faux. Corrigez-le", replacement="{double x = 0, y = 0; return x*x+y*y;}")
 * public double norme() {
 *    return sqrt(x*x+y*y);
 * }
 * </pre>
 *
 * <p>
 * sera remplacé par
 * </p>
 *
 *
 * <pre>
 * public double norme() {
 *    double x = 0, y = 0;
 *    return x*x+y*y;
 * // TODO Ce code est faux. Corrigez-le
 * }
 * </pre>
 *
 * <h3>Exemple d'annotation multivaluée (avec fichier)</h3>
 *
 * <pre>
 * &#64;ToDoIn(replacement = "afficher.replacement")
 * public static void afficher() {
 *     System.out.println("Pour afficher : ");
 *     System.out.println("\\ il faut écrire  System.out.println(\"\\\\\");");
 *     System.out.println("\" =&gt; System.out.println(\"\\\"\");");
 *     System.out.println("\u03C0 =&gt; System.out.println(\"\\u03C0\");");
 *     System.out.println("\u03C0\t\"\u03C0\" =&gt; System.out.println(\"\\u03C0\\t\\\"u03C0\\\");");
 *     System.out.println("Une tabulation =&gt; System.out.println(\"\\t\");");
 *     System.out.println("\\t =&gt; System.out.println(\"\\\\t\");");
 * }
 * </pre>
 * <p>
 * Si "afficher.replacement" est le chemin valide d'un fichier qui contient ceci
 * :
 * </p>
 * <pre>
 * {
 *     System.out.println("Pour afficher : ");
 *     System.out.println("\\ il faut écrire  System.out.println(\"\\\\\");");
 *     System.out.println("\" =&gt; System.out.println(\"\\\"\");");
 *     System.out.println("\u03C0 =&gt; System.out.println(\"\\u03C0\");");
 *     System.out.println("\u03C0\t\"\u03C0\" =&gt; System.out.println(\"\\u03C0\\t\\\"u03C0\\\");");
 *     // Exécutez ce programme et observez !
 *     // Puis continuez le pour afficher encore ceci :
 *     // Une tabulation =&gt; System.out.println("\t");
 *     // \t =&gt; System.out.println("\\t");
 * }
 * </pre>
 * 
 * <p>
 * <strong>Attention</strong> les accolades sont nécessaires.
 * </p>
 *
 * <p>
 * sera remplacé par
 * </p>
 *
 *
 * <pre>
 * public static void afficher() {
 *     System.out.println("Pour afficher : ");
 *     System.out.println("\\ il faut écrire  System.out.println(\"\\\\\");");
 *     System.out.println("\" =&gt; System.out.println(\"\\\"\");");
 *     System.out.println("\u03C0 =&gt; System.out.println(\"\\u03C0\");");
 *     System.out.println("\u03C0\t\"\u03C0\" =&gt; System.out.println(\"\\u03C0\\t\\\"u03C0\\\");");
 * // Exécutez ce programme et observez !
 * // Puis continuez-le pour afficher encore ceci :
 * // Une tabulation =&gt; System.out.println("\t");
 * // \t =&gt; System.out.println("\\t");
 * // TODO
 * }
 * </pre>
 *
 * <p>
 * <strong>Remarque</strong>
 * </p>
 * <p>
 * Les commentaires de la javadoc sont toujours conservés.
 * </p>
 *
 * <h2>Précautions</h2>
 * <p>
 * <strong>
 * &#64;ToDoIn n'entraîne jamais de code non compilable, si la propriété
 * <strong>replacement</strong> n'est pas utilisée.
 * </strong></p>
 * <p>
 * En revanche, si la propriété <strong>replacement</strong> est utilisée, il
 * est possible se retrouver dans l'impossible de pousser votre VPL sur le
 * serveur de Caséine.
 * </p>
 * <p>
 * Pour plus de détails relire la section <i>"Utilisation de la propriété
 * <strong>replacement</strong>"</i> plus haut.
 * </p>
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface ToDoIn {

    /**
     * Définit la consigne à destination de l'étudiant. Elle sera dans le corps
     * de la méthode et précédée de // TODO
     *
     * Par défaut, la consigne est vide.
     *
     * @return La consigne à destination de l'étudiant.
     */
    String value() default "";

    /**
     * Définit le remplacement du corps de la méthode annotée.
     *
     * @return une chaîne de caractères qui est <ol>
     * <li>soit la chaîne vide (par défaut)</li>
     * <li>soit le chemin valide d'un fichier qui contient le code à
     * remplacer</li>
     * <li>soit le code lui-même</li>
     * </ol>
     * <p>
     * <strong>Attention</strong> ce code doit être compilable.</p>
     */
    String replacement() default "";
}
