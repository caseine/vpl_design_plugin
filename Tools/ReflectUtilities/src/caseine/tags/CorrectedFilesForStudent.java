/*
 * Copyright 2019: Yvan Maillot
 * This software is part of the Caseine project.
 * This software was developped with the support of the following organizations:
 * Université de Haute-Alsace
 * Institut Polytechnique de Grenoble
 * 
 * 
 * This file is part of the VPL Design Tool.
 * The VPL Design Tool is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * The VPL Design Tool is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with the VPL Design Tool.  If not, see <https://www.gnu.org/licenses/>.
 */
package caseine.tags;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Pour donner la correction aux étudiants.
 * 
 * @author Yvan Maillot &lt;yvan.maillot@uha.fr&gt;
 *
 * <h2>L'annotation &#64;caseine.tags.FileToRemove</h2>
 *
 *
 * <p>
 * L'annotation &#64;CorrectedFilesForStudent, si elle est présente livre à l'étudiant la correction de l'exercice.
 *
 * </p>
 * <h3>Cible</h3>
 * <p>
 * &#64;CorrectedFilesForStudent ne s'applique qu'aux types publiques.
 * </p>
 *
 * <h3>Formes d'emploi</h3>
 * <p>
 * &#64;CorrectedFilesForStudent ne s'emploie que comme une annotation de marquage : e.g.
 * <code>&#64;CorrectedFilesForStudent</code>
 * </p>
 *
 * <h3>Précautions</h3>
 *
 * Attention à ne pas oublier d'enlever cette annotation pour un fonctionnement normal
 *
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface CorrectedFilesForStudent {
}
