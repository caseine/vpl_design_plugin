/*
 * Copyright 2019: Yvan Maillot
 * This software is part of the Caseine project.
 * This software was developped with the support of the following organizations:
 * Université de Haute-Alsace
 * Institut Polytechnique de Grenoble
 * 
 * 
 * This file is part of the VPL Design Tool.
 * The VPL Design Tool is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * The VPL Design Tool is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with the VPL Design Tool.  If not, see <https://www.gnu.org/licenses/>.
 */
package caseine.tags;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Pour vérifier que le nombre d'instructions de la méthode annotée, ou du
 * constructeur annoté, est dans un certain intervalle.
 *
 * @author Yvan Maillot &lt;yvan.maillot@uha.fr&gt;
 *
 *
 * <h2>L'annotation &#64;caseine.tags.StatementCountToCheck</h2>
 * <p>
 * L'annotation &#64;StatementCountToCheck engendre une évaluation qui vérifie
 * le nombre d'instructions de la méthode annotée ou du constructeur annoté.
 * </p>
 * <h3>Cible</h3>
 * <p>
 * &#64;StatementCountToCheck s'applique à
 * </p>
 * <ul>
 * <li>une méthode</li>
 * <li>un constructeur</li>
 * </ul>
 *
 *
 * <h3>Formes d'emploi</h3>
 * <p>
 * &#64;StatementCountToCheck s'emploie
 * </p>
 * <ol>
 * <li>comme une annotation multivaluée : (e.g.  <pre>
 * &#64;ToCheck(checkImplements = CheckImplements.NONE,
 *              grade = 1,
 *              modifiers = {CheckModifier.isAbstract, CheckModifier.isPrivate},
 *              priority = 2,
 *              requiersUnitTestsBefore = "1",
 *              value = "consigne")
 * </pre>
 * </li>
 * </ol>
 * <h3>Effet</h3>
 * <p>
 * L'annotation &#64;StatementCountToCheck a pour effet de générer une
 * évaluation qui contrôle que le nombre d'instructions se trouve entre
 * {@code minStmt} inclus et {@code maxStmt} inclus.
 * </p>
 * <p>
 * Par défaut, {@code minStmt = 1} et {@code maxStmt = 1}, ce qui signifie que
 * par défaut, on vérifie que la méthode annotée ou le constructeur annoté ne
 * contient qu'une et une seule instructions.
 * </p>
 *
 *
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD, ElementType.CONSTRUCTOR})
public @interface StatementCountToCheck {

    /**
     * Définit la consigne destinée à l'étudiant quand le test échoue.<br>
     * Quand la valeur par défaut est choisie, un message standard est généré.
     *
     * @return la consigne destinée à l'étudiant.
     */
    String value() default "";

    /**
     * définit l'ordre de priorité croissante du test généré dans sa classe de
     * tests unitaires
     *
     * @return l'ordre de priorité.
     */
    int priority() default 0;

    /**
     * définit le nombre de points attribués à la réussite de ce test ou une
     * valeur relative si le tag {@link caseine.tags.RelativeEvaluation} est
     * employé.<br>
     * <p>
     * Un grade négatif ou nul est ignoré.
     * </p>
     *
     * @return le grade.
     *
     * @see caseine.tags.RelativeEvaluation
     */
    double grade() default Double.MIN_VALUE;

    /**
     * définit le nombre minimum d'instructions de l'élément annoté.
     *
     * @return le nombre minimum d'instructions de l'élément annoté.
     */
    int minStmt() default 1;
    
    /**
     * définit le nombre maximum d'instructions de l'élément annoté.
     *
     * @return le nombre maximum d'instructions de l'élément annoté.
     */
    int maxStmt() default 1;

    /**
     * définit un tableau de chaînes de caractères qui représentent des tests
     * unitaires.
     * <p>
     * Chaque chaîne de ce tableau peut être
     * </p>
     * <ul>
     * <li>Soit convertible en entier, dans ce cas, elle représente tous des
     * tests unitaires de cette priorité.</li>
     * <li>Soit le nom pleinement qualifié d'une méthode de test unitaire.</li>
     * </ul>
     * <p>
     * Pour que le test unitaire annoté réussisse, il faut que tous les tests
     * unitaires représentés dans ce tableau réussissent.
     * </p>
     * <p>
     * <strong>Attention aux dépendances cycliques</strong>
     * </p>
     *
     *
     * @return le tableau des tests unitaires
     */
    String[] requiersUnitTestsBefore() default {};
}
