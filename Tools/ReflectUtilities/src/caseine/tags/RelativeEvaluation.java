/*
 * Copyright 2019: Yvan Maillot
 * This software is part of the Caseine project.
 * This software was developped with the support of the following organizations:
 * Université de Haute-Alsace
 * Institut Polytechnique de Grenoble
 * 
 * 
 * This file is part of the VPL Design Tool.
 * The VPL Design Tool is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * The VPL Design Tool is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with the VPL Design Tool.  If not, see <https://www.gnu.org/licenses/>.
 */
package caseine.tags;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Pour ne plus s'embêter à calculer les points des questions pour que le total
 * soit 20/20 (ou autre).
 *
 * @author Yvan Maillot &lt;yvan.maillot@uha.fr&gt;
 *
 * <h2>L'annotation &#64;caseine.tags.RelativeEvaluation</h2>
 *
 *
 * <p>
 * L'annotation &#64;RelativeEvaluation soulage l'enseignant de l'ajustement des
 * points attribués à chaque test.
 * </p>
 ** <h3>Cible</h3>
 * <p>
 * &#64;RelativeEvaluation ne s'applique qu'aux types comme les classes
 * publiques. Elle ne devrait d'ailleurs s'appliquer qu'une seule fois dans le
 * projet. Si elle figure à plusieurs endroits, c'est la dernière rencontrée par
 * le parseur qui fait foi.
 *
 * </p>
 *
 *
 * <h3>Formes d'emploi</h3>
 * <p>
 * &#64;RelativeEvaluation s'emploie
 * </p>
 * <ol>
 * <li>comme une annotation monovaluée : e.g. &#64;RelativeEvaluation(100.0)<br>
 * La valeur 100.0 représente la note maximale. Attention, cela n'a pas d'effet
 * sur la propriété "Note maximale" de Moodle.
 * </li>
 * <li>comme une annotation de marquage : &#64;RelativeEvaluation<br>
 * Dans ce cas, la note maximale est 20</li>
 * </ol>
 *
 * <h3>Effet</h3>
 * <p>
 * L'annotation &#64;RelativeEvaluation s'emploie conjointement avec les
 * annotations &#64;Grade. Tous les &#64;Grade seront recalculés de sorte à ce
 * que leur cumul atteigne la note maximale et que leur nouvelle valeur soit
 * proportionnelle à l'ancienne.
 * </p>
 *
 * <p>
 * Remarque : toute annotation &#64;Grade(v) est ignorée si v est négatif ou
 * nul.
 * </p>
 *
 *
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface RelativeEvaluation {

    double value() default 20.0;
}
