/*
 * Copyright 2019: Yvan Maillot
 * This software is part of the Caseine project.
 * This software was developped with the support of the following organizations:
 * Université de Haute-Alsace
 * Institut Polytechnique de Grenoble
 * 
 * 
 * This file is part of the VPL Design Tool.
 * The VPL Design Tool is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * The VPL Design Tool is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with the VPL Design Tool.  If not, see <https://www.gnu.org/licenses/>.
 */
package caseine.tags;

import caseine.tags.hidden.CodeQualityToCheckRepetable;
import java.lang.annotation.ElementType;
import java.lang.annotation.Repeatable;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Pour faciliter la vérification de la qualité de code de l'élément annoté.
 *
 *
 * @author Yvan Maillot &lt;yvan.maillot@uha.fr&gt;
 *
 *
 * <h2>L'annotation &#64;caseine.tags.CodeQualityToCheck</h2>
 * <p>
 * Le principe consiste à soumettre l'arbre syntaxe (AST) de la version
 * étudiante de l'élément annotée au contrôle de l'enseignant par
 * l'intermédiaire d'une méthode qu'il doit écrire.
 * </p>
 *
 * <p>
 * L'AST et son contrôle est réalisé grâce à
 * <a href="https://javaparser.org/">JavaParser</a>
 * </p>
 *
 * <h3>Cible</h3>
 * <p>
 * &#64;CodeQualityToCheck s'applique à
 * </p>
 * <ul>
 * <li>un type
 * <ul>
 * <li>une classe</li>
 * <li>une interface</li>
 * <li>un type d'annotation</li>
 * <li>un type énuméré</li>
 * </ul></li>
 * <li>une méthode</li>
 * <li>un constructeur</li>
 * </ul>
 *
 *
 *
 * <h3>Formes d'emploi</h3>
 * <p>
 * &#64;CodeQualityToCheck s'emploie uniquement commentune annotation
 * multivaluée : (e.g.  <pre>
 * &#64;CodeQualityToCheck(
 *              grade = 1,
 *              priority = 2,
 *              codeAnalyserMethodsName = "edu.uha.miage.AnUnitTestClass.aMethod"
 * )
 * </pre>
 *
 * <h3>Utilisation de la propriété {@literal  codeAnalyserMethodsName} </h3>
 *
 * <p>
 * Cette propriété est une chaîne de caractères ({@code String}) qui représente
 * </p>
 * <ul>
 * <li>le nom pleinement qualifié d'une méthode</li>
 * <li>qui retourne une chaîne de caractères</li>
 * <li>et reçoit en paramètre un objet de type
 * <ul>
 * <li>{@code com.github.javaparser.ast.body.MethodDeclaration} si l'élément
 * annoté est une méthode.</li>
 * <li>{@code com.github.javaparser.ast.body.ConstructorDeclaration} si
 * l'élément annoté est une méthode.</li>
 * <li>{@code com.github.javaparser.ast.body.TypeDeclaration} si l'élément
 * annoté est une méthode.</li>
 * </ul>
 * </li>
 * </ul>
 * <p>
 * Chacun de ces paramètres contient l'AST (résultat du parsing) de l'élément
 * annoté.
 * </p>
 * <p>
 * <strong>Par exemple,</strong>
 * </p>
 * <p>
 * <strong>{@code codeAnalyserMethodsName = "edu.uha.miage.AClassTest.aMethodAnalyser"}</strong>
 * </p>
 * <p>
 * représente l'une des méthodes de la classe {@code AClassTest} suivantes:
 * </p>
 * <ul>
 * <li><strong>{@code public static String aMethodAnalyser(com.github.javaparser.ast.body.MethodDeclaration)}</strong></li>
 * <li><strong>{@code public static String aMethodAnalyser(com.github.javaparser.ast.body.ConstructorDeclaration)}</strong></li>
 * <li><strong>{@code public static String aMethodAnalyser(com.github.javaparser.ast.body.TypeDeclaration)}</strong></li>
 * </ul>
 * <p>
 * selon que l'élément annoté soit une méthode, un constructeur ou une classe.
 * </p>
 *
* <p>
 * L'enseignant a tout le loisir d'utiliser l'API de JavaParser ou quelques utilitaires fournis dans {@link caseine.publication.ParserUtils} pour analyser le
 * code de l'étudiant.
 * </p>
 *
 *
 * <h3>Conventions d'écriture de la méthode de test</h3>
 * <p>
 * Il doit écrire {@code codeAnalyserMethodsName} de sorte qu'elle retourne
 * </p>
 * <ul>
 * <li>{@code "OK"} si l'analyse est satisfaisante, ou</li>
 * <li>autre chose dans le cas contraire.<br> Cette autre chose est un message
 * destiné à l'étudiant dont le contenu est laissé à l'appréciation de
 * l'enseignant.
 * </li>
 * </ul>
 *
 * <p>
 * Pour le reste voir la documentation des propriétés</p>
 * <p>
 *
 *
 * @see <a href="https://javaparser.org/">JavaParser</a>
 * @see caseine.publication.ParserUtils
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD, ElementType.CONSTRUCTOR, ElementType.TYPE})
@Repeatable(CodeQualityToCheckRepetable.class)
public @interface CodeQualityToCheck {

    /**
     * Définit la consigne destinée à l'étudiant quand le test échoue.<br>
     * Quand la valeur par défaut est choisie, un message standard est généré.
     *
     * @return la consigne destinée à l'étudiant.
     */
    String value() default "";

    /**
     * définit l'ordre de priorité croissante du test généré dans sa classe de
     * tests unitaires
     *
     * @return l'ordre de priorité.
     */
    int priority() default 0;

    /**
     * définit le nombre de points attribués à la réussite de ce test ou une
     * valeur relative si le tag {@link caseine.tags.RelativeEvaluation} est
     * employé.<br>
     * <p>
     * Un grade négatif ou nul est ignoré.
     * </p>
     *
     * @return le grade.
     *
     * @see caseine.tags.RelativeEvaluation
     */
    double grade() default Double.MIN_VALUE;

    /**
     * définit un tableau de chaînes de caractères qui représentent des tests
     * unitaires.
     * <p>
     * Chaque chaîne de ce tableau peut être
     * </p>
     * <ul>
     * <li>Soit convertible en entier, dans ce cas, elle représente tous des
     * tests unitaires de cette priorité.</li>
     * <li>Soit le nom pleinement qualifié d'une méthode de test unitaire.</li>
     * </ul>
     * <p>
     * Pour que le test unitaire annoté réussisse, il faut que tous les tests
     * unitaires représentés dans ce tableau réussissent.
     * </p>
     * <p>
     * <strong>Attention aux dépendances cycliques</strong>
     * </p>
     *
     * @return le tableau des tests unitaires
     */
    String[] requiersUnitTestsBefore() default {};

    /**
     * Un tableau des noms pleinement qualifiés de méthodes qui prennent en paramètre l'AST de l'élément annoté.
     * @return Un tableau des noms pleinement qualifiés de méthodes qui prennent en paramètre l'AST de l'élément annoté.
     */
    String[] codeAnalyserMethodsName() default {};
}
