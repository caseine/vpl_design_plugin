/*
 * Copyright 2019: Yvan Maillot
 * This software is part of the Caseine project.
 * This software was developped with the support of the following organizations:
 * Université de Haute-Alsace
 * Institut Polytechnique de Grenoble
 *
 *
 * This file is part of the VPL Design Tool.
 * The VPL Design Tool is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The VPL Design Tool is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the VPL Design Tool.  If not, see <https://www.gnu.org/licenses/>.
 */
package caseine.tags;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Pour vérifier l'existence dans le code de l'étudiant du fragment annoté et
 * également l'équivalence de certaines propriétés.
 *
 * @author Yvan Maillot &lt;yvan.maillot@uha.fr&gt;
 *
 * <h2>L'annotation &#64;caseine.tags.ToCheck</h2>
 * <p>
 * L'annotation &#64;ToCheck engendre une évaluation qui vérifie l'existence et
 * les propriétés du fragment annoté.
 * </p>
 * <h3>Cible</h3>
 * <p>
 * &#64;ToCheck s'applique à
 * </p>
 * <ul>
 * <li>un type (externe)
 * <ul>
 * <li>une classe</li>
 * <li>une interface</li>
 * <li>un type d'annotation</li>
 * <li>un type énuméré</li>
 * </ul></li>
 * <li>un attribut</li>
 * <li>une méthode</li>
 * <li>un constructeur</li>
 * </ul>
 *
 *
 * <h3>Formes d'emploi</h3>
 * <p>
 * &#64;ToCheck s'emploie
 * </p>
 * <ol>
 * <li>comme une annotation multivaluée : (e.g.  <pre>
 * &#64;ToCheck(checkImplements = CheckImplements.NONE,
 *              grade = 1,
 *              modifiers = {CheckModifier.isAbstract, CheckModifier.isPrivate},
 *              priority = 2,
 *              requiersUnitTestsBefore = "1",
 *              value = "consigne")
 * </pre>
 * <p>
 * Voir la documentation des propriétés</p>
 * </li>
 * <li>comme une annotation monovaluée : (e.g. &#64;ToCheck("consigne à
 * l'étudiant")) où "consigne à l'étudiant" est le message à afficher en cas
 * d'erreur et toutes autres propriétés ont leur valeur par défaut.
 * </li>
 * <li>comme une annotation de marquage (e.g. &#64;ToCheck)<br>Dans ce cas,
 * toutes les propriétés ont leur valeur par défaut</li>
 * </ol>
 *
 *
 * <h3>Utilisation de la propriété requiersUnitTestsBefore</h3>
 * <p>
 * Cette propriété est un tableau de chaînes de caractères
 * (<code>String[]</code>) dont chaque élément doit être:
 * </p>
 * <ul>
 * <li>soit une chaîne de caractères convertible en entier (e.g. "2")
 * <br>
 * Dans ce cas, cela signifie que toutes les évaluations automatiques de
 * priorité égale à cet entier converti (e.g. toutes les évaluations de priorité
 * 2) doivent réussir pour que celle-ci réussisse.</li>
 * <li>soit une chaîne de caractères qui représente le nom pleinement qualifié
 * d'un test unitaire accessible existant ou écrit par l'enseignant (e.g.
 * "edu.uha.miage.PointTest.checkOrigine").<br>
 * Dans ce cas, cela signifie que la méthode de test unitaire (e.g. public void
 * checkOrigine() de la classe public void edu.uha.miage.PointTest doit réussir
 * pour que cette évaluation réussisse.
 * </li>
 * </ul>
 *
 * <p>
 * <strong>Attention aux dépendances cycliques.</strong>
 * </p>
 *
 * @see caseine.tags.CheckModifier
 * @see caseine.tags.CheckImplements
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE, ElementType.CONSTRUCTOR, ElementType.FIELD, ElementType.METHOD})
public @interface ToCheck {

    /**
     * Définit la consigne destinée à l'étudiant quand le test échoue.<br>
     * Quand la valeur par défaut est choisie, un message standard est généré.
     *
     * @return la consigne destinée à l'étudiant.
     */
    String value() default "";

    /**
     * @return Les modificateurs que la version de l'étudiant doit avoir en
     * commun avec la solution de référence.
     * <br>C'est un sous-ensemble de<br>
     *
     * <ul>
     * <li>CheckModifier.isAbstract</li>
     * <li>CheckModifier.isFinal</li>
     * <li>CheckModifier.isInterface</li>
     * <li>CheckModifier.isNative</li>
     * <li>CheckModifier.isPrivate</li>
     * <li>CheckModifier.isProtected</li>
     * <li>CheckModifier.isPublic</li>
     * <li>CheckModifier.isStatic</li>
     * <li>CheckModifier.isStrict</li>
     * <li>CheckModifier.isSynchronized</li>
     * <li>CheckModifier.isTransient</li>
     * <li>CheckModifier.isVolatile</li>
     * </ul>
     * <p>
     * Attention: isPrivate ne signifie pas qu'on vérifie que le membre est
     * privé mais l'on vérifie que son état privé est le même que celui de la
     * référence.
     * </p>
     * <br>
     * @see caseine.tags.CheckModifier
     *
     */
    CheckModifier[] modifiers() default {
        /*CheckModifier.isAbstract,
        CheckModifier.isFinal,
        CheckModifier.isInterface,
        CheckModifier.isNative,*/
        CheckModifier.isPrivate,
        CheckModifier.isProtected,
        CheckModifier.isPublic,
        CheckModifier.isStatic/*,
        CheckModifier.isStrict,
        CheckModifier.isSynchronized,
        CheckModifier.isTransient,
        CheckModifier.isVolatile*/
    };

    /**
     * Utile seulement dans le cas où l'élément annoté est une classe
     *
     * @return une valeur parmi
     * <ul>
     * <li>CheckImplements.NONE</li>
     * <li>CheckImplements.AT_LEAST</li>
     * <li>CheckImplements.EXACT</li>
     * </ul><br>
     * <p> Si checkImplements() vaut : </p>
     *
     * <ul>
     * <li>NONE : aucune vérification n'est faite</li>
     * <li>AT_LEAST : la vérification est de s'assurer que les implémentations de la version de l'étudiant doivent au
     * moins contenir celles de la référence</li>
     * <li>EXACT : la vérification est de s'assurer que les implémentations de la version de l'étudiant exactement celles
     * de la référence.</li>
     * </ul>
     * <p>
     * Ignoré si l'élément annoté n'est pas une classe.
     * </p>
     * <br>
     * @see caseine.tags.CheckImplements
     */
    CheckImplements checkImplements() default CheckImplements.NONE;


    /**
     * Utile seulement dans le cas où l'élément annoté est une méthode ou un constructeur
     *
     * @return une valeur parmi
     * <ul>
     * <li>CheckExceptions.NONE</li>
     * <li>CheckExceptions.EXACT</li>
     * </ul><br>
     * <p> Si checkExceptions() vaut : </p>
     *
     * <ul>
     * <li>NONE : aucune vérification n'est faite</li>
     * <li>EXACT : la vérification est de s'assurer que les exceptions susceptibles d'être lancées par la version
     * de l'étudiant sont exactement celles de la référence</li>
     * </ul>
     * <p>
     * Ignoré si l'élément annoté n'est pas une méthode ou un constructeur.
     * </p>
     * <br>
     * @see caseine.tags.CheckExceptions
     */
    CheckExceptions checkExceptions() default CheckExceptions.NONE;
    /**
     * définit l'ordre de priorité croissante du test généré dans sa classe de
     * tests unitaires
     *
     * @return l'ordre de priorité.
     */
    int priority() default 0;

    /**
     * définit le nombre de points attribués à la réussite de ce test ou une
     * valeur relative si le tag {@link caseine.tags.RelativeEvaluation} est
     * employé.<br>
     * <p>
     * Un grade négatif ou nul est ignoré.
     * </p>
     *
     * @return le grade.
     * 
     * @see caseine.tags.RelativeEvaluation
     */
    double grade() default Double.MIN_VALUE;

    /**
     * définit un tableau de chaînes de caractères qui représentent des tests
     * unitaires.
     * <p>
     * Chaque chaîne de ce tableau peut être
     * </p>
     * <ul>
     * <li>Soit convertible en entier, dans ce cas, elle représente tous des
     * tests unitaires de cette priorité.</li>
     * <li>Soit le nom pleinement qualifié d'une méthode de test unitaire.</li>
     * </ul>
     * <p>
     * Pour que le test unitaire annoté réussisse, il faut que tous les tests
     * unitaires représentés dans ce tableau réussissent.
     * </p>
     * <p>
     * <strong>Attention aux dépendances cycliques</strong>
     * </p>
     *
     *
     * @return le tableau des tests unitaires
     */
    String[] requiersUnitTestsBefore() default {};
}
