package caseine.tags;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Pour vider ou remplacer le contenu d'un constructeur.
 * 
 * @author Yvan Maillot &lt;yvan.maillot@uha.fr&gt;
 * 
 * <h2>L'annotation &#64;caseine.tags.ToDoInConstructor</h2>
 * <p>
 * L'annotation &#64;ToDoInConstructor permet de retirer le corps des
 * constructeurs qu'elle annote ou de le remplacer par un autre code.
 * </p>
 ** <h3>Cible</h3>
 * <p>
 * &#64;ToDoInConstructor ne s'applique qu'aux constructeurs.
 * </p>
 *
 *
 * <h3>Formes d'emploi</h3>
 * <p>
 * &#64;ToDoInConstructor s'emploie
 * </p>
 * <ol>
 * <li>comme une annotation multivaluée : e.g. &#64;ToDoInConstructor(value =
 * "une consigne à l'étudiant", replacement = "{ this.x = 0.0; this.y = 0.0; }")
 * </li>
 * <li>comme une annotation monovaluée : e.g. &#64;ToDoInConstructor("une
 * consigne à l'étudiant")
 * </li>
 * <li>comme une annotation de marquage : &#64;ToDoInConstructor</li>
 * </ol>
 *
 *
 * <h3>Effet</h3>
 * L'annotation &#64;ToDoInConstructor a pour effet
 * <ol>
 * <li>dans tous les cas, de conserver la signature du constructeur,</li>
 * <li>et selon que la propriété <strong>replacement</strong> soit renseignée ou
 * non:
 * <ol>
 * <li>Si elle n'est pas renseignée (ou vide), de remplacer son contenu par un
 * simple commentaire qui commence par // TODO suivi de la consigne à
 * l'étudiant.
 * </li>
 * <li>Si elle est renseignée (et non vide), de remplacer le corps du
 * constructeur par du code défini par la valeur de cette propriété (cf.
 * <i>Utilisation de la propriété <strong>replacement</strong> ci-après</i>).
 * </li>
 * </ol>
 * </li>
 * </ol>
 *
 **
 * <h3>Utilisation de la propriété <strong>replacement</strong></h3>
 * <p>
 * Sans renseigner la propriété <strong>replacement</strong> (c'est-à-dire par
 * défaut), l'emploi de &#64;ToDoInConstructor est assez naturelle.
 * </p>
 * <p>
 * En revanche, l'utilisation de la propriété <strong>replacement</strong>
 * nécessite de prendre quelques précautions :
 * </p>
 * <p>
 * Le corps du constructeur est
 * </p>
 * <ol>
 * <li>soit remplacé par le contenu d'un fichier dont le chemin est donné par la
 * valeur de la propriété <strong>replacement</strong>, s'il s'agit du chemin
 * d'un ficher accessible.</li>
 * <li>soit remplacé par cette valeur elle-même sinon.
 * </li>
 * </ol>
 * <p>
 * <strong>Attention</strong>, pour pouvoir déployer sur le serveur, il faut
 * absolument que le code de remplacement soit <strong>parsable</strong>.
 * </p>
 * <ul>
 * <li>Il doit notamment commencer par { et finir par }</li>
 * <li>S'il contient des guillemets, ils devront être invalidés dans la valeur
 * de la propriété (puisqu'elle est elle-même entre guillemets).<br>
 * Par exemple, pour produire { this.name = ""; }, il faut écrire replacement="{
 * this.name = \"\"; }"</li>
 * <li>Ça peut devenir compliqué s'il faut ajouter des sauts de lignes ('\n'),
 * des tabulations ('\t'), des caractères unicode, etc.<br>
 * Dans ces cas, et le plus souvent, l'emploi d'un fichier externe est
 * préférable.</li>
 * </ul>
 *
 *
 * <h3>Exemple d'annotation de marquage</h3>
 *
 *
 * <pre>
 * &#64;ToDoInConstructor
 * public Point(double x, double) {
 *    this.x = y;
 *    this.y = y;
 * }
 * </pre>
 *
 * <p>
 * sera remplacé par
 * </p>
 *
 *
 * <pre>
 * public Point(double x, double) {
 * // TODO
 * }
 * </pre>
 *
 *
 * <h3>Exemple d'annotation monovaluée</h3>
 * <pre>
 * &#64;ToDoInConstructor("Écrire ce constructeur")
 * public Point(double x, double) {
 *    this.x = y;
 *    this.y = y;
 * }
 * </pre>
 *
 * <p>
 * sera remplacé par
 * </p>
 *
 * <pre>
 * public Point(double x, double) {
 * // TODO Écrire ce constructeur
 * }
 * </pre>
 *
 *
 * <h3>Exemple d'annotation multivaluée (sans fichier)</h3>
 * <pre>
 * &#64;ToDoInConstructor(value="Ce constructeur est mal écrit. Corrigez-le !", replacement = "{ this.x = 0.0; this.y = 0.0; }")
 * public Point() {
 *    this(0.0, 0.0);
 * }
 * </pre>
 *
 * <p>
 * sera remplacé par
 * </p>
 *
 * <pre>
 * public Point() {
 *    this.x = 0.0;
 *    this.y = 0.0;
 * // TODO Ce constructeur est mal écrit. Corrigez-le !
 * }
 * </pre>
 *
 *  *
 * <h3>Exemple d'annotation multivaluée (avec fichier)</h3>
 * <pre>
 * &#64;ToDoInConstructor(replacement = "aConstructorReplacementFile.txt")
 * public Point(double x, double y) {
 *    this.x = x;
 *    this.y = y;
 * }
 * </pre>
 *
 * si le fichier "aConstructorReplacementFile.txt" contient
 * <pre>
 * {
 *    x = x;
 *    y = y;
 *    // Corrigez ce code !
 * }
 * </pre>
 * 
 * <p>
 * <strong>Attention</strong> les accolades sont nécessaires.
 * </p>
 *
 * <p>
 * sera remplacé par
 * </p>
 *
 * <pre>
 * public Point(double x, double y) {
 *    x = x;
 *    y = y;
 * // Corrigez ce code !
 * }
 * </pre>
 *
 *
 *
 * <p>
 * <strong>Remarque</strong> : les commentaires de la javadoc sont toujours
 * conservés.
 * </p>
 *
 * <h2>Précautions</h2>
 * <p>
 * &#64;ToDoInConstructor peut livrer à l'étudiant un code non compilable. Par
 * exemple si le constructeur annoté affecte des attributs déclarés final.
 * </p>
 *
 * <p>
 * Il faut donc l'utiliser en connaissance de cause.
 * </p>
 *
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.CONSTRUCTOR)
public @interface ToDoInConstructor {

    /**
     * Définit la consigne à destination de l'étudiant. Elle sera dans le corps
     * du constructeur et précédée de // TODO
     *
     * Par défaut, la consigne est vide.
     *
     * @return La consigne à destination de l'étudiant.
     */
    String value() default "";

    /**
     * Définit le remplacement du corps du constructeur annotée.
     *
     * @return une chaîne de caractères qui est <ol>
     * <li>soit la chaîne vide (par défaut)</li>
     * <li>soit le chemin valide d'un fichier qui contient le code à
     * remplacer</li>
     * <li>soit le code lui-même</li>
     * </ol>
     * <p>
     * <strong>Attention</strong> ce code doit être compilable.</p>
     */
    String replacement() default "";
}
