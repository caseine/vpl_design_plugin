package caseine.tags;

public enum CheckImplements {
    NONE, EXACT, AT_LEAST
}
