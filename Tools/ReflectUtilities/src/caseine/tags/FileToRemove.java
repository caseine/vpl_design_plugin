/*
 * Copyright 2019: Yvan Maillot
 * This software is part of the Caseine project.
 * This software was developped with the support of the following organizations:
 * Université de Haute-Alsace
 * Institut Polytechnique de Grenoble
 * 
 * 
 * This file is part of the VPL Design Tool.
 * The VPL Design Tool is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * The VPL Design Tool is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with the VPL Design Tool.  If not, see <https://www.gnu.org/licenses/>.
 */
package caseine.tags;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Pour enlever le fichier de la classe annotée.
 * 
 * @author Yvan Maillot &lt;yvan.maillot@uha.fr&gt;
 *
 * <h2>L'annotation &#64;caseine.tags.FileToRemove</h2>
 *
 *
 * <p>
 * L'annotation &#64;FileToRemove enlève à l'étudiant le fichier dans lequel la
 * classe annotée se trouve.
 * </p>
 * <h3>Cible</h3>
 * <p>
 * &#64;FileToRemove ne s'applique qu'aux types publiques.
 * </p>
 *
 * <h3>Formes d'emploi</h3>
 * <p>
 * &#64;FileToRemove ne s'emploie que comme une annotation de marquage : e.g.
 * <code>&#64;FileToRemove</code>
 * </p>
 *
 * <h3>Précautions</h3>
 *
 * &#64;FileToRemove peut livrer à l'étudiant un code non compilable.
 *
 * <p>
 * Il faut donc l'utiliser en connaissance de cause et
 * </p>
 * <ol>
 * <li>soit, enlever des fichiers qui n'engendrent pas de code non
 * compilable,</li>
 * <li>soit, utiliser l'introspection dans les tests unitaires,</li>
 * <li>soit, prévenir l'étudiant.</li>
 * </ol>
 *<p>
 * <strong>Attention</strong> à utiliser des &laquo; <code>imports *</code>
 * &raquo; pour importer dans vos fichiers restants les classes qui
 * disparaissent.
 * </p>
 *
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface FileToRemove {
}
