/*
 * Copyright 2019: Yvan Maillot
 * This software is part of the Caseine project.
 * This software was developped with the support of the following organizations:
 * Université de Haute-Alsace
 * Institut Polytechnique de Grenoble
 * 
 * 
 * This file is part of the VPL Design Tool.
 * The VPL Design Tool is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * The VPL Design Tool is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with the VPL Design Tool.  If not, see <https://www.gnu.org/licenses/>.
 */
package caseine.tags;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Pour maîtriser l'ordre des évaluations.
 * 
 * @author Yvan Maillot &lt;yvan.maillot@uha.fr&gt;
 *
 * <h2>L'annotation &#64;caseine.tags.ClassTestPriority</h2>
 * <p>
 * L'annotation &#64;ClassTestPriority permet de définir un ordre (croissant) de
 * priorité d'une classe de tests.
 * </p>
 ** <h3>Cible</h3>
 * <p>
 * &#64;ClassTestPriority ne s'applique qu'aux classes publiques.
 * </p>
 * <p>
 * <strong>Remarque : </strong>Elle peut s'appliquer aussi bien à des classes de
 * tests unitaires qu'à celles de l'application.
 * </p>
 * <ul>
 * <li>S'il s'agit d'une classe unitaire, la priorité s'applique à la classe
 * elle-même.</li>
 * <li>Sinon elle s'applique à la classe de tests unitaires engendrée par la
 * classe annotée.</li>
 * </ul>
 *
 * <h3>Formes d'emploi</h3>
 * <p>
 * &#64;ClassTestPriority ne s'emploie que comme une annotation monovaluée :
 * e.g. <code>&#64;ClassTestPriority(2)</code>, où <code>2</code> est la
 * priorité.
 * </p>
 *
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface ClassTestPriority {

    /**
     *
     * @return la priorité de la classe unitaire annotée ou de la classe
     * unitaire engendrée par la classe annotée.
     */
    int value();
}
