package caseine.checker.test;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Main {
    public static void main(String[] args) {
        Pattern p = Pattern.compile("(.+)\\.(.+)");
        Scanner in = new Scanner(System.in);
        System.out.print("> ");
        String s = in.nextLine();
        Matcher m = p.matcher(s);
        if(m.matches()) {
            System.out.println(m.group(1));
            System.out.println(m.group(2));
        } else {
            System.out.println("Not match");
        }
    }
}
