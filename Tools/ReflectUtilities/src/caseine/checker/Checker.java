/*
 * Copyright 2019: Yvan Maillot
 * This software is part of the Caseine project.
 * This software was developed with the support of the following organizations:
 * Université de Haute-Alsace
 * Institut Polytechnique de Grenoble
 *
 *
 * This file is part of the VPL Design Tool.
 * The VPL Design Tool is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The VPL Design Tool is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the VPL Design Tool.  If not, see <https://www.gnu.org/licenses/>.
 */
package caseine.checker;

import caseine.publication.producers.*;
import caseine.reflect.ReflectUtilities;
import caseine.tags.*;
import caseine.tags.hidden.CodeQualityToCheckRepetable;
import com.github.javaparser.StaticJavaParser;
import com.github.javaparser.ast.body.BodyDeclaration;

import java.lang.reflect.Constructor;
import java.lang.reflect.Executable;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.*;

import static caseine.reflect.ReflectUtilities.paramsToString;

/**
 * Pour générer tous les tests unitaires d'une classe annotée avec des
 * annotations de {@code caseine.tags.*.}
 *
 * @author Yvan Maillot &lt;yvan.maillot@uha.fr&gt;
 * @see caseine.tags
 * @see caseine.tags
 */
public class Checker implements Iterable<BodyDeclaration<?>> {

    /**
     * SB contient un texte qui est le contenu du fichier de test
     */
    private final StringBuilder SB;
    /**
     * MSBall associe un nom de méthode de test à son contenu. Toutes les
     * méthodes de test s'y trouvent. Il est même possible d'enrichir MSBall
     * d'un checker à l'autre
     */
    private final TreeMap<String, StringBuilder> MSBall;
    private final TreeMap<String, BodyDeclaration<?>> BDall;
    // public final Map<String, StringBuilder> MSB;
    private final Formatter F;
    // La classe testée.
    private final Class<?> C;

    private Checker(Class<?> c, TreeMap<String, StringBuilder> stringStringBuilderTreeMap, TreeMap<String, BodyDeclaration<?>> stringBodyDeclarationTreeMap) {
        MSBall = stringStringBuilderTreeMap;
        BDall = stringBodyDeclarationTreeMap;
        SB = new StringBuilder();
        F = new Formatter(SB, Locale.US);
        C = c;
        check();
    }

    /**
     * Génération des tests unitaires d'une classe annotée.
     *
     * @param c l'objet de classe de la classe dont on veut générer les tests.
     * @see caseine.tags
     */
    public Checker(Class<?> c) {
        this(c, new TreeMap<>(), new TreeMap<>());
    }

    /**
     * Génération des tests unitaires d'une classe annotée et ajoutés à la suite
     * d'un <i>checker</i> existant.
     * <p>
     * Cela permet d'avoir dans le même <i>checker</i> des tests unitaires issus
     * de la génération de différentes classes.
     *
     * @param c   l'objet de classe de la classe dont on veut générer les tests.
     * @param chk l'autre <i>checker</i> pour ajouter ses tests.
     */
    public Checker(Class<?> c, Checker chk) {
        this(c, chk.MSBall, chk.BDall);
    }

    private void check() {
        ToCheck annotation = C.getAnnotation(ToCheck.class);
        if (annotation != null) {
            checkClass(annotation);
        }
        // --- Attributs 
        checkFields();
        // --- Constructeurs 
        checkConstructors();
        // --- Méthodes 
        checkMethods();
        // --- Setters 
        checkSetters();
        // --- Getters 
        checkGetters();
        // --- Méthodes et constructeurs
        testMethods();
        checkClassQuality();
        checkCountOfStatements();
        checkOnlyOneStatement();
        checkMethodQuality();
        checkConstructorQuality();
        // --- Types (internes)
        checkInnerClasses();
    }

    private void put(String testMethodName, StringBuilder sb) {
        MSBall.put(testMethodName, sb);
        BDall.put(testMethodName, StaticJavaParser.parseBodyDeclaration(sb.toString()));
    }

    private void unitTestHeader(Formatter formatter, ToCheck toCheck, String testMethodName) {
        f("\n@Test\n", formatter);
        if (toCheck.grade() != Double.MIN_VALUE) {
            f("@caseine.format.javajunit.Grade(%f)\n", formatter, toCheck.grade());
        }
        f("public void %s {\n", formatter, testMethodName);
    }

    /**
     * Write the unary test of the class C because it was tagged with @ToCheck in order to check if this class C
     * (of the student) has the same name, the same modifiers and the same super class as the corresponding class of
     * the correction.
     *
     * @param toCheck The tag of class C
     */
    private void checkClass(ToCheck toCheck) {
        String testMethodName = String.format("p%06d000_checkClass%s()", toCheck.priority(), C.getSimpleName());

        // sb contiendra à la fin de la méthode le code complet du test unitaire
        StringBuilder sb = new StringBuilder();
        Formatter formatter = new Formatter(sb, Locale.US);

        unitTestHeader(formatter, toCheck, testMethodName);

        f("   System.out.println(\"check class %s\");\n", formatter, C.getSimpleName());

        // Écrit (si demandé) des appels à des méthodes (probablement des tests unitaires) en préambule à ce
        // test unitaire.
        // Cela peut servir par exemple à empêcher ce test tant que d'autres tests échouent.
        if (toCheck.requiersUnitTestsBefore().length > 0) {
            f("%s", formatter, ReflectUtilities.writePreviousUnitTestCalling(MSBall, toCheck.requiersUnitTestsBefore()));
        }

        f("   try {\n", formatter);
        try {
            f("   		assertTrue(\"%s\", ReflectUtilities.parseType(\"%s\").getSuperclass().equals(ReflectUtilities.parseType(\"%s\")));\n", formatter,
                    toCheck.value() + " (Inheritance)",
                    C.getName(),
                    C.getSuperclass().getName());
        } catch (NullPointerException ex) {
            // Means there is no inheritance.
        }
        f("   		Class<?> x = ReflectUtilities.parseType(\"%s\");\n", formatter, C.getName());
        checkModifiers(toCheck.value(), C.getModifiers(), toCheck.modifiers(), formatter);
        checkImplementations(toCheck, formatter);
        String msg = "Class " + C.getSimpleName() + " not found";
        if (toCheck.value() != null && !toCheck.value().isBlank()) {
            msg = toCheck.value();
        }
        f("   } catch (ClassNotFoundException ex) {\n", formatter);
        f("        fail(\"%s\");\n", formatter, msg);
        f("   }\n", formatter);
        f("}\n", formatter);

        put(testMethodName, sb);
        //MSB.put(testMethodName, sb);
    }

    private void checkImplementations(ToCheck toCheck, Formatter formatter) {
        if (toCheck.checkImplements() != CheckImplements.NONE) {

            java.util.Set<Class<?>> implementations = Arrays.stream(C.getInterfaces())
                    .collect(java.util.stream.Collectors.toSet());

            f("   java.util.Set<Class<?>> refImplements =  new java.util.HashSet<>();\n", formatter);
            for (Class<?> implement : implementations) {
                f("   refImplements.add(ReflectUtilities.parseType(\"%s\"));\n", formatter, implement.getName());
            }
            f("   java.util.Set<Class<?>> stdImplements = java.util.Arrays.stream(x.getInterfaces()).collect(java.util.stream.Collectors.toSet());\n", formatter);
            if (toCheck.checkImplements() == CheckImplements.EXACT) {
                f("   assertEquals(\"%s\", refImplements, stdImplements);\n", formatter, toCheck.value() + " (Implements)");
            } else {
                f("   assertTrue(\"%s\", stdImplements.containsAll(refImplements));\n", formatter, toCheck.value() + " (Implements)");
            }
        }
    }

    private StringBuilder paramsOfAExecutable(Executable executable) {
        return Arrays.stream(executable.getParameterTypes())
                .map(x -> x.getSimpleName().replaceAll("\\[\\]", "Array"))
                .collect(
                        StringBuilder::new,
                        StringBuilder::append,
                        StringBuilder::append);
    }

    private void writeTheParametersListOf(Executable executable, Formatter formatter) {
        if (executable.getParameterCount() > 0) {
            Class<?>[] parameterTypes = executable.getParameterTypes();
            f("           ReflectUtilities.parseType(\"%s\")", formatter, parameterTypes[0].getName());
            for (int i = 1; i < parameterTypes.length; ++i) {
                f(",\n           ReflectUtilities.parseType(\"%s\")", formatter, parameterTypes[i].getName());
            }
        }
    }

    private void checkConstructor(Constructor<?> constructor, ToCheck toCheck) {
        String msg = toCheck.value().isEmpty() ? "Constructor of " + constructor.getName() : toCheck.value();
        StringBuilder params = paramsOfAExecutable(constructor);

        String testMethodName = String.format("p%06d000_checkConstructor%s%s()", toCheck.priority(), C.getSimpleName(), params);

        // sb contiendra à la fin de la méthode le code complet du test unitaire
        StringBuilder sb = new StringBuilder();
        Formatter formatter = new Formatter(sb, Locale.US);

        unitTestHeader(formatter, toCheck, testMethodName);
        f("   System.out.println(\"Declaration of %s\");\n", formatter, msg);
        f("   try {\n", formatter);
        f("        Constructor x =  ReflectUtilities.parseType(\"%s\").getDeclaredConstructor(\n", formatter, C.getName());

        writeTheParametersListOf(constructor, formatter);

        f("\n        );\n", formatter);
        //f("        assertTrue(\"\", c.getModifiers() == %d);\n", c.getModifiers());
        checkModifiers(toCheck.value(), constructor.getModifiers(), toCheck.modifiers(), formatter);
        checkExceptions(toCheck, constructor, formatter);

        f("   } catch (Exception ex) {\n", formatter);
        f("        fail(\"%s\");\n", formatter, msg);
        f("   }\n", formatter);
        f("}\n", formatter);

        /* TRAVAUX EN COURS.
        ToCompare toCompare = c.getAnnotation(ToCompare.class);
        if (toCompare != null) {
            compareConstructor(c, toCheck, toCompare);
        }*/
        put(testMethodName, sb);
        //MSB.put(testMethodName, sb);
    }


    private void checkConstructors() {
        SB.append('\n');
        Arrays.stream(C.getDeclaredConstructors())
                .filter(c -> c.getAnnotation(ToCheck.class) != null)
                .forEach((c) -> checkConstructor(c, c.getAnnotation(ToCheck.class)));
    }

    /**
     * TRAVAUX EN COURS. Pour l'instant ne compare rien, Fait la même chose que
     * check
     *
     * @param constructor the constructor to compare with the ref constructor
     * @param toCheck     the toCheck annotation
     * @param toCompare   the toCompare annotation
     */
    private void compareConstructor(Constructor<?> constructor, ToCheck toCheck, ToCompare toCompare) {
        String msg = toCheck.value().isEmpty() ? "Constructor of " + constructor.getName() : toCheck.value();

        StringBuilder params = paramsOfAExecutable(constructor);

        String testMethodName = String.format("p%06d000_testConstructor%s%s()", toCompare.priority(), C.getSimpleName(), params);

        // sb contiendra à la fin de la méthode le code complet du test unitaire
        StringBuilder sb = new StringBuilder();
        Formatter formatter = new Formatter(sb, Locale.US);

        f("\n@Test\n", formatter);
        f("public void %s {\n", formatter, testMethodName);

        f("   System.out.println(\"Test %s\");\n", formatter, msg);
        f("   try {\n", formatter);
        f("        Constructor x = ReflectUtilities.parseType(\"%s\").getDeclaredConstructor(\n", formatter, C.getName());

        writeTheParametersListOf(constructor, formatter);

        f("\n        );\n", formatter);
        //f("        assertTrue(\"\", c.getModifiers() == %d);\n", c.getModifiers());
        checkModifiers(toCheck.value(), constructor.getModifiers(), toCheck.modifiers(), formatter);

        f("   } catch (NoSuchMethodException | SecurityException | ClassNotFoundException ex) {\n", formatter);
        f("        fail(\"%s\");\n", formatter, msg);
        f("   }\n", formatter);
        f("}\n", formatter);

        put(testMethodName, sb);
        //MSB.put(testMethodName, sb);
    }

    private void checkOnlyOneStatement() {
        SB.append('\n');
        Arrays.stream(C.getDeclaredMethods())
                .filter(method -> method.getAnnotation(OnlyOneStatementToCheck.class) != null)
                .forEach(method -> {
                    OnlyOneStatementToCheck onlyOneStatementToCheck = method.getAnnotation(OnlyOneStatementToCheck.class);
                    checkOnlyOneStatement(method, onlyOneStatementToCheck);
                });
        Arrays.stream(C.getDeclaredConstructors())
                .filter(constructor -> constructor.getAnnotation(OnlyOneStatementToCheck.class) != null)
                .forEach(constructor -> {
                    OnlyOneStatementToCheck onlyOneStatementToCheck = constructor.getAnnotation(OnlyOneStatementToCheck.class);
                    checkOnlyOneStatement(constructor, onlyOneStatementToCheck);
                });
    }

    private void checkOnlyOneStatement(Method m, OnlyOneStatementToCheck onlyOneStatementToCheck) {
        OnlyOneStatementToCheckProducer tcp = new OnlyOneStatementToCheckProducer(C, MSBall, m, onlyOneStatementToCheck);
        put(tcp.getMethodName(), tcp.getStringBuilder());
        SB.append(tcp.getStringBuilder());
    }

    private void checkOnlyOneStatement(Constructor<?> m, OnlyOneStatementToCheck onlyOneStatementToCheck) {
        OnlyOneStatementToCheckProducer tcp = new OnlyOneStatementToCheckProducer(C, MSBall, m, onlyOneStatementToCheck);
        put(tcp.getMethodName(), tcp.getStringBuilder());
        SB.append(tcp.getStringBuilder());
    }

    private void checkCountOfStatements() {
        SB.append('\n');
        Arrays.stream(C.getDeclaredMethods())
                .filter(m -> m.getAnnotation(StatementCountToCheck.class) != null)
                .forEach(m -> {
                    StatementCountToCheck stmtCountToCheck = m.getAnnotation(StatementCountToCheck.class);
                    checkCountOfStatements(m, stmtCountToCheck);
                });
        Arrays.stream(C.getDeclaredConstructors())
                .filter(m -> m.getAnnotation(StatementCountToCheck.class) != null)
                .forEach(m -> {
                    StatementCountToCheck stmtCountToCheck = m.getAnnotation(StatementCountToCheck.class);
                    checkCountOfStatements(m, stmtCountToCheck);
                });
    }

    private void checkCountOfStatements(Method m, StatementCountToCheck stmtCountToCheck) {
        StatementCountToCheckProducer tcp = new StatementCountToCheckProducer(C, MSBall, m, stmtCountToCheck);
        put(tcp.getMethodName(), tcp.getStringBuilder());
        SB.append(tcp.getStringBuilder());
    }

    private void checkCountOfStatements(Constructor<?> m, StatementCountToCheck stmtCountToCheck) {
        StatementCountToCheckProducer tcp = new StatementCountToCheckProducer(C, MSBall, m, stmtCountToCheck);
        put(tcp.getMethodName(), tcp.getStringBuilder());
        SB.append(tcp.getStringBuilder());
    }

    private void checkClassQuality() {
        SB.append('\n');
        CodeQualityToCheck codeQualityToCheck = C.getAnnotation(CodeQualityToCheck.class);
        if (codeQualityToCheck != null) {
            checkCodeQuality(C, codeQualityToCheck, 0);
        }
        CodeQualityToCheckRepetable codeQualityToCheckRepetable = C.getAnnotation(CodeQualityToCheckRepetable.class);
        if (codeQualityToCheckRepetable != null) {
            int i = 0;
            for (CodeQualityToCheck c : codeQualityToCheckRepetable.value()) {
                checkCodeQuality(C, c, ++i);
            }
        }
    }

    private void launchAllCheckCodeQualityOf(Executable constructor) {
        CodeQualityToCheck codeQualityToCheck = constructor.getAnnotation(CodeQualityToCheck.class);
        if (codeQualityToCheck != null) {
            checkCodeQuality(constructor, codeQualityToCheck, 0);
        }
        CodeQualityToCheckRepetable codeQualityToCheckRepetable = constructor.getAnnotation(CodeQualityToCheckRepetable.class);
        if (codeQualityToCheckRepetable != null) {
            int i = 0;
            for (CodeQualityToCheck otherCodeQualityToCheck : codeQualityToCheckRepetable.value()) {
                checkCodeQuality(constructor, otherCodeQualityToCheck, ++i);
            }
        }
    }

    private void checkConstructorQuality() {
        SB.append('\n');
        for (Constructor<?> constructor : C.getDeclaredConstructors()) {
            launchAllCheckCodeQualityOf(constructor);
        }
    }

    private void checkMethodQuality() {
        SB.append('\n');
        for (Method method : C.getDeclaredMethods()) {
            launchAllCheckCodeQualityOf(method);
        }
    }

    private void checkCodeQuality(Executable executable, CodeQualityToCheck codeQualityToCheck, int n) {
        if (executable.getClass() == Constructor.class) {
            Constructor<?> constructor = (Constructor) executable;
            checkCodeQuality(constructor, codeQualityToCheck, n);
        } else {
            Method method = (Method) executable;
            checkCodeQuality(method, codeQualityToCheck, n);
        }
    }

    private void checkCodeQuality(Constructor<?> c, CodeQualityToCheck codeQualityToCheck, int n) {
        CodeQualityToCheckProducerForConstructors codeQualityToCheckProducerForConstructors = new CodeQualityToCheckProducerForConstructors(C, MSBall, c, codeQualityToCheck, n);
        put(codeQualityToCheckProducerForConstructors.getMethodName(), codeQualityToCheckProducerForConstructors.getStringBuilder());
        SB.append(codeQualityToCheckProducerForConstructors.getStringBuilder());
    }

    private void checkCodeQuality(Method m, CodeQualityToCheck codeQualityToCheck, int n) {
        CodeQualityToCheckProducer codeQualityToCheckProducer = new CodeQualityToCheckProducer(C, MSBall, m, codeQualityToCheck, n);
        put(codeQualityToCheckProducer.getMethodName(), codeQualityToCheckProducer.getStringBuilder());
        SB.append(codeQualityToCheckProducer.getStringBuilder());
    }

    private void checkCodeQuality(Class<?> C, CodeQualityToCheck codeQualityToCheck, int n) {
        CodeQualityToCheckProducerForClass codeQualityToCheckProducerForClass = new CodeQualityToCheckProducerForClass(C, MSBall, codeQualityToCheck, n);
        put(codeQualityToCheckProducerForClass.getMethodName(), codeQualityToCheckProducerForClass.getStringBuilder());
        SB.append(codeQualityToCheckProducerForClass.getStringBuilder());
    }

    private void testMethods() {
        SB.append('\n');
        Arrays.stream(C.getDeclaredMethods())
                .filter(m -> m.getAnnotation(ToCompare.class) != null)
                .forEach(m -> {
                    ToCompare toCompare = m.getAnnotation(ToCompare.class);
                    testMethod(m, toCompare);
                });
    }

    private void testMethod(Method m, ToCompare toCompare) {
        ToCompareProducer tcp = new ToCompareProducer(C, MSBall, m, toCompare);

        put(tcp.getMethodName(), tcp.getStringBuilder());
        SB.append(tcp.getStringBuilder());

    }

    private void f(String format, Formatter f, Object... o) {
        f.format(format, o);
        F.format(format, o);
    }

    private void checkFields() {
        SB.append('\n');

        Arrays.stream(C.getDeclaredFields())
                .filter(x -> x.getAnnotation(ToCheck.class) != null)
                .forEach(c -> checkField(c, c.getAnnotation(ToCheck.class)));

    }

    /**
     * Vérifie le champ f de la classe testée
     *
     * @param f       the field to check
     * @param toCheck the ToCheck annotation
     */
    private void checkField(Field f, ToCheck toCheck) {

        String testMethodName = String.format("p%06d000_check%sField%s()", toCheck.priority(), C.getSimpleName(), initial(f.getName()));

        // sb contiendra à la fin de la méthode le code complet du test unitaire
        StringBuilder sb = new StringBuilder();
        Formatter formatter = new Formatter(sb, Locale.US);

        unitTestHeader(formatter, toCheck, testMethodName);
        f("   System.out.println(\"Check attribute %s\");\n", formatter, f.getName());
        f("   try {\n", formatter);
        f("        Field x = ReflectUtilities.parseType(\"%s\").getDeclaredField(\"%s\");\n", formatter, C.getName(), f.getName());
        checkModifiers(toCheck.value(), f.getModifiers(), toCheck.modifiers(), formatter);
        f("        assertTrue(\"%s\", x.getType().equals(ReflectUtilities.parseType(\"%s\")));\n", formatter, toCheck.value(), f.getType().getName());
        f("   } catch (Exception ex) {\n", formatter);
        f("        fail(\"%s\");\n", formatter, toCheck.value());
        f("   }\n", formatter);
        f("}\n", formatter);

        put(testMethodName, sb);
        //MSB.put(testMethodName, sb);
    }

    private void checkSetters() {
        SB.append('\n');

        Arrays.stream(C.getDeclaredFields())
                .filter(x -> x.getAnnotation(SetterToCheck.class) != null)
                .forEach(f -> checkSetter(f, f.getAnnotation(SetterToCheck.class)));

    }

    private void checkSetter(Field f, SetterToCheck setterToCheck) {

        String testMethodName = String.format("p%06d000_checkSetter%s%s()", setterToCheck.priority(), C.getSimpleName(), f.getName());

        // sb contiendra à la fin de la méthode le code complet du test unitaire
        StringBuilder sb = new StringBuilder();
        Formatter formatter = new Formatter(sb, Locale.US);

        f("\n@Test\n", formatter);
        if (setterToCheck.grade() != Double.MIN_VALUE) {
            f("@caseine.format.javajunit.Grade(%f)\n", formatter, setterToCheck.grade());
        }
        f("public void %s {\n", formatter, testMethodName);
        f("   System.out.println(\"Check setter %s\");\n", formatter, f.getName());

        // Écrit (si demandé) des appels à des méthodes (probablement des tests unitaires) en préambule à ce
        // test unitaire.
        // Cela peut servir par exemple à empêcher ce test tant que d'autres tests échouent.
        if (setterToCheck.requiersUnitTestsBefore().length > 0) {
            f("%s", formatter, ReflectUtilities.writePreviousUnitTestCalling(MSBall, setterToCheck.requiersUnitTestsBefore()));
        }

        f("   try {\n", formatter);
        f("        Method x = ReflectUtilities.parseType(\"%s\").getDeclaredMethod(\"set%s\", ReflectUtilities.parseType(\"%s\"));\n", formatter,
                C.getName(),
                initial(f.getName()),
                f.getType().getName());
        f("        assertTrue(\"Fix set%s\", x.getReturnType().equals(void.class));\n", formatter,
                initial(f.getName()));
        f("        Object o = ReflectUtilities.randomValue(ReflectUtilities.parseType(\"%s\"));\n", formatter,
                C.getName());
        f("        for(int i = 0; i < 100; ++i) {\n", formatter);
        f("            %1$s v = (%1$s) ReflectUtilities.randomValue(%1$s.class);\n", formatter,
                f.getType().getSimpleName());
        f("            ReflectUtilities.getFromMethodTA(\n", formatter);
        f("                 ReflectUtilities.parseType(\"%s\"),\n", formatter, C.getName());
        f("                 o,\n", formatter);
        f("                 \"set%s\",\n", formatter, initial(f.getName()));
        f("                 ReflectUtilities.parseType(\"%s\"),\n", formatter, f.getType().getName());
        f("                 v\n", formatter);
        f("            );\n", formatter);
        f("            assertTrue(\"Fix set%s\", \n", formatter, initial(f.getName()));
        f("                 ReflectUtilities.getAttribut(\n", formatter);
        f("                 ReflectUtilities.parseType(\"%s\"), \n", formatter, C.getName());
        f("                 o, \n", formatter);
        f("                 \"%s\").equals(v)\n", formatter, f.getName());
        f("            );\n", formatter);
        f("        }\n", formatter);
        f("   } catch (Exception ex) {\n", formatter);
        f("        fail(\"Fix set%s\");\n", formatter, initial(f.getName()));
        f("   }\n", formatter);
        f("}\n", formatter);

        put(testMethodName, sb);
        //MSB.put(testMethodName, sb);

    }

    private void checkGetters() {
        SB.append('\n');

        Arrays.stream(C.getDeclaredFields())
                .filter(x -> x.getAnnotation(GetterToCheck.class) != null)
                .forEach(f -> checkGetter(f, f.getAnnotation(GetterToCheck.class)));

    }

    private void checkGetter(Field f, GetterToCheck getterToCheck) {
        /*           
            for (int i = 0; i < 100; ++i) {
                Object o = ReflectUtilities.randomValue(Point.class);
                Object attr = ReflectUtilities.getAttribut(Point.class, o, "x");
                Object getAttr = ReflectUtilities.getFromMethod(Point.class, o, "getX");
                assertTrue("Fix getX", ReflectUtilities.equals(attr, getAttr));
            }
        } catch (SecurityException | NoSuchMethodException | NoSuchFieldException ex) {
            fail("Fix getX");
        } catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
            fail("Constructor missing to test getX");
        }
    }*/
        String className = C.getName();
        String attributName = f.getName();
        String getterName = "get" + initial(f.getName());

        String testMethodName = String.format("p%06d000_checkGetter%s%s()", getterToCheck.priority(), C.getSimpleName(), initial(f.getName()));

        // sb contiendra à la fin de la méthode le code complet du test unitaire
        StringBuilder sb = new StringBuilder();
        Formatter formatter = new Formatter(sb, Locale.US);

        f("\n@Test\n", formatter);
        if (getterToCheck.grade() != Double.MIN_VALUE) {
            f("@caseine.format.javajunit.Grade(%f)\n", formatter, getterToCheck.grade());
        }
        f("public void %s {\n", formatter, testMethodName);

        f("   System.out.println(\"Check getter %s\");\n", formatter, attributName);

        // Écrit (si demandé) des appels à des méthodes (probablement des tests unitaires) en préambule à ce
        // test unitaire.
        // Cela peut servir par exemple à empêcher ce test tant que d'autres tests échouent.
        if (getterToCheck.requiersUnitTestsBefore().length > 0) {
            f("%s", formatter, ReflectUtilities.writePreviousUnitTestCalling(MSBall, getterToCheck.requiersUnitTestsBefore()));
        }

        f("   try {\n", formatter);
        f("     for(int i = 0; i < 100; ++i) {\n", formatter);
        f("         Object o = ReflectUtilities.randomValue(ReflectUtilities.parseType(\"%s\")); \n", formatter, className);
        f("         Object attr = ReflectUtilities.getAttribut(ReflectUtilities.parseType(\"%s\"), o, \"%s\");\n", formatter, className, attributName);
        f("         Object getAttr = ReflectUtilities.getFromMethod(ReflectUtilities.parseType(\"%s\"), o, \"%s\");\n", formatter, className, getterName);
        f("         assertTrue(\"Fix %s\", ReflectUtilities.equals(attr, getAttr));\n", formatter, getterName);
        f("     }\n", formatter);
        f("   } catch (Exception ex) {\n", formatter);
        f("        fail(\"Fix getter of %s\");\n", formatter, attributName);
        f("   }\n", formatter);
        f("}\n", formatter);

        put(testMethodName, sb);
        //MSB.put(testMethodName, sb);
    }

    /**
     * Affiche (grâce à f) tous les tests relatifs aux modificateurs.
     *
     * @param msg       le message à l'étudiant en cas d'échec
     * @param modifier  l'entier décrivant les modificateurs considérés
     * @param modifiers les modificateurs à comparer
     */
    private void checkModifiers(String msg, long modifier, CheckModifier[] modifiers, Formatter formatter) {
        for (CheckModifier cm : modifiers) {
            f("        " + cm.toString(), formatter, msg, modifier);
        }
    }


    private void checkExceptions(ToCheck toCheck, Executable m, Formatter formatter) {
        if (toCheck.checkExceptions() != CheckExceptions.NONE) {

            java.util.Set<Class<?>> implementations = Arrays.stream(C.getInterfaces())
                    .collect(java.util.stream.Collectors.toSet());

            f("   java.util.Set refExceptions =  new java.util.HashSet<>();\n", formatter);
            for (Class<?> exception : m.getExceptionTypes()) {
                f("   refExceptions.add(ReflectUtilities.parseType(\"%s\"));\n", formatter, exception.getName());
            }
            f("   java.util.Set stdExceptions = java.util.Arrays.stream(x.getExceptionTypes()).collect(java.util.stream.Collectors.toSet());\n", formatter);
            //if (toCheck.checkExceptions() == CheckExceptions.EXACT) {
            f("   assertTrue(\"%s\", refExceptions.equals(stdExceptions));\n", formatter, toCheck.value() + " (Exceptions)");
        }
    }


    private void checkMethods() {
        SB.append('\n');
        Arrays.stream(C.getDeclaredMethods())
                .filter(m -> m.getAnnotation(ToCheck.class) != null)
                .forEach(m -> {
                    ToCheck toCheck = m.getAnnotation(ToCheck.class);
                    checkMethod(m, toCheck);
                });
    }

    private void checkMethod(Method m, ToCheck toCheck) {
        String msg = toCheck.value();
        msg = msg.isEmpty() ? m.getName() : msg;

        String testMethodName = String.format("p%06d000_checkMethod%s%s%s()", toCheck.priority(), C.getSimpleName(), m.getName(), paramsToString(m));

        // sb contiendra à la fin de la méthode le code complet du test unitaire
        StringBuilder sb = new StringBuilder();
        Formatter formatter = new Formatter(sb, Locale.US);

        unitTestHeader(formatter, toCheck, testMethodName);

        f("   System.out.println(\"%s\");\n", formatter, msg);
        f("   try {\n", formatter);
        f("        Class<?> theClass = ReflectUtilities.parseType(\"%s\"); ", formatter, C.getName());
        f("        Method x = theClass.getDeclaredMethod(\"%s\"\n", formatter, m.getName());
        if (m.getParameterCount() > 0) {
            Class<?>[] parameterTypes = m.getParameterTypes();
            f("          , ReflectUtilities.parseType(\"%s\")", formatter, parameterTypes[0].getName());
            for (int i = 1; i < parameterTypes.length; ++i) {
                f(",\n           ReflectUtilities.parseType(\"%s\")", formatter, parameterTypes[i].getName());
            }
        }
        f("        );\n", formatter);
        checkModifiers(msg, m.getModifiers(), toCheck.modifiers(), formatter);

        checkExceptions(toCheck, m, formatter);

        //f("        assertTrue(\"Fix %s (modifiers)\", c.getModifiers() == %d);\n", msg, m.getModifiers());
        f("        assertTrue(\"Fix %s (Return)\", x.getReturnType().equals(ReflectUtilities.parseType(\"%s\")));\n", formatter, msg, m.getReturnType().getName());
        f("   } catch (NoSuchMethodException | SecurityException ex) {\n", formatter);
        f("        fail(\"Fix %s\");\n", formatter, msg);
        f("   } catch (ClassNotFoundException ex3) {\n", formatter);
        f("        fail(\"Unknown class \"+%s);\n", formatter, "ex3.toString()");
        f("   } catch (Exception ex2) {\n", formatter);
        f("        fail(\"Unexpected Error \"+%s);\n", formatter, "ex2.toString()");
        f("   }\n", formatter);
        f("}\n", formatter);

        put(testMethodName, sb);
        //MSB.put(testMethodName, sb);
    }

    private void checkInnerClasses() {
        SB.append('\n');

        Arrays.stream(C.getDeclaredClasses())
                .filter(x -> x.getAnnotation(ToCheck.class) != null)
                .forEach(c -> checkInnerClass(c, c.getAnnotation(ToCheck.class)));
    }

    private void checkInnerClass(Class<?> c, ToCheck toCheck) {

        String testMethodName = String.format("p%06d000_check%sInnerClass%s()", toCheck.priority(), C.getSimpleName(), initial(c.getSimpleName()));

        // sb contiendra à la fin de la méthode le code complet du test unitaire
        StringBuilder sb = new StringBuilder();
        Formatter formatter = new Formatter(sb, Locale.US);

        unitTestHeader(formatter, toCheck, testMethodName);
        f("   System.out.println(\"Check innerClass %s\");\n", formatter, c.getSimpleName());
        f("   try {\n", formatter);
        //f("        Class<?> x = ReflectUtilities.parseType(\"%s\").getDeclaredField(\"%s\");\n", formatter, C.getName(), f.getName());
        f("        Class<?> x = ReflectUtilities.getDeclaredClass(%s.class, \"%s\");\n", formatter, C.getName(), c.getSimpleName());
        f("        if (x != null) {\n", formatter);
        checkModifiers(toCheck.value(), c.getModifiers(), toCheck.modifiers(), formatter);
        f("        } else {\n", formatter);
        f("           fail(\"Classe interne %s absente\");", formatter, c.getSimpleName());
        f("        }\n", formatter);
        f("   } catch (Exception ex) {\n", formatter);
        f("        fail(\"%s\");\n", formatter, toCheck.value());
        f("   }\n", formatter);
        f("}\n", formatter);

        put(testMethodName, sb);
        //MSB.put(testMethodName, sb);
        new Checker(c, this);
    }

    /**
     * Tous les tests unitaires générés.
     *
     * @return une chaîne de caractères qui contient toutes les méthodes de
     * tests unitaires générées.
     */
    @Override
    public String toString() {
        return SB.toString();
    }

    private String initial(String name) {
        return name.substring(0, 1).toUpperCase() + name.substring(1);
    }

    private String getAllTUOrderByName() {
        StringBuilder sb = new StringBuilder();
        MSBall.keySet().forEach((name) -> sb.append(MSBall.get(name)));
        return sb.toString();
    }

    /**
     * Pour itérer sur tous les tests unitaires générés.
     *
     * @return un itérateur pour itérer sur tous les tests unitaires générés.
     */
    @Override
    public Iterator<BodyDeclaration<?>> iterator() {
        return new Iterator<BodyDeclaration<?>>() {
            private final Iterator<String> I = BDall.keySet().iterator();

            @Override
            public boolean hasNext() {
                return I.hasNext();
            }

            @Override
            public BodyDeclaration<?> next() {
                return BDall.get(I.next());
            }
        };
    }
}
