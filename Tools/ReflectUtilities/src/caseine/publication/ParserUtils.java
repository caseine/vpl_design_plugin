/*
 * Copyright 2019: Yvan Maillot
 * This software is part of the Caseine project.
 * This software was developped with the support of the following organizations:
 * Université de Haute-Alsace
 * Institut Polytechnique de Grenoble
 *
 *
 * This file is part of the VPL Design Tool.
 * The VPL Design Tool is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The VPL Design Tool is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the VPL Design Tool.  If not, see <https://www.gnu.org/licenses/>.
 */
package caseine.publication;

import caseine.reflect.ParserWithReflectUtilities;
import caseine.tags.*;
import com.github.javaparser.ParseProblemException;
import com.github.javaparser.StaticJavaParser;
import com.github.javaparser.ast.*;
import com.github.javaparser.ast.body.*;
import com.github.javaparser.ast.comments.BlockComment;
import com.github.javaparser.ast.comments.Comment;
import com.github.javaparser.ast.comments.JavadocComment;
import com.github.javaparser.ast.comments.LineComment;
import com.github.javaparser.ast.expr.*;
import com.github.javaparser.ast.modules.*;
import com.github.javaparser.ast.nodeTypes.NodeWithJavadoc;
import com.github.javaparser.ast.stmt.*;
import com.github.javaparser.ast.type.*;
import com.github.javaparser.ast.visitor.ModifierVisitor;
import com.github.javaparser.ast.visitor.Visitable;
import com.github.javaparser.ast.visitor.VoidVisitorAdapter;
import com.github.javaparser.utils.Pair;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;
import java.util.stream.Collectors;

public class ParserUtils {

    public static final double DEFAULT_MAXI_GRADE = 20.0;

    public static final Map<Integer, Modifier> intModifierToAstModifier;

    static {
        intModifierToAstModifier = new HashMap<>();
        intModifierToAstModifier.put(java.lang.reflect.Modifier.ABSTRACT, Modifier.abstractModifier());
        intModifierToAstModifier.put(java.lang.reflect.Modifier.FINAL, Modifier.finalModifier());
        intModifierToAstModifier.put(java.lang.reflect.Modifier.NATIVE, Modifier.nativeModifier());
        intModifierToAstModifier.put(java.lang.reflect.Modifier.PRIVATE, Modifier.privateModifier());
        intModifierToAstModifier.put(java.lang.reflect.Modifier.PROTECTED, Modifier.protectedModifier());
        intModifierToAstModifier.put(java.lang.reflect.Modifier.PUBLIC, Modifier.publicModifier());
        intModifierToAstModifier.put(java.lang.reflect.Modifier.STATIC, Modifier.staticModifier());
        intModifierToAstModifier.put(java.lang.reflect.Modifier.STRICT, Modifier.strictfpModifier());
        intModifierToAstModifier.put(java.lang.reflect.Modifier.SYNCHRONIZED, Modifier.synchronizedModifier());
        intModifierToAstModifier.put(java.lang.reflect.Modifier.TRANSIENT, Modifier.transientModifier());
        intModifierToAstModifier.put(java.lang.reflect.Modifier.VOLATILE, Modifier.volatileModifier());
    }

    public static enum CUType {
    	JUNIT4,
    	JUNIT5,
    	CLASS   	
    }
    /**
     *
     * @param nae l'annotation dont on veut la valeur
     * @return la valeur de la propriété value d'une annotation multi-valeurs,
     * si elle existe ou la chaine vide sinon.
     */
    private static String getValueOf(NormalAnnotationExpr nae) {
        return getValueOf(nae.getPairs());
    }

    private static String getValueOf(NodeList<MemberValuePair> pairs) {
        for (MemberValuePair mvp : pairs) {
            if ("value".equals(mvp.getName().getIdentifier())) {
                return mvp.getValue().toString();
            }
        }
        return "";
    }

    private static Comment toComment(String value) {
        if (value.contains("\n")) {
            return new BlockComment(" TODO " + value);
        } else {
            return new LineComment("TODO " + value);
        }
    }

    private static String whatReplacementString(AnnotationExpr annotation, CompilationUnit compilationUnit) {
        String replacement = null;
        if (annotation.isNormalAnnotationExpr()) {
            NormalAnnotationExpr na = annotation.asNormalAnnotationExpr();
            NodeList<MemberValuePair> list = na.getPairs();

            for (MemberValuePair mvp : list) {
                if ("replacement".equals(mvp.getName().asString())) {
                    replacement = mvp.getValue().asStringLiteralExpr().getValue()
                            .replace("\\\"", "\"")
                            .replace("\\\\\"", "\\\"");

                    if (replacement.isEmpty()) {
                        replacement = null;
                    }
                }
            }
        }
        if (replacement != null) {
            // Check if replacement is an available absolute path of a file
            try ( Scanner in = new Scanner(new File(replacement))) {
                StringBuilder sb = new StringBuilder();
                while (in.hasNextLine()) {
                    sb.append(in.nextLine()).append('\n');
                }
                return sb.toString();
            } catch (FileNotFoundException ignored) {
                // if not, check if replacement is an available relative path of a file
                if (compilationUnit.getStorage().isEmpty())
                    return replacement;
                try ( Scanner in = new Scanner(new File(compilationUnit.getStorage().get().getSourceRoot().toFile(), replacement))) {
                    StringBuilder sb = new StringBuilder();
                    while (in.hasNextLine()) {
                        sb.append(in.nextLine()).append('\n');
                    }
                    return sb.toString();
                } catch (FileNotFoundException ignoredBis) {
                    // if not, it should be something parsable
                    return replacement;
                }
            }
        }
        return replacement;
    }

    private static Comment whatToDo(AnnotationExpr annotation) {
        Comment toDo = null;
        if (annotation.isSingleMemberAnnotationExpr()) {
            SingleMemberAnnotationExpr sma = annotation.asSingleMemberAnnotationExpr();
            String value = toString(sma.getMemberValue());
            return toComment(value);
        } else if (annotation.isNormalAnnotationExpr()) {
            NormalAnnotationExpr na = annotation.asNormalAnnotationExpr();
            NodeList<MemberValuePair> list = na.getPairs();
            String value = null;
            for (MemberValuePair mvp : list) {
                if ("value".equals(mvp.getName().asString())) {
                    value = toString(mvp.getValue());
                }
            }
            if (value != null) {
                return toComment(value);
            }
            //toDo = getValueOf(na.getPairs()).replaceFirst("\"", "").replaceAll("\"$", "");
        }
        return toDo;
    }

    /*private static String whatToDo(AnnotationExpr annotation) {
        String toDo = "";
        if (annotation.getClass() == SingleMemberAnnotationExpr.class) {
            SingleMemberAnnotationExpr sma = (SingleMemberAnnotationExpr) annotation;
            toDo = sma.getMemberValue().toString().replaceFirst("\"", "").replaceAll("\"$", "");
        } else if (annotation.getClass() == NormalAnnotationExpr.class) {
            NormalAnnotationExpr na = (NormalAnnotationExpr) annotation;
            toDo = getValueOf(na.getPairs()).replaceFirst("\"", "").replaceAll("\"$", "");
        }
        return toDo;
    }*/
    public static void importTagSuppression(CompilationUnit compilationUnit) {
        compilationUnit.getImports().removeIf(id -> id.getName().asString().startsWith("caseine.tags"));
    }

    public static void changePackageForCfTest(CompilationUnit compilationUnit) {
        Optional<PackageDeclaration> opt = compilationUnit.getPackageDeclaration();
        if (opt.isPresent()) {
            opt.get().setName("cf." + opt.get().getNameAsString());
        } else {
            compilationUnit.setPackageDeclaration("cf");
        }
    }

    /**
     * @param value an expression which a StringLiteralExpr or an addition
     * between a StringLiteralExpr and the same thing For instance : "un" or
     * "un" + " deux" or "un" + " deux" + " trois"
     * @return "un" or "un deux" or "un deux trois"
     */
    private static String toString(Expression value) {
        if (value.isStringLiteralExpr()) {
            return value.asStringLiteralExpr().asString();
        } else if (value.isBinaryExpr() && value.asBinaryExpr().getOperator() == BinaryExpr.Operator.PLUS) {
            return toString(value.asBinaryExpr().getLeft()) + toString(value.asBinaryExpr().getRight());
        } else {
            throw new IllegalArgumentException("Illegal annotation " + value);
        }
    }

    /**
     * Supprime les implements marquées à supprimer dans la déclaration de class
     * Voir le tag ImplementationToRemove
     *
     * @param cid la classe ou l'interface dont on veut supprimer les
     * implémentations
     */
    public static void implementationSuppression(ClassOrInterfaceDeclaration cid) {

        Optional<AnnotationExpr> optA = cid.getAnnotationByClass(ImplementationToRemove.class);
        if (optA.isPresent()) {
            AnnotationExpr annotationImplementationToRemove = optA.get();
            final Set<String> implementation;
            Expression value;

            if (annotationImplementationToRemove.isSingleMemberAnnotationExpr()) {
                value = annotationImplementationToRemove.asSingleMemberAnnotationExpr().getMemberValue();
            } else /* if (a.isNormalAnnotationExpr()) */ {
                value = annotationImplementationToRemove.asNormalAnnotationExpr().getPairs().get(0).getValue();
            }
            if (value.isStringLiteralExpr()) {
                implementation = new HashSet<>();
                implementation.add(value.asStringLiteralExpr().asString());
            } else/* if (value.isArrayInitializerExpr())*/ {
                implementation = value.asArrayInitializerExpr().getValues()
                        .stream()
                        .map(e -> e.asStringLiteralExpr().getValue())
                        .collect(Collectors.toSet());
            }

            Set<ClassOrInterfaceType> sci;

            if (cid.isInterface()) {
                sci = cid.asClassOrInterfaceDeclaration().getExtendedTypes()
                        .stream()
                        .filter(ClassOrInterfaceType::isClassOrInterfaceType)
                        .map(ClassOrInterfaceType::asClassOrInterfaceType)
                        .filter(c -> implementation.contains(c.getNameAsString()))
                        .collect(Collectors.toSet());

            } else {
                sci = cid.asClassOrInterfaceDeclaration().getImplementedTypes()
                        .stream()
                        .filter(ClassOrInterfaceType::isClassOrInterfaceType)
                        .map(ClassOrInterfaceType::asClassOrInterfaceType)
                        .filter(c -> implementation.contains(c.getNameAsString()))
                        .collect(Collectors.toSet());
            }

            sci.stream().forEach(Node::remove);
        }
    }

    /*
     * Supprime les implements marquées à supprimer Voir le tag
     * ImplementationToRemove
     *
     * @param compilationUnit
     */
    public static void implementationSuppression(CompilationUnit compilationUnit) {
        compilationUnit.getTypes()
                .stream()
                .filter(BodyDeclaration::isClassOrInterfaceDeclaration)
                .map(BodyDeclaration::asClassOrInterfaceDeclaration)
                .forEach(ParserUtils::implementationSuppression);
    }

    /**
     * Remplace chaque tags ToDo par un commentaire et supprime l'élément annoté
     * dans toute l'unité de compilation.
     * <p>
     * Remplace chaque tags ToDoIn par un commentaire et supprime le corps de la
     * méthode annotée.
     *
     * @param compilationUnit Unité de compilation dans laquelle s'applique ce
     * traitement.
     */
    public static void toDoSuppression(CompilationUnit compilationUnit) {
        // Suppression des import tags.*;
        // importTagSuppression(compilationUnit);

        // Collecte des éléments annotés ToDo
        List<AnnotationExpr> toDoList = compilationUnit.stream()
                .filter(n -> n instanceof AnnotationExpr)
                .map(n -> (AnnotationExpr) n)
                .filter(n -> n.getName().getIdentifier().endsWith("ToDo")).toList();

        // Traitement des éléments annotés ToDo
        //toDoList.forEach(n -> {
        for (AnnotationExpr n : toDoList) {
            //n.getParentNode().ifPresent(p -> {
            if (n.getParentNode().isPresent()) {
                Node p = n.getParentNode().get();

                //p.getParentNode().ifPresent(gp -> {
                if (p.getParentNode().isPresent()) {
                    Node gp = p.getParentNode().get();
                    // On se place au niveau du "grand-père" pour ne pas scier
                    // la branche sur laquelle on est.
                    Comment whatToDo = whatToDo(n);
                    String replacement = whatReplacementString(n, compilationUnit);
                    // Dans le cas du @ToDo, un message vide n'engendre aucun
                    // commentaire. Pour mettre explicitement un // ToDo
                    // Il faut utiliser @ToDo(" ")
                    if (whatToDo != null && replacement == null) {
                        try {
                            // Si l'élément annoté est lui-même dans un élément annoté
                            // ses commentaires vont disparaitre avec lui.
                            // C'est pourquoi, dans ce cas, le commentaire TODO
                            // est rajouté au niveau de l'unité de compilation.
                            ClassOrInterfaceDeclaration nwa = (ClassOrInterfaceDeclaration) gp;
                            if (nwa.getAnnotationByName("ToDo").isPresent()) {
                                compilationUnit.addOrphanComment(whatToDo);
                                compilationUnit.addOrphanComment(new LineComment(""));
                            } else {
                                gp.addOrphanComment(whatToDo);
                                gp.addOrphanComment(new LineComment(""));
                            }
                        } catch (Exception ex) {
                            gp.addOrphanComment(whatToDo);
                            gp.addOrphanComment(new LineComment(""));
                        }

                    }

                    n.getComment().ifPresent(c -> {
                        gp.addOrphanComment(c);
                        gp.addOrphanComment(new LineComment(""));
                    });

                    // Si un élément annoté ToDo possède de la JavaDoc, celle-ci
                    // est rajoutée en tant que commentaire orphelin de la classe
                    // dans laquelle se trouve l'objet annoté (puisqu'il va disparaitre).
                    if (replacement == null && p instanceof NodeWithJavadoc) {

                        Optional<JavadocComment> jc = ((NodeWithJavadoc<?>) p).getJavadocComment();
                        jc.ifPresent(c -> {

                            gp.addOrphanComment(c);
                            gp.addOrphanComment(new LineComment(""));
                        });

                    }

                    if (replacement != null) {
                        Optional<JavadocComment> optjd = Optional.empty();
                        if (p instanceof NodeWithJavadoc) {
                            optjd = ((NodeWithJavadoc<?>) p).getJavadocComment();
                        }

                        Node newP = replace(p, replacement, whatToDo, optjd);
                        if (newP != null) {
                            newP.removeComment();
                            newP.setComment(whatToDo);
                            newP.setRange(null);
                        }
                        //}
                    } else {
                        // Si un élément annoté ToDo possède un commentaire, celui-ci
                        // est rajouté en tant que commentaire orphelin de la classe
                        // dans laquelle se trouve l'objet commenté (puisqu'il va disparaitre).
                        n.getComment().ifPresent(c -> {
                            gp.addOrphanComment(c);
                            gp.addOrphanComment(new LineComment(""));
                        });
                        p.remove();
                    }
                    //});
                    //});
                    //});
                }
            }
        }
        // Collecte des éléments annotés ToDoIn
        List<AnnotationExpr> toDoInList = compilationUnit.stream()
                .filter(n -> n instanceof AnnotationExpr)
                .map(n -> (AnnotationExpr) n)
                .filter(n -> n.getName().getIdentifier().endsWith("ToDoIn")).toList();

        // Traitement des éléments annotés ToDoIn
        toDoInList.stream().forEach((n) -> n.getParentNode().ifPresent(p -> {
            if (p.getClass() == MethodDeclaration.class) {
                MethodDeclaration md = (MethodDeclaration) p;
                Type typeOfReturn = md.getType();
                String body;
                String replacement = whatReplacementString(n, compilationUnit);
                if (replacement != null) {
                    body = replacement;
                } else if (typeOfReturn.isVoidType()) {
                    body = "{}";
                } else if (typeOfReturn.isReferenceType()) {
                    body = "{return null;}";
                } else if (typeOfReturn.isArrayType()) {
                    body = "{return null;}";
                } else if (typeOfReturn.asString().equals("boolean")) {
                    body = "{return false;}";
                } else {
                    body = "{return 0;}";
                }
                md = md.setBody(StaticJavaParser.parseBlock(body));
                Comment whatToDo = whatToDo(n);
                md.getBody().ifPresent(b -> {
                    if (b.getStatements().size() == 0) {
                        // Dans le cas du ToDoIn, un message vide entraîne un // TODO
                        b.addOrphanComment(whatToDo == null ? new LineComment("TODO") : whatToDo);
                    } else {
                        // Dans le cas du ToDoIn, un message vide entraîne un // TODO
                        b.getStatements().get(0).setComment(whatToDo == null ? new LineComment("TODO") : whatToDo);
                    }
                });
            }
        }));

        // ParserUtils.annotationSuppression(compilationUnit);
    }

    private static Node replace(Node node, String replacement, Comment comment, Optional<JavadocComment> optjd) {
        Node newNode = null;
        try {
            newNode = StaticJavaParser.parseParameter(replacement);
        } catch (ParseProblemException ignored) {

        }
        try {
            newNode = StaticJavaParser.parseBodyDeclaration(replacement);
        } catch (ParseProblemException ignored) {

        }
        try {
            newNode = StaticJavaParser.parseClassOrInterfaceType(replacement);
        } catch (ParseProblemException ignored) {

        }
        if (newNode == null) {
            return null;
        } else {
            if (newNode instanceof NodeWithJavadoc && optjd != null && optjd.isPresent()) {
                ((NodeWithJavadoc<?>) newNode).setJavadocComment(optjd.get());
            }
            if (comment != null) {
                newNode.setComment(comment);
            }
            if (node.replace(newNode)) {
                return newNode;
            } else {
                return null;
            }
        }
    }

    /**
     * Remplace chaque tags ToDoInConstructor par un commentaire et supprime le
     * corps du constructeur annoté.
     *
     * @param compilationUnit Unité de compilation dans laquelle s'applique ce
     * traitement.
     */
    public static void toDoInConstructorSuppression(CompilationUnit compilationUnit) {

        // Collecte des éléments annotés ToDoInConstructor
        List<AnnotationExpr> toDoInConstructorList = compilationUnit.stream()
                .filter(n -> n instanceof AnnotationExpr)
                .map(n -> (AnnotationExpr) n)
                .filter(n -> n.getName().getIdentifier().endsWith("ToDoInConstructor")).toList();

        // Traitement des éléments annotés ToDoInInConstructor
        toDoInConstructorList.stream().forEach((n) -> n.getParentNode().ifPresent(p -> {
            if (p.getClass() == ConstructorDeclaration.class) {
                ConstructorDeclaration constructorDeclaration = (ConstructorDeclaration) p;

                String replacement = whatReplacementString(n, compilationUnit);
                constructorDeclaration = constructorDeclaration.setBody(StaticJavaParser.parseBlock(Objects.requireNonNullElse(replacement, "{}")));

                Comment whatToDo = whatToDo(n);
                constructorDeclaration.getBody().addOrphanComment(Objects.requireNonNullElseGet(whatToDo, () -> new LineComment("TODO")));
            }

        }));

        // ParserUtils.annotationSuppression(compilationUnit);
    }

    public static void annotationSuppression(CompilationUnit compilationUnit) {
        importTagSuppression(compilationUnit);

        ModifierVisitor<?> mv = new NormalAnnotationSuppresser();
        mv.visit(compilationUnit, null);
        mv = new SingleMemberAnnotationSuppresser();
        mv.visit(compilationUnit, null);
        mv = new MarkerAnnotationSuppresser();
        mv.visit(compilationUnit, null);
    }

    public static Pair<Double, Double> getCounterAndMaxiGrade(CompilationUnit cu) {
        GradeCounter gc = new GradeCounter();
        gc.visit(cu, null);
        return gc.getCounterAndMaxiGrade();
    }

    public static void gradeTransforme(CompilationUnit cu, double cumul, double maximum) {
        GradeTransformer gc = new GradeTransformer(cumul, maximum);
        gc.visit(cu, null);
    }

    public static void gradeTransforme(CompilationUnit cu, double cumul) {
        gradeTransforme(cu, cumul, DEFAULT_MAXI_GRADE);
    }

    public static void copyResources(Path src) throws IOException {
        /*Files.walkFileTree(src, new SimpleFileVisitor<Path>() {
            @Override
            public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                System.out.println(file);
                return FileVisitResult.CONTINUE;
            }

        });*/
        Files.walk(src)
                .filter(p -> !p.endsWith(".java"))
                .forEach(System.out::println);
    }

    public static boolean compilationUnitHasAPrimaryTypeAnnoted(CompilationUnit cu, Class<?> annotation) {
        Optional<TypeDeclaration<?>> opttd = cu.getPrimaryType();
        if (opttd.isPresent()) {
            @SuppressWarnings("rawtypes")
            TypeDeclaration td = opttd.get();
            return td.isAnnotationPresent(annotation.getSimpleName());
        } else {
            return false;
        }
    }

    /**
     * Determine si une classe est de type JUnit, avec sa version.
     * @param cu
     * @return the type of cu
     */
    public static CUType getCompilationUnitType(CompilationUnit cu) {
    	CUType result = CUType.CLASS;
        NodeList<ImportDeclaration> imports = cu.getImports();
        Set<ImportDeclaration> testImports = imports.stream().filter(imp -> imp.toString().contains("org.junit.jupiter")).collect(Collectors.toSet());
        if (!testImports.isEmpty()) {
        	result = CUType.JUNIT5;
        } else {
	        testImports = imports.stream().filter(imp -> imp.toString().contains("junit")).collect(Collectors.toSet());
	        if (!testImports.isEmpty()) {
	        	result = CUType.JUNIT4;
	        }
        }
        return result;
    }
    
    public static boolean compilationUnitIsATest(CompilationUnit cu) {
    	return ParserUtils.getCompilationUnitType(cu) != CUType.CLASS;
    }

    public static boolean compilationUnitIsATest(CompilationUnit cu, CUType type) {
    	return ParserUtils.getCompilationUnitType(cu) == type;
    }

    public static boolean hasAnExplicitConstructorInvocation(ConstructorDeclaration k) {
        FinderOfExplicitConstructorInvocationStmt finder = new FinderOfExplicitConstructorInvocationStmt(k);

        return finder.getExplicitConstructorInvocationStmt().size() == 1;
    }

    public static boolean hasAnExplicitConstructorInvocation(Constructor<?> k) {
        /*FinderOfExplicitConstructorInvocationStmt finder = new FinderOfExplicitConstructorInvocationStmt(k);

        return finder.getExplicitConstructorInvocationStmt().size() == 1;*/
        return hasAnExplicitConstructorInvocation(ParserWithReflectUtilities.find(k));
    }

    private static final Set<String> caseineTagToSuppress;

    static {
        caseineTagToSuppress = new HashSet<>();
        caseineTagToSuppress.add("ClassTestPriority");
        caseineTagToSuppress.add("CodeQualityToCheck");
        caseineTagToSuppress.add("CorrectedFilesForStudent");
        caseineTagToSuppress.add("FileToRemove");
        caseineTagToSuppress.add("GetterToCheck");
        caseineTagToSuppress.add("ImplementationToRemove");
        caseineTagToSuppress.add("MaxInt");
        caseineTagToSuppress.add("MinInt");
        caseineTagToSuppress.add("OnlyOneStatementToCheck");
        caseineTagToSuppress.add("RelativeEvaluation");
        caseineTagToSuppress.add("SetterToCheck");
        caseineTagToSuppress.add("StatementCountToCheck");
        caseineTagToSuppress.add("ToCheck");
        caseineTagToSuppress.add("ToCompare");
        caseineTagToSuppress.add("ToDo");
        caseineTagToSuppress.add("ToDoIn");
        caseineTagToSuppress.add("ToDoInConstructor");
    }

    /**
     * Pour supprimer les annotations caseine.tag.*
     */
    private static class NormalAnnotationSuppresser extends ModifierVisitor<Void> {

        @Override
        public NormalAnnotationExpr visit(NormalAnnotationExpr fd, Void arg) {

            super.visit(fd, arg);

            if (ParserUtils.isToSupress(fd.getNameAsString())) {
                return null;
            } else {
                return fd;
            }
        }
    }

    /**
     * Les annotations à supprimer sont celles dans caseineTagToSuppress
     *
     * @param name of tag to suppress
     * @return a tag has been suppressed
     */
    private static boolean isToSupress(String name) {
        return caseineTagToSuppress.stream().anyMatch(s -> s.endsWith(name));
    }

    /**
     * Pour supprimer les annotations caseine.tag.*
     */
    private static class MarkerAnnotationSuppresser extends ModifierVisitor<Void> {

        @Override
        public MarkerAnnotationExpr visit(MarkerAnnotationExpr fd, Void arg) {

            super.visit(fd, arg);

            if (ParserUtils.isToSupress(fd.getNameAsString())) {
                return null;
            } else {
                return fd;
            }
        }
    }

    /**
     * Pour supprimer les annotations caseine.tag.*
     */
    private static class SingleMemberAnnotationSuppresser extends ModifierVisitor<Void> {

        @Override
        public SingleMemberAnnotationExpr visit(SingleMemberAnnotationExpr fd, Void arg) {

            super.visit(fd, arg);
            if (ParserUtils.isToSupress(fd.getNameAsString())) {
                return null;
            } else {
                return fd;
            }
        }
    }

    /**
     * Compte le cumul des grades et établit la note maximale si l'annotation RelativeEvaluation a été rencontrée.
     */
    private static class GradeCounter extends VoidVisitorAdapter<Void> {

        private double counter = 0;
        private double maxiGrade = 0.0;

        @Override
        public void visit(SingleMemberAnnotationExpr n, Void arg) {
            super.visit(n, arg);

            switch (n.getNameAsString()) {
                case "Grade", "caseine.format.javajunit.Grade" -> {
                    double value;
                    try {
                        value = Double.parseDouble(n.getMemberValue().toString());
                    } catch (NumberFormatException ex) {
                        value = 0;
                    }
                    counter += value;

                }
                case "RelativeEvaluation", "caseine.tags.RelativeEvaluation" -> {
                    try {
                        maxiGrade = Double.parseDouble(n.getMemberValue().toString());
                    } catch (NumberFormatException ex) {
                        maxiGrade = DEFAULT_MAXI_GRADE;
                    }

                }
            }

        }

        @Override
        public void visit(MarkerAnnotationExpr n, Void arg) {
            super.visit(n, arg);

            switch (n.getNameAsString()) {
                case "RelativeEvaluation", "caseine.tags.RelativeEvaluation" -> maxiGrade = DEFAULT_MAXI_GRADE;
            }

        }

        @Override
        public void visit(NormalAnnotationExpr n, Void arg) {
            super.visit(n, arg);

            switch (n.getNameAsString()) {
                case "RelativeEvaluation", "caseine.tags.RelativeEvaluation" -> {
                    try {
                        maxiGrade = Double.parseDouble(getValueOf(n));
                    } catch (NumberFormatException ex) {
                        maxiGrade = DEFAULT_MAXI_GRADE;
                    }

                }
            }

        }

        public double getCounter() {
            return counter;
        }

        public double getMaxiGrade() {
            return maxiGrade;
        }

        public Pair<Double, Double> getCounterAndMaxiGrade() {
            return new Pair<>(counter, maxiGrade);
        }

    }

    private static class GradeTransformer extends ModifierVisitor<Void> {

        private final double cumul;
        private final double maximum;

        public GradeTransformer(double cumul, double maximum) {
            this.cumul = cumul;
            this.maximum = maximum;
        }

        @Override
        public Visitable visit(SingleMemberAnnotationExpr n, Void arg) {
            super.visit(n, arg);
            if ("Grade".equals(n.getNameAsString())
                    || "caseine.format.javajunit.Grade".equals(n.getNameAsString())) {
                double value;
                try {
                    value = Double.parseDouble(n.getMemberValue().toString());
                } catch (NumberFormatException ex) {
                    value = 0;
                }
                n.setMemberValue(new DoubleLiteralExpr(((value * maximum) / cumul)));
            }
            return n;
        }
    }

    public static abstract class Finder {

        protected boolean deep;
        protected Visitable visitable;

        public Finder(Visitable visitable) {
            this(visitable, false);
        }

        public Finder(Visitable visitable, boolean deep) {
            this.deep = deep;
            this.visitable = visitable;
        }
    }

    /**
     * Pour extraire toutes les sélections If, Switch et ?:
     *
     * @author Yvan Maillot &lt;yvan.maillot@uha.fr&gt;
     */
    public static class SelectionFinder extends Finder {

        private final ArrayList<? super Node> selections = new ArrayList<>();
        private final VoidVisitorAdapter<Void> voidVisitorAdapter = new VoidVisitorAdapter<Void>() {

            @Override
            public void visit(IfStmt n, Void arg) {
                if (deep) {
                    super.visit(n, arg);
                }
                selections.add(n);
            }

            @Override
            public void visit(SwitchStmt n, Void arg) {
                if (deep) {
                    super.visit(n, arg);
                }
                selections.add(n);
            }

            @Override
            public void visit(ConditionalExpr n, Void arg) {
                if (deep) {
                    super.visit(n, arg);
                }
                selections.add(n);
            }
        };

        public SelectionFinder(Method m) {
            this(ParserWithReflectUtilities.find(m), true);
        }

        public SelectionFinder(Class<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public SelectionFinder(Class<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public SelectionFinder(Method m, boolean deep) {
            this(ParserWithReflectUtilities.find(m), deep);
        }

        public SelectionFinder(Constructor<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public SelectionFinder(Constructor<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), deep);
        }

        /**
         * Dans ce cas, deep vaut true.
         *
         * @param visitable le Visitable dans le lequel chercher les SwitchEntry
         */
        public SelectionFinder(Visitable visitable) {
            this(visitable, false);
        }

        /**
         * Effectue la recherche des SwitchEntry dans un Visitable, en
         * profondeur ou non selon l'état de {@code deep}.
         * <p>
         * Rechercher en profondeur signifie que si un élément a été trouvé, la
         * recherche continue dans l'élément lui-même. Cela permet de trouver
         * les "if" imbriqués par exemple.
         * <p>
         * Ne pas rechercher en profondeur signifie qu'on s'arrête au premier
         * niveau.
         * <p>
         * Cette propriété n'a pas d'intérêt pour tous les éléments
         *
         * @param visitable le Visitable dans le lequel chercher les SwitchEntry
         * @param deep recherche en profondeur ou non
         */
        public SelectionFinder(Visitable visitable, boolean deep) {
            super(visitable, deep);
            this.visitable.accept(voidVisitorAdapter, null);
        }

        /**
         * Retourne le tableau des boucles trouvés
         *
         * @return le tableau des SwitchEntry trouvés
         */
        public ArrayList<? super Node> getSelections() {
            return selections;
        }

        /**
         * Retourne le nombre de boucles trouvées
         *
         * @return le nombre de boucles trouvées
         */
        public int getNumberOfSelections() {
            return selections.size();
        }
    }

    /**
     * Pour extraire toutes les boucles For, Foreach, While et DoWhile
     *
     * @author Yvan Maillot &lt;yvan.maillot@uha.fr&gt;
     */
    public static class LoopFinder extends Finder {

        private final ArrayList<? super Statement> loops = new ArrayList();
        private final VoidVisitorAdapter<Void> voidVisitorAdapter = new VoidVisitorAdapter<>() {

            @Override
            public void visit(ForEachStmt n, Void arg) {
                if (deep) {
                    super.visit(n, arg);
                }
                loops.add(n);
            }

            @Override
            public void visit(ForStmt n, Void arg) {
                if (deep) {
                    super.visit(n, arg);
                }
                loops.add(n);
            }

            @Override
            public void visit(WhileStmt n, Void arg) {
                if (deep) {
                    super.visit(n, arg);
                }
                loops.add(n);
            }

            @Override
            public void visit(DoStmt n, Void arg) {
                if (deep) {
                    super.visit(n, arg);
                }
                loops.add(n);
            }
        };

        /**
         * Effectue la recherche des boucles dans un Visitable, en profondeur ou
         * non selon l'état de {@code deep}.
         * <p>
         * Rechercher en profondeur signifie que si un élément a été trouvé, la
         * recherche continue dans l'élément lui-même. Cela permet de trouver
         * les "boucle" imbriqués.
         * <p>
         * Ne pas rechercher en profondeur signifie qu'on s'arrête au premier
         * niveau.
         *
         * @param visitable le Visitable dans lequel chercher les boucles
         * @param deep recherche en profondeur ou non
         */
        public LoopFinder(Visitable visitable, boolean deep) {
            super(visitable, deep);
            this.visitable.accept(voidVisitorAdapter, null);
        }

        /**
         * Effectue la recherche en profondeur des boucles dans un Visitable,
         * <p>
         * Rechercher en profondeur signifie que si un élément a été trouvé, la
         * recherche continue dans l'élément lui-même. Cela permet de trouver
         * les "boucle" imbriqués.
         *
         * @param visitable le Visitable dans lequel chercher les boucles
         */
        public LoopFinder(Visitable visitable) {
            this(visitable, false);
        }

        /**
         * Effectue la recherche des boucles dans une méthode, en profondeur ou
         * non selon l'état de {@code deep}.
         * <p>
         * Rechercher en profondeur signifie que si un élément a été trouvé, la
         * recherche continue dans l'élément lui-même. Cela permet de trouver
         * les "boucle" imbriqués.
         * <p>
         * Ne pas rechercher en profondeur signifie qu'on s'arrête au premier
         * niveau.
         * <p>
         * Attention : ne fonctionne pas en cas de paramètre générique
         *
         * @param m la méthode dans laquelle chercher les boucles
         * @param deep recherche en profondeur ou non
         */
        public LoopFinder(Method m, boolean deep) {
            this(ParserWithReflectUtilities.find(m), deep);
        }

        public LoopFinder(Method m) {
            this(ParserWithReflectUtilities.find(m), true);
        }

        public LoopFinder(Class<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public LoopFinder(Class<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public LoopFinder(Constructor<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public LoopFinder(Constructor<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), deep);
        }

        /**
         * Retourne le tableau des boucles trouvés
         *
         * @return le tableau des boucles trouvés
         */
        public ArrayList<? super Statement> getLoops() {
            return loops;
        }

        /**
         * Retourne le nombre de boucles trouvées
         *
         * @return le nombre de boucles trouvées
         */
        public int getNumberOfLoops() {
            return loops.size();
        }
    }

    /**
     * Pour extraire tous les SwitchEntry
     *
     * @author Yvan Maillot &lt;yvan.maillot@uha.fr&gt;
     */
    public static class FinderOfSwitchEntry extends Finder {

        private final ArrayList<SwitchEntry> switchEntry = new ArrayList();
        private VoidVisitorAdapter<Void> vva = new VoidVisitorAdapter<Void>() {

            @Override
            public void visit(SwitchEntry n, Void arg) {
                if (deep) {
                    super.visit(n, arg);
                }
                switchEntry.add(n);
            }
        };

        public FinderOfSwitchEntry(Method m) {
            this(ParserWithReflectUtilities.find(m), true);
        }

        public FinderOfSwitchEntry(Class<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfSwitchEntry(Class<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfSwitchEntry(Method m, boolean deep) {
            this(ParserWithReflectUtilities.find(m), deep);
        }

        public FinderOfSwitchEntry(Constructor<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfSwitchEntry(Constructor<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), deep);
        }

        /**
         * Dans ce cas, deep vaut true.
         *
         * @param visitable le Visitable dans le lequel chercher les SwitchEntry
         */
        public FinderOfSwitchEntry(Visitable visitable) {
            this(visitable, false);
        }

        /**
         * Effectue la recherche des SwitchEntry dans un Visitable, en
         * profondeur ou non selon l'état de {@code deep}.
         * <p>
         * Rechercher en profondeur signifie que si un élément a été trouvé, la
         * recherche continue dans l'élément lui-même. Cela permet de trouver
         * les "if" imbriqués par exemple.
         * <p>
         * Ne pas rechercher en profondeur signifie qu'on s'arrête au premier
         * niveau.
         * <p>
         * Cette propriété n'a pas d'intérêt pour tous les éléments
         *
         * @param visitable le Visitable dans le lequel chercher les SwitchEntry
         * @param deep recherche en profondeur ou non
         */
        public FinderOfSwitchEntry(Visitable visitable, boolean deep) {
            super(visitable, deep);
            this.visitable.accept(vva, null);
        }

        /**
         * Retourne le tableau des SwitchEntry trouvés
         *
         * @return le tableau des SwitchEntry trouvés
         */
        public ArrayList<SwitchEntry> getSwitchEntry() {
            return switchEntry;
        }

        /**
         * Retourne le nombre de SwitchEntry trouvés
         *
         * @return le nombre de SwitchEntry trouvés
         */
        public int getNumberOfSwitchEntry() {
            return switchEntry.size();
        }
    }

    /**
     * Pour extraire tous les SwitchStmt
     *
     * @author Yvan Maillot &lt;yvan.maillot@uha.fr&gt;
     */
    public static class FinderOfSwitchStmt extends Finder {

        private final ArrayList<SwitchStmt> switchStmt = new ArrayList();
        private VoidVisitorAdapter<Void> vva = new VoidVisitorAdapter<Void>() {

            @Override
            public void visit(SwitchStmt n, Void arg) {
                if (deep) {
                    super.visit(n, arg);
                }
                switchStmt.add(n);
            }
        };

        public FinderOfSwitchStmt(Method m) {
            this(ParserWithReflectUtilities.find(m), true);
        }

        public FinderOfSwitchStmt(Class<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfSwitchStmt(Class<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfSwitchStmt(Method m, boolean deep) {
            this(ParserWithReflectUtilities.find(m), deep);
        }

        public FinderOfSwitchStmt(Constructor<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfSwitchStmt(Constructor<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), deep);
        }

        /**
         * Dans ce cas, deep vaut true.
         *
         * @param visitable le Visitable dans le lequel chercher les SwitchStmt
         */
        public FinderOfSwitchStmt(Visitable visitable) {
            this(visitable, false);
        }

        /**
         * Effectue la recherche des SwitchStmt dans un Visitable, en profondeur
         * ou non selon l'état de {@code deep}.
         * <p>
         * Rechercher en profondeur signifie que si un élément a été trouvé, la
         * recherche continue dans l'élément lui-même. Cela permet de trouver
         * les "if" imbriqués par exemple.
         * <p>
         * Ne pas rechercher en profondeur signifie qu'on s'arrête au premier
         * niveau.
         * <p>
         * Cette propriété n'a pas d'intérêt pour tous les éléments
         *
         * @param visitable le Visitable dans le lequel chercher les SwitchStmt
         * @param deep recherche en profondeur ou non
         */
        public FinderOfSwitchStmt(Visitable visitable, boolean deep) {
            super(visitable, deep);
            this.visitable.accept(vva, null);
        }

        /**
         * Retourne le tableau des SwitchStmt trouvés
         *
         * @return le tableau des SwitchStmt trouvés
         */
        public ArrayList<SwitchStmt> getSwitchStmt() {
            return switchStmt;
        }

        /**
         * Retourne le nombre de SwitchStmt trouvés
         *
         * @return le nombre de SwitchStmt trouvés
         */
        public int getNumberOfSwitchStmt() {
            return switchStmt.size();
        }
    }

    /**
     * Pour extraire tous les SynchronizedStmt
     *
     * @author Yvan Maillot &lt;yvan.maillot@uha.fr&gt;
     */
    public static class FinderOfSynchronizedStmt extends Finder {

        private final ArrayList<SynchronizedStmt> synchronizedStmt = new ArrayList();
        private VoidVisitorAdapter<Void> vva = new VoidVisitorAdapter<Void>() {

            @Override
            public void visit(SynchronizedStmt n, Void arg) {
                if (deep) {
                    super.visit(n, arg);
                }
                synchronizedStmt.add(n);
            }
        };

        public FinderOfSynchronizedStmt(Method m) {
            this(ParserWithReflectUtilities.find(m), true);
        }

        public FinderOfSynchronizedStmt(Class<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfSynchronizedStmt(Class<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfSynchronizedStmt(Method m, boolean deep) {
            this(ParserWithReflectUtilities.find(m), deep);
        }

        public FinderOfSynchronizedStmt(Constructor<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfSynchronizedStmt(Constructor<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), deep);
        }

        /**
         * Dans ce cas, deep vaut true.
         *
         * @param visitable le Visitable dans le lequel chercher les
         * SynchronizedStmt
         */
        public FinderOfSynchronizedStmt(Visitable visitable) {
            this(visitable, false);
        }

        /**
         * Effectue la recherche des SynchronizedStmt dans un Visitable, en
         * profondeur ou non selon l'état de {@code deep}.
         * <p>
         * Rechercher en profondeur signifie que si un élément a été trouvé, la
         * recherche continue dans l'élément lui-même. Cela permet de trouver
         * les "if" imbriqués par exemple.
         * <p>
         * Ne pas rechercher en profondeur signifie qu'on s'arrête au premier
         * niveau.
         * <p>
         * Cette propriété n'a pas d'intérêt pour tous les éléments
         *
         * @param visitable le Visitable dans le lequel chercher les
         * SynchronizedStmt
         * @param deep recherche en profondeur ou non
         */
        public FinderOfSynchronizedStmt(Visitable visitable, boolean deep) {
            super(visitable, deep);
            this.visitable.accept(vva, null);
        }

        /**
         * Retourne le tableau des SynchronizedStmt trouvés
         *
         * @return le tableau des SynchronizedStmt trouvés
         */
        public ArrayList<SynchronizedStmt> getSynchronizedStmt() {
            return synchronizedStmt;
        }

        /**
         * Retourne le nombre de SynchronizedStmt trouvés
         *
         * @return le nombre de SynchronizedStmt trouvés
         */
        public int getNumberOfSynchronizedStmt() {
            return synchronizedStmt.size();
        }
    }

    /**
     * Pour extraire tous les SuperExpr
     *
     * @author Yvan Maillot &lt;yvan.maillot@uha.fr&gt;
     */
    public static class FinderOfSuperExpr extends Finder {

        private final ArrayList<SuperExpr> superExpr = new ArrayList();
        private VoidVisitorAdapter<Void> vva = new VoidVisitorAdapter<Void>() {

            @Override
            public void visit(SuperExpr n, Void arg) {
                if (deep) {
                    super.visit(n, arg);
                }
                superExpr.add(n);
            }
        };

        public FinderOfSuperExpr(Method m) {
            this(ParserWithReflectUtilities.find(m), true);
        }

        public FinderOfSuperExpr(Class<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfSuperExpr(Class<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfSuperExpr(Method m, boolean deep) {
            this(ParserWithReflectUtilities.find(m), deep);
        }

        public FinderOfSuperExpr(Constructor<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfSuperExpr(Constructor<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), deep);
        }

        /**
         * Dans ce cas, deep vaut true.
         *
         * @param visitable le Visitable dans le lequel chercher les SuperExpr
         */
        public FinderOfSuperExpr(Visitable visitable) {
            this(visitable, false);
        }

        /**
         * Effectue la recherche des SuperExpr dans un Visitable, en profondeur
         * ou non selon l'état de {@code deep}.
         * <p>
         * Rechercher en profondeur signifie que si un élément a été trouvé, la
         * recherche continue dans l'élément lui-même. Cela permet de trouver
         * les "if" imbriqués par exemple.
         * <p>
         * Ne pas rechercher en profondeur signifie qu'on s'arrête au premier
         * niveau.
         * <p>
         * Cette propriété n'a pas d'intérêt pour tous les éléments
         *
         * @param visitable le Visitable dans le lequel chercher les SuperExpr
         * @param deep recherche en profondeur ou non
         */
        public FinderOfSuperExpr(Visitable visitable, boolean deep) {
            super(visitable, deep);
            this.visitable.accept(vva, null);
        }

        /**
         * Retourne le tableau des SuperExpr trouvés
         *
         * @return le tableau des SuperExpr trouvés
         */
        public ArrayList<SuperExpr> getSuperExpr() {
            return superExpr;
        }

        /**
         * Retourne le nombre de SuperExpr trouvés
         *
         * @return le nombre de SuperExpr trouvés
         */
        public int getNumberOfSuperExpr() {
            return superExpr.size();
        }
    }

    /**
     * Pour extraire tous les StringLiteralExpr
     *
     * @author Yvan Maillot &lt;yvan.maillot@uha.fr&gt;
     */
    public static class FinderOfStringLiteralExpr extends Finder {

        private final ArrayList<StringLiteralExpr> stringLiteralExpr = new ArrayList();
        private final VoidVisitorAdapter<Void> vva = new VoidVisitorAdapter<>() {

            @Override
            public void visit(StringLiteralExpr n, Void arg) {
                if (deep) {
                    super.visit(n, arg);
                }
                stringLiteralExpr.add(n);
            }
        };

        public FinderOfStringLiteralExpr(Method m) {
            this(ParserWithReflectUtilities.find(m), true);
        }

        public FinderOfStringLiteralExpr(Class<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfStringLiteralExpr(Class<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfStringLiteralExpr(Method m, boolean deep) {
            this(ParserWithReflectUtilities.find(m), deep);
        }

        public FinderOfStringLiteralExpr(Constructor<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfStringLiteralExpr(Constructor<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), deep);
        }

        /**
         * Dans ce cas, deep vaut true.
         *
         * @param visitable le Visitable dans le lequel chercher les
         * StringLiteralExpr
         */
        public FinderOfStringLiteralExpr(Visitable visitable) {
            this(visitable, false);
        }

        /**
         * Effectue la recherche des StringLiteralExpr dans un Visitable, en
         * profondeur ou non selon l'état de {@code deep}.
         * <p>
         * Rechercher en profondeur signifie que si un élément a été trouvé, la
         * recherche continue dans l'élément lui-même. Cela permet de trouver
         * les "if" imbriqués par exemple.
         * <p>
         * Ne pas rechercher en profondeur signifie qu'on s'arrête au premier
         * niveau.
         * <p>
         * Cette propriété n'a pas d'intérêt pour tous les éléments
         *
         * @param visitable le Visitable dans le lequel chercher les
         * StringLiteralExpr
         * @param deep recherche en profondeur ou non
         */
        public FinderOfStringLiteralExpr(Visitable visitable, boolean deep) {
            super(visitable, deep);
            this.visitable.accept(vva, null);
        }

        /**
         * Retourne le tableau des StringLiteralExpr trouvés
         *
         * @return le tableau des StringLiteralExpr trouvés
         */
        public ArrayList<StringLiteralExpr> getStringLiteralExpr() {
            return stringLiteralExpr;
        }

        /**
         * Retourne le nombre de StringLiteralExpr trouvés
         *
         * @return le nombre de StringLiteralExpr trouvés
         */
        public int getNumberOfStringLiteralExpr() {
            return stringLiteralExpr.size();
        }
    }

    /**
     * Pour extraire tous les SingleMemberAnnotationExpr
     *
     * @author Yvan Maillot &lt;yvan.maillot@uha.fr&gt;
     */
    public static class FinderOfSingleMemberAnnotationExpr extends Finder {

        private final ArrayList<SingleMemberAnnotationExpr> singleMemberAnnotationExpr = new ArrayList();
        private VoidVisitorAdapter<Void> vva = new VoidVisitorAdapter<Void>() {

            @Override
            public void visit(SingleMemberAnnotationExpr n, Void arg) {
                if (deep) {
                    super.visit(n, arg);
                }
                singleMemberAnnotationExpr.add(n);
            }
        };

        public FinderOfSingleMemberAnnotationExpr(Method m) {
            this(ParserWithReflectUtilities.find(m), true);
        }

        public FinderOfSingleMemberAnnotationExpr(Class<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfSingleMemberAnnotationExpr(Class<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfSingleMemberAnnotationExpr(Method m, boolean deep) {
            this(ParserWithReflectUtilities.find(m), deep);
        }

        public FinderOfSingleMemberAnnotationExpr(Constructor<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfSingleMemberAnnotationExpr(Constructor<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), deep);
        }

        /**
         * Dans ce cas, deep vaut true.
         *
         * @param visitable le Visitable dans le lequel chercher les
         * SingleMemberAnnotationExpr
         */
        public FinderOfSingleMemberAnnotationExpr(Visitable visitable) {
            this(visitable, false);
        }

        /**
         * Effectue la recherche des SingleMemberAnnotationExpr dans un
         * Visitable, en profondeur ou non selon l'état de {@code deep}.
         * <p>
         * Rechercher en profondeur signifie que si un élément a été trouvé, la
         * recherche continue dans l'élément lui-même. Cela permet de trouver
         * les "if" imbriqués par exemple.
         * <p>
         * Ne pas rechercher en profondeur signifie qu'on s'arrête au premier
         * niveau.
         * <p>
         * Cette propriété n'a pas d'intérêt pour tous les éléments
         *
         * @param visitable le Visitable dans le lequel chercher les
         * SingleMemberAnnotationExpr
         * @param deep recherche en profondeur ou non
         */
        public FinderOfSingleMemberAnnotationExpr(Visitable visitable, boolean deep) {
            super(visitable, deep);
            this.visitable.accept(vva, null);
        }

        /**
         * Retourne le tableau des SingleMemberAnnotationExpr trouvés
         *
         * @return le tableau des SingleMemberAnnotationExpr trouvés
         */
        public ArrayList<SingleMemberAnnotationExpr> getSingleMemberAnnotationExpr() {
            return singleMemberAnnotationExpr;
        }

        /**
         * Retourne le nombre de SingleMemberAnnotationExpr trouvés
         *
         * @return le nombre de SingleMemberAnnotationExpr trouvés
         */
        public int getNumberOfSingleMemberAnnotationExpr() {
            return singleMemberAnnotationExpr.size();
        }
    }

    /**
     * Pour extraire tous les UnaryExpr
     *
     * @author Yvan Maillot &lt;yvan.maillot@uha.fr&gt;
     */
    public static class FinderOfUnaryExpr extends Finder {

        private final ArrayList<UnaryExpr> unaryExpr = new ArrayList();
        private final VoidVisitorAdapter<Void> vva = new VoidVisitorAdapter<>() {

            @Override
            public void visit(UnaryExpr n, Void arg) {
                if (deep) {
                    super.visit(n, arg);
                }
                unaryExpr.add(n);
            }
        };

        public FinderOfUnaryExpr(Method m) {
            this(ParserWithReflectUtilities.find(m), true);
        }

        public FinderOfUnaryExpr(Class<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfUnaryExpr(Class<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfUnaryExpr(Method m, boolean deep) {
            this(ParserWithReflectUtilities.find(m), deep);
        }

        public FinderOfUnaryExpr(Constructor<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfUnaryExpr(Constructor<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), deep);
        }

        /**
         * Dans ce cas, deep vaut true.
         *
         * @param visitable le Visitable dans le lequel chercher les UnaryExpr
         */
        public FinderOfUnaryExpr(Visitable visitable) {
            this(visitable, false);
        }

        /**
         * Effectue la recherche des UnaryExpr dans un Visitable, en profondeur
         * ou non selon l'état de {@code deep}.
         * <p>
         * Rechercher en profondeur signifie que si un élément a été trouvé, la
         * recherche continue dans l'élément lui-même. Cela permet de trouver
         * les "if" imbriqués par exemple.
         * <p>
         * Ne pas rechercher en profondeur signifie qu'on s'arrête au premier
         * niveau.
         * <p>
         * Cette propriété n'a pas d'intérêt pour tous les éléments
         *
         * @param visitable le Visitable dans le lequel chercher les UnaryExpr
         * @param deep recherche en profondeur ou non
         */
        public FinderOfUnaryExpr(Visitable visitable, boolean deep) {
            super(visitable, deep);
            this.visitable.accept(vva, null);
        }

        /**
         * Retourne le tableau des UnaryExpr trouvés
         *
         * @return le tableau des UnaryExpr trouvés
         */
        public ArrayList<UnaryExpr> getUnaryExpr() {
            return unaryExpr;
        }

        /**
         * Retourne le nombre de UnaryExpr trouvés
         *
         * @return le nombre de UnaryExpr trouvés
         */
        public int getNumberOfUnaryExpr() {
            return unaryExpr.size();
        }
    }

    /**
     * Pour extraire tous les TypeParameter
     *
     * @author Yvan Maillot &lt;yvan.maillot@uha.fr&gt;
     */
    public static class FinderOfTypeParameter extends Finder {

        private final ArrayList<TypeParameter> typeParameter = new ArrayList<>();
        private final VoidVisitorAdapter<Void> vva = new VoidVisitorAdapter<>() {

            @Override
            public void visit(TypeParameter n, Void arg) {
                if (deep) {
                    super.visit(n, arg);
                }
                typeParameter.add(n);
            }
        };

        public FinderOfTypeParameter(Method m) {
            this(ParserWithReflectUtilities.find(m), true);
        }

        public FinderOfTypeParameter(Class<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfTypeParameter(Class<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfTypeParameter(Method m, boolean deep) {
            this(ParserWithReflectUtilities.find(m), deep);
        }

        public FinderOfTypeParameter(Constructor<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfTypeParameter(Constructor<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), deep);
        }

        /**
         * Dans ce cas, deep vaut true.
         *
         * @param visitable le Visitable dans le lequel chercher les
         * TypeParameter
         */
        public FinderOfTypeParameter(Visitable visitable) {
            this(visitable, false);
        }

        /**
         * Effectue la recherche des TypeParameter dans un Visitable, en
         * profondeur ou non selon l'état de {@code deep}.
         * <p>
         * Rechercher en profondeur signifie que si un élément a été trouvé, la
         * recherche continue dans l'élément lui-même. Cela permet de trouver
         * les "if" imbriqués par exemple.
         * <p>
         * Ne pas rechercher en profondeur signifie qu'on s'arrête au premier
         * niveau.
         * <p>
         * Cette propriété n'a pas d'intérêt pour tous les éléments
         *
         * @param visitable le Visitable dans le lequel chercher les
         * TypeParameter
         * @param deep recherche en profondeur ou non
         */
        public FinderOfTypeParameter(Visitable visitable, boolean deep) {
            super(visitable, deep);
            this.visitable.accept(vva, null);
        }

        /**
         * Retourne le tableau des TypeParameter trouvés
         *
         * @return le tableau des TypeParameter trouvés
         */
        public ArrayList<TypeParameter> getTypeParameter() {
            return typeParameter;
        }

        /**
         * Retourne le nombre de TypeParameter trouvés
         *
         * @return le nombre de TypeParameter trouvés
         */
        public int getNumberOfTypeParameter() {
            return typeParameter.size();
        }
    }

    /**
     * Pour extraire tous les LocalClassDeclarationStmt
     *
     * @author Yvan Maillot &lt;yvan.maillot@uha.fr&gt;
     */
    public static class FinderOfLocalClassDeclarationStmt extends Finder {

        private final ArrayList<LocalClassDeclarationStmt> localClassDeclarationStmt = new ArrayList();
        private final VoidVisitorAdapter<Void> vva = new VoidVisitorAdapter<>() {

            @Override
            public void visit(LocalClassDeclarationStmt n, Void arg) {
                if (deep) {
                    super.visit(n, arg);
                }
                localClassDeclarationStmt.add(n);
            }
        };

        public FinderOfLocalClassDeclarationStmt(Method m) {
            this(ParserWithReflectUtilities.find(m), true);
        }

        public FinderOfLocalClassDeclarationStmt(Class<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfLocalClassDeclarationStmt(Class<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfLocalClassDeclarationStmt(Method m, boolean deep) {
            this(ParserWithReflectUtilities.find(m), deep);
        }

        public FinderOfLocalClassDeclarationStmt(Constructor<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfLocalClassDeclarationStmt(Constructor<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), deep);
        }

        /**
         * Dans ce cas, deep vaut true.
         *
         * @param visitable le Visitable dans le lequel chercher les
         * LocalClassDeclarationStmt
         */
        public FinderOfLocalClassDeclarationStmt(Visitable visitable) {
            this(visitable, false);
        }

        /**
         * Effectue la recherche des LocalClassDeclarationStmt dans un
         * Visitable, en profondeur ou non selon l'état de {@code deep}.
         * <p>
         * Rechercher en profondeur signifie que si un élément a été trouvé, la
         * recherche continue dans l'élément lui-même. Cela permet de trouver
         * les "if" imbriqués par exemple.
         * <p>
         * Ne pas rechercher en profondeur signifie qu'on s'arrête au premier
         * niveau.
         * <p>
         * Cette propriété n'a pas d'intérêt pour tous les éléments
         *
         * @param visitable le Visitable dans le lequel chercher les
         * LocalClassDeclarationStmt
         * @param deep recherche en profondeur ou non
         */
        public FinderOfLocalClassDeclarationStmt(Visitable visitable, boolean deep) {
            super(visitable, deep);
            this.visitable.accept(vva, null);
        }

        /**
         * Retourne le tableau des LocalClassDeclarationStmt trouvés
         *
         * @return le tableau des LocalClassDeclarationStmt trouvés
         */
        public ArrayList<LocalClassDeclarationStmt> getLocalClassDeclarationStmt() {
            return localClassDeclarationStmt;
        }

        /**
         * Retourne le nombre de LocalClassDeclarationStmt trouvés
         *
         * @return le nombre de LocalClassDeclarationStmt trouvés
         */
        public int getNumberOfLocalClassDeclarationStmt() {
            return localClassDeclarationStmt.size();
        }
    }

    /**
     * Pour extraire tous les TryStmt
     *
     * @author Yvan Maillot &lt;yvan.maillot@uha.fr&gt;
     */
    public static class FinderOfTryStmt extends Finder {

        private final ArrayList<TryStmt> tryStmt = new ArrayList();
        private final VoidVisitorAdapter<Void> vva = new VoidVisitorAdapter<>() {

            @Override
            public void visit(TryStmt n, Void arg) {
                if (deep) {
                    super.visit(n, arg);
                }
                tryStmt.add(n);
            }
        };

        public FinderOfTryStmt(Method m) {
            this(ParserWithReflectUtilities.find(m), true);
        }

        public FinderOfTryStmt(Class<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfTryStmt(Class<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfTryStmt(Method m, boolean deep) {
            this(ParserWithReflectUtilities.find(m), deep);
        }

        public FinderOfTryStmt(Constructor<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfTryStmt(Constructor<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), deep);
        }

        /**
         * Dans ce cas, deep vaut true.
         *
         * @param visitable le Visitable dans le lequel chercher les TryStmt
         */
        public FinderOfTryStmt(Visitable visitable) {
            this(visitable, false);
        }

        /**
         * Effectue la recherche des TryStmt dans un Visitable, en profondeur ou
         * non selon l'état de {@code deep}.
         * <p>
         * Rechercher en profondeur signifie que si un élément a été trouvé, la
         * recherche continue dans l'élément lui-même. Cela permet de trouver
         * les "if" imbriqués par exemple.
         * <p>
         * Ne pas rechercher en profondeur signifie qu'on s'arrête au premier
         * niveau.
         * <p>
         * Cette propriété n'a pas d'intérêt pour tous les éléments
         *
         * @param visitable le Visitable dans le lequel chercher les TryStmt
         * @param deep recherche en profondeur ou non
         */
        public FinderOfTryStmt(Visitable visitable, boolean deep) {
            super(visitable, deep);
            this.visitable.accept(vva, null);
        }

        /**
         * Retourne le tableau des TryStmt trouvés
         *
         * @return le tableau des TryStmt trouvés
         */
        public ArrayList<TryStmt> getTryStmt() {
            return tryStmt;
        }

        /**
         * Retourne le nombre de TryStmt trouvés
         *
         * @return le nombre de TryStmt trouvés
         */
        public int getNumberOfTryStmt() {
            return tryStmt.size();
        }
    }

    /**
     * Pour extraire tous les ThrowStmt
     *
     * @author Yvan Maillot &lt;yvan.maillot@uha.fr&gt;
     */
    public static class FinderOfThrowStmt extends Finder {

        private final ArrayList<ThrowStmt> throwStmt = new ArrayList<>();
        private final VoidVisitorAdapter<Void> vva = new VoidVisitorAdapter<>() {

            @Override
            public void visit(ThrowStmt n, Void arg) {
                if (deep) {
                    super.visit(n, arg);
                }
                throwStmt.add(n);
            }
        };

        public FinderOfThrowStmt(Method m) {
            this(ParserWithReflectUtilities.find(m), true);
        }

        public FinderOfThrowStmt(Class<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfThrowStmt(Class<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfThrowStmt(Method m, boolean deep) {
            this(ParserWithReflectUtilities.find(m), deep);
        }

        public FinderOfThrowStmt(Constructor<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfThrowStmt(Constructor<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), deep);
        }

        /**
         * Dans ce cas, deep vaut true.
         *
         * @param visitable le Visitable dans le lequel chercher les ThrowStmt
         */
        public FinderOfThrowStmt(Visitable visitable) {
            this(visitable, false);
        }

        /**
         * Effectue la recherche des ThrowStmt dans un Visitable, en profondeur
         * ou non selon l'état de {@code deep}.
         * <p>
         * Rechercher en profondeur signifie que si un élément a été trouvé, la
         * recherche continue dans l'élément lui-même. Cela permet de trouver
         * les "if" imbriqués par exemple.
         * <p>
         * Ne pas rechercher en profondeur signifie qu'on s'arrête au premier
         * niveau.
         * <p>
         * Cette propriété n'a pas d'intérêt pour tous les éléments
         *
         * @param visitable le Visitable dans le lequel chercher les ThrowStmt
         * @param deep recherche en profondeur ou non
         */
        public FinderOfThrowStmt(Visitable visitable, boolean deep) {
            super(visitable, deep);
            this.visitable.accept(vva, null);
        }

        /**
         * Retourne le tableau des ThrowStmt trouvés
         *
         * @return le tableau des ThrowStmt trouvés
         */
        public ArrayList<ThrowStmt> getThrowStmt() {
            return throwStmt;
        }

        /**
         * Retourne le nombre de ThrowStmt trouvés
         *
         * @return le nombre de ThrowStmt trouvés
         */
        public int getNumberOfThrowStmt() {
            return throwStmt.size();
        }
    }

    /**
     * Pour extraire tous les ThisExpr
     *
     * @author Yvan Maillot &lt;yvan.maillot@uha.fr&gt;
     */
    public static class FinderOfThisExpr extends Finder {

        private final ArrayList<ThisExpr> thisExpr = new ArrayList();
        private final VoidVisitorAdapter<Void> vva = new VoidVisitorAdapter<>() {

            @Override
            public void visit(ThisExpr n, Void arg) {
                if (deep) {
                    super.visit(n, arg);
                }
                thisExpr.add(n);
            }
        };

        public FinderOfThisExpr(Method m) {
            this(ParserWithReflectUtilities.find(m), true);
        }

        public FinderOfThisExpr(Class<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfThisExpr(Class<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfThisExpr(Method m, boolean deep) {
            this(ParserWithReflectUtilities.find(m), deep);
        }

        public FinderOfThisExpr(Constructor<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfThisExpr(Constructor<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), deep);
        }

        /**
         * Dans ce cas, deep vaut true.
         *
         * @param visitable le Visitable dans le lequel chercher les ThisExpr
         */
        public FinderOfThisExpr(Visitable visitable) {
            this(visitable, false);
        }

        /**
         * Effectue la recherche des ThisExpr dans un Visitable, en profondeur
         * ou non selon l'état de {@code deep}.
         * <p>
         * Rechercher en profondeur signifie que si un élément a été trouvé, la
         * recherche continue dans l'élément lui-même. Cela permet de trouver
         * les "if" imbriqués par exemple.
         * <p>
         * Ne pas rechercher en profondeur signifie qu'on s'arrête au premier
         * niveau.
         * <p>
         * Cette propriété n'a pas d'intérêt pour tous les éléments
         *
         * @param visitable le Visitable dans le lequel chercher les ThisExpr
         * @param deep recherche en profondeur ou non
         */
        public FinderOfThisExpr(Visitable visitable, boolean deep) {
            super(visitable, deep);
            this.visitable.accept(vva, null);
        }

        /**
         * Retourne le tableau des ThisExpr trouvés
         *
         * @return le tableau des ThisExpr trouvés
         */
        public ArrayList<ThisExpr> getThisExpr() {
            return thisExpr;
        }

        /**
         * Retourne le nombre de ThisExpr trouvés
         *
         * @return le nombre de ThisExpr trouvés
         */
        public int getNumberOfThisExpr() {
            return thisExpr.size();
        }
    }

    /**
     * Pour extraire tous les Name
     *
     * @author Yvan Maillot &lt;yvan.maillot@uha.fr&gt;
     */
    public static class FinderOfName extends Finder {

        private final ArrayList<Name> name = new ArrayList();
        private VoidVisitorAdapter<Void> vva = new VoidVisitorAdapter<Void>() {

            @Override
            public void visit(Name n, Void arg) {
                if (deep) {
                    super.visit(n, arg);
                }
                name.add(n);
            }
        };

        public FinderOfName(Method m) {
            this(ParserWithReflectUtilities.find(m), true);
        }

        public FinderOfName(Class<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfName(Class<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfName(Method m, boolean deep) {
            this(ParserWithReflectUtilities.find(m), deep);
        }

        public FinderOfName(Constructor<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfName(Constructor<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), deep);
        }

        /**
         * Dans ce cas, deep vaut true.
         *
         * @param visitable le Visitable dans le lequel chercher les Name
         */
        public FinderOfName(Visitable visitable) {
            this(visitable, false);
        }

        /**
         * Effectue la recherche des Name dans un Visitable, en profondeur ou
         * non selon l'état de {@code deep}.
         * <p>
         * Rechercher en profondeur signifie que si un élément a été trouvé, la
         * recherche continue dans l'élément lui-même. Cela permet de trouver
         * les "if" imbriqués par exemple.
         * <p>
         * Ne pas rechercher en profondeur signifie qu'on s'arrête au premier
         * niveau.
         * <p>
         * Cette propriété n'a pas d'intérêt pour tous les éléments
         *
         * @param visitable le Visitable dans le lequel chercher les Name
         * @param deep recherche en profondeur ou non
         */
        public FinderOfName(Visitable visitable, boolean deep) {
            super(visitable, deep);
            this.visitable.accept(vva, null);
        }

        /**
         * Retourne le tableau des Name trouvés
         *
         * @return le tableau des Name trouvés
         */
        public ArrayList<Name> getName() {
            return name;
        }

        /**
         * Retourne le nombre de Name trouvés
         *
         * @return le nombre de Name trouvés
         */
        public int getNumberOfName() {
            return name.size();
        }
    }

    /**
     * Pour extraire tous les PrimitiveType
     *
     * @author Yvan Maillot &lt;yvan.maillot@uha.fr&gt;
     */
    public static class FinderOfPrimitiveType extends Finder {

        private final ArrayList<PrimitiveType> primitiveType = new ArrayList();
        private final VoidVisitorAdapter<Void> vva = new VoidVisitorAdapter<>() {

            @Override
            public void visit(PrimitiveType n, Void arg) {
                if (deep) {
                    super.visit(n, arg);
                }
                primitiveType.add(n);
            }
        };

        public FinderOfPrimitiveType(Method m) {
            this(ParserWithReflectUtilities.find(m), true);
        }

        public FinderOfPrimitiveType(Class<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfPrimitiveType(Class<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfPrimitiveType(Method m, boolean deep) {
            this(ParserWithReflectUtilities.find(m), deep);
        }

        public FinderOfPrimitiveType(Constructor<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfPrimitiveType(Constructor<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), deep);
        }

        /**
         * Dans ce cas, deep vaut true.
         *
         * @param visitable le Visitable dans le lequel chercher les
         * PrimitiveType
         */
        public FinderOfPrimitiveType(Visitable visitable) {
            this(visitable, false);
        }

        /**
         * Effectue la recherche des PrimitiveType dans un Visitable, en
         * profondeur ou non selon l'état de {@code deep}.
         * <p>
         * Rechercher en profondeur signifie que si un élément a été trouvé, la
         * recherche continue dans l'élément lui-même. Cela permet de trouver
         * les "if" imbriqués par exemple.
         * <p>
         * Ne pas rechercher en profondeur signifie qu'on s'arrête au premier
         * niveau.
         * <p>
         * Cette propriété n'a pas d'intérêt pour tous les éléments
         *
         * @param visitable le Visitable dans le lequel chercher les
         * PrimitiveType
         * @param deep recherche en profondeur ou non
         */
        public FinderOfPrimitiveType(Visitable visitable, boolean deep) {
            super(visitable, deep);
            this.visitable.accept(vva, null);
        }

        /**
         * Retourne le tableau des PrimitiveType trouvés
         *
         * @return le tableau des PrimitiveType trouvés
         */
        public ArrayList<PrimitiveType> getPrimitiveType() {
            return primitiveType;
        }

        /**
         * Retourne le nombre de PrimitiveType trouvés
         *
         * @return le nombre de PrimitiveType trouvés
         */
        public int getNumberOfPrimitiveType() {
            return primitiveType.size();
        }
    }

    /**
     * Pour extraire tous les Parameter
     *
     * @author Yvan Maillot &lt;yvan.maillot@uha.fr&gt;
     */
    public static class FinderOfParameter extends Finder {

        private final ArrayList<Parameter> parameter = new ArrayList();
        private final VoidVisitorAdapter<Void> vva = new VoidVisitorAdapter<>() {

            @Override
            public void visit(Parameter n, Void arg) {
                if (deep) {
                    super.visit(n, arg);
                }
                parameter.add(n);
            }
        };

        public FinderOfParameter(Method m) {
            this(ParserWithReflectUtilities.find(m), true);
        }

        public FinderOfParameter(Class<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfParameter(Class<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfParameter(Method m, boolean deep) {
            this(ParserWithReflectUtilities.find(m), deep);
        }

        public FinderOfParameter(Constructor<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfParameter(Constructor<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), deep);
        }

        /**
         * Dans ce cas, deep vaut true.
         *
         * @param visitable le Visitable dans le lequel chercher les Parameter
         */
        public FinderOfParameter(Visitable visitable) {
            this(visitable, false);
        }

        /**
         * Effectue la recherche des Parameter dans un Visitable, en profondeur
         * ou non selon l'état de {@code deep}.
         * <p>
         * Rechercher en profondeur signifie que si un élément a été trouvé, la
         * recherche continue dans l'élément lui-même. Cela permet de trouver
         * les "if" imbriqués par exemple.
         * <p>
         * Ne pas rechercher en profondeur signifie qu'on s'arrête au premier
         * niveau.
         * <p>
         * Cette propriété n'a pas d'intérêt pour tous les éléments
         *
         * @param visitable le Visitable dans le lequel chercher les Parameter
         * @param deep recherche en profondeur ou non
         */
        public FinderOfParameter(Visitable visitable, boolean deep) {
            super(visitable, deep);
            this.visitable.accept(vva, null);
        }

        /**
         * Retourne le tableau des Parameter trouvés
         *
         * @return le tableau des Parameter trouvés
         */
        public ArrayList<Parameter> getParameter() {
            return parameter;
        }

        /**
         * Retourne le nombre de Parameter trouvés
         *
         * @return le nombre de Parameter trouvés
         */
        public int getNumberOfParameter() {
            return parameter.size();
        }
    }

    /**
     * Pour extraire tous les PackageDeclaration
     *
     * @author Yvan Maillot &lt;yvan.maillot@uha.fr&gt;
     */
    public static class FinderOfPackageDeclaration extends Finder {

        private final ArrayList<PackageDeclaration> packageDeclaration = new ArrayList();
        private final VoidVisitorAdapter<Void> vva = new VoidVisitorAdapter<>() {

            @Override
            public void visit(PackageDeclaration n, Void arg) {
                if (deep) {
                    super.visit(n, arg);
                }
                packageDeclaration.add(n);
            }
        };

        public FinderOfPackageDeclaration(Method m) {
            this(ParserWithReflectUtilities.find(m), true);
        }

        public FinderOfPackageDeclaration(Class<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfPackageDeclaration(Class<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfPackageDeclaration(Method m, boolean deep) {
            this(ParserWithReflectUtilities.find(m), deep);
        }

        public FinderOfPackageDeclaration(Constructor<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfPackageDeclaration(Constructor<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), deep);
        }

        /**
         * Dans ce cas, deep vaut true.
         *
         * @param visitable le Visitable dans le lequel chercher les
         * PackageDeclaration
         */
        public FinderOfPackageDeclaration(Visitable visitable) {
            this(visitable, false);
        }

        /**
         * Effectue la recherche des PackageDeclaration dans un Visitable, en
         * profondeur ou non selon l'état de {@code deep}.
         * <p>
         * Rechercher en profondeur signifie que si un élément a été trouvé, la
         * recherche continue dans l'élément lui-même. Cela permet de trouver
         * les "if" imbriqués par exemple.
         * <p>
         * Ne pas rechercher en profondeur signifie qu'on s'arrête au premier
         * niveau.
         * <p>
         * Cette propriété n'a pas d'intérêt pour tous les éléments
         *
         * @param visitable le Visitable dans le lequel chercher les
         * PackageDeclaration
         * @param deep recherche en profondeur ou non
         */
        public FinderOfPackageDeclaration(Visitable visitable, boolean deep) {
            super(visitable, deep);
            this.visitable.accept(vva, null);
        }

        /**
         * Retourne le tableau des PackageDeclaration trouvés
         *
         * @return le tableau des PackageDeclaration trouvés
         */
        public ArrayList<PackageDeclaration> getPackageDeclaration() {
            return packageDeclaration;
        }

        /**
         * Retourne le nombre de PackageDeclaration trouvés
         *
         * @return le nombre de PackageDeclaration trouvés
         */
        public int getNumberOfPackageDeclaration() {
            return packageDeclaration.size();
        }
    }

    /**
     * Pour extraire tous les ObjectCreationExpr
     *
     * @author Yvan Maillot &lt;yvan.maillot@uha.fr&gt;
     */
    public static class FinderOfObjectCreationExpr extends Finder {

        private final ArrayList<ObjectCreationExpr> objectCreationExpr = new ArrayList();
        private final VoidVisitorAdapter<Void> vva = new VoidVisitorAdapter<>() {

            @Override
            public void visit(ObjectCreationExpr n, Void arg) {
                if (deep) {
                    super.visit(n, arg);
                }
                objectCreationExpr.add(n);
            }
        };

        public FinderOfObjectCreationExpr(Method m) {
            this(ParserWithReflectUtilities.find(m), true);
        }

        public FinderOfObjectCreationExpr(Class<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfObjectCreationExpr(Class<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfObjectCreationExpr(Method m, boolean deep) {
            this(ParserWithReflectUtilities.find(m), deep);
        }

        public FinderOfObjectCreationExpr(Constructor<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfObjectCreationExpr(Constructor<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), deep);
        }

        /**
         * Dans ce cas, deep vaut true.
         *
         * @param visitable le Visitable dans le lequel chercher les
         * ObjectCreationExpr
         */
        public FinderOfObjectCreationExpr(Visitable visitable) {
            this(visitable, false);
        }

        /**
         * Effectue la recherche des ObjectCreationExpr dans un Visitable, en
         * profondeur ou non selon l'état de {@code deep}.
         * <p>
         * Rechercher en profondeur signifie que si un élément a été trouvé, la
         * recherche continue dans l'élément lui-même. Cela permet de trouver
         * les "if" imbriqués par exemple.
         * <p>
         * Ne pas rechercher en profondeur signifie qu'on s'arrête au premier
         * niveau.
         * <p>
         * Cette propriété n'a pas d'intérêt pour tous les éléments
         *
         * @param visitable le Visitable dans le lequel chercher les
         * ObjectCreationExpr
         * @param deep recherche en profondeur ou non
         */
        public FinderOfObjectCreationExpr(Visitable visitable, boolean deep) {
            super(visitable, deep);
            this.visitable.accept(vva, null);
        }

        /**
         * Retourne le tableau des ObjectCreationExpr trouvés
         *
         * @return le tableau des ObjectCreationExpr trouvés
         */
        public ArrayList<ObjectCreationExpr> getObjectCreationExpr() {
            return objectCreationExpr;
        }

        /**
         * Retourne le nombre de ObjectCreationExpr trouvés
         *
         * @return le nombre de ObjectCreationExpr trouvés
         */
        public int getNumberOfObjectCreationExpr() {
            return objectCreationExpr.size();
        }
    }

    /**
     * Pour extraire tous les NullLiteralExpr
     *
     * @author Yvan Maillot &lt;yvan.maillot@uha.fr&gt;
     */
    public static class FinderOfNullLiteralExpr extends Finder {

        private final ArrayList<NullLiteralExpr> nullLiteralExpr = new ArrayList();
        private final VoidVisitorAdapter<Void> vva = new VoidVisitorAdapter<>() {

            @Override
            public void visit(NullLiteralExpr n, Void arg) {
                if (deep) {
                    super.visit(n, arg);
                }
                nullLiteralExpr.add(n);
            }
        };

        public FinderOfNullLiteralExpr(Method m) {
            this(ParserWithReflectUtilities.find(m), true);
        }

        public FinderOfNullLiteralExpr(Class<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfNullLiteralExpr(Class<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfNullLiteralExpr(Method m, boolean deep) {
            this(ParserWithReflectUtilities.find(m), deep);
        }

        public FinderOfNullLiteralExpr(Constructor<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfNullLiteralExpr(Constructor<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), deep);
        }

        /**
         * Dans ce cas, deep vaut true.
         *
         * @param visitable le Visitable dans le lequel chercher les
         * NullLiteralExpr
         */
        public FinderOfNullLiteralExpr(Visitable visitable) {
            this(visitable, false);
        }

        /**
         * Effectue la recherche des NullLiteralExpr dans un Visitable, en
         * profondeur ou non selon l'état de {@code deep}.
         * <p>
         * Rechercher en profondeur signifie que si un élément a été trouvé, la
         * recherche continue dans l'élément lui-même. Cela permet de trouver
         * les "if" imbriqués par exemple.
         * <p>
         * Ne pas rechercher en profondeur signifie qu'on s'arrête au premier
         * niveau.
         * <p>
         * Cette propriété n'a pas d'intérêt pour tous les éléments
         *
         * @param visitable le Visitable dans le lequel chercher les
         * NullLiteralExpr
         * @param deep recherche en profondeur ou non
         */
        public FinderOfNullLiteralExpr(Visitable visitable, boolean deep) {
            super(visitable, deep);
            this.visitable.accept(vva, null);
        }

        /**
         * Retourne le tableau des NullLiteralExpr trouvés
         *
         * @return le tableau des NullLiteralExpr trouvés
         */
        public ArrayList<NullLiteralExpr> getNullLiteralExpr() {
            return nullLiteralExpr;
        }

        /**
         * Retourne le nombre de NullLiteralExpr trouvés
         *
         * @return le nombre de NullLiteralExpr trouvés
         */
        public int getNumberOfNullLiteralExpr() {
            return nullLiteralExpr.size();
        }
    }

    /**
     * Pour extraire tous les ReturnStmt
     *
     * @author Yvan Maillot &lt;yvan.maillot@uha.fr&gt;
     */
    public static class FinderOfReturnStmt extends Finder {

        private final ArrayList<ReturnStmt> returnStmt = new ArrayList();
        private final VoidVisitorAdapter<Void> vva = new VoidVisitorAdapter<>() {

            @Override
            public void visit(ReturnStmt n, Void arg) {
                if (deep) {
                    super.visit(n, arg);
                }
                returnStmt.add(n);
            }
        };

        public FinderOfReturnStmt(Method m) {
            this(ParserWithReflectUtilities.find(m), true);
        }

        public FinderOfReturnStmt(Class<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfReturnStmt(Class<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfReturnStmt(Method m, boolean deep) {
            this(ParserWithReflectUtilities.find(m), deep);
        }

        public FinderOfReturnStmt(Constructor<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfReturnStmt(Constructor<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), deep);
        }

        /**
         * Dans ce cas, deep vaut true.
         *
         * @param visitable le Visitable dans le lequel chercher les ReturnStmt
         */
        public FinderOfReturnStmt(Visitable visitable) {
            this(visitable, false);
        }

        /**
         * Effectue la recherche des ReturnStmt dans un Visitable, en profondeur
         * ou non selon l'état de {@code deep}.
         * <p>
         * Rechercher en profondeur signifie que si un élément a été trouvé, la
         * recherche continue dans l'élément lui-même. Cela permet de trouver
         * les "if" imbriqués par exemple.
         * <p>
         * Ne pas rechercher en profondeur signifie qu'on s'arrête au premier
         * niveau.
         * <p>
         * Cette propriété n'a pas d'intérêt pour tous les éléments
         *
         * @param visitable le Visitable dans le lequel chercher les ReturnStmt
         * @param deep recherche en profondeur ou non
         */
        public FinderOfReturnStmt(Visitable visitable, boolean deep) {
            super(visitable, deep);
            this.visitable.accept(vva, null);
        }

        /**
         * Retourne le tableau des ReturnStmt trouvés
         *
         * @return le tableau des ReturnStmt trouvés
         */
        public ArrayList<ReturnStmt> getReturnStmt() {
            return returnStmt;
        }

        /**
         * Retourne le nombre de ReturnStmt trouvés
         *
         * @return le nombre de ReturnStmt trouvés
         */
        public int getNumberOfReturnStmt() {
            return returnStmt.size();
        }
    }

    /**
     * Pour extraire tous les UnionType
     *
     * @author Yvan Maillot &lt;yvan.maillot@uha.fr&gt;
     */
    public static class FinderOfUnionType extends Finder {

        private final ArrayList<UnionType> unionType = new ArrayList();
        private final VoidVisitorAdapter<Void> vva = new VoidVisitorAdapter<>() {

            @Override
            public void visit(UnionType n, Void arg) {
                if (deep) {
                    super.visit(n, arg);
                }
                unionType.add(n);
            }
        };

        public FinderOfUnionType(Method m) {
            this(ParserWithReflectUtilities.find(m), true);
        }

        public FinderOfUnionType(Class<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfUnionType(Class<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfUnionType(Method m, boolean deep) {
            this(ParserWithReflectUtilities.find(m), deep);
        }

        public FinderOfUnionType(Constructor<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfUnionType(Constructor<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), deep);
        }

        /**
         * Dans ce cas, deep vaut true.
         *
         * @param visitable le Visitable dans le lequel chercher les UnionType
         */
        public FinderOfUnionType(Visitable visitable) {
            this(visitable, false);
        }

        /**
         * Effectue la recherche des UnionType dans un Visitable, en profondeur
         * ou non selon l'état de {@code deep}.
         * <p>
         * Rechercher en profondeur signifie que si un élément a été trouvé, la
         * recherche continue dans l'élément lui-même. Cela permet de trouver
         * les "if" imbriqués par exemple.
         * <p>
         * Ne pas rechercher en profondeur signifie qu'on s'arrête au premier
         * niveau.
         * <p>
         * Cette propriété n'a pas d'intérêt pour tous les éléments
         *
         * @param visitable le Visitable dans le lequel chercher les UnionType
         * @param deep recherche en profondeur ou non
         */
        public FinderOfUnionType(Visitable visitable, boolean deep) {
            super(visitable, deep);
            this.visitable.accept(vva, null);
        }

        /**
         * Retourne le tableau des UnionType trouvés
         *
         * @return le tableau des UnionType trouvés
         */
        public ArrayList<UnionType> getUnionType() {
            return unionType;
        }

        /**
         * Retourne le nombre de UnionType trouvés
         *
         * @return le nombre de UnionType trouvés
         */
        public int getNumberOfUnionType() {
            return unionType.size();
        }
    }

    /**
     * Pour extraire tous les IntersectionType
     *
     * @author Yvan Maillot &lt;yvan.maillot@uha.fr&gt;
     */
    public static class FinderOfIntersectionType extends Finder {

        private final ArrayList<IntersectionType> intersectionType = new ArrayList();
        private final VoidVisitorAdapter<Void> vva = new VoidVisitorAdapter<>() {

            @Override
            public void visit(IntersectionType n, Void arg) {
                if (deep) {
                    super.visit(n, arg);
                }
                intersectionType.add(n);
            }
        };

        public FinderOfIntersectionType(Method m) {
            this(ParserWithReflectUtilities.find(m), true);
        }

        public FinderOfIntersectionType(Class<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfIntersectionType(Class<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfIntersectionType(Method m, boolean deep) {
            this(ParserWithReflectUtilities.find(m), deep);
        }

        public FinderOfIntersectionType(Constructor<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfIntersectionType(Constructor<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), deep);
        }

        /**
         * Dans ce cas, deep vaut true.
         *
         * @param visitable le Visitable dans le lequel chercher les
         * IntersectionType
         */
        public FinderOfIntersectionType(Visitable visitable) {
            this(visitable, false);
        }

        /**
         * Effectue la recherche des IntersectionType dans un Visitable, en
         * profondeur ou non selon l'état de {@code deep}.
         * <p>
         * Rechercher en profondeur signifie que si un élément a été trouvé, la
         * recherche continue dans l'élément lui-même. Cela permet de trouver
         * les "if" imbriqués par exemple.
         * <p>
         * Ne pas rechercher en profondeur signifie qu'on s'arrête au premier
         * niveau.
         * <p>
         * Cette propriété n'a pas d'intérêt pour tous les éléments
         *
         * @param visitable le Visitable dans le lequel chercher les
         * IntersectionType
         * @param deep recherche en profondeur ou non
         */
        public FinderOfIntersectionType(Visitable visitable, boolean deep) {
            super(visitable, deep);
            this.visitable.accept(vva, null);
        }

        /**
         * Retourne le tableau des IntersectionType trouvés
         *
         * @return le tableau des IntersectionType trouvés
         */
        public ArrayList<IntersectionType> getIntersectionType() {
            return intersectionType;
        }

        /**
         * Retourne le nombre de IntersectionType trouvés
         *
         * @return le nombre de IntersectionType trouvés
         */
        public int getNumberOfIntersectionType() {
            return intersectionType.size();
        }
    }

    /**
     * Pour extraire tous les ArrayCreationLevel
     *
     * @author Yvan Maillot &lt;yvan.maillot@uha.fr&gt;
     */
    public static class FinderOfArrayCreationLevel extends Finder {

        private final ArrayList<ArrayCreationLevel> arrayCreationLevel = new ArrayList();
        private final VoidVisitorAdapter<Void> vva = new VoidVisitorAdapter<>() {

            @Override
            public void visit(ArrayCreationLevel n, Void arg) {
                if (deep) {
                    super.visit(n, arg);
                }
                arrayCreationLevel.add(n);
            }
        };

        public FinderOfArrayCreationLevel(Method m) {
            this(ParserWithReflectUtilities.find(m), true);
        }

        public FinderOfArrayCreationLevel(Class<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfArrayCreationLevel(Class<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfArrayCreationLevel(Method m, boolean deep) {
            this(ParserWithReflectUtilities.find(m), deep);
        }

        public FinderOfArrayCreationLevel(Constructor<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfArrayCreationLevel(Constructor<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), deep);
        }

        /**
         * Dans ce cas, deep vaut true.
         *
         * @param visitable le Visitable dans le lequel chercher les
         * ArrayCreationLevel
         */
        public FinderOfArrayCreationLevel(Visitable visitable) {
            this(visitable, false);
        }

        /**
         * Effectue la recherche des ArrayCreationLevel dans un Visitable, en
         * profondeur ou non selon l'état de {@code deep}.
         * <p>
         * Rechercher en profondeur signifie que si un élément a été trouvé, la
         * recherche continue dans l'élément lui-même. Cela permet de trouver
         * les "if" imbriqués par exemple.
         * <p>
         * Ne pas rechercher en profondeur signifie qu'on s'arrête au premier
         * niveau.
         * <p>
         * Cette propriété n'a pas d'intérêt pour tous les éléments
         *
         * @param visitable le Visitable dans le lequel chercher les
         * ArrayCreationLevel
         * @param deep recherche en profondeur ou non
         */
        public FinderOfArrayCreationLevel(Visitable visitable, boolean deep) {
            super(visitable, deep);
            this.visitable.accept(vva, null);
        }

        /**
         * Retourne le tableau des ArrayCreationLevel trouvés
         *
         * @return le tableau des ArrayCreationLevel trouvés
         */
        public ArrayList<ArrayCreationLevel> getArrayCreationLevel() {
            return arrayCreationLevel;
        }

        /**
         * Retourne le nombre de ArrayCreationLevel trouvés
         *
         * @return le nombre de ArrayCreationLevel trouvés
         */
        public int getNumberOfArrayCreationLevel() {
            return arrayCreationLevel.size();
        }
    }

    /**
     * Pour extraire tous les ArrayType
     *
     * @author Yvan Maillot &lt;yvan.maillot@uha.fr&gt;
     */
    public static class FinderOfArrayType extends Finder {

        private final ArrayList<ArrayType> arrayType = new ArrayList<>();
        private final VoidVisitorAdapter<Void> vva = new VoidVisitorAdapter<>() {

            @Override
            public void visit(ArrayType n, Void arg) {
                if (deep) {
                    super.visit(n, arg);
                }
                arrayType.add(n);
            }
        };

        public FinderOfArrayType(Method m) {
            this(ParserWithReflectUtilities.find(m), true);
        }

        public FinderOfArrayType(Class<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfArrayType(Class<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfArrayType(Method m, boolean deep) {
            this(ParserWithReflectUtilities.find(m), deep);
        }

        public FinderOfArrayType(Constructor<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfArrayType(Constructor<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), deep);
        }

        /**
         * Dans ce cas, deep vaut true.
         *
         * @param visitable le Visitable dans le lequel chercher les ArrayType
         */
        public FinderOfArrayType(Visitable visitable) {
            this(visitable, false);
        }

        /**
         * Effectue la recherche des ArrayType dans un Visitable, en profondeur
         * ou non selon l'état de {@code deep}.
         * <p>
         * Rechercher en profondeur signifie que si un élément a été trouvé, la
         * recherche continue dans l'élément lui-même. Cela permet de trouver
         * les "if" imbriqués par exemple.
         * <p>
         * Ne pas rechercher en profondeur signifie qu'on s'arrête au premier
         * niveau.
         * <p>
         * Cette propriété n'a pas d'intérêt pour tous les éléments
         *
         * @param visitable le Visitable dans le lequel chercher les ArrayType
         * @param deep recherche en profondeur ou non
         */
        public FinderOfArrayType(Visitable visitable, boolean deep) {
            super(visitable, deep);
            this.visitable.accept(vva, null);
        }

        /**
         * Retourne le tableau des ArrayType trouvés
         *
         * @return le tableau des ArrayType trouvés
         */
        public ArrayList<ArrayType> getArrayType() {
            return arrayType;
        }

        /**
         * Retourne le nombre de ArrayType trouvés
         *
         * @return le nombre de ArrayType trouvés
         */
        public int getNumberOfArrayType() {
            return arrayType.size();
        }
    }

    /**
     * Pour extraire tous les SimpleName
     *
     * @author Yvan Maillot &lt;yvan.maillot@uha.fr&gt;
     */
    public static class FinderOfSimpleName extends Finder {

        private final ArrayList<SimpleName> simpleName = new ArrayList();
        private final VoidVisitorAdapter<Void> vva = new VoidVisitorAdapter<>() {

            @Override
            public void visit(SimpleName n, Void arg) {
                if (deep) {
                    super.visit(n, arg);
                }
                simpleName.add(n);
            }
        };

        public FinderOfSimpleName(Method m) {
            this(ParserWithReflectUtilities.find(m), true);
        }

        public FinderOfSimpleName(Class<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfSimpleName(Class<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfSimpleName(Method m, boolean deep) {
            this(ParserWithReflectUtilities.find(m), deep);
        }

        public FinderOfSimpleName(Constructor<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfSimpleName(Constructor<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), deep);
        }

        /**
         * Dans ce cas, deep vaut true.
         *
         * @param visitable le Visitable dans le lequel chercher les SimpleName
         */
        public FinderOfSimpleName(Visitable visitable) {
            this(visitable, false);
        }

        /**
         * Effectue la recherche des SimpleName dans un Visitable, en profondeur
         * ou non selon l'état de {@code deep}.
         * <p>
         * Rechercher en profondeur signifie que si un élément a été trouvé, la
         * recherche continue dans l'élément lui-même. Cela permet de trouver
         * les "if" imbriqués par exemple.
         * <p>
         * Ne pas rechercher en profondeur signifie qu'on s'arrête au premier
         * niveau.
         * <p>
         * Cette propriété n'a pas d'intérêt pour tous les éléments
         *
         * @param visitable le Visitable dans le lequel chercher les SimpleName
         * @param deep recherche en profondeur ou non
         */
        public FinderOfSimpleName(Visitable visitable, boolean deep) {
            super(visitable, deep);
            this.visitable.accept(vva, null);
        }

        /**
         * Retourne le tableau des SimpleName trouvés
         *
         * @return le tableau des SimpleName trouvés
         */
        public ArrayList<SimpleName> getSimpleName() {
            return simpleName;
        }

        /**
         * Retourne le nombre de SimpleName trouvés
         *
         * @return le nombre de SimpleName trouvés
         */
        public int getNumberOfSimpleName() {
            return simpleName.size();
        }
    }

    /**
     * Pour extraire tous les UnparsableStmt
     *
     * @author Yvan Maillot &lt;yvan.maillot@uha.fr&gt;
     */
    public static class FinderOfUnparsableStmt extends Finder {

        private final ArrayList<UnparsableStmt> unparsableStmt = new ArrayList();
        private final VoidVisitorAdapter<Void> vva = new VoidVisitorAdapter<>() {

            @Override
            public void visit(UnparsableStmt n, Void arg) {
                if (deep) {
                    super.visit(n, arg);
                }
                unparsableStmt.add(n);
            }
        };

        public FinderOfUnparsableStmt(Method m) {
            this(ParserWithReflectUtilities.find(m), true);
        }

        public FinderOfUnparsableStmt(Class<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfUnparsableStmt(Class<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfUnparsableStmt(Method m, boolean deep) {
            this(ParserWithReflectUtilities.find(m), deep);
        }

        public FinderOfUnparsableStmt(Constructor<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfUnparsableStmt(Constructor<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), deep);
        }

        /**
         * Dans ce cas, deep vaut true.
         *
         * @param visitable le Visitable dans le lequel chercher les
         * UnparsableStmt
         */
        public FinderOfUnparsableStmt(Visitable visitable) {
            this(visitable, false);
        }

        /**
         * Effectue la recherche des UnparsableStmt dans un Visitable, en
         * profondeur ou non selon l'état de {@code deep}.
         * <p>
         * Rechercher en profondeur signifie que si un élément a été trouvé, la
         * recherche continue dans l'élément lui-même. Cela permet de trouver
         * les "if" imbriqués par exemple.
         * <p>
         * Ne pas rechercher en profondeur signifie qu'on s'arrête au premier
         * niveau.
         * <p>
         * Cette propriété n'a pas d'intérêt pour tous les éléments
         *
         * @param visitable le Visitable dans le lequel chercher les
         * UnparsableStmt
         * @param deep recherche en profondeur ou non
         */
        public FinderOfUnparsableStmt(Visitable visitable, boolean deep) {
            super(visitable, deep);
            this.visitable.accept(vva, null);
        }

        /**
         * Retourne le tableau des UnparsableStmt trouvés
         *
         * @return le tableau des UnparsableStmt trouvés
         */
        public ArrayList<UnparsableStmt> getUnparsableStmt() {
            return unparsableStmt;
        }

        /**
         * Retourne le nombre de UnparsableStmt trouvés
         *
         * @return le nombre de UnparsableStmt trouvés
         */
        public int getNumberOfUnparsableStmt() {
            return unparsableStmt.size();
        }
    }

    /**
     * Pour extraire tous les ModuleOpensDirective
     *
     * @author Yvan Maillot &lt;yvan.maillot@uha.fr&gt;
     */
    public static class FinderOfModuleOpensDirective extends Finder {

        private final ArrayList<ModuleOpensDirective> moduleOpensDirective = new ArrayList();
        private final VoidVisitorAdapter<Void> vva = new VoidVisitorAdapter<>() {

            @Override
            public void visit(ModuleOpensDirective n, Void arg) {
                if (deep) {
                    super.visit(n, arg);
                }
                moduleOpensDirective.add(n);
            }
        };

        public FinderOfModuleOpensDirective(Method m) {
            this(ParserWithReflectUtilities.find(m), true);
        }

        public FinderOfModuleOpensDirective(Class<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfModuleOpensDirective(Class<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfModuleOpensDirective(Method m, boolean deep) {
            this(ParserWithReflectUtilities.find(m), deep);
        }

        public FinderOfModuleOpensDirective(Constructor<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfModuleOpensDirective(Constructor<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), deep);
        }

        /**
         * Dans ce cas, deep vaut true.
         *
         * @param visitable le Visitable dans le lequel chercher les
         * ModuleOpensDirective
         */
        public FinderOfModuleOpensDirective(Visitable visitable) {
            this(visitable, false);
        }

        /**
         * Effectue la recherche des ModuleOpensDirective dans un Visitable, en
         * profondeur ou non selon l'état de {@code deep}.
         * <p>
         * Rechercher en profondeur signifie que si un élément a été trouvé, la
         * recherche continue dans l'élément lui-même. Cela permet de trouver
         * les "if" imbriqués par exemple.
         * <p>
         * Ne pas rechercher en profondeur signifie qu'on s'arrête au premier
         * niveau.
         * <p>
         * Cette propriété n'a pas d'intérêt pour tous les éléments
         *
         * @param visitable le Visitable dans le lequel chercher les
         * ModuleOpensDirective
         * @param deep recherche en profondeur ou non
         */
        public FinderOfModuleOpensDirective(Visitable visitable, boolean deep) {
            super(visitable, deep);
            this.visitable.accept(vva, null);
        }

        /**
         * Retourne le tableau des ModuleOpensDirective trouvés
         *
         * @return le tableau des ModuleOpensDirective trouvés
         */
        public ArrayList<ModuleOpensDirective> getModuleOpensDirective() {
            return moduleOpensDirective;
        }

        /**
         * Retourne le nombre de ModuleOpensDirective trouvés
         *
         * @return le nombre de ModuleOpensDirective trouvés
         */
        public int getNumberOfModuleOpensDirective() {
            return moduleOpensDirective.size();
        }
    }

    /**
     * Pour extraire tous les ModuleUsesDirective
     *
     * @author Yvan Maillot &lt;yvan.maillot@uha.fr&gt;
     */
    public static class FinderOfModuleUsesDirective extends Finder {

        private final ArrayList<ModuleUsesDirective> moduleUsesDirective = new ArrayList();
        private final VoidVisitorAdapter<Void> vva = new VoidVisitorAdapter<>() {

            @Override
            public void visit(ModuleUsesDirective n, Void arg) {
                if (deep) {
                    super.visit(n, arg);
                }
                moduleUsesDirective.add(n);
            }
        };

        public FinderOfModuleUsesDirective(Method m) {
            this(ParserWithReflectUtilities.find(m), true);
        }

        public FinderOfModuleUsesDirective(Class<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfModuleUsesDirective(Class<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfModuleUsesDirective(Method m, boolean deep) {
            this(ParserWithReflectUtilities.find(m), deep);
        }

        public FinderOfModuleUsesDirective(Constructor<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfModuleUsesDirective(Constructor<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), deep);
        }

        /**
         * Dans ce cas, deep vaut true.
         *
         * @param visitable le Visitable dans le lequel chercher les
         * ModuleUsesDirective
         */
        public FinderOfModuleUsesDirective(Visitable visitable) {
            this(visitable, false);
        }

        /**
         * Effectue la recherche des ModuleUsesDirective dans un Visitable, en
         * profondeur ou non selon l'état de {@code deep}.
         * <p>
         * Rechercher en profondeur signifie que si un élément a été trouvé, la
         * recherche continue dans l'élément lui-même. Cela permet de trouver
         * les "if" imbriqués par exemple.
         * <p>
         * Ne pas rechercher en profondeur signifie qu'on s'arrête au premier
         * niveau.
         * <p>
         * Cette propriété n'a pas d'intérêt pour tous les éléments
         *
         * @param visitable le Visitable dans le lequel chercher les
         * ModuleUsesDirective
         * @param deep recherche en profondeur ou non
         */
        public FinderOfModuleUsesDirective(Visitable visitable, boolean deep) {
            super(visitable, deep);
            this.visitable.accept(vva, null);
        }

        /**
         * Retourne le tableau des ModuleUsesDirective trouvés
         *
         * @return le tableau des ModuleUsesDirective trouvés
         */
        public ArrayList<ModuleUsesDirective> getModuleUsesDirective() {
            return moduleUsesDirective;
        }

        /**
         * Retourne le nombre de ModuleUsesDirective trouvés
         *
         * @return le nombre de ModuleUsesDirective trouvés
         */
        public int getNumberOfModuleUsesDirective() {
            return moduleUsesDirective.size();
        }
    }

    /**
     * Pour extraire tous les ModuleProvidesDirective
     *
     * @author Yvan Maillot &lt;yvan.maillot@uha.fr&gt;
     */
    public static class FinderOfModuleProvidesDirective extends Finder {

        private final ArrayList<ModuleProvidesDirective> moduleProvidesDirective = new ArrayList();
        private final VoidVisitorAdapter<Void> vva = new VoidVisitorAdapter<>() {

            @Override
            public void visit(ModuleProvidesDirective n, Void arg) {
                if (deep) {
                    super.visit(n, arg);
                }
                moduleProvidesDirective.add(n);
            }
        };

        public FinderOfModuleProvidesDirective(Method m) {
            this(ParserWithReflectUtilities.find(m), true);
        }

        public FinderOfModuleProvidesDirective(Class<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfModuleProvidesDirective(Class<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfModuleProvidesDirective(Method m, boolean deep) {
            this(ParserWithReflectUtilities.find(m), deep);
        }

        public FinderOfModuleProvidesDirective(Constructor<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfModuleProvidesDirective(Constructor<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), deep);
        }

        /**
         * Dans ce cas, deep vaut true.
         *
         * @param visitable le Visitable dans le lequel chercher les
         * ModuleProvidesDirective
         */
        public FinderOfModuleProvidesDirective(Visitable visitable) {
            this(visitable, false);
        }

        /**
         * Effectue la recherche des ModuleProvidesDirective dans un Visitable,
         * en profondeur ou non selon l'état de {@code deep}.
         * <p>
         * Rechercher en profondeur signifie que si un élément a été trouvé, la
         * recherche continue dans l'élément lui-même. Cela permet de trouver
         * les "if" imbriqués par exemple.
         * <p>
         * Ne pas rechercher en profondeur signifie qu'on s'arrête au premier
         * niveau.
         * <p>
         * Cette propriété n'a pas d'intérêt pour tous les éléments
         *
         * @param visitable le Visitable dans le lequel chercher les
         * ModuleProvidesDirective
         * @param deep recherche en profondeur ou non
         */
        public FinderOfModuleProvidesDirective(Visitable visitable, boolean deep) {
            super(visitable, deep);
            this.visitable.accept(vva, null);
        }

        /**
         * Retourne le tableau des ModuleProvidesDirective trouvés
         *
         * @return le tableau des ModuleProvidesDirective trouvés
         */
        public ArrayList<ModuleProvidesDirective> getModuleProvidesDirective() {
            return moduleProvidesDirective;
        }

        /**
         * Retourne le nombre de ModuleProvidesDirective trouvés
         *
         * @return le nombre de ModuleProvidesDirective trouvés
         */
        public int getNumberOfModuleProvidesDirective() {
            return moduleProvidesDirective.size();
        }
    }

    /**
     * Pour extraire tous les ModuleExportsDirective
     *
     * @author Yvan Maillot &lt;yvan.maillot@uha.fr&gt;
     */
    public static class FinderOfModuleExportsDirective extends Finder {

        private final ArrayList<ModuleExportsDirective> moduleExportsDirective = new ArrayList();
        private final VoidVisitorAdapter<Void> vva = new VoidVisitorAdapter<>() {

            @Override
            public void visit(ModuleExportsDirective n, Void arg) {
                if (deep) {
                    super.visit(n, arg);
                }
                moduleExportsDirective.add(n);
            }
        };

        public FinderOfModuleExportsDirective(Method m) {
            this(ParserWithReflectUtilities.find(m), true);
        }

        public FinderOfModuleExportsDirective(Class<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfModuleExportsDirective(Class<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfModuleExportsDirective(Method m, boolean deep) {
            this(ParserWithReflectUtilities.find(m), deep);
        }

        public FinderOfModuleExportsDirective(Constructor<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfModuleExportsDirective(Constructor<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), deep);
        }

        /**
         * Dans ce cas, deep vaut true.
         *
         * @param visitable le Visitable dans le lequel chercher les
         * ModuleExportsDirective
         */
        public FinderOfModuleExportsDirective(Visitable visitable) {
            this(visitable, false);
        }

        /**
         * Effectue la recherche des ModuleExportsDirective dans un Visitable,
         * en profondeur ou non selon l'état de {@code deep}.
         * <p>
         * Rechercher en profondeur signifie que si un élément a été trouvé, la
         * recherche continue dans l'élément lui-même. Cela permet de trouver
         * les "if" imbriqués par exemple.
         * <p>
         * Ne pas rechercher en profondeur signifie qu'on s'arrête au premier
         * niveau.
         * <p>
         * Cette propriété n'a pas d'intérêt pour tous les éléments
         *
         * @param visitable le Visitable dans le lequel chercher les
         * ModuleExportsDirective
         * @param deep recherche en profondeur ou non
         */
        public FinderOfModuleExportsDirective(Visitable visitable, boolean deep) {
            super(visitable, deep);
            this.visitable.accept(vva, null);
        }

        /**
         * Retourne le tableau des ModuleExportsDirective trouvés
         *
         * @return le tableau des ModuleExportsDirective trouvés
         */
        public ArrayList<ModuleExportsDirective> getModuleExportsDirective() {
            return moduleExportsDirective;
        }

        /**
         * Retourne le nombre de ModuleExportsDirective trouvés
         *
         * @return le nombre de ModuleExportsDirective trouvés
         */
        public int getNumberOfModuleExportsDirective() {
            return moduleExportsDirective.size();
        }
    }

    /**
     * Pour extraire tous les ModuleRequiresDirective
     *
     * @author Yvan Maillot &lt;yvan.maillot@uha.fr&gt;
     */
    public static class FinderOfModuleRequiresDirective extends Finder {

        private final ArrayList<ModuleRequiresDirective> moduleRequiresDirective = new ArrayList();
        private final VoidVisitorAdapter<Void> vva = new VoidVisitorAdapter<>() {

            @Override
            public void visit(ModuleRequiresDirective n, Void arg) {
                if (deep) {
                    super.visit(n, arg);
                }
                moduleRequiresDirective.add(n);
            }
        };

        public FinderOfModuleRequiresDirective(Method m) {
            this(ParserWithReflectUtilities.find(m), true);
        }

        public FinderOfModuleRequiresDirective(Class<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfModuleRequiresDirective(Class<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfModuleRequiresDirective(Method m, boolean deep) {
            this(ParserWithReflectUtilities.find(m), deep);
        }

        public FinderOfModuleRequiresDirective(Constructor<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfModuleRequiresDirective(Constructor<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), deep);
        }

        /**
         * Dans ce cas, deep vaut true.
         *
         * @param visitable le Visitable dans le lequel chercher les
         * ModuleRequiresDirective
         */
        public FinderOfModuleRequiresDirective(Visitable visitable) {
            this(visitable, false);
        }

        /**
         * Effectue la recherche des ModuleRequiresDirective dans un Visitable,
         * en profondeur ou non selon l'état de {@code deep}.
         * <p>
         * Rechercher en profondeur signifie que si un élément a été trouvé, la
         * recherche continue dans l'élément lui-même. Cela permet de trouver
         * les "if" imbriqués par exemple.
         * <p>
         * Ne pas rechercher en profondeur signifie qu'on s'arrête au premier
         * niveau.
         * <p>
         * Cette propriété n'a pas d'intérêt pour tous les éléments
         *
         * @param visitable le Visitable dans le lequel chercher les
         * ModuleRequiresDirective
         * @param deep recherche en profondeur ou non
         */
        public FinderOfModuleRequiresDirective(Visitable visitable, boolean deep) {
            super(visitable, deep);
            this.visitable.accept(vva, null);
        }

        /**
         * Retourne le tableau des ModuleRequiresDirective trouvés
         *
         * @return le tableau des ModuleRequiresDirective trouvés
         */
        public ArrayList<ModuleRequiresDirective> getModuleRequiresDirective() {
            return moduleRequiresDirective;
        }

        /**
         * Retourne le nombre de ModuleRequiresDirective trouvés
         *
         * @return le nombre de ModuleRequiresDirective trouvés
         */
        public int getNumberOfModuleRequiresDirective() {
            return moduleRequiresDirective.size();
        }
    }

    /**
     * Pour extraire tous les YieldStmt
     *
     * @author Yvan Maillot &lt;yvan.maillot@uha.fr&gt;
     */
    public static class FinderOfYieldStmt extends Finder {

        private final ArrayList<YieldStmt> yieldStmt = new ArrayList();
        private final VoidVisitorAdapter<Void> vva = new VoidVisitorAdapter<Void>() {

            @Override
            public void visit(YieldStmt n, Void arg) {
                if (deep) {
                    super.visit(n, arg);
                }
                yieldStmt.add(n);
            }
        };

        public FinderOfYieldStmt(Method m) {
            this(ParserWithReflectUtilities.find(m), true);
        }

        public FinderOfYieldStmt(Class<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfYieldStmt(Class<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfYieldStmt(Method m, boolean deep) {
            this(ParserWithReflectUtilities.find(m), deep);
        }

        public FinderOfYieldStmt(Constructor<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfYieldStmt(Constructor<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), deep);
        }

        /**
         * Dans ce cas, deep vaut true.
         *
         * @param visitable le Visitable dans le lequel chercher les YieldStmt
         */
        public FinderOfYieldStmt(Visitable visitable) {
            this(visitable, false);
        }

        /**
         * Effectue la recherche des YieldStmt dans un Visitable, en profondeur
         * ou non selon l'état de {@code deep}.
         * <p>
         * Rechercher en profondeur signifie que si un élément a été trouvé, la
         * recherche continue dans l'élément lui-même. Cela permet de trouver
         * les "if" imbriqués par exemple.
         * <p>
         * Ne pas rechercher en profondeur signifie qu'on s'arrête au premier
         * niveau.
         * <p>
         * Cette propriété n'a pas d'intérêt pour tous les éléments
         *
         * @param visitable le Visitable dans le lequel chercher les YieldStmt
         * @param deep recherche en profondeur ou non
         */
        public FinderOfYieldStmt(Visitable visitable, boolean deep) {
            super(visitable, deep);
            this.visitable.accept(vva, null);
        }

        /**
         * Retourne le tableau des YieldStmt trouvés
         *
         * @return le tableau des YieldStmt trouvés
         */
        public ArrayList<YieldStmt> getYieldStmt() {
            return yieldStmt;
        }

        /**
         * Retourne le nombre de YieldStmt trouvés
         *
         * @return le nombre de YieldStmt trouvés
         */
        public int getNumberOfYieldStmt() {
            return yieldStmt.size();
        }
    }

    /**
     * Pour extraire tous les TextBlockLiteralExpr
     *
     * @author Yvan Maillot &lt;yvan.maillot@uha.fr&gt;
     */
    public static class FinderOfTextBlockLiteralExpr extends Finder {

        private final ArrayList<TextBlockLiteralExpr> textBlockLiteralExpr = new ArrayList();
        private final VoidVisitorAdapter<Void> vva = new VoidVisitorAdapter<>() {

            @Override
            public void visit(TextBlockLiteralExpr n, Void arg) {
                if (deep) {
                    super.visit(n, arg);
                }
                textBlockLiteralExpr.add(n);
            }
        };

        public FinderOfTextBlockLiteralExpr(Method m) {
            this(ParserWithReflectUtilities.find(m), true);
        }

        public FinderOfTextBlockLiteralExpr(Class<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfTextBlockLiteralExpr(Class<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfTextBlockLiteralExpr(Method m, boolean deep) {
            this(ParserWithReflectUtilities.find(m), deep);
        }

        public FinderOfTextBlockLiteralExpr(Constructor<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfTextBlockLiteralExpr(Constructor<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), deep);
        }

        /**
         * Dans ce cas, deep vaut true.
         *
         * @param visitable le Visitable dans le lequel chercher les
         * TextBlockLiteralExpr
         */
        public FinderOfTextBlockLiteralExpr(Visitable visitable) {
            this(visitable, false);
        }

        /**
         * Effectue la recherche des TextBlockLiteralExpr dans un Visitable, en
         * profondeur ou non selon l'état de {@code deep}.
         * <p>
         * Rechercher en profondeur signifie que si un élément a été trouvé, la
         * recherche continue dans l'élément lui-même. Cela permet de trouver
         * les "if" imbriqués par exemple.
         * <p>
         * Ne pas rechercher en profondeur signifie qu'on s'arrête au premier
         * niveau.
         * <p>
         * Cette propriété n'a pas d'intérêt pour tous les éléments
         *
         * @param visitable le Visitable dans le lequel chercher les
         * TextBlockLiteralExpr
         * @param deep recherche en profondeur ou non
         */
        public FinderOfTextBlockLiteralExpr(Visitable visitable, boolean deep) {
            super(visitable, deep);
            this.visitable.accept(vva, null);
        }

        /**
         * Retourne le tableau des TextBlockLiteralExpr trouvés
         *
         * @return le tableau des TextBlockLiteralExpr trouvés
         */
        public ArrayList<TextBlockLiteralExpr> getTextBlockLiteralExpr() {
            return textBlockLiteralExpr;
        }

        /**
         * Retourne le nombre de TextBlockLiteralExpr trouvés
         *
         * @return le nombre de TextBlockLiteralExpr trouvés
         */
        public int getNumberOfTextBlockLiteralExpr() {
            return textBlockLiteralExpr.size();
        }
    }

    /**
     * Pour extraire tous les SwitchExpr
     *
     * @author Yvan Maillot &lt;yvan.maillot@uha.fr&gt;
     */
    public static class FinderOfSwitchExpr extends Finder {

        private final ArrayList<SwitchExpr> switchExpr = new ArrayList();
        private final VoidVisitorAdapter<Void> vva = new VoidVisitorAdapter<>() {

            @Override
            public void visit(SwitchExpr n, Void arg) {
                if (deep) {
                    super.visit(n, arg);
                }
                switchExpr.add(n);
            }
        };

        public FinderOfSwitchExpr(Method m) {
            this(ParserWithReflectUtilities.find(m), true);
        }

        public FinderOfSwitchExpr(Class<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfSwitchExpr(Class<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfSwitchExpr(Method m, boolean deep) {
            this(ParserWithReflectUtilities.find(m), deep);
        }

        public FinderOfSwitchExpr(Constructor<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfSwitchExpr(Constructor<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), deep);
        }

        /**
         * Dans ce cas, deep vaut true.
         *
         * @param visitable le Visitable dans le lequel chercher les SwitchExpr
         */
        public FinderOfSwitchExpr(Visitable visitable) {
            this(visitable, false);
        }

        /**
         * Effectue la recherche des SwitchExpr dans un Visitable, en profondeur
         * ou non selon l'état de {@code deep}.
         * <p>
         * Rechercher en profondeur signifie que si un élément a été trouvé, la
         * recherche continue dans l'élément lui-même. Cela permet de trouver
         * les "if" imbriqués par exemple.
         * <p>
         * Ne pas rechercher en profondeur signifie qu'on s'arrête au premier
         * niveau.
         * <p>
         * Cette propriété n'a pas d'intérêt pour tous les éléments
         *
         * @param visitable le Visitable dans le lequel chercher les SwitchExpr
         * @param deep recherche en profondeur ou non
         */
        public FinderOfSwitchExpr(Visitable visitable, boolean deep) {
            super(visitable, deep);
            this.visitable.accept(vva, null);
        }

        /**
         * Retourne le tableau des SwitchExpr trouvés
         *
         * @return le tableau des SwitchExpr trouvés
         */
        public ArrayList<SwitchExpr> getSwitchExpr() {
            return switchExpr;
        }

        /**
         * Retourne le nombre de SwitchExpr trouvés
         *
         * @return le nombre de SwitchExpr trouvés
         */
        public int getNumberOfSwitchExpr() {
            return switchExpr.size();
        }
    }

    /**
     * Pour extraire tous les Modifier
     *
     * @author Yvan Maillot &lt;yvan.maillot@uha.fr&gt;
     */
    public static class FinderOfModifier extends Finder {

        private final ArrayList<Modifier> modifier = new ArrayList();
        private final VoidVisitorAdapter<Void> vva = new VoidVisitorAdapter<>() {

            @Override
            public void visit(Modifier n, Void arg) {
                if (deep) {
                    super.visit(n, arg);
                }
                modifier.add(n);
            }
        };

        public FinderOfModifier(Method m) {
            this(ParserWithReflectUtilities.find(m), true);
        }

        public FinderOfModifier(Class<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfModifier(Class<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfModifier(Method m, boolean deep) {
            this(ParserWithReflectUtilities.find(m), deep);
        }

        public FinderOfModifier(Constructor<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfModifier(Constructor<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), deep);
        }

        /**
         * Dans ce cas, deep vaut true.
         *
         * @param visitable le Visitable dans le lequel chercher les Modifier
         */
        public FinderOfModifier(Visitable visitable) {
            this(visitable, false);
        }

        /**
         * Effectue la recherche des Modifier dans un Visitable, en profondeur
         * ou non selon l'état de {@code deep}.
         * <p>
         * Rechercher en profondeur signifie que si un élément a été trouvé, la
         * recherche continue dans l'élément lui-même. Cela permet de trouver
         * les "if" imbriqués par exemple.
         * <p>
         * Ne pas rechercher en profondeur signifie qu'on s'arrête au premier
         * niveau.
         * <p>
         * Cette propriété n'a pas d'intérêt pour tous les éléments
         *
         * @param visitable le Visitable dans le lequel chercher les Modifier
         * @param deep recherche en profondeur ou non
         */
        public FinderOfModifier(Visitable visitable, boolean deep) {
            super(visitable, deep);
            this.visitable.accept(vva, null);
        }

        /**
         * Retourne le tableau des Modifier trouvés
         *
         * @return le tableau des Modifier trouvés
         */
        public ArrayList<Modifier> getModifier() {
            return modifier;
        }

        /**
         * Retourne le nombre de Modifier trouvés
         *
         * @return le nombre de Modifier trouvés
         */
        public int getNumberOfModifier() {
            return modifier.size();
        }
    }

    /**
     * Pour extraire tous les VarType
     *
     * @author Yvan Maillot &lt;yvan.maillot@uha.fr&gt;
     */
    public static class FinderOfVarType extends Finder {

        private final ArrayList<VarType> varType = new ArrayList();
        private final VoidVisitorAdapter<Void> vva = new VoidVisitorAdapter<>() {

            @Override
            public void visit(VarType n, Void arg) {
                if (deep) {
                    super.visit(n, arg);
                }
                varType.add(n);
            }
        };

        public FinderOfVarType(Method m) {
            this(ParserWithReflectUtilities.find(m), true);
        }

        public FinderOfVarType(Class<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfVarType(Class<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfVarType(Method m, boolean deep) {
            this(ParserWithReflectUtilities.find(m), deep);
        }

        public FinderOfVarType(Constructor<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfVarType(Constructor<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), deep);
        }

        /**
         * Dans ce cas, deep vaut true.
         *
         * @param visitable le Visitable dans le lequel chercher les VarType
         */
        public FinderOfVarType(Visitable visitable) {
            this(visitable, false);
        }

        /**
         * Effectue la recherche des VarType dans un Visitable, en profondeur ou
         * non selon l'état de {@code deep}.
         * <p>
         * Rechercher en profondeur signifie que si un élément a été trouvé, la
         * recherche continue dans l'élément lui-même. Cela permet de trouver
         * les "if" imbriqués par exemple.
         * <p>
         * Ne pas rechercher en profondeur signifie qu'on s'arrête au premier
         * niveau.
         * <p>
         * Cette propriété n'a pas d'intérêt pour tous les éléments
         *
         * @param visitable le Visitable dans le lequel chercher les VarType
         * @param deep recherche en profondeur ou non
         */
        public FinderOfVarType(Visitable visitable, boolean deep) {
            super(visitable, deep);
            this.visitable.accept(vva, null);
        }

        /**
         * Retourne le tableau des VarType trouvés
         *
         * @return le tableau des VarType trouvés
         */
        public ArrayList<VarType> getVarType() {
            return varType;
        }

        /**
         * Retourne le nombre de VarType trouvés
         *
         * @return le nombre de VarType trouvés
         */
        public int getNumberOfVarType() {
            return varType.size();
        }
    }

    /**
     * Pour extraire tous les ReceiverParameter
     *
     * @author Yvan Maillot &lt;yvan.maillot@uha.fr&gt;
     */
    public static class FinderOfReceiverParameter extends Finder {

        private final ArrayList<ReceiverParameter> receiverParameter = new ArrayList();
        private final VoidVisitorAdapter<Void> vva = new VoidVisitorAdapter<>() {

            @Override
            public void visit(ReceiverParameter n, Void arg) {
                if (deep) {
                    super.visit(n, arg);
                }
                receiverParameter.add(n);
            }
        };

        public FinderOfReceiverParameter(Method m) {
            this(ParserWithReflectUtilities.find(m), true);
        }

        public FinderOfReceiverParameter(Class<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfReceiverParameter(Class<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfReceiverParameter(Method m, boolean deep) {
            this(ParserWithReflectUtilities.find(m), deep);
        }

        public FinderOfReceiverParameter(Constructor<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfReceiverParameter(Constructor<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), deep);
        }

        /**
         * Dans ce cas, deep vaut true.
         *
         * @param visitable le Visitable dans le lequel chercher les
         * ReceiverParameter
         */
        public FinderOfReceiverParameter(Visitable visitable) {
            this(visitable, false);
        }

        /**
         * Effectue la recherche des ReceiverParameter dans un Visitable, en
         * profondeur ou non selon l'état de {@code deep}.
         * <p>
         * Rechercher en profondeur signifie que si un élément a été trouvé, la
         * recherche continue dans l'élément lui-même. Cela permet de trouver
         * les "if" imbriqués par exemple.
         * <p>
         * Ne pas rechercher en profondeur signifie qu'on s'arrête au premier
         * niveau.
         * <p>
         * Cette propriété n'a pas d'intérêt pour tous les éléments
         *
         * @param visitable le Visitable dans le lequel chercher les
         * ReceiverParameter
         * @param deep recherche en profondeur ou non
         */
        public FinderOfReceiverParameter(Visitable visitable, boolean deep) {
            super(visitable, deep);
            this.visitable.accept(vva, null);
        }

        /**
         * Retourne le tableau des ReceiverParameter trouvés
         *
         * @return le tableau des ReceiverParameter trouvés
         */
        public ArrayList<ReceiverParameter> getReceiverParameter() {
            return receiverParameter;
        }

        /**
         * Retourne le nombre de ReceiverParameter trouvés
         *
         * @return le nombre de ReceiverParameter trouvés
         */
        public int getNumberOfReceiverParameter() {
            return receiverParameter.size();
        }
    }

    /**
     * Pour extraire tous les WildcardType
     *
     * @author Yvan Maillot &lt;yvan.maillot@uha.fr&gt;
     */
    public static class FinderOfWildcardType extends Finder {

        private final ArrayList<WildcardType> wildcardType = new ArrayList();
        private final VoidVisitorAdapter<Void> vva = new VoidVisitorAdapter<>() {

            @Override
            public void visit(WildcardType n, Void arg) {
                if (deep) {
                    super.visit(n, arg);
                }
                wildcardType.add(n);
            }
        };

        public FinderOfWildcardType(Method m) {
            this(ParserWithReflectUtilities.find(m), true);
        }

        public FinderOfWildcardType(Class<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfWildcardType(Class<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfWildcardType(Method m, boolean deep) {
            this(ParserWithReflectUtilities.find(m), deep);
        }

        public FinderOfWildcardType(Constructor<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfWildcardType(Constructor<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), deep);
        }

        /**
         * Dans ce cas, deep vaut true.
         *
         * @param visitable le Visitable dans le lequel chercher les
         * WildcardType
         */
        public FinderOfWildcardType(Visitable visitable) {
            this(visitable, false);
        }

        /**
         * Effectue la recherche des WildcardType dans un Visitable, en
         * profondeur ou non selon l'état de {@code deep}.
         * <p>
         * Rechercher en profondeur signifie que si un élément a été trouvé, la
         * recherche continue dans l'élément lui-même. Cela permet de trouver
         * les "if" imbriqués par exemple.
         * <p>
         * Ne pas rechercher en profondeur signifie qu'on s'arrête au premier
         * niveau.
         * <p>
         * Cette propriété n'a pas d'intérêt pour tous les éléments
         *
         * @param visitable le Visitable dans le lequel chercher les
         * WildcardType
         * @param deep recherche en profondeur ou non
         */
        public FinderOfWildcardType(Visitable visitable, boolean deep) {
            super(visitable, deep);
            this.visitable.accept(vva, null);
        }

        /**
         * Retourne le tableau des WildcardType trouvés
         *
         * @return le tableau des WildcardType trouvés
         */
        public ArrayList<WildcardType> getWildcardType() {
            return wildcardType;
        }

        /**
         * Retourne le nombre de WildcardType trouvés
         *
         * @return le nombre de WildcardType trouvés
         */
        public int getNumberOfWildcardType() {
            return wildcardType.size();
        }
    }

    /**
     * Pour extraire tous les WhileStmt
     *
     * @author Yvan Maillot &lt;yvan.maillot@uha.fr&gt;
     */
    public static class FinderOfWhileStmt extends Finder {

        private final ArrayList<WhileStmt> whileStmt = new ArrayList();
        private final VoidVisitorAdapter<Void> vva = new VoidVisitorAdapter<>() {

            @Override
            public void visit(WhileStmt n, Void arg) {
                if (deep) {
                    super.visit(n, arg);
                }
                whileStmt.add(n);
            }
        };

        public FinderOfWhileStmt(Method m) {
            this(ParserWithReflectUtilities.find(m), true);
        }

        public FinderOfWhileStmt(Class<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfWhileStmt(Class<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfWhileStmt(Method m, boolean deep) {
            this(ParserWithReflectUtilities.find(m), deep);
        }

        public FinderOfWhileStmt(Constructor<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfWhileStmt(Constructor<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), deep);
        }

        /**
         * Dans ce cas, deep vaut true.
         *
         * @param visitable le Visitable dans le lequel chercher les WhileStmt
         */
        public FinderOfWhileStmt(Visitable visitable) {
            this(visitable, false);
        }

        /**
         * Effectue la recherche des WhileStmt dans un Visitable, en profondeur
         * ou non selon l'état de {@code deep}.
         * <p>
         * Rechercher en profondeur signifie que si un élément a été trouvé, la
         * recherche continue dans l'élément lui-même. Cela permet de trouver
         * les "if" imbriqués par exemple.
         * <p>
         * Ne pas rechercher en profondeur signifie qu'on s'arrête au premier
         * niveau.
         * <p>
         * Cette propriété n'a pas d'intérêt pour tous les éléments
         *
         * @param visitable le Visitable dans le lequel chercher les WhileStmt
         * @param deep recherche en profondeur ou non
         */
        public FinderOfWhileStmt(Visitable visitable, boolean deep) {
            super(visitable, deep);
            this.visitable.accept(vva, null);
        }

        /**
         * Retourne le tableau des WhileStmt trouvés
         *
         * @return le tableau des WhileStmt trouvés
         */
        public ArrayList<WhileStmt> getWhileStmt() {
            return whileStmt;
        }

        /**
         * Retourne le nombre de WhileStmt trouvés
         *
         * @return le nombre de WhileStmt trouvés
         */
        public int getNumberOfWhileStmt() {
            return whileStmt.size();
        }
    }

    /**
     * Pour extraire tous les VoidType
     *
     * @author Yvan Maillot &lt;yvan.maillot@uha.fr&gt;
     */
    public static class FinderOfVoidType extends Finder {

        private final ArrayList<VoidType> voidType = new ArrayList();
        private final VoidVisitorAdapter<Void> vva = new VoidVisitorAdapter<>() {

            @Override
            public void visit(VoidType n, Void arg) {
                if (deep) {
                    super.visit(n, arg);
                }
                voidType.add(n);
            }
        };

        public FinderOfVoidType(Method m) {
            this(ParserWithReflectUtilities.find(m), true);
        }

        public FinderOfVoidType(Class<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfVoidType(Class<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfVoidType(Method m, boolean deep) {
            this(ParserWithReflectUtilities.find(m), deep);
        }

        public FinderOfVoidType(Constructor<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfVoidType(Constructor<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), deep);
        }

        /**
         * Dans ce cas, deep vaut true.
         *
         * @param visitable le Visitable dans le lequel chercher les VoidType
         */
        public FinderOfVoidType(Visitable visitable) {
            this(visitable, false);
        }

        /**
         * Effectue la recherche des VoidType dans un Visitable, en profondeur
         * ou non selon l'état de {@code deep}.
         * <p>
         * Rechercher en profondeur signifie que si un élément a été trouvé, la
         * recherche continue dans l'élément lui-même. Cela permet de trouver
         * les "if" imbriqués par exemple.
         * <p>
         * Ne pas rechercher en profondeur signifie qu'on s'arrête au premier
         * niveau.
         * <p>
         * Cette propriété n'a pas d'intérêt pour tous les éléments
         *
         * @param visitable le Visitable dans le lequel chercher les VoidType
         * @param deep recherche en profondeur ou non
         */
        public FinderOfVoidType(Visitable visitable, boolean deep) {
            super(visitable, deep);
            this.visitable.accept(vva, null);
        }

        /**
         * Retourne le tableau des VoidType trouvés
         *
         * @return le tableau des VoidType trouvés
         */
        public ArrayList<VoidType> getVoidType() {
            return voidType;
        }

        /**
         * Retourne le nombre de VoidType trouvés
         *
         * @return le nombre de VoidType trouvés
         */
        public int getNumberOfVoidType() {
            return voidType.size();
        }
    }

    /**
     * Pour extraire tous les VariableDeclarator
     *
     * @author Yvan Maillot &lt;yvan.maillot@uha.fr&gt;
     */
    public static class FinderOfVariableDeclarator extends Finder {

        private final ArrayList<VariableDeclarator> variableDeclarator = new ArrayList();
        private final VoidVisitorAdapter<Void> vva = new VoidVisitorAdapter<>() {

            @Override
            public void visit(VariableDeclarator n, Void arg) {
                if (deep) {
                    super.visit(n, arg);
                }
                variableDeclarator.add(n);
            }
        };

        public FinderOfVariableDeclarator(Method m) {
            this(ParserWithReflectUtilities.find(m), true);
        }

        public FinderOfVariableDeclarator(Class<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfVariableDeclarator(Class<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfVariableDeclarator(Method m, boolean deep) {
            this(ParserWithReflectUtilities.find(m), deep);
        }

        public FinderOfVariableDeclarator(Constructor<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfVariableDeclarator(Constructor<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), deep);
        }

        /**
         * Dans ce cas, deep vaut true.
         *
         * @param visitable le Visitable dans le lequel chercher les
         * VariableDeclarator
         */
        public FinderOfVariableDeclarator(Visitable visitable) {
            this(visitable, false);
        }

        /**
         * Effectue la recherche des VariableDeclarator dans un Visitable, en
         * profondeur ou non selon l'état de {@code deep}.
         * <p>
         * Rechercher en profondeur signifie que si un élément a été trouvé, la
         * recherche continue dans l'élément lui-même. Cela permet de trouver
         * les "if" imbriqués par exemple.
         * <p>
         * Ne pas rechercher en profondeur signifie qu'on s'arrête au premier
         * niveau.
         * <p>
         * Cette propriété n'a pas d'intérêt pour tous les éléments
         *
         * @param visitable le Visitable dans le lequel chercher les
         * VariableDeclarator
         * @param deep recherche en profondeur ou non
         */
        public FinderOfVariableDeclarator(Visitable visitable, boolean deep) {
            super(visitable, deep);
            this.visitable.accept(vva, null);
        }

        /**
         * Retourne le tableau des VariableDeclarator trouvés
         *
         * @return le tableau des VariableDeclarator trouvés
         */
        public ArrayList<VariableDeclarator> getVariableDeclarator() {
            return variableDeclarator;
        }

        /**
         * Retourne le nombre de VariableDeclarator trouvés
         *
         * @return le nombre de VariableDeclarator trouvés
         */
        public int getNumberOfVariableDeclarator() {
            return variableDeclarator.size();
        }
    }

    /**
     * Pour extraire tous les VariableDeclarationExpr
     *
     * @author Yvan Maillot &lt;yvan.maillot@uha.fr&gt;
     */
    public static class FinderOfVariableDeclarationExpr extends Finder {

        private final ArrayList<VariableDeclarationExpr> variableDeclarationExpr = new ArrayList();
        private final VoidVisitorAdapter<Void> vva = new VoidVisitorAdapter<>() {

            @Override
            public void visit(VariableDeclarationExpr n, Void arg) {
                if (deep) {
                    super.visit(n, arg);
                }
                variableDeclarationExpr.add(n);
            }
        };

        public FinderOfVariableDeclarationExpr(Method m) {
            this(ParserWithReflectUtilities.find(m), true);
        }

        public FinderOfVariableDeclarationExpr(Class<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfVariableDeclarationExpr(Class<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfVariableDeclarationExpr(Method m, boolean deep) {
            this(ParserWithReflectUtilities.find(m), deep);
        }

        public FinderOfVariableDeclarationExpr(Constructor<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfVariableDeclarationExpr(Constructor<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), deep);
        }

        /**
         * Dans ce cas, deep vaut true.
         *
         * @param visitable le Visitable dans le lequel chercher les
         * VariableDeclarationExpr
         */
        public FinderOfVariableDeclarationExpr(Visitable visitable) {
            this(visitable, false);
        }

        /**
         * Effectue la recherche des VariableDeclarationExpr dans un Visitable,
         * en profondeur ou non selon l'état de {@code deep}.
         * <p>
         * Rechercher en profondeur signifie que si un élément a été trouvé, la
         * recherche continue dans l'élément lui-même. Cela permet de trouver
         * les "if" imbriqués par exemple.
         * <p>
         * Ne pas rechercher en profondeur signifie qu'on s'arrête au premier
         * niveau.
         * <p>
         * Cette propriété n'a pas d'intérêt pour tous les éléments
         *
         * @param visitable le Visitable dans le lequel chercher les
         * VariableDeclarationExpr
         * @param deep recherche en profondeur ou non
         */
        public FinderOfVariableDeclarationExpr(Visitable visitable, boolean deep) {
            super(visitable, deep);
            this.visitable.accept(vva, null);
        }

        /**
         * Retourne le tableau des VariableDeclarationExpr trouvés
         *
         * @return le tableau des VariableDeclarationExpr trouvés
         */
        public ArrayList<VariableDeclarationExpr> getVariableDeclarationExpr() {
            return variableDeclarationExpr;
        }

        /**
         * Retourne le nombre de VariableDeclarationExpr trouvés
         *
         * @return le nombre de VariableDeclarationExpr trouvés
         */
        public int getNumberOfVariableDeclarationExpr() {
            return variableDeclarationExpr.size();
        }
    }

    /**
     * Pour extraire tous les UnknownType
     *
     * @author Yvan Maillot &lt;yvan.maillot@uha.fr&gt;
     */
    public static class FinderOfUnknownType extends Finder {

        private final ArrayList<UnknownType> unknownType = new ArrayList();
        private final VoidVisitorAdapter<Void> vva = new VoidVisitorAdapter<>() {

            @Override
            public void visit(UnknownType n, Void arg) {
                if (deep) {
                    super.visit(n, arg);
                }
                unknownType.add(n);
            }
        };

        public FinderOfUnknownType(Method m) {
            this(ParserWithReflectUtilities.find(m), true);
        }

        public FinderOfUnknownType(Class<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfUnknownType(Class<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfUnknownType(Method m, boolean deep) {
            this(ParserWithReflectUtilities.find(m), deep);
        }

        public FinderOfUnknownType(Constructor<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfUnknownType(Constructor<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), deep);
        }

        /**
         * Dans ce cas, deep vaut true.
         *
         * @param visitable le Visitable dans le lequel chercher les UnknownType
         */
        public FinderOfUnknownType(Visitable visitable) {
            this(visitable, false);
        }

        /**
         * Effectue la recherche des UnknownType dans un Visitable, en
         * profondeur ou non selon l'état de {@code deep}.
         * <p>
         * Rechercher en profondeur signifie que si un élément a été trouvé, la
         * recherche continue dans l'élément lui-même. Cela permet de trouver
         * les "if" imbriqués par exemple.
         * <p>
         * Ne pas rechercher en profondeur signifie qu'on s'arrête au premier
         * niveau.
         * <p>
         * Cette propriété n'a pas d'intérêt pour tous les éléments
         *
         * @param visitable le Visitable dans le lequel chercher les UnknownType
         * @param deep recherche en profondeur ou non
         */
        public FinderOfUnknownType(Visitable visitable, boolean deep) {
            super(visitable, deep);
            this.visitable.accept(vva, null);
        }

        /**
         * Retourne le tableau des UnknownType trouvés
         *
         * @return le tableau des UnknownType trouvés
         */
        public ArrayList<UnknownType> getUnknownType() {
            return unknownType;
        }

        /**
         * Retourne le nombre de UnknownType trouvés
         *
         * @return le nombre de UnknownType trouvés
         */
        public int getNumberOfUnknownType() {
            return unknownType.size();
        }
    }

    /**
     * Pour extraire tous les ModuleDeclaration
     *
     * @author Yvan Maillot &lt;yvan.maillot@uha.fr&gt;
     */
    public static class FinderOfModuleDeclaration extends Finder {

        private final ArrayList<ModuleDeclaration> moduleDeclaration = new ArrayList();
        private final VoidVisitorAdapter<Void> vva = new VoidVisitorAdapter<>() {

            @Override
            public void visit(ModuleDeclaration n, Void arg) {
                if (deep) {
                    super.visit(n, arg);
                }
                moduleDeclaration.add(n);
            }
        };

        public FinderOfModuleDeclaration(Method m) {
            this(ParserWithReflectUtilities.find(m), true);
        }

        public FinderOfModuleDeclaration(Class<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfModuleDeclaration(Class<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfModuleDeclaration(Method m, boolean deep) {
            this(ParserWithReflectUtilities.find(m), deep);
        }

        public FinderOfModuleDeclaration(Constructor<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfModuleDeclaration(Constructor<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), deep);
        }

        /**
         * Dans ce cas, deep vaut true.
         *
         * @param visitable le Visitable dans le lequel chercher les
         * ModuleDeclaration
         */
        public FinderOfModuleDeclaration(Visitable visitable) {
            this(visitable, false);
        }

        /**
         * Effectue la recherche des ModuleDeclaration dans un Visitable, en
         * profondeur ou non selon l'état de {@code deep}.
         * <p>
         * Rechercher en profondeur signifie que si un élément a été trouvé, la
         * recherche continue dans l'élément lui-même. Cela permet de trouver
         * les "if" imbriqués par exemple.
         * <p>
         * Ne pas rechercher en profondeur signifie qu'on s'arrête au premier
         * niveau.
         * <p>
         * Cette propriété n'a pas d'intérêt pour tous les éléments
         *
         * @param visitable le Visitable dans le lequel chercher les
         * ModuleDeclaration
         * @param deep recherche en profondeur ou non
         */
        public FinderOfModuleDeclaration(Visitable visitable, boolean deep) {
            super(visitable, deep);
            this.visitable.accept(vva, null);
        }

        /**
         * Retourne le tableau des ModuleDeclaration trouvés
         *
         * @return le tableau des ModuleDeclaration trouvés
         */
        public ArrayList<ModuleDeclaration> getModuleDeclaration() {
            return moduleDeclaration;
        }

        /**
         * Retourne le nombre de ModuleDeclaration trouvés
         *
         * @return le nombre de ModuleDeclaration trouvés
         */
        public int getNumberOfModuleDeclaration() {
            return moduleDeclaration.size();
        }
    }

    /**
     * Pour extraire tous les ImportDeclaration
     *
     * @author Yvan Maillot &lt;yvan.maillot@uha.fr&gt;
     */
    public static class FinderOfImportDeclaration extends Finder {

        private final ArrayList<ImportDeclaration> importDeclaration = new ArrayList();
        private final VoidVisitorAdapter<Void> vva = new VoidVisitorAdapter<>() {

            @Override
            public void visit(ImportDeclaration n, Void arg) {
                if (deep) {
                    super.visit(n, arg);
                }
                importDeclaration.add(n);
            }
        };

        public FinderOfImportDeclaration(Method m) {
            this(ParserWithReflectUtilities.find(m), true);
        }

        public FinderOfImportDeclaration(Class<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfImportDeclaration(Class<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfImportDeclaration(Method m, boolean deep) {
            this(ParserWithReflectUtilities.find(m), deep);
        }

        public FinderOfImportDeclaration(Constructor<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfImportDeclaration(Constructor<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), deep);
        }

        /**
         * Dans ce cas, deep vaut true.
         *
         * @param visitable le Visitable dans le lequel chercher les
         * ImportDeclaration
         */
        public FinderOfImportDeclaration(Visitable visitable) {
            this(visitable, false);
        }

        /**
         * Effectue la recherche des ImportDeclaration dans un Visitable, en
         * profondeur ou non selon l'état de {@code deep}.
         * <p>
         * Rechercher en profondeur signifie que si un élément a été trouvé, la
         * recherche continue dans l'élément lui-même. Cela permet de trouver
         * les "if" imbriqués par exemple.
         * <p>
         * Ne pas rechercher en profondeur signifie qu'on s'arrête au premier
         * niveau.
         * <p>
         * Cette propriété n'a pas d'intérêt pour tous les éléments
         *
         * @param visitable le Visitable dans le lequel chercher les
         * ImportDeclaration
         * @param deep recherche en profondeur ou non
         */
        public FinderOfImportDeclaration(Visitable visitable, boolean deep) {
            super(visitable, deep);
            this.visitable.accept(vva, null);
        }

        /**
         * Retourne le tableau des ImportDeclaration trouvés
         *
         * @return le tableau des ImportDeclaration trouvés
         */
        public ArrayList<ImportDeclaration> getImportDeclaration() {
            return importDeclaration;
        }

        /**
         * Retourne le nombre de ImportDeclaration trouvés
         *
         * @return le nombre de ImportDeclaration trouvés
         */
        public int getNumberOfImportDeclaration() {
            return importDeclaration.size();
        }
    }

    /**
     * Pour extraire tous les NodeList
     *
     * @author Yvan Maillot &lt;yvan.maillot@uha.fr&gt;
     */
    public static class FinderOfNodeList extends Finder {

        private final ArrayList<NodeList> nodeList = new ArrayList();
        private final VoidVisitorAdapter<Void> vva = new VoidVisitorAdapter<>() {

            @Override
            public void visit(NodeList n, Void arg) {
                if (deep) {
                    super.visit(n, arg);
                }
                nodeList.add(n);
            }
        };

        public FinderOfNodeList(Method m) {
            this(ParserWithReflectUtilities.find(m), true);
        }

        public FinderOfNodeList(Class<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfNodeList(Class<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfNodeList(Method m, boolean deep) {
            this(ParserWithReflectUtilities.find(m), deep);
        }

        public FinderOfNodeList(Constructor<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfNodeList(Constructor<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), deep);
        }

        /**
         * Dans ce cas, deep vaut true.
         *
         * @param visitable le Visitable dans le lequel chercher les NodeList
         */
        public FinderOfNodeList(Visitable visitable) {
            this(visitable, false);
        }

        /**
         * Effectue la recherche des NodeList dans un Visitable, en profondeur
         * ou non selon l'état de {@code deep}.
         * <p>
         * Rechercher en profondeur signifie que si un élément a été trouvé, la
         * recherche continue dans l'élément lui-même. Cela permet de trouver
         * les "if" imbriqués par exemple.
         * <p>
         * Ne pas rechercher en profondeur signifie qu'on s'arrête au premier
         * niveau.
         * <p>
         * Cette propriété n'a pas d'intérêt pour tous les éléments
         *
         * @param visitable le Visitable dans le lequel chercher les NodeList
         * @param deep recherche en profondeur ou non
         */
        public FinderOfNodeList(Visitable visitable, boolean deep) {
            super(visitable, deep);
            this.visitable.accept(vva, null);
        }

        /**
         * Retourne le tableau des NodeList trouvés
         *
         * @return le tableau des NodeList trouvés
         */
        public ArrayList<NodeList> getNodeList() {
            return nodeList;
        }

        /**
         * Retourne le nombre de NodeList trouvés
         *
         * @return le nombre de NodeList trouvés
         */
        public int getNumberOfNodeList() {
            return nodeList.size();
        }
    }

    /**
     * Pour extraire tous les TypeExpr
     *
     * @author Yvan Maillot &lt;yvan.maillot@uha.fr&gt;
     */
    public static class FinderOfTypeExpr extends Finder {

        private final ArrayList<TypeExpr> typeExpr = new ArrayList();
        private final VoidVisitorAdapter<Void> vva = new VoidVisitorAdapter<>() {

            @Override
            public void visit(TypeExpr n, Void arg) {
                if (deep) {
                    super.visit(n, arg);
                }
                typeExpr.add(n);
            }
        };

        public FinderOfTypeExpr(Method m) {
            this(ParserWithReflectUtilities.find(m), true);
        }

        public FinderOfTypeExpr(Class<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfTypeExpr(Class<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfTypeExpr(Method m, boolean deep) {
            this(ParserWithReflectUtilities.find(m), deep);
        }

        public FinderOfTypeExpr(Constructor<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfTypeExpr(Constructor<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), deep);
        }

        /**
         * Dans ce cas, deep vaut true.
         *
         * @param visitable le Visitable dans le lequel chercher les TypeExpr
         */
        public FinderOfTypeExpr(Visitable visitable) {
            this(visitable, false);
        }

        /**
         * Effectue la recherche des TypeExpr dans un Visitable, en profondeur
         * ou non selon l'état de {@code deep}.
         * <p>
         * Rechercher en profondeur signifie que si un élément a été trouvé, la
         * recherche continue dans l'élément lui-même. Cela permet de trouver
         * les "if" imbriqués par exemple.
         * <p>
         * Ne pas rechercher en profondeur signifie qu'on s'arrête au premier
         * niveau.
         * <p>
         * Cette propriété n'a pas d'intérêt pour tous les éléments
         *
         * @param visitable le Visitable dans le lequel chercher les TypeExpr
         * @param deep recherche en profondeur ou non
         */
        public FinderOfTypeExpr(Visitable visitable, boolean deep) {
            super(visitable, deep);
            this.visitable.accept(vva, null);
        }

        /**
         * Retourne le tableau des TypeExpr trouvés
         *
         * @return le tableau des TypeExpr trouvés
         */
        public ArrayList<TypeExpr> getTypeExpr() {
            return typeExpr;
        }

        /**
         * Retourne le nombre de TypeExpr trouvés
         *
         * @return le nombre de TypeExpr trouvés
         */
        public int getNumberOfTypeExpr() {
            return typeExpr.size();
        }
    }

    /**
     * Pour extraire tous les MethodReferenceExpr
     *
     * @author Yvan Maillot &lt;yvan.maillot@uha.fr&gt;
     */
    public static class FinderOfMethodReferenceExpr extends Finder {

        private final ArrayList<MethodReferenceExpr> methodReferenceExpr = new ArrayList();
        private final VoidVisitorAdapter<Void> vva = new VoidVisitorAdapter<>() {

            @Override
            public void visit(MethodReferenceExpr n, Void arg) {
                if (deep) {
                    super.visit(n, arg);
                }
                methodReferenceExpr.add(n);
            }
        };

        public FinderOfMethodReferenceExpr(Method m) {
            this(ParserWithReflectUtilities.find(m), true);
        }

        public FinderOfMethodReferenceExpr(Class<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfMethodReferenceExpr(Class<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfMethodReferenceExpr(Method m, boolean deep) {
            this(ParserWithReflectUtilities.find(m), deep);
        }

        public FinderOfMethodReferenceExpr(Constructor<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfMethodReferenceExpr(Constructor<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), deep);
        }

        /**
         * Dans ce cas, deep vaut true.
         *
         * @param visitable le Visitable dans le lequel chercher les
         * MethodReferenceExpr
         */
        public FinderOfMethodReferenceExpr(Visitable visitable) {
            this(visitable, false);
        }

        /**
         * Effectue la recherche des MethodReferenceExpr dans un Visitable, en
         * profondeur ou non selon l'état de {@code deep}.
         * <p>
         * Rechercher en profondeur signifie que si un élément a été trouvé, la
         * recherche continue dans l'élément lui-même. Cela permet de trouver
         * les "if" imbriqués par exemple.
         * <p>
         * Ne pas rechercher en profondeur signifie qu'on s'arrête au premier
         * niveau.
         * <p>
         * Cette propriété n'a pas d'intérêt pour tous les éléments
         *
         * @param visitable le Visitable dans le lequel chercher les
         * MethodReferenceExpr
         * @param deep recherche en profondeur ou non
         */
        public FinderOfMethodReferenceExpr(Visitable visitable, boolean deep) {
            super(visitable, deep);
            this.visitable.accept(vva, null);
        }

        /**
         * Retourne le tableau des MethodReferenceExpr trouvés
         *
         * @return le tableau des MethodReferenceExpr trouvés
         */
        public ArrayList<MethodReferenceExpr> getMethodReferenceExpr() {
            return methodReferenceExpr;
        }

        /**
         * Retourne le nombre de MethodReferenceExpr trouvés
         *
         * @return le nombre de MethodReferenceExpr trouvés
         */
        public int getNumberOfMethodReferenceExpr() {
            return methodReferenceExpr.size();
        }
    }

    /**
     * Pour extraire tous les LambdaExpr
     *
     * @author Yvan Maillot &lt;yvan.maillot@uha.fr&gt;
     */
    public static class FinderOfLambdaExpr extends Finder {

        private final ArrayList<LambdaExpr> lambdaExpr = new ArrayList();
        private final VoidVisitorAdapter<Void> vva = new VoidVisitorAdapter<>() {

            @Override
            public void visit(LambdaExpr n, Void arg) {
                if (deep) {
                    super.visit(n, arg);
                }
                lambdaExpr.add(n);
            }
        };

        public FinderOfLambdaExpr(Method m) {
            this(ParserWithReflectUtilities.find(m), true);
        }

        public FinderOfLambdaExpr(Class<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfLambdaExpr(Class<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfLambdaExpr(Method m, boolean deep) {
            this(ParserWithReflectUtilities.find(m), deep);
        }

        public FinderOfLambdaExpr(Constructor<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfLambdaExpr(Constructor<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), deep);
        }

        /**
         * Dans ce cas, deep vaut true.
         *
         * @param visitable le Visitable dans le lequel chercher les LambdaExpr
         */
        public FinderOfLambdaExpr(Visitable visitable) {
            this(visitable, false);
        }

        /**
         * Effectue la recherche des LambdaExpr dans un Visitable, en profondeur
         * ou non selon l'état de {@code deep}.
         * <p>
         * Rechercher en profondeur signifie que si un élément a été trouvé, la
         * recherche continue dans l'élément lui-même. Cela permet de trouver
         * les "if" imbriqués par exemple.
         * <p>
         * Ne pas rechercher en profondeur signifie qu'on s'arrête au premier
         * niveau.
         * <p>
         * Cette propriété n'a pas d'intérêt pour tous les éléments
         *
         * @param visitable le Visitable dans le lequel chercher les LambdaExpr
         * @param deep recherche en profondeur ou non
         */
        public FinderOfLambdaExpr(Visitable visitable, boolean deep) {
            super(visitable, deep);
            this.visitable.accept(vva, null);
        }

        /**
         * Retourne le tableau des LambdaExpr trouvés
         *
         * @return le tableau des LambdaExpr trouvés
         */
        public ArrayList<LambdaExpr> getLambdaExpr() {
            return lambdaExpr;
        }

        /**
         * Retourne le nombre de LambdaExpr trouvés
         *
         * @return le nombre de LambdaExpr trouvés
         */
        public int getNumberOfLambdaExpr() {
            return lambdaExpr.size();
        }
    }

    /**
     * Pour extraire tous les ClassOrInterfaceType
     *
     * @author Yvan Maillot &lt;yvan.maillot@uha.fr&gt;
     */
    public static class FinderOfClassOrInterfaceType extends Finder {

        private final ArrayList<ClassOrInterfaceType> classOrInterfaceType = new ArrayList();
        private final VoidVisitorAdapter<Void> vva = new VoidVisitorAdapter<>() {

            @Override
            public void visit(ClassOrInterfaceType n, Void arg) {
                if (deep) {
                    super.visit(n, arg);
                }
                classOrInterfaceType.add(n);
            }
        };

        public FinderOfClassOrInterfaceType(Method m) {
            this(ParserWithReflectUtilities.find(m), true);
        }

        public FinderOfClassOrInterfaceType(Class<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfClassOrInterfaceType(Class<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfClassOrInterfaceType(Method m, boolean deep) {
            this(ParserWithReflectUtilities.find(m), deep);
        }

        public FinderOfClassOrInterfaceType(Constructor<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfClassOrInterfaceType(Constructor<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), deep);
        }

        /**
         * Dans ce cas, deep vaut true.
         *
         * @param visitable le Visitable dans le lequel chercher les
         * ClassOrInterfaceType
         */
        public FinderOfClassOrInterfaceType(Visitable visitable) {
            this(visitable, false);
        }

        /**
         * Effectue la recherche des ClassOrInterfaceType dans un Visitable, en
         * profondeur ou non selon l'état de {@code deep}.
         * <p>
         * Rechercher en profondeur signifie que si un élément a été trouvé, la
         * recherche continue dans l'élément lui-même. Cela permet de trouver
         * les "if" imbriqués par exemple.
         * <p>
         * Ne pas rechercher en profondeur signifie qu'on s'arrête au premier
         * niveau.
         * <p>
         * Cette propriété n'a pas d'intérêt pour tous les éléments
         *
         * @param visitable le Visitable dans le lequel chercher les
         * ClassOrInterfaceType
         * @param deep recherche en profondeur ou non
         */
        public FinderOfClassOrInterfaceType(Visitable visitable, boolean deep) {
            super(visitable, deep);
            this.visitable.accept(vva, null);
        }

        /**
         * Retourne le tableau des ClassOrInterfaceType trouvés
         *
         * @return le tableau des ClassOrInterfaceType trouvés
         */
        public ArrayList<ClassOrInterfaceType> getClassOrInterfaceType() {
            return classOrInterfaceType;
        }

        /**
         * Retourne le nombre de ClassOrInterfaceType trouvés
         *
         * @return le nombre de ClassOrInterfaceType trouvés
         */
        public int getNumberOfClassOrInterfaceType() {
            return classOrInterfaceType.size();
        }
    }

    /**
     * Pour extraire tous les ClassOrInterfaceDeclaration
     *
     * @author Yvan Maillot &lt;yvan.maillot@uha.fr&gt;
     */
    public static class FinderOfClassOrInterfaceDeclaration extends Finder {

        private final ArrayList<ClassOrInterfaceDeclaration> classOrInterfaceDeclaration = new ArrayList();
        private final VoidVisitorAdapter<Void> vva = new VoidVisitorAdapter<>() {

            @Override
            public void visit(ClassOrInterfaceDeclaration n, Void arg) {
                if (deep) {
                    super.visit(n, arg);
                }
                classOrInterfaceDeclaration.add(n);
            }
        };

        public FinderOfClassOrInterfaceDeclaration(Method m) {
            this(ParserWithReflectUtilities.find(m), true);
        }

        public FinderOfClassOrInterfaceDeclaration(Class<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfClassOrInterfaceDeclaration(Class<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfClassOrInterfaceDeclaration(Method m, boolean deep) {
            this(ParserWithReflectUtilities.find(m), deep);
        }

        public FinderOfClassOrInterfaceDeclaration(Constructor<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfClassOrInterfaceDeclaration(Constructor<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), deep);
        }

        /**
         * Dans ce cas, deep vaut true.
         *
         * @param visitable le Visitable dans le lequel chercher les
         * ClassOrInterfaceDeclaration
         */
        public FinderOfClassOrInterfaceDeclaration(Visitable visitable) {
            this(visitable, false);
        }

        /**
         * Effectue la recherche des ClassOrInterfaceDeclaration dans un
         * Visitable, en profondeur ou non selon l'état de {@code deep}.
         * <p>
         * Rechercher en profondeur signifie que si un élément a été trouvé, la
         * recherche continue dans l'élément lui-même. Cela permet de trouver
         * les "if" imbriqués par exemple.
         * <p>
         * Ne pas rechercher en profondeur signifie qu'on s'arrête au premier
         * niveau.
         * <p>
         * Cette propriété n'a pas d'intérêt pour tous les éléments
         *
         * @param visitable le Visitable dans le lequel chercher les
         * ClassOrInterfaceDeclaration
         * @param deep recherche en profondeur ou non
         */
        public FinderOfClassOrInterfaceDeclaration(Visitable visitable, boolean deep) {
            super(visitable, deep);
            this.visitable.accept(vva, null);
        }

        /**
         * Retourne le tableau des ClassOrInterfaceDeclaration trouvés
         *
         * @return le tableau des ClassOrInterfaceDeclaration trouvés
         */
        public ArrayList<ClassOrInterfaceDeclaration> getClassOrInterfaceDeclaration() {
            return classOrInterfaceDeclaration;
        }

        /**
         * Retourne le nombre de ClassOrInterfaceDeclaration trouvés
         *
         * @return le nombre de ClassOrInterfaceDeclaration trouvés
         */
        public int getNumberOfClassOrInterfaceDeclaration() {
            return classOrInterfaceDeclaration.size();
        }
    }

    /**
     * Pour extraire tous les ClassExpr
     *
     * @author Yvan Maillot &lt;yvan.maillot@uha.fr&gt;
     */
    public static class FinderOfClassExpr extends Finder {

        private final ArrayList<ClassExpr> classExpr = new ArrayList();
        private final VoidVisitorAdapter<Void> vva = new VoidVisitorAdapter<>() {

            @Override
            public void visit(ClassExpr n, Void arg) {
                if (deep) {
                    super.visit(n, arg);
                }
                classExpr.add(n);
            }
        };

        public FinderOfClassExpr(Method m) {
            this(ParserWithReflectUtilities.find(m), true);
        }

        public FinderOfClassExpr(Class<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfClassExpr(Class<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfClassExpr(Method m, boolean deep) {
            this(ParserWithReflectUtilities.find(m), deep);
        }

        public FinderOfClassExpr(Constructor<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfClassExpr(Constructor<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), deep);
        }

        /**
         * Dans ce cas, deep vaut true.
         *
         * @param visitable le Visitable dans le lequel chercher les ClassExpr
         */
        public FinderOfClassExpr(Visitable visitable) {
            this(visitable, false);
        }

        /**
         * Effectue la recherche des ClassExpr dans un Visitable, en profondeur
         * ou non selon l'état de {@code deep}.
         * <p>
         * Rechercher en profondeur signifie que si un élément a été trouvé, la
         * recherche continue dans l'élément lui-même. Cela permet de trouver
         * les "if" imbriqués par exemple.
         * <p>
         * Ne pas rechercher en profondeur signifie qu'on s'arrête au premier
         * niveau.
         * <p>
         * Cette propriété n'a pas d'intérêt pour tous les éléments
         *
         * @param visitable le Visitable dans le lequel chercher les ClassExpr
         * @param deep recherche en profondeur ou non
         */
        public FinderOfClassExpr(Visitable visitable, boolean deep) {
            super(visitable, deep);
            this.visitable.accept(vva, null);
        }

        /**
         * Retourne le tableau des ClassExpr trouvés
         *
         * @return le tableau des ClassExpr trouvés
         */
        public ArrayList<ClassExpr> getClassExpr() {
            return classExpr;
        }

        /**
         * Retourne le nombre de ClassExpr trouvés
         *
         * @return le nombre de ClassExpr trouvés
         */
        public int getNumberOfClassExpr() {
            return classExpr.size();
        }
    }

    /**
     * Pour extraire tous les CharLiteralExpr
     *
     * @author Yvan Maillot &lt;yvan.maillot@uha.fr&gt;
     */
    public static class FinderOfCharLiteralExpr extends Finder {

        private final ArrayList<CharLiteralExpr> charLiteralExpr = new ArrayList();
        private final VoidVisitorAdapter<Void> vva = new VoidVisitorAdapter<>() {

            @Override
            public void visit(CharLiteralExpr n, Void arg) {
                if (deep) {
                    super.visit(n, arg);
                }
                charLiteralExpr.add(n);
            }
        };

        public FinderOfCharLiteralExpr(Method m) {
            this(ParserWithReflectUtilities.find(m), true);
        }

        public FinderOfCharLiteralExpr(Class<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfCharLiteralExpr(Class<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfCharLiteralExpr(Method m, boolean deep) {
            this(ParserWithReflectUtilities.find(m), deep);
        }

        public FinderOfCharLiteralExpr(Constructor<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfCharLiteralExpr(Constructor<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), deep);
        }

        /**
         * Dans ce cas, deep vaut true.
         *
         * @param visitable le Visitable dans le lequel chercher les
         * CharLiteralExpr
         */
        public FinderOfCharLiteralExpr(Visitable visitable) {
            this(visitable, false);
        }

        /**
         * Effectue la recherche des CharLiteralExpr dans un Visitable, en
         * profondeur ou non selon l'état de {@code deep}.
         * <p>
         * Rechercher en profondeur signifie que si un élément a été trouvé, la
         * recherche continue dans l'élément lui-même. Cela permet de trouver
         * les "if" imbriqués par exemple.
         * <p>
         * Ne pas rechercher en profondeur signifie qu'on s'arrête au premier
         * niveau.
         * <p>
         * Cette propriété n'a pas d'intérêt pour tous les éléments
         *
         * @param visitable le Visitable dans le lequel chercher les
         * CharLiteralExpr
         * @param deep recherche en profondeur ou non
         */
        public FinderOfCharLiteralExpr(Visitable visitable, boolean deep) {
            super(visitable, deep);
            this.visitable.accept(vva, null);
        }

        /**
         * Retourne le tableau des CharLiteralExpr trouvés
         *
         * @return le tableau des CharLiteralExpr trouvés
         */
        public ArrayList<CharLiteralExpr> getCharLiteralExpr() {
            return charLiteralExpr;
        }

        /**
         * Retourne le nombre de CharLiteralExpr trouvés
         *
         * @return le nombre de CharLiteralExpr trouvés
         */
        public int getNumberOfCharLiteralExpr() {
            return charLiteralExpr.size();
        }
    }

    /**
     * Pour extraire tous les CatchClause
     *
     * @author Yvan Maillot &lt;yvan.maillot@uha.fr&gt;
     */
    public static class FinderOfCatchClause extends Finder {

        private final ArrayList<CatchClause> catchClause = new ArrayList();
        private final VoidVisitorAdapter<Void> vva = new VoidVisitorAdapter<>() {

            @Override
            public void visit(CatchClause n, Void arg) {
                if (deep) {
                    super.visit(n, arg);
                }
                catchClause.add(n);
            }
        };

        public FinderOfCatchClause(Method m) {
            this(ParserWithReflectUtilities.find(m), true);
        }

        public FinderOfCatchClause(Class<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfCatchClause(Class<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfCatchClause(Method m, boolean deep) {
            this(ParserWithReflectUtilities.find(m), deep);
        }

        public FinderOfCatchClause(Constructor<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfCatchClause(Constructor<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), deep);
        }

        /**
         * Dans ce cas, deep vaut true.
         *
         * @param visitable le Visitable dans le lequel chercher les CatchClause
         */
        public FinderOfCatchClause(Visitable visitable) {
            this(visitable, false);
        }

        /**
         * Effectue la recherche des CatchClause dans un Visitable, en
         * profondeur ou non selon l'état de {@code deep}.
         * <p>
         * Rechercher en profondeur signifie que si un élément a été trouvé, la
         * recherche continue dans l'élément lui-même. Cela permet de trouver
         * les "if" imbriqués par exemple.
         * <p>
         * Ne pas rechercher en profondeur signifie qu'on s'arrête au premier
         * niveau.
         * <p>
         * Cette propriété n'a pas d'intérêt pour tous les éléments
         *
         * @param visitable le Visitable dans le lequel chercher les CatchClause
         * @param deep recherche en profondeur ou non
         */
        public FinderOfCatchClause(Visitable visitable, boolean deep) {
            super(visitable, deep);
            this.visitable.accept(vva, null);
        }

        /**
         * Retourne le tableau des CatchClause trouvés
         *
         * @return le tableau des CatchClause trouvés
         */
        public ArrayList<CatchClause> getCatchClause() {
            return catchClause;
        }

        /**
         * Retourne le nombre de CatchClause trouvés
         *
         * @return le nombre de CatchClause trouvés
         */
        public int getNumberOfCatchClause() {
            return catchClause.size();
        }
    }

    /**
     * Pour extraire tous les CastExpr
     *
     * @author Yvan Maillot &lt;yvan.maillot@uha.fr&gt;
     */
    public static class FinderOfCastExpr extends Finder {

        private final ArrayList<CastExpr> castExpr = new ArrayList();
        private final VoidVisitorAdapter<Void> vva = new VoidVisitorAdapter<>() {

            @Override
            public void visit(CastExpr n, Void arg) {
                if (deep) {
                    super.visit(n, arg);
                }
                castExpr.add(n);
            }
        };

        public FinderOfCastExpr(Method m) {
            this(ParserWithReflectUtilities.find(m), true);
        }

        public FinderOfCastExpr(Class<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfCastExpr(Class<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfCastExpr(Method m, boolean deep) {
            this(ParserWithReflectUtilities.find(m), deep);
        }

        public FinderOfCastExpr(Constructor<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfCastExpr(Constructor<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), deep);
        }

        /**
         * Dans ce cas, deep vaut true.
         *
         * @param visitable le Visitable dans le lequel chercher les CastExpr
         */
        public FinderOfCastExpr(Visitable visitable) {
            this(visitable, false);
        }

        /**
         * Effectue la recherche des CastExpr dans un Visitable, en profondeur
         * ou non selon l'état de {@code deep}.
         * <p>
         * Rechercher en profondeur signifie que si un élément a été trouvé, la
         * recherche continue dans l'élément lui-même. Cela permet de trouver
         * les "if" imbriqués par exemple.
         * <p>
         * Ne pas rechercher en profondeur signifie qu'on s'arrête au premier
         * niveau.
         * <p>
         * Cette propriété n'a pas d'intérêt pour tous les éléments
         *
         * @param visitable le Visitable dans le lequel chercher les CastExpr
         * @param deep recherche en profondeur ou non
         */
        public FinderOfCastExpr(Visitable visitable, boolean deep) {
            super(visitable, deep);
            this.visitable.accept(vva, null);
        }

        /**
         * Retourne le tableau des CastExpr trouvés
         *
         * @return le tableau des CastExpr trouvés
         */
        public ArrayList<CastExpr> getCastExpr() {
            return castExpr;
        }

        /**
         * Retourne le nombre de CastExpr trouvés
         *
         * @return le nombre de CastExpr trouvés
         */
        public int getNumberOfCastExpr() {
            return castExpr.size();
        }
    }

    /**
     * Pour extraire tous les DoubleLiteralExpr
     *
     * @author Yvan Maillot &lt;yvan.maillot@uha.fr&gt;
     */
    public static class FinderOfDoubleLiteralExpr extends Finder {

        private final ArrayList<DoubleLiteralExpr> doubleLiteralExpr = new ArrayList();
        private final VoidVisitorAdapter<Void> vva = new VoidVisitorAdapter<>() {

            @Override
            public void visit(DoubleLiteralExpr n, Void arg) {
                if (deep) {
                    super.visit(n, arg);
                }
                doubleLiteralExpr.add(n);
            }
        };

        public FinderOfDoubleLiteralExpr(Method m) {
            this(ParserWithReflectUtilities.find(m), true);
        }

        public FinderOfDoubleLiteralExpr(Class<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfDoubleLiteralExpr(Class<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfDoubleLiteralExpr(Method m, boolean deep) {
            this(ParserWithReflectUtilities.find(m), deep);
        }

        public FinderOfDoubleLiteralExpr(Constructor<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfDoubleLiteralExpr(Constructor<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), deep);
        }

        /**
         * Dans ce cas, deep vaut true.
         *
         * @param visitable le Visitable dans le lequel chercher les
         * DoubleLiteralExpr
         */
        public FinderOfDoubleLiteralExpr(Visitable visitable) {
            this(visitable, false);
        }

        /**
         * Effectue la recherche des DoubleLiteralExpr dans un Visitable, en
         * profondeur ou non selon l'état de {@code deep}.
         * <p>
         * Rechercher en profondeur signifie que si un élément a été trouvé, la
         * recherche continue dans l'élément lui-même. Cela permet de trouver
         * les "if" imbriqués par exemple.
         * <p>
         * Ne pas rechercher en profondeur signifie qu'on s'arrête au premier
         * niveau.
         * <p>
         * Cette propriété n'a pas d'intérêt pour tous les éléments
         *
         * @param visitable le Visitable dans le lequel chercher les
         * DoubleLiteralExpr
         * @param deep recherche en profondeur ou non
         */
        public FinderOfDoubleLiteralExpr(Visitable visitable, boolean deep) {
            super(visitable, deep);
            this.visitable.accept(vva, null);
        }

        /**
         * Retourne le tableau des DoubleLiteralExpr trouvés
         *
         * @return le tableau des DoubleLiteralExpr trouvés
         */
        public ArrayList<DoubleLiteralExpr> getDoubleLiteralExpr() {
            return doubleLiteralExpr;
        }

        /**
         * Retourne le nombre de DoubleLiteralExpr trouvés
         *
         * @return le nombre de DoubleLiteralExpr trouvés
         */
        public int getNumberOfDoubleLiteralExpr() {
            return doubleLiteralExpr.size();
        }
    }

    /**
     * Pour extraire tous les DoStmt
     *
     * @author Yvan Maillot &lt;yvan.maillot@uha.fr&gt;
     */
    public static class FinderOfDoStmt extends Finder {

        private final ArrayList<DoStmt> doStmt = new ArrayList();
        private final VoidVisitorAdapter<Void> vva = new VoidVisitorAdapter<>() {

            @Override
            public void visit(DoStmt n, Void arg) {
                if (deep) {
                    super.visit(n, arg);
                }
                doStmt.add(n);
            }
        };

        public FinderOfDoStmt(Method m) {
            this(ParserWithReflectUtilities.find(m), true);
        }

        public FinderOfDoStmt(Class<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfDoStmt(Class<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfDoStmt(Method m, boolean deep) {
            this(ParserWithReflectUtilities.find(m), deep);
        }

        public FinderOfDoStmt(Constructor<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfDoStmt(Constructor<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), deep);
        }

        /**
         * Dans ce cas, deep vaut true.
         *
         * @param visitable le Visitable dans le lequel chercher les DoStmt
         */
        public FinderOfDoStmt(Visitable visitable) {
            this(visitable, false);
        }

        /**
         * Effectue la recherche des DoStmt dans un Visitable, en profondeur ou
         * non selon l'état de {@code deep}.
         * <p>
         * Rechercher en profondeur signifie que si un élément a été trouvé, la
         * recherche continue dans l'élément lui-même. Cela permet de trouver
         * les "if" imbriqués par exemple.
         * <p>
         * Ne pas rechercher en profondeur signifie qu'on s'arrête au premier
         * niveau.
         * <p>
         * Cette propriété n'a pas d'intérêt pour tous les éléments
         *
         * @param visitable le Visitable dans le lequel chercher les DoStmt
         * @param deep recherche en profondeur ou non
         */
        public FinderOfDoStmt(Visitable visitable, boolean deep) {
            super(visitable, deep);
            this.visitable.accept(vva, null);
        }

        /**
         * Retourne le tableau des DoStmt trouvés
         *
         * @return le tableau des DoStmt trouvés
         */
        public ArrayList<DoStmt> getDoStmt() {
            return doStmt;
        }

        /**
         * Retourne le nombre de DoStmt trouvés
         *
         * @return le nombre de DoStmt trouvés
         */
        public int getNumberOfDoStmt() {
            return doStmt.size();
        }
    }

    /**
     * Pour extraire tous les ContinueStmt
     *
     * @author Yvan Maillot &lt;yvan.maillot@uha.fr&gt;
     */
    public static class FinderOfContinueStmt extends Finder {

        private final ArrayList<ContinueStmt> continueStmt = new ArrayList();
        private final VoidVisitorAdapter<Void> vva = new VoidVisitorAdapter<>() {

            @Override
            public void visit(ContinueStmt n, Void arg) {
                if (deep) {
                    super.visit(n, arg);
                }
                continueStmt.add(n);
            }
        };

        public FinderOfContinueStmt(Method m) {
            this(ParserWithReflectUtilities.find(m), true);
        }

        public FinderOfContinueStmt(Class<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfContinueStmt(Class<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfContinueStmt(Method m, boolean deep) {
            this(ParserWithReflectUtilities.find(m), deep);
        }

        public FinderOfContinueStmt(Constructor<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfContinueStmt(Constructor<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), deep);
        }

        /**
         * Dans ce cas, deep vaut true.
         *
         * @param visitable le Visitable dans le lequel chercher les
         * ContinueStmt
         */
        public FinderOfContinueStmt(Visitable visitable) {
            this(visitable, false);
        }

        /**
         * Effectue la recherche des ContinueStmt dans un Visitable, en
         * profondeur ou non selon l'état de {@code deep}.
         * <p>
         * Rechercher en profondeur signifie que si un élément a été trouvé, la
         * recherche continue dans l'élément lui-même. Cela permet de trouver
         * les "if" imbriqués par exemple.
         * <p>
         * Ne pas rechercher en profondeur signifie qu'on s'arrête au premier
         * niveau.
         * <p>
         * Cette propriété n'a pas d'intérêt pour tous les éléments
         *
         * @param visitable le Visitable dans le lequel chercher les
         * ContinueStmt
         * @param deep recherche en profondeur ou non
         */
        public FinderOfContinueStmt(Visitable visitable, boolean deep) {
            super(visitable, deep);
            this.visitable.accept(vva, null);
        }

        /**
         * Retourne le tableau des ContinueStmt trouvés
         *
         * @return le tableau des ContinueStmt trouvés
         */
        public ArrayList<ContinueStmt> getContinueStmt() {
            return continueStmt;
        }

        /**
         * Retourne le nombre de ContinueStmt trouvés
         *
         * @return le nombre de ContinueStmt trouvés
         */
        public int getNumberOfContinueStmt() {
            return continueStmt.size();
        }
    }

    /**
     * Pour extraire tous les ConstructorDeclaration
     *
     * @author Yvan Maillot &lt;yvan.maillot@uha.fr&gt;
     */
    public static class FinderOfConstructorDeclaration extends Finder {

        private final ArrayList<ConstructorDeclaration> constructorDeclaration = new ArrayList();
        private final VoidVisitorAdapter<Void> vva = new VoidVisitorAdapter<>() {

            @Override
            public void visit(ConstructorDeclaration n, Void arg) {
                if (deep) {
                    super.visit(n, arg);
                }
                constructorDeclaration.add(n);
            }
        };

        public FinderOfConstructorDeclaration(Method m) {
            this(ParserWithReflectUtilities.find(m), true);
        }

        public FinderOfConstructorDeclaration(Class<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfConstructorDeclaration(Class<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfConstructorDeclaration(Method m, boolean deep) {
            this(ParserWithReflectUtilities.find(m), deep);
        }

        public FinderOfConstructorDeclaration(Constructor<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfConstructorDeclaration(Constructor<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), deep);
        }

        /**
         * Dans ce cas, deep vaut true.
         *
         * @param visitable le Visitable dans le lequel chercher les
         * ConstructorDeclaration
         */
        public FinderOfConstructorDeclaration(Visitable visitable) {
            this(visitable, false);
        }

        /**
         * Effectue la recherche des ConstructorDeclaration dans un Visitable,
         * en profondeur ou non selon l'état de {@code deep}.
         * <p>
         * Rechercher en profondeur signifie que si un élément a été trouvé, la
         * recherche continue dans l'élément lui-même. Cela permet de trouver
         * les "if" imbriqués par exemple.
         * <p>
         * Ne pas rechercher en profondeur signifie qu'on s'arrête au premier
         * niveau.
         * <p>
         * Cette propriété n'a pas d'intérêt pour tous les éléments
         *
         * @param visitable le Visitable dans le lequel chercher les
         * ConstructorDeclaration
         * @param deep recherche en profondeur ou non
         */
        public FinderOfConstructorDeclaration(Visitable visitable, boolean deep) {
            super(visitable, deep);
            this.visitable.accept(vva, null);
        }

        /**
         * Retourne le tableau des ConstructorDeclaration trouvés
         *
         * @return le tableau des ConstructorDeclaration trouvés
         */
        public ArrayList<ConstructorDeclaration> getConstructorDeclaration() {
            return constructorDeclaration;
        }

        /**
         * Retourne le nombre de ConstructorDeclaration trouvés
         *
         * @return le nombre de ConstructorDeclaration trouvés
         */
        public int getNumberOfConstructorDeclaration() {
            return constructorDeclaration.size();
        }
    }

    /**
     * Pour extraire tous les ConditionalExpr
     *
     * @author Yvan Maillot &lt;yvan.maillot@uha.fr&gt;
     */
    public static class FinderOfConditionalExpr extends Finder {

        private final ArrayList<ConditionalExpr> conditionalExpr = new ArrayList();
        private final VoidVisitorAdapter<Void> vva = new VoidVisitorAdapter<>() {

            @Override
            public void visit(ConditionalExpr n, Void arg) {
                if (deep) {
                    super.visit(n, arg);
                }
                conditionalExpr.add(n);
            }
        };

        public FinderOfConditionalExpr(Method m) {
            this(ParserWithReflectUtilities.find(m), true);
        }

        public FinderOfConditionalExpr(Class<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfConditionalExpr(Class<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfConditionalExpr(Method m, boolean deep) {
            this(ParserWithReflectUtilities.find(m), deep);
        }

        public FinderOfConditionalExpr(Constructor<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfConditionalExpr(Constructor<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), deep);
        }

        /**
         * Dans ce cas, deep vaut true.
         *
         * @param visitable le Visitable dans le lequel chercher les
         * ConditionalExpr
         */
        public FinderOfConditionalExpr(Visitable visitable) {
            this(visitable, false);
        }

        /**
         * Effectue la recherche des ConditionalExpr dans un Visitable, en
         * profondeur ou non selon l'état de {@code deep}.
         * <p>
         * Rechercher en profondeur signifie que si un élément a été trouvé, la
         * recherche continue dans l'élément lui-même. Cela permet de trouver
         * les "if" imbriqués par exemple.
         * <p>
         * Ne pas rechercher en profondeur signifie qu'on s'arrête au premier
         * niveau.
         * <p>
         * Cette propriété n'a pas d'intérêt pour tous les éléments
         *
         * @param visitable le Visitable dans le lequel chercher les
         * ConditionalExpr
         * @param deep recherche en profondeur ou non
         */
        public FinderOfConditionalExpr(Visitable visitable, boolean deep) {
            super(visitable, deep);
            this.visitable.accept(vva, null);
        }

        /**
         * Retourne le tableau des ConditionalExpr trouvés
         *
         * @return le tableau des ConditionalExpr trouvés
         */
        public ArrayList<ConditionalExpr> getConditionalExpr() {
            return conditionalExpr;
        }

        /**
         * Retourne le nombre de ConditionalExpr trouvés
         *
         * @return le nombre de ConditionalExpr trouvés
         */
        public int getNumberOfConditionalExpr() {
            return conditionalExpr.size();
        }
    }

    /**
     * Pour extraire tous les CompilationUnit
     *
     * @author Yvan Maillot &lt;yvan.maillot@uha.fr&gt;
     */
    public static class FinderOfCompilationUnit extends Finder {

        private final ArrayList<CompilationUnit> compilationUnit = new ArrayList();
        private final VoidVisitorAdapter<Void> vva = new VoidVisitorAdapter<>() {

            @Override
            public void visit(CompilationUnit n, Void arg) {
                if (deep) {
                    super.visit(n, arg);
                }
                compilationUnit.add(n);
            }
        };

        public FinderOfCompilationUnit(Method m) {
            this(ParserWithReflectUtilities.find(m), true);
        }

        public FinderOfCompilationUnit(Class<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfCompilationUnit(Class<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfCompilationUnit(Method m, boolean deep) {
            this(ParserWithReflectUtilities.find(m), deep);
        }

        public FinderOfCompilationUnit(Constructor<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfCompilationUnit(Constructor<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), deep);
        }

        /**
         * Dans ce cas, deep vaut true.
         *
         * @param visitable le Visitable dans le lequel chercher les
         * CompilationUnit
         */
        public FinderOfCompilationUnit(Visitable visitable) {
            this(visitable, false);
        }

        /**
         * Effectue la recherche des CompilationUnit dans un Visitable, en
         * profondeur ou non selon l'état de {@code deep}.
         * <p>
         * Rechercher en profondeur signifie que si un élément a été trouvé, la
         * recherche continue dans l'élément lui-même. Cela permet de trouver
         * les "if" imbriqués par exemple.
         * <p>
         * Ne pas rechercher en profondeur signifie qu'on s'arrête au premier
         * niveau.
         * <p>
         * Cette propriété n'a pas d'intérêt pour tous les éléments
         *
         * @param visitable le Visitable dans le lequel chercher les
         * CompilationUnit
         * @param deep recherche en profondeur ou non
         */
        public FinderOfCompilationUnit(Visitable visitable, boolean deep) {
            super(visitable, deep);
            this.visitable.accept(vva, null);
        }

        /**
         * Retourne le tableau des CompilationUnit trouvés
         *
         * @return le tableau des CompilationUnit trouvés
         */
        public ArrayList<CompilationUnit> getCompilationUnit() {
            return compilationUnit;
        }

        /**
         * Retourne le nombre de CompilationUnit trouvés
         *
         * @return le nombre de CompilationUnit trouvés
         */
        public int getNumberOfCompilationUnit() {
            return compilationUnit.size();
        }
    }

    /**
     * Pour extraire tous les AssertStmt
     *
     * @author Yvan Maillot &lt;yvan.maillot@uha.fr&gt;
     */
    public static class FinderOfAssertStmt extends Finder {

        private final ArrayList<AssertStmt> assertStmt = new ArrayList();
        private final VoidVisitorAdapter<Void> vva = new VoidVisitorAdapter<>() {

            @Override
            public void visit(AssertStmt n, Void arg) {
                if (deep) {
                    super.visit(n, arg);
                }
                assertStmt.add(n);
            }
        };

        public FinderOfAssertStmt(Method m) {
            this(ParserWithReflectUtilities.find(m), true);
        }

        public FinderOfAssertStmt(Class<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfAssertStmt(Class<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfAssertStmt(Method m, boolean deep) {
            this(ParserWithReflectUtilities.find(m), deep);
        }

        public FinderOfAssertStmt(Constructor<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfAssertStmt(Constructor<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), deep);
        }

        /**
         * Dans ce cas, deep vaut true.
         *
         * @param visitable le Visitable dans le lequel chercher les AssertStmt
         */
        public FinderOfAssertStmt(Visitable visitable) {
            this(visitable, false);
        }

        /**
         * Effectue la recherche des AssertStmt dans un Visitable, en profondeur
         * ou non selon l'état de {@code deep}.
         * <p>
         * Rechercher en profondeur signifie que si un élément a été trouvé, la
         * recherche continue dans l'élément lui-même. Cela permet de trouver
         * les "if" imbriqués par exemple.
         * <p>
         * Ne pas rechercher en profondeur signifie qu'on s'arrête au premier
         * niveau.
         * <p>
         * Cette propriété n'a pas d'intérêt pour tous les éléments
         *
         * @param visitable le Visitable dans le lequel chercher les AssertStmt
         * @param deep recherche en profondeur ou non
         */
        public FinderOfAssertStmt(Visitable visitable, boolean deep) {
            super(visitable, deep);
            this.visitable.accept(vva, null);
        }

        /**
         * Retourne le tableau des AssertStmt trouvés
         *
         * @return le tableau des AssertStmt trouvés
         */
        public ArrayList<AssertStmt> getAssertStmt() {
            return assertStmt;
        }

        /**
         * Retourne le nombre de AssertStmt trouvés
         *
         * @return le nombre de AssertStmt trouvés
         */
        public int getNumberOfAssertStmt() {
            return assertStmt.size();
        }
    }

    /**
     * Pour extraire tous les ArrayInitializerExpr
     *
     * @author Yvan Maillot &lt;yvan.maillot@uha.fr&gt;
     */
    public static class FinderOfArrayInitializerExpr extends Finder {

        private final ArrayList<ArrayInitializerExpr> arrayInitializerExpr = new ArrayList();
        private final VoidVisitorAdapter<Void> vva = new VoidVisitorAdapter<>() {

            @Override
            public void visit(ArrayInitializerExpr n, Void arg) {
                if (deep) {
                    super.visit(n, arg);
                }
                arrayInitializerExpr.add(n);
            }
        };

        public FinderOfArrayInitializerExpr(Method m) {
            this(ParserWithReflectUtilities.find(m), true);
        }

        public FinderOfArrayInitializerExpr(Class<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfArrayInitializerExpr(Class<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfArrayInitializerExpr(Method m, boolean deep) {
            this(ParserWithReflectUtilities.find(m), deep);
        }

        public FinderOfArrayInitializerExpr(Constructor<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfArrayInitializerExpr(Constructor<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), deep);
        }

        /**
         * Dans ce cas, deep vaut true.
         *
         * @param visitable le Visitable dans le lequel chercher les
         * ArrayInitializerExpr
         */
        public FinderOfArrayInitializerExpr(Visitable visitable) {
            this(visitable, false);
        }

        /**
         * Effectue la recherche des ArrayInitializerExpr dans un Visitable, en
         * profondeur ou non selon l'état de {@code deep}.
         * <p>
         * Rechercher en profondeur signifie que si un élément a été trouvé, la
         * recherche continue dans l'élément lui-même. Cela permet de trouver
         * les "if" imbriqués par exemple.
         * <p>
         * Ne pas rechercher en profondeur signifie qu'on s'arrête au premier
         * niveau.
         * <p>
         * Cette propriété n'a pas d'intérêt pour tous les éléments
         *
         * @param visitable le Visitable dans le lequel chercher les
         * ArrayInitializerExpr
         * @param deep recherche en profondeur ou non
         */
        public FinderOfArrayInitializerExpr(Visitable visitable, boolean deep) {
            super(visitable, deep);
            this.visitable.accept(vva, null);
        }

        /**
         * Retourne le tableau des ArrayInitializerExpr trouvés
         *
         * @return le tableau des ArrayInitializerExpr trouvés
         */
        public ArrayList<ArrayInitializerExpr> getArrayInitializerExpr() {
            return arrayInitializerExpr;
        }

        /**
         * Retourne le nombre de ArrayInitializerExpr trouvés
         *
         * @return le nombre de ArrayInitializerExpr trouvés
         */
        public int getNumberOfArrayInitializerExpr() {
            return arrayInitializerExpr.size();
        }
    }

    /**
     * Pour extraire tous les ArrayCreationExpr
     *
     * @author Yvan Maillot &lt;yvan.maillot@uha.fr&gt;
     */
    public static class FinderOfArrayCreationExpr extends Finder {

        private final ArrayList<ArrayCreationExpr> arrayCreationExpr = new ArrayList();
        private final VoidVisitorAdapter<Void> vva = new VoidVisitorAdapter<>() {

            @Override
            public void visit(ArrayCreationExpr n, Void arg) {
                if (deep) {
                    super.visit(n, arg);
                }
                arrayCreationExpr.add(n);
            }
        };

        public FinderOfArrayCreationExpr(Method m) {
            this(ParserWithReflectUtilities.find(m), true);
        }

        public FinderOfArrayCreationExpr(Class<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfArrayCreationExpr(Class<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfArrayCreationExpr(Method m, boolean deep) {
            this(ParserWithReflectUtilities.find(m), deep);
        }

        public FinderOfArrayCreationExpr(Constructor<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfArrayCreationExpr(Constructor<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), deep);
        }

        /**
         * Dans ce cas, deep vaut true.
         *
         * @param visitable le Visitable dans le lequel chercher les
         * ArrayCreationExpr
         */
        public FinderOfArrayCreationExpr(Visitable visitable) {
            this(visitable, false);
        }

        /**
         * Effectue la recherche des ArrayCreationExpr dans un Visitable, en
         * profondeur ou non selon l'état de {@code deep}.
         * <p>
         * Rechercher en profondeur signifie que si un élément a été trouvé, la
         * recherche continue dans l'élément lui-même. Cela permet de trouver
         * les "if" imbriqués par exemple.
         * <p>
         * Ne pas rechercher en profondeur signifie qu'on s'arrête au premier
         * niveau.
         * <p>
         * Cette propriété n'a pas d'intérêt pour tous les éléments
         *
         * @param visitable le Visitable dans le lequel chercher les
         * ArrayCreationExpr
         * @param deep recherche en profondeur ou non
         */
        public FinderOfArrayCreationExpr(Visitable visitable, boolean deep) {
            super(visitable, deep);
            this.visitable.accept(vva, null);
        }

        /**
         * Retourne le tableau des ArrayCreationExpr trouvés
         *
         * @return le tableau des ArrayCreationExpr trouvés
         */
        public ArrayList<ArrayCreationExpr> getArrayCreationExpr() {
            return arrayCreationExpr;
        }

        /**
         * Retourne le nombre de ArrayCreationExpr trouvés
         *
         * @return le nombre de ArrayCreationExpr trouvés
         */
        public int getNumberOfArrayCreationExpr() {
            return arrayCreationExpr.size();
        }
    }

    /**
     * Pour extraire tous les ArrayAccessExpr
     *
     * @author Yvan Maillot &lt;yvan.maillot@uha.fr&gt;
     */
    public static class FinderOfArrayAccessExpr extends Finder {

        private final ArrayList<ArrayAccessExpr> arrayAccessExpr = new ArrayList();
        private final VoidVisitorAdapter<Void> vva = new VoidVisitorAdapter<>() {

            @Override
            public void visit(ArrayAccessExpr n, Void arg) {
                if (deep) {
                    super.visit(n, arg);
                }
                arrayAccessExpr.add(n);
            }
        };

        public FinderOfArrayAccessExpr(Method m) {
            this(ParserWithReflectUtilities.find(m), true);
        }

        public FinderOfArrayAccessExpr(Class<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfArrayAccessExpr(Class<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfArrayAccessExpr(Method m, boolean deep) {
            this(ParserWithReflectUtilities.find(m), deep);
        }

        public FinderOfArrayAccessExpr(Constructor<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfArrayAccessExpr(Constructor<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), deep);
        }

        /**
         * Dans ce cas, deep vaut true.
         *
         * @param visitable le Visitable dans le lequel chercher les
         * ArrayAccessExpr
         */
        public FinderOfArrayAccessExpr(Visitable visitable) {
            this(visitable, false);
        }

        /**
         * Effectue la recherche des ArrayAccessExpr dans un Visitable, en
         * profondeur ou non selon l'état de {@code deep}.
         * <p>
         * Rechercher en profondeur signifie que si un élément a été trouvé, la
         * recherche continue dans l'élément lui-même. Cela permet de trouver
         * les "if" imbriqués par exemple.
         * <p>
         * Ne pas rechercher en profondeur signifie qu'on s'arrête au premier
         * niveau.
         * <p>
         * Cette propriété n'a pas d'intérêt pour tous les éléments
         *
         * @param visitable le Visitable dans le lequel chercher les
         * ArrayAccessExpr
         * @param deep recherche en profondeur ou non
         */
        public FinderOfArrayAccessExpr(Visitable visitable, boolean deep) {
            super(visitable, deep);
            this.visitable.accept(vva, null);
        }

        /**
         * Retourne le tableau des ArrayAccessExpr trouvés
         *
         * @return le tableau des ArrayAccessExpr trouvés
         */
        public ArrayList<ArrayAccessExpr> getArrayAccessExpr() {
            return arrayAccessExpr;
        }

        /**
         * Retourne le nombre de ArrayAccessExpr trouvés
         *
         * @return le nombre de ArrayAccessExpr trouvés
         */
        public int getNumberOfArrayAccessExpr() {
            return arrayAccessExpr.size();
        }
    }

    /**
     * Pour extraire tous les AnnotationMemberDeclaration
     *
     * @author Yvan Maillot &lt;yvan.maillot@uha.fr&gt;
     */
    public static class FinderOfAnnotationMemberDeclaration extends Finder {

        private final ArrayList<AnnotationMemberDeclaration> annotationMemberDeclaration = new ArrayList();
        private final VoidVisitorAdapter<Void> vva = new VoidVisitorAdapter<>() {

            @Override
            public void visit(AnnotationMemberDeclaration n, Void arg) {
                if (deep) {
                    super.visit(n, arg);
                }
                annotationMemberDeclaration.add(n);
            }
        };

        public FinderOfAnnotationMemberDeclaration(Method m) {
            this(ParserWithReflectUtilities.find(m), true);
        }

        public FinderOfAnnotationMemberDeclaration(Class<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfAnnotationMemberDeclaration(Class<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfAnnotationMemberDeclaration(Method m, boolean deep) {
            this(ParserWithReflectUtilities.find(m), deep);
        }

        public FinderOfAnnotationMemberDeclaration(Constructor<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfAnnotationMemberDeclaration(Constructor<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), deep);
        }

        /**
         * Dans ce cas, deep vaut true.
         *
         * @param visitable le Visitable dans le lequel chercher les
         * AnnotationMemberDeclaration
         */
        public FinderOfAnnotationMemberDeclaration(Visitable visitable) {
            this(visitable, false);
        }

        /**
         * Effectue la recherche des AnnotationMemberDeclaration dans un
         * Visitable, en profondeur ou non selon l'état de {@code deep}.
         * <p>
         * Rechercher en profondeur signifie que si un élément a été trouvé, la
         * recherche continue dans l'élément lui-même. Cela permet de trouver
         * les "if" imbriqués par exemple.
         * <p>
         * Ne pas rechercher en profondeur signifie qu'on s'arrête au premier
         * niveau.
         * <p>
         * Cette propriété n'a pas d'intérêt pour tous les éléments
         *
         * @param visitable le Visitable dans le lequel chercher les
         * AnnotationMemberDeclaration
         * @param deep recherche en profondeur ou non
         */
        public FinderOfAnnotationMemberDeclaration(Visitable visitable, boolean deep) {
            super(visitable, deep);
            this.visitable.accept(vva, null);
        }

        /**
         * Retourne le tableau des AnnotationMemberDeclaration trouvés
         *
         * @return le tableau des AnnotationMemberDeclaration trouvés
         */
        public ArrayList<AnnotationMemberDeclaration> getAnnotationMemberDeclaration() {
            return annotationMemberDeclaration;
        }

        /**
         * Retourne le nombre de AnnotationMemberDeclaration trouvés
         *
         * @return le nombre de AnnotationMemberDeclaration trouvés
         */
        public int getNumberOfAnnotationMemberDeclaration() {
            return annotationMemberDeclaration.size();
        }
    }

    /**
     * Pour extraire tous les AnnotationDeclaration
     *
     * @author Yvan Maillot &lt;yvan.maillot@uha.fr&gt;
     */
    public static class FinderOfAnnotationDeclaration extends Finder {

        private final ArrayList<AnnotationDeclaration> annotationDeclaration = new ArrayList();
        private final VoidVisitorAdapter<Void> vva = new VoidVisitorAdapter<>() {

            @Override
            public void visit(AnnotationDeclaration n, Void arg) {
                if (deep) {
                    super.visit(n, arg);
                }
                annotationDeclaration.add(n);
            }
        };

        public FinderOfAnnotationDeclaration(Method m) {
            this(ParserWithReflectUtilities.find(m), true);
        }

        public FinderOfAnnotationDeclaration(Class<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfAnnotationDeclaration(Class<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfAnnotationDeclaration(Method m, boolean deep) {
            this(ParserWithReflectUtilities.find(m), deep);
        }

        public FinderOfAnnotationDeclaration(Constructor<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfAnnotationDeclaration(Constructor<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), deep);
        }

        /**
         * Dans ce cas, deep vaut true.
         *
         * @param visitable le Visitable dans le lequel chercher les
         * AnnotationDeclaration
         */
        public FinderOfAnnotationDeclaration(Visitable visitable) {
            this(visitable, false);
        }

        /**
         * Effectue la recherche des AnnotationDeclaration dans un Visitable, en
         * profondeur ou non selon l'état de {@code deep}.
         * <p>
         * Rechercher en profondeur signifie que si un élément a été trouvé, la
         * recherche continue dans l'élément lui-même. Cela permet de trouver
         * les "if" imbriqués par exemple.
         * <p>
         * Ne pas rechercher en profondeur signifie qu'on s'arrête au premier
         * niveau.
         * <p>
         * Cette propriété n'a pas d'intérêt pour tous les éléments
         *
         * @param visitable le Visitable dans le lequel chercher les
         * AnnotationDeclaration
         * @param deep recherche en profondeur ou non
         */
        public FinderOfAnnotationDeclaration(Visitable visitable, boolean deep) {
            super(visitable, deep);
            this.visitable.accept(vva, null);
        }

        /**
         * Retourne le tableau des AnnotationDeclaration trouvés
         *
         * @return le tableau des AnnotationDeclaration trouvés
         */
        public ArrayList<AnnotationDeclaration> getAnnotationDeclaration() {
            return annotationDeclaration;
        }

        /**
         * Retourne le nombre de AnnotationDeclaration trouvés
         *
         * @return le nombre de AnnotationDeclaration trouvés
         */
        public int getNumberOfAnnotationDeclaration() {
            return annotationDeclaration.size();
        }
    }

    /**
     * Pour extraire tous les BreakStmt
     *
     * @author Yvan Maillot &lt;yvan.maillot@uha.fr&gt;
     */
    public static class FinderOfBreakStmt extends Finder {

        private final ArrayList<BreakStmt> breakStmt = new ArrayList();
        private final VoidVisitorAdapter<Void> vva = new VoidVisitorAdapter<>() {

            @Override
            public void visit(BreakStmt n, Void arg) {
                if (deep) {
                    super.visit(n, arg);
                }
                breakStmt.add(n);
            }
        };

        public FinderOfBreakStmt(Method m) {
            this(ParserWithReflectUtilities.find(m), true);
        }

        public FinderOfBreakStmt(Class<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfBreakStmt(Class<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfBreakStmt(Method m, boolean deep) {
            this(ParserWithReflectUtilities.find(m), deep);
        }

        public FinderOfBreakStmt(Constructor<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfBreakStmt(Constructor<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), deep);
        }

        /**
         * Dans ce cas, deep vaut true.
         *
         * @param visitable le Visitable dans le lequel chercher les BreakStmt
         */
        public FinderOfBreakStmt(Visitable visitable) {
            this(visitable, false);
        }

        /**
         * Effectue la recherche des BreakStmt dans un Visitable, en profondeur
         * ou non selon l'état de {@code deep}.
         * <p>
         * Rechercher en profondeur signifie que si un élément a été trouvé, la
         * recherche continue dans l'élément lui-même. Cela permet de trouver
         * les "if" imbriqués par exemple.
         * <p>
         * Ne pas rechercher en profondeur signifie qu'on s'arrête au premier
         * niveau.
         * <p>
         * Cette propriété n'a pas d'intérêt pour tous les éléments
         *
         * @param visitable le Visitable dans le lequel chercher les BreakStmt
         * @param deep recherche en profondeur ou non
         */
        public FinderOfBreakStmt(Visitable visitable, boolean deep) {
            super(visitable, deep);
            this.visitable.accept(vva, null);
        }

        /**
         * Retourne le tableau des BreakStmt trouvés
         *
         * @return le tableau des BreakStmt trouvés
         */
        public ArrayList<BreakStmt> getBreakStmt() {
            return breakStmt;
        }

        /**
         * Retourne le nombre de BreakStmt trouvés
         *
         * @return le nombre de BreakStmt trouvés
         */
        public int getNumberOfBreakStmt() {
            return breakStmt.size();
        }
    }

    /**
     * Pour extraire tous les BooleanLiteralExpr
     *
     * @author Yvan Maillot &lt;yvan.maillot@uha.fr&gt;
     */
    public static class FinderOfBooleanLiteralExpr extends Finder {

        private final ArrayList<BooleanLiteralExpr> booleanLiteralExpr = new ArrayList();
        private final VoidVisitorAdapter<Void> vva = new VoidVisitorAdapter<>() {

            @Override
            public void visit(BooleanLiteralExpr n, Void arg) {
                if (deep) {
                    super.visit(n, arg);
                }
                booleanLiteralExpr.add(n);
            }
        };

        public FinderOfBooleanLiteralExpr(Method m) {
            this(ParserWithReflectUtilities.find(m), true);
        }

        public FinderOfBooleanLiteralExpr(Class<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfBooleanLiteralExpr(Class<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfBooleanLiteralExpr(Method m, boolean deep) {
            this(ParserWithReflectUtilities.find(m), deep);
        }

        public FinderOfBooleanLiteralExpr(Constructor<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfBooleanLiteralExpr(Constructor<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), deep);
        }

        /**
         * Dans ce cas, deep vaut true.
         *
         * @param visitable le Visitable dans le lequel chercher les
         * BooleanLiteralExpr
         */
        public FinderOfBooleanLiteralExpr(Visitable visitable) {
            this(visitable, false);
        }

        /**
         * Effectue la recherche des BooleanLiteralExpr dans un Visitable, en
         * profondeur ou non selon l'état de {@code deep}.
         * <p>
         * Rechercher en profondeur signifie que si un élément a été trouvé, la
         * recherche continue dans l'élément lui-même. Cela permet de trouver
         * les "if" imbriqués par exemple.
         * <p>
         * Ne pas rechercher en profondeur signifie qu'on s'arrête au premier
         * niveau.
         * <p>
         * Cette propriété n'a pas d'intérêt pour tous les éléments
         *
         * @param visitable le Visitable dans le lequel chercher les
         * BooleanLiteralExpr
         * @param deep recherche en profondeur ou non
         */
        public FinderOfBooleanLiteralExpr(Visitable visitable, boolean deep) {
            super(visitable, deep);
            this.visitable.accept(vva, null);
        }

        /**
         * Retourne le tableau des BooleanLiteralExpr trouvés
         *
         * @return le tableau des BooleanLiteralExpr trouvés
         */
        public ArrayList<BooleanLiteralExpr> getBooleanLiteralExpr() {
            return booleanLiteralExpr;
        }

        /**
         * Retourne le nombre de BooleanLiteralExpr trouvés
         *
         * @return le nombre de BooleanLiteralExpr trouvés
         */
        public int getNumberOfBooleanLiteralExpr() {
            return booleanLiteralExpr.size();
        }
    }

    /**
     * Pour extraire tous les BlockStmt
     *
     * @author Yvan Maillot &lt;yvan.maillot@uha.fr&gt;
     */
    public static class FinderOfBlockStmt extends Finder {

        private final ArrayList<BlockStmt> blockStmt = new ArrayList();
        private final VoidVisitorAdapter<Void> vva = new VoidVisitorAdapter<>() {

            @Override
            public void visit(BlockStmt n, Void arg) {
                if (deep) {
                    super.visit(n, arg);
                }
                blockStmt.add(n);
            }
        };

        public FinderOfBlockStmt(Method m) {
            this(ParserWithReflectUtilities.find(m), true);
        }

        public FinderOfBlockStmt(Class<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfBlockStmt(Class<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfBlockStmt(Method m, boolean deep) {
            this(ParserWithReflectUtilities.find(m), deep);
        }

        public FinderOfBlockStmt(Constructor<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfBlockStmt(Constructor<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), deep);
        }

        /**
         * Dans ce cas, deep vaut true.
         *
         * @param visitable le Visitable dans le lequel chercher les BlockStmt
         */
        public FinderOfBlockStmt(Visitable visitable) {
            this(visitable, false);
        }

        /**
         * Effectue la recherche des BlockStmt dans un Visitable, en profondeur
         * ou non selon l'état de {@code deep}.
         * <p>
         * Rechercher en profondeur signifie que si un élément a été trouvé, la
         * recherche continue dans l'élément lui-même. Cela permet de trouver
         * les "if" imbriqués par exemple.
         * <p>
         * Ne pas rechercher en profondeur signifie qu'on s'arrête au premier
         * niveau.
         * <p>
         * Cette propriété n'a pas d'intérêt pour tous les éléments
         *
         * @param visitable le Visitable dans le lequel chercher les BlockStmt
         * @param deep recherche en profondeur ou non
         */
        public FinderOfBlockStmt(Visitable visitable, boolean deep) {
            super(visitable, deep);
            this.visitable.accept(vva, null);
        }

        /**
         * Retourne le tableau des BlockStmt trouvés
         *
         * @return le tableau des BlockStmt trouvés
         */
        public ArrayList<BlockStmt> getBlockStmt() {
            return blockStmt;
        }

        /**
         * Retourne le nombre de BlockStmt trouvés
         *
         * @return le nombre de BlockStmt trouvés
         */
        public int getNumberOfBlockStmt() {
            return blockStmt.size();
        }
    }

    /**
     * Pour extraire tous les BlockComment
     *
     * @author Yvan Maillot &lt;yvan.maillot@uha.fr&gt;
     */
    public static class FinderOfBlockComment extends Finder {

        private final ArrayList<BlockComment> blockComment = new ArrayList();
        private final VoidVisitorAdapter<Void> vva = new VoidVisitorAdapter<>() {

            @Override
            public void visit(BlockComment n, Void arg) {
                if (deep) {
                    super.visit(n, arg);
                }
                blockComment.add(n);
            }
        };

        public FinderOfBlockComment(Method m) {
            this(ParserWithReflectUtilities.find(m), true);
        }

        public FinderOfBlockComment(Class<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfBlockComment(Class<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfBlockComment(Method m, boolean deep) {
            this(ParserWithReflectUtilities.find(m), deep);
        }

        public FinderOfBlockComment(Constructor<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfBlockComment(Constructor<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), deep);
        }

        /**
         * Dans ce cas, deep vaut true.
         *
         * @param visitable le Visitable dans le lequel chercher les
         * BlockComment
         */
        public FinderOfBlockComment(Visitable visitable) {
            this(visitable, false);
        }

        /**
         * Effectue la recherche des BlockComment dans un Visitable, en
         * profondeur ou non selon l'état de {@code deep}.
         * <p>
         * Rechercher en profondeur signifie que si un élément a été trouvé, la
         * recherche continue dans l'élément lui-même. Cela permet de trouver
         * les "if" imbriqués par exemple.
         * <p>
         * Ne pas rechercher en profondeur signifie qu'on s'arrête au premier
         * niveau.
         * <p>
         * Cette propriété n'a pas d'intérêt pour tous les éléments
         *
         * @param visitable le Visitable dans le lequel chercher les
         * BlockComment
         * @param deep recherche en profondeur ou non
         */
        public FinderOfBlockComment(Visitable visitable, boolean deep) {
            super(visitable, deep);
            this.visitable.accept(vva, null);
        }

        /**
         * Retourne le tableau des BlockComment trouvés
         *
         * @return le tableau des BlockComment trouvés
         */
        public ArrayList<BlockComment> getBlockComment() {
            return blockComment;
        }

        /**
         * Retourne le nombre de BlockComment trouvés
         *
         * @return le nombre de BlockComment trouvés
         */
        public int getNumberOfBlockComment() {
            return blockComment.size();
        }
    }

    /**
     * Pour extraire tous les BinaryExpr
     *
     * @author Yvan Maillot &lt;yvan.maillot@uha.fr&gt;
     */
    public static class FinderOfBinaryExpr extends Finder {

        private final ArrayList<BinaryExpr> binaryExpr = new ArrayList();
        private final VoidVisitorAdapter<Void> vva = new VoidVisitorAdapter<>() {

            @Override
            public void visit(BinaryExpr n, Void arg) {
                if (deep) {
                    super.visit(n, arg);
                }
                binaryExpr.add(n);
            }
        };

        public FinderOfBinaryExpr(Method m) {
            this(ParserWithReflectUtilities.find(m), true);
        }

        public FinderOfBinaryExpr(Class<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfBinaryExpr(Class<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfBinaryExpr(Method m, boolean deep) {
            this(ParserWithReflectUtilities.find(m), deep);
        }

        public FinderOfBinaryExpr(Constructor<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfBinaryExpr(Constructor<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), deep);
        }

        /**
         * Dans ce cas, deep vaut true.
         *
         * @param visitable le Visitable dans le lequel chercher les BinaryExpr
         */
        public FinderOfBinaryExpr(Visitable visitable) {
            this(visitable, false);
        }

        /**
         * Effectue la recherche des BinaryExpr dans un Visitable, en profondeur
         * ou non selon l'état de {@code deep}.
         * <p>
         * Rechercher en profondeur signifie que si un élément a été trouvé, la
         * recherche continue dans l'élément lui-même. Cela permet de trouver
         * les "if" imbriqués par exemple.
         * <p>
         * Ne pas rechercher en profondeur signifie qu'on s'arrête au premier
         * niveau.
         * <p>
         * Cette propriété n'a pas d'intérêt pour tous les éléments
         *
         * @param visitable le Visitable dans le lequel chercher les BinaryExpr
         * @param deep recherche en profondeur ou non
         */
        public FinderOfBinaryExpr(Visitable visitable, boolean deep) {
            super(visitable, deep);
            this.visitable.accept(vva, null);
        }

        /**
         * Retourne le tableau des BinaryExpr trouvés
         *
         * @return le tableau des BinaryExpr trouvés
         */
        public ArrayList<BinaryExpr> getBinaryExpr() {
            return binaryExpr;
        }

        /**
         * Retourne le nombre de BinaryExpr trouvés
         *
         * @return le nombre de BinaryExpr trouvés
         */
        public int getNumberOfBinaryExpr() {
            return binaryExpr.size();
        }
    }

    /**
     * Pour extraire tous les AssignExpr
     *
     * @author Yvan Maillot &lt;yvan.maillot@uha.fr&gt;
     */
    public static class FinderOfAssignExpr extends Finder {

        private final ArrayList<AssignExpr> assignExpr = new ArrayList();
        private final VoidVisitorAdapter<Void> vva = new VoidVisitorAdapter<>() {

            @Override
            public void visit(AssignExpr n, Void arg) {
                if (deep) {
                    super.visit(n, arg);
                }
                assignExpr.add(n);
            }
        };

        public FinderOfAssignExpr(Method m) {
            this(ParserWithReflectUtilities.find(m), true);
        }

        public FinderOfAssignExpr(Class<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfAssignExpr(Class<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfAssignExpr(Method m, boolean deep) {
            this(ParserWithReflectUtilities.find(m), deep);
        }

        public FinderOfAssignExpr(Constructor<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfAssignExpr(Constructor<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), deep);
        }

        /**
         * Dans ce cas, deep vaut true.
         *
         * @param visitable le Visitable dans le lequel chercher les AssignExpr
         */
        public FinderOfAssignExpr(Visitable visitable) {
            this(visitable, false);
        }

        /**
         * Effectue la recherche des AssignExpr dans un Visitable, en profondeur
         * ou non selon l'état de {@code deep}.
         * <p>
         * Rechercher en profondeur signifie que si un élément a été trouvé, la
         * recherche continue dans l'élément lui-même. Cela permet de trouver
         * les "if" imbriqués par exemple.
         * <p>
         * Ne pas rechercher en profondeur signifie qu'on s'arrête au premier
         * niveau.
         * <p>
         * Cette propriété n'a pas d'intérêt pour tous les éléments
         *
         * @param visitable le Visitable dans le lequel chercher les AssignExpr
         * @param deep recherche en profondeur ou non
         */
        public FinderOfAssignExpr(Visitable visitable, boolean deep) {
            super(visitable, deep);
            this.visitable.accept(vva, null);
        }

        /**
         * Retourne le tableau des AssignExpr trouvés
         *
         * @return le tableau des AssignExpr trouvés
         */
        public ArrayList<AssignExpr> getAssignExpr() {
            return assignExpr;
        }

        /**
         * Retourne le nombre de AssignExpr trouvés
         *
         * @return le nombre de AssignExpr trouvés
         */
        public int getNumberOfAssignExpr() {
            return assignExpr.size();
        }
    }

    /**
     * Pour extraire tous les LongLiteralExpr
     *
     * @author Yvan Maillot &lt;yvan.maillot@uha.fr&gt;
     */
    public static class FinderOfLongLiteralExpr extends Finder {

        private final ArrayList<LongLiteralExpr> longLiteralExpr = new ArrayList();
        private final VoidVisitorAdapter<Void> vva = new VoidVisitorAdapter<>() {

            @Override
            public void visit(LongLiteralExpr n, Void arg) {
                if (deep) {
                    super.visit(n, arg);
                }
                longLiteralExpr.add(n);
            }
        };

        public FinderOfLongLiteralExpr(Method m) {
            this(ParserWithReflectUtilities.find(m), true);
        }

        public FinderOfLongLiteralExpr(Class<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfLongLiteralExpr(Class<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfLongLiteralExpr(Method m, boolean deep) {
            this(ParserWithReflectUtilities.find(m), deep);
        }

        public FinderOfLongLiteralExpr(Constructor<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfLongLiteralExpr(Constructor<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), deep);
        }

        /**
         * Dans ce cas, deep vaut true.
         *
         * @param visitable le Visitable dans le lequel chercher les
         * LongLiteralExpr
         */
        public FinderOfLongLiteralExpr(Visitable visitable) {
            this(visitable, false);
        }

        /**
         * Effectue la recherche des LongLiteralExpr dans un Visitable, en
         * profondeur ou non selon l'état de {@code deep}.
         * <p>
         * Rechercher en profondeur signifie que si un élément a été trouvé, la
         * recherche continue dans l'élément lui-même. Cela permet de trouver
         * les "if" imbriqués par exemple.
         * <p>
         * Ne pas rechercher en profondeur signifie qu'on s'arrête au premier
         * niveau.
         * <p>
         * Cette propriété n'a pas d'intérêt pour tous les éléments
         *
         * @param visitable le Visitable dans le lequel chercher les
         * LongLiteralExpr
         * @param deep recherche en profondeur ou non
         */
        public FinderOfLongLiteralExpr(Visitable visitable, boolean deep) {
            super(visitable, deep);
            this.visitable.accept(vva, null);
        }

        /**
         * Retourne le tableau des LongLiteralExpr trouvés
         *
         * @return le tableau des LongLiteralExpr trouvés
         */
        public ArrayList<LongLiteralExpr> getLongLiteralExpr() {
            return longLiteralExpr;
        }

        /**
         * Retourne le nombre de LongLiteralExpr trouvés
         *
         * @return le nombre de LongLiteralExpr trouvés
         */
        public int getNumberOfLongLiteralExpr() {
            return longLiteralExpr.size();
        }
    }

    /**
     * Pour extraire tous les LineComment
     *
     * @author Yvan Maillot &lt;yvan.maillot@uha.fr&gt;
     */
    public static class FinderOfLineComment extends Finder {

        private final ArrayList<LineComment> lineComment = new ArrayList();
        private final VoidVisitorAdapter<Void> vva = new VoidVisitorAdapter<>() {

            @Override
            public void visit(LineComment n, Void arg) {
                if (deep) {
                    super.visit(n, arg);
                }
                lineComment.add(n);
            }
        };

        public FinderOfLineComment(Method m) {
            this(ParserWithReflectUtilities.find(m), true);
        }

        public FinderOfLineComment(Class<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfLineComment(Class<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfLineComment(Method m, boolean deep) {
            this(ParserWithReflectUtilities.find(m), deep);
        }

        public FinderOfLineComment(Constructor<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfLineComment(Constructor<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), deep);
        }

        /**
         * Dans ce cas, deep vaut true.
         *
         * @param visitable le Visitable dans le lequel chercher les LineComment
         */
        public FinderOfLineComment(Visitable visitable) {
            this(visitable, false);
        }

        /**
         * Effectue la recherche des LineComment dans un Visitable, en
         * profondeur ou non selon l'état de {@code deep}.
         * <p>
         * Rechercher en profondeur signifie que si un élément a été trouvé, la
         * recherche continue dans l'élément lui-même. Cela permet de trouver
         * les "if" imbriqués par exemple.
         * <p>
         * Ne pas rechercher en profondeur signifie qu'on s'arrête au premier
         * niveau.
         * <p>
         * Cette propriété n'a pas d'intérêt pour tous les éléments
         *
         * @param visitable le Visitable dans le lequel chercher les LineComment
         * @param deep recherche en profondeur ou non
         */
        public FinderOfLineComment(Visitable visitable, boolean deep) {
            super(visitable, deep);
            this.visitable.accept(vva, null);
        }

        /**
         * Retourne le tableau des LineComment trouvés
         *
         * @return le tableau des LineComment trouvés
         */
        public ArrayList<LineComment> getLineComment() {
            return lineComment;
        }

        /**
         * Retourne le nombre de LineComment trouvés
         *
         * @return le nombre de LineComment trouvés
         */
        public int getNumberOfLineComment() {
            return lineComment.size();
        }
    }

    /**
     * Pour extraire tous les LabeledStmt
     *
     * @author Yvan Maillot &lt;yvan.maillot@uha.fr&gt;
     */
    public static class FinderOfLabeledStmt extends Finder {

        private final ArrayList<LabeledStmt> labeledStmt = new ArrayList();
        private final VoidVisitorAdapter<Void> vva = new VoidVisitorAdapter<>() {

            @Override
            public void visit(LabeledStmt n, Void arg) {
                if (deep) {
                    super.visit(n, arg);
                }
                labeledStmt.add(n);
            }
        };

        public FinderOfLabeledStmt(Method m) {
            this(ParserWithReflectUtilities.find(m), true);
        }

        public FinderOfLabeledStmt(Class<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfLabeledStmt(Class<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfLabeledStmt(Method m, boolean deep) {
            this(ParserWithReflectUtilities.find(m), deep);
        }

        public FinderOfLabeledStmt(Constructor<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfLabeledStmt(Constructor<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), deep);
        }

        /**
         * Dans ce cas, deep vaut true.
         *
         * @param visitable le Visitable dans le lequel chercher les LabeledStmt
         */
        public FinderOfLabeledStmt(Visitable visitable) {
            this(visitable, false);
        }

        /**
         * Effectue la recherche des LabeledStmt dans un Visitable, en
         * profondeur ou non selon l'état de {@code deep}.
         * <p>
         * Rechercher en profondeur signifie que si un élément a été trouvé, la
         * recherche continue dans l'élément lui-même. Cela permet de trouver
         * les "if" imbriqués par exemple.
         * <p>
         * Ne pas rechercher en profondeur signifie qu'on s'arrête au premier
         * niveau.
         * <p>
         * Cette propriété n'a pas d'intérêt pour tous les éléments
         *
         * @param visitable le Visitable dans le lequel chercher les LabeledStmt
         * @param deep recherche en profondeur ou non
         */
        public FinderOfLabeledStmt(Visitable visitable, boolean deep) {
            super(visitable, deep);
            this.visitable.accept(vva, null);
        }

        /**
         * Retourne le tableau des LabeledStmt trouvés
         *
         * @return le tableau des LabeledStmt trouvés
         */
        public ArrayList<LabeledStmt> getLabeledStmt() {
            return labeledStmt;
        }

        /**
         * Retourne le nombre de LabeledStmt trouvés
         *
         * @return le nombre de LabeledStmt trouvés
         */
        public int getNumberOfLabeledStmt() {
            return labeledStmt.size();
        }
    }

    /**
     * Pour extraire tous les JavadocComment
     *
     * @author Yvan Maillot &lt;yvan.maillot@uha.fr&gt;
     */
    public static class FinderOfJavadocComment extends Finder {

        private final ArrayList<JavadocComment> javadocComment = new ArrayList();
        private final VoidVisitorAdapter<Void> vva = new VoidVisitorAdapter<>() {

            @Override
            public void visit(JavadocComment n, Void arg) {
                if (deep) {
                    super.visit(n, arg);
                }
                javadocComment.add(n);
            }
        };

        public FinderOfJavadocComment(Method m) {
            this(ParserWithReflectUtilities.find(m), true);
        }

        public FinderOfJavadocComment(Class<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfJavadocComment(Class<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfJavadocComment(Method m, boolean deep) {
            this(ParserWithReflectUtilities.find(m), deep);
        }

        public FinderOfJavadocComment(Constructor<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfJavadocComment(Constructor<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), deep);
        }

        /**
         * Dans ce cas, deep vaut true.
         *
         * @param visitable le Visitable dans le lequel chercher les
         * JavadocComment
         */
        public FinderOfJavadocComment(Visitable visitable) {
            this(visitable, false);
        }

        /**
         * Effectue la recherche des JavadocComment dans un Visitable, en
         * profondeur ou non selon l'état de {@code deep}.
         * <p>
         * Rechercher en profondeur signifie que si un élément a été trouvé, la
         * recherche continue dans l'élément lui-même. Cela permet de trouver
         * les "if" imbriqués par exemple.
         * <p>
         * Ne pas rechercher en profondeur signifie qu'on s'arrête au premier
         * niveau.
         * <p>
         * Cette propriété n'a pas d'intérêt pour tous les éléments
         *
         * @param visitable le Visitable dans le lequel chercher les
         * JavadocComment
         * @param deep recherche en profondeur ou non
         */
        public FinderOfJavadocComment(Visitable visitable, boolean deep) {
            super(visitable, deep);
            this.visitable.accept(vva, null);
        }

        /**
         * Retourne le tableau des JavadocComment trouvés
         *
         * @return le tableau des JavadocComment trouvés
         */
        public ArrayList<JavadocComment> getJavadocComment() {
            return javadocComment;
        }

        /**
         * Retourne le nombre de JavadocComment trouvés
         *
         * @return le nombre de JavadocComment trouvés
         */
        public int getNumberOfJavadocComment() {
            return javadocComment.size();
        }
    }

    /**
     * Pour extraire tous les IntegerLiteralExpr
     *
     * @author Yvan Maillot &lt;yvan.maillot@uha.fr&gt;
     */
    public static class FinderOfIntegerLiteralExpr extends Finder {

        private final ArrayList<IntegerLiteralExpr> integerLiteralExpr = new ArrayList();
        private final VoidVisitorAdapter<Void> vva = new VoidVisitorAdapter<>() {

            @Override
            public void visit(IntegerLiteralExpr n, Void arg) {
                if (deep) {
                    super.visit(n, arg);
                }
                integerLiteralExpr.add(n);
            }
        };

        public FinderOfIntegerLiteralExpr(Method m) {
            this(ParserWithReflectUtilities.find(m), true);
        }

        public FinderOfIntegerLiteralExpr(Class<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfIntegerLiteralExpr(Class<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfIntegerLiteralExpr(Method m, boolean deep) {
            this(ParserWithReflectUtilities.find(m), deep);
        }

        public FinderOfIntegerLiteralExpr(Constructor<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfIntegerLiteralExpr(Constructor<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), deep);
        }

        /**
         * Dans ce cas, deep vaut true.
         *
         * @param visitable le Visitable dans le lequel chercher les
         * IntegerLiteralExpr
         */
        public FinderOfIntegerLiteralExpr(Visitable visitable) {
            this(visitable, false);
        }

        /**
         * Effectue la recherche des IntegerLiteralExpr dans un Visitable, en
         * profondeur ou non selon l'état de {@code deep}.
         * <p>
         * Rechercher en profondeur signifie que si un élément a été trouvé, la
         * recherche continue dans l'élément lui-même. Cela permet de trouver
         * les "if" imbriqués par exemple.
         * <p>
         * Ne pas rechercher en profondeur signifie qu'on s'arrête au premier
         * niveau.
         * <p>
         * Cette propriété n'a pas d'intérêt pour tous les éléments
         *
         * @param visitable le Visitable dans le lequel chercher les
         * IntegerLiteralExpr
         * @param deep recherche en profondeur ou non
         */
        public FinderOfIntegerLiteralExpr(Visitable visitable, boolean deep) {
            super(visitable, deep);
            this.visitable.accept(vva, null);
        }

        /**
         * Retourne le tableau des IntegerLiteralExpr trouvés
         *
         * @return le tableau des IntegerLiteralExpr trouvés
         */
        public ArrayList<IntegerLiteralExpr> getIntegerLiteralExpr() {
            return integerLiteralExpr;
        }

        /**
         * Retourne le nombre de IntegerLiteralExpr trouvés
         *
         * @return le nombre de IntegerLiteralExpr trouvés
         */
        public int getNumberOfIntegerLiteralExpr() {
            return integerLiteralExpr.size();
        }
    }

    /**
     * Pour extraire tous les InstanceOfExpr
     *
     * @author Yvan Maillot &lt;yvan.maillot@uha.fr&gt;
     */
    public static class FinderOfInstanceOfExpr extends Finder {

        private final ArrayList<InstanceOfExpr> instanceOfExpr = new ArrayList();
        private final VoidVisitorAdapter<Void> vva = new VoidVisitorAdapter<>() {

            @Override
            public void visit(InstanceOfExpr n, Void arg) {
                if (deep) {
                    super.visit(n, arg);
                }
                instanceOfExpr.add(n);
            }
        };

        public FinderOfInstanceOfExpr(Method m) {
            this(ParserWithReflectUtilities.find(m), true);
        }

        public FinderOfInstanceOfExpr(Class<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfInstanceOfExpr(Class<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfInstanceOfExpr(Method m, boolean deep) {
            this(ParserWithReflectUtilities.find(m), deep);
        }

        public FinderOfInstanceOfExpr(Constructor<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfInstanceOfExpr(Constructor<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), deep);
        }

        /**
         * Dans ce cas, deep vaut true.
         *
         * @param visitable le Visitable dans le lequel chercher les
         * InstanceOfExpr
         */
        public FinderOfInstanceOfExpr(Visitable visitable) {
            this(visitable, false);
        }

        /**
         * Effectue la recherche des InstanceOfExpr dans un Visitable, en
         * profondeur ou non selon l'état de {@code deep}.
         * <p>
         * Rechercher en profondeur signifie que si un élément a été trouvé, la
         * recherche continue dans l'élément lui-même. Cela permet de trouver
         * les "if" imbriqués par exemple.
         * <p>
         * Ne pas rechercher en profondeur signifie qu'on s'arrête au premier
         * niveau.
         * <p>
         * Cette propriété n'a pas d'intérêt pour tous les éléments
         *
         * @param visitable le Visitable dans le lequel chercher les
         * InstanceOfExpr
         * @param deep recherche en profondeur ou non
         */
        public FinderOfInstanceOfExpr(Visitable visitable, boolean deep) {
            super(visitable, deep);
            this.visitable.accept(vva, null);
        }

        /**
         * Retourne le tableau des InstanceOfExpr trouvés
         *
         * @return le tableau des InstanceOfExpr trouvés
         */
        public ArrayList<InstanceOfExpr> getInstanceOfExpr() {
            return instanceOfExpr;
        }

        /**
         * Retourne le nombre de InstanceOfExpr trouvés
         *
         * @return le nombre de InstanceOfExpr trouvés
         */
        public int getNumberOfInstanceOfExpr() {
            return instanceOfExpr.size();
        }
    }

    /**
     * Pour extraire tous les NormalAnnotationExpr
     *
     * @author Yvan Maillot &lt;yvan.maillot@uha.fr&gt;
     */
    public static class FinderOfNormalAnnotationExpr extends Finder {

        private final ArrayList<NormalAnnotationExpr> normalAnnotationExpr = new ArrayList();
        private final VoidVisitorAdapter<Void> vva = new VoidVisitorAdapter<>() {

            @Override
            public void visit(NormalAnnotationExpr n, Void arg) {
                if (deep) {
                    super.visit(n, arg);
                }
                normalAnnotationExpr.add(n);
            }
        };

        public FinderOfNormalAnnotationExpr(Method m) {
            this(ParserWithReflectUtilities.find(m), true);
        }

        public FinderOfNormalAnnotationExpr(Class<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfNormalAnnotationExpr(Class<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfNormalAnnotationExpr(Method m, boolean deep) {
            this(ParserWithReflectUtilities.find(m), deep);
        }

        public FinderOfNormalAnnotationExpr(Constructor<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfNormalAnnotationExpr(Constructor<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), deep);
        }

        /**
         * Dans ce cas, deep vaut true.
         *
         * @param visitable le Visitable dans le lequel chercher les
         * NormalAnnotationExpr
         */
        public FinderOfNormalAnnotationExpr(Visitable visitable) {
            this(visitable, false);
        }

        /**
         * Effectue la recherche des NormalAnnotationExpr dans un Visitable, en
         * profondeur ou non selon l'état de {@code deep}.
         * <p>
         * Rechercher en profondeur signifie que si un élément a été trouvé, la
         * recherche continue dans l'élément lui-même. Cela permet de trouver
         * les "if" imbriqués par exemple.
         * <p>
         * Ne pas rechercher en profondeur signifie qu'on s'arrête au premier
         * niveau.
         * <p>
         * Cette propriété n'a pas d'intérêt pour tous les éléments
         *
         * @param visitable le Visitable dans le lequel chercher les
         * NormalAnnotationExpr
         * @param deep recherche en profondeur ou non
         */
        public FinderOfNormalAnnotationExpr(Visitable visitable, boolean deep) {
            super(visitable, deep);
            this.visitable.accept(vva, null);
        }

        /**
         * Retourne le tableau des NormalAnnotationExpr trouvés
         *
         * @return le tableau des NormalAnnotationExpr trouvés
         */
        public ArrayList<NormalAnnotationExpr> getNormalAnnotationExpr() {
            return normalAnnotationExpr;
        }

        /**
         * Retourne le nombre de NormalAnnotationExpr trouvés
         *
         * @return le nombre de NormalAnnotationExpr trouvés
         */
        public int getNumberOfNormalAnnotationExpr() {
            return normalAnnotationExpr.size();
        }
    }

    /**
     * Pour extraire tous les NameExpr
     *
     * @author Yvan Maillot &lt;yvan.maillot@uha.fr&gt;
     */
    public static class FinderOfNameExpr extends Finder {

        private final ArrayList<NameExpr> nameExpr = new ArrayList();
        private final VoidVisitorAdapter<Void> vva = new VoidVisitorAdapter<>() {

            @Override
            public void visit(NameExpr n, Void arg) {
                if (deep) {
                    super.visit(n, arg);
                }
                nameExpr.add(n);
            }
        };

        public FinderOfNameExpr(Method m) {
            this(ParserWithReflectUtilities.find(m), true);
        }

        public FinderOfNameExpr(Class<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfNameExpr(Class<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfNameExpr(Method m, boolean deep) {
            this(ParserWithReflectUtilities.find(m), deep);
        }

        public FinderOfNameExpr(Constructor<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfNameExpr(Constructor<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), deep);
        }

        /**
         * Dans ce cas, deep vaut true.
         *
         * @param visitable le Visitable dans le lequel chercher les NameExpr
         */
        public FinderOfNameExpr(Visitable visitable) {
            this(visitable, false);
        }

        /**
         * Effectue la recherche des NameExpr dans un Visitable, en profondeur
         * ou non selon l'état de {@code deep}.
         * <p>
         * Rechercher en profondeur signifie que si un élément a été trouvé, la
         * recherche continue dans l'élément lui-même. Cela permet de trouver
         * les "if" imbriqués par exemple.
         * <p>
         * Ne pas rechercher en profondeur signifie qu'on s'arrête au premier
         * niveau.
         * <p>
         * Cette propriété n'a pas d'intérêt pour tous les éléments
         *
         * @param visitable le Visitable dans le lequel chercher les NameExpr
         * @param deep recherche en profondeur ou non
         */
        public FinderOfNameExpr(Visitable visitable, boolean deep) {
            super(visitable, deep);
            this.visitable.accept(vva, null);
        }

        /**
         * Retourne le tableau des NameExpr trouvés
         *
         * @return le tableau des NameExpr trouvés
         */
        public ArrayList<NameExpr> getNameExpr() {
            return nameExpr;
        }

        /**
         * Retourne le nombre de NameExpr trouvés
         *
         * @return le nombre de NameExpr trouvés
         */
        public int getNumberOfNameExpr() {
            return nameExpr.size();
        }
    }

    /**
     * Pour extraire tous les MethodDeclaration
     *
     * @author Yvan Maillot &lt;yvan.maillot@uha.fr&gt;
     */
    public static class FinderOfMethodDeclaration extends Finder {

        private final ArrayList<MethodDeclaration> methodDeclaration = new ArrayList();
        private final VoidVisitorAdapter<Void> vva = new VoidVisitorAdapter<>() {

            @Override
            public void visit(MethodDeclaration n, Void arg) {
                if (deep) {
                    super.visit(n, arg);
                }
                methodDeclaration.add(n);
            }
        };

        public FinderOfMethodDeclaration(Method m) {
            this(ParserWithReflectUtilities.find(m), true);
        }

        public FinderOfMethodDeclaration(Class<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfMethodDeclaration(Class<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfMethodDeclaration(Method m, boolean deep) {
            this(ParserWithReflectUtilities.find(m), deep);
        }

        public FinderOfMethodDeclaration(Constructor<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfMethodDeclaration(Constructor<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), deep);
        }

        /**
         * Dans ce cas, deep vaut true.
         *
         * @param visitable le Visitable dans le lequel chercher les
         * MethodDeclaration
         */
        public FinderOfMethodDeclaration(Visitable visitable) {
            this(visitable, false);
        }

        /**
         * Effectue la recherche des MethodDeclaration dans un Visitable, en
         * profondeur ou non selon l'état de {@code deep}.
         * <p>
         * Rechercher en profondeur signifie que si un élément a été trouvé, la
         * recherche continue dans l'élément lui-même. Cela permet de trouver
         * les "if" imbriqués par exemple.
         * <p>
         * Ne pas rechercher en profondeur signifie qu'on s'arrête au premier
         * niveau.
         * <p>
         * Cette propriété n'a pas d'intérêt pour tous les éléments
         *
         * @param visitable le Visitable dans le lequel chercher les
         * MethodDeclaration
         * @param deep recherche en profondeur ou non
         */
        public FinderOfMethodDeclaration(Visitable visitable, boolean deep) {
            super(visitable, deep);
            this.visitable.accept(vva, null);
        }

        /**
         * Retourne le tableau des MethodDeclaration trouvés
         *
         * @return le tableau des MethodDeclaration trouvés
         */
        public ArrayList<MethodDeclaration> getMethodDeclaration() {
            return methodDeclaration;
        }

        /**
         * Retourne le nombre de MethodDeclaration trouvés
         *
         * @return le nombre de MethodDeclaration trouvés
         */
        public int getNumberOfMethodDeclaration() {
            return methodDeclaration.size();
        }
    }

    /**
     * Pour extraire tous les MethodCallExpr
     *
     * @author Yvan Maillot &lt;yvan.maillot@uha.fr&gt;
     */
    public static class FinderOfMethodCallExpr extends Finder {

        private final ArrayList<MethodCallExpr> methodCallExpr = new ArrayList();
        private final VoidVisitorAdapter<Void> vva = new VoidVisitorAdapter<>() {

            @Override
            public void visit(MethodCallExpr n, Void arg) {
                if (deep) {
                    super.visit(n, arg);
                }
                methodCallExpr.add(n);
            }
        };

        public FinderOfMethodCallExpr(Method m) {
            this(ParserWithReflectUtilities.find(m), true);
        }

        public FinderOfMethodCallExpr(Class<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfMethodCallExpr(Class<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfMethodCallExpr(Method m, boolean deep) {
            this(ParserWithReflectUtilities.find(m), deep);
        }

        public FinderOfMethodCallExpr(Constructor<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfMethodCallExpr(Constructor<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), deep);
        }

        /**
         * Dans ce cas, deep vaut true.
         *
         * @param visitable le Visitable dans le lequel chercher les
         * MethodCallExpr
         */
        public FinderOfMethodCallExpr(Visitable visitable) {
            this(visitable, false);
        }

        /**
         * Effectue la recherche des MethodCallExpr dans un Visitable, en
         * profondeur ou non selon l'état de {@code deep}.
         * <p>
         * Rechercher en profondeur signifie que si un élément a été trouvé, la
         * recherche continue dans l'élément lui-même. Cela permet de trouver
         * les "if" imbriqués par exemple.
         * <p>
         * Ne pas rechercher en profondeur signifie qu'on s'arrête au premier
         * niveau.
         * <p>
         * Cette propriété n'a pas d'intérêt pour tous les éléments
         *
         * @param visitable le Visitable dans le lequel chercher les
         * MethodCallExpr
         * @param deep recherche en profondeur ou non
         */
        public FinderOfMethodCallExpr(Visitable visitable, boolean deep) {
            super(visitable, deep);
            this.visitable.accept(vva, null);
        }

        /**
         * Retourne le tableau des MethodCallExpr trouvés
         *
         * @return le tableau des MethodCallExpr trouvés
         */
        public ArrayList<MethodCallExpr> getMethodCallExpr() {
            return methodCallExpr;
        }

        /**
         * Retourne le nombre de MethodCallExpr trouvés
         *
         * @return le nombre de MethodCallExpr trouvés
         */
        public int getNumberOfMethodCallExpr() {
            return methodCallExpr.size();
        }
    }

    /**
     * Pour extraire tous les MemberValuePair
     *
     * @author Yvan Maillot &lt;yvan.maillot@uha.fr&gt;
     */
    public static class FinderOfMemberValuePair extends Finder {

        private final ArrayList<MemberValuePair> memberValuePair = new ArrayList();
        private final VoidVisitorAdapter<Void> vva = new VoidVisitorAdapter<>() {

            @Override
            public void visit(MemberValuePair n, Void arg) {
                if (deep) {
                    super.visit(n, arg);
                }
                memberValuePair.add(n);
            }
        };

        public FinderOfMemberValuePair(Method m) {
            this(ParserWithReflectUtilities.find(m), true);
        }

        public FinderOfMemberValuePair(Class<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfMemberValuePair(Class<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfMemberValuePair(Method m, boolean deep) {
            this(ParserWithReflectUtilities.find(m), deep);
        }

        public FinderOfMemberValuePair(Constructor<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfMemberValuePair(Constructor<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), deep);
        }

        /**
         * Dans ce cas, deep vaut true.
         *
         * @param visitable le Visitable dans le lequel chercher les
         * MemberValuePair
         */
        public FinderOfMemberValuePair(Visitable visitable) {
            this(visitable, false);
        }

        /**
         * Effectue la recherche des MemberValuePair dans un Visitable, en
         * profondeur ou non selon l'état de {@code deep}.
         * <p>
         * Rechercher en profondeur signifie que si un élément a été trouvé, la
         * recherche continue dans l'élément lui-même. Cela permet de trouver
         * les "if" imbriqués par exemple.
         * <p>
         * Ne pas rechercher en profondeur signifie qu'on s'arrête au premier
         * niveau.
         * <p>
         * Cette propriété n'a pas d'intérêt pour tous les éléments
         *
         * @param visitable le Visitable dans le lequel chercher les
         * MemberValuePair
         * @param deep recherche en profondeur ou non
         */
        public FinderOfMemberValuePair(Visitable visitable, boolean deep) {
            super(visitable, deep);
            this.visitable.accept(vva, null);
        }

        /**
         * Retourne le tableau des MemberValuePair trouvés
         *
         * @return le tableau des MemberValuePair trouvés
         */
        public ArrayList<MemberValuePair> getMemberValuePair() {
            return memberValuePair;
        }

        /**
         * Retourne le nombre de MemberValuePair trouvés
         *
         * @return le nombre de MemberValuePair trouvés
         */
        public int getNumberOfMemberValuePair() {
            return memberValuePair.size();
        }
    }

    /**
     * Pour extraire tous les MarkerAnnotationExpr
     *
     * @author Yvan Maillot &lt;yvan.maillot@uha.fr&gt;
     */
    public static class FinderOfMarkerAnnotationExpr extends Finder {

        private final ArrayList<MarkerAnnotationExpr> markerAnnotationExpr = new ArrayList();
        private final VoidVisitorAdapter<Void> vva = new VoidVisitorAdapter<>() {

            @Override
            public void visit(MarkerAnnotationExpr n, Void arg) {
                if (deep) {
                    super.visit(n, arg);
                }
                markerAnnotationExpr.add(n);
            }
        };

        public FinderOfMarkerAnnotationExpr(Method m) {
            this(ParserWithReflectUtilities.find(m), true);
        }

        public FinderOfMarkerAnnotationExpr(Class<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfMarkerAnnotationExpr(Class<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfMarkerAnnotationExpr(Method m, boolean deep) {
            this(ParserWithReflectUtilities.find(m), deep);
        }

        public FinderOfMarkerAnnotationExpr(Constructor<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfMarkerAnnotationExpr(Constructor<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), deep);
        }

        /**
         * Dans ce cas, deep vaut true.
         *
         * @param visitable le Visitable dans le lequel chercher les
         * MarkerAnnotationExpr
         */
        public FinderOfMarkerAnnotationExpr(Visitable visitable) {
            this(visitable, false);
        }

        /**
         * Effectue la recherche des MarkerAnnotationExpr dans un Visitable, en
         * profondeur ou non selon l'état de {@code deep}.
         * <p>
         * Rechercher en profondeur signifie que si un élément a été trouvé, la
         * recherche continue dans l'élément lui-même. Cela permet de trouver
         * les "if" imbriqués par exemple.
         * <p>
         * Ne pas rechercher en profondeur signifie qu'on s'arrête au premier
         * niveau.
         * <p>
         * Cette propriété n'a pas d'intérêt pour tous les éléments
         *
         * @param visitable le Visitable dans le lequel chercher les
         * MarkerAnnotationExpr
         * @param deep recherche en profondeur ou non
         */
        public FinderOfMarkerAnnotationExpr(Visitable visitable, boolean deep) {
            super(visitable, deep);
            this.visitable.accept(vva, null);
        }

        /**
         * Retourne le tableau des MarkerAnnotationExpr trouvés
         *
         * @return le tableau des MarkerAnnotationExpr trouvés
         */
        public ArrayList<MarkerAnnotationExpr> getMarkerAnnotationExpr() {
            return markerAnnotationExpr;
        }

        /**
         * Retourne le nombre de MarkerAnnotationExpr trouvés
         *
         * @return le nombre de MarkerAnnotationExpr trouvés
         */
        public int getNumberOfMarkerAnnotationExpr() {
            return markerAnnotationExpr.size();
        }
    }

    /**
     * Pour extraire tous les ExpressionStmt
     *
     * @author Yvan Maillot &lt;yvan.maillot@uha.fr&gt;
     */
    public static class FinderOfExpressionStmt extends Finder {

        private final ArrayList<ExpressionStmt> expressionStmt = new ArrayList();
        private final VoidVisitorAdapter<Void> vva = new VoidVisitorAdapter<>() {

            @Override
            public void visit(ExpressionStmt n, Void arg) {
                if (deep) {
                    super.visit(n, arg);
                }
                expressionStmt.add(n);
            }
        };

        public FinderOfExpressionStmt(Method m) {
            this(ParserWithReflectUtilities.find(m), true);
        }

        public FinderOfExpressionStmt(Class<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfExpressionStmt(Class<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfExpressionStmt(Method m, boolean deep) {
            this(ParserWithReflectUtilities.find(m), deep);
        }

        public FinderOfExpressionStmt(Constructor<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfExpressionStmt(Constructor<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), deep);
        }

        /**
         * Dans ce cas, deep vaut true.
         *
         * @param visitable le Visitable dans le lequel chercher les
         * ExpressionStmt
         */
        public FinderOfExpressionStmt(Visitable visitable) {
            this(visitable, false);
        }

        /**
         * Effectue la recherche des ExpressionStmt dans un Visitable, en
         * profondeur ou non selon l'état de {@code deep}.
         * <p>
         * Rechercher en profondeur signifie que si un élément a été trouvé, la
         * recherche continue dans l'élément lui-même. Cela permet de trouver
         * les "if" imbriqués par exemple.
         * <p>
         * Ne pas rechercher en profondeur signifie qu'on s'arrête au premier
         * niveau.
         * <p>
         * Cette propriété n'a pas d'intérêt pour tous les éléments
         *
         * @param visitable le Visitable dans le lequel chercher les
         * ExpressionStmt
         * @param deep recherche en profondeur ou non
         */
        public FinderOfExpressionStmt(Visitable visitable, boolean deep) {
            super(visitable, deep);
            this.visitable.accept(vva, null);
        }

        /**
         * Retourne le tableau des ExpressionStmt trouvés
         *
         * @return le tableau des ExpressionStmt trouvés
         */
        public ArrayList<ExpressionStmt> getExpressionStmt() {
            return expressionStmt;
        }

        /**
         * Retourne le nombre de ExpressionStmt trouvés
         *
         * @return le nombre de ExpressionStmt trouvés
         */
        public int getNumberOfExpressionStmt() {
            return expressionStmt.size();
        }
    }

    /**
     * Pour extraire tous les ExplicitConstructorInvocationStmt
     *
     * @author Yvan Maillot &lt;yvan.maillot@uha.fr&gt;
     */
    public static class FinderOfExplicitConstructorInvocationStmt extends Finder {

        private final ArrayList<ExplicitConstructorInvocationStmt> explicitConstructorInvocationStmt = new ArrayList();
        private final VoidVisitorAdapter<Void> vva = new VoidVisitorAdapter<>() {

            @Override
            public void visit(ExplicitConstructorInvocationStmt n, Void arg) {
                if (deep) {
                    super.visit(n, arg);
                }
                explicitConstructorInvocationStmt.add(n);
            }
        };

        public FinderOfExplicitConstructorInvocationStmt(Method m) {
            this(ParserWithReflectUtilities.find(m), true);
        }

        public FinderOfExplicitConstructorInvocationStmt(Class<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfExplicitConstructorInvocationStmt(Class<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfExplicitConstructorInvocationStmt(Method m, boolean deep) {
            this(ParserWithReflectUtilities.find(m), deep);
        }

        public FinderOfExplicitConstructorInvocationStmt(Constructor<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfExplicitConstructorInvocationStmt(Constructor<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), deep);
        }

        /**
         * Dans ce cas, deep vaut true.
         *
         * @param visitable le Visitable dans le lequel chercher les
         * ExplicitConstructorInvocationStmt
         */
        public FinderOfExplicitConstructorInvocationStmt(Visitable visitable) {
            this(visitable, false);
        }

        /**
         * Effectue la recherche des ExplicitConstructorInvocationStmt dans un
         * Visitable, en profondeur ou non selon l'état de {@code deep}.
         * <p>
         * Rechercher en profondeur signifie que si un élément a été trouvé, la
         * recherche continue dans l'élément lui-même. Cela permet de trouver
         * les "if" imbriqués par exemple.
         * <p>
         * Ne pas rechercher en profondeur signifie qu'on s'arrête au premier
         * niveau.
         * <p>
         * Cette propriété n'a pas d'intérêt pour tous les éléments
         *
         * @param visitable le Visitable dans le lequel chercher les
         * ExplicitConstructorInvocationStmt
         * @param deep recherche en profondeur ou non
         */
        public FinderOfExplicitConstructorInvocationStmt(Visitable visitable, boolean deep) {
            super(visitable, deep);
            this.visitable.accept(vva, null);
        }

        /**
         * Retourne le tableau des ExplicitConstructorInvocationStmt trouvés
         *
         * @return le tableau des ExplicitConstructorInvocationStmt trouvés
         */
        public ArrayList<ExplicitConstructorInvocationStmt> getExplicitConstructorInvocationStmt() {
            return explicitConstructorInvocationStmt;
        }

        /**
         * Retourne le nombre de ExplicitConstructorInvocationStmt trouvés
         *
         * @return le nombre de ExplicitConstructorInvocationStmt trouvés
         */
        public int getNumberOfExplicitConstructorInvocationStmt() {
            return explicitConstructorInvocationStmt.size();
        }
    }

    /**
     * Pour extraire tous les EnumDeclaration
     *
     * @author Yvan Maillot &lt;yvan.maillot@uha.fr&gt;
     */
    public static class FinderOfEnumDeclaration extends Finder {

        private final ArrayList<EnumDeclaration> enumDeclaration = new ArrayList();
        private final VoidVisitorAdapter<Void> vva = new VoidVisitorAdapter<>() {

            @Override
            public void visit(EnumDeclaration n, Void arg) {
                if (deep) {
                    super.visit(n, arg);
                }
                enumDeclaration.add(n);
            }
        };

        public FinderOfEnumDeclaration(Method m) {
            this(ParserWithReflectUtilities.find(m), true);
        }

        public FinderOfEnumDeclaration(Class<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfEnumDeclaration(Class<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfEnumDeclaration(Method m, boolean deep) {
            this(ParserWithReflectUtilities.find(m), deep);
        }

        public FinderOfEnumDeclaration(Constructor<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfEnumDeclaration(Constructor<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), deep);
        }

        /**
         * Dans ce cas, deep vaut true.
         *
         * @param visitable le Visitable dans le lequel chercher les
         * EnumDeclaration
         */
        public FinderOfEnumDeclaration(Visitable visitable) {
            this(visitable, false);
        }

        /**
         * Effectue la recherche des EnumDeclaration dans un Visitable, en
         * profondeur ou non selon l'état de {@code deep}.
         * <p>
         * Rechercher en profondeur signifie que si un élément a été trouvé, la
         * recherche continue dans l'élément lui-même. Cela permet de trouver
         * les "if" imbriqués par exemple.
         * <p>
         * Ne pas rechercher en profondeur signifie qu'on s'arrête au premier
         * niveau.
         * <p>
         * Cette propriété n'a pas d'intérêt pour tous les éléments
         *
         * @param visitable le Visitable dans le lequel chercher les
         * EnumDeclaration
         * @param deep recherche en profondeur ou non
         */
        public FinderOfEnumDeclaration(Visitable visitable, boolean deep) {
            super(visitable, deep);
            this.visitable.accept(vva, null);
        }

        /**
         * Retourne le tableau des EnumDeclaration trouvés
         *
         * @return le tableau des EnumDeclaration trouvés
         */
        public ArrayList<EnumDeclaration> getEnumDeclaration() {
            return enumDeclaration;
        }

        /**
         * Retourne le nombre de EnumDeclaration trouvés
         *
         * @return le nombre de EnumDeclaration trouvés
         */
        public int getNumberOfEnumDeclaration() {
            return enumDeclaration.size();
        }
    }

    /**
     * Pour extraire tous les EnumConstantDeclaration
     *
     * @author Yvan Maillot &lt;yvan.maillot@uha.fr&gt;
     */
    public static class FinderOfEnumConstantDeclaration extends Finder {

        private final ArrayList<EnumConstantDeclaration> enumConstantDeclaration = new ArrayList();
        private final VoidVisitorAdapter<Void> vva = new VoidVisitorAdapter<>() {

            @Override
            public void visit(EnumConstantDeclaration n, Void arg) {
                if (deep) {
                    super.visit(n, arg);
                }
                enumConstantDeclaration.add(n);
            }
        };

        public FinderOfEnumConstantDeclaration(Method m) {
            this(ParserWithReflectUtilities.find(m), true);
        }

        public FinderOfEnumConstantDeclaration(Class<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfEnumConstantDeclaration(Class<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfEnumConstantDeclaration(Method m, boolean deep) {
            this(ParserWithReflectUtilities.find(m), deep);
        }

        public FinderOfEnumConstantDeclaration(Constructor<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfEnumConstantDeclaration(Constructor<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), deep);
        }

        /**
         * Dans ce cas, deep vaut true.
         *
         * @param visitable le Visitable dans le lequel chercher les
         * EnumConstantDeclaration
         */
        public FinderOfEnumConstantDeclaration(Visitable visitable) {
            this(visitable, false);
        }

        /**
         * Effectue la recherche des EnumConstantDeclaration dans un Visitable,
         * en profondeur ou non selon l'état de {@code deep}.
         * <p>
         * Rechercher en profondeur signifie que si un élément a été trouvé, la
         * recherche continue dans l'élément lui-même. Cela permet de trouver
         * les "if" imbriqués par exemple.
         * <p>
         * Ne pas rechercher en profondeur signifie qu'on s'arrête au premier
         * niveau.
         * <p>
         * Cette propriété n'a pas d'intérêt pour tous les éléments
         *
         * @param visitable le Visitable dans le lequel chercher les
         * EnumConstantDeclaration
         * @param deep recherche en profondeur ou non
         */
        public FinderOfEnumConstantDeclaration(Visitable visitable, boolean deep) {
            super(visitable, deep);
            this.visitable.accept(vva, null);
        }

        /**
         * Retourne le tableau des EnumConstantDeclaration trouvés
         *
         * @return le tableau des EnumConstantDeclaration trouvés
         */
        public ArrayList<EnumConstantDeclaration> getEnumConstantDeclaration() {
            return enumConstantDeclaration;
        }

        /**
         * Retourne le nombre de EnumConstantDeclaration trouvés
         *
         * @return le nombre de EnumConstantDeclaration trouvés
         */
        public int getNumberOfEnumConstantDeclaration() {
            return enumConstantDeclaration.size();
        }
    }

    /**
     * Pour extraire tous les EnclosedExpr
     *
     * @author Yvan Maillot &lt;yvan.maillot@uha.fr&gt;
     */
    public static class FinderOfEnclosedExpr extends Finder {

        private final ArrayList<EnclosedExpr> enclosedExpr = new ArrayList();
        private final VoidVisitorAdapter<Void> vva = new VoidVisitorAdapter<>() {

            @Override
            public void visit(EnclosedExpr n, Void arg) {
                if (deep) {
                    super.visit(n, arg);
                }
                enclosedExpr.add(n);
            }
        };

        public FinderOfEnclosedExpr(Method m) {
            this(ParserWithReflectUtilities.find(m), true);
        }

        public FinderOfEnclosedExpr(Class<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfEnclosedExpr(Class<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfEnclosedExpr(Method m, boolean deep) {
            this(ParserWithReflectUtilities.find(m), deep);
        }

        public FinderOfEnclosedExpr(Constructor<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfEnclosedExpr(Constructor<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), deep);
        }

        /**
         * Dans ce cas, deep vaut true.
         *
         * @param visitable le Visitable dans le lequel chercher les
         * EnclosedExpr
         */
        public FinderOfEnclosedExpr(Visitable visitable) {
            this(visitable, false);
        }

        /**
         * Effectue la recherche des EnclosedExpr dans un Visitable, en
         * profondeur ou non selon l'état de {@code deep}.
         * <p>
         * Rechercher en profondeur signifie que si un élément a été trouvé, la
         * recherche continue dans l'élément lui-même. Cela permet de trouver
         * les "if" imbriqués par exemple.
         * <p>
         * Ne pas rechercher en profondeur signifie qu'on s'arrête au premier
         * niveau.
         * <p>
         * Cette propriété n'a pas d'intérêt pour tous les éléments
         *
         * @param visitable le Visitable dans le lequel chercher les
         * EnclosedExpr
         * @param deep recherche en profondeur ou non
         */
        public FinderOfEnclosedExpr(Visitable visitable, boolean deep) {
            super(visitable, deep);
            this.visitable.accept(vva, null);
        }

        /**
         * Retourne le tableau des EnclosedExpr trouvés
         *
         * @return le tableau des EnclosedExpr trouvés
         */
        public ArrayList<EnclosedExpr> getEnclosedExpr() {
            return enclosedExpr;
        }

        /**
         * Retourne le nombre de EnclosedExpr trouvés
         *
         * @return le nombre de EnclosedExpr trouvés
         */
        public int getNumberOfEnclosedExpr() {
            return enclosedExpr.size();
        }
    }

    /**
     * Pour extraire tous les EmptyStmt
     *
     * @author Yvan Maillot &lt;yvan.maillot@uha.fr&gt;
     */
    public static class FinderOfEmptyStmt extends Finder {

        private final ArrayList<EmptyStmt> emptyStmt = new ArrayList();
        private final VoidVisitorAdapter<Void> vva = new VoidVisitorAdapter<>() {

            @Override
            public void visit(EmptyStmt n, Void arg) {
                if (deep) {
                    super.visit(n, arg);
                }
                emptyStmt.add(n);
            }
        };

        public FinderOfEmptyStmt(Method m) {
            this(ParserWithReflectUtilities.find(m), true);
        }

        public FinderOfEmptyStmt(Class<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfEmptyStmt(Class<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfEmptyStmt(Method m, boolean deep) {
            this(ParserWithReflectUtilities.find(m), deep);
        }

        public FinderOfEmptyStmt(Constructor<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfEmptyStmt(Constructor<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), deep);
        }

        /**
         * Dans ce cas, deep vaut true.
         *
         * @param visitable le Visitable dans le lequel chercher les EmptyStmt
         */
        public FinderOfEmptyStmt(Visitable visitable) {
            this(visitable, false);
        }

        /**
         * Effectue la recherche des EmptyStmt dans un Visitable, en profondeur
         * ou non selon l'état de {@code deep}.
         * <p>
         * Rechercher en profondeur signifie que si un élément a été trouvé, la
         * recherche continue dans l'élément lui-même. Cela permet de trouver
         * les "if" imbriqués par exemple.
         * <p>
         * Ne pas rechercher en profondeur signifie qu'on s'arrête au premier
         * niveau.
         * <p>
         * Cette propriété n'a pas d'intérêt pour tous les éléments
         *
         * @param visitable le Visitable dans le lequel chercher les EmptyStmt
         * @param deep recherche en profondeur ou non
         */
        public FinderOfEmptyStmt(Visitable visitable, boolean deep) {
            super(visitable, deep);
            this.visitable.accept(vva, null);
        }

        /**
         * Retourne le tableau des EmptyStmt trouvés
         *
         * @return le tableau des EmptyStmt trouvés
         */
        public ArrayList<EmptyStmt> getEmptyStmt() {
            return emptyStmt;
        }

        /**
         * Retourne le nombre de EmptyStmt trouvés
         *
         * @return le nombre de EmptyStmt trouvés
         */
        public int getNumberOfEmptyStmt() {
            return emptyStmt.size();
        }
    }

    /**
     * Pour extraire tous les InitializerDeclaration
     *
     * @author Yvan Maillot &lt;yvan.maillot@uha.fr&gt;
     */
    public static class FinderOfInitializerDeclaration extends Finder {

        private final ArrayList<InitializerDeclaration> initializerDeclaration = new ArrayList();
        private final VoidVisitorAdapter<Void> vva = new VoidVisitorAdapter<>() {

            @Override
            public void visit(InitializerDeclaration n, Void arg) {
                if (deep) {
                    super.visit(n, arg);
                }
                initializerDeclaration.add(n);
            }
        };

        public FinderOfInitializerDeclaration(Method m) {
            this(ParserWithReflectUtilities.find(m), true);
        }

        public FinderOfInitializerDeclaration(Class<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfInitializerDeclaration(Class<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfInitializerDeclaration(Method m, boolean deep) {
            this(ParserWithReflectUtilities.find(m), deep);
        }

        public FinderOfInitializerDeclaration(Constructor<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfInitializerDeclaration(Constructor<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), deep);
        }

        /**
         * Dans ce cas, deep vaut true.
         *
         * @param visitable le Visitable dans le lequel chercher les
         * InitializerDeclaration
         */
        public FinderOfInitializerDeclaration(Visitable visitable) {
            this(visitable, false);
        }

        /**
         * Effectue la recherche des InitializerDeclaration dans un Visitable,
         * en profondeur ou non selon l'état de {@code deep}.
         * <p>
         * Rechercher en profondeur signifie que si un élément a été trouvé, la
         * recherche continue dans l'élément lui-même. Cela permet de trouver
         * les "if" imbriqués par exemple.
         * <p>
         * Ne pas rechercher en profondeur signifie qu'on s'arrête au premier
         * niveau.
         * <p>
         * Cette propriété n'a pas d'intérêt pour tous les éléments
         *
         * @param visitable le Visitable dans le lequel chercher les
         * InitializerDeclaration
         * @param deep recherche en profondeur ou non
         */
        public FinderOfInitializerDeclaration(Visitable visitable, boolean deep) {
            super(visitable, deep);
            this.visitable.accept(vva, null);
        }

        /**
         * Retourne le tableau des InitializerDeclaration trouvés
         *
         * @return le tableau des InitializerDeclaration trouvés
         */
        public ArrayList<InitializerDeclaration> getInitializerDeclaration() {
            return initializerDeclaration;
        }

        /**
         * Retourne le nombre de InitializerDeclaration trouvés
         *
         * @return le nombre de InitializerDeclaration trouvés
         */
        public int getNumberOfInitializerDeclaration() {
            return initializerDeclaration.size();
        }
    }

    /**
     * Pour extraire tous les IfStmt
     *
     * @author Yvan Maillot &lt;yvan.maillot@uha.fr&gt;
     */
    public static class FinderOfIfStmt extends Finder {

        private final ArrayList<IfStmt> ifStmt = new ArrayList();
        private final VoidVisitorAdapter<Void> vva = new VoidVisitorAdapter<>() {

            @Override
            public void visit(IfStmt n, Void arg) {
                if (deep) {
                    super.visit(n, arg);
                }
                ifStmt.add(n);
            }
        };

        public FinderOfIfStmt(Method m) {
            this(ParserWithReflectUtilities.find(m), true);
        }

        public FinderOfIfStmt(Class<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfIfStmt(Class<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfIfStmt(Method m, boolean deep) {
            this(ParserWithReflectUtilities.find(m), deep);
        }

        public FinderOfIfStmt(Constructor<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfIfStmt(Constructor<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), deep);
        }

        /**
         * Dans ce cas, deep vaut true.
         *
         * @param visitable le Visitable dans le lequel chercher les IfStmt
         */
        public FinderOfIfStmt(Visitable visitable) {
            this(visitable, false);
        }

        /**
         * Effectue la recherche des IfStmt dans un Visitable, en profondeur ou
         * non selon l'état de {@code deep}.
         * <p>
         * Rechercher en profondeur signifie que si un élément a été trouvé, la
         * recherche continue dans l'élément lui-même. Cela permet de trouver
         * les "if" imbriqués par exemple.
         * <p>
         * Ne pas rechercher en profondeur signifie qu'on s'arrête au premier
         * niveau.
         * <p>
         * Cette propriété n'a pas d'intérêt pour tous les éléments
         *
         * @param visitable le Visitable dans le lequel chercher les IfStmt
         * @param deep recherche en profondeur ou non
         */
        public FinderOfIfStmt(Visitable visitable, boolean deep) {
            super(visitable, deep);
            this.visitable.accept(vva, null);
        }

        /**
         * Retourne le tableau des IfStmt trouvés
         *
         * @return le tableau des IfStmt trouvés
         */
        public ArrayList<IfStmt> getIfStmt() {
            return ifStmt;
        }

        /**
         * Retourne le nombre de IfStmt trouvés
         *
         * @return le nombre de IfStmt trouvés
         */
        public int getNumberOfIfStmt() {
            return ifStmt.size();
        }
    }

    /**
     * Pour extraire tous les ForStmt
     *
     * @author Yvan Maillot &lt;yvan.maillot@uha.fr&gt;
     */
    public static class FinderOfForStmt extends Finder {

        private final ArrayList<ForStmt> forStmt = new ArrayList();
        private final VoidVisitorAdapter<Void> vva = new VoidVisitorAdapter<>() {

            @Override
            public void visit(ForStmt n, Void arg) {
                if (deep) {
                    super.visit(n, arg);
                }
                forStmt.add(n);
            }
        };

        public FinderOfForStmt(Method m) {
            this(ParserWithReflectUtilities.find(m), true);
        }

        public FinderOfForStmt(Class<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfForStmt(Class<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfForStmt(Method m, boolean deep) {
            this(ParserWithReflectUtilities.find(m), deep);
        }

        public FinderOfForStmt(Constructor<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfForStmt(Constructor<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), deep);
        }

        /**
         * Dans ce cas, deep vaut true.
         *
         * @param visitable le Visitable dans le lequel chercher les ForStmt
         */
        public FinderOfForStmt(Visitable visitable) {
            this(visitable, false);
        }

        /**
         * Effectue la recherche des ForStmt dans un Visitable, en profondeur ou
         * non selon l'état de {@code deep}.
         * <p>
         * Rechercher en profondeur signifie que si un élément a été trouvé, la
         * recherche continue dans l'élément lui-même. Cela permet de trouver
         * les "if" imbriqués par exemple.
         * <p>
         * Ne pas rechercher en profondeur signifie qu'on s'arrête au premier
         * niveau.
         * <p>
         * Cette propriété n'a pas d'intérêt pour tous les éléments
         *
         * @param visitable le Visitable dans le lequel chercher les ForStmt
         * @param deep recherche en profondeur ou non
         */
        public FinderOfForStmt(Visitable visitable, boolean deep) {
            super(visitable, deep);
            this.visitable.accept(vva, null);
        }

        /**
         * Retourne le tableau des ForStmt trouvés
         *
         * @return le tableau des ForStmt trouvés
         */
        public ArrayList<ForStmt> getForStmt() {
            return forStmt;
        }

        /**
         * Retourne le nombre de ForStmt trouvés
         *
         * @return le nombre de ForStmt trouvés
         */
        public int getNumberOfForStmt() {
            return forStmt.size();
        }
    }

    /**
     * Pour extraire tous les FieldAccessExpr
     *
     * @author Yvan Maillot &lt;yvan.maillot@uha.fr&gt;
     */
    public static class FinderOfFieldAccessExpr extends Finder {

        private final ArrayList<FieldAccessExpr> fieldAccessExpr = new ArrayList();
        private final VoidVisitorAdapter<Void> vva = new VoidVisitorAdapter<>() {

            @Override
            public void visit(FieldAccessExpr n, Void arg) {
                if (deep) {
                    super.visit(n, arg);
                }
                fieldAccessExpr.add(n);
            }
        };

        public FinderOfFieldAccessExpr(Method m) {
            this(ParserWithReflectUtilities.find(m), true);
        }

        public FinderOfFieldAccessExpr(Class<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfFieldAccessExpr(Class<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfFieldAccessExpr(Method m, boolean deep) {
            this(ParserWithReflectUtilities.find(m), deep);
        }

        public FinderOfFieldAccessExpr(Constructor<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfFieldAccessExpr(Constructor<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), deep);
        }

        /**
         * Dans ce cas, deep vaut true.
         *
         * @param visitable le Visitable dans le lequel chercher les
         * FieldAccessExpr
         */
        public FinderOfFieldAccessExpr(Visitable visitable) {
            this(visitable, false);
        }

        /**
         * Effectue la recherche des FieldAccessExpr dans un Visitable, en
         * profondeur ou non selon l'état de {@code deep}.
         * <p>
         * Rechercher en profondeur signifie que si un élément a été trouvé, la
         * recherche continue dans l'élément lui-même. Cela permet de trouver
         * les "if" imbriqués par exemple.
         * <p>
         * Ne pas rechercher en profondeur signifie qu'on s'arrête au premier
         * niveau.
         * <p>
         * Cette propriété n'a pas d'intérêt pour tous les éléments
         *
         * @param visitable le Visitable dans le lequel chercher les
         * FieldAccessExpr
         * @param deep recherche en profondeur ou non
         */
        public FinderOfFieldAccessExpr(Visitable visitable, boolean deep) {
            super(visitable, deep);
            this.visitable.accept(vva, null);
        }

        /**
         * Retourne le tableau des FieldAccessExpr trouvés
         *
         * @return le tableau des FieldAccessExpr trouvés
         */
        public ArrayList<FieldAccessExpr> getFieldAccessExpr() {
            return fieldAccessExpr;
        }

        /**
         * Retourne le nombre de FieldAccessExpr trouvés
         *
         * @return le nombre de FieldAccessExpr trouvés
         */
        public int getNumberOfFieldAccessExpr() {
            return fieldAccessExpr.size();
        }
    }

    /**
     * Pour extraire tous les FieldDeclaration
     *
     * @author Yvan Maillot &lt;yvan.maillot@uha.fr&gt;
     */
    public static class FinderOfFieldDeclaration extends Finder {

        private final ArrayList<FieldDeclaration> fieldDeclaration = new ArrayList();
        private final VoidVisitorAdapter<Void> vva = new VoidVisitorAdapter<>() {

            @Override
            public void visit(FieldDeclaration n, Void arg) {
                if (deep) {
                    super.visit(n, arg);
                }
                fieldDeclaration.add(n);
            }
        };

        public FinderOfFieldDeclaration(Method m) {
            this(ParserWithReflectUtilities.find(m), true);
        }

        public FinderOfFieldDeclaration(Class<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfFieldDeclaration(Class<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfFieldDeclaration(Method m, boolean deep) {
            this(ParserWithReflectUtilities.find(m), deep);
        }

        public FinderOfFieldDeclaration(Constructor<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfFieldDeclaration(Constructor<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), deep);
        }

        /**
         * Dans ce cas, deep vaut true.
         *
         * @param visitable le Visitable dans le lequel chercher les
         * FieldDeclaration
         */
        public FinderOfFieldDeclaration(Visitable visitable) {
            this(visitable, false);
        }

        /**
         * Effectue la recherche des FieldDeclaration dans un Visitable, en
         * profondeur ou non selon l'état de {@code deep}.
         * <p>
         * Rechercher en profondeur signifie que si un élément a été trouvé, la
         * recherche continue dans l'élément lui-même. Cela permet de trouver
         * les "if" imbriqués par exemple.
         * <p>
         * Ne pas rechercher en profondeur signifie qu'on s'arrête au premier
         * niveau.
         * <p>
         * Cette propriété n'a pas d'intérêt pour tous les éléments
         *
         * @param visitable le Visitable dans le lequel chercher les
         * FieldDeclaration
         * @param deep recherche en profondeur ou non
         */
        public FinderOfFieldDeclaration(Visitable visitable, boolean deep) {
            super(visitable, deep);
            this.visitable.accept(vva, null);
        }

        /**
         * Retourne le tableau des FieldDeclaration trouvés
         *
         * @return le tableau des FieldDeclaration trouvés
         */
        public ArrayList<FieldDeclaration> getFieldDeclaration() {
            return fieldDeclaration;
        }

        /**
         * Retourne le nombre de FieldDeclaration trouvés
         *
         * @return le nombre de FieldDeclaration trouvés
         */
        public int getNumberOfFieldDeclaration() {
            return fieldDeclaration.size();
        }
    }

    /**
     * Pour extraire tous les ForEachStmt
     *
     * @author Yvan Maillot &lt;yvan.maillot@uha.fr&gt;
     */
    public static class FinderOfForEachStmt extends Finder {

        private final ArrayList<ForEachStmt> forEachStmt = new ArrayList();
        private final VoidVisitorAdapter<Void> vva = new VoidVisitorAdapter<>() {

            @Override
            public void visit(ForEachStmt n, Void arg) {
                if (deep) {
                    super.visit(n, arg);
                }
                forEachStmt.add(n);
            }
        };

        public FinderOfForEachStmt(Method m) {
            this(ParserWithReflectUtilities.find(m), true);
        }

        public FinderOfForEachStmt(Class<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfForEachStmt(Class<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfForEachStmt(Method m, boolean deep) {
            this(ParserWithReflectUtilities.find(m), deep);
        }

        public FinderOfForEachStmt(Constructor<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public FinderOfForEachStmt(Constructor<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), deep);
        }

        /**
         * Dans ce cas, deep vaut true.
         *
         * @param visitable le Visitable dans le lequel chercher les ForEachStmt
         */
        public FinderOfForEachStmt(Visitable visitable) {
            this(visitable, false);
        }

        /**
         * Effectue la recherche des ForEachStmt dans un Visitable, en
         * profondeur ou non selon l'état de {@code deep}.
         * <p>
         * Rechercher en profondeur signifie que si un élément a été trouvé, la
         * recherche continue dans l'élément lui-même. Cela permet de trouver
         * les "if" imbriqués par exemple.
         * <p>
         * Ne pas rechercher en profondeur signifie qu'on s'arrête au premier
         * niveau.
         * <p>
         * Cette propriété n'a pas d'intérêt pour tous les éléments
         *
         * @param visitable le Visitable dans le lequel chercher les ForEachStmt
         * @param deep recherche en profondeur ou non
         */
        public FinderOfForEachStmt(Visitable visitable, boolean deep) {
            super(visitable, deep);
            this.visitable.accept(vva, null);
        }

        /**
         * Retourne le tableau des ForEachStmt trouvés
         *
         * @return le tableau des ForEachStmt trouvés
         */
        public ArrayList<ForEachStmt> getForEachStmt() {
            return forEachStmt;
        }

        /**
         * Retourne le nombre de ForEachStmt trouvés
         *
         * @return le nombre de ForEachStmt trouvés
         */
        public int getNumberOfForEachStmt() {
            return forEachStmt.size();
        }
    }

    public static class AllFinder extends VoidVisitorAdapter<Void> {

        private final ArrayList<ExplicitConstructorInvocationStmt> explicitConstructorInvocationStmt = new ArrayList<>();
        private final ArrayList<SwitchExpr> switchExpr = new ArrayList<>();
        private final ArrayList<Parameter> parameter = new ArrayList<>();
        private final ArrayList<AnnotationDeclaration> annotationDeclaration = new ArrayList();
        private final ArrayList<AnnotationMemberDeclaration> annotationMemberDeclaration = new ArrayList();
        private final ArrayList<ArrayAccessExpr> arrayAccessExpr = new ArrayList();
        private final ArrayList<ArrayCreationExpr> arrayCreationExpr = new ArrayList();
        private final ArrayList<ArrayInitializerExpr> arrayInitializerExpr = new ArrayList();
        private final ArrayList<AssertStmt> assertStmt = new ArrayList();
        private final ArrayList<AssignExpr> assignExpr = new ArrayList();
        private final ArrayList<BinaryExpr> binaryExpr = new ArrayList();
        private final ArrayList<BlockComment> blockComment = new ArrayList();
        private final ArrayList<BlockStmt> blockStmt = new ArrayList();
        private final ArrayList<BooleanLiteralExpr> booleanLiteralExpr = new ArrayList();
        private final ArrayList<BreakStmt> breakStmt = new ArrayList();
        private final ArrayList<CastExpr> castExpr = new ArrayList();
        private final ArrayList<CatchClause> catchClause = new ArrayList();
        private final ArrayList<CharLiteralExpr> charLiteralExpr = new ArrayList();
        private final ArrayList<ClassExpr> classExpr = new ArrayList();
        private final ArrayList<ClassOrInterfaceDeclaration> classOrInterfaceDeclaration = new ArrayList();
        private final ArrayList<ClassOrInterfaceType> classOrInterfaceType = new ArrayList();
        private final ArrayList<CompilationUnit> compilationUnit = new ArrayList();
        private final ArrayList<ConditionalExpr> conditionalExpr = new ArrayList();
        private final ArrayList<ConstructorDeclaration> constructorDeclaration = new ArrayList();
        private final ArrayList<ContinueStmt> continueStmt = new ArrayList();
        private final ArrayList<DoStmt> doStmt = new ArrayList();
        private final ArrayList<DoubleLiteralExpr> doubleLiteralExpr = new ArrayList();
        private final ArrayList<EmptyStmt> emptyStmt = new ArrayList();
        private final ArrayList<EnclosedExpr> enclosedExpr = new ArrayList();
        private final ArrayList<EnumConstantDeclaration> enumConstantDeclaration = new ArrayList();
        private final ArrayList<EnumDeclaration> enumDeclaration = new ArrayList();
        private final ArrayList<ExpressionStmt> expressionStmt = new ArrayList();
        private final ArrayList<FieldAccessExpr> fieldAccessExpr = new ArrayList();
        private final ArrayList<FieldDeclaration> fieldDeclaration = new ArrayList();
        private final ArrayList<ForEachStmt> forEachStmt = new ArrayList();
        private final ArrayList<ForStmt> forStmt = new ArrayList();
        private final ArrayList<IfStmt> ifStmt = new ArrayList();
        private final ArrayList<InitializerDeclaration> initializerDeclaration = new ArrayList();
        private final ArrayList<InstanceOfExpr> instanceOfExpr = new ArrayList();
        private final ArrayList<IntegerLiteralExpr> integerLiteralExpr = new ArrayList();
        private final ArrayList<JavadocComment> javadocComment = new ArrayList();
        private final ArrayList<LabeledStmt> labeledStmt = new ArrayList();
        private final ArrayList<LineComment> lineComment = new ArrayList();
        private final ArrayList<LongLiteralExpr> longLiteralExpr = new ArrayList();
        private final ArrayList<MarkerAnnotationExpr> markerAnnotationExpr = new ArrayList();
        private final ArrayList<MemberValuePair> memberValuePair = new ArrayList();
        private final ArrayList<MethodCallExpr> methodCallExpr = new ArrayList();
        private final ArrayList<MethodDeclaration> methodDeclaration = new ArrayList();
        private final ArrayList<NameExpr> nameExpr = new ArrayList();
        private final ArrayList<NormalAnnotationExpr> normalAnnotationExpr = new ArrayList();
        private final ArrayList<NullLiteralExpr> nullLiteralExpr = new ArrayList();
        private final ArrayList<ObjectCreationExpr> objectCreationExpr = new ArrayList();
        private final ArrayList<PackageDeclaration> packageDeclaration = new ArrayList();
        private final ArrayList<PrimitiveType> primitiveType = new ArrayList();
        private final ArrayList<Name> name = new ArrayList();
        private final ArrayList<SimpleName> simpleName = new ArrayList();
        private final ArrayList<ArrayType> arrayType = new ArrayList();
        private final ArrayList<ArrayCreationLevel> arrayCreationLevel = new ArrayList();
        private final ArrayList<IntersectionType> intersectionType = new ArrayList();
        private final ArrayList<UnionType> unionType = new ArrayList();
        private final ArrayList<ReturnStmt> returnStmt = new ArrayList();
        private final ArrayList<SingleMemberAnnotationExpr> singleMemberAnnotationExpr = new ArrayList();
        private final ArrayList<StringLiteralExpr> stringLiteralExpr = new ArrayList();
        private final ArrayList<SuperExpr> superExpr = new ArrayList();
        private final ArrayList<SwitchEntry> switchEntry = new ArrayList();
        private final ArrayList<SwitchStmt> switchStmt = new ArrayList();
        private final ArrayList<SynchronizedStmt> synchronizedStmt = new ArrayList();
        private final ArrayList<ThisExpr> thisExpr = new ArrayList();
        private final ArrayList<ThrowStmt> throwStmt = new ArrayList();
        private final ArrayList<TryStmt> tryStmt = new ArrayList();
        private final ArrayList<LocalClassDeclarationStmt> localClassDeclarationStmt = new ArrayList();
        private final ArrayList<TypeParameter> typeParameter = new ArrayList();
        private final ArrayList<UnaryExpr> unaryExpr = new ArrayList();
        private final ArrayList<UnknownType> unknownType = new ArrayList();
        private final ArrayList<VariableDeclarationExpr> variableDeclarationExpr = new ArrayList();
        private final ArrayList<VariableDeclarator> variableDeclarator = new ArrayList();
        private final ArrayList<VoidType> voidType = new ArrayList();
        private final ArrayList<WhileStmt> whileStmt = new ArrayList();
        private final ArrayList<WildcardType> wildcardType = new ArrayList();
        private final ArrayList<LambdaExpr> lambdaExpr = new ArrayList();
        private final ArrayList<MethodReferenceExpr> methodReferenceExpr = new ArrayList();
        private final ArrayList<TypeExpr> typeExpr = new ArrayList();
        private final ArrayList<NodeList> nodeList = new ArrayList();
        private final ArrayList<ImportDeclaration> importDeclaration = new ArrayList();

        private boolean deep;
        private Visitable visitable;

        public AllFinder(Class<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public AllFinder(Class<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), deep);
        }

        public AllFinder(Method m) {
            this(ParserWithReflectUtilities.find(m), true);
        }

        public AllFinder(Method m, boolean deep) {
            this(ParserWithReflectUtilities.find(m), deep);
        }

        public AllFinder(Constructor<?> c) {
            this(ParserWithReflectUtilities.find(c), true);
        }

        public AllFinder(Constructor<?> c, boolean deep) {
            this(ParserWithReflectUtilities.find(c), deep);
        }

        public AllFinder(Visitable visitable, boolean deep) {
            this.deep = deep;
            this.visitable = visitable;
        }

        public AllFinder(Visitable visitable) {
            this(visitable, false);
        }

        public void findAll() {
            if (visitable != null) {
                visitable.accept(this, null);
            }
        }

        @Override
        public void visit(YieldStmt n, Void arg) {
            if (deep) {
                super.visit(n, arg);
            }
        }

        @Override
        public void visit(TextBlockLiteralExpr n, Void arg) {
            if (deep) {
                super.visit(n, arg);
            }
        }

        @Override
        public void visit(SwitchExpr n, Void arg) {
            if (deep) {
                super.visit(n, arg);
            }
            switchExpr.add(n);
        }

        @Override
        public void visit(Modifier n, Void arg) {
            if (deep) {
                super.visit(n, arg);
            }
        }

        @Override
        public void visit(VarType n, Void arg) {
            if (deep) {
                super.visit(n, arg);
            }
        }

        @Override
        public void visit(ReceiverParameter n, Void arg) {
            if (deep) {
                super.visit(n, arg);
            }
        }

        @Override
        public void visit(UnparsableStmt n, Void arg) {
            if (deep) {
                super.visit(n, arg);
            }
        }

        @Override
        public void visit(ModuleOpensDirective n, Void arg) {
            if (deep) {
                super.visit(n, arg);
            }
        }

        @Override
        public void visit(ModuleUsesDirective n, Void arg) {
            if (deep) {
                super.visit(n, arg);
            }
        }

        @Override
        public void visit(ModuleProvidesDirective n, Void arg) {
            if (deep) {
                super.visit(n, arg);
            }
        }

        @Override
        public void visit(ModuleExportsDirective n, Void arg) {
            if (deep) {
                super.visit(n, arg);
            }
        }

        @Override
        public void visit(ModuleRequiresDirective n, Void arg) {
            if (deep) {
                super.visit(n, arg);
            }
        }

        @Override
        public void visit(ModuleDeclaration n, Void arg) {
            if (deep) {
                super.visit(n, arg);
            }
        }

        @Override
        public void visit(ImportDeclaration n, Void arg) {
            if (deep) {
                super.visit(n, arg);
            }
            importDeclaration.add(n);
        }

        @Override
        public void visit(NodeList n, Void arg) {
            if (deep) {
                super.visit(n, arg);
            }
            nodeList.add(n);
        }

        @Override
        public void visit(TypeExpr n, Void arg) {
            if (deep) {
                super.visit(n, arg);
            }
            typeExpr.add(n);
        }

        @Override
        public void visit(MethodReferenceExpr n, Void arg) {
            if (deep) {
                super.visit(n, arg);
            }
            methodReferenceExpr.add(n);
        }

        @Override
        public void visit(LambdaExpr n, Void arg) {
            if (deep) {
                super.visit(n, arg);
            }
            lambdaExpr.add(n);
        }

        @Override
        public void visit(WildcardType n, Void arg) {
            if (deep) {
                super.visit(n, arg);
            }
            wildcardType.add(n);
        }

        @Override
        public void visit(WhileStmt n, Void arg) {
            if (deep) {
                super.visit(n, arg);
            }
            whileStmt.add(n);
        }

        @Override
        public void visit(VoidType n, Void arg) {
            if (deep) {
                super.visit(n, arg);
            }
            voidType.add(n);
        }

        @Override
        public void visit(VariableDeclarator n, Void arg) {
            if (deep) {
                super.visit(n, arg);
            }
            variableDeclarator.add(n);
        }

        @Override
        public void visit(VariableDeclarationExpr n, Void arg) {
            if (deep) {
                super.visit(n, arg);
            }
            variableDeclarationExpr.add(n);
        }

        @Override
        public void visit(UnknownType n, Void arg) {
            if (deep) {
                super.visit(n, arg);
            }
            unknownType.add(n);
        }

        @Override
        public void visit(UnaryExpr n, Void arg) {
            if (deep) {
                super.visit(n, arg);
            }
            unaryExpr.add(n);
        }

        @Override
        public void visit(TypeParameter n, Void arg) {
            if (deep) {
                super.visit(n, arg);
            }
            typeParameter.add(n);
        }

        @Override
        public void visit(LocalClassDeclarationStmt n, Void arg) {
            if (deep) {
                super.visit(n, arg);
            }
            localClassDeclarationStmt.add(n);
        }

        @Override
        public void visit(TryStmt n, Void arg) {
            if (deep) {
                super.visit(n, arg);
            }
            tryStmt.add(n);
        }

        @Override
        public void visit(ThrowStmt n, Void arg) {
            if (deep) {
                super.visit(n, arg);
            }
            throwStmt.add(n);
        }

        @Override
        public void visit(ThisExpr n, Void arg) {
            if (deep) {
                super.visit(n, arg);
            }
            thisExpr.add(n);
        }

        @Override
        public void visit(SynchronizedStmt n, Void arg) {
            if (deep) {
                super.visit(n, arg);
            }
            synchronizedStmt.add(n);
        }

        @Override
        public void visit(SwitchStmt n, Void arg) {
            if (deep) {
                super.visit(n, arg);
            }
            switchStmt.add(n);
        }

        @Override
        public void visit(SwitchEntry n, Void arg) {
            if (deep) {
                super.visit(n, arg);
            }
            switchEntry.add(n);
        }

        @Override
        public void visit(SuperExpr n, Void arg) {
            if (deep) {
                super.visit(n, arg);
            }
            superExpr.add(n);
        }

        @Override
        public void visit(StringLiteralExpr n, Void arg) {
            if (deep) {
                super.visit(n, arg);
            }
            stringLiteralExpr.add(n);
        }

        @Override
        public void visit(SingleMemberAnnotationExpr n, Void arg) {
            if (deep) {
                super.visit(n, arg);
            }
            singleMemberAnnotationExpr.add(n);
        }

        @Override
        public void visit(ReturnStmt n, Void arg) {
            if (deep) {
                super.visit(n, arg);
            }
            returnStmt.add(n);
        }

        @Override
        public void visit(UnionType n, Void arg) {
            if (deep) {
                super.visit(n, arg);
            }
            unionType.add(n);
        }

        @Override
        public void visit(IntersectionType n, Void arg) {
            if (deep) {
                super.visit(n, arg);
            }
            intersectionType.add(n);
        }

        @Override
        public void visit(ArrayCreationLevel n, Void arg) {
            if (deep) {
                super.visit(n, arg);
            }
            arrayCreationLevel.add(n);
        }

        @Override
        public void visit(ArrayType n, Void arg) {
            if (deep) {
                super.visit(n, arg);
            }
            arrayType.add(n);
        }

        @Override
        public void visit(SimpleName n, Void arg) {
            if (deep) {
                super.visit(n, arg);
            }
            simpleName.add(n);
        }

        @Override
        public void visit(Name n, Void arg) {
            if (deep) {
                super.visit(n, arg);
            }
            name.add(n);
        }

        @Override
        public void visit(PrimitiveType n, Void arg) {
            if (deep) {
                super.visit(n, arg);
            }
            primitiveType.add(n);
        }

        @Override
        public void visit(Parameter n, Void arg) {
            if (deep) {
                super.visit(n, arg);
            }
            parameter.add(n);
        }

        @Override
        public void visit(PackageDeclaration n, Void arg) {
            if (deep) {
                super.visit(n, arg);
            }
            packageDeclaration.add(n);
        }

        @Override
        public void visit(ObjectCreationExpr n, Void arg) {
            if (deep) {
                super.visit(n, arg);
            }
            objectCreationExpr.add(n);
        }

        @Override
        public void visit(NullLiteralExpr n, Void arg) {
            if (deep) {
                super.visit(n, arg);
            }
            nullLiteralExpr.add(n);
        }

        @Override
        public void visit(NormalAnnotationExpr n, Void arg) {
            if (deep) {
                super.visit(n, arg);
            }
            normalAnnotationExpr.add(n);
        }

        @Override
        public void visit(NameExpr n, Void arg) {
            if (deep) {
                super.visit(n, arg);
            }
            nameExpr.add(n);
        }

        @Override
        public void visit(MethodDeclaration n, Void arg) {
            if (deep) {
                super.visit(n, arg);
            }
            methodDeclaration.add(n);
        }

        @Override
        public void visit(MethodCallExpr n, Void arg) {
            if (deep) {
                super.visit(n, arg);
            }
            methodCallExpr.add(n);
        }

        @Override
        public void visit(MemberValuePair n, Void arg) {
            if (deep) {
                super.visit(n, arg);
            }
            memberValuePair.add(n);
        }

        @Override
        public void visit(MarkerAnnotationExpr n, Void arg) {
            if (deep) {
                super.visit(n, arg);
            }
            markerAnnotationExpr.add(n);
        }

        @Override
        public void visit(LongLiteralExpr n, Void arg) {
            if (deep) {
                super.visit(n, arg);
            }
            longLiteralExpr.add(n);
        }

        @Override
        public void visit(LineComment n, Void arg) {
            if (deep) {
                super.visit(n, arg);
            }
            lineComment.add(n);
        }

        @Override
        public void visit(LabeledStmt n, Void arg) {
            if (deep) {
                super.visit(n, arg);
            }
            labeledStmt.add(n);
        }

        @Override
        public void visit(JavadocComment n, Void arg) {
            if (deep) {
                super.visit(n, arg);
            }
            javadocComment.add(n);
        }

        @Override
        public void visit(IntegerLiteralExpr n, Void arg) {
            if (deep) {
                super.visit(n, arg);
            }
            integerLiteralExpr.add(n);
        }

        @Override
        public void visit(InstanceOfExpr n, Void arg) {
            if (deep) {
                super.visit(n, arg);
            }
            instanceOfExpr.add(n);
        }

        @Override
        public void visit(InitializerDeclaration n, Void arg) {
            if (deep) {
                super.visit(n, arg);
            }
            initializerDeclaration.add(n);
        }

        @Override
        public void visit(IfStmt n, Void arg) {
            if (deep) {
                super.visit(n, arg);
            }
            ifStmt.add(n);
        }

        @Override
        public void visit(ForStmt n, Void arg) {
            if (deep) {
                super.visit(n, arg);
            }
            forStmt.add(n);
        }

        @Override
        public void visit(ForEachStmt n, Void arg) {
            if (deep) {
                super.visit(n, arg);
            }
            forEachStmt.add(n);
        }

        @Override
        public void visit(FieldDeclaration n, Void arg) {
            if (deep) {
                super.visit(n, arg);
            }
            fieldDeclaration.add(n);
        }

        @Override
        public void visit(FieldAccessExpr n, Void arg) {
            if (deep) {
                super.visit(n, arg);
            }
            fieldAccessExpr.add(n);
        }

        @Override
        public void visit(ExpressionStmt n, Void arg) {
            if (deep) {
                super.visit(n, arg);
            }
            expressionStmt.add(n);
        }

        @Override
        public void visit(ExplicitConstructorInvocationStmt n, Void arg) {

            if (deep) {
                super.visit(n, arg);
            }

            explicitConstructorInvocationStmt.add(n);
        }

        @Override
        public void visit(EnumDeclaration n, Void arg) {
            if (deep) {
                super.visit(n, arg);
            }
            enumDeclaration.add(n);
        }

        @Override
        public void visit(EnumConstantDeclaration n, Void arg) {
            if (deep) {
                super.visit(n, arg);
            }
            enumConstantDeclaration.add(n);
        }

        @Override
        public void visit(EnclosedExpr n, Void arg) {
            if (deep) {
                super.visit(n, arg);
            }
            enclosedExpr.add(n);
        }

        @Override
        public void visit(EmptyStmt n, Void arg) {
            if (deep) {
                super.visit(n, arg);
            }
            emptyStmt.add(n);
        }

        @Override
        public void visit(DoubleLiteralExpr n, Void arg) {
            if (deep) {
                super.visit(n, arg);
            }
            doubleLiteralExpr.add(n);
        }

        @Override
        public void visit(DoStmt n, Void arg) {
            if (deep) {
                super.visit(n, arg);
            }
            doStmt.add(n);
        }

        @Override
        public void visit(ContinueStmt n, Void arg) {
            if (deep) {
                super.visit(n, arg);
            }
            continueStmt.add(n);
        }

        @Override
        public void visit(ConstructorDeclaration n, Void arg) {
            if (deep) {
                super.visit(n, arg);
            }
            constructorDeclaration.add(n);
        }

        @Override
        public void visit(ConditionalExpr n, Void arg) {
            if (deep) {
                super.visit(n, arg);
            }
            conditionalExpr.add(n);
        }

        @Override
        public void visit(CompilationUnit n, Void arg) {
            if (deep) {
                super.visit(n, arg);
            }
            compilationUnit.add(n);
        }

        @Override
        public void visit(ClassOrInterfaceType n, Void arg) {
            if (deep) {
                super.visit(n, arg);
            }
            classOrInterfaceType.add(n);
        }

        @Override
        public void visit(ClassOrInterfaceDeclaration n, Void arg) {
            if (deep) {
                super.visit(n, arg);
            }
            classOrInterfaceDeclaration.add(n);
        }

        @Override
        public void visit(ClassExpr n, Void arg) {
            if (deep) {
                super.visit(n, arg);
            }
            classExpr.add(n);
        }

        @Override
        public void visit(CharLiteralExpr n, Void arg) {
            if (deep) {
                super.visit(n, arg);
            }
            charLiteralExpr.add(n);
        }

        @Override
        public void visit(CatchClause n, Void arg) {
            if (deep) {
                super.visit(n, arg);
            }
            catchClause.add(n);
        }

        @Override
        public void visit(CastExpr n, Void arg) {
            if (deep) {
                super.visit(n, arg);
            }
            castExpr.add(n);
        }

        @Override
        public void visit(BreakStmt n, Void arg) {
            if (deep) {
                super.visit(n, arg);
            }
            breakStmt.add(n);
        }

        @Override
        public void visit(BooleanLiteralExpr n, Void arg) {
            if (deep) {
                super.visit(n, arg);
            }
            booleanLiteralExpr.add(n);
        }

        @Override
        public void visit(BlockStmt n, Void arg) {
            if (deep) {
                super.visit(n, arg);
            }
            blockStmt.add(n);
        }

        @Override
        public void visit(BlockComment n, Void arg) {
            if (deep) {
                super.visit(n, arg);
            }
            blockComment.add(n);
        }

        @Override
        public void visit(BinaryExpr n, Void arg) {
            if (deep) {
                super.visit(n, arg);
            }
            binaryExpr.add(n);
        }

        @Override
        public void visit(AssignExpr n, Void arg) {
            if (deep) {
                super.visit(n, arg);
            }
            assignExpr.add(n);
        }

        @Override
        public void visit(AssertStmt n, Void arg) {
            if (deep) {
                super.visit(n, arg);
            }
            assertStmt.add(n);
        }

        @Override
        public void visit(ArrayInitializerExpr n, Void arg) {
            if (deep) {
                super.visit(n, arg);
            }
            arrayInitializerExpr.add(n);
        }

        @Override
        public void visit(ArrayCreationExpr n, Void arg) {
            if (deep) {
                super.visit(n, arg);
            }
            arrayCreationExpr.add(n);
        }

        @Override
        public void visit(ArrayAccessExpr n, Void arg) {
            if (deep) {
                super.visit(n, arg);
            }
            arrayAccessExpr.add(n);
        }

        @Override
        public void visit(AnnotationMemberDeclaration n, Void arg) {
            if (deep) {
                super.visit(n, arg);
            }
            annotationMemberDeclaration.add(n);
        }

        @Override
        public void visit(AnnotationDeclaration n, Void arg) {
            if (deep) {
                super.visit(n, arg);
            }
            annotationDeclaration.add(n);
        }

        public ArrayList<ExplicitConstructorInvocationStmt> getExplicitConstructorInvocationStmt() {
            return explicitConstructorInvocationStmt;
        }

        public ArrayList<SwitchExpr> getSwitchExpr() {
            return switchExpr;
        }

        public ArrayList<Parameter> getParameter() {
            return parameter;
        }

        public ArrayList<AnnotationDeclaration> getAnnotationDeclaration() {
            return annotationDeclaration;
        }

        public ArrayList<AnnotationMemberDeclaration> getAnnotationMemberDeclaration() {
            return annotationMemberDeclaration;
        }

        public ArrayList<ArrayAccessExpr> getArrayAccessExpr() {
            return arrayAccessExpr;
        }

        public ArrayList<ArrayCreationExpr> getArrayCreationExpr() {
            return arrayCreationExpr;
        }

        public ArrayList<ArrayInitializerExpr> getArrayInitializerExpr() {
            return arrayInitializerExpr;
        }

        public ArrayList<AssertStmt> getAssertStmt() {
            return assertStmt;
        }

        public ArrayList<AssignExpr> getAssignExpr() {
            return assignExpr;
        }

        public ArrayList<BinaryExpr> getBinaryExpr() {
            return binaryExpr;
        }

        public ArrayList<BlockComment> getBlockComment() {
            return blockComment;
        }

        public ArrayList<BlockStmt> getBlockStmt() {
            return blockStmt;
        }

        public ArrayList<BooleanLiteralExpr> getBooleanLiteralExpr() {
            return booleanLiteralExpr;
        }

        public ArrayList<BreakStmt> getBreakStmt() {
            return breakStmt;
        }

        public ArrayList<CastExpr> getCastExpr() {
            return castExpr;
        }

        public ArrayList<CatchClause> getCatchClause() {
            return catchClause;
        }

        public ArrayList<CharLiteralExpr> getCharLiteralExpr() {
            return charLiteralExpr;
        }

        public ArrayList<ClassExpr> getClassExpr() {
            return classExpr;
        }

        public ArrayList<ClassOrInterfaceDeclaration> getClassOrInterfaceDeclaration() {
            return classOrInterfaceDeclaration;
        }

        public ArrayList<ClassOrInterfaceType> getClassOrInterfaceType() {
            return classOrInterfaceType;
        }

        public ArrayList<CompilationUnit> getCompilationUnit() {
            return compilationUnit;
        }

        public ArrayList<ConditionalExpr> getConditionalExpr() {
            return conditionalExpr;
        }

        public ArrayList<ConstructorDeclaration> getConstructorDeclaration() {
            return constructorDeclaration;
        }

        public ArrayList<ContinueStmt> getContinueStmt() {
            return continueStmt;
        }

        public ArrayList<DoStmt> getDoStmt() {
            return doStmt;
        }

        public ArrayList<DoubleLiteralExpr> getDoubleLiteralExpr() {
            return doubleLiteralExpr;
        }

        public ArrayList<EmptyStmt> getEmptyStmt() {
            return emptyStmt;
        }

        public ArrayList<EnclosedExpr> getEnclosedExpr() {
            return enclosedExpr;
        }

        public ArrayList<EnumConstantDeclaration> getEnumConstantDeclaration() {
            return enumConstantDeclaration;
        }

        public ArrayList<EnumDeclaration> getEnumDeclaration() {
            return enumDeclaration;
        }

        public ArrayList<ExpressionStmt> getExpressionStmt() {
            return expressionStmt;
        }

        public ArrayList<FieldAccessExpr> getFieldAccessExpr() {
            return fieldAccessExpr;
        }

        public ArrayList<FieldDeclaration> getFieldDeclaration() {
            return fieldDeclaration;
        }

        public ArrayList<ForEachStmt> getForEachStmt() {
            return forEachStmt;
        }

        public ArrayList<ForStmt> getForStmt() {
            return forStmt;
        }

        public ArrayList<IfStmt> getIfStmt() {
            return ifStmt;
        }

        public ArrayList<InitializerDeclaration> getInitializerDeclaration() {
            return initializerDeclaration;
        }

        public ArrayList<InstanceOfExpr> getInstanceOfExpr() {
            return instanceOfExpr;
        }

        public ArrayList<IntegerLiteralExpr> getIntegerLiteralExpr() {
            return integerLiteralExpr;
        }

        public ArrayList<JavadocComment> getJavadocComment() {
            return javadocComment;
        }

        public ArrayList<LabeledStmt> getLabeledStmt() {
            return labeledStmt;
        }

        public ArrayList<LineComment> getLineComment() {
            return lineComment;
        }

        public ArrayList<LongLiteralExpr> getLongLiteralExpr() {
            return longLiteralExpr;
        }

        public ArrayList<MarkerAnnotationExpr> getMarkerAnnotationExpr() {
            return markerAnnotationExpr;
        }

        public ArrayList<MemberValuePair> getMemberValuePair() {
            return memberValuePair;
        }

        public ArrayList<MethodCallExpr> getMethodCallExpr() {
            return methodCallExpr;
        }

        public ArrayList<MethodDeclaration> getMethodDeclaration() {
            return methodDeclaration;
        }

        public ArrayList<NameExpr> getNameExpr() {
            return nameExpr;
        }

        public ArrayList<NormalAnnotationExpr> getNormalAnnotationExpr() {
            return normalAnnotationExpr;
        }

        public ArrayList<NullLiteralExpr> getNullLiteralExpr() {
            return nullLiteralExpr;
        }

        public ArrayList<ObjectCreationExpr> getObjectCreationExpr() {
            return objectCreationExpr;
        }

        public ArrayList<PackageDeclaration> getPackageDeclaration() {
            return packageDeclaration;
        }

        public ArrayList<PrimitiveType> getPrimitiveType() {
            return primitiveType;
        }

        public ArrayList<Name> getName() {
            return name;
        }

        public ArrayList<SimpleName> getSimpleName() {
            return simpleName;
        }

        public ArrayList<ArrayType> getArrayType() {
            return arrayType;
        }

        public ArrayList<ArrayCreationLevel> getArrayCreationLevel() {
            return arrayCreationLevel;
        }

        public ArrayList<IntersectionType> getIntersectionType() {
            return intersectionType;
        }

        public ArrayList<UnionType> getUnionType() {
            return unionType;
        }

        public ArrayList<ReturnStmt> getReturnStmt() {
            return returnStmt;
        }

        public ArrayList<SingleMemberAnnotationExpr> getSingleMemberAnnotationExpr() {
            return singleMemberAnnotationExpr;
        }

        public ArrayList<StringLiteralExpr> getStringLiteralExpr() {
            return stringLiteralExpr;
        }

        public ArrayList<SuperExpr> getSuperExpr() {
            return superExpr;
        }

        public ArrayList<SwitchEntry> getSwitchEntry() {
            return switchEntry;
        }

        public ArrayList<SwitchStmt> getSwitchStmt() {
            return switchStmt;
        }

        public ArrayList<SynchronizedStmt> getSynchronizedStmt() {
            return synchronizedStmt;
        }

        public ArrayList<ThisExpr> getThisExpr() {
            return thisExpr;
        }

        public ArrayList<ThrowStmt> getThrowStmt() {
            return throwStmt;
        }

        public ArrayList<TryStmt> getTryStmt() {
            return tryStmt;
        }

        public ArrayList<LocalClassDeclarationStmt> getLocalClassDeclarationStmt() {
            return localClassDeclarationStmt;
        }

        public ArrayList<TypeParameter> getTypeParameter() {
            return typeParameter;
        }

        public ArrayList<UnaryExpr> getUnaryExpr() {
            return unaryExpr;
        }

        public ArrayList<UnknownType> getUnknownType() {
            return unknownType;
        }

        public ArrayList<VariableDeclarationExpr> getVariableDeclarationExpr() {
            return variableDeclarationExpr;
        }

        public ArrayList<VariableDeclarator> getVariableDeclarator() {
            return variableDeclarator;
        }

        public ArrayList<VoidType> getVoidType() {
            return voidType;
        }

        public ArrayList<WhileStmt> getWhileStmt() {
            return whileStmt;
        }

        public ArrayList<WildcardType> getWildcardType() {
            return wildcardType;
        }

        public ArrayList<LambdaExpr> getLambdaExpr() {
            return lambdaExpr;
        }

        public ArrayList<MethodReferenceExpr> getMethodReferenceExpr() {
            return methodReferenceExpr;
        }

        public ArrayList<TypeExpr> getTypeExpr() {
            return typeExpr;
        }

        public ArrayList<NodeList> getNodeList() {
            return nodeList;
        }

        public ArrayList<ImportDeclaration> getImportDeclaration() {
            return importDeclaration;
        }

    }

}
