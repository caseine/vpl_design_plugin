/*
 * Creative commons CC BY-NC-SA 2020 Yvan Maillot <yvan.maillot@uha.fr>
 *
 *     Share - You can copy and redistribute the material in any medium or format
 *
 *     Adapt - You can remix, transform, and build upon the material
 *
 * Under the following terms :
 *
 *     Attribution - You must give appropriate credit, provide a link to the license,
 *     and indicate if changes were made. You may do so in any reasonable manner,
 *     but not in any way that suggests the licensor endorses you or your use.
 *
 *     NonCommercial — You may not use the material for commercial purposes.
 *
 *     ShareAlike — If you remix, transform, or build upon the material,
 *     you must distribute your contributions under the same license as the original.
 *
 * Notices:    You do not have to comply with the license for elements of
 *             the material in the public domain or where your use is permitted
 *             by an applicable exception or limitation.
 *
 * No warranties are given. The license may not give you all of the permissions
 * necessary for your intended use. For example, other rights such as publicity,
 * privacy, or moral rights may limit how you use the material.
 *
 * See <https://creativecommons.org/licenses/by-nc-sa/4.0/>.
 */
package caseine.publication.producers;

import caseine.reflect.ReflectUtilities;
import caseine.tags.CodeQualityToCheck;

import java.lang.reflect.Constructor;
import java.util.Formatter;
import java.util.Locale;
import java.util.TreeMap;

/*
 * @author Yvan Maillot <yvan.maillot@uha.fr>
 */
public class CodeQualityToCheckProducerForConstructors extends Producer {

    public CodeQualityToCheckProducerForConstructors(Class<?> C, TreeMap<String, StringBuilder> msb, Constructor<?> constructor, CodeQualityToCheck codeQualityToCheck, int testNumber) {

        super(C, msb, String.format("p%06d000_testCodeQualityConstructor%s%s_%d()", codeQualityToCheck.priority(), C.getSimpleName(), ReflectUtilities.paramsToString(constructor), testNumber));

        Formatter formatter = new Formatter(stringBuilder, Locale.US);

        f("\n@Test\n", formatter);

        if (codeQualityToCheck.grade() != Double.MIN_VALUE) {
            f("@caseine.format.javajunit.Grade(%f)\n", formatter, codeQualityToCheck.grade());
        }

        f("public void %s {\n", formatter, methodName);

        f("   System.out.println(\"Check Quality code in Constructor %s%s\");\n", formatter, C.getSimpleName(), ReflectUtilities.paramsToString(constructor));

        if (codeQualityToCheck.requiersUnitTestsBefore().length > 0) {
            f("%s", formatter, ReflectUtilities.writePreviousUnitTestCalling(msb, codeQualityToCheck.requiersUnitTestsBefore()));
        }

        f("   try {\n", formatter);
        f("   	com.github.javaparser.ast.body.ConstructorDeclaration md = caseine.reflect.ParserWithReflectUtilities.find(java.lang.Class.forName(\"%s\").getDeclaredConstructor(%s));\n", formatter, C.getName(), ReflectUtilities.paramsToStringClass(constructor).replaceAll("^, ", ""));

        for (String methodName : codeQualityToCheck.codeAnalyserMethodsName()) {
            if (codeQualityToCheck.value().isEmpty()) {
                f("     assertEquals(\"Check the quality of your code\", \"OK\", %s(md).toUpperCase());\n", formatter, methodName);
            } else {
                f("     assertEquals(\"%s\", \"OK\", %s(md).toUpperCase());\n", formatter, codeQualityToCheck.value(), methodName);
            }
        }
        f("   } catch (Exception ex) {\n", formatter);
        f("     fail(\"Fix %s.%s(%s) \");\n", formatter, C.getSimpleName(), constructor.getName(), ReflectUtilities.paramsToString(constructor));
        f("   }\n", formatter);
        f("}\n", formatter);
    }

}
