/*
 * Creative commons CC BY-NC-SA 2020 Yvan Maillot <yvan.maillot@uha.fr>
 *
 *     Share - You can copy and redistribute the material in any medium or format
 *
 *     Adapt - You can remix, transform, and build upon the material
 *
 * Under the following terms :
 *
 *     Attribution - You must give appropriate credit, provide a link to the license,
 *     and indicate if changes were made. You may do so in any reasonable manner,
 *     but not in any way that suggests the licensor endorses you or your use.
 *
 *     NonCommercial — You may not use the material for commercial purposes.
 *
 *     ShareAlike — If you remix, transform, or build upon the material,
 *     you must distribute your contributions under the same license as the original.
 *
 * Notices:    You do not have to comply with the license for elements of
 *             the material in the public domain or where your use is permitted
 *             by an applicable exception or limitation.
 *
 * No warranties are given. The license may not give you all of the permissions
 * necessary for your intended use. For example, other rights such as publicity,
 * privacy, or moral rights may limit how you use the material.
 *
 * See <https://creativecommons.org/licenses/by-nc-sa/4.0/>.
 */
package caseine.publication.producers;

import caseine.reflect.ReflectUtilities;
import caseine.tags.ToCompare;

import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.lang.reflect.Method;
import java.util.Formatter;
import java.util.List;
import java.util.Locale;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Pour générer des tests unitaires de comparaison entre le comportement d'une
 * méthode annotée {@literal  @ToCompare}
 *
 * @author Yvan Maillot {@literal  <yvan.maillot@uha.fr>}
 */
public class ToCompareProducer extends Producer {

    public ToCompareProducer(Class<?> C, TreeMap<String, StringBuilder> msb, Method method, ToCompare toCompare) {
        super(C, msb, String.format("p%06d000_testMethod%s%s%s()", toCompare.priority(), C.getSimpleName(), method.getName(), caseine.reflect.ReflectUtilities.paramsToString(method)));

        // sb contiendra à la fin de la méthode le code complet du test unitaire
        Formatter formatter = new Formatter(stringBuilder, Locale.US);

        f("\n@Test\n", formatter);
        if (toCompare.grade() != Double.MIN_VALUE) {
            f("@caseine.format.javajunit.Grade(%f)\n", formatter, toCompare.grade());
        }

        f("public void %s {\n", formatter, methodName);

        f("   System.out.println(\"Test Method %s.%s\");\n", formatter, C.getSimpleName(), method.getName());

        // Écrit (si demandé) des appels à des méthodes (probablement des tests unitaires) en préambule à ce
        // test unitaire.
        // Cela peut servir par exemple à empêcher ce test tant que d'autres tests échouent.
        if (toCompare.requiersUnitTestsBefore().length > 0) {
            f("%s", formatter, ReflectUtilities.writePreviousUnitTestCalling(msb, toCompare.requiersUnitTestsBefore()));
        }

        f("   try {\n", formatter);
        // La classe de référence existe dans cf
        f("   	Class<?> classRef = cf.%s.class;\n", formatter, C.getName());
        f("   	Class<?> classToTest = ReflectUtilities.parseType(\"%s\");\n", formatter, C.getName());

        // Cas où le nom de la méthode qui génère les jeux de tests a été omise dans le TAG
        if (toCompare.testSetsMethodName() == null || "".equals(toCompare.testSetsMethodName())) {
            // Un tirage de 100 jeux de tests aléatoires est fait.
            f("   	for (int i = 0; i < %d; ++i) {\n", formatter, toCompare.numberOfAleaTestSets());
            f("         StringBuilder msg = new StringBuilder(\"Fix %s.%s() --> \");\n", formatter, C.getSimpleName(), method.getName());

            if (method.getParameterCount() == 0) {
                // %smsg associé à (toCompare.checkStdOut()?"true, ":"") peut paraître bizarre, mais j'ai été obligé
                // de faire pour des raisons de compatibilité ascendante.
                f("         boolean result = ReflectUtilities.sameResult(%smsg, classRef, classToTest, \"%s\");\n", formatter, (toCompare.checkStdOut() ? "true, " : ""), method.getName());
            } else {
                StringBuilder parameterTypes = new StringBuilder();
                for (Class<?> type : method.getParameterTypes()) {
                    parameterTypes.append(", ").append(type.getName()).append(".class");
                }
                // %smsg associé à (toCompare.checkStdOut()?"true, ":"") peut paraître bizarre, mais j'ai été obligé
                // de le faire pour des raisons de compatibilité ascendante.
                f("         boolean result = ReflectUtilities.sameResult(%smsg, classRef, classToTest, \"%s\" %s);\n", formatter, (toCompare.checkStdOut() ? "true, " : ""), method.getName(), parameterTypes);
            }

            if (toCompare.value().isEmpty()) {
                f("         assertTrue(msg.toString(), result);\n", formatter);
            } else {
                f("         assertTrue(\"%s\", result);\n", formatter, toCompare.value());
            }
            f("   	}\n", formatter);
            f("   } catch (Exception ex) {\n", formatter);
            f("         fail(\"Fix %s.%s() \");\n", formatter, C.getSimpleName(), method.getName());
        } else {
            // Tout ce travail permet de récupérer la méthode à tester et la méthode de référence.
            StringBuilder parameterTypesToTest;
            parameterTypesToTest = new StringBuilder();
            StringBuilder parameterTypesRef;
            parameterTypesRef = new StringBuilder();
            for (Class<?> type : method.getParameterTypes()) {
                parameterTypesRef.append(", caseine.publication.producers.ToCompareProducer.getEquivalentTypeInCfIfExist(").append(type.getName()).append(".class)");
                parameterTypesToTest.append(", ").append(type.getName()).append(".class");
            }
            f("   	Method mtotest = classToTest.getDeclaredMethod(\"%s\" %s);", formatter, method.getName(), parameterTypesToTest);

            f("   	Method mref = classRef.getDeclaredMethod(\"%s\" %s);", formatter, method.getName(), parameterTypesRef);
            // Sinon, les jeux des tests sont extraits de la méthode donnée qui retourne une liste de jeux de tests
            f("   	for (caseine.publication.producers.ToCompareProducer.InstanceAndParameters msgInstAndParams : %s()) {\n", formatter, toCompare.testSetsMethodName());
            // Chaque jeu de test est un tableau d'instances de InstanceAndParameters
            // Ses propriétés sont
            //       0. Le message à afficher à l'étudiant en cas d'erreur ou null pour l'omettre
            //       1. L'entrée standard
            //       2. l'instance sur laquelle s'applique ce jeu de test ou null dans le cas d'une méthode statique
            //       3. la suite sont les arguments de méthode.
            f("         StringBuilder msg = new StringBuilder(\"Fix %s.%s() --> \");\n", formatter, C.getSimpleName(), method.getName());
            f("   	   String instanceMessage = (msgInstAndParams.message == null ? \"\" : (\" : \" + (String) msgInstAndParams.message));", formatter);
            f("   	   Object inst = msgInstAndParams.instance;", formatter);
            // L'instance de référence est (éventuellement clonée) par rapport à l'instance à tester.
            f("   	   Object refinst = ReflectUtilities.clone(inst, classRef);", formatter);
            // Le tableau des arguments à tester est construit à partir du jeu de tests
            f("   	   Object[] params = msgInstAndParams.parameters;", formatter);
            // Le tableau des arguments de référence est construit à partir du jeu de tests en allant leur équivalence dans cf
            f("   	   Object[] paramsRef = new Object[params.length];", formatter);
            f("   	   for (int i = 0; i < paramsRef.length; ++i) {", formatter);
            f("   	      if (params[i] == null)", formatter);
            f("   	          paramsRef[i] = null;", formatter);
            f("   	      else", formatter);
            f("   	      paramsRef[i] = ReflectUtilities.clone(params[i], caseine.publication.producers.ToCompareProducer.getEquivalentTypeInCfIfExist(params[i].getClass()));", formatter);
            f("         }", formatter);

            // Tout ce travail permet de récupérer la méthode à tester et la méthode de référence.
            /*StringBuilder parameterTypesToTest;
            parameterTypesToTest = new StringBuilder("");
            StringBuilder parameterTypesRef;
            parameterTypesRef = new StringBuilder("");
            for (Class<?> type : m.getParameterTypes()) {
                parameterTypesRef.append(", caseine.publication.producers.ToCompareProducer.getEquivalentTypeInCfIfExist(").append(type.getName()).append(".class)");
                parameterTypesToTest.append(", ").append(type.getName()).append(".class");
            }
            f("   	  Method mtotest = classToTest.getDeclaredMethod(\"%s\" %s);", formatter, m.getName(), parameterTypesToTest);

            f("   	  Method mref = classRef.getDeclaredMethod(\"%s\" %s);", formatter, m.getName(), parameterTypesRef);*/
            // Il est possible de tester aussi la sortie standard si la propriétés checkStdOut est vraie
            //if (toCompare.checkStdOut()) {
            f("   	   java.io.PrintStream out;", formatter);
            f("   	   java.io.InputStream in;", formatter);

            f("   	   out = System.out;", formatter);
            f("   	   in = System.in;", formatter);
            //}
            f("   	   String stdoutRef = \"\";", formatter);

            f("   	   Object oref = null;", formatter);

            f("   	   Throwable thrownRef = null;", formatter);

            //if (toCompare.checkStdOut()) {
            f("   	   try (java.io.ByteArrayOutputStream bos = new java.io.ByteArrayOutputStream()) {", formatter);
            f("   	       System.setOut(new java.io.PrintStream(bos));", formatter);

            f("   	       System.setIn(caseine.publication.producers.ToCompareProducer.getInputStreamForStdInFromString(msgInstAndParams.input));", formatter);

            //}
            /////////////////////////////////////////////////////////////////////////////////////////////
            // Invocation de la méthode de référence et récupération du résultat dans oref (ou null si void)
            f("   	       try {", formatter);
            f("   	          oref = mref.invoke(refinst, paramsRef);", formatter);
            f("   	       } catch(Throwable tex) {", formatter);
            f("   	          thrownRef = tex;", formatter);
            f("   	       }", formatter);
            /////////////////////////////////////////////////////////////////////////////////////////////
            // Récupération dans sdtoutTest de la sortie standard
            //if (toCompare.checkStdOut()) {
            f("   	       stdoutRef = bos.toString();", formatter);
            f("   	   } catch (IllegalArgumentException ex) {", formatter);
            f("   	        fail(\"Unexpected error - Call your teacher : \" + java.util.Arrays.toString(ex.getStackTrace()));", formatter);
            f("         } finally {", formatter);
            f("             System.setOut(out);", formatter);
            f("             System.setIn(in);", formatter);
            f("         }", formatter);
            f("   	   out = System.out;", formatter);
            f("   	   in = System.in;", formatter);
            //}
            // La même chose pour l'instance à tester
            f("   	   String stdoutTest = \"\";", formatter);

            f("   	   Object ototest = null;", formatter);

            f("   	   Throwable thrownToTest = null;", formatter);
            //if (toCompare.checkStdOut()) {
            f("   	   try (java.io.ByteArrayOutputStream bos = new  java.io.ByteArrayOutputStream()) {", formatter);
            f("   	       System.setOut(new java.io.PrintStream(bos));", formatter);

            f("   	       System.setIn(caseine.publication.producers.ToCompareProducer.getInputStreamForStdInFromString(msgInstAndParams.input));", formatter);

            //}
            /////////////////////////////////////////////////////////////////////////////////////////////
            f("   	       try {", formatter);
            f("   	          ototest = mtotest.invoke(inst, params);", formatter);
            f("   	       } catch(Throwable tex) {", formatter);
            f("   	          thrownToTest = tex;", formatter);
            f("   	       }", formatter);
            /////////////////////////////////////////////////////////////////////////////////////////////
            //if (toCompare.checkStdOut()) {
            f("   	       stdoutTest = bos.toString();", formatter);
            f("   	   } catch (IllegalArgumentException ex) {", formatter);
            f("         } finally {", formatter);
            f("             System.setOut(out);", formatter);
            f("             System.setIn(in);", formatter);
            f("         }", formatter);
            //}

            if ("".equals(toCompare.comparatorMethodName())) {
                f("         assertTrue(%s + instanceMessage, ReflectUtilities.equals(oref, ototest));\n", formatter,
                        (toCompare.value().isEmpty()
                        ? ("msg.toString()")
                        : ("\"" + toCompare.value() + "\"")));
                if (toCompare.checkStdOut()) {
                    f("         assertEquals(%s + instanceMessage, stdoutRef, stdoutTest);\n", formatter,
                            (toCompare.value().isEmpty()
                            ? ("msg.toString()")
                            : ("\"" + toCompare.value() + " : std output\"")));
                }
                f("         if(thrownRef != null && thrownToTest != null) {\n", formatter);
                f("             assertEquals(%s + instanceMessage, thrownRef.getClass(), thrownToTest.getClass());\n", formatter,
                        (toCompare.value().isEmpty()
                        ? ("msg.toString()")
                        : ("\"" + toCompare.value() + " : Thrown exception\"")));
                f("             if(thrownRef.getCause() != null && thrownToTest.getCause() != null) {\n", formatter);
                f("                     assertEquals(%s + instanceMessage, thrownRef.getCause().getClass(), thrownToTest.getCause().getClass());\n", formatter,
                        (toCompare.value().isEmpty()
                        ? ("msg.toString()")
                        : ("\"" + toCompare.value() + " : Thrown exception\"")));
                f("             }\n", formatter);
                f("         }\n", formatter);
            } else {
                f("         caseine.publication.producers.ToCompareProducer.ResultForComparison resultForComparison = new caseine.publication.producers.ToCompareProducer.ResultForComparison(msgInstAndParams.message, refinst, msgInstAndParams.instance, msgInstAndParams.parameters, msgInstAndParams.input, oref, ototest, stdoutRef, stdoutTest, thrownRef, thrownToTest);\n", formatter);
                f("         assertEquals(%s  + instanceMessage, \"OK\", %s(resultForComparison));\n", formatter,
                        (toCompare.value().isEmpty()
                        ? ("msg.toString()")
                        : ("\"" + toCompare.value() + "\"")), toCompare.comparatorMethodName(), C.getName());
            }

            f("   	}\n", formatter);
            f("   } catch (Exception ex) {\n", formatter);
            f("         fail(\"Fix %s.%s() (PERHAPS an unexpected exception) \" + java.util.Arrays.toString(ex.getStackTrace()));\n", formatter, C.getSimpleName(), method.getName());

        }
        f("   }\n", formatter);
        f("}\n", formatter);
    }

    public static Class<?> getEquivalentTypeInCfIfExist(Class<?> c) {
        try {
            return Class.forName("cf." + c.getName());
        } catch (ClassNotFoundException e) {
            return c;
        }
    }

    /**
     * @param strStdIn The name of a file containing the input or the input
     * itself
     * @return the InputStream which is the opened file named strStdIn or a
     * ByteArrayInputStream of the string stdStdIn if the file named strStdIn
     * does not exist
     */
    public static InputStream getInputStreamForStdInFromString(String strStdIn) {
        if (strStdIn == null || "System.in".equals(strStdIn) || "".equals(strStdIn)) {
            return System.in;
        } else {
            try {
                return new FileInputStream(strStdIn);
            } catch (FileNotFoundException e) {

                return new ByteArrayInputStream(strStdIn.getBytes());
            }
        }
    }

    /**
     * @param toCompare annotation of a student method to compare with a ref
     * method
     * @return the method that gives the test sets. This method should be in a
     * class test, should have no parameter and its return type should be
     * List<InstanceAndParameters>)
     */
    private Method getTestSetsMethod(ToCompare toCompare) {
        String refMethodName = toCompare.testSetsMethodName();
        Pattern pattern = Pattern.compile("(.+)\\.(.+)");
        Matcher m = pattern.matcher(refMethodName);
        if (m.matches()) {
            try {
                Class<?> c = Class.forName(m.group(1));
                Method method = c.getDeclaredMethod(m.group(2));
                if (method.getReturnType() == List.class) {
                    return method;
                } else {
                    return null;
                }
            } catch (ClassNotFoundException | NoSuchMethodException e) {
                return null;
            }
        } else {
            return null;
        }
    }

    /**
     * Pour définir un jeu de tests, c'est-à-dire un ensemble de données, à
     * utiliser lors d'une évaluation qui compare l'exécution de deux méthodes
     * (voir {@link caseine.tags.ToCompare})
     *
     * @author Yvan Maillot {@literal <yvan.maillot@uha.fr>}
     *
     * <h2>La classe
     * &#64;caseine.publication.producers.ToCompareProducer.InstanceAndParameters</h2>
     * <p>
     * Une instance de <code>InstanceAndParameters</code> définit tout ou partie
     * des données suivantes:
     * </p>
     * <ul>
     * <li>{@link #message message}</li>
     * <li>{@link #instance instance}</li>
     * <li>{@link #input input}</li>
     * <li>{@link #parameters parameters}</li>
     * </ul>
     *
     * <p>
     * Un jeu de tests dépend fortement de la méthode à tester. Les fabriques
     * (method factory) suivantes permettent d'élaborer des jeux de tests dans
     * toutes les situations :
     * </p>
     *
     * <ol>
     * <li>
     * <code>{@link #newMessageInputInstanceParams(java.lang.String, java.lang.String, java.lang.Object, java.lang.Object...) newMessageInputInstanceParams(String message, String input, Object instance, Object... parameters)}</code>
     * </li>
     * <li>
     * <code>{@link #newMessageInstanceParams(String message, Object instance, Object... parameters) newMessageInstanceParams(String message, Object instance, Object... parameters)}</code>
     * </li>
     * <li>
     * <code>{@link #newInputInstanceParams(String input, Object instance, Object... parameters) newInputInstanceParams(String input, Object instance, Object... parameters)}</code>
     * </li>
     * <li>
     * <code>{@link #newInstanceParams(Object instance, Object... parameters) newInstanceParams(Object instance, Object... parameters)}</code>
     * </li>
     * <li>
     * <code>{@link #newMessageInputParamsForStaticMethod(String message, String input, Object... parameters) newMessageInputParamsForStaticMethod(String message, String input, Object... parameters)}</code>
     * </li>
     * <li>
     * <code>{@link #newMessageParamsForStaticMethod(String message, Object... parameters)
     * }</code>
     * </li>
     * <li>
     * <code>{@link #newInputParamsForStaticMethod(String input, Object... parameters)
     * }</code>
     * </li>
     * <li>
     * <code>{@link #newParamsForStaticMethod(Object... parameters) }</code>
     * </li>
     * </ol>
     * <p>
     * L'instance créée sera exploitée pour comparer le comportement d'une
     * méthode annotée (celle de l'étudiant) à une méthode référence (celle de
     * l'enseignant).
     * </p>
     *
     * @see caseine.tags.ToCompare
     */
    public static class InstanceAndParameters {

        /**
         * Le message à afficher en cas d'échec de la comparaison (optionnel).
         */
        public final String message;
        /**
         * Le flux en entrée si la méthode à comparer lit des données sur
         * l'entrée standard (au clavier) (optionnel).
         */
        public final String input;
        /**
         * L'objet sur lequel s'applique la méthode à comparer
         * ({@literal null} si elle est déclarée {@code static})
         */
        public final Object instance;
        /**
         * Les arguments de la méthode à comparer.
         */
        public final Object[] parameters;

        /**
         * Pour construire tout type de jeux de tests.
         * <p>
         * Pas toujours facile à utiliser. Préférer les fabriques aux noms
         * évocateurs pour construire des jeux de tests plus facilement.
         *
         * @param message le message si la comparaison échoue.
         * @param input une chaîne de caractères qui contient les données lues
         * par la méthode comparer.
         * @param instance l'instance sur laquelle s'applique la méthode à
         * comparer.
         * @param parameters des arguments requis par la méthode à comparer, en
         * nombre suffisant et de types adéquats.
         */
        public InstanceAndParameters(String message, String input, Object instance, Object... parameters) {
            this.message = message;
            this.input = input;
            this.instance = instance;
            this.parameters = parameters;
        }

        /**
         * Un jeu de tests destiné à une méthode d'instance qui lit des données
         * sur son entrée standard (en général le clavier).
         *
         * @param message le message si la comparaison échoue.
         * @param input une chaîne de caractères qui contient les données lues
         * par la méthode comparer.
         * @param instance l'instance sur laquelle s'applique la méthode à
         * comparer.
         * @param parameters des arguments requis par la méthode à comparer, en
         * nombre suffisant et de types adéquats.
         *
         * @return une instance qui contient le jeu de tests voulu.
         */
        public static InstanceAndParameters newMessageInputInstanceParams(String message, String input, Object instance, Object... parameters) {
            return new InstanceAndParameters(message, input, instance, parameters);
        }

        /**
         * Un jeu de tests destiné à une méthode d'instance.
         *
         * @author Yvan Maillot {@literal  <yvan.maillot@uha.fr>}
         *
         * @param message le message si la comparaison échoue.
         * @param instance l'instance sur laquelle s'applique la méthode à
         * comparer.
         * @param parameters des arguments requis par la méthode à comparer, en
         * nombre suffisant et de types adéquats.
         *
         * @return une instance qui contient le jeu de tests voulu.
         */
        public static InstanceAndParameters newMessageInstanceParams(String message, Object instance, Object... parameters) {
            return new InstanceAndParameters(message, "", instance, parameters);
        }

        /**
         * Un jeu de tests destiné à une méthode de classe (static) qui lit des
         * données sur son entrée standard (en général le clavier).
         *
         * @param message le message si la comparaison échoue.
         * @param input une chaîne de caractères qui contient les données lues
         * par la méthode comparer.
         * @param parameters des arguments requis par la méthode à comparer, en
         * nombre suffisant et de types adéquats.
         *
         * @return une instance qui contient le jeu de tests voulu.
         */
        public static InstanceAndParameters newMessageInputParamsForStaticMethod(String message, String input, Object... parameters) {
            return new InstanceAndParameters(message, input, null, parameters);
        }

        /**
         * Un jeu de tests destiné à une méthode d'instance qui lit des données
         * sur son entrée standard (en général le clavier).
         * <p>
         * Un message standard sera généré en cas d'échec de la comparaison.
         *
         * @param input une chaîne de caractères qui contient les données lues
         * par la méthode comparer.
         * @param instance l'instance sur laquelle s'applique la méthode à
         * comparer.
         * @param parameters des arguments requis par la méthode à comparer, en
         * nombre suffisant et de types adéquats.
         *
         * @return une instance qui contient le jeu de tests voulu.
         */
        public static InstanceAndParameters newInputInstanceParams(String input, Object instance, Object... parameters) {
            return new InstanceAndParameters("", input, instance, parameters);
        }

        /**
         * Un jeu de tests destiné à une méthode d'instance.
         * <p>
         * Un message standard sera généré en cas d'échec de la comparaison.
         *
         * @param instance l'instance sur laquelle s'applique la méthode à
         * comparer.
         * @param parameters des arguments requis par la méthode à comparer, en
         * nombre suffisant et de types adéquats.
         *
         * @return une instance qui contient le jeu de tests voulu.
         */
        public static InstanceAndParameters newInstanceParams(Object instance, Object... parameters) {
            return new InstanceAndParameters("", "", instance, parameters);
        }

        /**
         * Un jeu de tests destiné à une méthode de classe (static).
         *
         * @param message le message si la comparaison échoue.
         * @param parameters des arguments requis par la méthode à comparer, en
         * nombre suffisant et de types adéquats.
         *
         * @return une instance qui contient le jeu de tests voulu.
         */
        public static InstanceAndParameters newMessageParamsForStaticMethod(String message, Object... parameters) {
            return new InstanceAndParameters(message, "", null, parameters);
        }

        /**
         * Un jeu de tests destiné à une méthode de classe (static) qui lit des
         * données sur son entrée standard (en général le clavier).
         * <p>
         * Un message standard sera généré en cas d'échec de la comparaison.
         *
         * @param input une chaîne de caractères qui contient les données lues
         * par la méthode comparer.
         * @param parameters des arguments requis par la méthode à comparer, en
         * nombre suffisant et de types adéquats.
         *
         * @return une instance qui contient le jeu de tests voulu.
         */
        public static InstanceAndParameters newInputParamsForStaticMethod(String input, Object... parameters) {
            return new InstanceAndParameters("", input, null, parameters);
        }

        /**
         * Un jeu de tests destiné à une méthode de classe (static).
         * <p>
         * Un message standard sera généré en cas d'échec de la comparaison.
         *
         * @param parameters des arguments requis par la méthode à comparer, en
         * nombre suffisant et de types adéquats.
         *
         * @return une instance qui contient le jeu de tests voulu.
         */
        public static InstanceAndParameters newParamsForStaticMethod(Object... parameters) {
            return new InstanceAndParameters("", "", null, parameters);
        }

        /**
         * Une représentation qui résume le jeu de tests.
         *
         * @return Une représentation qui résume le jeu de tests.
         */
        @Override
        public String toString() {
            return "InstanceAndParameters{" + "message=" + message + ", input=" + input + ", instance=" + instance + ", parameters=" + java.util.Arrays.toString(parameters) + '}';
        }

    }

    /**
     * Instancie un objet qui encapsule (presque) toutes les données nécessaires
     * pour effectuer une comparaison entre les résultats de l'exécution d'une
     * méthode de l'étudiant avec une référence.
     *
     * @author Yvan Maillot {@literal  <yvan.maillot@uha.fr>}
     *
     */
    public static class ResultForComparison {

        private final String message;
        private final Object inst;
        private final Object refInstance;
        private final Object studentInstance;
        private final Object[] params;
        private final String input;
        private final Object refResult;
        private final Object studentResult;
        private final String refStdOutput;
        private final String studentStdOutput;
        private final Throwable refThrown;
        private final Throwable studentThrown;

        public ResultForComparison(
                String message,
                Object refInstance,
                Object studentInstance,
                Object[] params,
                String input,
                Object refResult,
                Object studentResult,
                String refStdOutput,
                String studentStdOutput,
                Throwable refThrown,
                Throwable studentThrown) {
            this.message = message;
            this.refInstance = refInstance;
            this.inst = this.studentInstance = studentInstance;
            this.params = params;
            this.input = input;
            this.refResult = refResult;
            this.studentResult = studentResult;
            this.refStdOutput = refStdOutput;
            this.studentStdOutput = studentStdOutput;
            this.refThrown = refThrown;
            this.studentThrown = studentThrown;
        }

        /**
         * Pour connaître le message prévu par l'enseignant et à afficher en cas
         * d'échec de la comparaison.
         *
         * @return Le message à afficher en cas d'échec de la comparaison.
         */
        public String getMessage() {
            return message;
        }

        @Deprecated
        public Object getInst() {
            return inst;
        }

        /**
         * Pour connaître les paramètres des méthodes à comparer utilisées pour
         * cette évaluation.
         *
         * @return les paramètres des méthodes à comparer
         */
        public Object[] getParams() {
            return params;
        }

        /**
         * Pour connaître le flux d'entrée consommé par les méthodes à comparer
         * utilisées pour cette évaluation.
         *
         * @return le flux d'entrée consommé par les méthodes.
         */
        public String getInput() {
            return input;
        }

        /**
         * Pour connaître le retour de la méthode de référence lors de cette
         * évaluation.
         * <p>
         * Attention à la comparaison avec le retour de l'étudiant, car ils
         * peuvent ne pas être de même type. Il est préférable pour les comparer
         * d'utiliser {@link caseine.reflect.ReflectUtilities#equals(java.lang.Object, java.lang.Object)
         * }.
         *
         * @return le retour de la méthode de référence lors de cette
         * évaluation.
         */
        public Object getRefResult() {
            return refResult;
        }

        /**
         * Pour connaître le retour de la méthode de l'étudiant lors de cette
         * évaluation.
         * <p>
         * Attention à la comparaison avec le retour de la référence, car ils
         * peuvent ne pas être de même type. Il est préférable pour les comparer
         * d'utiliser {@link caseine.reflect.ReflectUtilities#equals(java.lang.Object, java.lang.Object)
         * }.
         *
         * @return le retour de la méthode de l'étudiant lors de cette
         * évaluation.
         */
        public Object getStudentResult() {
            return studentResult;
        }

        /**
         * Pour connaître le contenu de la sortie standard (affichage à l'écran)
         * produit par la méthode de référence lors de cette évaluation.
         *
         * @return le contenu de la sortie standard produit par la méthode de
         * référence.
         */
        public String getRefStdOutput() {
            return refStdOutput;
        }

        /**
         * Pour connaître le contenu de la sortie standard (affichage à l'écran)
         * produit par la méthode de l'étudiant lors de cette évaluation.
         *
         * @return le contenu de la sortie standard produit par la méthode de
         * l'étudiant.
         */
        public String getStudentStdOutput() {
            return studentStdOutput;
        }

        /**
         * Pour connaître l'exception lancée par la méthode de référence lors de
         * cette évaluation.
         *
         * @return l'exception lancée par la méthode de référence.
         */
        public Throwable getRefThrown() {
            return refThrown;
        }

        /**
         * Pour connaître l'exception lancée par la méthode de l'étudiant lors
         * de cette évaluation.
         *
         * @return l'exception lancée par la méthode de l'étudiant.
         */
        public Throwable getStudentThrown() {
            return studentThrown;
        }

        /**
         * Pour connaître l'instance sur laquelle s'est appliquée la méthode de
         * référence lors de cette évaluation.
         * <p>
         * Attention à la comparaison avec l'instance de référence, car ils
         * peuvent ne pas être de même type. Il est préférable pour les comparer
         * d'utiliser {@link caseine.reflect.ReflectUtilities#equals(java.lang.Object, java.lang.Object)
         * }.
         *
         * @return le retour de la méthode de référence lors de cette
         * évaluation.
         */
        public Object getRefInstance() {
            return refInstance;
        }

        /**
         * Pour connaître l'instance sur laquelle s'est appliquée la méthode de
         * l'étudiant lors de cette évaluation.
         * <p>
         * Attention à la comparaison avec l'instance de l'étudiant, car ils
         * peuvent ne pas être de même type. Il est préférable pour les comparer
         * d'utiliser {@link caseine.reflect.ReflectUtilities#equals(java.lang.Object, java.lang.Object)
         * }.
         *
         * @return le retour de la méthode de l'étudiant lors de cette
         * évaluation.
         */
        public Object getStudentInstance() {
            return studentInstance;
        }

        /**
         * Une représentation qui résume les éléments de comparaison.
         *
         * @return Une représentation qui résume le jeu de tests.
         */
        @Override
        public String toString() {
            return "ResultForComparison{"
                    + "message=" + message
                    + ", inst=" + inst
                    + ", params=" + java.util.Arrays.toString(params)
                    + ", input=" + input
                    + ", refResult=" + refResult
                    + ", studentResult=" + studentResult
                    + ", refStdOutput=" + refStdOutput
                    + ", studentStdOutput=" + studentStdOutput
                    + ", refThrown=" + refThrown
                    + ", studentThrown=" + studentThrown + '}';
        }
    }
}
