/*
 * Creative commons CC BY-NC-SA 2020 Yvan Maillot <yvan.maillot@uha.fr>
 *
 *     Share - You can copy and redistribute the material in any medium or format
 *
 *     Adapt - You can remix, transform, and build upon the material
 *
 * Under the following terms :
 *
 *     Attribution - You must give appropriate credit, provide a link to the license,
 *     and indicate if changes were made. You may do so in any reasonable manner,
 *     but not in any way that suggests the licensor endorses you or your use.
 *
 *     NonCommercial — You may not use the material for commercial purposes.
 *
 *     ShareAlike — If you remix, transform, or build upon the material,
 *     you must distribute your contributions under the same license as the original.
 *
 * Notices:    You do not have to comply with the license for elements of
 *             the material in the public domain or where your use is permitted
 *             by an applicable exception or limitation.
 *
 * No warranties are given. The license may not give you all of the permissions
 * necessary for your intended use. For example, other rights such as publicity,
 * privacy, or moral rights may limit how you use the material.
 *
 * See <https://creativecommons.org/licenses/by-nc-sa/4.0/>.
 */
package caseine.publication.producers;

import caseine.reflect.ReflectUtilities;
import caseine.tags.StatementCountToCheck;

import java.lang.reflect.Constructor;
import java.lang.reflect.Executable;
import java.lang.reflect.Method;
import java.util.Formatter;
import java.util.Locale;
import java.util.TreeMap;

/*
 * @author Yvan Maillot <yvan.maillot@uha.fr>
 */
public class StatementCountToCheckProducer extends Producer {
    private final Executable executable;

    public StatementCountToCheckProducer(Class<?> C, TreeMap<String, StringBuilder> msb, Method method, StatementCountToCheck stmtCountToCheck) {
        super(C, msb, String.format("p%06d000_testStmtCountMethod%s%s%s()", stmtCountToCheck.priority(), C.getSimpleName(), method.getName(), ReflectUtilities.paramsToString(method)));
        this.executable = method;

        // sb contiendra à la fin de la méthode le code complet du test unitaire
        Formatter formatter = new Formatter(stringBuilder, Locale.US);

        f("\n@Test\n", formatter);
        if (stmtCountToCheck.grade() != Double.MIN_VALUE) {
            f("@caseine.format.javajunit.Grade(%f)\n", formatter, stmtCountToCheck.grade());
        }

        f("public void %s {\n", formatter, methodName);

        f("   System.out.println(\"Check Number of statements in Method %s.%s\");\n", formatter, C.getSimpleName(), method.getName());


        // Écrit (si demandé) des appels à des méthodes (probablement des tests unitaires) en préambule à ce
        // test unitaire.
        // Cela peut servir par exemple à empêcher ce test tant que d'autres tests échouent.
        if (stmtCountToCheck.requiersUnitTestsBefore().length > 0) {
            f("%s", formatter, ReflectUtilities.writePreviousUnitTestCalling(msb , stmtCountToCheck.requiersUnitTestsBefore()));
        }


        f("   try {\n", formatter);
        f("   	com.github.javaparser.ast.body.MethodDeclaration md = caseine.reflect.ParserWithReflectUtilities.find(java.lang.Class.forName(\"%s\").getDeclaredMethod(\"%s\" %s));\n", formatter, C.getName(), method.getName(), ReflectUtilities.paramsToStringClass(method));
        f("   	if (md.getBody().isPresent()) {\n", formatter);
        f("   	    int nbStmts = md.getBody().get().getStatements().size();\n", formatter);
        if (stmtCountToCheck.value().isEmpty()) {
            f("   	    assertTrue(\"Check the number of statements (must between %d and %d)\", %d <= nbStmts && nbStmts <= %d );\n", formatter, stmtCountToCheck.minStmt(), stmtCountToCheck.maxStmt(), stmtCountToCheck.minStmt(), stmtCountToCheck.maxStmt());
        } else {
            f("   	    assertTrue(\"%s\", %d <= nbStmts && nbStmts <= %d);\n", formatter, stmtCountToCheck.value(), stmtCountToCheck.minStmt(), stmtCountToCheck.maxStmt());
        }
        f("   	} else {\n", formatter);
        f("       fail(\"Missing method body %s.%s(%s) \");\n", formatter, C.getName(), method.getName(), ReflectUtilities.paramsToStringClass(method).replaceAll("^, ", "").replaceAll("\\.class", ""));
        f("     }\n", formatter);

        f("   } catch (NoSuchMethodException ex) {\n", formatter);
        f("     fail(\"Missing method %s.%s(%s) \");\n", formatter, C.getName(), method.getName(), ReflectUtilities.paramsToStringClass(method).replaceAll("^, ", "").replaceAll("\\.class", ""));
        f("   } catch (Exception ex) {\n", formatter);
        f("     fail(\"Fix %s.%s() \");\n", formatter, C.getSimpleName(), method.getName());
        f("   }\n", formatter);
        f("}\n", formatter);
    }


    public StatementCountToCheckProducer(Class<?> C, TreeMap<String, StringBuilder> msb, Constructor<?> constructor, StatementCountToCheck stmtCountToCheck) {
        super(C, msb, String.format("p%06d000_testStmtCountConstructor%s%s%s()", stmtCountToCheck.priority(), C.getSimpleName(), "", ReflectUtilities.paramsToString(constructor)));
        this.executable = constructor;

        // sb contiendra à la fin de la méthode le code complet du test unitaire
        Formatter formatter = new Formatter(stringBuilder, Locale.US);

        f("\n@Test\n", formatter);
        if (stmtCountToCheck.grade() != Double.MIN_VALUE) {
            f("@caseine.format.javajunit.Grade(%f)\n", formatter, stmtCountToCheck.grade());
        }

        f("public void %s {\n", formatter, methodName);

        f("   System.out.println(\"Check Number of statements in Constructor %s\");\n", formatter, C.getSimpleName());


        // Écrit (si demandé) des appels à des méthodes (probablement des tests unitaires) en préambule à ce
        // test unitaire.
        // Cela peut servir par exemple à empêcher ce test tant que d'autres tests échouent.
        if (stmtCountToCheck.requiersUnitTestsBefore().length > 0) {
            f("%s", formatter, ReflectUtilities.writePreviousUnitTestCalling(msb , stmtCountToCheck.requiersUnitTestsBefore()));
        }

        f("   try {\n", formatter);
        f("   	com.github.javaparser.ast.body.ConstructorDeclaration md = caseine.reflect.ParserWithReflectUtilities.find(java.lang.Class.forName(\"%s\").getDeclaredConstructor(%s));\n", formatter, C.getName(), ReflectUtilities.paramsToStringClass(executable).replaceAll("^, ", ""));

        f("   	int nbStmts = md.getBody().getStatements().size();\n", formatter);
        if (stmtCountToCheck.value().isEmpty()) {
            f("   	 assertTrue(\"Check the number of statements (must between %d and %d)\", %d <= nbStmts && nbStmts <= %d );\n", formatter, stmtCountToCheck.minStmt(), stmtCountToCheck.maxStmt(), stmtCountToCheck.minStmt(), stmtCountToCheck.maxStmt());
        } else {
            f("   	 assertTrue(\"%s\", %d <= nbStmts && nbStmts <= %d);\n", formatter, stmtCountToCheck.value(), stmtCountToCheck.minStmt(), stmtCountToCheck.maxStmt());
        }

        f("   } catch (NoSuchMethodException ex) {\n", formatter);
        f("     fail(\"Missing constructor %s(%s) \");\n", formatter, C.getName(), ReflectUtilities.paramsToStringClass(executable).replaceAll("^, ", "").replaceAll("\\.class", ""));
        f("   } catch (Exception ex) {\n", formatter);
        f("     fail(\"Fix %s() \");\n", formatter, C.getSimpleName());
        f("   }\n", formatter);
        f("}\n", formatter);
    }
}
