/*
 * Copyright 2019: Yvan Maillot
 * This software is part of the Caseine project.
 * This software was developped with the support of the following organizations:
 * Université de Haute-Alsace
 * Institut Polytechnique de Grenoble
 *
 *
 * This file is part of the VPL Design Tool.
 * The VPL Design Tool is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The VPL Design Tool is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the VPL Design Tool.  If not, see <https://www.gnu.org/licenses/>.
 */
package caseine.publication.test;

import com.github.javaparser.StaticJavaParser;
import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.body.BodyDeclaration;
import com.github.javaparser.ast.body.ClassOrInterfaceDeclaration;

import caseine.checker.Checker;

public class CompilationUnitTest extends CompilationUnit {

    public CompilationUnitTest(String classRef, String packageRef, String importRef, Checker chk) {
        addImport("java.lang.reflect.*");
        addImport("org.junit.*");
        addImport("static org.junit.Assert.*");
        addImport("org.junit.FixMethodOrder");
        addImport("org.junit.runners.MethodSorters");
        addImport("caseine.reflect.ReflectUtilities");
        addImport("org.junit.jupiter.api.TestMethodOrder");
        addImport("org.junit.jupiter.api.MethodOrderer");

        // addImport(importRef);
        ClassOrInterfaceDeclaration corid = addClass(classRef);
        if (!"".equals(packageRef)) {
            setPackageDeclaration(packageRef);
        }
        corid.addAnnotation(StaticJavaParser.parseAnnotation("@FixMethodOrder(MethodSorters.NAME_ASCENDING)"));
        corid.addAnnotation(StaticJavaParser.parseAnnotation("@TestMethodOrder(MethodOrderer.MethodName.class)"));
        for (BodyDeclaration<?> bd : chk) {
            corid.addMember(bd);
        }
    }

    public static void main(String[] args) {
        CompilationUnitTest ct = new CompilationUnitTest("PointTest", "cf.geom", "cf.geom.Point", new Checker(caseine.tests.A.class));


        System.out.println(ct);
    }
}
