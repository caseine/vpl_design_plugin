/*
 * Copyright 2019: Yvan Maillot
 * This software is part of the Caseine project.
 * This software was developed with the support of the following organizations:
 * Université de Haute-Alsace
 * Institut Polytechnique de Grenoble
 *
 *
 * This file is part of the VPL Design Tool.
 * The VPL Design Tool is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The VPL Design Tool is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the VPL Design Tool.  If not, see <https://www.gnu.org/licenses/>.
 */
package caseine.publication;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.github.javaparser.ParseResult;
import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.ImportDeclaration;
import com.github.javaparser.ast.PackageDeclaration;
import com.github.javaparser.ast.body.TypeDeclaration;
import com.github.javaparser.ast.expr.AnnotationExpr;
import com.github.javaparser.utils.Pair;
import com.github.javaparser.utils.SourceRoot;

import caseine.checker.Checker;
import caseine.extra.utils.mutations.Mutation;
import caseine.publication.test.CompilationUnitTest;
import caseine.tags.ClassTestPriority;
import caseine.tags.CorrectedFilesForStudent;
import caseine.tags.FileToRemove;

/**
 * Publisher manipulates a package tree containing annotated java files using
 * the annotations in the "ReflectUtilities" library to produce the java files
 * in the "Required Files", "Corrected Files" and "Execution Files" parts.
 */
public class Publisher implements Iterable<String> {

    private final Path src;
    private final Path test;
    private final Path classesTarget;
    private final Path cfTarget;
    private final Path rfTarget;
    private final Path testTarget;
    private final Path testCfTarget;
    private final ClassLoader cl;
    private final Set<String> classes;
    private Set<Path> pathsToRemove;
    private Set<Path> pathsToTest;

    private final Path resourcesSrc;
    private final Path resourcesTest;

    private final static String[] excludedFiles = {".DS_Store"};
    private double grade; // to write in .caseine
    private int maxfiles = 20; // to write in .caseine (default 20)

    private boolean isGraphical = false; // to choose basedon in .caseine (default false)

    private final static class PriorityTestClassName implements Comparable<PriorityTestClassName> {

        public final int priority;
        public final String className;

        public PriorityTestClassName(String className) {
            this(Integer.MAX_VALUE, className);
        }

        public PriorityTestClassName(int priority, String className) {
            this.priority = priority;
            this.className = className;
        }

        @Override
        public int compareTo(PriorityTestClassName o) {
            if (priority < o.priority) {
                return -1;
            } else if (priority > o.priority) {
                return 1;
            } else {
                return this.className.compareTo(o.className);
            }
        }

        @Override
        public int hashCode() {
            int hash = 5;
            hash = 97 * hash + this.priority;
            hash = 97 * hash + Objects.hashCode(this.className);
            return hash;
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            final PriorityTestClassName other = (PriorityTestClassName) obj;
            if (this.priority != other.priority) {
                return false;
            }
            return Objects.equals(this.className, other.className);
        }

        @Override
        public String toString() {
            return className;
        }

    }

    // Utilisé pour générer les JUnitFiles dans l'ordre défini par @ClassTestPriority
    private final Set<PriorityTestClassName> priorityTestClassNameTreeSet = new TreeSet<>();

    private class StringIterator implements Iterator<String> {

        private final Iterator<PriorityTestClassName> it = priorityTestClassNameTreeSet.iterator();

        @Override
        public boolean hasNext() {
            return it.hasNext();
        }

        @Override
        public String next() {
            return it.next().toString();
        }
    }

    @Override
    public Iterator<String> iterator() {
        return new StringIterator();
    }

    // Rajouté uniquement pour faire quelques tests
    private final Path testTargetRF;
    private final Path testCfTargetRF;

    /*
     *
     * @param src path of the source directory base
     * @param bin path for binaries
     * @param test path of the source test directory base
     * @param target path of the destination
     */
    public Publisher(Path src, Path bin, Path test, Path target) {
        this(src, bin, test, null, null, target, null);
    }

    public Publisher(Path src, Path bin, Path test, Path resourcesSrc, Path resourcesTest, Path target, ClassLoader cl) {
    	this.cl = cl;
        this.src = src;
        this.test = test;
        this.resourcesSrc = resourcesSrc;
        this.resourcesTest = resourcesTest;
        this.classesTarget = bin;
        this.cfTarget = new File(target.toFile(), "cf").toPath();
        this.rfTarget = new File(target.toFile(), "rf").toPath();
        this.testTarget = new File(target.toFile(), "ef").toPath();
        this.testCfTarget = new File(testTarget.toFile(), "cf").toPath();
        this.pathsToRemove = new HashSet<>();
        this.classes = new HashSet<>();
        // Rajouté uniquement pour faire des tests en local
        this.testTargetRF = new File(target.toFile(), "testrf").toPath();
        this.testCfTargetRF = new File(testTargetRF.toFile(), "cf").toPath();
    }

    private void addPackage(CompilationUnit cu) {
        Optional<TypeDeclaration<?>> optionalTypeDeclaration = cu.getPrimaryType();
        optionalTypeDeclaration.ifPresent(typeDeclaration -> this.classes.add(typeDeclaration.getFullyQualifiedName().get()));
    }

    private boolean containsGraphical(CompilationUnit cu) {
    	return cu.getImports().stream().anyMatch(
                id -> id.getNameAsString().contains("javafx") ||
                        id.getNameAsString().contains("javax.swing")
        );
    }

    public double getGrade() {
        return grade;
    }

    public int getMaxfiles() {
        return maxfiles;
    }

    public boolean isGraphical() {
    	return isGraphical;
    }

    public void publishCf() throws IOException {

        SourceRoot sr = new SourceRoot(src);
        List<ParseResult<CompilationUnit>> prs = sr.tryToParse();
        if (prs.stream().allMatch(ParseResult::isSuccessful)) {
            sr.getCompilationUnits().stream()
                    .peek(this::addPackage)
                    .forEach(ParserUtils::annotationSuppression);
            sr.saveAll(cfTarget);
            isGraphical = sr.getCompilationUnits().stream().anyMatch(this::containsGraphical);
            maxfiles = sr.getCompilationUnits().size();

        } else {
            prs.stream()
                    .filter(pr -> !pr.isSuccessful())
                    .forEach(pr -> System.out.println("Issue " + pr.getResult().toString()));
        }
        File cfDirectory = cfTarget.toFile();
        if (!cfDirectory.exists()) {
        	cfDirectory.mkdirs();
        }
    }

    private Path compilationUnitToPath(CompilationUnit cu) {
        String prefix = "";
        Optional<PackageDeclaration> optionalPackageDeclaration = cu.getPackageDeclaration();
        if (optionalPackageDeclaration.isPresent()) {
            prefix = optionalPackageDeclaration.get().getNameAsString().replaceAll("\\.", "/");
        }
        return new File(prefix, cu.getPrimaryType().get().getNameAsString() + ".java").toPath();
    }

    public Set<Path> getPathToRemove() {
        return pathsToRemove;
    }

    public Set<Path> getPathToTest() {
        return pathsToTest;
    }

    public void publishRf() throws IOException {
        SourceRoot sr = new SourceRoot(src);
        List<ParseResult<CompilationUnit>> prs = sr.tryToParse();
        if (prs.stream().allMatch(ParseResult::isSuccessful)) {
            pathsToTest = sr.getCompilationUnits().stream()
                    .filter(f->ParserUtils.compilationUnitIsATest(f))
                    .map(this::compilationUnitToPath)
                    .collect(Collectors.toSet());
            // Si une classe est annotée CorrectedFilesForStudent, tout est écrit dans rf comme dans cf
            if (sr.getCompilationUnits().stream()
                    .anyMatch(cu -> ParserUtils.compilationUnitHasAPrimaryTypeAnnoted(cu, CorrectedFilesForStudent.class))) {
                pathsToRemove = new HashSet<>();
            } else {
                // Collecte des chemins des unités de compilations marquées à supprimer (@FileToRemove)
                pathsToRemove = sr.getCompilationUnits().stream()
                        .filter(cu -> ParserUtils.compilationUnitHasAPrimaryTypeAnnoted(cu, FileToRemove.class))
                        .map(this::compilationUnitToPath)
                        .collect(Collectors.toSet());
            }
            // Si une classe est annotée CorrectedFilesForStudent, tout est écrit dans rf comme dans cf
            if (sr.getCompilationUnits().stream()
                    .anyMatch(cu -> ParserUtils.compilationUnitHasAPrimaryTypeAnnoted(cu, CorrectedFilesForStudent.class))) {
                sr.getCompilationUnits().stream()
                        .peek(this::addPackage)
                        .forEach(ParserUtils::annotationSuppression);
                sr.saveAll(rfTarget);
            } else { // Sinon, il faut enlever des choses

                // Pour chaque unité de compilation
                sr.getCompilationUnits().stream()
                        // enlever les imports
                        .peek(ParserUtils::importTagSuppression)
                        // traiter les @ToDo et @ToDoIn
                        .peek(ParserUtils::toDoSuppression)
                        // traiter les @ToDoInConstructor
                        .peek(ParserUtils::toDoInConstructorSuppression)
                        // supprimer les implémentations marquées
                        .peek(ParserUtils::implementationSuppression)
                        // supprimer les annotations
                        .forEach(ParserUtils::annotationSuppression);

                sr.saveAll(rfTarget);
            }
        } else {
            prs.stream()
                    .filter(pr -> !pr.isSuccessful())
                    .forEach(pr -> System.out.println("Issue with " + pr.getResult().toString()));
        }
        // Copie des resources
        if (resourcesSrc != null && resourcesSrc.toFile().exists()) {
            copyFiles(resourcesSrc, rfTarget);
            maxfiles += Files.walk(resourcesSrc).filter(p -> p.toFile().isFile()).count();
        }
        File rfDirectory = rfTarget.toFile();
        if (!rfDirectory.exists()) {
        	rfDirectory.mkdirs();
        }
    }

    private void putPriorityTestClassName(CompilationUnit compilationUnit) {
        int priority = Integer.MAX_VALUE;
        String testClassName = "";
        Optional<PackageDeclaration> compilationUnitPackageDeclaration = compilationUnit.getPackageDeclaration();
        String prefix;
        prefix = compilationUnitPackageDeclaration.map(packageDeclaration -> packageDeclaration.getNameAsString() + ".").orElse("");

        Optional<TypeDeclaration<?>> optionalTypeDeclaration = compilationUnit.getPrimaryType();
        if (optionalTypeDeclaration.isPresent()) {
            TypeDeclaration<?> td = optionalTypeDeclaration.get();
            Optional<AnnotationExpr> optionalAnnotationExpr = td.getAnnotationByName("ClassTestPriority");
            if (optionalAnnotationExpr.isPresent()) {
                AnnotationExpr annotationExpr = optionalAnnotationExpr.get();

                if (annotationExpr.isSingleMemberAnnotationExpr()) {
                    annotationExpr = annotationExpr.asSingleMemberAnnotationExpr();
                    priority = annotationExpr.asSingleMemberAnnotationExpr().getMemberValue().asIntegerLiteralExpr().asInt();
                }
                // td.remove(annotationExpr);
            }
            testClassName = optionalTypeDeclaration.get().getNameAsString();

        }

        if (!testClassName.isEmpty()) {
            priorityTestClassNameTreeSet.add(new PriorityTestClassName(priority, prefix + testClassName));
        }
    }

    private void changeGradesIfRelativeGradesIsAsked() throws IOException {

        /*
            Calcule le cumul des points figurant dans les @Grade de tous les fichiers
            de test, générés ou non.
         */
        SourceRoot sourceRoot = new SourceRoot(testTarget);
        sourceRoot.tryToParse();

        Pair<Double, Double> result = sourceRoot.getCompilationUnits().stream()
                .map(ParserUtils::getCounterAndMaxiGrade)
                .reduce(new Pair<>(0.0, 0.0),
                        (p1, p2) -> new Pair<>(p1.a + p2.a,
                                Double.max(p1.b, p2.b)));
        double total = result.a;
        double maxi = result.b;
        /*
            Change la valeur relative si demandée (c'est-à-dire maxi positif)
         */
        if (maxi > 0) {
            for (CompilationUnit cu : sourceRoot.getCompilationUnits()) {
                ParserUtils.gradeTransforme(cu, total, maxi);
            }
            grade = maxi;
        } else {
            grade = 100;
        }


        sourceRoot.saveAll();

    }

    public void publishTest() throws IOException, ClassNotFoundException {
        if (!test.toFile().exists() || !test.toFile().isDirectory()) {
            test.toFile().mkdir();
        }
        SourceRoot sourceRoot = new SourceRoot(test);
        List<ParseResult<CompilationUnit>> prs = sourceRoot.tryToParse();
        if (prs.stream().allMatch(ParseResult::isSuccessful)) {
            sourceRoot.getCompilationUnits().stream()
                    .peek(this::removeCLI)
                    .peek(this::putPriorityTestClassName)
                    .forEach(ParserUtils::toDoSuppression);
            sourceRoot.saveAll(testTarget);
            // Rajouté uniquement pour faire quelques tests
            sourceRoot.saveAll(testTargetRF);
        } else {
            prs.stream()
                    .filter(pr -> !pr.isSuccessful())
                    .forEach(pr -> System.out.println("Problème " + pr.getResult().toString()));
        }
        for (Class<?> c : tryToGetAllClasses()) {
            Checker chk = new Checker(c);
            String packageName = "";
            if (c.getPackage() != null) {
                packageName = c.getPackage().getName();
            }
            String className = "Generated" + c.getSimpleName() + "Test";
            CompilationUnitTest cut = new CompilationUnitTest(className, packageName, "cf", chk);
            Path path = new File(testTarget.toFile(), packageName.replaceAll("\\.", "/") + "/" + className + ".java").toPath();
            cut.setStorage(path);
            sourceRoot.add(cut);
            sourceRoot.saveAll(testTarget);
            // Recherche de l'éventuelle priorité de test de cette classe
            // et ajout dans le Set afin de générer plus tard le {@code vpl_evaluates.cases}
            // dans le bon ordre.
            ClassTestPriority ctp = c.getAnnotation(ClassTestPriority.class);
            String prefix = "";
            if (!packageName.isEmpty()) {
                prefix = packageName + ".";
            }
            if (ctp == null) {
                priorityTestClassNameTreeSet.add(new PriorityTestClassName(prefix + className));
            } else {
                priorityTestClassNameTreeSet.add(new PriorityTestClassName(ctp.value(), prefix + className));
            }
        }
        // Rajouté uniquement pour faire quelques tests
        for (Class<?> c : tryToGetAllClasses()) {
            Checker chk = new Checker(c);

            String packageName = "";
            if (c.getPackage() != null) {
                packageName = c.getPackage().getName();
            }
            String className = "Generated" + c.getSimpleName() + "Test";
            CompilationUnitTest cut = new CompilationUnitTest(className, packageName, "cf", chk);
            Path path = new File(testTargetRF.toFile(), packageName.replaceAll("\\.", "/") + "/" + className + ".java")
                    .toPath();
            cut.setStorage(path);
            sourceRoot.add(cut);
            sourceRoot.saveAll(testTargetRF);
        }
        // Copie des resources de test
        if (resourcesTest != null && resourcesTest.toFile().exists()) {
            copyFiles(resourcesTest, testTarget);
        }
        File testDirectory = testTarget.toFile();
        if (!testDirectory.exists()) {
        	testDirectory.mkdirs();
        }
    }

    private void removeCLI(CompilationUnit compilationUnit) {
        compilationUnit.getImports()
                .stream()
                .filter(id -> id.getName().asString().startsWith("caseine.format.javajunit.CLI"))
                .forEach(id -> {
                    String strimport = id.getName().asString().replaceAll("caseine.format.javajunit.CLI", "caseine.format.javajunit");
                    id.setName(strimport);
                });
    }

    private void changeImportForCfTest(CompilationUnit cu) {
        for (ImportDeclaration id : cu.getImports()) {
            String theImport = id.getNameAsString();
            if (!id.isAsterisk()) {
                theImport = theImport.replaceFirst(".[A-Z](.*)$", "");
            }
            for (String anImport : classes) {
                String starImport = anImport.replaceFirst(".[A-Z](.*)$", "");
                if (starImport.equals(theImport)) {
                    id.setName("\tcf." + id.getNameAsString());
                    break;
                }
            }
        }
    }

    public void publishCfTest() throws IOException {
        SourceRoot sr = new SourceRoot(src);
        List<ParseResult<CompilationUnit>> prs = sr.tryToParse();
        if (prs.stream().allMatch(ParseResult::isSuccessful)) {
            sr.getCompilationUnits().stream()
                    .peek(this::changeImportForCfTest)
                    .forEach(ParserUtils::changePackageForCfTest);
            sr.saveAll(testCfTarget);
        } else {
            prs.stream()
                    .filter(pr -> !pr.isSuccessful())
                    .forEach(pr -> System.out.println("Problème " + pr.getResult().toString()));
        }
        // Rajouté uniquement pour faire quelques tests (pour avoir {@code cf} dans testrf)
        {
            sr = new SourceRoot(src);
            prs.clear();
            prs = sr.tryToParse();
            if (prs.stream().allMatch(ParseResult::isSuccessful)) {
                sr.getCompilationUnits().stream()
                        //.peek(ParserUtils::annotationSuppression)
                        .forEach(ParserUtils::changePackageForCfTest);
                sr.saveAll(testCfTargetRF);
            } else {
                prs.stream()
                        .filter(pr -> !pr.isSuccessful())
                        .forEach(pr -> System.out.println("Problème " + pr.getResult().toString()));
            }
        }
    }

    public void publishAll() throws IOException, ClassNotFoundException {
        publishCf();
        publishRf();
        publishTest();
        publishMutations();
        publishCfTest();

        /* 
            Ramène les valeurs de grade de telle sorte que la somme soit égale à
            la valeur maximum qui est 20 par défaut.
         */
        changeGradesIfRelativeGradesIsAsked();
    }

    private void publishMutations() {
        try {
            SourceRoot sr = new SourceRoot(src);
            List<ParseResult<CompilationUnit>> prs = sr.tryToParse();
            HashMap<String, String> result = new Mutation(test.toString(), sr.getCompilationUnits()).generateDynamicTests();
            if (!result.keySet().isEmpty()) {
                String fileName = result.keySet().iterator().next();
                String fileShortName = fileName;
                int index = fileShortName.lastIndexOf(".");
                if (index != -1) {
                    fileShortName = fileName.substring(index + 1);
                }
                // Creates File TestTestX which is the test suite
                String modifiedName = "Test" + fileShortName + ".java";
                String filePath = testTarget.toString() + File.separator + modifiedName;
                BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(
                        new FileOutputStream(filePath), StandardCharsets.UTF_8));
                writer.write(result.get(fileName));
                writer.close();

                priorityTestClassNameTreeSet.add(new PriorityTestClassName(modifiedName.replace(".java", "")));
            }
        } catch (ClassNotFoundException | IOException e) {
            e.printStackTrace();
        }
    }

	public List<Class<?>> tryToGetAllClasses() throws IOException, ClassNotFoundException {
		SourceRoot sr = new SourceRoot(src);
		List<ParseResult<CompilationUnit>> cus = sr.tryToParse();
        List<String> classNames = new ArrayList<>();
        List<String> sourceCodesInText = new ArrayList<>();
		for (ParseResult<CompilationUnit> pr : cus) {
			String paquetage = "";
			if (pr.isSuccessful() && pr.getResult().isPresent()) {
				CompilationUnit cu = pr.getResult().get();
				if (cu.getPackageDeclaration().isPresent()) {
					paquetage = cu.getPackageDeclaration().get().getNameAsString() + ".";
				}
				for (TypeDeclaration<?> t : cu.getTypes()) {
					classNames.add(paquetage + t.getNameAsString());
					sourceCodesInText.add(cu.toString()); 
				}
			}
		}
		if (!cus.isEmpty()) {
			try {
				CodeCompiler scompiler = new CodeCompiler(cl, classesTarget);
				return scompiler.compile(classNames, sourceCodesInText);
			} catch (Exception ex) {
				throw new ClassNotFoundException("Compilation failed");
			}			
		}
		return new ArrayList<>();
	}


    private void copyFiles(Path sourceParentFolder, Path destinationParentFolder) {
        try (Stream<Path> allFilesPathStream = Files.walk(sourceParentFolder)) {
            Consumer<? super Path> action = (Consumer<Path>) t -> {
                try {
                    String destinationPath = t.toString().replace(sourceParentFolder.toString(),
                            destinationParentFolder.toString());
                    for (String excludedFile : excludedFiles) {
                        if (t.endsWith(excludedFile)) {
                            return;
                        }
                    }
                    Files.copy(t, Paths.get(destinationPath));
                } catch (FileAlreadyExistsException e) {
                    // TODO do acc to business needs
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                }
            };
            allFilesPathStream.forEach(action);

        } catch (FileAlreadyExistsException e) {
            // file already exists and unable to copy
        } catch (IOException e) {
            // permission issue
            e.printStackTrace();
        }
    }
}
