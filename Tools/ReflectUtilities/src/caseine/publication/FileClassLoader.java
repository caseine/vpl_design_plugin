package caseine.publication;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Path;

/**
 * Class extends {@link ClassLoader}
 */
class FileClassLoader extends ClassLoader {
	
	private Path classesTarget;

    /**
     * Constructor.
     * Creates instance of {@link FileClassLoader} by given parent class loader
     * @param parent parent class loader
     * @param classesTarget the path for searching classes
     */
    FileClassLoader(final ClassLoader parent, Path classesTarget) {
        super(parent);
        this.classesTarget = classesTarget;
    }

    @Override
    protected Class<?> findClass(final String name)
            throws ClassNotFoundException {
        
    	String fileName = classesTarget.toString() + File.separatorChar + name.replace('.', File.separatorChar) + ".class";
    	File file = new File(fileName);
    	if (file.exists()) {
        	byte[] bytes;
			try {
				bytes = convertFileToBytes(file);
			} catch (IOException e) {
	            return super.findClass(name);   		
			}    		
            return defineClass(name, bytes, 0, bytes.length);
    	} else {
            return super.findClass(name);   		
    	}
    }
    
    /**
     * Converts a file to a byte array 
     * @param file
     * @return the array of bytes
     * @throws IOException
     */
    public static byte[] convertFileToBytes(File file) 
        throws IOException 
    { 
        FileInputStream fl = new FileInputStream(file);  
        byte[] arr = new byte[(int)file.length()]; 
 
        fl.read(arr);   
        fl.close();
        return arr; 
    }
}