package caseine.publication;

import java.io.File;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.tools.Diagnostic;
import javax.tools.DiagnosticCollector;
import javax.tools.JavaCompiler;
import javax.tools.JavaCompiler.CompilationTask;
import javax.tools.JavaFileObject;
import javax.tools.StandardJavaFileManager;
import javax.tools.StandardLocation;
import javax.tools.ToolProvider;

/**
 * Class for compile string with java code to byte code
 * in memory
 *
 * @since 1.8
 */
class CodeCompiler {

    /**
     * System java compiler
     */
    private static JavaCompiler javac = ToolProvider.getSystemJavaCompiler();
    private ClassLoader cl;
    private Path classesTarget;

    /**
     * Constructor.
     * Creates instance of {@link CodeCompiler} by given class loader
     * @param cl instance of {@link ClassLoader}
     * @param classesTarget 
     */
    CodeCompiler(final ClassLoader cl, Path classesTarget) {
        this.cl = cl;
        this.classesTarget = classesTarget;
    }

    /**
     * Compile {@link String} with custom class to java byte code and represent
     * compiled class
     * @param className full name of future class
     * @param sourceCodeInText code source
     * @return compiled class
     * @throws Exception if any errors occurred
     */
    List<Class<?>> compile(
            final List<String> classNames,
            final List<String> sourceCodesInText
    )
            throws Exception {
        try {
            List<String> optionList = new ArrayList<>();
            optionList.addAll(Arrays.asList("-classpath", getClassPath(cl)));
            List<SourceCode> compilationUnits = new ArrayList<>();
            for(int i=0; i < classNames.size(); i++) {
                SourceCode sourceCode = new SourceCode(classNames.get(i), sourceCodesInText.get(i));
                compilationUnits.add(sourceCode);
            }
            DiagnosticCollector<JavaFileObject> diagnostics = new DiagnosticCollector<>();
            StandardJavaFileManager fileManager = javac.getStandardFileManager(
                    null,
                    null,
                    null);

            File outputFile = new File(classesTarget.toUri());
            if (!outputFile.exists()) {
                outputFile.mkdirs();
            }
            fileManager.setLocation(StandardLocation.CLASS_OUTPUT, 
                                    Arrays.asList(outputFile));
            
            CompilationTask task = javac.getTask(
                    null,
                    fileManager,
                    diagnostics,
                    optionList,
                    null,
                    compilationUnits
            );
            if (!task.call()) {
                StringBuilder s = new StringBuilder();
                for (Diagnostic<?> diagnostic : diagnostics.getDiagnostics()) {
                    s
                            .append("\n")
                            .append(diagnostic);
                }
                throw new Exception("Failed to compile" +  s.toString());
            }
            List<Class<?>> result = new ArrayList<>();
            ClassLoader cl = new FileClassLoader(this.cl, classesTarget);
            for(String className: classNames) {
                result.add(cl.loadClass(className));
            }
            return result;
        } catch (Throwable e) {
            throw new Exception(e);
        }
    }
    
    /**
     * Return all class paths as instance of {@link String} form given instance of {@link ClassLoader}
     * @param classLoader instance of {@link ClassLoader}
     * @return all class paths
     */
    private static String getClassPath(final ClassLoader classLoader) {
        ClassLoader cl = classLoader;
        StringBuilder buf = new StringBuilder();
        buf.append(".");
        String separator = System.getProperty("path.separator");
        while (null != cl) {
            try {
                URLClassLoader ucl = (URLClassLoader) cl;
                URL[] urls = ucl.getURLs();
                for (URL url : urls) {
                    String jarPathName = Path.of(url.toURI()).toString();
                    jarPathName = jarPathName.replace("%20", " ");
                    buf.append(separator).append(
                            jarPathName
                    );
                }
            } catch (Exception e) {
                // do nothing
                // because this try-catch check cast ClassLoader to URLClassLoader
            }
            cl = cl.getParent();
        }

        return buf.toString();
    }
}