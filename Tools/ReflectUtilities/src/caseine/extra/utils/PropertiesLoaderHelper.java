package caseine.extra.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

/**
 * 
 * @author Christophe Saint-Marcel &lt;christophe.saint-marcel@univ-grenoble-alpes.fr&gt;
 *
 */
public class PropertiesLoaderHelper {

	public static File find(String path, String fName) {
		File f = new File(path);
		if (fName.equalsIgnoreCase(f.getName()))
			return f;
		if (f.isDirectory()) {
			for (String aChild : f.list()) {
				File ff = find(path + File.separator + aChild, fName);
				if (ff != null)
					return ff;
			}
		}
		return null;
	}

	public static Class<?> loadClass(String pattern, String classKey) throws ClassNotFoundException, IOException {
		Properties p = new Properties();
		Class<?> result = null;
		String className = null;
		File file = PropertiesLoaderHelper.find(".", pattern + ".properties");
		FileInputStream fis = new FileInputStream(file);
		p.load(fis);
		className = p.getProperty(classKey);
		if (className != null && !className.equals("")) {
			result = Class.forName(className);
		}

		return result;
	}

}
