package caseine.extra.utils.mutations;

public @interface Mutant {

	String testClass();

	String errorMessage();

}
