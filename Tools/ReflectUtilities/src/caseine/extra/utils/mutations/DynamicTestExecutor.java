package caseine.extra.utils.mutations;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.MissingResourceException;
import java.util.stream.Collectors;

import javax.tools.JavaCompiler;
import javax.tools.ToolProvider;

import org.junit.runner.JUnitCore;
import org.junit.runner.Result;

/**
 * This class is introduced to run dynamically classes of tests. It is
 * specifically used in the problems of tests by mutation (cf. Lab on mutants).
 * 
 * @author Christophe Saint-Marcel
 *         &lt;christophe.saint-marcel@univ-grenoble-alpes.fr&gt;
 *
 */
public abstract class DynamicTestExecutor {

	/**
	 * The java class name.
	 */
	String testClass;
	/**
	 * Package name of the mutant.
	 */
	String newPackageName;

	/**
	 * Construct the executor.
	 * 
	 * @param testClass      class of test
	 * @param newPackageName location of the mutant
	 * @throws FileNotFoundException
	 */
	public DynamicTestExecutor(String testClass, String newPackageName) throws FileNotFoundException {
		this.testClass = testClass;
		this.newPackageName = newPackageName;
	}

	/**
	 * Look into different paths (for maven or on the Caseine server).
	 * 
	 * @param pathname
	 * @return
	 * @throws FileNotFoundException
	 */
	private String findClass(String pathname) throws FileNotFoundException {
		pathname = pathname.replace(".", File.separator) + ".java";
		File f = new File(pathname);
		if (f.exists())
			return pathname;
		f = new File("src" + File.separator + pathname);
		if (f.exists())
			return "src" + File.separator + pathname;
		f = new File("test" + File.separator + pathname);
		if (f.exists())
			return "test" + File.separator + pathname;
		throw new FileNotFoundException();
	}

	/**
	 * Read sources.
	 * 
	 * @param classNames
	 * @return
	 * @throws FileNotFoundException
	 */
	protected String[] readCodes(String... classNames) throws FileNotFoundException {
		String[] sources = new String[classNames.length];
		for (int i = 0; i < classNames.length; i++) {
			sources[i] = findClass(classNames[i]);
			InputStream stream = new FileInputStream(sources[i]);
			String separator = System.getProperty("line.separator");
			try (BufferedReader reader = new BufferedReader(new InputStreamReader(stream))) {
				sources[i] = reader.lines().collect(Collectors.joining(separator));
			} catch (IOException e) {
				e.printStackTrace();
			}
			;
		}
		return sources;
	}

	/**
	 * Save the source files.
	 * 
	 * @param sources    codes
	 * @param classNames
	 * @return
	 * @throws IOException
	 */
	protected Path[] saveSources(String[] sources, String[] classNames) throws IOException {
		// String tmpProperty = System.getProperty("java.io.tmpdir");
		String tmpProperty = "." + File.separator + "tmp";
		if (this.newPackageName != null) {
			tmpProperty = tmpProperty + File.separator + this.newPackageName;
		}
		File tmp_dir = new File(tmpProperty);
		if (!tmp_dir.exists()) {
			tmp_dir.mkdirs();
		}
		Path[] sourcePaths = new Path[sources.length];
		for (int i = 0; i < sources.length; i++) {
			String testName = classNames[i];
			int index = classNames[i].lastIndexOf(".");
			if (index != -1) {
				testName = testName.substring(index + 1);
			}
			Path sourcePath = Paths.get(tmpProperty, testName + ".java");
			Files.createDirectories(sourcePath.getParent());
			Files.write(sourcePath, sources[i].getBytes(StandardCharsets.UTF_8));
			sourcePaths[i] = sourcePath;
		}
		return sourcePaths;
	}

	/**
	 * Compile sources.
	 * 
	 * @param javaFiles
	 * @return
	 */
	protected Path[] compileSources(Path[] javaFiles) {
		JavaCompiler compiler = ToolProvider.getSystemJavaCompiler();
		if (compiler == null) {
			throw new MissingResourceException("No compiler found in your classpath, configure a JDK instead of a JRE!",
					null, null);
		}
		String[] absPaths = new String[javaFiles.length];
		for (int i = 0; i < javaFiles.length; i++) {
			absPaths[i] = javaFiles[i].toFile().getAbsolutePath();
		}
		compiler.run(null, null, null, absPaths);
		Path[] paths = new Path[javaFiles.length];
		for (int i = 0; i < javaFiles.length; i++) {
			paths[i] = Paths.get(absPaths[i].replace(".java", ".class"));
		}
		return paths;
	}

	/**
	 * For debug, display the result.
	 * 
	 * @param result
	 */
	@SuppressWarnings("unused")
	private static void resultReport(Result result) {
		System.out.println(
				"Finished. Result: Failures: " + result.getFailureCount() + ". Ignored: " + result.getIgnoreCount()
						+ ". Tests run: " + result.getRunCount() + ". Time: " + result.getRunTime() + "ms.");
	}

	/**
	 * Play now a single test.
	 * 
	 * @return number of failures
	 * @throws Exception
	 */
	public abstract boolean doTest() throws Exception;

	/**
	 * Change the package to reference the mutant.
	 * 
	 * @param testName name of the class with its packages
	 * @param src      the source to modify
	 * @return the modified source
	 */
	protected String replacePackaging(String testName, String src) {
		String newPackage = "package " + this.newPackageName + ";\n";
		if (this.newPackageName != null) {
			int index = testName.lastIndexOf(".");
			if (index != -1) {
				index = src.indexOf(";");
				src = src.substring(index + 1);
			}
			return newPackage + src;
		} else
			return src;
	}

	/**
	 * Remove tmp directory.
	 */
	protected void cleanClassPath() {
		File f = new File("./tmp");
		if (f.exists()) {
			deleteDirectory(f);
		}
	}

	/**
	 * Delete a non empty directory.
	 * 
	 * @param directoryToBeDeleted the file to delete
	 * @return true if deleted
	 */
	private boolean deleteDirectory(File directoryToBeDeleted) {
		File[] allContents = directoryToBeDeleted.listFiles();
		if (allContents != null) {
			for (File file : allContents) {
				deleteDirectory(file);
			}
		}
		return directoryToBeDeleted.delete();
	}

}