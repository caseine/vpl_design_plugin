package caseine.extra.utils.mutations;
import static org.junit.platform.engine.discovery.ClassNameFilter.excludeClassNamePatterns;
import static org.junit.platform.engine.discovery.DiscoverySelectors.selectPackage;

import java.io.PrintWriter;
import java.util.logging.LogManager;

import org.junit.platform.launcher.Launcher;
import org.junit.platform.launcher.LauncherDiscoveryRequest;
import org.junit.platform.launcher.core.LauncherDiscoveryRequestBuilder;
import org.junit.platform.launcher.core.LauncherFactory;
import org.junit.platform.launcher.listeners.SummaryGeneratingListener;
import org.junit.platform.launcher.listeners.TestExecutionSummary;

public class JUnit5Runner {

	SummaryGeneratingListener listener = new SummaryGeneratingListener();

	public void runAll() {
		LauncherDiscoveryRequest request = LauncherDiscoveryRequestBuilder.request().selectors(selectPackage("")).filters(excludeClassNamePatterns(".*Test.*Test.*")).build();
		Launcher launcher = LauncherFactory.create();

		launcher.registerTestExecutionListeners(listener);

		launcher.execute(request);
	}

	public static void main(String[] args) {
		
		LogManager.getLogManager().reset();	

		JUnit5Runner runner = new JUnit5Runner();
		runner.runAll();

		TestExecutionSummary summary = runner.listener.getSummary();
		// summary.printTo(new PrintWriter(System.out));
		if (summary.getFailures().isEmpty()) {
			System.out.println("JUnit5 tests OK");
		} else {
			summary.printFailuresTo(new PrintWriter(System.out));
		}
	}
}
