package caseine.extra.utils.mutations;

import java.io.File;
import java.io.FileNotFoundException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.file.Path;
import java.util.List;

import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;
import org.junit.runners.model.InvalidTestClassError;

/**
 * This class is introduced to run dynamically classes of tests. It is
 * specifically used in the problems of tests by mutation (cf. Lab on mutants).
 * 
 * @author Christophe Saint-Marcel
 *         &lt;christophe.saint-marcel@univ-grenoble-alpes.fr&gt;
 *
 */
public class DynamicTestExecutorImpl4 extends DynamicTestExecutor {


	/**
	 * Construct the executor.
	 * 
	 * @param testClass      class of test
	 * @param newPackageName location of the mutant
	 * @throws FileNotFoundException
	 */
	public DynamicTestExecutorImpl4(String testClass, String newPackageName) throws FileNotFoundException {
		super(testClass, newPackageName);
	}

	/**
	 * Play the test.
	 * 
	 * @param testName
	 * @return
	 * @throws MalformedURLException
	 * @throws ClassNotFoundException
	 * @throws IllegalAccessException
	 * @throws InstantiationException
	 * @throws InvalidTestClassException 
	 */
	private Result testClass(String testName) throws MalformedURLException, ClassNotFoundException,
			IllegalAccessException, InstantiationException, InvalidTestClassException {
		URL[] classURLs = new URL[1];
		classURLs[0] = new File("./tmp").toURI().toURL();
		URLClassLoader classLoader = URLClassLoader.newInstance(classURLs);
		if (this.newPackageName != null) {
			int index = testName.lastIndexOf(".");
			if (index != -1) {
				testName = testName.substring(index + 1);
			}
			testName = this.newPackageName + "." + testName;
		}
		Class<?> clazz = Class.forName(testName, true, classLoader);
		JUnitCore junit = new JUnitCore();
		// junit.addListener(new TextListener(System.out));
		Result result = junit.run(clazz);
		
		List<Failure> failures = result.getFailures();

		for (Failure f : failures) {
			if (f.getException() instanceof InvalidTestClassError) {
				throw new InvalidTestClassException(testName);
			}
		}

		// resultReport(result);
		return result;
	}

	/**
	 * Play now a single test.
	 * 
	 * @return number of failures
	 * @throws Exception
	 */
	public boolean doTest() throws Exception {
		String[] classNames = new String[1];
		classNames[0] = this.testClass;
		String[] sources = null;
		try {
			sources = readCodes(classNames);
		} catch (FileNotFoundException fnfe) {
			throw new ClassNotFoundException(classNames[0]);
		}
		sources[sources.length - 1] = replacePackaging(classNames[classNames.length - 1], sources[sources.length - 1]);
		Path[] javaFiles = saveSources(sources, classNames);
		compileSources(javaFiles);
		Result result = testClass(classNames[classNames.length - 1]);
		// resultReport(result);
		cleanClassPath();
		return result.getFailureCount() > 0;
	}
}