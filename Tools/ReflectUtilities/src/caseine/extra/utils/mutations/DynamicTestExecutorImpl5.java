package caseine.extra.utils.mutations;

import static org.junit.platform.engine.discovery.DiscoverySelectors.selectClass;

import java.io.File;
import java.io.FileNotFoundException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.file.Path;
import java.util.List;

import org.junit.platform.launcher.Launcher;
import org.junit.platform.launcher.LauncherDiscoveryRequest;
import org.junit.platform.launcher.core.LauncherDiscoveryRequestBuilder;
import org.junit.platform.launcher.core.LauncherFactory;
import org.junit.platform.launcher.listeners.SummaryGeneratingListener;
import org.junit.platform.launcher.listeners.TestExecutionSummary;
import org.junit.platform.launcher.listeners.TestExecutionSummary.Failure;
import org.junit.runners.model.InvalidTestClassError;

/**
 * This class is introduced to run dynamically classes of tests. It is
 * specifically used in the problems of tests by mutation (cf. Lab on mutants).
 * 
 * @author Christophe Saint-Marcel
 *         &lt;christophe.saint-marcel@univ-grenoble-alpes.fr&gt;
 *
 */
public class DynamicTestExecutorImpl5 extends DynamicTestExecutor {

	/**
	 * Construct the executor.
	 * 
	 * @param testClass      class of test
	 * @param newPackageName location of the mutant
	 * @throws FileNotFoundException
	 */
	public DynamicTestExecutorImpl5(String testClass, String newPackageName) throws FileNotFoundException {
		super(testClass, newPackageName);
	}

	/**
	 * Play the test for JUnit5.
	 * 
	 * @param testName
	 * @return
	 * @return
	 * @throws MalformedURLException
	 * @throws ClassNotFoundException
	 * @throws IllegalAccessException
	 * @throws InstantiationException
	 * @throws InvalidTestClassError
	 */
	private TestExecutionSummary testClass(String testName) throws MalformedURLException, ClassNotFoundException,
			IllegalAccessException, InstantiationException {
		URL[] classURLs = new URL[1];
		classURLs[0] = new File("./tmp").toURI().toURL();
		URLClassLoader classLoader = URLClassLoader.newInstance(classURLs);
		if (this.newPackageName != null) {
			int index = testName.lastIndexOf(".");
			if (index != -1) {
				testName = testName.substring(index + 1);
			}
			testName = this.newPackageName + "." + testName;
		}
		Class<?> clazz = Class.forName(testName, true, classLoader);
		SummaryGeneratingListener listener = new SummaryGeneratingListener();

		LauncherDiscoveryRequest request = LauncherDiscoveryRequestBuilder.request().selectors(selectClass(clazz))
				.build();
		Launcher launcher = LauncherFactory.create();
		launcher.discover(request);
		launcher.registerTestExecutionListeners(listener);
		launcher.execute(request);

		return listener.getSummary();
	}

	/**
	 * Play now a single test.
	 * 
	 * @return number of failures
	 * @throws Exception
	 */
	public boolean doTest() throws Exception {
		String[] classNames = new String[1];
		classNames[0] = this.testClass;
		String[] sources = null;
		try {
			sources = readCodes(classNames);
		} catch (FileNotFoundException fnfe) {
			throw new ClassNotFoundException(classNames[0]);
		}
		sources[sources.length - 1] = replacePackaging(classNames[classNames.length - 1], sources[sources.length - 1]);
		Path[] javaFiles = saveSources(sources, classNames);
		compileSources(javaFiles);
		TestExecutionSummary result = testClass(classNames[classNames.length - 1]);
		List<Failure> failures = result.getFailures();

		for (Failure f : failures) {
			if (f.getException() instanceof InvalidTestClassError) {
				throw new InvalidTestClassException(classNames[0]);
			}
		}
		// resultReport(result);
		cleanClassPath();
		return result.getFailures().size() > 0;
	}
}