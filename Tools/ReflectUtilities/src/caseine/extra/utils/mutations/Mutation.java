package caseine.extra.utils.mutations;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.expr.MemberValuePair;
import com.github.javaparser.ast.expr.NormalAnnotationExpr;
import com.github.javaparser.utils.SourceRoot;

import caseine.publication.ParserUtils;

public class Mutation {

	private Path testPath;
	private List<CompilationUnit> compilationUnits;

	public Mutation(String path, List<CompilationUnit> compilationUnits) {
		testPath = new File(path).toPath();
		this.compilationUnits = compilationUnits;
	}

	public HashMap<String, String> generateDynamicTests() throws ClassNotFoundException, IOException {
		HashMap<String, String> result = new HashMap<String, String>();
		String mutations[][] = getMutations();		
		if(mutations.length > 0) {
			String classShortName = mutations[0][0];
			int index = classShortName.lastIndexOf(".");
			if (index != -1) {
				classShortName = mutations[0][0].substring(index + 1);
			}
			CompilationUnit compilationUnit = null;
			boolean junit5 = false;
			String template = "TestTemplateJUnit4.tmp";
			if (compilationUnits != null) {
				for (CompilationUnit cu: compilationUnits) {
					if (cu.getPrimaryType().get().getNameAsString().equals(classShortName)) {
						compilationUnit = cu;
						break;
					}
				}				
				if (ParserUtils.compilationUnitIsATest(compilationUnit, ParserUtils.CUType.JUNIT5)) {
					junit5 = true;
					template = "TestTemplateJUnit5.tmp";
				}
			}
			InputStream is = getClass().getResourceAsStream(template);				
			int bufferSize = 1024;
			char[] buffer = new char[bufferSize];
			StringBuilder out = new StringBuilder();
			Reader in = new InputStreamReader(is, StandardCharsets.UTF_8);
			try {
				for (int numRead; (numRead = in.read(buffer, 0, buffer.length)) > 0;) {
					out.append(buffer, 0, numRead);
				}

				String resultingSource = out.toString().replace("/*CLASS*/", classShortName);
				resultingSource = resultingSource.replace("/*TEST_CLASS*/", mutations[0][0]);
				StringBuilder sb = new StringBuilder();
				for (int i = 0; i < mutations.length; i++) {
					sb.append("        @Test\n");
					sb.append("        public void kill_" + mutations[i][1] + "() throws Exception {\n");
					if (junit5) {
						sb.append("            DynamicTestExecutor dte = new DynamicTestExecutorImpl5(\"" + mutations[0][0] + "\", null);\n");
						sb.append("            assertFalse(dte.doTest(), \"The code MUST be implemented\");\n");						
						sb.append("            dte = new DynamicTestExecutorImpl5(\n");
					} else {
						sb.append("            DynamicTestExecutor dte = new DynamicTestExecutorImpl4(\"" + mutations[0][0] + "\", null);\n");
						sb.append("            assertFalse(\"The code MUST be implemented\", dte.doTest());\n");
						sb.append("            dte = new DynamicTestExecutorImpl4(\n");
					}
					sb.append("                \"" + mutations[i][0] + "\",\n");
					sb.append("                \"" + mutations[i][1] + "\"\n");
					sb.append("            );\n");
					if (junit5) {
						sb.append("            assertTrue(dte.doTest(), ERROR_NOT_DETECTED + \" " + mutations[i][2] + "\");\n");
					} else {
						sb.append("            assertTrue(ERROR_NOT_DETECTED + \" " + mutations[i][2] + "\", dte.doTest());\n");
					}
					sb.append("        }\n");
					sb.append("\n");
				}
				resultingSource = resultingSource.replace("/*DYNAMIC_TESTS*/", sb.toString());
				result.put(mutations[0][0], resultingSource);
			} catch (IOException e) {
				return null;
			}
		}
		return result;
	}

	public String[][] getMutations() throws IOException, ClassNotFoundException {
		String result[][] = {};
		SourceRoot srtest = new SourceRoot(testPath);
		srtest.tryToParse();
		List<CompilationUnit> cus = srtest.getCompilationUnits().stream()
				.filter(cu -> ParserUtils.compilationUnitHasAPrimaryTypeAnnoted(cu, Mutant.class))
				.sorted(Comparator.comparing(cu -> cu.getPackageDeclaration().get().getNameAsString()))
				.collect(Collectors.toList());
		result = new String[cus.size()][3];
		int numberOfCUs = 0;
		for (CompilationUnit cu : cus) {
			String packageName = cu.getPackageDeclaration().get().getNameAsString();
			result[numberOfCUs][1] = packageName;
			NormalAnnotationExpr annotation = (NormalAnnotationExpr) cu.getPrimaryType().get()
					.getAnnotationByClass(Mutant.class).get();
			for (MemberValuePair p : annotation.getPairs()) {
				if (p.getNameAsString().equals("testClass")) {
					result[numberOfCUs][0] = p.getValue().toString().replace("\"", "");
				}
				if (p.getNameAsString().equals("errorMessage")) {
					result[numberOfCUs][2] = p.getValue().toString().replace("\"", "");
				}
			}
			numberOfCUs++;
		}
		return result;
	}

}
