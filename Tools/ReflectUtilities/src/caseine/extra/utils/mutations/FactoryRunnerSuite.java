package caseine.extra.utils.mutations;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.ArrayList;

import org.junit.runner.RunWith;

/**
 * Useful for JUNIT5. A way to test locally and dynamically all the mutations. 
 * @author christophe
 *
 */
@RunWith(FactoryRunner.class)
public class FactoryRunnerSuite implements FactoryRunner.Producer  {

    private static final String ERROR_NOT_DETECTED = "Error not detected: ";

	static String parameters[][];
	
	static {
		try {
			parameters = new Mutation("test",  new ArrayList<>()).getMutations();
		} catch (IOException | ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

    @Override
    public void produceTests(FactoryRunner.TestConsumer tc) throws Throwable {
        for (int i=0; i<parameters.length; i++) {
            final int number = i;
            final String name = parameters[i][1];

            tc.accept(name, () -> {
            	DynamicTestExecutor dte = new DynamicTestExecutorImpl5(parameters[number][0], parameters[number][1]);
            	assertTrue(ERROR_NOT_DETECTED + parameters[number][2], dte.doTest());

            });
        }
    }
}