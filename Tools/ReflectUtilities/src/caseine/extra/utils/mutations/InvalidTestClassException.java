package caseine.extra.utils.mutations;

public class InvalidTestClassException extends Exception {

	public InvalidTestClassException() {
		// TODO Auto-generated constructor stub
	}

	public InvalidTestClassException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public InvalidTestClassException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public InvalidTestClassException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public InvalidTestClassException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

}
