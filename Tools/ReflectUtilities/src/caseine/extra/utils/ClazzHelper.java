package caseine.extra.utils;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import org.junit.extensions.cpsuite.ClassTester;
import org.junit.extensions.cpsuite.ClasspathClassesFinder;

public class ClazzHelper {

	private static final String EDU_UGA_MIAGE_M1_PATTERN = "edu.uga.miage.m1.pattern";

	public static HashSet<Class<?>> loadImplementations(Class<?> clazz) {
		HashSet<Class<?>> result = new HashSet<>();
		List<String> packages = findAllPackagesStartingWith("edu.uga.miage.m1");
		for (String packageName : packages) {
			if (!packageName.equals(EDU_UGA_MIAGE_M1_PATTERN)) {
				List<Class<?>> classes = getClasses(clazz, packageName, true);
				result.addAll(classes);
			}
		}
		result.remove(clazz); // Remove the interface or the parent class itself
		return result;
	}

	private static List<Class<?>> getClasses(final Class<?> aClass, final String packageName,
			final boolean includeChildPackages) {
		return new ClasspathClassesFinder(new ClassTester() {
			@Override
			public boolean searchInJars() {
				return true;
			}

			@Override
			public boolean acceptInnerClass() {
				return false;
			}

			@Override
			public boolean acceptClassName(String name) {
				return name.startsWith(packageName)
						&& (includeChildPackages || name.indexOf(".", packageName.length()) != -1);
			}

			@Override
			public boolean acceptClass(Class<?> c) {
				return aClass.isAssignableFrom(c);
			}
		}, System.getProperty("java.class.path")).find();
	}

	/**
	 * Finds all package names starting with prefix
	 * @param prefix the prefix of wanted packages
	 * @return Set of package names
	 */
	public static List<String> findAllPackagesStartingWith(String prefix) {
		List<String> result = new ArrayList<>();
		for (Package p : Package.getPackages()) {
			if (p.getName().startsWith(prefix)) {
				result.add(p.getName());
			}
		}
		return result;
	}

	@SuppressWarnings("rawtypes")
	public static Object get_existing_class_instance(Class<?> clazz, Object... initArgs)
			throws InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException {
		Class parameterTypes[] = new Class[initArgs.length];
		for (int i = 0; i < initArgs.length; i++) {
			parameterTypes[i] = initArgs[i].getClass();
		}
		Constructor constructor = clazz.getConstructor(parameterTypes);
		return constructor.newInstance(initArgs);
	}

}
