/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package caseine.checker;

import caseine.publication.ParserUtils;
import caseine.tags.ImplementationToRemove;
import com.github.javaparser.ast.body.TypeDeclaration;
import com.github.javaparser.ast.expr.AnnotationExpr;
import com.github.javaparser.ast.expr.Expression;
import com.github.javaparser.ast.type.ClassOrInterfaceType;
import com.github.javaparser.utils.SourceRoot;
import java.io.File;
import java.io.IOException;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.Test;
import static org.junit.Assert.*;

public class CheckerTest {

    public CheckerTest() {
    }

    @Test
    public void testSomeMethod() {
    }
    private static SourceRoot SR;
    private static Optional<?> optTd = null;

    static {
        try {
            SR = new SourceRoot(new File("caseine/tests").toPath());
        } catch (Exception ex) {
            SR = new SourceRoot(new File("src/caseine/tests").toPath());
        }
    }

    private static TypeDeclaration<?> getCompilationUnit(String fullyQualifiedName) throws IOException {
        Optional<Optional<TypeDeclaration<?>>> optopttd = SR.tryToParse().stream()
                .filter(pr -> pr.isSuccessful())
                .map(pr -> pr.getResult().get())
                .filter(cu -> cu.getPrimaryType().isPresent())
                .map(cu -> cu.getPrimaryType())
                .filter(pt -> fullyQualifiedName.equals(pt.get().getFullyQualifiedName().get()))
                .findAny();
        if (optopttd.isPresent() && optopttd.get().isPresent()) {
            return optopttd.get().get();
        } else {
            return null;
        }
    }

    private static TypeDeclaration<?> getTypeDeclaration(String fullyQualifiedName) throws IOException {
        Optional<Optional<TypeDeclaration<?>>> optopttd = SR.tryToParse().stream()
                .filter(pr -> pr.isSuccessful())
                .map(pr -> pr.getResult().get())
                .filter(cu -> cu.getPrimaryType().isPresent())
                .map(cu -> cu.getPrimaryType())
                .filter(pt -> fullyQualifiedName.equals(pt.get().getFullyQualifiedName().get()))
                .findAny();
        if (optopttd.isPresent() && optopttd.get().isPresent()) {
            return optopttd.get().get();
        } else {
            return null;
        }
    }

    @Test
    public void testImplementationToRemove() {
        System.out.println("Test ImplementationToRemove");

        try {
            TypeDeclaration td = getTypeDeclaration("caseine.tests.K");
            if (td.isClassOrInterfaceDeclaration()) {
                ParserUtils.implementationSuppression(td.asClassOrInterfaceDeclaration());

                assertEquals("@ImplementationToRemove(\"I1\") public class K { }", td.toString().replaceAll("[\\s]+", " "));

            }
            td = getTypeDeclaration("caseine.tests.L");
            if (td.isClassOrInterfaceDeclaration()) {
                ParserUtils.implementationSuppression(td.asClassOrInterfaceDeclaration());

                assertEquals("@ImplementationToRemove(\"I1\") public class L implements I2 { }", td.toString().replaceAll("[\\s]+", " "));

            }
            td = getTypeDeclaration("caseine.tests.M");
            if (td.isClassOrInterfaceDeclaration()) {
                ParserUtils.implementationSuppression(td.asClassOrInterfaceDeclaration());

                assertEquals("@ImplementationToRemove({ \"I1\", \"I2\" }) public class M { }", td.toString().replaceAll("[\\s]+", " "));

            }
            td = getTypeDeclaration("caseine.tests.IsansI1");
            if (td.isClassOrInterfaceDeclaration()) {
                ParserUtils.implementationSuppression(td.asClassOrInterfaceDeclaration());

                assertEquals("@ImplementationToRemove(\"I1\") public interface IsansI1 extends I2 { }", td.toString().replaceAll("[\\s]+", " "));

            }
            td = getTypeDeclaration("caseine.tests.IsansI2");
            if (td.isClassOrInterfaceDeclaration()) {
                ParserUtils.implementationSuppression(td.asClassOrInterfaceDeclaration());

                assertEquals("@ImplementationToRemove(\"I2\") public interface IsansI2 extends I1 { }", td.toString().replaceAll("[\\s]+", " "));

            }
            td = getTypeDeclaration("caseine.tests.IsansI1I2");
            if (td.isClassOrInterfaceDeclaration()) {
                ParserUtils.implementationSuppression(td.asClassOrInterfaceDeclaration());

                assertEquals("@ImplementationToRemove({ \"I1\", \"I2\" }) public interface IsansI1I2 { }", td.toString().replaceAll("[\\s]+", " "));

            }
        } catch (IOException ex) {
            Logger.getLogger(CheckerTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    //@Test
    public void test() {
        try {
            TypeDeclaration<?> td = getTypeDeclaration("caseine.tests.K").asClassOrInterfaceDeclaration();
            System.out.println(td.asClassOrInterfaceDeclaration());
            Optional<AnnotationExpr> optA = td.getAnnotationByClass(ImplementationToRemove.class);

            if (optA.isPresent()) {
                String implementation;
                AnnotationExpr a = optA.get();
                if (a.isSingleMemberAnnotationExpr()) {
                    Expression value = a.asSingleMemberAnnotationExpr().getMemberValue();
                    implementation = value.asStringLiteralExpr().asString();
                } else /* if (a.isNormalAnnotationExpr()) */ {
                    Expression value = a.asNormalAnnotationExpr().getPairs().get(0).getValue();
                    implementation = value.toString();
                }
                if (td.isClassOrInterfaceDeclaration()) {
                    Optional<ClassOrInterfaceType> optci;
                    optci = td.asClassOrInterfaceDeclaration().getImplementedTypes()
                            .stream()
                            .filter(cid -> cid.isClassOrInterfaceType())
                            .map(cid -> cid.asClassOrInterfaceType())
                            .filter(cid -> implementation.equals(cid.getNameAsString()))
                            .findFirst();
                    if (optci.isPresent()) {
                        System.out.println(optci.get().remove());
                    }
                }

            }

            td.getAnnotationByClass(ImplementationToRemove.class).ifPresent(a -> {
                System.out.println("---> " + a);
                System.out.println("getClass ---> " + a.getClass());

            });

            assertEquals("/** * @author yvan */@ImplementationToRemove(\"I1\")public class K {}", td.toString().replaceAll("[\\n]+", ""));

        } catch (IOException ex) {
            System.err.println(ex);
        }
    }
}
