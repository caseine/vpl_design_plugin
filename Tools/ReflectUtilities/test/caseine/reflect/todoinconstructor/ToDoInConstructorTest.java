/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package caseine.reflect.todoinconstructor;

import caseine.publication.ParserUtils;
import caseine.tests.equals_checker.*;
import com.github.javaparser.StaticJavaParser;
import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.body.ClassOrInterfaceDeclaration;
import com.github.javaparser.ast.body.TypeDeclaration;
import com.github.javaparser.utils.SourceRoot;
import java.io.File;
import java.io.IOException;
import java.util.Optional;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author yvan
 */
public class ToDoInConstructorTest {

    private static SourceRoot SR;

    static {
        try {
            SR = new SourceRoot(new File("caseine/tests/todoinconstructor").toPath());
        } catch (Exception ex) {
            SR = new SourceRoot(new File("src/caseine/tests/todoinconstructor").toPath());
        }
    }

    private static CompilationUnit getCompilationUnit(String fullyQualifiedName) throws IOException {
        Optional<CompilationUnit> optcu = SR.tryToParse().stream()
                .filter(pr -> pr.isSuccessful())
                .map(pr -> pr.getResult().get())
                .filter(cu -> cu.getPrimaryType().isPresent())
                .filter(cu -> cu.getPrimaryType().get().getFullyQualifiedName().get().equals(fullyQualifiedName))
                .findAny();
        if (optcu.isPresent()) {
            return optcu.get();
        } else {
            return null;
        }
    }

    private static TypeDeclaration<?> getTypeDeclaration(String fullyQualifiedName) throws IOException {
        Optional<Optional<TypeDeclaration<?>>> optopttd = SR.tryToParse().stream()
                .filter(pr -> pr.isSuccessful())
                .map(pr -> pr.getResult().get())
                .filter(cu -> cu.getPrimaryType().isPresent())
                .map(cu -> cu.getPrimaryType())
                .filter(pt -> fullyQualifiedName.equals(pt.get().getFullyQualifiedName().get()))
                .findAny();
        if (optopttd.isPresent() && optopttd.get().isPresent()) {
            return optopttd.get().get();
        } else {
            return null;
        }
    }

    @Test
    public void testA() {
        try {
            CompilationUnit cutt = getCompilationUnit("caseine.tests.todoinconstructor.A");
            if (cutt == null) {
                fail("caseine.tests.todoinconstructor.A absent");
            } else {
                ParserUtils.toDoInConstructorSuppression(cutt);
                Optional<ClassOrInterfaceDeclaration> classA = cutt.getClassByName("A");
                
                CompilationUnit compilationUnit = StaticJavaParser.parse(Thread.currentThread().getContextClassLoader().getResourceAsStream("reflect/todoinconstructor/A.ref"));
                Optional<ClassOrInterfaceDeclaration> refClassA = compilationUnit.getClassByName("A");
                if (classA.isPresent() && refClassA.isPresent()) {
                    assertEquals(classA.get().toString(), refClassA.get().toString());
                } else {
                    fail("caseine.tests.todoinconstructor.A absent");
                }
            }
        } catch (IOException ex) {
            fail("caseine.tests.todoinconstructor.A absent");
        }

    }

}
