/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package caseine.reflect;

import caseine.tests.method_finder.ClassWithSomeMethods;
import com.github.javaparser.StaticJavaParser;
import com.github.javaparser.ast.body.MethodDeclaration;
import org.junit.Test;

import java.awt.*;
import java.lang.reflect.Method;

import static org.junit.Assert.*;

public class ParserWithReflectUtilitiesTest {

    @Test
    public void primitiveTest() {
        System.out.println("Test (sort of) equals between Class<?> and parsed type");
        Class<?>[] classType = {
                boolean.class,
                char.class,
                int.class,
                byte.class,
                short.class,
                long.class,
                float.class,
                double.class,
                void.class,
        };
        String[] stringType = {
                "boolean",
                "char",
                "int",
                "byte",
                "short",
                "long",
                "float",
                "double",
                "void",
        };
        for (int i = 0; i < classType.length; ++i) {

            for (int j = 0; j < i; ++j) {
                assertFalse(String.format("%s devrait être différent de %s",
                        classType[i],
                        stringType[j]),
                        ParserWithReflectUtilities.equals(StaticJavaParser.parseType(stringType[i]), classType[j]));
            }
            assertTrue(String.format("%s devrait être égal à %s",
                    classType[i],
                    stringType[i]),
                    ParserWithReflectUtilities.equals(StaticJavaParser.parseType(stringType[i]), classType[i]));
            for (int j = i + 1; j < classType.length; ++j) {
                assertFalse(String.format("%s devrait être différent de %s",
                        classType[i],
                        stringType[j]),
                        ParserWithReflectUtilities.equals(StaticJavaParser.parseType(stringType[i]), classType[j]));
            }
        }
        Class<?> ct = int[].class;
        String st = "int[]";
        assertTrue(String.format("%s devrait être égal à %s",
                ct,
                st),
                ParserWithReflectUtilities.equals(StaticJavaParser.parseType(st), ct));
        ct = int[][].class;
        st = "int[][]";
        assertTrue(String.format("%s devrait être égal à %s",
                ct,
                st),
                ParserWithReflectUtilities.equals(StaticJavaParser.parseType(st), ct));
        ct = java.awt.Color.class;
        st = "java.awt.Color";
        assertTrue(String.format("%s devrait être égal à %s",
                ct,
                st),
                ParserWithReflectUtilities.equals(StaticJavaParser.parseType(st), ct));
        ct = java.lang.Class.class;
        st = "Class<?>";
        assertTrue(String.format("%s devrait être égal à %s",
                ct,
                st),
                ParserWithReflectUtilities.equals(StaticJavaParser.parseType(st), ct));
    }

    @Test
    public void methodFinderTest() {
        System.out.println("Test method finder");


        try {
            // private static void m(int i) { System.out.println(1); }
            Method ref = ClassWithSomeMethods.class.getDeclaredMethod("m", int.class);
            assertNotNull("ClassWithSomeMethods.m(int) exists", ref);
            MethodDeclaration md = ParserWithReflectUtilities.find(ref);
            assertEquals(
                    md.toString().replaceAll("[\\s]+", ""),
                    "private static void m(int i) {System.out.println(1);}".replaceAll("[\\s]+", "")
            );
            // private void m(double d) { System.out.println(2.0); }
            ref = ClassWithSomeMethods.class.getDeclaredMethod("m", double.class);
            assertNotNull("ClassWithSomeMethods.m(double) exists", ref);
            md = ParserWithReflectUtilities.find(ref);
            assertEquals(
                    md.toString().replaceAll("[\\s]+", ""),
                    "private void m(double d) { System.out.println(2.0); }".replaceAll("[\\s]+", "")
            );
            // private static void m(int i, double d) {System.out.println(3 + 4.0); }
            ref = ClassWithSomeMethods.class.getDeclaredMethod("m", int.class, double.class);
            assertNotNull("ClassWithSomeMethods.m(int, double) exists", ref);
            md = ParserWithReflectUtilities.find(ref);
            assertEquals(
                    md.toString().replaceAll("[\\s]+", ""),
                    "private static void m(int i, double d) {System.out.println(3 + 4.0); }".replaceAll("[\\s]+", "")
            );

            // private void m(int... i) { System.out.println("Int Varargs");}
            ref = ClassWithSomeMethods.class.getDeclaredMethod("m", int[].class);
            assertNotNull("ClassWithSomeMethods.m(int...) exists", ref);
            md = ParserWithReflectUtilities.find(ref);
            assertEquals(
                    md.toString().replaceAll("[\\s]+", ""),
                    "private void m(int[] i) { System.out.println(\"Int Varargs\");}".replaceAll("[\\s]+", "")
            );

            // private static void m(String s) {System.out.println("String");}
            ref = ClassWithSomeMethods.class.getDeclaredMethod("m", java.lang.String.class);
            assertNotNull("ClassWithSomeMethods.m(String) exists", ref);
            md = ParserWithReflectUtilities.find(ref);
            assertEquals(
                    md.toString().replaceAll("[\\s]+", ""),
                    "private static void m(String s) {System.out.println(\"String\");}".replaceAll("[\\s]+", "")
            );
            // private void m(Color s) { System.out.println("Color"); }
            ref = ClassWithSomeMethods.class.getDeclaredMethod("m", Color.class);
            assertNotNull("ClassWithSomeMethods.m(Color) exists", ref);
            md = ParserWithReflectUtilities.find(ref);
            assertEquals(
                    md.toString().replaceAll("[\\s]+", ""),
                    "private void m(Color s) { System.out.println(\"Color\"); }".replaceAll("[\\s]+", "")
            );
        } catch (NoSuchMethodException e) {
            fail("Erreur inattendue");
        }
    }


}
