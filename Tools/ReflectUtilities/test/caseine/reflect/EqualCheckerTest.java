/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package caseine.reflect;

import caseine.tests.equals_checker.*;
import com.github.javaparser.ast.body.MethodDeclaration;
import com.github.javaparser.ast.body.TypeDeclaration;
import com.github.javaparser.utils.SourceRoot;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Optional;
import org.junit.Test;
import static org.junit.Assert.*;

public class EqualCheckerTest {

    private static SourceRoot SR;
    private static Optional<?> optTd = null;

    static {
        try {
            SR = new SourceRoot(new File("caseine/tests/equals_checker").toPath());
        } catch (Exception ex) {
            SR = new SourceRoot(new File("src/caseine/tests/equals_checker").toPath());
        }
    }

    private static TypeDeclaration<?> getCompilationUnit(String fullyQualifiedName) throws IOException {
        Optional<Optional<TypeDeclaration<?>>> optopttd = SR.tryToParse().stream()
                .filter(pr -> pr.isSuccessful())
                .map(pr -> pr.getResult().get())
                .filter(cu -> cu.getPrimaryType().isPresent())
                .map(cu -> cu.getPrimaryType())
                .filter(pt -> fullyQualifiedName.equals(pt.get().getFullyQualifiedName().get()))
                .findAny();
        if (optopttd.isPresent() && optopttd.get().isPresent()) {
            return optopttd.get().get();
        } else {
            return null;
        }
    }

    private static TypeDeclaration<?> getTypeDeclaration(String fullyQualifiedName) throws IOException {
        Optional<Optional<TypeDeclaration<?>>> optopttd = SR.tryToParse().stream()
                .filter(pr -> pr.isSuccessful())
                .map(pr -> pr.getResult().get())
                .filter(cu -> cu.getPrimaryType().isPresent())
                .map(cu -> cu.getPrimaryType())
                .filter(pt -> fullyQualifiedName.equals(pt.get().getFullyQualifiedName().get()))
                .findAny();
        if (optopttd.isPresent() && optopttd.get().isPresent()) {
            return optopttd.get().get();
        } else {
            return null;
        }
    }

    public EqualCheckerTest() {
    }

    @Test
    public void testA() {
        EqualChecker ec = new EqualChecker(A.class);
        assertFalse(ec.isEqualsDeclared());
        assertFalse(ec.isHashcodeDeclared());
    }

    @Test
    public void testB() {
        EqualChecker ec = new EqualChecker(B.class);
        assertTrue(ec.isEqualsDeclared());
        assertFalse(ec.isHashcodeDeclared());
    }

    @Test
    public void testC() {
        EqualChecker ec = new EqualChecker(C.class);
        assertFalse(ec.isEqualsDeclared());
        assertTrue(ec.isHashcodeDeclared());
    }

    @Test
    public void testD() {
        EqualChecker ec = new EqualChecker(D.class);
        assertTrue(ec.isEqualsDeclared());
        assertTrue(ec.isHashcodeDeclared());
    }

    @Test
    public void testE() {
        EqualChecker ec = new EqualChecker(E.class);
        assertTrue(ec.isEqualsDeclared());
        assertTrue(ec.isHashcodeDeclared());
        assertTrue(ec.isEqualToItSelf());
        assertFalse(ec.isNotEqualToNull());
    }

    @Test
    public void testF() {
        try {
            TypeDeclaration td = getTypeDeclaration("caseine.tests.equals_checker.F");
            if (td != null) {
                List<MethodDeclaration> lm = td.getMethodsBySignature("equals", "Object");
                if (lm.size() == 1) {
                    EqualChecker ec = new EqualChecker(F.class, lm.get(0).getBody().get());
                    assertTrue(ec.isEqualsDeclared());
                    assertTrue(ec.isHashcodeDeclared());
                    assertTrue(ec.isEqualToItSelf());
                    assertTrue(ec.isNotEqualToNull());
                    assertTrue(ec.isNotUseInstanceOf());
                }
            } else {
                fail("Unable to access to caseine.tests.equals_checker.F");
            }

        } catch (IOException ex) {
            fail("Unable to access to caseine.tests.equals_checker.F");
        }
    }

    @Test
    public void testG() {
        try {
            TypeDeclaration td = getTypeDeclaration("caseine.tests.equals_checker.G");
            if (td != null) {
                List<MethodDeclaration> lm = td.getMethodsBySignature("equals", "Object");
                if (lm.size() == 1) {
                    EqualChecker<G> ec = new EqualChecker<>(G.class, lm.get(0).getBody().get());
                    assertTrue(ec.isEqualsDeclared());
                    assertTrue(ec.isHashcodeDeclared());
                    assertTrue(ec.isEqualToItSelf());
                    assertTrue(ec.isNotEqualToNull());
                    assertFalse(ec.isNotUseInstanceOf());
                }
            } else {
                fail("Unable to access to caseine.tests.equals_checker.G");
            }

        } catch (IOException ex) {
            fail("Unable to access to caseine.tests.equals_checker.G");
        }
    }
}
