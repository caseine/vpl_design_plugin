package caseine.extra.utils.mutations;

import static org.junit.Assert.*;

import java.io.IOException;
import java.util.ArrayList;

import org.junit.Test;

@Mutant(testClass="AJavaClass.java", errorMessage="Check other mutations")
public class MutationTest {

	@Test
	public void testGetMutations() throws ClassNotFoundException, IOException {
		Mutation m = new Mutation("test", new ArrayList<>());
		assertEquals("AJavaClass.java", m.getMutations()[0][0]);
		assertEquals("caseine.extra.utils.mutations", m.getMutations()[0][1]);
		assertEquals("Check other mutations", m.getMutations()[0][2]);
	}
}
