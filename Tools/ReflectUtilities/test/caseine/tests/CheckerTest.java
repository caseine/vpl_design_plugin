/*
 * Copyright 2019: Yvan Maillot
 * This software is part of the Caseine project.
 * This software was developped with the support of the following organizations:
 * Université de Haute-Alsace
 * Institut Polytechnique de Grenoble
 * 
 * 
 * This file is part of the VPL Design Tool.
 * The VPL Design Tool is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * The VPL Design Tool is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with the VPL Design Tool.  If not, see <https://www.gnu.org/licenses/>.
 */
package caseine.tests;

import com.github.javaparser.ast.body.TypeDeclaration;
import com.github.javaparser.utils.SourceRoot;
import java.io.File;
import java.io.IOException;
import java.util.Optional;

public class CheckerTest {

    private static SourceRoot SR;
    private static Optional<?> optTd = null;

    static {
        try {
            SR = new SourceRoot(new File("caseine/tests").toPath());
        } catch (Exception ex) {
            SR = new SourceRoot(new File("src/caseine/tests").toPath());
        }
    }

    private static TypeDeclaration<?> getTypeDeclaration(String fullyQualifiedName) throws IOException {
        Optional<Optional<TypeDeclaration<?>>> optopttd = SR.tryToParse().stream()
                .filter(pr -> pr.isSuccessful())
                .map(pr -> pr.getResult().get())
                .filter(cu -> cu.getPrimaryType().isPresent())
                .map(cu -> cu.getPrimaryType())
                .filter(pt -> fullyQualifiedName.equals(pt.get().getFullyQualifiedName().get()))
                .findAny();
        if (optopttd.isPresent() && optopttd.get().isPresent()) {
            return optopttd.get().get();
        } else {
            return null;
        }
    }



}
