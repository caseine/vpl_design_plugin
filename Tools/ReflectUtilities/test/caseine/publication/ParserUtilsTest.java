/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package caseine.publication;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import org.junit.Test;

import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.utils.SourceRoot;

import caseine.extra.utils.mutations.Mutant;

public class ParserUtilsTest {
    
    public ParserUtilsTest() {
    }

	@Test
	public void test() throws IOException {
		SourceRoot srtest = new SourceRoot(Paths.get("test"));
		srtest.tryToParse();
		List<CompilationUnit> cus = srtest.getCompilationUnits().stream()
				.filter(cu -> ParserUtils.compilationUnitHasAPrimaryTypeAnnoted(cu, Mutant.class))
				.sorted(Comparator.comparing(cu -> cu.getPackageDeclaration().get().getNameAsString()))
				.collect(Collectors.toList());
		assertEquals(1, cus.size());
	}    
}
