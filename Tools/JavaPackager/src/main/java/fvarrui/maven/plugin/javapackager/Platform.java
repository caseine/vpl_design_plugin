package fvarrui.maven.plugin.javapackager;

public enum Platform {
	auto,
	linux,
	mac,
	windows;
}
