package caseine;

import java.io.*;

public class NoTypeWriters {


    public static void writeAString(String aString, File toWrite) throws IOException {
        try (
                BufferedWriter out = new BufferedWriter(new PrintWriter(toWrite));
        ) {
            out.write(aString);
        }
    }
}
