package caseine.cases;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.net.URISyntaxException;
import java.util.*;

public class VplEvaluateCases implements Iterable<Case> {
    private File file;
    private List<Case> cases = new LinkedList<>();
    private List<Case> proportionalCases = new LinkedList<>();


    public VplEvaluateCases(File file) throws FileNotFoundException {
        this.file = file;
        read();
        proportionate(100);
    }

    /**
     *
     * @param line a String that must begin by "case = "
     * @param in the red stream
     * @return a String that begin by "case = " or "" and its the end of the stream
     */
    private String readACase(String line, Scanner in) {

        return "";
    }

    private void proportionate(double nm) {
    }


    private void read() throws FileNotFoundException {
        try (Scanner in = new Scanner(file)) {
            String line = "";
            // To find the first case ....
            while (in.hasNextLine() && !line.toUpperCase().startsWith("CASE")) {
                line = in.nextLine().trim();
            }
            // To find all the remaining cases
            while (line.toUpperCase().startsWith("CASE")) {
                line = readACase(line, in);
            }
        }
    }

    public String toString(List<Case> cases) {
        StringBuilder sb = new StringBuilder();
        for(Case c : cases) {
            sb.append(c).append('\n');
        }
        return sb.toString();
    }
    public String toString() {
        return toString(cases);
    }
    public String toStringProportional() {
        return toString(proportionalCases);
    }


    @Override
    public Iterator<Case> iterator() {
        return cases.iterator();
    }

    public int size() {
        return cases.size();
    }

    public static void main(String[] args) throws URISyntaxException, FileNotFoundException {
        VplEvaluateCases vec = new VplEvaluateCases(new File(VplEvaluateCases.class.getResource("/vpl_evaluate.cases").toURI()));
        System.out.println(vec);
    }
}
