package caseine;

import caseine.exceptions.*;
import caseine.generators.NoTypeCfRfFileGenerator;
import caseine.generators.NoTypeTestFilesGenerator;

import java.io.*;
import java.util.*;


/**
 * Analyse un dossier contenant des fichiers *.py pour produire le dossier caseine-output ...
 */
public class NoTypePathReader {
    // The root of the directory to analyze
    private final File root;
    // The *.py files in order to generate vpl_run.sh
    private final List<File> pythonsInEf = new LinkedList<>();
    // The root of the directory cf
    private File rootCf;
    // The root of the directory rf
    private File rootRf;
    // The root of the directory ef
    private File rootEf;
    // The root of the directory where to put unit tests
    private File test;
    // A template file
    private File student;
    // A test file
    private File scripttest;
    // All files in ef are kept in order to change imports before written them
    private Map<File, String> efFiles = new HashMap<>();
    // The replacements in ef in order to change includes in ref
    private Map<File, File> changedInEf = new HashMap<>();
    // Root file was empty firt
    private boolean rootWasEmptyFirst = false;


    // The extensions of the files to be analyzed
    public String[] EXTENSIONS;
    // A pattern to tell that a file is to be deleted from rf and cf, but it has to be added to ef
    public String TO_DEL;
    // A pattern to tell where begins and ends the area to be removed (for instance, #-#)
    private String REMOVE_PATTERN;
    // A pattern to tell where begins the area to uncomment and to copy (for instance, #=#)
    private String COPYING_PATTERN;
    // A pattern with a group to tell how uncomment and copy (for instance, #(.*))
    private String UNCOMMENT_PATTERN;

    private void writeATemplate() throws TestDirectoryMissingException {

    }

    /**
     * @param root the root directory
     * @throws IOException if directory is wrong
     * @throws WrongGradeException grade is wrong
     * @throws RootDirectoryMissingException root missing
     *
     */
    public NoTypePathReader(File root, String[] extensions) throws
            IOException,
            WrongGradeException,
            RootDirectoryMissingException {
        if (root == null) {
            throw new RootDirectoryMissingException("The root directory is missing");
        }
        this.EXTENSIONS = extensions;
        this.root = root;
        setExtensions();
        setPatterns();
        setTheScene();
        read();
    }

    private void setPatterns() {
        Properties properties = new Properties();
        try (InputStream in = new FileInputStream(new File(root, ".caseine"))) {
            properties.load(in);
            String removePattern = properties.getProperty("REMOVE_PATTERN");
            if (removePattern != null && !removePattern.isEmpty()) {
                REMOVE_PATTERN = removePattern;
            }
            String copyingPattern = properties.getProperty("COPYING_PATTERN");
            if (copyingPattern != null && !copyingPattern.isEmpty()) {
                COPYING_PATTERN = copyingPattern;
            }
            String uncommentPattern = properties.getProperty("UNCOMMENT_PATTERN");
            if (uncommentPattern != null && !uncommentPattern.isEmpty()) {
                UNCOMMENT_PATTERN = uncommentPattern;
            }
            String toDelPattern = properties.getProperty("TO_DEL_PATTERN");
            if (toDelPattern != null && !toDelPattern.isEmpty()) {
                TO_DEL = toDelPattern;
            }
        } catch (IOException e) {

        }
    }

    private void setExtensions() {
        Properties properties = new Properties();
        try (InputStream in = new FileInputStream(new File(root, ".caseine"))) {
            properties.load(in);
            String extensionsAsString = properties.getProperty("FILE_PATTERNS");
            if (extensionsAsString != null && !extensionsAsString.isEmpty()) {
                String[] extensions = extensionsAsString.split(";");
                EXTENSIONS = extensions;
            }
        } catch (IOException e) {

        }
    }

    private void checkRoot() {

    }

    /**
     * Sets the scene to push on caseine
     *
     * @throws IOException
     */
    private void setTheScene() throws IOException {
        // Creating file tree
        File caseineOutput = new File(root, "target/caseine-output");
        caseineOutput.mkdirs();
        rootEf = new File(caseineOutput, "ef");
        rootEf.mkdirs();
        rootRf = new File(caseineOutput, "rf");
        rootRf.mkdirs();
        rootCf = new File(caseineOutput, "cf");
        rootCf.mkdirs();
        test = new File(root, "test");
        test.mkdirs();
        if (!rootEf.exists()) {
            throw new IOException("Unable to create " + rootEf.getAbsolutePath());
        }
        if (!rootRf.exists()) {
            throw new IOException("Unable to create " + rootRf.getAbsolutePath());
        }
        if (!rootCf.exists()) {
            throw new IOException("Unable to create " + rootCf.getAbsolutePath());
        }
    }

    private void write(File file, String contenu) throws FileNotFoundException {
        try (PrintWriter out = new PrintWriter(file)) {
            out.print(contenu);
        }
    }

    /*
    If the first line of a file is the TO_DEL Tag, this file is to be deleted from rf and cf, but it has to be added
    to ef.
     */
    private boolean isToDel(File file) {
        try (java.util.Scanner in = new Scanner(file)) {
            if (in.hasNextLine() && TO_DEL != null) {
                String line = in.nextLine();
                return line.matches(TO_DEL);
            } else {
                return false;
            }
        } catch (FileNotFoundException e) {
            return false;
        }
    }

    private void read() throws IOException, WrongGradeException {
        NoTypeCfRfFileGenerator noTypeCfRfFileGenerator;
        File main = null;
        for (File file : root.listFiles()) {
            if (file.isFile() && matches(file)) {
                noTypeCfRfFileGenerator = new NoTypeCfRfFileGenerator(file, REMOVE_PATTERN, COPYING_PATTERN, UNCOMMENT_PATTERN);
                // A file to delete is indeed in ef and not in cf nor rf
                // Otherwise it is in cf and rf and not in ef
                if (!isToDel(file)) {
                    write(new File(rootRf, file.getName()), noTypeCfRfFileGenerator.getRf());
                    write(new File(rootCf, file.getName()), noTypeCfRfFileGenerator.getCf());
                } else {
                    write(new File(rootEf, file.getName()), noTypeCfRfFileGenerator.getCf());
                }
            }
        }
        // In ef
        new NoTypeTestFilesGenerator(test, rootEf);
    }

    private boolean matches(File file) {
        for (String extension : EXTENSIONS) {
            if (file.getName().matches(extension)) {
                return true;
            }
        }
        return false;
    }

    public static void main(String[] args) throws IOException, WrongGradeException, TestDirectoryMissingException, FileMissingException, UnitTestsFileMissingException, RootDirectoryMissingException {
        new NoTypePathReader(new File("/home/yvan/tmp/mesVPLs/exnotype"), new String[]{".+\\.sh"});
    }
}
