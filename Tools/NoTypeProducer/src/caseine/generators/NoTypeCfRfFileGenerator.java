package caseine.generators;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Scanner;

/**
 * <p>
 * Reads a special source file and prepare it to be written by 2 ways in cf, rf caseine paths
 * </p>
 * <p>
 * Such a file may contains 2 kinds of patterns : REMOVE_PATTERN and COPYING_PATTERN
 * </p>
 * <p>
 * An area to remove is limited by 2 REMOVE_PATTERN, the first at its beginning and the second at its ending.
 * A COPYING_PATTERN may exist or not. If yes, it should be between an beginning REMOVE_PATTERN and a ending
 * REMOVE_PATTERN.
 * </p>
 * <p>
 * Rf (required files)
 * </p>
 * <p>
 * In rf, the patterns and the area between the beginning REMOVE_PATTERN and COPYING_PATTERN (or the ending
 * REMOVE_PATTERN, if COPYING_PATTERN is missing) are removed.
 * </p>
 * <p>
 * Cf (corrected files)
 * </p>
 * <p>
 * In cf, the patterns and the area between COPYING_PATTERN et the ending REMOVE_PATTERN are removed. If COPYING_PATTERN
 * is missing, only the patterns are removed.
 * </p>
 * <p>
 * Theses contents are available by the methods getRf() and getCf()
 * </p>
 */
public class NoTypeCfRfFileGenerator {
    private final File sourceFile;
    private final String REMOVE_PATTERN;
    private final String COPYING_PATTERN;
    private final String UNCOMMENT_PATTERN;
    private boolean removing = false;
    private boolean copying = false;

    private StringBuilder rf = new StringBuilder();
    private StringBuilder cf = new StringBuilder();

    /**
     * Create a NoTypeCfRfFileGenerator in order to generate ef, rf and cf files from headerOrSourceFile
     *
     * @param sourceFile the file whose images you want in cf, rf and ef.
     * @throws IOException if headerOrSourceFile does not exist or is unreadable
     */
    public NoTypeCfRfFileGenerator(File sourceFile) throws IOException {
        this(sourceFile, null, null, null);
    }

    /**
     * Create a NoTypeCfRfFileGenerator in order to generate ef, rf and cf files from headerOrSourceFile
     *
     * @param sourceFile the file whose images you want in cf, rf and ef.
     * @throws IOException if headerOrSourceFile does not exist or is unreadable
     */
    public NoTypeCfRfFileGenerator(File sourceFile, String removePattern, String copyingPattern, String uncommentPattern) throws IOException {
        this.sourceFile = sourceFile;

        this.REMOVE_PATTERN = removePattern != null && !removePattern.isEmpty() ? removePattern : "^[\\s]*#[\\s]*-#";
        this.COPYING_PATTERN = copyingPattern != null && !copyingPattern.isEmpty() ? copyingPattern : "^[\\s]*#[\\s]*=#";
        this.UNCOMMENT_PATTERN = uncommentPattern != null && !uncommentPattern.isEmpty() ? uncommentPattern : "(^\\s*)#(.*)$";
        analyse();
    }

    private void analyse() throws FileNotFoundException {
        try (Scanner in = new Scanner(this.sourceFile)) {
            String line = "";
            while (in.hasNextLine()/* && !line.trim().startsWith("#")*/) {
                line = in.nextLine();
                if (line.replaceAll("[\\s]+", "").matches(REMOVE_PATTERN))/* .startsWith(REMOVE_PATTERN)/*/ {
                    removing = !removing;
                    if (!removing) {
                        copying = false;
                    }
                } else if (line.replaceAll("[\\s]+", "").matches(COPYING_PATTERN))/*.startsWith(COPYING_PATTERN))*/ {
                    copying = true;
                } else {
                    if (!removing) {
                        rf.append(line).append('\n');
                        cf.append(line).append('\n');
                    } else {
                        if (copying) {
                            try {
                                rf.append(line.replaceAll(UNCOMMENT_PATTERN, "$1$2")).append('\n');
                            } catch (Exception e1) {
                                try {
                                    rf.append(line.replaceAll(UNCOMMENT_PATTERN, "$1")).append('\n');
                                } catch (Exception e2) {
                                    rf.append(line.replaceFirst(UNCOMMENT_PATTERN, "")).append('\n');
                                }
                            }
                        } else {
                            cf.append(line).append('\n');
                        }
                    }
                }
            }
            while (in.hasNextByte()) {
                rf.append(in.nextByte());
                cf.append(in.nextByte());
            }
        }

    }

    public String getRf() {
        return rf.toString();
    }

    public String getCf() {
        return cf.toString();
    }

}
