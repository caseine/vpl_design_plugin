package caseine.generators;

import caseine.FileUtils;
import caseine.exceptions.WrongGradeException;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.util.regex.Pattern;


/**
 * 1. Copie les fichiers du dossier test dans ef en ajustant les grades.
 * 2. Génère dans ef le fichier vpl_evaluate.cases
 */
public class NoTypeTestFilesGenerator {
    private final File rootTestFile;
    private final File rootEf;
    private final Pattern GRADE_PATTERN = Pattern.compile("@grade\\(([^()]+)\\)");

    public NoTypeTestFilesGenerator(File rootTestFile, File rootEf) throws IOException, WrongGradeException {
        this.rootTestFile = rootTestFile;
        this.rootEf = rootEf;
        analyse();
    }

    private void writeEvaluateCases() throws IOException {
        System.out.println("Writing evaluate cases");
        Files.walk(rootTestFile.toPath())
                .filter(path -> path.toFile().isFile())
                .forEach(path -> {
                    try {
                        FileUtils.fileCopy(path.toFile(), new File(rootEf, path.toFile().getName()));
                    } catch (IOException e) {
                        throw new RuntimeException(e);
                    }
                });
    }

    private double getTotalGrade() throws FileNotFoundException, WrongGradeException {
        return 100.0;
    }

    private void putInEfWithRelativeGrade(double totalGrade) throws IOException {

    }

    private void analyse() throws IOException, WrongGradeException {
        writeEvaluateCases();
        double totalGrade = getTotalGrade();
        putInEfWithRelativeGrade(totalGrade);
    }

}
