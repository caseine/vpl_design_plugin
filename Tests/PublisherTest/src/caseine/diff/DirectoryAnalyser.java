/*
 * Copyright (C) 2019 Yvan Maillot <yvan.maillot@uha.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package caseine.diff;

import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.body.TypeDeclaration;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Compare les unités de compilation bien parsées et bien placé à tester avec la
 * référence.
 *
 * getWrong() donne la liste unités de compilation bien parsées dont le contenu
 * diffère de la référence.
 *
 * @author Yvan Maillot &lt;yvan.maillot@uha.fr&gt;
 */
public class DirectoryAnalyser {

    private final List<TestAndRef<CompilationUnit>> wrong;
    private final List<CompilationUnit> missing;

    public static class TestAndRef<T> {

        public final T ref;
        public final T toTest;

        private TestAndRef(T ref, T toTest) {
            this.ref = ref;
            this.toTest = toTest;
        }

    }

    public static String getMainClassName(CompilationUnit cu) {
        Optional<String> className = cu.getPrimaryType()
            .flatMap(TypeDeclaration::getFullyQualifiedName);
        if (className.isPresent()) {
            return className.get();
        } else {
            Optional<String> typeName = cu.getPrimaryTypeName();
            String pack = cu.getPackageDeclaration()
                .map(packd -> packd.getName().asString()).orElseGet(() -> "");
            return pack + "." + typeName.get() ;
        }

    }

    public DirectoryAnalyser(DirectoryParser dpRef, DirectoryParser dpToTest) {
        List<CompilationUnit> wrongToTest = new LinkedList<>(dpToTest.getWellParsed());
        wrongToTest.removeAll(dpRef.getWellParsed());

        wrong = new LinkedList<>();
        for (CompilationUnit cuToTest : wrongToTest) {
            //System.out.println(cuToTest.getStorage().get().getPath().normalize().toString());
            CompilationUnit cuToRef = correspondingRefCompilationUnit(cuToTest, dpRef.getWellParsed());
            wrong.add(new TestAndRef<>(cuToTest, cuToRef));
        }

        List<CompilationUnit> nonProperlyGeneratedRefs = new LinkedList<>(dpRef.getWellParsed());
        nonProperlyGeneratedRefs.removeAll(dpToTest.getWellParsed());

        missing = nonProperlyGeneratedRefs.stream()
            .filter(refCU -> ! dpToTest.getWellParsed().stream()
                .anyMatch(testCu -> getMainClassName(refCU).equals(getMainClassName(testCu))))
            .collect(Collectors.toList());
    }

    private CompilationUnit correspondingRefCompilationUnit(CompilationUnit cuToTest, List<CompilationUnit> refs) {
        // String cuToTestName = cuToTest.getStorage().get().getPath().normalize().toString().replaceFirst(".*/target/caseine-output/", "");
        String cuToTestName = getMainClassName(cuToTest);

        for (CompilationUnit cuRef : refs) {
            String cuToRefName = getMainClassName(cuRef);
            if (cuToTestName.equals(cuToRefName)) {
                return cuRef;
            }
        }
        return null;
    }

    public List<TestAndRef<CompilationUnit>> getWrong() {
        return wrong;
    }

    public List<CompilationUnit> getMissing() {
        return missing;
    }


}
