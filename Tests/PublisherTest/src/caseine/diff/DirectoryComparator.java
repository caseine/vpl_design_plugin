/*
 * Copyright (C) 2019 Yvan Maillot <yvan.maillot@uha.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package caseine.diff;

import com.github.difflib.DiffUtils;
import com.github.difflib.algorithm.DiffException;
import com.github.difflib.patch.AbstractDelta;
import com.github.difflib.patch.Patch;
import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.body.TypeDeclaration;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Compare un dossier de référence à un dossier à tester.
 * 
 * L'état est ok si le dossier de test est identique à la référence.
 * 
 * getWrong() retourne la liste des unités de compilations incorrectes.
 * 
 * @author Yvan Maillot &lt;yvan.maillot@uha.fr&gt;
 */
public class DirectoryComparator {

    private Path toTest;
    private Path ref;
    private boolean ok;
    private DirectoryAnalyser da;

    public DirectoryComparator(Path toTest, Path ref) throws IOException {
        this.toTest = toTest;
        this.ref = ref;

        DirectoryParser dpRef = new DirectoryParser(this.ref);
        DirectoryParser dpToTest = new DirectoryParser(this.toTest);

        da = new DirectoryAnalyser(dpRef, dpToTest);

        ok = dpRef.getWellParsed().equals(dpToTest.getWellParsed());

    }

    public DirectoryComparator(File toTest, File ref) throws IOException {
        this(toTest.toPath(), ref.toPath());
    }

    public DirectoryComparator(String toTest, String ref) throws IOException {
        this(new File(toTest), new File(ref));
    }

    public boolean isOk() {
        return ok;
    }
    
    public List<DirectoryAnalyser.TestAndRef<CompilationUnit>> getWrong() {
        return da.getWrong();
    }

    public List<CompilationUnit> getMissing() {
        return da.getMissing();
    }

    @Override
    public String toString() {
           StringBuilder sb = new StringBuilder();
        if (ok) {
            sb.append("************ GOOD ******************\n");
            for (DirectoryAnalyser.TestAndRef<CompilationUnit> cu : getWrong()) {
                sb.append(cu.ref.getPrimaryTypeName());
            }
        } else {
            sb.append("************ WRONG !!! ******************\n");
            for (DirectoryAnalyser.TestAndRef<CompilationUnit> cu : getWrong()) {
                try {
                    List<String> original = Files.readAllLines(cu.ref.getStorage().get().getPath());
                    
                    sb.append("class ")
                            .append(cu.ref.getPrimaryType().flatMap(TypeDeclaration::getFullyQualifiedName).get())
                            .append(" differs from the reference\n");

                    sb.append('\n');

                    sb.append(cu.ref.getStorage().get().getDirectory());
                    sb.append('\n');
                    
                    
                    List<String> revised = Files.readAllLines(cu.toTest.getStorage().get().getPath());
                    
                    //compute the patch: this is the diffutils part
                    Patch<String> patch = DiffUtils.diff(original, revised);
                    
                    //simple output the computed patch to console
                    sb.append("--- The differences of class ")
                            .append(DirectoryAnalyser.getMainClassName(cu.ref));
                    
                    for (AbstractDelta<String> delta : patch.getDeltas()) {
                        sb.append("-> ").append(delta).append("\n");
                    }
                } catch (Exception ex) {
                    //sb.append(ex.toString());
                }
            }

            for (CompilationUnit missingCu : getMissing()) {
                sb.append("Class \"").append(DirectoryAnalyser.getMainClassName(missingCu)).append("\" n'est pas générée.");
     
            }
        }
        sb.append('\n');
        return sb.toString();
    }
}
