/*
 * Copyright (C) 2019 Yvan Maillot <yvan.maillot@uha.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package caseine.diff;

import com.github.javaparser.ast.CompilationUnit;
import java.io.File;
import java.nio.file.Path;
import java.util.List;

/**
 *
 * @author Yvan Maillot &lt;yvan.maillot@uha.fr&gt;
 */
public class FileComparator {
    private final CompilationUnit toTest;
    private final List<CompilationUnit> refs;
    private final CompilationUnit ref = null;

    public FileComparator(CompilationUnit toTest, List<CompilationUnit> refs) {
        this.toTest = toTest;
        this.refs = refs;
        
        String fullyQualifiedTestName = "";
        
        Path testPath = toTest.getStorage().get().getPath().normalize();
        
        refs.stream()
                .filter(cu -> cu.getPrimaryType().isPresent())
                .filter(cu -> cu.getPrimaryType().get().getFullyQualifiedName().isPresent())
                .forEach(System.out::println);
        
    }
    
    
}
