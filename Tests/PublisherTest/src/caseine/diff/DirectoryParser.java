/*
 * Copyright (C) 2019 Yvan Maillot <yvan.maillot@uha.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package caseine.diff;

import com.github.javaparser.ParseResult;
import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.PackageDeclaration;
import com.github.javaparser.utils.SourceRoot;
import java.io.IOException;
import java.nio.file.Path;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

/**
 * Parse un dossier et construit trois listes d'unités de compilation
 *   - les bien parsées
 *   - les bien parsées mais avec un paquetage incohérent avec son chemin
 *   - les mal parsées.
 * 
 * @author Yvan Maillot &lt;yvan.maillot@uha.fr&gt;
 */
public class DirectoryParser {
    
    private final Path root;
    private List<CompilationUnit> wellParsed;
    private List<CompilationUnit> notWellParsed;
    private List<CompilationUnit> wrongPath;
    
    public DirectoryParser(Path root) throws IOException {
        this.root = root;
    
        SourceRoot sr = new SourceRoot(root);

        List<ParseResult<CompilationUnit>> lpcu = sr.tryToParse();
                
        wellParsed = new LinkedList<>();
        notWellParsed = new LinkedList<>();
        wrongPath = new LinkedList<>();

        lpcu.forEach(pr -> {
            if (pr.isSuccessful()) {
                CompilationUnit cu = pr.getResult().get();
                String path = cu.getStorage().get().getDirectory().toString();
                Optional<PackageDeclaration> pkgDeclaration = cu.getPackageDeclaration();
                if (pkgDeclaration.isPresent()) {
                	String pkg = pkgDeclaration.get().getNameAsString().replace(".", "/");                
	                if (!path.endsWith(pkg)) {
	                    wrongPath.add(cu);                    
	                } else {
	                    wellParsed.add(cu);
	                }
                } else {
                    wellParsed.add(cu);                	
                }
            } else {
                notWellParsed.add(pr.getResult().get());
            }
        });
    }

    public Path getRoot() {
        return root;
    }

    public List<CompilationUnit> getWellParsed() {
        return wellParsed;
    }

    public List<CompilationUnit> getNotWellParsed() {
        return notWellParsed;
    }

    public List<CompilationUnit> getWrongPath() {
        return wrongPath;
    }
    
    
        
}
