package caseine;

import static com.github.javaparser.utils.CodeGenerationUtils.mavenModuleRoot;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.junit.Test;

import com.github.difflib.algorithm.DiffException;

import caseine.diff.DirectoryComparator;

public class GeneratedFilesTest {

    Path projects = mavenModuleRoot(GeneratedFilesTest.class).resolve(Paths.get("..", "Labs")).normalize(); 
    Path refs = mavenModuleRoot(GeneratedFilesTest.class).resolve(Paths.get("resources", "test", "refs")).normalize();


    private void doTheDiff(String project) {
        try {

            Path caseineoutput = projects.resolve(Paths.get(project, "target", "caseine-output"));
            Path ref = refs.resolve(project.replaceFirst("ToTest", ""));

            /* 
            
            Path source = projects.resolve(Paths.get(project, "src"));
            Path test = projects.resolve(Paths.get(project, "test"));
            
            Publisher pbshr = new Publisher(source, test, caseineoutput);
            
            try {
                pbshr.publishCf();
                pbshr.publishRf();
                pbshr.publishTest();
                pbshr.publishCfTest();
            } catch (ClassNotFoundException ex) {
                fail("Publisher " + ex);
            }*/
            
            System.out.println("************ CF ******************");
            testGeneratedDirectory(caseineoutput.resolve("cf"), ref.resolve("cf"));
            System.out.println("************ RF ******************");
            testGeneratedDirectory(caseineoutput.resolve("rf"), ref.resolve("rf"));
            System.out.println("************ EF ******************");
            testGeneratedDirectory(caseineoutput.resolve("ef"), ref.resolve("ef"));
            System.out.println("************ TESTRF ******************");
            testGeneratedDirectory(caseineoutput.resolve("testrf"), ref.resolve("testrf"));

        } catch (IOException | DiffException ex) {
            fail(ex.toString());
        }
    }

    public void testGeneration(String project) {
        System.out.println("Test " + project);

        // generateTheCaseineOutput(project);

        doTheDiff(project);

    }

    private void testGeneratedDirectory(Path testDirectory, Path refDirectory)
            throws IOException, DiffException {
        DirectoryComparator dc = new DirectoryComparator(testDirectory, refDirectory);
        assertTrue("Generated files differ from reference . \n" + dc, dc.isOk());
    }

    //@Test
    public void testMinimal() {
        testGeneration("minimal");
    }

    //@Test
    public void testTriangles() {
        testGeneration("triangles");
    }

    //@Test
    public void testQuelquesExpressionsBooleennes() {
        testGeneration("QuelquesExpressionsBooleennes");
    }

    //@Test
    public void testRegionsDuPlan() {
        testGeneration("RegionsDuPlan");
    }

    @Test
    public void tests() throws IOException {    
    	Files.list(projects)
    	.filter(Files::isDirectory)
        .filter(f -> !f.toFile().isHidden() && !f.toFile().getName().equals("target"))
        .forEach(f -> testGeneration(f.toFile().getName()));
    	//.forEach(System.out::println);
    }
	    
}
