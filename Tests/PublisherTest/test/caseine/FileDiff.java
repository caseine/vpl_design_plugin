/*
 * Copyright (C) 2019 Yvan Maillot <yvan.maillot@uha.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package caseine;

import caseine.diff.DirectoryAnalyser;
import caseine.diff.DirectoryComparator;
import com.github.difflib.DiffUtils;
import com.github.difflib.algorithm.DiffException;
import com.github.difflib.patch.AbstractDelta;
import com.github.difflib.patch.Patch;
import com.github.javaparser.ast.CompilationUnit;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;

/**
 *
 * @author Yvan Maillot &lt;yvan.maillot@uha.fr&gt;
 */
public class FileDiff {

    private static void affiche(DirectoryComparator dc) throws IOException, DiffException {
        if (dc.isOk()) {
            System.out.println("************ GOOD ******************");
            for (DirectoryAnalyser.TestAndRef<CompilationUnit> cu : dc.getWrong()) {
                System.out.println(cu.ref.getPrimaryTypeName());
            }
        } else {
            System.out.println("************ WRONG ******************");
            for (DirectoryAnalyser.TestAndRef<CompilationUnit> cu : dc.getWrong()) {
                System.out.println("Le fichier "
                        + cu.ref.getStorage().get().getPath().normalize().toString().replaceFirst(".*/target/caseine-output/", "")
                        + " diffère de sa référence."
                );

                List<String> original = Files.readAllLines(cu.ref.getStorage().get().getPath());
                List<String> revised = Files.readAllLines(cu.toTest.getStorage().get().getPath());

                //compute the patch: this is the diffutils part
                Patch<String> patch = DiffUtils.diff(original, revised);

                //simple output the computed patch to console
                System.out.println("************ Les différences ******************");
                for (AbstractDelta<String> delta : patch.getDeltas()) {
                    System.out.println("-> " + delta + "\n") ;
                }

            }
        }
        System.out.println();
    }

    public static void main(String[] args) throws IOException, DiffException {

        

    }
}
