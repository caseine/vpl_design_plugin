package edu.uha.miage;

import caseine.publication.ParserUtils;
import com.github.javaparser.ast.NodeList;
import com.github.javaparser.ast.body.ConstructorDeclaration;
import com.github.javaparser.ast.body.MethodDeclaration;
import com.github.javaparser.ast.expr.AssignExpr;
import static org.junit.jupiter.api.Assertions.*;

/*
 * Creative commons CC BY-NC-SA 2021 Yvan Maillot <yvan.maillot@uha.fr>
 *
 *     Share - You can copy and redistribute the material in any medium or format
 *
 *     Adapt - You can remix, transform, and build upon the material
 *
 * Under the following terms :
 *
 *     Attribution - You must give appropriate credit, provide a link to the license,
 *     and indicate if changes were made. You may do so in any reasonable manner,
 *     but not in any way that suggests the licensor endorses you or your use.
 *
 *     NonCommercial — You may not use the material for commercial purposes.
 *
 *     ShareAlike — If you remix, transform, or build upon the material,
 *     you must distribute your contributions under the same license as the original.
 *
 * Notices:    You do not have to comply with the license for elements of
 *             the material in the public domain or where your use is permitted
 *             by an applicable exception or limitation.
 *
 * No warranties are given. The license may not give you all of the permissions
 * necessary for your intended use. For example, other rights such as publicity,
 * privacy, or moral rights may limit how you use the material.
 *
 * See <https://creativecommons.org/licenses/by-nc-sa/4.0/>.
 */
class BidonTest {

    public static String codeQualityCheckForConstructor(ConstructorDeclaration constructorDeclaration) {
        ParserUtils.FinderOfAssignExpr finder = new ParserUtils.FinderOfAssignExpr(constructorDeclaration);
        if (finder.getNumberOfAssignExpr() != 3) {
            return "Il faut 3 affectations dans le constructeur";
        } else {
            AssignExpr first = finder.getAssignExpr().get(0);
            AssignExpr second = finder.getAssignExpr().get(1);
            AssignExpr third = finder.getAssignExpr().get(2);
            if (!first.getTarget().toString().equals("this.a")) {
                return "La première affectation doit être sur this.a";
            }
            if (!second.getTarget().toString().equals("this.b")) {
                return "La deuxième affectation doit être sur this.b";
            }
            if (!third.getTarget().toString().equals("this.c")) {
                return "La troisième affectation doit être sur this.c";
            }
            if (!first.getValue().toString().equals("a")) {
                return "Le premier paramètre de votre constructeur doit s'appeler a";
            }
            if (!second.getValue().toString().equals("b")) {
                return "Le deuxième paramètre de votre constructeur doit s'appeler b";
            }
            if (!third.getValue().toString().equals("c")) {
                return "Le troisième paramètre de votre constructeur doit s'appeler c";
            }
        }
        return "OK";
    }

    public static String codeQualityCheckExplicitConstructorCall(ConstructorDeclaration constructorDeclaration) {
        if (constructorDeclaration.getBody().getStatements().size() > 0) {
            var stmt = constructorDeclaration.getBody().getStatement(0);
            if (!stmt.isExplicitConstructorInvocationStmt())
                return "La première instruction doit être un appel explicite au constructeur à trois paramètres";
            else {
                if (constructorDeclaration.getParameters().size() == 2) {
                    var eci = stmt.asExplicitConstructorInvocationStmt();
                    if (eci.getArguments().size() != 3)
                        return "L'appel explicite au constructeur doit avoir trois arguments";
                    else {
                        NodeList args = eci.getArguments();
                        if (!args.get(0).toString().equals("a"))
                            return "Le premier argument de l'appel explicite au constructeur doit être a";
                        if (!args.get(1).toString().equals("b"))
                            return "Le deuxième argument de l'appel explicite au constructeur doit être b";
                        if (!args.get(2).toString().equals("1"))
                            return "Le troisième argument de l'appel explicite au constructeur doit être 1";
                    }
                } else {
                    // nécessairement 1
                    var eci = stmt.asExplicitConstructorInvocationStmt();
                    if (eci.getArguments().size() != 3)
                        return "L'appel explicite au constructeur doit avoir trois arguments";
                    else {
                        NodeList args = eci.getArguments();
                        if (!args.get(0).toString().equals("a"))
                            return "Le premier argument de l'appel explicite au constructeur doit être a";
                        if (!args.get(1).toString().equals("2"))
                            return "Le deuxième argument de l'appel explicite au constructeur doit être 2";
                        if (!args.get(2).toString().equals("2"))
                            return "Le troisième argument de l'appel explicite au constructeur doit être 2";
                    }
                }
            }
        }
        return "OK";
    }

    public static String codeQualityCheckForGetter(MethodDeclaration methodDeclaration) {
        String getterName = methodDeclaration.getNameAsString().replaceAll("get", "").toLowerCase();
        if (methodDeclaration.getBody().isPresent()) {
            if (methodDeclaration.getBody().get().getStatements().size() != 1)
                return "Le getter doit avoir une seule instruction";
            else {
                var stmt = methodDeclaration.getBody().get().getStatement(0);
                if (!stmt.isReturnStmt())
                    return "Le getter doit avoir une seule instruction qui est un return";
                else {
                    var ret = stmt.asReturnStmt();
                    if (!ret.getExpression().isPresent())
                        return "Le getter doit avoir une seule instruction qui est un return avec une expression";
                    else {
                        var expr = ret.getExpression().get();
                        if (!expr.isNameExpr())
                            return "Le getter doit avoir une seule instruction qui est un return";
                        else {
                            var name = expr.asNameExpr();
                            if (!name.getNameAsString().equals(getterName))
                                return "Le getter doit avoir une seule instruction qui retourne le bon attribut";
                        }
                    }
                }
            }
        }
        return "OK";
    }

    public static String codeQualityCheckForGetSomme(MethodDeclaration methodDeclaration) {
        if (methodDeclaration.getBody().isPresent()) {
            if (methodDeclaration.getBody().get().getStatements().size() != 1)
                return "La méthode doit avoir une seule instruction";
            else {
                var stmt = methodDeclaration.getBody().get().getStatement(0);
                if (!stmt.isReturnStmt())
                    return "La méthode doit avoir une seule instruction qui est un return";
                else {
                    var ret = stmt.asReturnStmt();
                    if (!ret.getExpression().isPresent())
                        return "La méthode doit avoir une seule instruction qui est un return avec une expression";
                    else {
                        var expr = ret.getExpression().get();
                        ParserUtils.FinderOfBinaryExpr finder = new ParserUtils.FinderOfBinaryExpr(expr, true);
                        int nbOperandes = methodDeclaration.getParameters().size() + 3;
                        if (finder.getNumberOfBinaryExpr() != nbOperandes - 1)
                            return "La méthode doit avoir une seule instruction qui est un return avec une expression " + "qui est une somme de tous les attributs et paramètres ";
                    }
                }
            }
            return "OK";
        } else {
            return "La méthode doit avoir une seule instruction";
        }
    }

    public static String codeQualityCheckForWithAForStatement(MethodDeclaration methodDeclaration) {
        ParserUtils.FinderOfForStmt finder = new ParserUtils.FinderOfForStmt(methodDeclaration);
        if (finder.getNumberOfForStmt() != 1)
            return "La méthode doit avoir une seule boucle for";
        return "OK";
    }

    public static String codeQualityCheckForWithAWhileStatement(MethodDeclaration methodDeclaration) {
        ParserUtils.FinderOfWhileStmt finder = new ParserUtils.FinderOfWhileStmt(methodDeclaration);
        if (finder.getNumberOfWhileStmt() != 1)
            return "La méthode doit avoir une seule boucle while";
        return "OK";
    }

    public static String codeQualityCheckForWithIfStatements(MethodDeclaration methodDeclaration) {
        ParserUtils.FinderOfIfStmt finder = new ParserUtils.FinderOfIfStmt(methodDeclaration);
        if (finder.getNumberOfIfStmt() != methodDeclaration.getBody().get().getStatements().size())
            return "La méthode ne doit contenir que des ifs";
        return "OK";
    }
}
