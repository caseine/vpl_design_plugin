package edu.uha.miage;

import java.lang.reflect.*;
import org.junit.*;
import static org.junit.Assert.*;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;
import caseine.reflect.ReflectUtilities;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.MethodOrderer;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@TestMethodOrder(MethodOrderer.MethodName.class)
public class GeneratedBidonTest {

    @Test
    public void p000000000_testOnlyOneStatementInConstructorBidonint() {
        System.out.println("Check if there is only one statement in Constructor Bidon");
        try {
            com.github.javaparser.ast.body.ConstructorDeclaration md = caseine.reflect.ParserWithReflectUtilities.find(java.lang.Class.forName("edu.uha.miage.Bidon").getDeclaredConstructor(int.class));
            int nbStmts = md.getBody().getStatements().size();
            assertTrue("Only one statement expected but found " + nbStmts, nbStmts == 1);
        } catch (NoSuchMethodException ex) {
            fail("Missing method edu.uha.miage.Bidon(int.class) ");
        } catch (Exception ex) {
            fail("Fix Bidon() ");
        }
    }

    @Test
    public void p000000000_testOnlyOneStatementInConstructorBidonintint() {
        System.out.println("Check if there is only one statement in Constructor Bidon");
        try {
            com.github.javaparser.ast.body.ConstructorDeclaration md = caseine.reflect.ParserWithReflectUtilities.find(java.lang.Class.forName("edu.uha.miage.Bidon").getDeclaredConstructor(int.class, int.class));
            int nbStmts = md.getBody().getStatements().size();
            assertTrue("Only one statement expected but found " + nbStmts, nbStmts == 1);
        } catch (NoSuchMethodException ex) {
            fail("Missing method edu.uha.miage.Bidon(int.class, int.class) ");
        } catch (Exception ex) {
            fail("Fix Bidon() ");
        }
    }

    @Test
    public void p000000000_testOnlyOneStatementInMethodBidongetA() {
        System.out.println("Check if there is only one statement in Method Bidon.getA");
        try {
            com.github.javaparser.ast.body.MethodDeclaration md = caseine.reflect.ParserWithReflectUtilities.find(java.lang.Class.forName("edu.uha.miage.Bidon").getDeclaredMethod("getA"));
            if (md.getBody().isPresent()) {
                int nbStmts = md.getBody().get().getStatements().size();
                assertTrue("Only one statement expected but found " + nbStmts, nbStmts == 1);
            } else {
                fail("Missing method body edu.uha.miage.Bidon.getA() ");
            }
        } catch (NoSuchMethodException ex) {
            fail("Missing method edu.uha.miage.Bidon.getA() ");
        } catch (Exception ex) {
            fail("Fix Bidon.getA() ");
        }
    }

    @Test
    public void p000000000_testOnlyOneStatementInMethodBidongetB() {
        System.out.println("Check if there is only one statement in Method Bidon.getB");
        try {
            com.github.javaparser.ast.body.MethodDeclaration md = caseine.reflect.ParserWithReflectUtilities.find(java.lang.Class.forName("edu.uha.miage.Bidon").getDeclaredMethod("getB"));
            if (md.getBody().isPresent()) {
                int nbStmts = md.getBody().get().getStatements().size();
                assertTrue("Only one statement expected but found " + nbStmts, nbStmts == 1);
            } else {
                fail("Missing method body edu.uha.miage.Bidon.getB() ");
            }
        } catch (NoSuchMethodException ex) {
            fail("Missing method edu.uha.miage.Bidon.getB() ");
        } catch (Exception ex) {
            fail("Fix Bidon.getB() ");
        }
    }

    @Test
    public void p000000000_testOnlyOneStatementInMethodBidongetC() {
        System.out.println("Check if there is only one statement in Method Bidon.getC");
        try {
            com.github.javaparser.ast.body.MethodDeclaration md = caseine.reflect.ParserWithReflectUtilities.find(java.lang.Class.forName("edu.uha.miage.Bidon").getDeclaredMethod("getC"));
            if (md.getBody().isPresent()) {
                int nbStmts = md.getBody().get().getStatements().size();
                assertTrue("Only one statement expected but found " + nbStmts, nbStmts == 1);
            } else {
                fail("Missing method body edu.uha.miage.Bidon.getC() ");
            }
        } catch (NoSuchMethodException ex) {
            fail("Missing method edu.uha.miage.Bidon.getC() ");
        } catch (Exception ex) {
            fail("Fix Bidon.getC() ");
        }
    }

    @Test
    public void p000000000_testOnlyOneStatementInMethodBidongetSommeint() {
        System.out.println("Check if there is only one statement in Method Bidon.getSomme");
        try {
            com.github.javaparser.ast.body.MethodDeclaration md = caseine.reflect.ParserWithReflectUtilities.find(java.lang.Class.forName("edu.uha.miage.Bidon").getDeclaredMethod("getSomme", int.class));
            if (md.getBody().isPresent()) {
                int nbStmts = md.getBody().get().getStatements().size();
                assertTrue("Only one statement expected but found " + nbStmts, nbStmts == 1);
            } else {
                fail("Missing method body edu.uha.miage.Bidon.getSomme(int) ");
            }
        } catch (NoSuchMethodException ex) {
            fail("Missing method edu.uha.miage.Bidon.getSomme(int) ");
        } catch (Exception ex) {
            fail("Fix Bidon.getSomme() ");
        }
    }

    @Test
    public void p000001000_testStmtCountConstructorBidonintintint() {
        System.out.println("Check Number of statements in Constructor Bidon");
        try {
            com.github.javaparser.ast.body.ConstructorDeclaration md = caseine.reflect.ParserWithReflectUtilities.find(java.lang.Class.forName("edu.uha.miage.Bidon").getDeclaredConstructor(int.class, int.class, int.class));
            int nbStmts = md.getBody().getStatements().size();
            assertTrue("Seulement 3 instructions", 3 <= nbStmts && nbStmts <= 3);
        } catch (NoSuchMethodException ex) {
            fail("Missing constructor edu.uha.miage.Bidon(int, int, int) ");
        } catch (Exception ex) {
            fail("Fix Bidon() ");
        }
    }

    @Test
    public void p000002000_testCodeQualityConstructorBidonintintint_0() {
        System.out.println("Check Quality code in Constructor Bidonintintint");
        try {
            com.github.javaparser.ast.body.ConstructorDeclaration md = caseine.reflect.ParserWithReflectUtilities.find(java.lang.Class.forName("edu.uha.miage.Bidon").getDeclaredConstructor(int.class, int.class, int.class));
            assertEquals("Check the quality of your code", "OK", edu.uha.miage.BidonTest.codeQualityCheckForConstructor(md).toUpperCase());
        } catch (Exception ex) {
            fail("Fix Bidon.edu.uha.miage.Bidon(intintint) ");
        }
    }

    @Test
    public void p000002000_testStmtCountConstructorBidonintint() {
        System.out.println("Check Number of statements in Constructor Bidon");
        try {
            com.github.javaparser.ast.body.ConstructorDeclaration md = caseine.reflect.ParserWithReflectUtilities.find(java.lang.Class.forName("edu.uha.miage.Bidon").getDeclaredConstructor(int.class, int.class));
            int nbStmts = md.getBody().getStatements().size();
            assertTrue("Seulement 1 instruction", 1 <= nbStmts && nbStmts <= 1);
        } catch (NoSuchMethodException ex) {
            fail("Missing constructor edu.uha.miage.Bidon(int, int) ");
        } catch (Exception ex) {
            fail("Fix Bidon() ");
        }
    }

    @Test
    public void p000003000_testCodeQualityConstructorBidonintint_0() {
        System.out.println("Check Quality code in Constructor Bidonintint");
        try {
            com.github.javaparser.ast.body.ConstructorDeclaration md = caseine.reflect.ParserWithReflectUtilities.find(java.lang.Class.forName("edu.uha.miage.Bidon").getDeclaredConstructor(int.class, int.class));
            assertEquals("Check the quality of your code", "OK", edu.uha.miage.BidonTest.codeQualityCheckExplicitConstructorCall(md).toUpperCase());
        } catch (Exception ex) {
            fail("Fix Bidon.edu.uha.miage.Bidon(intint) ");
        }
    }

    @Test
    public void p000003000_testStmtCountConstructorBidonint() {
        System.out.println("Check Number of statements in Constructor Bidon");
        try {
            com.github.javaparser.ast.body.ConstructorDeclaration md = caseine.reflect.ParserWithReflectUtilities.find(java.lang.Class.forName("edu.uha.miage.Bidon").getDeclaredConstructor(int.class));
            int nbStmts = md.getBody().getStatements().size();
            assertTrue("Seulement 1 instruction", 1 <= nbStmts && nbStmts <= 1);
        } catch (NoSuchMethodException ex) {
            fail("Missing constructor edu.uha.miage.Bidon(int) ");
        } catch (Exception ex) {
            fail("Fix Bidon() ");
        }
    }

    @Test
    public void p000004000_testCodeQualityConstructorBidonint_0() {
        System.out.println("Check Quality code in Constructor Bidonint");
        try {
            com.github.javaparser.ast.body.ConstructorDeclaration md = caseine.reflect.ParserWithReflectUtilities.find(java.lang.Class.forName("edu.uha.miage.Bidon").getDeclaredConstructor(int.class));
            assertEquals("Check the quality of your code", "OK", edu.uha.miage.BidonTest.codeQualityCheckExplicitConstructorCall(md).toUpperCase());
        } catch (Exception ex) {
            fail("Fix Bidon.edu.uha.miage.Bidon(int) ");
        }
    }

    @Test
    public void p000004000_testStmtCountConstructorBidon() {
        System.out.println("Check Number of statements in Constructor Bidon");
        try {
            com.github.javaparser.ast.body.ConstructorDeclaration md = caseine.reflect.ParserWithReflectUtilities.find(java.lang.Class.forName("edu.uha.miage.Bidon").getDeclaredConstructor());
            int nbStmts = md.getBody().getStatements().size();
            assertTrue("Pas d'instruction", 0 <= nbStmts && nbStmts <= 0);
        } catch (NoSuchMethodException ex) {
            fail("Missing constructor edu.uha.miage.Bidon() ");
        } catch (Exception ex) {
            fail("Fix Bidon() ");
        }
    }

    @Test
    public void p000005000_testStmtCountMethodBidongetA() {
        System.out.println("Check Number of statements in Method Bidon.getA");
        try {
            com.github.javaparser.ast.body.MethodDeclaration md = caseine.reflect.ParserWithReflectUtilities.find(java.lang.Class.forName("edu.uha.miage.Bidon").getDeclaredMethod("getA"));
            if (md.getBody().isPresent()) {
                int nbStmts = md.getBody().get().getStatements().size();
                assertTrue("Seulement 1 instruction", 1 <= nbStmts && nbStmts <= 1);
            } else {
                fail("Missing method body edu.uha.miage.Bidon.getA() ");
            }
        } catch (NoSuchMethodException ex) {
            fail("Missing method edu.uha.miage.Bidon.getA() ");
        } catch (Exception ex) {
            fail("Fix Bidon.getA() ");
        }
    }

    @Test
    public void p000005000_testStmtCountMethodBidongetB() {
        System.out.println("Check Number of statements in Method Bidon.getB");
        try {
            com.github.javaparser.ast.body.MethodDeclaration md = caseine.reflect.ParserWithReflectUtilities.find(java.lang.Class.forName("edu.uha.miage.Bidon").getDeclaredMethod("getB"));
            if (md.getBody().isPresent()) {
                int nbStmts = md.getBody().get().getStatements().size();
                assertTrue("Seulement 1 instruction", 1 <= nbStmts && nbStmts <= 1);
            } else {
                fail("Missing method body edu.uha.miage.Bidon.getB() ");
            }
        } catch (NoSuchMethodException ex) {
            fail("Missing method edu.uha.miage.Bidon.getB() ");
        } catch (Exception ex) {
            fail("Fix Bidon.getB() ");
        }
    }

    @Test
    public void p000005000_testStmtCountMethodBidongetC() {
        System.out.println("Check Number of statements in Method Bidon.getC");
        try {
            com.github.javaparser.ast.body.MethodDeclaration md = caseine.reflect.ParserWithReflectUtilities.find(java.lang.Class.forName("edu.uha.miage.Bidon").getDeclaredMethod("getC"));
            if (md.getBody().isPresent()) {
                int nbStmts = md.getBody().get().getStatements().size();
                assertTrue("Seulement 1 instruction", 1 <= nbStmts && nbStmts <= 1);
            } else {
                fail("Missing method body edu.uha.miage.Bidon.getC() ");
            }
        } catch (NoSuchMethodException ex) {
            fail("Missing method edu.uha.miage.Bidon.getC() ");
        } catch (Exception ex) {
            fail("Fix Bidon.getC() ");
        }
    }

    @Test
    public void p000006000_testCodeQualityMethodBidongetA_0() {
        System.out.println("Check Quality code in Method Bidon.getA()");
        try {
            com.github.javaparser.ast.body.MethodDeclaration md = caseine.reflect.ParserWithReflectUtilities.find(java.lang.Class.forName("edu.uha.miage.Bidon").getDeclaredMethod("getA"));
            assertEquals("Check the quality of your code", "OK", edu.uha.miage.BidonTest.codeQualityCheckForGetter(md).toUpperCase());
        } catch (Exception ex) {
            fail("Fix Bidon.getA() ");
        }
    }

    @Test
    public void p000006000_testCodeQualityMethodBidongetB_0() {
        System.out.println("Check Quality code in Method Bidon.getB()");
        try {
            com.github.javaparser.ast.body.MethodDeclaration md = caseine.reflect.ParserWithReflectUtilities.find(java.lang.Class.forName("edu.uha.miage.Bidon").getDeclaredMethod("getB"));
            assertEquals("Check the quality of your code", "OK", edu.uha.miage.BidonTest.codeQualityCheckForGetter(md).toUpperCase());
        } catch (Exception ex) {
            fail("Fix Bidon.getB() ");
        }
    }

    @Test
    public void p000006000_testCodeQualityMethodBidongetC_0() {
        System.out.println("Check Quality code in Method Bidon.getC()");
        try {
            com.github.javaparser.ast.body.MethodDeclaration md = caseine.reflect.ParserWithReflectUtilities.find(java.lang.Class.forName("edu.uha.miage.Bidon").getDeclaredMethod("getC"));
            assertEquals("Check the quality of your code", "OK", edu.uha.miage.BidonTest.codeQualityCheckForGetter(md).toUpperCase());
        } catch (Exception ex) {
            fail("Fix Bidon.getC() ");
        }
    }

    @Test
    public void p000007000_testCodeQualityMethodBidongetSommeint_0() {
        System.out.println("Check Quality code in Method Bidon.getSomme(int)");
        try {
            com.github.javaparser.ast.body.MethodDeclaration md = caseine.reflect.ParserWithReflectUtilities.find(java.lang.Class.forName("edu.uha.miage.Bidon").getDeclaredMethod("getSomme", int.class));
            assertEquals("Check the quality of your code", "OK", edu.uha.miage.BidonTest.codeQualityCheckForGetSomme(md).toUpperCase());
        } catch (Exception ex) {
            fail("Fix Bidon.getSomme(int) ");
        }
    }

    @Test
    public void p000007000_testCodeQualityMethodBidongetSommeintint_0() {
        System.out.println("Check Quality code in Method Bidon.getSomme(intint)");
        try {
            com.github.javaparser.ast.body.MethodDeclaration md = caseine.reflect.ParserWithReflectUtilities.find(java.lang.Class.forName("edu.uha.miage.Bidon").getDeclaredMethod("getSomme", int.class, int.class));
            assertEquals("Check the quality of your code", "OK", edu.uha.miage.BidonTest.codeQualityCheckForGetSomme(md).toUpperCase());
        } catch (Exception ex) {
            fail("Fix Bidon.getSomme(intint) ");
        }
    }

    @Test
    public void p000007000_testCodeQualityMethodBidongetSommeintintint_0() {
        System.out.println("Check Quality code in Method Bidon.getSomme(intintint)");
        try {
            com.github.javaparser.ast.body.MethodDeclaration md = caseine.reflect.ParserWithReflectUtilities.find(java.lang.Class.forName("edu.uha.miage.Bidon").getDeclaredMethod("getSomme", int.class, int.class, int.class));
            assertEquals("Check the quality of your code", "OK", edu.uha.miage.BidonTest.codeQualityCheckForGetSomme(md).toUpperCase());
        } catch (Exception ex) {
            fail("Fix Bidon.getSomme(intintint) ");
        }
    }

    @Test
    public void p000008000_testCodeQualityMethodBidonwithAForStatement_0() {
        System.out.println("Check Quality code in Method Bidon.withAForStatement()");
        try {
            com.github.javaparser.ast.body.MethodDeclaration md = caseine.reflect.ParserWithReflectUtilities.find(java.lang.Class.forName("edu.uha.miage.Bidon").getDeclaredMethod("withAForStatement"));
            assertEquals("Check the quality of your code", "OK", edu.uha.miage.BidonTest.codeQualityCheckForWithAForStatement(md).toUpperCase());
        } catch (Exception ex) {
            fail("Fix Bidon.withAForStatement() ");
        }
    }

    @Test
    public void p000008000_testMethodBidonwithIfStatementsintintint() {
        System.out.println("Test Method Bidon.withIfStatements");
        try {
            Class<?> classRef = cf.edu.uha.miage.Bidon.class;
            Class<?> classToTest = ReflectUtilities.parseType("edu.uha.miage.Bidon");
            for (int i = 0; i < 100; ++i) {
                StringBuilder msg = new StringBuilder("Fix Bidon.withIfStatements() --> ");
                boolean result = ReflectUtilities.sameResult(msg, classRef, classToTest, "withIfStatements", int.class, int.class, int.class);
                assertTrue(msg.toString(), result);
            }
        } catch (Exception ex) {
            fail("Fix Bidon.withIfStatements() ");
        }
    }

    @Test
    public void p000009000_testCodeQualityMethodBidonwithAWhileStatementint_0() {
        System.out.println("Check Quality code in Method Bidon.withAWhileStatement(int)");
        try {
            com.github.javaparser.ast.body.MethodDeclaration md = caseine.reflect.ParserWithReflectUtilities.find(java.lang.Class.forName("edu.uha.miage.Bidon").getDeclaredMethod("withAWhileStatement", int.class));
            assertEquals("Check the quality of your code", "OK", edu.uha.miage.BidonTest.codeQualityCheckForWithAWhileStatement(md).toUpperCase());
        } catch (Exception ex) {
            fail("Fix Bidon.withAWhileStatement(int) ");
        }
    }

    @Test
    public void p000009000_testCodeQualityMethodBidonwithIfStatementsintintint_0() {
        System.out.println("Check Quality code in Method Bidon.withIfStatements(intintint)");
        try {
            com.github.javaparser.ast.body.MethodDeclaration md = caseine.reflect.ParserWithReflectUtilities.find(java.lang.Class.forName("edu.uha.miage.Bidon").getDeclaredMethod("withIfStatements", int.class, int.class, int.class));
            assertEquals("Check the quality of your code", "OK", edu.uha.miage.BidonTest.codeQualityCheckForWithIfStatements(md).toUpperCase());
        } catch (Exception ex) {
            fail("Fix Bidon.withIfStatements(intintint) ");
        }
    }

    @Test
    public void p000009000_testStmtCountMethodBidongetSommeint() {
        System.out.println("Check Number of statements in Method Bidon.getSomme");
        try {
            com.github.javaparser.ast.body.MethodDeclaration md = caseine.reflect.ParserWithReflectUtilities.find(java.lang.Class.forName("edu.uha.miage.Bidon").getDeclaredMethod("getSomme", int.class));
            if (md.getBody().isPresent()) {
                int nbStmts = md.getBody().get().getStatements().size();
                assertTrue("Seulement 1 instruction", 1 <= nbStmts && nbStmts <= 1);
            } else {
                fail("Missing method body edu.uha.miage.Bidon.getSomme(int) ");
            }
        } catch (NoSuchMethodException ex) {
            fail("Missing method edu.uha.miage.Bidon.getSomme(int) ");
        } catch (Exception ex) {
            fail("Fix Bidon.getSomme() ");
        }
    }

    @Test
    public void p000010000_testStmtCountMethodBidongetSommeintint() {
        System.out.println("Check Number of statements in Method Bidon.getSomme");
        try {
            com.github.javaparser.ast.body.MethodDeclaration md = caseine.reflect.ParserWithReflectUtilities.find(java.lang.Class.forName("edu.uha.miage.Bidon").getDeclaredMethod("getSomme", int.class, int.class));
            if (md.getBody().isPresent()) {
                int nbStmts = md.getBody().get().getStatements().size();
                assertTrue("Seulement 1 instruction", 1 <= nbStmts && nbStmts <= 1);
            } else {
                fail("Missing method body edu.uha.miage.Bidon.getSomme(int, int) ");
            }
        } catch (NoSuchMethodException ex) {
            fail("Missing method edu.uha.miage.Bidon.getSomme(int, int) ");
        } catch (Exception ex) {
            fail("Fix Bidon.getSomme() ");
        }
    }

    @Test
    public void p000011000_testStmtCountMethodBidongetSommeintintint() {
        System.out.println("Check Number of statements in Method Bidon.getSomme");
        try {
            com.github.javaparser.ast.body.MethodDeclaration md = caseine.reflect.ParserWithReflectUtilities.find(java.lang.Class.forName("edu.uha.miage.Bidon").getDeclaredMethod("getSomme", int.class, int.class, int.class));
            if (md.getBody().isPresent()) {
                int nbStmts = md.getBody().get().getStatements().size();
                assertTrue("Seulement 1 instruction", 1 <= nbStmts && nbStmts <= 1);
            } else {
                fail("Missing method body edu.uha.miage.Bidon.getSomme(int, int, int) ");
            }
        } catch (NoSuchMethodException ex) {
            fail("Missing method edu.uha.miage.Bidon.getSomme(int, int, int) ");
        } catch (Exception ex) {
            fail("Fix Bidon.getSomme() ");
        }
    }

    @Test
    public void p000012000_testStmtCountMethodBidonwithNoStatement() {
        System.out.println("Check Number of statements in Method Bidon.withNoStatement");
        try {
            com.github.javaparser.ast.body.MethodDeclaration md = caseine.reflect.ParserWithReflectUtilities.find(java.lang.Class.forName("edu.uha.miage.Bidon").getDeclaredMethod("withNoStatement"));
            if (md.getBody().isPresent()) {
                int nbStmts = md.getBody().get().getStatements().size();
                assertTrue("Pas d'instruction", 0 <= nbStmts && nbStmts <= 0);
            } else {
                fail("Missing method body edu.uha.miage.Bidon.withNoStatement() ");
            }
        } catch (NoSuchMethodException ex) {
            fail("Missing method edu.uha.miage.Bidon.withNoStatement() ");
        } catch (Exception ex) {
            fail("Fix Bidon.withNoStatement() ");
        }
    }

    @Test
    public void p000013000_testStmtCountMethodBidonwithAForStatement() {
        System.out.println("Check Number of statements in Method Bidon.withAForStatement");
        try {
            com.github.javaparser.ast.body.MethodDeclaration md = caseine.reflect.ParserWithReflectUtilities.find(java.lang.Class.forName("edu.uha.miage.Bidon").getDeclaredMethod("withAForStatement"));
            if (md.getBody().isPresent()) {
                int nbStmts = md.getBody().get().getStatements().size();
                assertTrue("Entre 1 et 2 instructions", 1 <= nbStmts && nbStmts <= 2);
            } else {
                fail("Missing method body edu.uha.miage.Bidon.withAForStatement() ");
            }
        } catch (NoSuchMethodException ex) {
            fail("Missing method edu.uha.miage.Bidon.withAForStatement() ");
        } catch (Exception ex) {
            fail("Fix Bidon.withAForStatement() ");
        }
    }

    @Test
    public void p000014000_testStmtCountMethodBidonwithAWhileStatementint() {
        System.out.println("Check Number of statements in Method Bidon.withAWhileStatement");
        try {
            com.github.javaparser.ast.body.MethodDeclaration md = caseine.reflect.ParserWithReflectUtilities.find(java.lang.Class.forName("edu.uha.miage.Bidon").getDeclaredMethod("withAWhileStatement", int.class));
            if (md.getBody().isPresent()) {
                int nbStmts = md.getBody().get().getStatements().size();
                assertTrue("Entre 1 et 3 instructions", 1 <= nbStmts && nbStmts <= 3);
            } else {
                fail("Missing method body edu.uha.miage.Bidon.withAWhileStatement(int) ");
            }
        } catch (NoSuchMethodException ex) {
            fail("Missing method edu.uha.miage.Bidon.withAWhileStatement(int) ");
        } catch (Exception ex) {
            fail("Fix Bidon.withAWhileStatement() ");
        }
    }

    @Test
    public void p000016000_testStmtCountMethodBidonwithIfStatementsintintint() {
        System.out.println("Check Number of statements in Method Bidon.withIfStatements");
        try {
            com.github.javaparser.ast.body.MethodDeclaration md = caseine.reflect.ParserWithReflectUtilities.find(java.lang.Class.forName("edu.uha.miage.Bidon").getDeclaredMethod("withIfStatements", int.class, int.class, int.class));
            if (md.getBody().isPresent()) {
                int nbStmts = md.getBody().get().getStatements().size();
                assertTrue("Entre 1 et 4 instructions", 1 <= nbStmts && nbStmts <= 4);
            } else {
                fail("Missing method body edu.uha.miage.Bidon.withIfStatements(int, int, int) ");
            }
        } catch (NoSuchMethodException ex) {
            fail("Missing method edu.uha.miage.Bidon.withIfStatements(int, int, int) ");
        } catch (Exception ex) {
            fail("Fix Bidon.withIfStatements() ");
        }
    }
}
