/*
 * @author : Yvan Maillot (yvan.maillot@uha.fr)
 */
package edu.uha.miage;

/**
 * Pour tester les tags @CodeQualityToCheck, @OnlyOneStatementToCheck et @StatementCountToCheck
 *
 * @author yvan
 */
public class Bidon {

    private int a, b, c;
    // TODO Écrire le constructeur à 3 paramètres entiers en seulement 3 instructions
    // 
    // TODO Écrire le constructeur à 2 paramètres entiers, le premier est affecté à a, le second à b et c vaut 1 en seulement 1 instruction
    // 
    // TODO Écrire le constructeur à 1 paramètre entier, il est affecté à a, b et c valent 2, en seulement 1 instruction
    // 
    // TODO Écrire le constructeur par défaut sans instruction (les attributs sont initialisés par défaut)
    // 
    // TODO Écrire tous les getters
    // 
    // TODO Écrire getSomme(), une fonction à un paramètre entier qui retourne la somme des attributs et du paramètre
    // 
    // TODO Écrire getSomme(), une fonction à deux paramètres entiers qui retourne la somme des attributs et des paramètres
    // 
    // TODO Écrire getSomme(), une fonction à trois paramètres entiers qui retourne la somme des attributs et des paramètres
    // 
    // TODO Écrire withNoStatement(), une fonction sans paramètre et sans instruction
    // 
    // TODO Écrire withAForStatement(), une fonction sans paramètre et avec une boucle for, avec 1 ou 2 instructions
    // 
    // TODO Écrire withAWhileStatement(), une fonction avec un paramètre entier et avec une boucle while, avec 1 à 3 instructions
    // 
    // TODO Écrire withIfStatements(), une fonction avec trois paramètres entiers et avec un à trois if, avec 1 à 4 instructions, qui retourne -1 si le premier paramètre est inférieur au deuxième, 1 si le premier paramètre est supérieur au troisième, et 0 sinon,avec seulement des if
    // 
}
