/*
 * @author : Yvan Maillot (yvan.maillot@uha.fr)
 */
package edu.uha.miage;

/**
 * Pour tester les tags @CodeQualityToCheck, @OnlyOneStatementToCheck et @StatementCountToCheck
 *
 * @author yvan
 */
public class Bidon {

    private int a, b, c;

    public Bidon(int a, int b, int c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }

    public Bidon(int a, int b) {
        this(a, b, 0);
    }

    public Bidon(int a) {
        this(a, 0, 0);
    }

    public Bidon() {
    }

    public int getA() {
        return a;
    }

    public int getB() {
        return b;
    }

    public int getC() {
        return c;
    }

    public int getSomme(int d) {
        return a + b + c + d;
    }

    public int getSomme(int d, int e) {
        return a + b + c + d + e;
    }

    public int getSomme(int d, int e, int f) {
        return a + b + c + d + e + f;
    }

    public void withNoStatement() {
    }

    public void withAForStatement() {
        for (int i = 0; i < 10; ++i) {
            System.out.println(i);
        }
    }

    public void withAWhileStatement(int n) {
        while (n > 0) {
            System.out.println(n);
            --n;
        }
    }

    public int withIfStatements(int x, int mini, int maxi) {
        if (x < mini) {
            return -1;
        } else if (x > maxi) {
            return 1;
        } else {
            return 0;
        }
    }
}
