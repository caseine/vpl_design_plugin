/*
 * @author : Yvan Maillot (yvan.maillot@uha.fr)
 */
package edu.uha.miage;

/**
 * Le but de l'exercice est d'écrire un ensemble de méthodes pour
 *
 *  1. Tout savoir (ou presque) sur une chaîne de caractères
 *
 *  2. Tout faire (ou presque) à une chaîne de caractères
 *
 * Bien sûr, le "tout ou presque" est très exagéré. Mais il y a quelques questions
 * pour apprendre à manipuler des chaînes de caractères et surtout à découvrir
 * le type String.
 *
 * Il est vivement conseillé de LIRE SA DOCUMENTATION en visitant le lien suivant :
 *
 * https://docs.oracle.com/javase/8/docs/api/java/lang/String.html
 *
 * Pour répondre à l'exercice, il suffit d'écrire, pour chaque méthode, à l'endroit
 * précisé par TODO, du code qui satisfait la spécification donnée par la JavaDoc.
 *
 * Il est également recommandé d'utiliser les méthodes de String pour répondre à
 * l'exercice. Chose qui sera d'autant plus aisée que la LECTURE DE SA
 * DOCUMENTATION sera sérieuse.
 *
 * La solution de la première méthode est donnée pour faire comprendre l'esprit
 * de l'exercice.
 *
 * Par ailleurs, pour toutes les méthodes où les paramètres peuvent connaître
 * des contraintes, comme par exemple un indice en dehors de la chaîne, la politique
 * est de NE PAS TESTER LES PARAMETRES. L'utilisateur est supposé employer
 * correctement la méthode. Tant pis si une mauvaise utilisation provoque un
 * plantage.
 *
 * @author yvan
 */
public class ManipulationChainetique {

    /**
     * Méthode qui retourne la longueur d'une chaîne de caractères
     *
     * @param s la chaîne dont on veut la longueur
     * @return la longueur de s
     *
     * Le type String comprend la méthode length() qui est documenté :
     * https://docs.oracle.com/javase/8/docs/api/java/lang/String.html#length--
     *
     * Inutile d'aller chercher midi à 14 heures, cette méthode existe utilisons-la.
     */
    public static int longueur(String s) {
        return s.length();
    /*
        Pour la plupart des questions vous trouverez le pendant de la méthode 
        demandée dans String. Dans ce cas, il faudra l'utiliser.
        
        Parfois non, dans ce cas, il faudra écrire un peu plus de code.
        */
    }

    /**
     * Méthode qui retourne le caractère d'une chaîne de caractères à une position donnée
     *
     * @param s la chaîne dont on veut un caractère
     * @param p la position du caractère recherché
     * @return le caractère de s en position p
     */
    public static int caractereDeSEnPositionP(String s, int p) {
        return s.charAt(p);
    }

    /**
     * Méthode pour savoir si deux chaînes de caractères sont égales ou non.
     *
     * @param s1 une première chaîne de caractères
     * @param s2 une seconde chaîne de caractères
     * @return vrai si s1 est la même chaîne que s2.
     */
    public static boolean sontEgales(String s1, String s2) {
        return s1.equals(s2);
    }

    /**
     * Méthode pour savoir si deux chaînes de caractères sont égales ou non,
     * sans tenir compte de la casse des caractères.
     *
     * @param s1 une première chaîne de caractères
     * @param s2 une seconde chaîne de caractères
     * @return vrai si s1 est la même chaîne que s2 sans tenir compte de la casse.
     */
    public static boolean sontEgalesSansCompteLaCasse(String s1, String s2) {
        return s1.equalsIgnoreCase(s2);
    }

    /**
     * Méthode pour savoir si une chaîne de caractères est avant une autre dans
     * l'ordre lexicographique (du dictionnaire).
     *
     * @param s1 une première chaîne de caractères
     * @param s2 une seconde chaîne de caractères
     * @return vrai si s1 est lexicographiquement avant s2 (autrement dit, dans
     * l'ordre du dictionnaire.
     */
    public static boolean s1EstAvantS2(String s1, String s2) {
        return s1.compareTo(s2) < 0;
    }

    /**
     * Méthode pour savoir si une chaîne de caractères est écrit dans une autre.
     *
     * @param s1 une première chaîne de caractères
     * @param s2 une seconde chaîne de caractères
     * @return vrai si s1 est une sous-chaîne de s2.
     */
    public static boolean s1estDansS2(String s1, String s2) {
        return s2.contains(s1);
    }

    /**
     * Méthode pour savoir si une chaîne de caractères commence par une autre.
     *
     * @param s1 une première chaîne de caractères
     * @param s2 une seconde chaîne de caractères
     * @return vrai si s1 commence par s2.
     */
    public static boolean s1commenceParS2(String s1, String s2) {
        return s1.startsWith(s2);
    }

    /**
     * Méthode qui donne la position de la première occurence d'un caractère dans
     * une chaîne de caractères.
     *
     * @param c un caractère
     * @param s une chaîne de caractères
     * @return la position de la première occurence de c dans s ou -1 s'il est absent.
     */
    public static int premierePositionDeCdansS(char c, String s) {
        return s.indexOf(c);
    }

    /**
     * Méthode qui donne la position de la dernière occurence d'un caractère dans
     * une chaîne de caractères.
     *
     * @param c un caractère
     * @param s une chaîne de caractères
     * @return la position de la dernière occurence de c dans s ou -1 s'il est absent.
     */
    public static int dernierePositionDeCdansS(char c, String s) {
        return s.lastIndexOf(c);
    }

    /**
     * Méthode qui retourne une chaîne de caractères qui est celle donnée en
     * paramètre dans laquelle un caractère est remplacé par un autre.
     *
     * @param s une chaîne de caractères
     * @param c1 un premier caractère
     * @param c2 un second caractère
     * @return une chaîne de caractères qui est s dont tous les caractères c1 sont
     * remplacés par c2.
     */
    public static String remplaceDansSC1ParC2(String s, char c1, char c2) {
        return s.replace(c1, c2);
    }

    /**
     * Méthode qui retourne une chaîne de caractères qui est celle donnée en
     * paramètre privée de tous ces espaces.
     *
     * @param s une chaîne de caractères
     * @return une chaîne de caractères qui est s privée de ses espaces.
     *
     * Par exemple,
     *
     *    - "    abc " -&gt; "abc"
     *
     *    - "    abc   de " -&gt; "abcde"
     *
     *    - "abc" -&gt; "abc"
     *
     *    - "   " -&gt; ""
     */
    public static String sPriveDeSesEspaces(String s) {
        return s.replace(" ", "");
    }

    /**
     * Méthode qui retourne une chaîne de caractères qui est la sous-chaîne d'une
     * chaîne de caractères entre deux indices.
     *
     * @param s une chaîne de caractères
     * @param debut un premier indice dans s
     * @param fin un second indice dans s
     * @return la sous-chaine de s entre p1 inclus et p2 exclus.
     */
    public static String sousChaineDeSEntreP1etP2(String s, int debut, int fin) {
        return s.substring(debut, fin);
    }

    /**
     * Méthode pour savoir si une chaîne de caractères est un palindrome.
     *
     * @param s une chaîne de caractères
     * @return vrai si s est un palindrome (https://fr.wikipedia.org/wiki/Palindrome).
     *
     * Exemples :
     *
     * "kayak" est un palindrome
     *
     * "lol" est un palindrome
     *
     * "a" est un palindrome
     *
     * "palindrome" n'est pas un palindrome
     *
     * "ab" n'est pas un palindrome.
     *
     * Remarques :
     *
     *    1. Les majuscules et les minuscules ne sont pas différenciées
     *
     *       Par exemple : "Laval" est aussi un palindrome
     *
     *    2. Les espaces ne comptent pas.
     *
     *       Par exemple : "Esope reste ici et se repose" est encore un palindrome
     *
     *    3. Mais pour nous simplifier l'exercice, les accents sont différenciés
     *
     *       Ainsi, "La mariée ira mal" n'est pas un palindrome à cause de l'accent.
     *       Ou plutot, ce n'est pas grave si ça n'en est pas un.
     */
    public static boolean estUnPalindrome(String s) {
        s = s.toLowerCase();
        s = s.replace(" ", "");
        int i = 0, j = s.length() - 1;
        while (i < j && s.charAt(i) == s.charAt(j)) {
            ++i;
            --j;
        }
        return i >= j;
    }

    /**
     * Méthode qui retourne une chaîne de caractères qui est celle passée en paramètre
     * renversée.
     *
     * @param s une chaîne de caractères
     * @return s renversée (c'est-à-dire dont le premier caractère devient le dernier
     * et le dernier le premier).
     *
     * Par exemple, "exemple" -&gt; "elpmexe", "bon" -&gt; "nob"
     */
    public static String renverseDeS(String s) {
        StringBuilder sb = new StringBuilder();
        for (int i = s.length() - 1; i >= 0; --i) {
            sb.append(s.charAt(i));
        }
        return sb.toString();
    }

    /**
     * Méthode qui retourne une chaîne de caractères qui est celle passée en paramètre
     * en majuscule.
     *
     * @param s une chaîne de caractères
     * @return s entièrement en majuscule.
     */
    public static String sEnMajuscule(String s) {
        return s.toUpperCase();
    }

    /**
     * Méthode qui retourne une chaîne de caractères qui est celle passée en paramètre
     * en minuscule.
     *
     * @param s une chaîne de caractères
     * @return s entièrement en minuscule.
     */
    public static String sEnMinuscule(String s) {
        return s.toLowerCase();
    }

    /**
     * Méthode qui retourne une chaîne de caractères qui est celle passée en paramètre
     * privée de ses éventuels espaces devant et derrière.
     *
     * Par exemple,
     *
     *    - "    abc " -&gt; "abc",
     *
     *    - "    abc   de " -&gt; "abc   de"
     *
     *    - "abc" -&gt; "abc"
     *
     *    - "   " -&gt; ""
     *
     * @param s une chaîne de caractères
     * @return s sans tous ses éventuels espaces devant et derrière.
     */
    public static String sSansEspaceDevantEtDerriere(String s) {
        return s.trim();
    }
}
