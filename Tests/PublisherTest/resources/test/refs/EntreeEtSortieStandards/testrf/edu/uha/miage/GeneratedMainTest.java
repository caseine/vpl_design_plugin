package edu.uha.miage;

import java.lang.reflect.*;
import org.junit.*;
import static org.junit.Assert.*;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;
import caseine.reflect.ReflectUtilities;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.MethodOrderer;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@TestMethodOrder(MethodOrderer.MethodName.class)
public class GeneratedMainTest {

    @Test
    @caseine.format.javajunit.Grade(1.000000)
    public void p000010000_testMethodMainaireTriangledoubledoubledouble() {
        System.out.println("Test Method Main.aireTriangle");
        try {
            Class<?> classRef = cf.edu.uha.miage.Main.class;
            Class<?> classToTest = ReflectUtilities.parseType("edu.uha.miage.Main");
            for (int i = 0; i < 100; ++i) {
                StringBuilder msg = new StringBuilder("Fix Main.aireTriangle() --> ");
                boolean result = ReflectUtilities.sameResult(msg, classRef, classToTest, "aireTriangle", double.class, double.class, double.class);
                assertTrue(msg.toString(), result);
            }
        } catch (Exception ex) {
            fail("Fix Main.aireTriangle() ");
        }
    }
}
