/*
 * @author : Yvan Maillot (yvan.maillot@uha.fr)
 */
package edu.uha.miage;

import edu.uha.miage.Main;
import caseine.format.javajunit.Grade;
import caseine.reflect.ReflectUtilities;
import caseine.tags.ClassTestPriority;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * @author yvan
 */
@ClassTestPriority(2)
public class MainTest {

    private static final Random R = new Random();

    private static int nextInt(int min, int max) {
        return min + R.nextInt(max + min + 1);
    }

    @Test
    public void testLireUnEntierUnDoubleAuClavierPuisLesAfficherAinsiQueLeurProduit() {
        System.out.println("lireUnEntierUnDoubleAuClavierPuisLesAfficherAinsiQueLeurProduit");
        try {
            for (int j = 0; j < 100; ++j) {
                int i = nextInt(-10, 10);
                double d = nextInt(-5000, 5000) / 100.0;
                String out = ReflectUtilities.getFromMethodTASystemInOut(Main.class, null, "lireUnEntierUnDoubleAuClavierPuisLesAfficherAinsiQueLeurProduit", String.format("%d %f", i, d));
                Pattern pattern = Pattern.compile(".*entier.*" + i + ".*" + ".*double.*" + d + ".*" + ".*produit.*" + (i * d) + ".*");
                assertTrue(out, pattern.matcher(out.replaceAll("[\\s]+", " ")).matches());
            }
        } catch (IOException | NoSuchMethodException | IllegalAccessException | InvocationTargetException ex) {
            fail("Erreur inattendue " + ex.getCause());
        }
    }

    @Test
    @Grade(4.0)
    public void testLireTemperatureEnCelsiusAfficherEnFarenheit() {
        System.out.println("lireTemperatureEnCelsiusAfficherEnFarenheit");
        try {
            for (int c = -100; c < 100; ++c) {
                String out = ReflectUtilities.getFromMethodTASystemInOut(Main.class, null, "lireTemperatureEnCelsiusAfficherEnFarenheit", String.format("%d", c));
                Pattern pattern = Pattern.compile("[^0-9\\-]*(-?[0-9]+).?C[^0-9,.-]+(-?[0-9]*[.,][0-9]*).?F.*$");
                Matcher matcher = pattern.matcher(out.replaceAll("[\\s]+", " "));
                assertTrue(out + "Assurez-vous que les degrés celcius sont affichés comme un entier suivis de C et que les degrés farenheit sont affichés comme un nombre à virgule suivis de F", matcher.find());
                String cs = matcher.group(1);
                try {
                    assertEquals("Ce ne sont pas les degrés celcius saisies : ", c, Integer.parseInt(cs));
                } catch (NumberFormatException ex) {
                    fail("Ce sont pas les degrés celcius saisies : " + cs);
                }
                String fs = matcher.group(2).replaceFirst(",", ".");
                double f = (1.8 * c + 32);
                try {
                    assertEquals("Erreur de calcul : ", f, Double.parseDouble(fs), 1e-05);
                } catch (NumberFormatException ex) {
                    fail("Erreur de calcul : " + fs);
                }
            }
        } catch (IOException | NoSuchMethodException | IllegalAccessException | InvocationTargetException ex) {
            fail("Erreur inattendue " + ex.getCause());
        }
    }

    @Test
    @Grade(8.0)
    public void testLireTemperatureEnFarenheitAfficherEnCelsius() {
        System.out.println("lireTemperatureEnFarenheitAfficherEnCelsius");
        try {
            for (int f = -100; f < 100; ++f) {
                String out = ReflectUtilities.getFromMethodTASystemInOut(Main.class, null, "lireTemperatureEnFarenheitAfficherEnCelsius", String.format("%d", f));
                Pattern pattern = Pattern.compile("[^0-9-]*(-?[0-9]+).?F[^0-9,.-]+(-?[0-9]*[.,][0-9]*).?C.*$");
                Matcher matcher = pattern.matcher(out.replaceAll("[\\s]+", " "));
                assertTrue(out + "Assurez-vous que les degrés celcius sont affichés comme un entier suivis de C et que les degrés farenheit sont affichés comme un nombre à virgule suivis de F", matcher.find());
                String fs = matcher.group(1);
                try {
                    assertEquals("Ce ne sont pas les degrés farenheit saisies : ", f, Integer.parseInt(fs));
                } catch (NumberFormatException ex) {
                    fail("Ce sont pas les degrés farenheit saisies : " + fs);
                }
                String cs = matcher.group(2).replaceFirst(",", ".");
                double c = (5 * (f - 32.0) / 9.0);
                try {
                    assertEquals("Erreur de calcul : ", c, Double.parseDouble(cs), 1e-05);
                } catch (NumberFormatException ex) {
                    fail("Erreur de calcul : " + cs);
                }
            }
        } catch (IOException | NoSuchMethodException | IllegalAccessException | InvocationTargetException ex) {
            fail("Erreur inattendue " + ex.getCause());
        }
    }

    @Test
    @Grade(4.0)
    public void testLireLeRayonDunCercleAuClavierEtAfficherSonAireEtSonPerimetre() {
        System.out.println("lireLeRayonDunCercleAuClavierEtAfficherSonAireEtSonPerimetre");
        try {
            for (int j = 0; j < 100; ++j) {
                double r = nextInt(-100, 100) / 10.0;
                String out = ReflectUtilities.getFromMethodTASystemInOut(Main.class, null, "lireLeRayonDunCercleAuClavierEtAfficherSonAireEtSonPerimetre", String.format("%f", r));
                Pattern pattern = Pattern.compile("[aA]ire[^0-9,.-]*(-?[0-9]*[.,]?[0-9]*).*[pP].rim.tre[^0-9,.-]+(-?[0-9]*[.,]?[0-9]*)");
                Matcher matcher = pattern.matcher(out.replaceAll("[\\s]+", " "));
                assertTrue(out + "Assurez-vous que l'aire calculée est précédée du mot aire et que le périmètre calculé est précédée du mot périmètre", matcher.find());
                String aireAsString = matcher.group(1);
                try {
                    assertEquals("Mauvais calcul de l'aire : ", Math.PI * r * r, Double.parseDouble(aireAsString), 1e-5);
                } catch (NumberFormatException ex) {
                    fail("Mauvais calcul de l'aire : " + aireAsString);
                }
                String perimetreAsString = matcher.group(2);
                try {
                    assertEquals("Mauvais calcul du périmètre : ", 2 * Math.PI * r, Double.parseDouble(perimetreAsString), 1e-5);
                } catch (NumberFormatException ex) {
                    fail("Mauvais calcul du périmètre : " + aireAsString);
                }
            }
        } catch (IOException | NoSuchMethodException | IllegalAccessException | InvocationTargetException ex) {
            fail("Erreur inattendue " + ex.getCause());
        }
    }
}
