package edu.uha.miage;

import java.lang.reflect.*;
import org.junit.*;
import static org.junit.Assert.*;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;
import caseine.reflect.ReflectUtilities;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.MethodOrderer;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@TestMethodOrder(MethodOrderer.MethodName.class)
public class GeneratedQuelquesExpressionsBooleennesTest {

    @Test
    @caseine.format.javajunit.Grade(20.0)
    public void p000000000_checkMethodQuelquesExpressionsBooleennesestMinusculechar() {
        System.out.println("estMinuscule");
        try {
            Class<?> theClass = ReflectUtilities.parseType("edu.uha.miage.QuelquesExpressionsBooleennes");
            Method x = theClass.getDeclaredMethod("estMinuscule", ReflectUtilities.parseType("char"));
            assertTrue("Fix estMinuscule (modifiers)", Modifier.isPrivate(9) == Modifier.isPrivate(x.getModifiers()));
            assertTrue("Fix estMinuscule (modifiers)", Modifier.isProtected(9) == Modifier.isProtected(x.getModifiers()));
            assertTrue("Fix estMinuscule (modifiers)", Modifier.isPublic(9) == Modifier.isPublic(x.getModifiers()));
            assertTrue("Fix estMinuscule (modifiers)", Modifier.isStatic(9) == Modifier.isStatic(x.getModifiers()));
            assertTrue("Fix estMinuscule (Return)", x.getReturnType().equals(ReflectUtilities.parseType("boolean")));
        } catch (NoSuchMethodException | SecurityException ex) {
            fail("Fix estMinuscule");
        } catch (ClassNotFoundException ex3) {
            fail("Unknown class " + ex3.toString());
        } catch (Exception ex2) {
            fail("Unexpected Error " + ex2.toString());
        }
    }
}
