/*
 * @author : Yvan Maillot (yvan.maillot@uha.fr)
 */
package cf.edu.uha.miage;

import caseine.tags.RelativeEvaluation;
import caseine.tags.ToCheck;
import caseine.tags.ToDoIn;

/**
 * Écrivez le contenu des méthodes de cette classe pour qu'elles remplissent
 * les contraintes demandées dans leur JavaDoc, en UNE SEULE INSTRUCTION.
 * @author yvan
 */
@RelativeEvaluation
public class QuelquesExpressionsBooleennes {

    /**
     * Méthode qui retourne vrai si son paramètre c est une lettre de l'alphabet
     * latin sans accent et minuscule. Dans le cas contraire elle retourne faux.
     *
     * Le "défi" est de le faire en une seule instruction : le "return"
     *
     * @param c un caractère quelconque
     * @return vrai si c est une lettre sans accent minuscule, faux sinon.
     */
    @ToDoIn("1\n\t\tÉcrire estMinuscule qui retourne vrai" + "\n\t\tsi c est une lettre minuscule" + "\n\t\tet faux sinon." + "\n\t\tPour simplifier l'exercice, les caractères accentués" + "\n\t\tne sont pas considérés comme minuscule\n\t")
    @ToCheck(grade = 1)
    public static boolean estMinuscule(char c) {
        return c >= 'a' && c <= 'z';
    }

    /**
     * Méthode qui retourne vrai si son paramètre c n'est pas un chiffre
     * décimal, faux sinon.
     *
     * Le "défi" est de le faire en une seule instruction : le "return"
     *
     * @param c un caractère quelconque
     * @return vrai si c n'est pas un chiffre décimal, faux sinon.
     */
    @ToDoIn("2\n\t\tÉcrire nEstPasUnChiffreDecimal qui retourne vrai" + "\n\t\tsi c n'est pas un chiffre décimal" + "\n\t\tet faux sinon.\n\t")
    public static boolean nEstPasUnChiffreDecimal(char c) {
        return c < '0' || c > '9';
    }

    /**
     * Méthode qui retourne vrai si son paramètre c est une lettre de l'alphabet
     * latin sans accent.
     *
     * Le "défi" est de le faire en une seule instruction : le "return"
     *
     * @param c un caractère quelconque
     * @return vrai si c une lettre de l'alphabet latin sans accent, faux sinon.
     */
    @ToDoIn("3\n\t\tÉcrire estUneLettreLatineSansAccent qui retourne vrai" + "\n\t\tsi c est une lettre de l'alphabet latin" + "\n\t\ten majuscule ou en minuscule mais sans les " + "\n\t\tmais sans les caractères accentués.\n\t")
    public static boolean estUneLettreLatineSansAccent(char c) {
        return c >= 'a' && c <= 'z' || c >= 'A' && c <= 'Z';
    }

    /**
     * Méthode qui retourne vrai si son paramètre i est un entier naturel impair
     * strictement inférieur à 5. Faux sinon.
     *
     * Le "défi" est de le faire en une seule instruction : le "return"
     *
     * @param i un entier quelconque
     * @return vrai si i est un entier naturel impair strictement inférieur à 5.
     */
    @ToDoIn("4\n\t\tÉcrire estUnEntierNaturelImpairStrictementPlusPetitQue5" + "\n\t\tqui retourne vrai si son paramètre i" + "\n\t\test un entier naturel impair, tel que i < 5\n\t")
    public static boolean estUnEntierNaturelImpairStrictementPlusPetitQue5(int i) {
        return i % 2 != 0 && i >= 0 && i < 5;
    }

    /**
     * Méthode qui retourne vrai si son paramètre i est un entier impair
     * strictement inférieur à 54. Faux sinon.
     *
     * Le "défi" est de le faire en une seule instruction : le "return"
     *
     * @param i un entier quelconque
     * @return vrai si i est un entier impair strictement inférieur à 54.
     */
    @ToDoIn("5\n\t\tÉcrire estUnEntierImpairStrictementPlusPetitQue54" + "\n\t\tqui retourne vrai si son paramètre i" + "\n\t\test un entier impair, tel que i < 54\n\t")
    public static boolean estUnEntierImpairStrictementPlusPetitQue54(int i) {
        return i % 2 != 0 && i < 54;
    }

    /**
     * Méthode qui retourne vrai si son paramètre i est un entier impair. Faux sinon.
     *
     * Le "défi" est de le faire en une seule instruction : le "return"
     *
     * @param i un entier quelconque
     * @return vrai si i est impair.
     */
    @ToDoIn("6\n\t\tÉcrire estImpair" + "\n\t\tqui retourne vrai si son paramètre i" + "\n\t\test un entier impair\n\t")
    public static boolean estUnEntierImpair(int i) {
        return i % 2 != 0;
    }
}
