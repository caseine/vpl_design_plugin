/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.uha.miage;

/**
 * @author yvan
 */
public class Main {

    public static void main(String[] args) {
        // Ecrire ici vos tests personnels
        System.out.println("Ecrire ici vos tests personnels");
        // Par exemple
        char c = 'x';
        if (QuelquesExpressionsBooleennes.estMinuscule('x')) {
            System.out.println("OK : " + c + " est bien une minuscule.");
        } else {
            System.err.println("FAUX : " + c + " devrait être une minuscule.");
        }
        c = 'X';
        if (QuelquesExpressionsBooleennes.estMinuscule(c)) {
            System.err.println("FAUX : " + c + " ne devrait pas être une minuscule.");
        } else {
            System.err.println("OK : " + c + " n'est pas une minuscule.");
        }
    }
}
