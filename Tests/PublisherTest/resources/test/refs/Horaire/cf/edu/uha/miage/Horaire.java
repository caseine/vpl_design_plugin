/*
 * @author : Yvan Maillot (yvan.maillot@uha.fr)
 */
package edu.uha.miage;

/**
 * Une classe horaire pour gérer des horaires (heures, minutes, secondes).
 *
 * Les horaires appartiennent à l'un des deux formats 24h ou 12h
 *
 * Le format 24h signifie que les heures sont dans [0, 23] Le format 12h
 * signifie que les heures sont dans [0, 11]
 *
 * La responsabilité de cette classe est de garantir un horaire cohérent,
 *
 * c'est-à-dire
 *
 * 1. dont les secondes sont dans [0, 59]
 *
 * 2. dont les minutes sont dans [0, 59]
 *
 * 2. dont les heures sont dans [0, 11] au format 12h ou dans [0,23] au format
 * 24h
 *
 * @author yvan
 */
public class Horaire {

    private final boolean LOCAL = true;

    private int h, m, s;

    private boolean h24;

    /**
     * Construit un nouvel horaire à partir des arguments.
     *
     * @param h24 vrai =&gt; format 24h, faux =&gt; format 12h
     *
     * @param h les heures de cet horaire. Si h n'est pas dans l'intervalle
     * requis, il est considéré égal à 0.
     *
     * @param m les minutes de cet horaire. Si m n'est pas dans [0, 59], il est
     * considéré égal à 0.
     *
     * @param s les secondes de cet horaire. Si s n'est pas dans [0, 59], il est
     * considéré égal à 0.
     */
    public Horaire(boolean h24, int h, int m, int s) {
        if (s < 0 || s > 59) {
            s = 0;
        }
        if (m < 0 || m > 59) {
            m = 0;
        }
        if (h < 0 || h > (h24 ? 23 : 11)) {
            h = 0;
        }
        this.h = h;
        this.m = m;
        this.s = s;
        this.h24 = h24;
    }

    /**
     * Construit un nouvel horaire à partir des arguments (les secondes sont
     * nulles).
     *
     * @param h24 vrai =&gt; format 24h, faux =&gt; format 12h
     *
     * @param h les heures de cet horaire. Si h n'est pas dans l'intervalle
     * requis, il est considéré égal à 0.
     *
     * @param m les minutes de cet horaire. Si m n'est pas dans [0, 59], il est
     * considéré égal à 0.
     */
    public Horaire(boolean h24, int h, int m) {
        this(h24, h, m, 0);
    }

    /**
     * Construit un nouvel horaire à partir des arguments (les secondes et les
     * minutes sont nulles).
     *
     * @param h24 vrai =&gt; format 24h, faux =&gt; format 12h
     *
     * @param h les heures de cet horaire. Si h n'est pas dans l'intervalle
     * requis, il est considéré égal à 0.
     */
    public Horaire(boolean h24, int h) {
        this(h24, h, 0, 0);
    }

    /**
     * Construit l'horaire 00:00:00 au format 24h ou 12h.
     *
     * @param h24 vrai =&gt; format 24h, faux =&gt; format 12h
     */
    public Horaire(boolean h24) {
        this(h24, 0, 0, 0);
    }

    /**
     * Construit un nouvel horaire au format 24h à partir des arguments.
     *
     * @param h les heures de cet horaire. Si h n'est pas dans l'intervalle
     * requis, il est considéré égal à 0.
     *
     * @param m les minutes de cet horaire. Si m n'est pas dans [0, 59], il est
     * considéré égal à 0.
     *
     * @param s les secondes de cet horaire. Si s n'est pas dans [0, 59], il est
     * considéré égal à 0.
     */
    public Horaire(int h, int m, int s) {
        this(true, h, m, s);
    }

    /**
     * Construit un nouvel horaire au format 24h à partir des arguments (les
     * secondes sont nulles).
     *
     * @param h les heures de cet horaire. Si h n'est pas dans l'intervalle
     * requis, il est considéré égal à 0.
     *
     * @param m les minutes de cet horaire. Si m n'est pas dans [0, 59], il est
     * considéré égal à 0.
     */
    public Horaire(int h, int m) {
        this(true, h, m, 0);
    }

    /**
     * Construit un nouvel horaire au format 24h à partir des arguments (les
     * secondes et les minutes sont nulles).
     *
     * @param h les heures de cet horaire. Si h n'est pas dans l'intervalle
     * requis, il est considéré égal à 0.
     */
    public Horaire(int h) {
        this(true, h, 0, 0);
    }

    /**
     * Construit l'horaire 00:00:00 au format 24h .
     */
    public Horaire() {
        this(true, 0, 0, 0);
    }

    /**
     * @return les heures de cet horaire
     */
    public int getHeures() {
        return h;
    }

    /**
     * @return les minutes de cet horaire
     */
    public int getMinutes() {
        return m;
    }

    /**
     * @return les secondes de cet horaire
     */
    public int getSecondes() {
        return s;
    }

    /**
     * @return vrai si format 24h, faux sinon.
     */
    public boolean isH24() {
        return h24;
    }

    /**
     * Met à jour les heures
     * @param heures les heures du nouvel horaire
     */
    public void setHeures(int heures) {
        if (heures >= 0 && heures < (h24 ? 24 : 12)) {
            h = heures;
        }
    }

    /**
     * Met à jour les minutes
     * @param minutes les minutes du nouvel horaire
     */
    public void setMinutes(int minutes) {
        if (minutes >= 0 && minutes < 60) {
            m = minutes;
        }
    }

    /**
     * Met à jour les secondes
     * @param secondes les secondes du nouvel horaire
     */
    public void setSecondes(int secondes) {
        if (secondes >= 0 && secondes < 60) {
            s = secondes;
        }
    }

    /**
     * Met à jour le format 24h ou 12h
     *
     * @param h24 vrai =&gt; format 24h , faux sinon.
     */
    public void setH24(boolean h24) {
        this.h24 = h24;
        if (!h24) {
            h = h % 12;
        }
    }

    /**
     * Retourne un horaire sous la forme d'une chaîne de caractères sous la
     * forme "hh:mm:ss" où hh, mm, ss sont les heures, les minutes et les
     * secondes sur deux chiffres.
     *
     * Par exemple "00:00:00", "02:33:59"
     *
     * Conseil : lire la javadoc de String.format() :
     * https://docs.oracle.com/en/java/javase/12/docs/api/java.base/java/lang/String.html#format(java.lang.String,java.lang.Object...)
     *
     * @return un horaire de la forme "hh:mm:ss"
     */
    @Override
    public String toString() {
        return String.format("%02d:%02d:%02d", h, m, s);
    }

    /**
     * Augmente cet horaire d'une heure.
     */
    public void uneHeureDePlus() {
        ++h;
        if (h > (h24 ? 23 : 11)) {
            h = 0;
        }
    }

    /**
     * Augmente cet horaire d'une minute.
     */
    public void uneMinuteDePlus() {
        ++m;
        if (m > 59) {
            m = 0;
            uneHeureDePlus();
        }
    }

    /**
     * Augmente cet horaire d'une seconde.
     */
    public void uneSecondeDePlus() {
        ++s;
        if (s > 59) {
            s = 0;
            uneMinuteDePlus();
        }
    }
}
