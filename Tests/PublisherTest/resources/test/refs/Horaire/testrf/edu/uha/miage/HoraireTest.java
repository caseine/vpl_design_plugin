/*
 * @author : Yvan Maillot (yvan.maillot@uha.fr)
 */
package edu.uha.miage;

import caseine.format.javajunit.Grade;
import com.github.javaparser.ast.body.ConstructorDeclaration;
import com.github.javaparser.ast.body.MethodDeclaration;
import com.github.javaparser.ast.body.TypeDeclaration;
import com.github.javaparser.ast.comments.Comment;
import com.github.javaparser.ast.stmt.BlockStmt;
import com.github.javaparser.ast.stmt.Statement;
import com.github.javaparser.utils.SourceRoot;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.Modifier;
import java.util.Arrays;
import java.util.Optional;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;

/**
 * @author yvan
 */
public class HoraireTest {

    private static boolean LOCAL;

    public HoraireTest() {
        try {
            Horaire.class.getDeclaredField("LOCAL");
            LOCAL = true;
        } catch (NoSuchFieldException | SecurityException ex) {
            LOCAL = false;
        }
    }

    private static SourceRoot SR;

    private static Optional<?> optTd = null;

    static {
        try {
            SR = new SourceRoot(new File("edu").toPath());
        } catch (Exception ex) {
            SR = new SourceRoot(new File("src/edu").toPath());
        }
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
        try {
            optTd = SR.tryToParse().stream().filter(pr -> pr.isSuccessful()).map(pr -> pr.getResult().get()).filter(cu -> cu.getPrimaryType().isPresent()).map(cu -> cu.getPrimaryType().get()).filter(pt -> "Horaire".equals(pt.getName().asString())).findFirst();
        } catch (IOException ex) {
            fail("Bien relire le cours et/ou l'énoncé");
        }
    }

    private boolean sameParameters(Constructor<?> c, ConstructorDeclaration cd) {
        if (cd.getParameters().size() != c.getParameterCount()) {
            return false;
        } else {
            for (int i = 0; i < c.getParameterCount(); ++i) {
                // System.out.println("c. => " + );
                if (!cd.getParameter(i).getTypeAsString().equals(c.getParameterTypes()[i].toString())) {
                    return false;
                }
            }
            return true;
        }
    }

    private BlockStmt getBody(Constructor<?> c, TypeDeclaration<?> td) {
        Optional<ConstructorDeclaration> opttd = td.getMembers().stream().filter(bd -> bd.isConstructorDeclaration()).map(bd -> bd.asConstructorDeclaration()).filter(cd -> sameParameters(c, cd)).findFirst();
        if (opttd.isPresent()) {
            return opttd.get().getBody();
        } else {
            return null;
        }
    }

    private void assertExplicitConstructorInvocation(Constructor<?> c, String msg) {
        if (optTd.isPresent()) {
            TypeDeclaration<?> td = (TypeDeclaration<?>) optTd.get();
            System.out.println("-----------------------------------------");
            System.out.println(c);
            System.out.println("-----------------------------------------");
            System.out.println(getBody(c, td));
            System.out.println("-----------------------------------------");
            BlockStmt bs = getBody(c, td);
            if (bs != null) {
                assertTrue(msg, bs.getStatements().size() == 1 && bs.getStatement(0).isExplicitConstructorInvocationStmt());
            }
        } else {
            fail("Bien relire le cours et/ou l'énoncé");
        }
    }

    private void assertNotEmptyConstructor(Constructor<?> c, String msg) {
        if (optTd.isPresent()) {
            TypeDeclaration<?> td = (TypeDeclaration<?>) optTd.get();
            BlockStmt bs = getBody(c, td);
            if (bs != null) {
                assertTrue(msg, bs.getStatements().size() > 0);
            } else {
                fail("Bien relire le cours et/ou l'énoncé");
            }
        } else {
            fail("Bien relire le cours et/ou l'énoncé");
        }
    }

    @Test
    @Grade(1)
    public void checkAttributs() {
        System.out.println("check attributs");
        assertTrue("Il manque probablement des attributs ", Arrays.stream(Horaire.class.getDeclaredFields()).count() > 0);
        Arrays.stream(Horaire.class.getDeclaredFields()).forEach(f -> {
            assertTrue("Il ne devrait y avoir que des attributs privés ", Modifier.isPrivate(f.getModifiers()));
        });
    }

    @Test
    @Grade(2)
    public void checkConstructors() {
        System.out.println("check constructors");
        Arrays.stream(Horaire.class.getDeclaredConstructors()).sorted((c1, c2) -> {
            if (c1.getParameterCount() > 0) {
                if (c2.getParameterCount() > 0) {
                    if (c1.getParameterTypes()[0] == boolean.class) {
                        if (c2.getParameterTypes()[0] == boolean.class) {
                            return c2.getParameterCount() - c1.getParameterCount();
                        } else {
                            return -1;
                        }
                    } else {
                        if (c2.getParameterTypes()[0] != boolean.class) {
                            return c2.getParameterCount() - c1.getParameterCount();
                        } else {
                            return 1;
                        }
                    }
                } else {
                    return -1;
                }
            } else {
                return 1;
            }
        }).peek(System.out::println).forEach(c -> this.assertNotEmptyConstructor(c, "Ecrire le constructeur " + c));
    }

    @Test
    @Grade(1)
    public void testGetHeures() {
        System.out.println("getHeures");
        Horaire horaire;
        horaire = new Horaire();
        assertEquals("Revoir le constructeur Horaire() ou getHeures", 0, horaire.getHeures());
        horaire = new Horaire(true);
        assertEquals("Revoir le constructeur Horaire(boolean h24) ou getHeures", 0, horaire.getHeures());
        horaire = new Horaire(false);
        assertEquals("Revoir le constructeur Horaire(boolean h24) ou getHeures", 0, horaire.getHeures());
        horaire = new Horaire(false, -5);
        assertEquals("Revoir le constructeur Horaire(boolean h24, int h) ou getHeures", 0, horaire.getHeures());
        horaire = new Horaire(false, 25);
        assertEquals("Revoir le constructeur Horaire(boolean h24, int h) ou getHeures", 0, horaire.getHeures());
        horaire = new Horaire(true, 25);
        assertEquals("Revoir le constructeur Horaire(boolean h24, int h) ou getHeures", 0, horaire.getHeures());
        horaire = new Horaire(true, 25);
        assertEquals("Revoir le constructeur Horaire(boolean h24, int h) ou getHeures", 0, horaire.getHeures());
        // Sans les heures
        horaire = new Horaire();
        assertEquals("Revoir le constructeur Horaire() ou getHeures", 0, horaire.getHeures());
        horaire = new Horaire(true);
        assertEquals("Revoir le constructeur Horaire(true) ou getHeures", 0, horaire.getHeures());
        horaire = new Horaire(false);
        assertEquals("Revoir le constructeur Horaire(false) ou getHeures", 0, horaire.getHeures());
        // Sans les minutes
        horaire = new Horaire(-1);
        assertEquals("Revoir le constructeur Horaire(-1) ou getHeures", 0, horaire.getHeures());
        horaire = new Horaire(true, -1);
        assertEquals("Revoir le constructeur Horaire(true, -1) ou getHeures", 0, horaire.getHeures());
        horaire = new Horaire(false, -1);
        assertEquals("Revoir le constructeur Horaire(false, -1) ou getHeures", 0, horaire.getHeures());
        horaire = new Horaire(24);
        assertEquals("Revoir le constructeur Horaire(24) ou getHeures", 0, horaire.getHeures());
        horaire = new Horaire(true, 24);
        assertEquals("Revoir le constructeur Horaire(true, 24) ou getHeures", 0, horaire.getHeures());
        horaire = new Horaire(false, 12);
        assertEquals("Revoir le constructeur Horaire(false, 12) ou getHeures", 0, horaire.getHeures());
        horaire = new Horaire(1);
        assertEquals("Revoir le constructeur Horaire(1) ou getHeures", 1, horaire.getHeures());
        horaire = new Horaire(true, 1);
        assertEquals("Revoir le constructeur Horaire(true, 1) ou getHeures", 1, horaire.getHeures());
        horaire = new Horaire(false, 1);
        assertEquals("Revoir le constructeur Horaire(false, 1) ou getHeures", 1, horaire.getHeures());
        // Sans les secondes
        horaire = new Horaire(-1, -1);
        assertEquals("Revoir le constructeur Horaire(-1, -1) ou getHeures", 0, horaire.getHeures());
        horaire = new Horaire(true, -1, -1);
        assertEquals("Revoir le constructeur Horaire(true, -1, -1) ou getHeures", 0, horaire.getHeures());
        horaire = new Horaire(false, -1, -1);
        assertEquals("Revoir le constructeur Horaire(false, -1, -1) ou getHeures", 0, horaire.getHeures());
        horaire = new Horaire(24, 60);
        assertEquals("Revoir le constructeur Horaire(24, 60) ou getHeures", 0, horaire.getHeures());
        horaire = new Horaire(true, 24, 60);
        assertEquals("Revoir le constructeur Horaire(true, 24, 60) ou getHeures", 0, horaire.getHeures());
        horaire = new Horaire(false, 12, 60);
        assertEquals("Revoir le constructeur Horaire(false, 12, 60) ou getHeures", 0, horaire.getHeures());
        horaire = new Horaire(1, 1);
        assertEquals("Revoir le constructeur Horaire(1, 1) ou getHeures", 1, horaire.getHeures());
        horaire = new Horaire(true, 1, 1);
        assertEquals("Revoir le constructeur Horaire(true, 1, 1) ou getHeures", 1, horaire.getHeures());
        horaire = new Horaire(false, 1, 1);
        assertEquals("Revoir le constructeur Horaire(false, 1, 1) ou getHeures", 1, horaire.getHeures());
        // Avec les secondes
        horaire = new Horaire(-1, -1, -1);
        assertEquals("Revoir le constructeur Horaire(-1, -1, -1) ou getHeures", 0, horaire.getHeures());
        horaire = new Horaire(true, -1, -1, -1);
        assertEquals("Revoir le constructeur Horaire(true, -1, -1, -1) ou getHeures", 0, horaire.getHeures());
        horaire = new Horaire(false, -1, -1, -1);
        assertEquals("Revoir le constructeur Horaire(false, -1, -1, -1) ou getHeures", 0, horaire.getHeures());
        horaire = new Horaire(24, 60, 60);
        assertEquals("Revoir le constructeur Horaire(24, 60, 60) ou getHeures", 0, horaire.getHeures());
        horaire = new Horaire(true, 24, 60, 60);
        assertEquals("Revoir le constructeur Horaire(true, 24, 60, 60) ou getHeures", 0, horaire.getHeures());
        horaire = new Horaire(false, 12, 60, 60);
        assertEquals("Revoir le constructeur Horaire(false, 12, 60, 60) ou getHeures", 0, horaire.getHeures());
        for (int h = 0; h < 24; ++h) {
            horaire = new Horaire(h, 1, 1);
            assertEquals(String.format("Revoir le constructeur Horaire(%d, 1, 1) ou getHeures", h), h, horaire.getHeures());
            horaire = new Horaire(true, h, 1, 1);
            assertEquals(String.format("Revoir le constructeur Horaire(%d, 1, 1) ou getHeures", h), h, horaire.getHeures());
        }
        for (int h = 0; h < 12; ++h) {
            horaire = new Horaire(false, h, 1, 1);
            assertEquals(String.format("Revoir le constructeur Horaire(%d, 1, 1) ou getHeures", h), h, horaire.getHeures());
        }
    }

    @Test
    @Grade(1)
    public void testGetMinutes() {
        System.out.println("getMinutes");
        Horaire h;
        // Sans les heures
        h = new Horaire();
        assertEquals("Revoir le constructeur Horaire() ou getMinutes", 0, h.getMinutes());
        h = new Horaire(true);
        assertEquals("Revoir le constructeur Horaire(true) ou getMinutes", 0, h.getMinutes());
        h = new Horaire(false);
        assertEquals("Revoir le constructeur Horaire(false) ou getMinutes", 0, h.getMinutes());
        // Sans les minutes
        h = new Horaire(-1);
        assertEquals("Revoir le constructeur Horaire(-1) ou getMinutes", 0, h.getMinutes());
        h = new Horaire(true, -1);
        assertEquals("Revoir le constructeur Horaire(true, -1) ou getMinutes", 0, h.getMinutes());
        h = new Horaire(false, -1);
        assertEquals("Revoir le constructeur Horaire(false, -1) ou getMinutes", 0, h.getMinutes());
        h = new Horaire(24);
        assertEquals("Revoir le constructeur Horaire(24) ou getMinutes", 0, h.getMinutes());
        h = new Horaire(true, 24);
        assertEquals("Revoir le constructeur Horaire(true, 24) ou getMinutes", 0, h.getMinutes());
        h = new Horaire(false, 12);
        assertEquals("Revoir le constructeur Horaire(false, 12) ou getMinutes", 0, h.getMinutes());
        h = new Horaire(1);
        assertEquals("Revoir le constructeur Horaire(1) ou getMinutes", 0, h.getMinutes());
        h = new Horaire(true, 1);
        assertEquals("Revoir le constructeur Horaire(true, 1) ou getMinutes", 0, h.getMinutes());
        h = new Horaire(false, 1);
        assertEquals("Revoir le constructeur Horaire(false, 1) ou getMinutes", 0, h.getMinutes());
        // Sans les secondes
        h = new Horaire(-1, -1);
        assertEquals("Revoir le constructeur Horaire(-1, -1) ou getMinutes", 0, h.getMinutes());
        h = new Horaire(true, -1, -1);
        assertEquals("Revoir le constructeur Horaire(true, -1, -1) ou getMinutes", 0, h.getMinutes());
        h = new Horaire(false, -1, -1);
        assertEquals("Revoir le constructeur Horaire(false, -1, -1) ou getMinutes", 0, h.getMinutes());
        h = new Horaire(24, 60);
        assertEquals("Revoir le constructeur Horaire(24, 60) ou getMinutes", 0, h.getMinutes());
        h = new Horaire(true, 24, 60);
        assertEquals("Revoir le constructeur Horaire(true, 24, 60) ou getMinutes", 0, h.getMinutes());
        h = new Horaire(false, 12, 60);
        assertEquals("Revoir le constructeur Horaire(false, 12, 60) ou getMinutes", 0, h.getMinutes());
        h = new Horaire(1, 1);
        assertEquals("Revoir le constructeur Horaire(1, 1) ou getMinutes", 1, h.getMinutes());
        h = new Horaire(true, 1, 1);
        assertEquals("Revoir le constructeur Horaire(true, 1, 1) ou getMinutes", 1, h.getMinutes());
        h = new Horaire(false, 1, 1);
        assertEquals("Revoir le constructeur Horaire(false, 1, 1) ou getMinutes", 1, h.getMinutes());
        // Avec les secondes
        h = new Horaire(-1, -1, -1);
        assertEquals("Revoir le constructeur Horaire(-1, -1, -1) ou getMinutes", 0, h.getMinutes());
        h = new Horaire(true, -1, -1, -1);
        assertEquals("Revoir le constructeur Horaire(true, -1, -1, -1) ou getMinutes", 0, h.getMinutes());
        h = new Horaire(false, -1, -1, -1);
        assertEquals("Revoir le constructeur Horaire(false, -1, -1, -1) ou getMinutes", 0, h.getMinutes());
        h = new Horaire(24, 60, 60);
        assertEquals("Revoir le constructeur Horaire(24, 60, 60) ou getMinutes", 0, h.getMinutes());
        h = new Horaire(true, 24, 60, 60);
        assertEquals("Revoir le constructeur Horaire(true, 24, 60, 60) ou getMinutes", 0, h.getMinutes());
        h = new Horaire(false, 12, 60, 60);
        assertEquals("Revoir le constructeur Horaire(false, 12, 60, 60) ou getMinutes", 0, h.getMinutes());
        for (int m = 0; m < 60; ++m) {
            h = new Horaire(1, m, 1);
            assertEquals(String.format("Revoir le constructeur Horaire(1, %d, 1) ou getMinutes", m), m, h.getMinutes());
            h = new Horaire(true, 1, m, 1);
            assertEquals(String.format("Revoir le constructeur Horaire(1, %d, 1) ou getMinutes", m), m, h.getMinutes());
            h = new Horaire(false, 1, m, 1);
            assertEquals(String.format("Revoir le constructeur Horaire(1, %d, 1) ou getMinutes", m), m, h.getMinutes());
        }
    }

    @Test
    @Grade(1)
    public void testGetSecondes() {
        System.out.println("getSecondes");
        Horaire h;
        // Sans les heures
        h = new Horaire();
        assertEquals("Revoir le constructeur Horaire() ou getSecondes", 0, h.getSecondes());
        h = new Horaire(true);
        assertEquals("Revoir le constructeur Horaire(true) ou getSecondes", 0, h.getSecondes());
        h = new Horaire(false);
        assertEquals("Revoir le constructeur Horaire(false) ou getSecondes", 0, h.getSecondes());
        // Sans les minutes
        h = new Horaire(-1);
        assertEquals("Revoir le constructeur Horaire(-1) ou getSecondes", 0, h.getSecondes());
        h = new Horaire(true, -1);
        assertEquals("Revoir le constructeur Horaire(true, -1) ou getSecondes", 0, h.getSecondes());
        h = new Horaire(false, -1);
        assertEquals("Revoir le constructeur Horaire(false, -1) ou getSecondes", 0, h.getSecondes());
        h = new Horaire(24);
        assertEquals("Revoir le constructeur Horaire(24) ou getSecondes", 0, h.getSecondes());
        h = new Horaire(true, 24);
        assertEquals("Revoir le constructeur Horaire(true, 24) ou getSecondes", 0, h.getSecondes());
        h = new Horaire(false, 12);
        assertEquals("Revoir le constructeur Horaire(false, 12) ou getSecondes", 0, h.getSecondes());
        h = new Horaire(1);
        assertEquals("Revoir le constructeur Horaire(1) ou getSecondes", 0, h.getSecondes());
        h = new Horaire(true, 1);
        assertEquals("Revoir le constructeur Horaire(true, 1) ou getSecondes", 0, h.getSecondes());
        h = new Horaire(false, 1);
        assertEquals("Revoir le constructeur Horaire(false, 1) ou getSecondes", 0, h.getSecondes());
        // Sans les secondes
        h = new Horaire(-1, -1);
        assertEquals("Revoir le constructeur Horaire(-1, -1) ou getSecondes", 0, h.getSecondes());
        h = new Horaire(true, -1, -1);
        assertEquals("Revoir le constructeur Horaire(true, -1, -1) ou getSecondes", 0, h.getSecondes());
        h = new Horaire(false, -1, -1);
        assertEquals("Revoir le constructeur Horaire(false, -1, -1) ou getSecondes", 0, h.getSecondes());
        h = new Horaire(24, 60);
        assertEquals("Revoir le constructeur Horaire(24, 60) ou getSecondes", 0, h.getSecondes());
        h = new Horaire(true, 24, 60);
        assertEquals("Revoir le constructeur Horaire(true, 24, 60) ou getSecondes", 0, h.getSecondes());
        h = new Horaire(false, 12, 60);
        assertEquals("Revoir le constructeur Horaire(false, 12, 60) ou getSecondes", 0, h.getSecondes());
        h = new Horaire(1, 1);
        assertEquals("Revoir le constructeur Horaire(1, 1) ou getSecondes", 0, h.getSecondes());
        h = new Horaire(true, 1, 1);
        assertEquals("Revoir le constructeur Horaire(true, 1, 1) ou getSecondes", 0, h.getSecondes());
        h = new Horaire(false, 1, 1);
        assertEquals("Revoir le constructeur Horaire(false, 1, 1) ou getSecondes", 0, h.getSecondes());
        // Avec les secondes
        h = new Horaire(-1, -1, -1);
        assertEquals("Revoir le constructeur Horaire(-1, -1, -1) ou getSecondes", 0, h.getSecondes());
        h = new Horaire(true, -1, -1, -1);
        assertEquals("Revoir le constructeur Horaire(true, -1, -1, -1) ou getSecondes", 0, h.getSecondes());
        h = new Horaire(false, -1, -1, -1);
        assertEquals("Revoir le constructeur Horaire(false, -1, -1, -1) ou getSecondes", 0, h.getSecondes());
        h = new Horaire(24, 60, 60);
        assertEquals("Revoir le constructeur Horaire(24, 60, 60) ou getSecondes", 0, h.getSecondes());
        h = new Horaire(true, 24, 60, 60);
        assertEquals("Revoir le constructeur Horaire(true, 24, 60, 60) ou getSecondes", 0, h.getSecondes());
        h = new Horaire(false, 12, 60, 60);
        assertEquals("Revoir le constructeur Horaire(false, 12, 60, 60) ou getSecondes", 0, h.getSecondes());
        for (int s = 0; s < 60; ++s) {
            h = new Horaire(1, 1, s);
            assertEquals(String.format("Revoir le constructeur Horaire(1, 1, %d) ou getSecondes", s), s, h.getSecondes());
            h = new Horaire(true, 1, 1, s);
            assertEquals(String.format("Revoir le constructeur Horaire(1, 1, %d) ou getSecondes", s), s, h.getSecondes());
            h = new Horaire(false, 1, 1, s);
            assertEquals(String.format("Revoir le constructeur Horaire(1, 1, %d) ou getSecondes", s), s, h.getSecondes());
        }
    }

    @Test
    @Grade(3)
    public void testSetHeures() {
        System.out.println("setHeures");
        Horaire horaire = new Horaire();
        for (int h = 0; h < 24; ++h) {
            horaire.setHeures(h);
            assertEquals(String.format("Revoir setHeures(%d) ou getHeures()", h), h, horaire.getHeures());
        }
        {
            int h = -1;
            horaire.setHeures(h);
            assertTrue(String.format("Revoir setHeures(%d) ou getHeures()", h), horaire.getHeures() >= 0 && horaire.getHeures() < 24);
            h = 24;
            horaire.setHeures(h);
            assertTrue(String.format("Revoir setHeures(%d) ou getHeures()", h), horaire.getHeures() >= 0 && horaire.getHeures() < 24);
        }
        horaire = new Horaire(false);
        for (int h = 0; h < 12; ++h) {
            horaire.setHeures(h);
            assertEquals(String.format("Revoir setHeures(%d) ou getHeures()", h), h, horaire.getHeures());
        }
        int h = -1;
        horaire.setHeures(h);
        assertTrue(String.format("Revoir setHeures(%d) ou getHeures()", h), horaire.getHeures() >= 0 && horaire.getHeures() < 12);
        h = 12;
        horaire.setHeures(h);
        assertTrue(String.format("Revoir setHeures(%d) ou getHeures()", h), horaire.getHeures() >= 0 && horaire.getHeures() < 12);
    }

    @Test
    @Grade(3)
    public void testSetMinutes() {
        System.out.println("setMinutes");
        Horaire horaire = new Horaire();
        for (int m = 0; m < 60; ++m) {
            horaire.setMinutes(m);
            assertEquals(String.format("Revoir setMinutes(%d) ou getMinutes()", m), m, horaire.getMinutes());
        }
        {
            int m = -1;
            horaire.setMinutes(m);
            assertTrue(String.format("Revoir setMinutes(%d) ou getMinutes()", m), horaire.getMinutes() >= 0 && horaire.getMinutes() < 60);
            m = 60;
            horaire.setMinutes(m);
            assertTrue(String.format("Revoir setMinutes(%d) ou getMinutes()", m), horaire.getMinutes() >= 0 && horaire.getMinutes() < 60);
        }
    }

    @Test
    @Grade(3)
    public void testSetSecondes() {
        System.out.println("setSecondes");
        Horaire horaire = new Horaire();
        for (int s = 0; s < 60; ++s) {
            horaire.setSecondes(s);
            assertEquals(String.format("Revoir setSecondes(%d) ou getSecondes()", s), s, horaire.getSecondes());
        }
        {
            int s = -1;
            horaire.setSecondes(s);
            assertTrue(String.format("Revoir setSecondes(%d) ou getSecondes()", s), horaire.getSecondes() >= 0 && horaire.getSecondes() < 60);
            s = 60;
            horaire.setSecondes(s);
            assertTrue(String.format("Revoir setSecondes(%d) ou getSecondes()", s), horaire.getSecondes() >= 0 && horaire.getSecondes() < 60);
        }
    }

    @Test
    @Grade(1)
    public void testIsH24() {
        System.out.println("isH24");
        Horaire horaire;
        horaire = new Horaire();
        assertTrue("Revoir le constructeur Horaire() ou isH24()", horaire.isH24());
        horaire = new Horaire(true);
        assertTrue("Revoir le constructeur Horaire(boolean h24) ou isH24()", horaire.isH24());
        horaire = new Horaire(false);
        assertFalse("Revoir le constructeur Horaire(boolean h24) ou isH24()", horaire.isH24());
        horaire = new Horaire(false, -5);
        assertFalse("Revoir le constructeur Horaire(boolean h24, int h) ou isH24()", horaire.isH24());
        horaire = new Horaire(false, 25);
        assertFalse("Revoir le constructeur Horaire(boolean h24, int h) ou isH24()", horaire.isH24());
        horaire = new Horaire(true, 25);
        assertTrue("Revoir le constructeur Horaire(boolean h24, int h) ou isH24()", horaire.isH24());
        horaire = new Horaire(true, 25);
        assertTrue("Revoir le constructeur Horaire(boolean h24, int h) ou isH24()", horaire.isH24());
        // Sans les heures
        horaire = new Horaire();
        assertTrue("Revoir le constructeur Horaire() ou isH24()", horaire.isH24());
        horaire = new Horaire(true);
        assertTrue("Revoir le constructeur Horaire(true) ou isH24()", horaire.isH24());
        horaire = new Horaire(false);
        assertFalse("Revoir le constructeur Horaire(false) ou isH24()", horaire.isH24());
        // Sans les minutes
        horaire = new Horaire(-1);
        assertTrue("Revoir le constructeur Horaire(-1) ou isH24()", horaire.isH24());
        horaire = new Horaire(true, -1);
        assertTrue("Revoir le constructeur Horaire(true, -1) ou isH24()", horaire.isH24());
        horaire = new Horaire(false, -1);
        assertFalse("Revoir le constructeur Horaire(false, -1) ou isH24()", horaire.isH24());
        horaire = new Horaire(24);
        assertTrue("Revoir le constructeur Horaire(24) ou isH24()", horaire.isH24());
        horaire = new Horaire(true, 24);
        assertTrue("Revoir le constructeur Horaire(true, 24) ou isH24()", horaire.isH24());
        horaire = new Horaire(false, 12);
        assertFalse("Revoir le constructeur Horaire(false, 12) ou isH24()", horaire.isH24());
        horaire = new Horaire(1);
        assertTrue("Revoir le constructeur Horaire(1) ou isH24()", horaire.isH24());
        horaire = new Horaire(true, 1);
        assertTrue("Revoir le constructeur Horaire(true, 1) ou isH24()", horaire.isH24());
        horaire = new Horaire(false, 1);
        assertFalse("Revoir le constructeur Horaire(false, 1) ou isH24()", horaire.isH24());
        // Sans les secondes
        horaire = new Horaire(-1, -1);
        assertTrue("Revoir le constructeur Horaire(-1, -1) ou isH24()", horaire.isH24());
        horaire = new Horaire(true, -1, -1);
        assertTrue("Revoir le constructeur Horaire(true, -1, -1) ou isH24()", horaire.isH24());
        horaire = new Horaire(false, -1, -1);
        assertFalse("Revoir le constructeur Horaire(false, -1, -1) ou isH24()", horaire.isH24());
        horaire = new Horaire(24, 60);
        assertTrue("Revoir le constructeur Horaire(24, 60) ou isH24()", horaire.isH24());
        horaire = new Horaire(true, 24, 60);
        assertTrue("Revoir le constructeur Horaire(true, 24, 60) ou isH24()", horaire.isH24());
        horaire = new Horaire(false, 12, 60);
        assertFalse("Revoir le constructeur Horaire(false, 12, 60) ou isH24()", horaire.isH24());
        horaire = new Horaire(1, 1);
        assertTrue("Revoir le constructeur Horaire(1, 1) ou isH24()", horaire.isH24());
        horaire = new Horaire(true, 1, 1);
        assertTrue("Revoir le constructeur Horaire(true, 1, 1) ou isH24()", horaire.isH24());
        horaire = new Horaire(false, 1, 1);
        assertFalse("Revoir le constructeur Horaire(false, 1, 1) ou isH24()", horaire.isH24());
        // Avec les secondes
        horaire = new Horaire(-1, -1, -1);
        assertTrue("Revoir le constructeur Horaire(-1, -1, -1) ou isH24()", horaire.isH24());
        horaire = new Horaire(true, -1, -1, -1);
        assertTrue("Revoir le constructeur Horaire(true, -1, -1, -1) ou isH24()", horaire.isH24());
        horaire = new Horaire(false, -1, -1, -1);
        assertFalse("Revoir le constructeur Horaire(false, -1, -1, -1) ou isH24()", horaire.isH24());
        horaire = new Horaire(24, 660);
        assertTrue("Revoir le constructeur Horaire(24, 660) ou isH24()", horaire.isH24());
        horaire = new Horaire(true, 24, 660);
        assertTrue("Revoir le constructeur Horaire(true, 24, 660) ou isH24()", horaire.isH24());
        horaire = new Horaire(false, 12, 660);
        assertFalse("Revoir le constructeur Horaire(false, 12, 660) ou isH24()", horaire.isH24());
        for (int h = 0; h < 24; ++h) {
            horaire = new Horaire(h, 1, 1);
            assertTrue(String.format("Revoir le constructeur Horaire(%d, 1, 1) ou isH24()", h), horaire.isH24());
            horaire = new Horaire(true, h, 1, 1);
            assertTrue(String.format("Revoir le constructeur Horaire(%d, 1, 1) ou isH24()", h), horaire.isH24());
        }
        for (int h = 0; h < 12; ++h) {
            horaire = new Horaire(false, h, 1, 1);
            assertFalse(String.format("Revoir le constructeur Horaire(%d, 1, 1) ou isH24()", h), horaire.isH24());
        }
    }

    @Test
    @Grade(5)
    public void testSetH24() {
        System.out.println("setH24");
        Horaire horaire = new Horaire();
        assertTrue("Revoir constructeur Horaire() ou isH24()", horaire.isH24());
        horaire.setH24(false);
        assertFalse("Revoir setH24(false) ou isH24()", horaire.isH24());
        horaire.setH24(true);
        assertTrue("Revoir setH24(true) ou isH24()", horaire.isH24());
        horaire = new Horaire(12);
        assertTrue("Revoir constructeur Horaire(20) ou isH24()", horaire.isH24());
        horaire.setH24(false);
        assertTrue("Revoir setH24(false) : Horaire incohérent", horaire.getHeures() >= 0 && horaire.getHeures() < 12);
    }

    @Test
    @Grade(2)
    public void testToString() {
        Horaire horaire;
        System.out.println("toString");
        for (int h = 0; h < 24; ++h) {
            for (int m = 0; m < 60; ++m) {
                for (int s = 0; s < 60; ++s) {
                    horaire = new Horaire(h, m, s);
                    assertEquals(String.format("Revoir new Horaire(%d, %d, %d) ou toString()", h, m, s), String.format("%02d:%02d:%02d", h, m, s), horaire.toString());
                }
            }
        }
    }

    @Test
    @Grade(4)
    public void testUneHeureDePlus() {
        System.out.println("uneHeureDePlus");
        Horaire horaire;
        horaire = new Horaire(0, 23, 17);
        for (int i = 0; i <= 24; ++i) {
            assertEquals("Revoir uneHeureDePlus ou toString()", String.format("%02d:%02d:%02d", (i % 24), 23, 17), horaire.toString());
            horaire.uneHeureDePlus();
        }
        horaire = new Horaire(false, 0, 23, 17);
        for (int i = 0; i <= 24; ++i) {
            assertEquals("Revoir uneHeureDePlus ou toString()", String.format("%02d:%02d:%02d", (i % 12), 23, 17), horaire.toString());
            horaire.uneHeureDePlus();
        }
    }

    @Test
    @Grade(4)
    public void testUneMinuteDePlus() {
        System.out.println("uneMinuteDePlus");
        Horaire horaire;
        horaire = new Horaire(0, 0, 17);
        for (int i = 0; i < 60; ++i) {
            assertEquals("Revoir uneMinuteDePlus ou toString()", String.format("%02d:%02d:%02d", 0, i, 17), horaire.toString());
            horaire.uneMinuteDePlus();
        }
        for (int i = 0; i < 60; ++i) {
            assertEquals("Revoir uneMinuteDePlus ou toString()", String.format("%02d:%02d:%02d", 1, i, 17), horaire.toString());
            horaire.uneMinuteDePlus();
        }
    }

    @Test
    @Grade(4)
    public void testUneSecondeDePlus() {
        System.out.println("uneSecondeDePlus");
        Horaire horaire;
        horaire = new Horaire();
        for (int i = 0; i < 60; ++i) {
            assertEquals("Revoir uneSecondeDePlus ou toString()", String.format("%02d:%02d:%02d", 0, 0, i), horaire.toString());
            horaire.uneSecondeDePlus();
        }
        for (int i = 0; i < 60; ++i) {
            assertEquals("Revoir uneSecondeDePlus ou toString()", String.format("%02d:%02d:%02d", 0, 1, i), horaire.toString());
            horaire.uneSecondeDePlus();
        }
    }

    @Test
    @Grade(8)
    public void testPrettyConstructors() {
        System.out.println("testPrettyConstructors");
        Arrays.stream(Horaire.class.getDeclaredConstructors()).filter(c -> c.getParameterCount() < 4).sorted((c1, c2) -> {
            if (c1.getParameterCount() > 0) {
                if (c2.getParameterCount() > 0) {
                    if (c1.getParameterTypes()[0] == boolean.class) {
                        if (c2.getParameterTypes()[0] == boolean.class) {
                            return c2.getParameterCount() - c1.getParameterCount();
                        } else {
                            return -1;
                        }
                    } else {
                        if (c2.getParameterTypes()[0] != boolean.class) {
                            return c2.getParameterCount() - c1.getParameterCount();
                        } else {
                            return 1;
                        }
                    }
                } else {
                    return -1;
                }
            } else {
                return 1;
            }
        }).forEach(c -> this.assertExplicitConstructorInvocation(c, "Ce n'est pas la bonne façon d'écrire le constructeur " + c));
    }
}
