package edu.uga.date;

public class Calendrier {

    /**
     * Indique si l'annee passee en parametre est bissextile.
     *
     * Une annee bissextile repond aux criteres suivants :
     * - annee est multiple de 4 (RG01) mais pas de 100 (RG02)
     * - SAUF si annee est multiple de 400 (RG03)
     *
     * REGLES : RG01, RG02, RG03
     *
     * @param annee
     * @return true si l'annee est bissextile et false sinon.
     */
    public boolean isBissextile(final int annee) {
        // TODO
        return false;
    }
}
