package edu.uga.date;

import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class CalendrierTest {

    private Calendrier calc;

    @BeforeEach
    public void setUp() throws Exception {
        calc = new Calendrier();
    }

    /**
     * Test si l'annee est bissextile.
     *
     * REGLE : RG01
     *
     * annee : 2016
     * bissextile : oui
     */
    @Test
    public void testMultipleDe4_RG01() {
    // TODO
    }

    /**
     * Test si l'annee est bissextile.
     *
     * REGLE : RG02
     *
     * annee : 1900
     * bissextile : non
     */
    @Test
    public void testMultipleDe100_RG02() {
    // TODO
    }

    /**
     * Test si l'annee est bissextile.
     *
     * REGLE : RG03
     *
     * annee : 2000
     * bissextile : oui
     */
    @Test
    public void testMultipleDe400_RG03() {
    // TODO
    }

    /**
     * Test si l'annee est bissextile.
     *
     * REGLE : RG01
     *
     * annee : 2014
     * bissextile : non
     */
    @Test
    public void testNonBissextile2014_RG01() {
    // TODO
    }

    /**
     * Test si l'annee est bissextile.
     *
     * REGLE : RG01
     *
     * annee : 2015
     * bissextile : non
     */
    @Test
    public void testNonBissextile2015_RG01() {
    // TODO
    }
}
