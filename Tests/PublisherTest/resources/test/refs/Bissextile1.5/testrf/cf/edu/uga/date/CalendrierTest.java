package cf.edu.uga.date;

import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import caseine.tags.ToDo;
import caseine.tags.ToDoIn;

public class CalendrierTest {

    private Calendrier calc;

    @BeforeEach
    public void setUp() throws Exception {
        calc = new Calendrier();
    }

    /**
     * Test si l'annee est bissextile.
     *
     * REGLE : RG01
     *
     * annee : 2016
     * bissextile : oui
     */
    @Test
    @ToDoIn
    public void testMultipleDe4_RG01() {
        // Arrange
        final int annee = 2016;
        final boolean expected = true;
        // Act et Assert
        testBissextile(annee, expected);
    }

    /**
     * Test si l'annee est bissextile.
     *
     * REGLE : RG02
     *
     * annee : 1900
     * bissextile : non
     */
    @Test
    @ToDoIn
    public void testMultipleDe100_RG02() {
        // Arrange
        final int annee = 1900;
        final boolean expected = false;
        // Act et Assert
        testBissextile(annee, expected);
    }

    /**
     * Test si l'annee est bissextile.
     *
     * REGLE : RG03
     *
     * annee : 2000
     * bissextile : oui
     */
    @Test
    @ToDoIn
    public void testMultipleDe400_RG03() {
        // Arrange
        final int annee = 2000;
        final boolean expected = true;
        // Act et Assert
        testBissextile(annee, expected);
    }

    /**
     * Test si l'annee est bissextile.
     *
     * REGLE : RG01
     *
     * annee : 2014
     * bissextile : non
     */
    @Test
    @ToDoIn
    public void testNonBissextile2014_RG01() {
        // Arrange
        final int annee = 2014;
        final boolean expected = false;
        // Act et Assert
        testBissextile(annee, expected);
    }

    /**
     * Test si l'annee est bissextile.
     *
     * REGLE : RG01
     *
     * annee : 2015
     * bissextile : non
     */
    @Test
    @ToDoIn
    public void testNonBissextile2015_RG01() {
        // Arrange
        final int annee = 2015;
        final boolean expected = false;
        // Act et Assert
        testBissextile(annee, expected);
    }

    @ToDo
    private void testBissextile(final int annee, final boolean expected) {
        // Act
        final boolean bissextile = calc.isBissextile(annee);
        // Assert
        assertEquals(expected, bissextile);
    }
}
