import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import org.junit.jupiter.api.Test;
import caseine.extra.utils.mutations.DynamicTestExecutor;
import caseine.extra.utils.mutations.DynamicTestExecutorImpl5;

public class TestCalendrierTest {

    private static final String ERROR_NOT_DETECTED = "MUTANT SURVIVED -> ";

    @Test
    public void nominal_test() throws Exception {
        DynamicTestExecutor dte = new DynamicTestExecutorImpl5("edu.uga.date.CalendrierTest", null);
        assertFalse(dte.doTest(), "The code MUST be implemented");
    }

    @Test
    public void kill_mutant_01() throws Exception {
        DynamicTestExecutor dte = new DynamicTestExecutorImpl5("edu.uga.date.CalendrierTest", null);
        assertFalse(dte.doTest(), "The code MUST be implemented");
        dte = new DynamicTestExecutorImpl5("edu.uga.date.CalendrierTest", "mutant_01");
        assertTrue(dte.doTest(), ERROR_NOT_DETECTED + " RG01");
    }

    @Test
    public void kill_mutant_02() throws Exception {
        DynamicTestExecutor dte = new DynamicTestExecutorImpl5("edu.uga.date.CalendrierTest", null);
        assertFalse(dte.doTest(), "The code MUST be implemented");
        dte = new DynamicTestExecutorImpl5("edu.uga.date.CalendrierTest", "mutant_02");
        assertTrue(dte.doTest(), ERROR_NOT_DETECTED + " RG02");
    }

    @Test
    public void kill_mutant_03() throws Exception {
        DynamicTestExecutor dte = new DynamicTestExecutorImpl5("edu.uga.date.CalendrierTest", null);
        assertFalse(dte.doTest(), "The code MUST be implemented");
        dte = new DynamicTestExecutorImpl5("edu.uga.date.CalendrierTest", "mutant_03");
        assertTrue(dte.doTest(), ERROR_NOT_DETECTED + " RG03");
    }
}
