/**
 * A class representing a triplet of three integers
 * @author hadrien cambazard
 */
public class IntegerTriplet {

    private int a;

    private int b;

    private int c;

    /**
     * Constructor using three values
     *
     * @param a the first integer
     * @param b the second integer
     * @param c the third integer
     */
    public IntegerTriplet(int a, int b, int c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }

    /**
     * @return the sum of the elements in the set
     */
    public int sum() {
        // TODO Compute and return the sum of the elements in the set
        return 0;
    }

    /**
     * @return the average value of the set
     */
    public double average() {
        // TODO Compute and return the average value of the set
        return 0;
    }

    /**
     * @return the string obtained by concatenating the three integers
     */
    public String concatenate() {
        // TODO Compute and return the string obtained by concatenating the three integers
        return null;
    }
}
