import java.lang.reflect.*;
import org.junit.*;
import static org.junit.Assert.*;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;
import caseine.reflect.ReflectUtilities;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.MethodOrderer;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@TestMethodOrder(MethodOrderer.MethodName.class)
public class GeneratedClasseAutonomeTest {

    @Test
    @caseine.format.javajunit.Grade(10.0)
    public void p000001000_checkClassClasseAutonome() {
        System.out.println("check class ClasseAutonome");
        try {
            assertTrue("??? (Inheritance)", ReflectUtilities.parseType("ClasseAutonome").getSuperclass().equals(ReflectUtilities.parseType("java.lang.Object")));
            Class<?> x = ReflectUtilities.parseType("ClasseAutonome");
            assertTrue("Fix ??? (modifiers)", Modifier.isPrivate(1) == Modifier.isPrivate(x.getModifiers()));
            assertTrue("Fix ??? (modifiers)", Modifier.isProtected(1) == Modifier.isProtected(x.getModifiers()));
            assertTrue("Fix ??? (modifiers)", Modifier.isPublic(1) == Modifier.isPublic(x.getModifiers()));
            assertTrue("Fix ??? (modifiers)", Modifier.isStatic(1) == Modifier.isStatic(x.getModifiers()));
        } catch (ClassNotFoundException ex) {
            fail("???");
        }
    }

    @Test
    @caseine.format.javajunit.Grade(10.0)
    public void p000002000_checkMethodClasseAutonomemainStringArray() {
        System.out.println("votre classe. Elle n'est pas autonome");
        try {
            Class<?> theClass = ReflectUtilities.parseType("ClasseAutonome");
            Method x = theClass.getDeclaredMethod("main", ReflectUtilities.parseType("[Ljava.lang.String;"));
            assertTrue("Fix votre classe. Elle n'est pas autonome (modifiers)", Modifier.isPrivate(9) == Modifier.isPrivate(x.getModifiers()));
            assertTrue("Fix votre classe. Elle n'est pas autonome (modifiers)", Modifier.isProtected(9) == Modifier.isProtected(x.getModifiers()));
            assertTrue("Fix votre classe. Elle n'est pas autonome (modifiers)", Modifier.isPublic(9) == Modifier.isPublic(x.getModifiers()));
            assertTrue("Fix votre classe. Elle n'est pas autonome (modifiers)", Modifier.isStatic(9) == Modifier.isStatic(x.getModifiers()));
            assertTrue("Fix votre classe. Elle n'est pas autonome (Return)", x.getReturnType().equals(ReflectUtilities.parseType("void")));
        } catch (NoSuchMethodException | SecurityException ex) {
            fail("Fix votre classe. Elle n'est pas autonome");
        } catch (ClassNotFoundException ex3) {
            fail("Unknown class " + ex3.toString());
        } catch (Exception ex2) {
            fail("Unexpected Error " + ex2.toString());
        }
    }
}
