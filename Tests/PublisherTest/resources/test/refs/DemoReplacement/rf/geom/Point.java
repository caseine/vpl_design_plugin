package geom;

public class Point {

    private double x, y;

    // #########################################################################
    // TODO 2. Écrire le constructeur aux coordonnées cartésiennes
    // 
    // TODO 3. Écrire un getter pour x
    // 
    // TODO 4. Écrire un getter pour y
    // 
    // TODO 5. Écrire un setter pour x
    // 
    // TODO 6. Écrire un setter pour y
    // 
    // TODO 7. public toString() qui retourne par exemple (2.5, 3.2)
    // 
    // TODO 8. Ici, equals() est mal écrit. Le réécrire comme vous l'avez appris en cours.
    public boolean equals(Point p) {
        return x == p.x && y == p.y;
    }
}
