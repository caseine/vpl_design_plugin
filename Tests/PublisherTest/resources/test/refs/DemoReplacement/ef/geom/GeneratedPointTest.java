package geom;

import java.lang.reflect.*;
import org.junit.*;
import static org.junit.Assert.*;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;
import caseine.reflect.ReflectUtilities;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.MethodOrderer;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@TestMethodOrder(MethodOrderer.MethodName.class)
public class GeneratedPointTest {

    @Test
    @caseine.format.javajunit.Grade(0.5555555555555556)
    public void p000002000_checkConstructorPointdoubledouble() {
        System.out.println("Declaration of Constructor of geom.Point");
        try {
            Constructor x = ReflectUtilities.parseType("geom.Point").getDeclaredConstructor(ReflectUtilities.parseType("double"), ReflectUtilities.parseType("double"));
            assertTrue("Fix  (modifiers)", Modifier.isPrivate(1) == Modifier.isPrivate(x.getModifiers()));
            assertTrue("Fix  (modifiers)", Modifier.isProtected(1) == Modifier.isProtected(x.getModifiers()));
            assertTrue("Fix  (modifiers)", Modifier.isPublic(1) == Modifier.isPublic(x.getModifiers()));
            assertTrue("Fix  (modifiers)", Modifier.isStatic(1) == Modifier.isStatic(x.getModifiers()));
        } catch (Exception ex) {
            fail("Constructor of geom.Point");
        }
    }

    @Test
    @caseine.format.javajunit.Grade(0.5555555555555556)
    public void p000003000_checkGetterPointX() {
        System.out.println("Check getter x");
        try {
            for (int i = 0; i < 100; ++i) {
                Object o = ReflectUtilities.randomValue(ReflectUtilities.parseType("geom.Point"));
                Object attr = ReflectUtilities.getAttribut(ReflectUtilities.parseType("geom.Point"), o, "x");
                Object getAttr = ReflectUtilities.getFromMethod(ReflectUtilities.parseType("geom.Point"), o, "getX");
                assertTrue("Fix getX", ReflectUtilities.equals(attr, getAttr));
            }
        } catch (Exception ex) {
            fail("Fix getter of x");
        }
    }

    @Test
    @caseine.format.javajunit.Grade(0.5555555555555556)
    public void p000003000_checkGetterPointY() {
        System.out.println("Check getter y");
        try {
            for (int i = 0; i < 100; ++i) {
                Object o = ReflectUtilities.randomValue(ReflectUtilities.parseType("geom.Point"));
                Object attr = ReflectUtilities.getAttribut(ReflectUtilities.parseType("geom.Point"), o, "y");
                Object getAttr = ReflectUtilities.getFromMethod(ReflectUtilities.parseType("geom.Point"), o, "getY");
                assertTrue("Fix getY", ReflectUtilities.equals(attr, getAttr));
            }
        } catch (Exception ex) {
            fail("Fix getter of y");
        }
    }

    @Test
    @caseine.format.javajunit.Grade(0.5555555555555556)
    public void p000004000_checkSetterPointx() {
        System.out.println("Check setter x");
        try {
            Method x = ReflectUtilities.parseType("geom.Point").getDeclaredMethod("setX", ReflectUtilities.parseType("double"));
            assertTrue("Fix setX", x.getReturnType().equals(void.class));
            Object o = ReflectUtilities.randomValue(ReflectUtilities.parseType("geom.Point"));
            for (int i = 0; i < 100; ++i) {
                double v = (double) ReflectUtilities.randomValue(double.class);
                ReflectUtilities.getFromMethodTA(ReflectUtilities.parseType("geom.Point"), o, "setX", ReflectUtilities.parseType("double"), v);
                assertTrue("Fix setX", ReflectUtilities.getAttribut(ReflectUtilities.parseType("geom.Point"), o, "x").equals(v));
            }
        } catch (Exception ex) {
            fail("Fix setX");
        }
    }

    @Test
    @caseine.format.javajunit.Grade(0.5555555555555556)
    public void p000004000_checkSetterPointy() {
        System.out.println("Check setter y");
        try {
            Method x = ReflectUtilities.parseType("geom.Point").getDeclaredMethod("setY", ReflectUtilities.parseType("double"));
            assertTrue("Fix setY", x.getReturnType().equals(void.class));
            Object o = ReflectUtilities.randomValue(ReflectUtilities.parseType("geom.Point"));
            for (int i = 0; i < 100; ++i) {
                double v = (double) ReflectUtilities.randomValue(double.class);
                ReflectUtilities.getFromMethodTA(ReflectUtilities.parseType("geom.Point"), o, "setY", ReflectUtilities.parseType("double"), v);
                assertTrue("Fix setY", ReflectUtilities.getAttribut(ReflectUtilities.parseType("geom.Point"), o, "y").equals(v));
            }
        } catch (Exception ex) {
            fail("Fix setY");
        }
    }

    @Test
    @caseine.format.javajunit.Grade(0.5555555555555556)
    public void p000005000_checkMethodPointtoString() {
        System.out.println("toString");
        try {
            Class<?> theClass = ReflectUtilities.parseType("geom.Point");
            Method x = theClass.getDeclaredMethod("toString");
            assertTrue("Fix toString (modifiers)", Modifier.isPrivate(1) == Modifier.isPrivate(x.getModifiers()));
            assertTrue("Fix toString (modifiers)", Modifier.isProtected(1) == Modifier.isProtected(x.getModifiers()));
            assertTrue("Fix toString (modifiers)", Modifier.isPublic(1) == Modifier.isPublic(x.getModifiers()));
            assertTrue("Fix toString (modifiers)", Modifier.isStatic(1) == Modifier.isStatic(x.getModifiers()));
            assertTrue("Fix toString (Return)", x.getReturnType().equals(ReflectUtilities.parseType("java.lang.String")));
        } catch (NoSuchMethodException | SecurityException ex) {
            fail("Fix toString");
        } catch (ClassNotFoundException ex3) {
            fail("Unknown class " + ex3.toString());
        } catch (Exception ex2) {
            fail("Unexpected Error " + ex2.toString());
        }
    }

    @Test
    @caseine.format.javajunit.Grade(1.1111111111111112)
    public void p000010000_testMethodPointtoString() {
        System.out.println("Test Method Point.toString");
        try {
            Class<?> classRef = cf.geom.Point.class;
            Class<?> classToTest = ReflectUtilities.parseType("geom.Point");
            for (int i = 0; i < 100; ++i) {
                StringBuilder msg = new StringBuilder("Fix Point.toString() --> ");
                boolean result = ReflectUtilities.sameResult(msg, classRef, classToTest, "toString");
                assertTrue(msg.toString(), result);
            }
        } catch (Exception ex) {
            fail("Fix Point.toString() ");
        }
    }

    @Test
    @caseine.format.javajunit.Grade(2.7777777777777777)
    public void p000011000_checkMethodPointequalsObject() {
        System.out.println("equals");
        try {
            Class<?> theClass = ReflectUtilities.parseType("geom.Point");
            Method x = theClass.getDeclaredMethod("equals", ReflectUtilities.parseType("java.lang.Object"));
            assertTrue("Fix equals (modifiers)", Modifier.isPrivate(1) == Modifier.isPrivate(x.getModifiers()));
            assertTrue("Fix equals (modifiers)", Modifier.isProtected(1) == Modifier.isProtected(x.getModifiers()));
            assertTrue("Fix equals (modifiers)", Modifier.isPublic(1) == Modifier.isPublic(x.getModifiers()));
            assertTrue("Fix equals (modifiers)", Modifier.isStatic(1) == Modifier.isStatic(x.getModifiers()));
            assertTrue("Fix equals (Return)", x.getReturnType().equals(ReflectUtilities.parseType("boolean")));
        } catch (NoSuchMethodException | SecurityException ex) {
            fail("Fix equals");
        } catch (ClassNotFoundException ex3) {
            fail("Unknown class " + ex3.toString());
        } catch (Exception ex2) {
            fail("Unexpected Error " + ex2.toString());
        }
    }

    @Test
    @caseine.format.javajunit.Grade(1.1111111111111112)
    public void p000012000_checkMethodPointhashCode() {
        System.out.println("La redéfinition de equals() nécessite encore quelque chose");
        try {
            Class<?> theClass = ReflectUtilities.parseType("geom.Point");
            Method x = theClass.getDeclaredMethod("hashCode");
            assertTrue("Fix La redéfinition de equals() nécessite encore quelque chose (modifiers)", Modifier.isPrivate(1) == Modifier.isPrivate(x.getModifiers()));
            assertTrue("Fix La redéfinition de equals() nécessite encore quelque chose (modifiers)", Modifier.isProtected(1) == Modifier.isProtected(x.getModifiers()));
            assertTrue("Fix La redéfinition de equals() nécessite encore quelque chose (modifiers)", Modifier.isPublic(1) == Modifier.isPublic(x.getModifiers()));
            assertTrue("Fix La redéfinition de equals() nécessite encore quelque chose (modifiers)", Modifier.isStatic(1) == Modifier.isStatic(x.getModifiers()));
            assertTrue("Fix La redéfinition de equals() nécessite encore quelque chose (Return)", x.getReturnType().equals(ReflectUtilities.parseType("int")));
        } catch (NoSuchMethodException | SecurityException ex) {
            fail("Fix La redéfinition de equals() nécessite encore quelque chose");
        } catch (ClassNotFoundException ex3) {
            fail("Unknown class " + ex3.toString());
        } catch (Exception ex2) {
            fail("Unexpected Error " + ex2.toString());
        }
    }
}
