/*
 * @author : Yvan Maillot (yvan.maillot@uha.fr)
 */
package edu.uha.miage;

import caseine.format.javajunit.Grade;
import java.util.Random;
import org.junit.Test;
import static org.junit.Assert.*;
import static edu.uha.miage.ManipulationDeTableaux.*;
import java.util.Arrays;
import static java.util.Arrays.sort;

/**
 * @author yvan
 */
public class ManipulationDeTableauxTest {

    private static Random R = new Random();

    @Test
    public void testTaille() {
        System.out.println("taille");
        int[] t;
        for (int n = 0; n < 20; ++n) {
            t = new int[n];
            assertTrue("Revoir taille()", t.length == taille(t));
        }
    }

    private static boolean queDes0(int[] t) {
        for (int v : t) if (v != 0)
            return false;
        return true;
    }

    @Test
    public void testRemplirDe0() {
        System.out.println("remplirDe0");
        int[] t;
        for (int n = 0; n < 20; ++n) {
            t = new int[n];
            remplirDe0(t);
            assertTrue("Revoir taille()", t.length == taille(t));
        }
    }

    @Test
    @Grade(1)
    public void testEgaux() {
        System.out.println("egaux");
        int[] t1;
        int[] t2;
        t1 = new int[] { 1, 2, 3 };
        t2 = new int[] { 1, 2, 3 };
        assertTrue("Revoir egaux()", egaux(t1, t2));
        t1 = new int[] {};
        t2 = new int[] {};
        assertTrue("Revoir egaux()", egaux(t1, t2));
        t1 = new int[] { 1 };
        t2 = new int[] { 1 };
        assertTrue("Revoir egaux()", egaux(t1, t2));
        t1 = new int[] { 1 };
        t2 = new int[] { 2 };
        assertFalse("Revoir egaux()", egaux(t1, t2));
        t1 = new int[] {};
        t2 = new int[] { 2 };
        assertFalse("Revoir taille()", egaux(t1, t2));
        t1 = new int[] { 1 };
        t2 = new int[] { 1, 2 };
        assertFalse("Revoir egaux()", egaux(t1, t2));
    }

    @Test
    @Grade(3)
    public void testMini() {
        System.out.println("mini");
        int[] t;
        t = new int[] { 1, 2, 3 };
        assertTrue("Revoir mini()", 1 == mini(t));
        t = new int[] { 3 };
        assertTrue("Revoir mini()", 3 == mini(t));
        t = new int[] { 3, 2, 1 };
        assertTrue("Revoir mini()", 1 == mini(t));
        t = new int[] { 5, 1, 3, 2, 1 };
        assertTrue("Revoir mini()", 1 == mini(t));
    }

    @Test
    @Grade(4)
    public void testImini() {
        System.out.println("imini");
        int[] t;
        t = new int[] { 1, 2, 3 };
        assertTrue("Revoir imini()", 0 == imini(t));
        t = new int[] { 3 };
        assertTrue("Revoir imini()", 0 == imini(t));
        t = new int[] { 3, 2, 1 };
        assertTrue("Revoir imini()", 2 == imini(t));
        t = new int[] { 5, 1, 3, 2, 1 };
        assertTrue("Revoir imini()", 1 == t[imini(t)]);
    }

    @Test
    @Grade(2)
    public void testMoyenne() {
        System.out.println("moyenne");
        int[] t;
        for (int i = 0; i < 20; ++i) {
            t = new int[1 + R.nextInt(20)];
            double s = 0;
            for (int j = 0; j < t.length; ++j) {
                t[j] = R.nextInt(21);
                s += t[j];
            }
            assertEquals("Revoir moyenne()", s / t.length, moyenne(t), 1e-6);
        }
    }

    @Test
    @Grade(5)
    public void testCopie() {
        System.out.println("copie");
        int[] t;
        int[] c;
        for (int i = 0; i < 20; ++i) {
            t = new int[1 + R.nextInt(20)];
            for (int j = 0; j < t.length; ++j) {
                t[j] = R.nextInt(21);
            }
            c = copie(t);
            assertTrue("Revoir copie()", t != c && Arrays.equals(t, c));
        }
    }

    private static boolean trie(int[] t, int[] c) {
        int[] ct = Arrays.copyOf(t, t.length);
        int[] cc = Arrays.copyOf(c, c.length);
        if (ct.length != cc.length)
            return false;
        for (int i = 1; i < ct.length; ++i) {
            if (ct[i - 1] > ct[i])
                return false;
        }
        sort(cc);
        for (int i = 0; i < ct.length; ++i) {
            if (ct[i] != cc[i])
                return false;
        }
        return true;
    }

    @Test
    @Grade(4)
    public void testTrier() {
        System.out.println("trier");
        int[] t;
        int[] c;
        for (int i = 0; i < 20; ++i) {
            t = new int[1 + R.nextInt(20)];
            for (int j = 0; j < t.length; ++j) {
                t[j] = R.nextInt(21);
            }
            c = Arrays.copyOf(t, t.length);
            trier(t);
            assertTrue("Revoir trier()", trie(t, c));
        }
    }

    @Test
    @Grade(4)
    public void testDichotomie() {
        System.out.println("dichotomie");
        int[] t;
        for (int i = 0; i < 20; ++i) {
            t = new int[1 + R.nextInt(20)];
            for (int j = 0; j < t.length; ++j) {
                t[j] = R.nextInt(21);
            }
            sort(t);
            for (int v : t) {
                assertTrue("Revoir dichotomie()", Arrays.binarySearch(t, v) == dichotomie(t, v));
            }
            assertTrue("Revoir dichotomie()", dichotomie(t, 100) < 0);
        }
    }
}
