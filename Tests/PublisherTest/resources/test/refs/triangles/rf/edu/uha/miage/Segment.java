/*
 * @author : Yvan Maillot (yvan.maillot@uha.fr)
 */
package edu.uha.miage;

/**
 * @author yvan
 */
public class Segment {
    // TODO 2.01. Déclarer les attributs p1 et p2, les extrémités de ce segment
    // 
    // TODO 2.02. Écrire le constructeur attendant les extrémités de ce segment
    // 
    // TODO 2.03. Écrire un getter pour chaque extrémité
    // 
    // TODO 2.04. Écrire un setter pour chaque extrémité
    // 
    // TODO 2.05. Écrire getLongueur() qui retourne la longueur de ce segment
    // 
    // TODO 2.06. Redéfinir toString() une représentation de ce segment comme demandé dans l'énoncé, Par exemple [(1.2, 3.4) ; (4.5, 5.6)]
    // 
}
