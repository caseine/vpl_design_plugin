/*
 * @author : Yvan Maillot (yvan.maillot@uha.fr)
 */
package edu.uha.miage;

public class Triangle {
    // TODO 3.01. Les trois segments du triangle : s1, s2, s3
    // 
    // TODO 3.02. Écrire le constructeur avec ses trois sommets
    // 
    // TODO 3.03. getP1() qui retourne le premier sommet de ce triangle
    // 
    // TODO 3.04. getP2() qui retourne le deuxième sommet de ce triangle
    // 
    // TODO 3.05. getP3() qui retourne le troisième sommet de ce triangle
    // 
    // TODO 3.06. setP1() qui met à jour le premier sommet de ce triangle
    // 
    // TODO 3.07. setP2() qui met à jour le deuxième sommet de ce triangle
    // 
    // TODO 3.08. setP3() qui met à jour le troisième sommet de ce triangle
    // 
    // TODO 3.05. Retourner le périmètre de ce triangle
    // 
    // TODO 3.06. Retourner la surface de ce triangle
    // 
    // TODO 3.07. Retourner le barycentre de ce triangle
    // 
    // TODO 3.08. La méthode toString()
    // 
}
