/*
 * @author : Yvan Maillot (yvan.maillot@uha.fr)
 */
package edu.uha.miage;

public class Point {
    // TODO 1.01. Déclarer x et y, les coordonnées cartésiennes de ce point
    // 
    // TODO 1.02. Écrire le constructeur aux coordonnées cartésiennes
    // 
    // TODO 1.03. Écrire un getter pour x
    // 
    // TODO 1.04. Écrire un getter pour y
    // 
    // TODO 1.05. Écrire un setter pour x
    // 
    // TODO 1.06. Écrire un setter pour y
    // 
    // TODO 1.07. public toString() qui retourne par exemple (2.5, 3.2)
    // 
}
