package edu.uha.miage;

import java.lang.reflect.*;
import org.junit.*;
import static org.junit.Assert.*;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;
import caseine.reflect.ReflectUtilities;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.MethodOrderer;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@TestMethodOrder(MethodOrderer.MethodName.class)
public class GeneratedSegmentTest {

    @Test
    @caseine.format.javajunit.Grade(0.2702702702702703)
    public void p000020000_checkSegmentFieldP1() {
        System.out.println("Check attribute p1");
        try {
            Field x = ReflectUtilities.parseType("edu.uha.miage.Segment").getDeclaredField("p1");
            assertTrue("Fix  (modifiers)", Modifier.isPrivate(2) == Modifier.isPrivate(x.getModifiers()));
            assertTrue("Fix  (modifiers)", Modifier.isProtected(2) == Modifier.isProtected(x.getModifiers()));
            assertTrue("Fix  (modifiers)", Modifier.isPublic(2) == Modifier.isPublic(x.getModifiers()));
            assertTrue("Fix  (modifiers)", Modifier.isStatic(2) == Modifier.isStatic(x.getModifiers()));
            assertTrue("", x.getType().equals(ReflectUtilities.parseType("edu.uha.miage.Point")));
        } catch (Exception ex) {
            fail("");
        }
    }

    @Test
    @caseine.format.javajunit.Grade(0.2702702702702703)
    public void p000020000_checkSegmentFieldP2() {
        System.out.println("Check attribute p2");
        try {
            Field x = ReflectUtilities.parseType("edu.uha.miage.Segment").getDeclaredField("p2");
            assertTrue("Fix  (modifiers)", Modifier.isPrivate(2) == Modifier.isPrivate(x.getModifiers()));
            assertTrue("Fix  (modifiers)", Modifier.isProtected(2) == Modifier.isProtected(x.getModifiers()));
            assertTrue("Fix  (modifiers)", Modifier.isPublic(2) == Modifier.isPublic(x.getModifiers()));
            assertTrue("Fix  (modifiers)", Modifier.isStatic(2) == Modifier.isStatic(x.getModifiers()));
            assertTrue("", x.getType().equals(ReflectUtilities.parseType("edu.uha.miage.Point")));
        } catch (Exception ex) {
            fail("");
        }
    }

    @Test
    @caseine.format.javajunit.Grade(0.2702702702702703)
    public void p000021000_checkConstructorSegmentPointPoint() {
        System.out.println("Declaration of Constructor of edu.uha.miage.Segment");
        try {
            Constructor x = ReflectUtilities.parseType("edu.uha.miage.Segment").getDeclaredConstructor(ReflectUtilities.parseType("edu.uha.miage.Point"), ReflectUtilities.parseType("edu.uha.miage.Point"));
            assertTrue("Fix  (modifiers)", Modifier.isPrivate(1) == Modifier.isPrivate(x.getModifiers()));
            assertTrue("Fix  (modifiers)", Modifier.isProtected(1) == Modifier.isProtected(x.getModifiers()));
            assertTrue("Fix  (modifiers)", Modifier.isPublic(1) == Modifier.isPublic(x.getModifiers()));
            assertTrue("Fix  (modifiers)", Modifier.isStatic(1) == Modifier.isStatic(x.getModifiers()));
        } catch (Exception ex) {
            fail("Constructor of edu.uha.miage.Segment");
        }
    }

    @Test
    @caseine.format.javajunit.Grade(0.2702702702702703)
    public void p000022000_checkMethodSegmentgetP1() {
        System.out.println("getP1");
        try {
            Class<?> theClass = ReflectUtilities.parseType("edu.uha.miage.Segment");
            Method x = theClass.getDeclaredMethod("getP1");
            assertTrue("Fix getP1 (modifiers)", Modifier.isPrivate(1) == Modifier.isPrivate(x.getModifiers()));
            assertTrue("Fix getP1 (modifiers)", Modifier.isProtected(1) == Modifier.isProtected(x.getModifiers()));
            assertTrue("Fix getP1 (modifiers)", Modifier.isPublic(1) == Modifier.isPublic(x.getModifiers()));
            assertTrue("Fix getP1 (modifiers)", Modifier.isStatic(1) == Modifier.isStatic(x.getModifiers()));
            assertTrue("Fix getP1 (Return)", x.getReturnType().equals(ReflectUtilities.parseType("edu.uha.miage.Point")));
        } catch (NoSuchMethodException | SecurityException ex) {
            fail("Fix getP1");
        } catch (ClassNotFoundException ex3) {
            fail("Unknown class " + ex3.toString());
        } catch (Exception ex2) {
            fail("Unexpected Error " + ex2.toString());
        }
    }

    @Test
    @caseine.format.javajunit.Grade(0.2702702702702703)
    public void p000022000_checkMethodSegmentgetP2() {
        System.out.println("getP2");
        try {
            Class<?> theClass = ReflectUtilities.parseType("edu.uha.miage.Segment");
            Method x = theClass.getDeclaredMethod("getP2");
            assertTrue("Fix getP2 (modifiers)", Modifier.isPrivate(1) == Modifier.isPrivate(x.getModifiers()));
            assertTrue("Fix getP2 (modifiers)", Modifier.isProtected(1) == Modifier.isProtected(x.getModifiers()));
            assertTrue("Fix getP2 (modifiers)", Modifier.isPublic(1) == Modifier.isPublic(x.getModifiers()));
            assertTrue("Fix getP2 (modifiers)", Modifier.isStatic(1) == Modifier.isStatic(x.getModifiers()));
            assertTrue("Fix getP2 (Return)", x.getReturnType().equals(ReflectUtilities.parseType("edu.uha.miage.Point")));
        } catch (NoSuchMethodException | SecurityException ex) {
            fail("Fix getP2");
        } catch (ClassNotFoundException ex3) {
            fail("Unknown class " + ex3.toString());
        } catch (Exception ex2) {
            fail("Unexpected Error " + ex2.toString());
        }
    }

    @Test
    @caseine.format.javajunit.Grade(0.2702702702702703)
    public void p000023000_checkMethodSegmentsetP1Point() {
        System.out.println("setP1");
        try {
            Class<?> theClass = ReflectUtilities.parseType("edu.uha.miage.Segment");
            Method x = theClass.getDeclaredMethod("setP1", ReflectUtilities.parseType("edu.uha.miage.Point"));
            assertTrue("Fix setP1 (modifiers)", Modifier.isPrivate(1) == Modifier.isPrivate(x.getModifiers()));
            assertTrue("Fix setP1 (modifiers)", Modifier.isProtected(1) == Modifier.isProtected(x.getModifiers()));
            assertTrue("Fix setP1 (modifiers)", Modifier.isPublic(1) == Modifier.isPublic(x.getModifiers()));
            assertTrue("Fix setP1 (modifiers)", Modifier.isStatic(1) == Modifier.isStatic(x.getModifiers()));
            assertTrue("Fix setP1 (Return)", x.getReturnType().equals(ReflectUtilities.parseType("void")));
        } catch (NoSuchMethodException | SecurityException ex) {
            fail("Fix setP1");
        } catch (ClassNotFoundException ex3) {
            fail("Unknown class " + ex3.toString());
        } catch (Exception ex2) {
            fail("Unexpected Error " + ex2.toString());
        }
    }

    @Test
    @caseine.format.javajunit.Grade(0.2702702702702703)
    public void p000023000_checkMethodSegmentsetP2Point() {
        System.out.println("setP2");
        try {
            Class<?> theClass = ReflectUtilities.parseType("edu.uha.miage.Segment");
            Method x = theClass.getDeclaredMethod("setP2", ReflectUtilities.parseType("edu.uha.miage.Point"));
            assertTrue("Fix setP2 (modifiers)", Modifier.isPrivate(1) == Modifier.isPrivate(x.getModifiers()));
            assertTrue("Fix setP2 (modifiers)", Modifier.isProtected(1) == Modifier.isProtected(x.getModifiers()));
            assertTrue("Fix setP2 (modifiers)", Modifier.isPublic(1) == Modifier.isPublic(x.getModifiers()));
            assertTrue("Fix setP2 (modifiers)", Modifier.isStatic(1) == Modifier.isStatic(x.getModifiers()));
            assertTrue("Fix setP2 (Return)", x.getReturnType().equals(ReflectUtilities.parseType("void")));
        } catch (NoSuchMethodException | SecurityException ex) {
            fail("Fix setP2");
        } catch (ClassNotFoundException ex3) {
            fail("Unknown class " + ex3.toString());
        } catch (Exception ex2) {
            fail("Unexpected Error " + ex2.toString());
        }
    }

    @Test
    @caseine.format.javajunit.Grade(0.2702702702702703)
    public void p000024000_checkMethodSegmentgetLongueur() {
        System.out.println("Signature getLongueur() of Segment");
        try {
            Class<?> theClass = ReflectUtilities.parseType("edu.uha.miage.Segment");
            Method x = theClass.getDeclaredMethod("getLongueur");
            assertTrue("Fix Signature getLongueur() of Segment (modifiers)", Modifier.isPrivate(1) == Modifier.isPrivate(x.getModifiers()));
            assertTrue("Fix Signature getLongueur() of Segment (modifiers)", Modifier.isProtected(1) == Modifier.isProtected(x.getModifiers()));
            assertTrue("Fix Signature getLongueur() of Segment (modifiers)", Modifier.isPublic(1) == Modifier.isPublic(x.getModifiers()));
            assertTrue("Fix Signature getLongueur() of Segment (modifiers)", Modifier.isStatic(1) == Modifier.isStatic(x.getModifiers()));
            assertTrue("Fix Signature getLongueur() of Segment (Return)", x.getReturnType().equals(ReflectUtilities.parseType("double")));
        } catch (NoSuchMethodException | SecurityException ex) {
            fail("Fix Signature getLongueur() of Segment");
        } catch (ClassNotFoundException ex3) {
            fail("Unknown class " + ex3.toString());
        } catch (Exception ex2) {
            fail("Unexpected Error " + ex2.toString());
        }
    }

    @Test
    @caseine.format.javajunit.Grade(0.2702702702702703)
    public void p000025000_checkMethodSegmenttoString() {
        System.out.println("toString");
        try {
            Class<?> theClass = ReflectUtilities.parseType("edu.uha.miage.Segment");
            Method x = theClass.getDeclaredMethod("toString");
            assertTrue("Fix toString (modifiers)", Modifier.isPrivate(1) == Modifier.isPrivate(x.getModifiers()));
            assertTrue("Fix toString (modifiers)", Modifier.isProtected(1) == Modifier.isProtected(x.getModifiers()));
            assertTrue("Fix toString (modifiers)", Modifier.isPublic(1) == Modifier.isPublic(x.getModifiers()));
            assertTrue("Fix toString (modifiers)", Modifier.isStatic(1) == Modifier.isStatic(x.getModifiers()));
            assertTrue("Fix toString (Return)", x.getReturnType().equals(ReflectUtilities.parseType("java.lang.String")));
        } catch (NoSuchMethodException | SecurityException ex) {
            fail("Fix toString");
        } catch (ClassNotFoundException ex3) {
            fail("Unknown class " + ex3.toString());
        } catch (Exception ex2) {
            fail("Unexpected Error " + ex2.toString());
        }
    }

    @Test
    @caseine.format.javajunit.Grade(0.2702702702702703)
    public void p000026000_checkGetterSegmentP1() {
        System.out.println("Check getter p1");
        try {
            for (int i = 0; i < 100; ++i) {
                Object o = ReflectUtilities.randomValue(ReflectUtilities.parseType("edu.uha.miage.Segment"));
                Object attr = ReflectUtilities.getAttribut(ReflectUtilities.parseType("edu.uha.miage.Segment"), o, "p1");
                Object getAttr = ReflectUtilities.getFromMethod(ReflectUtilities.parseType("edu.uha.miage.Segment"), o, "getP1");
                assertTrue("Fix getP1", ReflectUtilities.equals(attr, getAttr));
            }
        } catch (Exception ex) {
            fail("Fix getter of p1");
        }
    }

    @Test
    @caseine.format.javajunit.Grade(0.2702702702702703)
    public void p000026000_checkGetterSegmentP2() {
        System.out.println("Check getter p2");
        try {
            for (int i = 0; i < 100; ++i) {
                Object o = ReflectUtilities.randomValue(ReflectUtilities.parseType("edu.uha.miage.Segment"));
                Object attr = ReflectUtilities.getAttribut(ReflectUtilities.parseType("edu.uha.miage.Segment"), o, "p2");
                Object getAttr = ReflectUtilities.getFromMethod(ReflectUtilities.parseType("edu.uha.miage.Segment"), o, "getP2");
                assertTrue("Fix getP2", ReflectUtilities.equals(attr, getAttr));
            }
        } catch (Exception ex) {
            fail("Fix getter of p2");
        }
    }

    @Test
    @caseine.format.javajunit.Grade(0.2702702702702703)
    public void p000027000_checkSetterSegmentp1() {
        System.out.println("Check setter p1");
        try {
            Method x = ReflectUtilities.parseType("edu.uha.miage.Segment").getDeclaredMethod("setP1", ReflectUtilities.parseType("edu.uha.miage.Point"));
            assertTrue("Fix setP1", x.getReturnType().equals(void.class));
            Object o = ReflectUtilities.randomValue(ReflectUtilities.parseType("edu.uha.miage.Segment"));
            for (int i = 0; i < 100; ++i) {
                Point v = (Point) ReflectUtilities.randomValue(Point.class);
                ReflectUtilities.getFromMethodTA(ReflectUtilities.parseType("edu.uha.miage.Segment"), o, "setP1", ReflectUtilities.parseType("edu.uha.miage.Point"), v);
                assertTrue("Fix setP1", ReflectUtilities.getAttribut(ReflectUtilities.parseType("edu.uha.miage.Segment"), o, "p1").equals(v));
            }
        } catch (Exception ex) {
            fail("Fix setP1");
        }
    }

    @Test
    @caseine.format.javajunit.Grade(0.2702702702702703)
    public void p000027000_checkSetterSegmentp2() {
        System.out.println("Check setter p2");
        try {
            Method x = ReflectUtilities.parseType("edu.uha.miage.Segment").getDeclaredMethod("setP2", ReflectUtilities.parseType("edu.uha.miage.Point"));
            assertTrue("Fix setP2", x.getReturnType().equals(void.class));
            Object o = ReflectUtilities.randomValue(ReflectUtilities.parseType("edu.uha.miage.Segment"));
            for (int i = 0; i < 100; ++i) {
                Point v = (Point) ReflectUtilities.randomValue(Point.class);
                ReflectUtilities.getFromMethodTA(ReflectUtilities.parseType("edu.uha.miage.Segment"), o, "setP2", ReflectUtilities.parseType("edu.uha.miage.Point"), v);
                assertTrue("Fix setP2", ReflectUtilities.getAttribut(ReflectUtilities.parseType("edu.uha.miage.Segment"), o, "p2").equals(v));
            }
        } catch (Exception ex) {
            fail("Fix setP2");
        }
    }

    @Test
    @caseine.format.javajunit.Grade(2.7027027027027026)
    public void p000028000_testMethodSegmentgetLongueur() {
        System.out.println("Test Method Segment.getLongueur");
        try {
            Class<?> classRef = cf.edu.uha.miage.Segment.class;
            Class<?> classToTest = ReflectUtilities.parseType("edu.uha.miage.Segment");
            for (int i = 0; i < 100; ++i) {
                StringBuilder msg = new StringBuilder("Fix Segment.getLongueur() --> ");
                boolean result = ReflectUtilities.sameResult(msg, classRef, classToTest, "getLongueur");
                assertTrue("Behavior getLongueur() of Segment", result);
            }
        } catch (Exception ex) {
            fail("Fix Segment.getLongueur() ");
        }
    }
}
