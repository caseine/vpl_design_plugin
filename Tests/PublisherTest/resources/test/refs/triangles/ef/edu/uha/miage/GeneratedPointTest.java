package edu.uha.miage;

import java.lang.reflect.*;
import org.junit.*;
import static org.junit.Assert.*;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;
import caseine.reflect.ReflectUtilities;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.MethodOrderer;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@TestMethodOrder(MethodOrderer.MethodName.class)
public class GeneratedPointTest {

    @Test
    @caseine.format.javajunit.Grade(0.2702702702702703)
    public void p000001000_checkPointFieldX() {
        System.out.println("Check attribute x");
        try {
            Field x = ReflectUtilities.parseType("edu.uha.miage.Point").getDeclaredField("x");
            assertTrue("Fix x ou y : déclaration incorrecte ou manquante (modifiers)", Modifier.isPrivate(2) == Modifier.isPrivate(x.getModifiers()));
            assertTrue("Fix x ou y : déclaration incorrecte ou manquante (modifiers)", Modifier.isProtected(2) == Modifier.isProtected(x.getModifiers()));
            assertTrue("Fix x ou y : déclaration incorrecte ou manquante (modifiers)", Modifier.isPublic(2) == Modifier.isPublic(x.getModifiers()));
            assertTrue("Fix x ou y : déclaration incorrecte ou manquante (modifiers)", Modifier.isStatic(2) == Modifier.isStatic(x.getModifiers()));
            assertTrue("x ou y : déclaration incorrecte ou manquante", x.getType().equals(ReflectUtilities.parseType("double")));
        } catch (Exception ex) {
            fail("x ou y : déclaration incorrecte ou manquante");
        }
    }

    @Test
    @caseine.format.javajunit.Grade(0.2702702702702703)
    public void p000001000_checkPointFieldY() {
        System.out.println("Check attribute y");
        try {
            Field x = ReflectUtilities.parseType("edu.uha.miage.Point").getDeclaredField("y");
            assertTrue("Fix x ou y : déclaration incorrecte ou manquante (modifiers)", Modifier.isPrivate(2) == Modifier.isPrivate(x.getModifiers()));
            assertTrue("Fix x ou y : déclaration incorrecte ou manquante (modifiers)", Modifier.isProtected(2) == Modifier.isProtected(x.getModifiers()));
            assertTrue("Fix x ou y : déclaration incorrecte ou manquante (modifiers)", Modifier.isPublic(2) == Modifier.isPublic(x.getModifiers()));
            assertTrue("Fix x ou y : déclaration incorrecte ou manquante (modifiers)", Modifier.isStatic(2) == Modifier.isStatic(x.getModifiers()));
            assertTrue("x ou y : déclaration incorrecte ou manquante", x.getType().equals(ReflectUtilities.parseType("double")));
        } catch (Exception ex) {
            fail("x ou y : déclaration incorrecte ou manquante");
        }
    }

    @Test
    @caseine.format.javajunit.Grade(0.2702702702702703)
    public void p000002000_checkConstructorPointdoubledouble() {
        System.out.println("Declaration of Constructor of edu.uha.miage.Point");
        try {
            Constructor x = ReflectUtilities.parseType("edu.uha.miage.Point").getDeclaredConstructor(ReflectUtilities.parseType("double"), ReflectUtilities.parseType("double"));
            assertTrue("Fix  (modifiers)", Modifier.isPrivate(1) == Modifier.isPrivate(x.getModifiers()));
            assertTrue("Fix  (modifiers)", Modifier.isProtected(1) == Modifier.isProtected(x.getModifiers()));
            assertTrue("Fix  (modifiers)", Modifier.isPublic(1) == Modifier.isPublic(x.getModifiers()));
            assertTrue("Fix  (modifiers)", Modifier.isStatic(1) == Modifier.isStatic(x.getModifiers()));
        } catch (Exception ex) {
            fail("Constructor of edu.uha.miage.Point");
        }
    }

    @Test
    @caseine.format.javajunit.Grade(0.2702702702702703)
    public void p000003000_checkMethodPointgetX() {
        System.out.println("getX");
        try {
            Class<?> theClass = ReflectUtilities.parseType("edu.uha.miage.Point");
            Method x = theClass.getDeclaredMethod("getX");
            assertTrue("Fix getX (modifiers)", Modifier.isPrivate(1) == Modifier.isPrivate(x.getModifiers()));
            assertTrue("Fix getX (modifiers)", Modifier.isProtected(1) == Modifier.isProtected(x.getModifiers()));
            assertTrue("Fix getX (modifiers)", Modifier.isPublic(1) == Modifier.isPublic(x.getModifiers()));
            assertTrue("Fix getX (modifiers)", Modifier.isStatic(1) == Modifier.isStatic(x.getModifiers()));
            assertTrue("Fix getX (Return)", x.getReturnType().equals(ReflectUtilities.parseType("double")));
        } catch (NoSuchMethodException | SecurityException ex) {
            fail("Fix getX");
        } catch (ClassNotFoundException ex3) {
            fail("Unknown class " + ex3.toString());
        } catch (Exception ex2) {
            fail("Unexpected Error " + ex2.toString());
        }
    }

    @Test
    @caseine.format.javajunit.Grade(0.2702702702702703)
    public void p000003000_checkMethodPointgetY() {
        System.out.println("getY");
        try {
            Class<?> theClass = ReflectUtilities.parseType("edu.uha.miage.Point");
            Method x = theClass.getDeclaredMethod("getY");
            assertTrue("Fix getY (modifiers)", Modifier.isPrivate(1) == Modifier.isPrivate(x.getModifiers()));
            assertTrue("Fix getY (modifiers)", Modifier.isProtected(1) == Modifier.isProtected(x.getModifiers()));
            assertTrue("Fix getY (modifiers)", Modifier.isPublic(1) == Modifier.isPublic(x.getModifiers()));
            assertTrue("Fix getY (modifiers)", Modifier.isStatic(1) == Modifier.isStatic(x.getModifiers()));
            assertTrue("Fix getY (Return)", x.getReturnType().equals(ReflectUtilities.parseType("double")));
        } catch (NoSuchMethodException | SecurityException ex) {
            fail("Fix getY");
        } catch (ClassNotFoundException ex3) {
            fail("Unknown class " + ex3.toString());
        } catch (Exception ex2) {
            fail("Unexpected Error " + ex2.toString());
        }
    }

    @Test
    @caseine.format.javajunit.Grade(0.2702702702702703)
    public void p000004000_checkMethodPointsetXdouble() {
        System.out.println("setX");
        try {
            Class<?> theClass = ReflectUtilities.parseType("edu.uha.miage.Point");
            Method x = theClass.getDeclaredMethod("setX", ReflectUtilities.parseType("double"));
            assertTrue("Fix setX (modifiers)", Modifier.isPrivate(1) == Modifier.isPrivate(x.getModifiers()));
            assertTrue("Fix setX (modifiers)", Modifier.isProtected(1) == Modifier.isProtected(x.getModifiers()));
            assertTrue("Fix setX (modifiers)", Modifier.isPublic(1) == Modifier.isPublic(x.getModifiers()));
            assertTrue("Fix setX (modifiers)", Modifier.isStatic(1) == Modifier.isStatic(x.getModifiers()));
            assertTrue("Fix setX (Return)", x.getReturnType().equals(ReflectUtilities.parseType("void")));
        } catch (NoSuchMethodException | SecurityException ex) {
            fail("Fix setX");
        } catch (ClassNotFoundException ex3) {
            fail("Unknown class " + ex3.toString());
        } catch (Exception ex2) {
            fail("Unexpected Error " + ex2.toString());
        }
    }

    @Test
    @caseine.format.javajunit.Grade(0.2702702702702703)
    public void p000004000_checkMethodPointsetYdouble() {
        System.out.println("setY");
        try {
            Class<?> theClass = ReflectUtilities.parseType("edu.uha.miage.Point");
            Method x = theClass.getDeclaredMethod("setY", ReflectUtilities.parseType("double"));
            assertTrue("Fix setY (modifiers)", Modifier.isPrivate(1) == Modifier.isPrivate(x.getModifiers()));
            assertTrue("Fix setY (modifiers)", Modifier.isProtected(1) == Modifier.isProtected(x.getModifiers()));
            assertTrue("Fix setY (modifiers)", Modifier.isPublic(1) == Modifier.isPublic(x.getModifiers()));
            assertTrue("Fix setY (modifiers)", Modifier.isStatic(1) == Modifier.isStatic(x.getModifiers()));
            assertTrue("Fix setY (Return)", x.getReturnType().equals(ReflectUtilities.parseType("void")));
        } catch (NoSuchMethodException | SecurityException ex) {
            fail("Fix setY");
        } catch (ClassNotFoundException ex3) {
            fail("Unknown class " + ex3.toString());
        } catch (Exception ex2) {
            fail("Unexpected Error " + ex2.toString());
        }
    }

    @Test
    @caseine.format.javajunit.Grade(0.2702702702702703)
    public void p000005000_checkMethodPointtoString() {
        System.out.println("toString");
        try {
            Class<?> theClass = ReflectUtilities.parseType("edu.uha.miage.Point");
            Method x = theClass.getDeclaredMethod("toString");
            assertTrue("Fix toString (modifiers)", Modifier.isPrivate(1) == Modifier.isPrivate(x.getModifiers()));
            assertTrue("Fix toString (modifiers)", Modifier.isProtected(1) == Modifier.isProtected(x.getModifiers()));
            assertTrue("Fix toString (modifiers)", Modifier.isPublic(1) == Modifier.isPublic(x.getModifiers()));
            assertTrue("Fix toString (modifiers)", Modifier.isStatic(1) == Modifier.isStatic(x.getModifiers()));
            assertTrue("Fix toString (Return)", x.getReturnType().equals(ReflectUtilities.parseType("java.lang.String")));
        } catch (NoSuchMethodException | SecurityException ex) {
            fail("Fix toString");
        } catch (ClassNotFoundException ex3) {
            fail("Unknown class " + ex3.toString());
        } catch (Exception ex2) {
            fail("Unexpected Error " + ex2.toString());
        }
    }

    @Test
    @caseine.format.javajunit.Grade(0.2702702702702703)
    public void p000006000_checkGetterPointX() {
        System.out.println("Check getter x");
        try {
            for (int i = 0; i < 100; ++i) {
                Object o = ReflectUtilities.randomValue(ReflectUtilities.parseType("edu.uha.miage.Point"));
                Object attr = ReflectUtilities.getAttribut(ReflectUtilities.parseType("edu.uha.miage.Point"), o, "x");
                Object getAttr = ReflectUtilities.getFromMethod(ReflectUtilities.parseType("edu.uha.miage.Point"), o, "getX");
                assertTrue("Fix getX", ReflectUtilities.equals(attr, getAttr));
            }
        } catch (Exception ex) {
            fail("Fix getter of x");
        }
    }

    @Test
    @caseine.format.javajunit.Grade(0.2702702702702703)
    public void p000006000_checkGetterPointY() {
        System.out.println("Check getter y");
        try {
            for (int i = 0; i < 100; ++i) {
                Object o = ReflectUtilities.randomValue(ReflectUtilities.parseType("edu.uha.miage.Point"));
                Object attr = ReflectUtilities.getAttribut(ReflectUtilities.parseType("edu.uha.miage.Point"), o, "y");
                Object getAttr = ReflectUtilities.getFromMethod(ReflectUtilities.parseType("edu.uha.miage.Point"), o, "getY");
                assertTrue("Fix getY", ReflectUtilities.equals(attr, getAttr));
            }
        } catch (Exception ex) {
            fail("Fix getter of y");
        }
    }

    @Test
    @caseine.format.javajunit.Grade(0.2702702702702703)
    public void p000007000_checkSetterPointx() {
        System.out.println("Check setter x");
        try {
            Method x = ReflectUtilities.parseType("edu.uha.miage.Point").getDeclaredMethod("setX", ReflectUtilities.parseType("double"));
            assertTrue("Fix setX", x.getReturnType().equals(void.class));
            Object o = ReflectUtilities.randomValue(ReflectUtilities.parseType("edu.uha.miage.Point"));
            for (int i = 0; i < 100; ++i) {
                double v = (double) ReflectUtilities.randomValue(double.class);
                ReflectUtilities.getFromMethodTA(ReflectUtilities.parseType("edu.uha.miage.Point"), o, "setX", ReflectUtilities.parseType("double"), v);
                assertTrue("Fix setX", ReflectUtilities.getAttribut(ReflectUtilities.parseType("edu.uha.miage.Point"), o, "x").equals(v));
            }
        } catch (Exception ex) {
            fail("Fix setX");
        }
    }

    @Test
    @caseine.format.javajunit.Grade(0.2702702702702703)
    public void p000007000_checkSetterPointy() {
        System.out.println("Check setter y");
        try {
            Method x = ReflectUtilities.parseType("edu.uha.miage.Point").getDeclaredMethod("setY", ReflectUtilities.parseType("double"));
            assertTrue("Fix setY", x.getReturnType().equals(void.class));
            Object o = ReflectUtilities.randomValue(ReflectUtilities.parseType("edu.uha.miage.Point"));
            for (int i = 0; i < 100; ++i) {
                double v = (double) ReflectUtilities.randomValue(double.class);
                ReflectUtilities.getFromMethodTA(ReflectUtilities.parseType("edu.uha.miage.Point"), o, "setY", ReflectUtilities.parseType("double"), v);
                assertTrue("Fix setY", ReflectUtilities.getAttribut(ReflectUtilities.parseType("edu.uha.miage.Point"), o, "y").equals(v));
            }
        } catch (Exception ex) {
            fail("Fix setY");
        }
    }
}
