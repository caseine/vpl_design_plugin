package edu.uha.miage;

import java.lang.reflect.*;
import org.junit.*;
import static org.junit.Assert.*;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;
import caseine.reflect.ReflectUtilities;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.MethodOrderer;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@TestMethodOrder(MethodOrderer.MethodName.class)
public class GeneratedCTest {

    @Test
    @caseine.format.javajunit.Grade(1.8181818181818181)
    public void p000000000_checkCFieldPfi() {
        System.out.println("Check attribute pfi");
        try {
            Field x = ReflectUtilities.parseType("edu.uha.miage.C").getDeclaredField("pfi");
            assertTrue("Fix Attribut pfi (modifiers)", Modifier.isPrivate(17) == Modifier.isPrivate(x.getModifiers()));
            assertTrue("Fix Attribut pfi (modifiers)", Modifier.isProtected(17) == Modifier.isProtected(x.getModifiers()));
            assertTrue("Fix Attribut pfi (modifiers)", Modifier.isPublic(17) == Modifier.isPublic(x.getModifiers()));
            assertTrue("Fix Attribut pfi (modifiers)", Modifier.isStatic(17) == Modifier.isStatic(x.getModifiers()));
            assertTrue("Attribut pfi", x.getType().equals(ReflectUtilities.parseType("int")));
        } catch (Exception ex) {
            fail("Attribut pfi");
        }
    }

    @Test
    @caseine.format.javajunit.Grade(3.6363636363636362)
    public void p000000000_checkCInnerClassIC1() {
        System.out.println("Check innerClass IC1");
        try {
            Class<?> x = ReflectUtilities.getDeclaredClass(edu.uha.miage.C.class, "IC1");
            if (x != null) {
                assertTrue("Fix Inner class IC1 (modifiers)", Modifier.isPrivate(10) == Modifier.isPrivate(x.getModifiers()));
                assertTrue("Fix Inner class IC1 (modifiers)", Modifier.isProtected(10) == Modifier.isProtected(x.getModifiers()));
                assertTrue("Fix Inner class IC1 (modifiers)", Modifier.isPublic(10) == Modifier.isPublic(x.getModifiers()));
                assertTrue("Fix Inner class IC1 (modifiers)", Modifier.isStatic(10) == Modifier.isStatic(x.getModifiers()));
            } else {
                fail("Classe interne IC1 absente");
            }
        } catch (Exception ex) {
            fail("Inner class IC1");
        }
    }

    @Test
    @caseine.format.javajunit.Grade(3.6363636363636362)
    public void p000000000_checkCInnerClassIC2() {
        System.out.println("Check innerClass IC2");
        try {
            Class<?> x = ReflectUtilities.getDeclaredClass(edu.uha.miage.C.class, "IC2");
            if (x != null) {
                assertTrue("Fix Inner class IC2 (modifiers)", Modifier.isPrivate(0) == Modifier.isPrivate(x.getModifiers()));
                assertTrue("Fix Inner class IC2 (modifiers)", Modifier.isProtected(0) == Modifier.isProtected(x.getModifiers()));
                assertTrue("Fix Inner class IC2 (modifiers)", Modifier.isPublic(0) == Modifier.isPublic(x.getModifiers()));
                assertTrue("Fix Inner class IC2 (modifiers)", Modifier.isStatic(0) == Modifier.isStatic(x.getModifiers()));
            } else {
                fail("Classe interne IC2 absente");
            }
        } catch (Exception ex) {
            fail("Inner class IC2");
        }
    }

    @Test
    @caseine.format.javajunit.Grade(3.6363636363636362)
    public void p000000000_checkClassIC1() {
        System.out.println("check class IC1");
        try {
            assertTrue("Inner class IC1 (Inheritance)", ReflectUtilities.parseType("edu.uha.miage.C$IC1").getSuperclass().equals(ReflectUtilities.parseType("java.lang.Object")));
            Class<?> x = ReflectUtilities.parseType("edu.uha.miage.C$IC1");
            assertTrue("Fix Inner class IC1 (modifiers)", Modifier.isPrivate(10) == Modifier.isPrivate(x.getModifiers()));
            assertTrue("Fix Inner class IC1 (modifiers)", Modifier.isProtected(10) == Modifier.isProtected(x.getModifiers()));
            assertTrue("Fix Inner class IC1 (modifiers)", Modifier.isPublic(10) == Modifier.isPublic(x.getModifiers()));
            assertTrue("Fix Inner class IC1 (modifiers)", Modifier.isStatic(10) == Modifier.isStatic(x.getModifiers()));
        } catch (ClassNotFoundException ex) {
            fail("Inner class IC1");
        }
    }

    @Test
    @caseine.format.javajunit.Grade(3.6363636363636362)
    public void p000000000_checkClassIC2() {
        System.out.println("check class IC2");
        try {
            assertTrue("Inner class IC2 (Inheritance)", ReflectUtilities.parseType("edu.uha.miage.C$IC2").getSuperclass().equals(ReflectUtilities.parseType("java.lang.Object")));
            Class<?> x = ReflectUtilities.parseType("edu.uha.miage.C$IC2");
            assertTrue("Fix Inner class IC2 (modifiers)", Modifier.isPrivate(0) == Modifier.isPrivate(x.getModifiers()));
            assertTrue("Fix Inner class IC2 (modifiers)", Modifier.isProtected(0) == Modifier.isProtected(x.getModifiers()));
            assertTrue("Fix Inner class IC2 (modifiers)", Modifier.isPublic(0) == Modifier.isPublic(x.getModifiers()));
            assertTrue("Fix Inner class IC2 (modifiers)", Modifier.isStatic(0) == Modifier.isStatic(x.getModifiers()));
        } catch (ClassNotFoundException ex) {
            fail("Inner class IC2");
        }
    }

    @Test
    @caseine.format.javajunit.Grade(1.8181818181818181)
    public void p000000000_checkIC1FieldPbfi() {
        System.out.println("Check attribute pbfi");
        try {
            Field x = ReflectUtilities.parseType("edu.uha.miage.C$IC1").getDeclaredField("pbfi");
            assertTrue("Fix Attribut pbfi dans IC1 (modifiers)", Modifier.isPrivate(17) == Modifier.isPrivate(x.getModifiers()));
            assertTrue("Fix Attribut pbfi dans IC1 (modifiers)", Modifier.isProtected(17) == Modifier.isProtected(x.getModifiers()));
            assertTrue("Fix Attribut pbfi dans IC1 (modifiers)", Modifier.isPublic(17) == Modifier.isPublic(x.getModifiers()));
            assertTrue("Fix Attribut pbfi dans IC1 (modifiers)", Modifier.isStatic(17) == Modifier.isStatic(x.getModifiers()));
            assertTrue("Attribut pbfi dans IC1", x.getType().equals(ReflectUtilities.parseType("int")));
        } catch (Exception ex) {
            fail("Attribut pbfi dans IC1");
        }
    }

    @Test
    @caseine.format.javajunit.Grade(1.8181818181818181)
    public void p000000000_checkIC2FieldPvs() {
        System.out.println("Check attribute pvs");
        try {
            Field x = ReflectUtilities.parseType("edu.uha.miage.C$IC2").getDeclaredField("pvs");
            assertTrue("Fix Attribut pvs dans IC2 (modifiers)", Modifier.isPrivate(2) == Modifier.isPrivate(x.getModifiers()));
            assertTrue("Fix Attribut pvs dans IC2 (modifiers)", Modifier.isProtected(2) == Modifier.isProtected(x.getModifiers()));
            assertTrue("Fix Attribut pvs dans IC2 (modifiers)", Modifier.isPublic(2) == Modifier.isPublic(x.getModifiers()));
            assertTrue("Fix Attribut pvs dans IC2 (modifiers)", Modifier.isStatic(2) == Modifier.isStatic(x.getModifiers()));
            assertTrue("Attribut pvs dans IC2", x.getType().equals(ReflectUtilities.parseType("java.lang.String")));
        } catch (Exception ex) {
            fail("Attribut pvs dans IC2");
        }
    }
}
