/*
 * @author : Yvan Maillot (yvan.maillot@uha.fr)
 */
package cf.edu.uha.miage;

import caseine.tags.RelativeEvaluation;
import caseine.tags.ToCheck;
import caseine.tags.ToDo;

/**
 * @author yvan
 */
@RelativeEvaluation
public class C {

    @ToDo("1.01 Déclarer pfi, un attribut d'instance public et constant de type int ")
    @ToCheck(value = "Attribut pfi", grade = 1)
    public final int pfi = 0;

    @ToDo("1.02 Déclarer IC1 une classe interne de classe privée")
    @ToCheck(value = "Inner class IC1", grade = 2)
    private static class IC1 {

        @ToDo("2.01 Déclarer pbfi, dans IC1, un attribut d'instance public et constant de type int ")
        @ToCheck(value = "Attribut pbfi dans IC1", grade = 1)
        public final int pbfi = 0;
    }

    @ToDo("1.03 Déclarer IC2 une classe interne de visibilité amicale")
    @ToCheck(value = "Inner class IC2", grade = 2)
    class IC2 {

        @ToDo("3.01 Déclarer pvs, dans IC2, un attribut privé de type String ")
        @ToCheck(value = "Attribut pvs dans IC2", grade = 1)
        private String pvs;
    }
}
