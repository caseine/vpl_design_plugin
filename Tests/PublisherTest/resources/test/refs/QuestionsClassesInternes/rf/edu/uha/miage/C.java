/*
 * @author : Yvan Maillot (yvan.maillot@uha.fr)
 */
package edu.uha.miage;

/**
 * @author yvan
 */
public class C {
    // TODO 1.01 Déclarer pfi, un attribut d'instance public et constant de type int
    // 
    // TODO 1.02 Déclarer IC1 une classe interne de classe privée
    // 
    // TODO 1.03 Déclarer IC2 une classe interne de visibilité amicale
    // 
}
// TODO 2.01 Déclarer pbfi, dans IC1, un attribut d'instance public et constant de type int
// 
// TODO 3.01 Déclarer pvs, dans IC2, un attribut privé de type String
// 
