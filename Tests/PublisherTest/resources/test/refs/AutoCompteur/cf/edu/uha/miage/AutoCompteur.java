/*
 * @author : Yvan Maillot (yvan.maillot@uha.fr)
 */
package edu.uha.miage;

/**
 * Écrire une classe AutoCompteur dont l’instance affiche à sa construction le
 * nombre d’instances de AutoCompteur créées.
 *
 * Chaque instance de cette classe doit être identifiée par le numéro d'ordre
 * de sa création. L'identifiant de la première créée est 1, la deuxième est 2,
 * la nième est n.
 *
 * Cette classe devra possèder deux méthodes :
 *
 *   1. getNbInstances() qui donne le nombre d'instances de cette classe déjà créées.
 *   2. getId() qui donne l'identifiant de l'instance sur laquelle elle s'applique.
 *
 * @author yvan
 */
public class AutoCompteur {

    // Pour compter le nombre d'instances créées, un attribut de classe est nécessaire
    private static int nbInstances;

    private int id;

    public AutoCompteur() {
        System.out.println(id = ++nbInstances);
    }

    public static int getNbInstances() {
        return nbInstances;
    }

    public int getId() {
        return id;
    }
}
