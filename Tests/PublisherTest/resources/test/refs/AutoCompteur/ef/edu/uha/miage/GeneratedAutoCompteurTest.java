package edu.uha.miage;

import java.lang.reflect.*;
import org.junit.*;
import static org.junit.Assert.*;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;
import caseine.reflect.ReflectUtilities;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.MethodOrderer;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@TestMethodOrder(MethodOrderer.MethodName.class)
public class GeneratedAutoCompteurTest {

    @Test
    @caseine.format.javajunit.Grade(2.0)
    public void p000003000_checkMethodAutoCompteurgetId() {
        System.out.println("getId");
        try {
            Class<?> theClass = ReflectUtilities.parseType("edu.uha.miage.AutoCompteur");
            Method x = theClass.getDeclaredMethod("getId");
            assertTrue("Fix getId (modifiers)", Modifier.isPrivate(1) == Modifier.isPrivate(x.getModifiers()));
            assertTrue("Fix getId (modifiers)", Modifier.isProtected(1) == Modifier.isProtected(x.getModifiers()));
            assertTrue("Fix getId (modifiers)", Modifier.isPublic(1) == Modifier.isPublic(x.getModifiers()));
            assertTrue("Fix getId (modifiers)", Modifier.isStatic(1) == Modifier.isStatic(x.getModifiers()));
            assertTrue("Fix getId (Return)", x.getReturnType().equals(ReflectUtilities.parseType("int")));
        } catch (NoSuchMethodException | SecurityException ex) {
            fail("Fix getId");
        } catch (ClassNotFoundException ex3) {
            fail("Unknown class " + ex3.toString());
        } catch (Exception ex2) {
            fail("Unexpected Error " + ex2.toString());
        }
    }

    @Test
    @caseine.format.javajunit.Grade(2.0)
    public void p000003000_checkMethodAutoCompteurgetNbInstances() {
        System.out.println("getNbInstances");
        try {
            Class<?> theClass = ReflectUtilities.parseType("edu.uha.miage.AutoCompteur");
            Method x = theClass.getDeclaredMethod("getNbInstances");
            assertTrue("Fix getNbInstances (modifiers)", Modifier.isPrivate(9) == Modifier.isPrivate(x.getModifiers()));
            assertTrue("Fix getNbInstances (modifiers)", Modifier.isProtected(9) == Modifier.isProtected(x.getModifiers()));
            assertTrue("Fix getNbInstances (modifiers)", Modifier.isPublic(9) == Modifier.isPublic(x.getModifiers()));
            assertTrue("Fix getNbInstances (modifiers)", Modifier.isStatic(9) == Modifier.isStatic(x.getModifiers()));
            assertTrue("Fix getNbInstances (Return)", x.getReturnType().equals(ReflectUtilities.parseType("int")));
        } catch (NoSuchMethodException | SecurityException ex) {
            fail("Fix getNbInstances");
        } catch (ClassNotFoundException ex3) {
            fail("Unknown class " + ex3.toString());
        } catch (Exception ex2) {
            fail("Unexpected Error " + ex2.toString());
        }
    }
}
