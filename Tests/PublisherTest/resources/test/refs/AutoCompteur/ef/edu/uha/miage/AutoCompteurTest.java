/*
 * @author : Yvan Maillot (yvan.maillot@uha.fr)
 */
package edu.uha.miage;

import caseine.format.javajunit.Grade;
import caseine.tags.ClassTestPriority;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Arrays;
import java.util.Random;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;

/**
 * @author yvan
 */
@ClassTestPriority(1)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class AutoCompteurTest {

    private static AutoCompteur[] acpt;

    private static int nbInstances;

    private static Random R = new Random();

    private static boolean constructeurExistant = false;

    private static boolean affConstCorrectOuNbInstancesMalCalc = false;

    private static void maz() {
        Arrays.stream(AutoCompteur.class.getDeclaredFields()).filter(f -> Modifier.isPrivate(f.getModifiers())).filter(f -> Modifier.isStatic(f.getModifiers())).filter(f -> !Modifier.isFinal(f.getModifiers())).filter(f -> f.getType() == int.class || f.getType() == Integer.class || f.getType() == Byte.class || f.getType() == byte.class || f.getType() == Short.class || f.getType() == short.class).forEach(f -> {
            f.setAccessible(true);
            try {
                f.set(null, 0);
            } catch (IllegalArgumentException | IllegalAccessException ex) {
            }
        });
    }

    @BeforeClass
    public static void beforeClass() {
        Constructor<?> k;
        // goodFieldNbInstances();
        // goodFieldId();
        nbInstances = 5 + R.nextInt(6);
        try {
            k = AutoCompteur.class.getConstructor();
            maz();
            constructeurExistant = true;
            acpt = new AutoCompteur[nbInstances];
            for (int i = 1; i <= nbInstances; ++i) {
                PrintStream out = System.out;
                try (ByteArrayOutputStream bos = new ByteArrayOutputStream()) {
                    System.setOut(new PrintStream(bos));
                    acpt[i - 1] = (AutoCompteur) k.newInstance();
                    affConstCorrectOuNbInstancesMalCalc = affConstCorrectOuNbInstancesMalCalc || bos.toString().contains(i + "");
                } catch (IOException ex) {
                    constructeurExistant = false;
                } finally {
                    System.setOut(out);
                }
            }
        } catch (NoSuchMethodException | SecurityException | InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
            constructeurExistant = false;
        }
    }

    private static void goodFieldNbInstances() {
        try {
            assertTrue("Il manque un attribut adapté pour le nombre total d'instances créés", Arrays.stream(AutoCompteur.class.getDeclaredFields()).filter(f -> Modifier.isPrivate(f.getModifiers())).filter(f -> Modifier.isStatic(f.getModifiers())).filter(f -> !Modifier.isFinal(f.getModifiers())).filter(f -> f.getType() == int.class || f.getType() == Integer.class || f.getType() == Byte.class || f.getType() == byte.class || f.getType() == Short.class || f.getType() == short.class).count() > 0);
        } catch (Exception ex) {
            fail("Check attribute nbInstances");
        }
    }

    @Test
    @Grade(2.0)
    public void p01checkAutoCompteurFieldNbInstances() {
        System.out.println("Check attribute nbInstances");
        goodFieldNbInstances();
    }

    private static void goodFieldId() {
        try {
            assertTrue("Il manque un attribut adapté pour l'identifiant d'une instance", Arrays.stream(AutoCompteur.class.getDeclaredFields()).filter(f -> Modifier.isPrivate(f.getModifiers())).filter(f -> !Modifier.isStatic(f.getModifiers())).filter(f -> f.getType() == int.class || f.getType() == Integer.class || f.getType() == Byte.class || f.getType() == byte.class || f.getType() == Short.class || f.getType() == short.class).count() > 0);
        } catch (Exception ex) {
            fail("Check attribute id");
        }
    }

    @Test
    @Grade(2.0)
    public void p02checkAutoCompteurFieldId() {
        System.out.println("Check attribute id");
        goodFieldId();
    }

    @Test
    @Grade(2.0)
    public void p03PresenceConstructeur() {
        System.out.println("Constructeur");
        assertTrue("Constructeur manquant", constructeurExistant);
    }

    @Test
    @Grade(2.0)
    public void p04ConstructeurIncorrect() {
        System.out.println("Constructeur");
        assertTrue("Il semble que le nombre d'instances n'est pas affiché correctement à la construction ou alors le nombre totale d'instances est mal calculé", affConstCorrectOuNbInstancesMalCalc);
    }

    @Test
    @Grade(4.0)
    public void p05testGetNbInstances() {
        System.out.println("getNbInstances");
        Method getNbInstances;
        try {
            getNbInstances = AutoCompteur.class.getDeclaredMethod("getNbInstances");
            assertEquals("Il semble que le nombre d'instances créées n'est pas comptabilisé", nbInstances, getNbInstances.invoke(null));
        } catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
            fail("getNbInstances manquant ou mal signée");
        }
    }

    @Test
    @Grade(4.0)
    public void p06testGetId() {
        System.out.println("getId");
        Method getId;
        try {
            getId = AutoCompteur.class.getDeclaredMethod("getId");
            for (int i = 1; i <= nbInstances; ++i) {
                assertEquals("Il semble que l'identifiant soit incorrect", i, getId.invoke(acpt[i - 1]));
            }
        } catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
            fail("getId manquant ou mal signée");
        }
    }
}
