public class Date {

    private Date() {
    }

    public static boolean valiDate(int dd, int mm, int yy) {
        boolean res = true;
        boolean bissextile;
        int[] month_day = { 0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
        bissextile = ((yy % 400 == 0) || ((yy % 4 == 0) && (yy % 100 != 0)));
        if (bissextile)
            month_day[2] = 29;
        if ((mm < 1) || (mm > 12)) {
            res = false;
        } else if ((dd < 1) || (dd > month_day[mm])) {
            res = false;
        }
        return res;
    }
}
