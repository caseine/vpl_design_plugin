import static org.junit.Assert.*;
import org.junit.Test;
import caseine.extra.utils.mutations.DynamicTestExecutor;
import caseine.extra.utils.mutations.DynamicTestExecutorImpl4;

public class TestTestDate {

    private static final String ERROR_NOT_DETECTED = "MUTANT SURVIVED -> ";

    @Test
    public void nominal_test() throws Exception {
        DynamicTestExecutor dte = new DynamicTestExecutorImpl4("TestDate", null);
        assertFalse("The code MUST be implemented", dte.doTest());
    }

    @Test
    public void kill_mutant_1() throws Exception {
        DynamicTestExecutor dte = new DynamicTestExecutorImpl4("TestDate", null);
        assertFalse("The code MUST be implemented", dte.doTest());
        dte = new DynamicTestExecutorImpl4("TestDate", "mutant_1");
        assertTrue(ERROR_NOT_DETECTED + " calcul des années bissextiles", dte.doTest());
    }

    @Test
    public void kill_mutant_2() throws Exception {
        DynamicTestExecutor dte = new DynamicTestExecutorImpl4("TestDate", null);
        assertFalse("The code MUST be implemented", dte.doTest());
        dte = new DynamicTestExecutorImpl4("TestDate", "mutant_2");
        assertTrue(ERROR_NOT_DETECTED + " (line 10 : && --> ||)", dte.doTest());
    }

    @Test
    public void kill_mutant_26() throws Exception {
        DynamicTestExecutor dte = new DynamicTestExecutorImpl4("TestDate", null);
        assertFalse("The code MUST be implemented", dte.doTest());
        dte = new DynamicTestExecutorImpl4("TestDate", "mutant_26");
        assertTrue(ERROR_NOT_DETECTED + " Verifier nb max jours octobre", dte.doTest());
    }

    @Test
    public void kill_mutant_27() throws Exception {
        DynamicTestExecutor dte = new DynamicTestExecutorImpl4("TestDate", null);
        assertFalse("The code MUST be implemented", dte.doTest());
        dte = new DynamicTestExecutorImpl4("TestDate", "mutant_27");
        assertTrue(ERROR_NOT_DETECTED + " Verifier nb max jours novembre", dte.doTest());
    }
}
