package geom;

public class Triangle {

    // Cette classe n'est pas écrite comme demandé dans l'énoncé.
    // À vous de faire les modifications qui s'imposent.
    // En particulier, ses attributs ne sont pas des points mais des segments
    private Point p1, p2, p3;

    // Il faut créer les segments à partir d'eux.
    public Triangle(Point p1, Point p2, Point p3) {
        this.p1 = p1;
        this.p2 = p2;
        this.p3 = p3;
    }

    // Les getters et les setters concernent bien les points et non les segments.
    public Point getP1() {
        return p1;
    }

    public Point getP2() {
        return p2;
    }

    public Point getP3() {
        return p3;
    }

    public void setP1(Point p1) {
        this.p1 = p1;
    }

    public void setP2(Point p2) {
        this.p2 = p2;
    }

    public void setP3(Point p3) {
        this.p3 = p3;
    }

    public double getPerimetre() {
        return Double.NaN;
    }

    public Point getBaryCentre() {
        return null;
    }

    public double getSurface() {
        return Double.NaN;
    }

    public String toString() {
        return "";
    }
}
