package cf.geom;

import caseine.tags.*;

@ToDo(replacement = "/home/yvan/Development/Caseine/VPL-DESIGN-PLUGIN/vpl_design_plugin/Tests/Labs/DemoReplacement2/Triangle.txt")
public class Triangle {

    @ToCheck(priority = 30, grade = 2)
    private Segment s1, s2, s3;

    @ToCheck(value = "Bien relire l'énoncé du constructeur de Triangle", priority = 31, grade = 5)
    public Triangle(Point p1, Point p2, Point p3) {
        this.s1 = new Segment(p1, p2);
        this.s2 = new Segment(p2, p3);
        this.s3 = new Segment(p3, p1);
    }

    @ToCheck(priority = 32, grade = 1)
    public Point getP1() {
        return s1.getP1();
    }

    @ToCheck(priority = 32, grade = 1)
    public Point getP2() {
        return s2.getP1();
    }

    @ToCheck(priority = 32, grade = 1)
    public Point getP3() {
        return s3.getP1();
    }

    @ToCheck(priority = 33, grade = 1)
    public void setP1(Point p1) {
        this.s1 = new Segment(p1, s2.getP1());
        this.s3 = new Segment(s3.getP1(), p1);
    }

    @ToCheck(priority = 33, grade = 1)
    public void setP2(Point p2) {
        this.s1 = new Segment(s1.getP1(), p2);
        this.s2 = new Segment(p2, s3.getP1());
    }

    @ToCheck(priority = 33, grade = 1)
    public void setP3(Point p3) {
        this.s2 = new Segment(s2.getP1(), p3);
        this.s3 = new Segment(p3, s1.getP1());
    }

    @ToCheck(priority = 34, grade = 1)
    @ToCompare(priority = 35, grade = 2)
    public double getPerimetre() {
        return s1.getLongueur() + s2.getLongueur() + s3.getLongueur();
    }

    @ToCheck(priority = 36, grade = 1)
    @ToCompare(priority = 37, grade = 3)
    public Point getBaryCentre() {
        Point a = s1.getP1();
        Point b = s2.getP1();
        Point c = s3.getP1();
        return new Point((a.getX() + b.getX() + c.getX()) / 3, (a.getY() + b.getY() + c.getY()) / 3);
    }

    @ToCheck(priority = 38, grade = 1)
    @ToCompare(priority = 39, grade = 4)
    public double getSurface() {
        double a = s1.getLongueur();
        double b = s2.getLongueur();
        double c = s3.getLongueur();
        double p = (a + b + c) / 2;
        return Math.sqrt(p * (p - a) * (p - b) * (p - c));
    }

    @ToCheck(priority = 40, grade = 1)
    @ToCompare(priority = 41, grade = 2)
    @Override
    public String toString() {
        return "<" + s1 + ", " + s2 + ", " + s3 + ">";
    }
}
