package cf.geom;

import caseine.tags.*;

public class Segment {

    @ToDo("1. Déclarer les attributs p1 et p2, les extrémités de ce segment")
    @ToCheck(priority = 20, grade = 2)
    @GetterToCheck(priority = 22, grade = 2)
    @SetterToCheck(priority = 23, grade = 2)
    private Point p1, p2;

    @ToDo("2. Écrire le constructeur attendant les extrémités de ce segment")
    @ToCheck(priority = 21, grade = 1)
    public Segment(Point p1, Point p2) {
        this.p1 = p1;
        this.p2 = p2;
    }

    @ToDo("3. Écrire un getter pour chaque extrémité")
    public Point getP1() {
        return p1;
    }

    @ToDo
    public void setP1(Point p1) {
        this.p1 = p1;
    }

    @ToDo("4. Écrire un setter pour chaque extrémité")
    public Point getP2() {
        return p2;
    }

    @ToDo
    public void setP2(Point p2) {
        this.p2 = p2;
    }

    @ToDo("5. Écrire getLongueur() qui retourne la longeur de ce segment")
    @ToCheck(priority = 24, grade = 1)
    @ToCompare(priority = 25, grade = 3)
    public double getLongueur() {
        return Math.sqrt((p2.getX() - p1.getX()) * (p2.getX() - p1.getX()) + (p2.getY() - p1.getY()) * (p2.getY() - p1.getY()));
    }

    @ToDo("5. Écrire toString() qui retourne une description de ce segment à l'image : [(1.0, 1.0) ; (1.5, 3.0)]")
    @ToCheck(priority = 26, grade = 1)
    @ToCompare(priority = 27, grade = 3)
    @Override
    public String toString() {
        return "[" + p1 + " ; " + p2 + "]";
    }
}
