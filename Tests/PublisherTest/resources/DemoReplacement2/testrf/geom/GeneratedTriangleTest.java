package geom;

import java.lang.reflect.*;
import org.junit.*;
import static org.junit.Assert.*;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;
import caseine.reflect.ReflectUtilities;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class GeneratedTriangleTest {

    @Test
    @caseine.format.javajunit.Grade(2.000000)
    public void p000030000_checkTriangleFieldS1() {
        System.out.println("Check attribute s1");
        try {
            Field x = ReflectUtilities.parseType("geom.Triangle").getDeclaredField("s1");
            assertTrue("Fix  (modifiers)", Modifier.isPrivate(2) == Modifier.isPrivate(x.getModifiers()));
            assertTrue("Fix  (modifiers)", Modifier.isProtected(2) == Modifier.isProtected(x.getModifiers()));
            assertTrue("Fix  (modifiers)", Modifier.isPublic(2) == Modifier.isPublic(x.getModifiers()));
            assertTrue("Fix  (modifiers)", Modifier.isStatic(2) == Modifier.isStatic(x.getModifiers()));
            assertTrue("", x.getType().equals(ReflectUtilities.parseType("geom.Segment")));
        } catch (Exception ex) {
            fail("");
        }
    }

    @Test
    @caseine.format.javajunit.Grade(2.000000)
    public void p000030000_checkTriangleFieldS2() {
        System.out.println("Check attribute s2");
        try {
            Field x = ReflectUtilities.parseType("geom.Triangle").getDeclaredField("s2");
            assertTrue("Fix  (modifiers)", Modifier.isPrivate(2) == Modifier.isPrivate(x.getModifiers()));
            assertTrue("Fix  (modifiers)", Modifier.isProtected(2) == Modifier.isProtected(x.getModifiers()));
            assertTrue("Fix  (modifiers)", Modifier.isPublic(2) == Modifier.isPublic(x.getModifiers()));
            assertTrue("Fix  (modifiers)", Modifier.isStatic(2) == Modifier.isStatic(x.getModifiers()));
            assertTrue("", x.getType().equals(ReflectUtilities.parseType("geom.Segment")));
        } catch (Exception ex) {
            fail("");
        }
    }

    @Test
    @caseine.format.javajunit.Grade(2.000000)
    public void p000030000_checkTriangleFieldS3() {
        System.out.println("Check attribute s3");
        try {
            Field x = ReflectUtilities.parseType("geom.Triangle").getDeclaredField("s3");
            assertTrue("Fix  (modifiers)", Modifier.isPrivate(2) == Modifier.isPrivate(x.getModifiers()));
            assertTrue("Fix  (modifiers)", Modifier.isProtected(2) == Modifier.isProtected(x.getModifiers()));
            assertTrue("Fix  (modifiers)", Modifier.isPublic(2) == Modifier.isPublic(x.getModifiers()));
            assertTrue("Fix  (modifiers)", Modifier.isStatic(2) == Modifier.isStatic(x.getModifiers()));
            assertTrue("", x.getType().equals(ReflectUtilities.parseType("geom.Segment")));
        } catch (Exception ex) {
            fail("");
        }
    }

    @Test
    @caseine.format.javajunit.Grade(5.000000)
    public void p000031000_checkConstructorTrianglePointPointPoint() {
        System.out.println("Declaration of Bien relire l'énoncé du constructeur de Triangle");
        try {
            Constructor x = ReflectUtilities.parseType("geom.Triangle").getDeclaredConstructor(ReflectUtilities.parseType("geom.Point"), ReflectUtilities.parseType("geom.Point"), ReflectUtilities.parseType("geom.Point"));
            assertTrue("Fix Bien relire l'énoncé du constructeur de Triangle (modifiers)", Modifier.isPrivate(1) == Modifier.isPrivate(x.getModifiers()));
            assertTrue("Fix Bien relire l'énoncé du constructeur de Triangle (modifiers)", Modifier.isProtected(1) == Modifier.isProtected(x.getModifiers()));
            assertTrue("Fix Bien relire l'énoncé du constructeur de Triangle (modifiers)", Modifier.isPublic(1) == Modifier.isPublic(x.getModifiers()));
            assertTrue("Fix Bien relire l'énoncé du constructeur de Triangle (modifiers)", Modifier.isStatic(1) == Modifier.isStatic(x.getModifiers()));
        } catch (Exception ex) {
            fail("Bien relire l'énoncé du constructeur de Triangle");
        }
    }

    @Test
    @caseine.format.javajunit.Grade(1.000000)
    public void p000032000_checkMethodTrianglegetP1() {
        System.out.println("getP1");
        try {
            Class<?> theClass = ReflectUtilities.parseType("geom.Triangle");
            Method x = theClass.getDeclaredMethod("getP1");
            assertTrue("Fix getP1 (modifiers)", Modifier.isPrivate(1) == Modifier.isPrivate(x.getModifiers()));
            assertTrue("Fix getP1 (modifiers)", Modifier.isProtected(1) == Modifier.isProtected(x.getModifiers()));
            assertTrue("Fix getP1 (modifiers)", Modifier.isPublic(1) == Modifier.isPublic(x.getModifiers()));
            assertTrue("Fix getP1 (modifiers)", Modifier.isStatic(1) == Modifier.isStatic(x.getModifiers()));
            assertTrue("Fix getP1 (Return)", x.getReturnType().equals(ReflectUtilities.parseType("geom.Point")));
        } catch (NoSuchMethodException | SecurityException ex) {
            fail("Fix getP1");
        } catch (ClassNotFoundException ex3) {
            fail("Inexisting class " + ex3.toString());
        } catch (Exception ex2) {
            fail("Unexpected Error " + ex2.toString());
        }
    }

    @Test
    @caseine.format.javajunit.Grade(1.000000)
    public void p000032000_checkMethodTrianglegetP2() {
        System.out.println("getP2");
        try {
            Class<?> theClass = ReflectUtilities.parseType("geom.Triangle");
            Method x = theClass.getDeclaredMethod("getP2");
            assertTrue("Fix getP2 (modifiers)", Modifier.isPrivate(1) == Modifier.isPrivate(x.getModifiers()));
            assertTrue("Fix getP2 (modifiers)", Modifier.isProtected(1) == Modifier.isProtected(x.getModifiers()));
            assertTrue("Fix getP2 (modifiers)", Modifier.isPublic(1) == Modifier.isPublic(x.getModifiers()));
            assertTrue("Fix getP2 (modifiers)", Modifier.isStatic(1) == Modifier.isStatic(x.getModifiers()));
            assertTrue("Fix getP2 (Return)", x.getReturnType().equals(ReflectUtilities.parseType("geom.Point")));
        } catch (NoSuchMethodException | SecurityException ex) {
            fail("Fix getP2");
        } catch (ClassNotFoundException ex3) {
            fail("Inexisting class " + ex3.toString());
        } catch (Exception ex2) {
            fail("Unexpected Error " + ex2.toString());
        }
    }

    @Test
    @caseine.format.javajunit.Grade(1.000000)
    public void p000032000_checkMethodTrianglegetP3() {
        System.out.println("getP3");
        try {
            Class<?> theClass = ReflectUtilities.parseType("geom.Triangle");
            Method x = theClass.getDeclaredMethod("getP3");
            assertTrue("Fix getP3 (modifiers)", Modifier.isPrivate(1) == Modifier.isPrivate(x.getModifiers()));
            assertTrue("Fix getP3 (modifiers)", Modifier.isProtected(1) == Modifier.isProtected(x.getModifiers()));
            assertTrue("Fix getP3 (modifiers)", Modifier.isPublic(1) == Modifier.isPublic(x.getModifiers()));
            assertTrue("Fix getP3 (modifiers)", Modifier.isStatic(1) == Modifier.isStatic(x.getModifiers()));
            assertTrue("Fix getP3 (Return)", x.getReturnType().equals(ReflectUtilities.parseType("geom.Point")));
        } catch (NoSuchMethodException | SecurityException ex) {
            fail("Fix getP3");
        } catch (ClassNotFoundException ex3) {
            fail("Inexisting class " + ex3.toString());
        } catch (Exception ex2) {
            fail("Unexpected Error " + ex2.toString());
        }
    }

    @Test
    @caseine.format.javajunit.Grade(1.000000)
    public void p000033000_checkMethodTrianglesetP1Point() {
        System.out.println("setP1");
        try {
            Class<?> theClass = ReflectUtilities.parseType("geom.Triangle");
            Method x = theClass.getDeclaredMethod("setP1", ReflectUtilities.parseType("geom.Point"));
            assertTrue("Fix setP1 (modifiers)", Modifier.isPrivate(1) == Modifier.isPrivate(x.getModifiers()));
            assertTrue("Fix setP1 (modifiers)", Modifier.isProtected(1) == Modifier.isProtected(x.getModifiers()));
            assertTrue("Fix setP1 (modifiers)", Modifier.isPublic(1) == Modifier.isPublic(x.getModifiers()));
            assertTrue("Fix setP1 (modifiers)", Modifier.isStatic(1) == Modifier.isStatic(x.getModifiers()));
            assertTrue("Fix setP1 (Return)", x.getReturnType().equals(ReflectUtilities.parseType("void")));
        } catch (NoSuchMethodException | SecurityException ex) {
            fail("Fix setP1");
        } catch (ClassNotFoundException ex3) {
            fail("Inexisting class " + ex3.toString());
        } catch (Exception ex2) {
            fail("Unexpected Error " + ex2.toString());
        }
    }

    @Test
    @caseine.format.javajunit.Grade(1.000000)
    public void p000033000_checkMethodTrianglesetP2Point() {
        System.out.println("setP2");
        try {
            Class<?> theClass = ReflectUtilities.parseType("geom.Triangle");
            Method x = theClass.getDeclaredMethod("setP2", ReflectUtilities.parseType("geom.Point"));
            assertTrue("Fix setP2 (modifiers)", Modifier.isPrivate(1) == Modifier.isPrivate(x.getModifiers()));
            assertTrue("Fix setP2 (modifiers)", Modifier.isProtected(1) == Modifier.isProtected(x.getModifiers()));
            assertTrue("Fix setP2 (modifiers)", Modifier.isPublic(1) == Modifier.isPublic(x.getModifiers()));
            assertTrue("Fix setP2 (modifiers)", Modifier.isStatic(1) == Modifier.isStatic(x.getModifiers()));
            assertTrue("Fix setP2 (Return)", x.getReturnType().equals(ReflectUtilities.parseType("void")));
        } catch (NoSuchMethodException | SecurityException ex) {
            fail("Fix setP2");
        } catch (ClassNotFoundException ex3) {
            fail("Inexisting class " + ex3.toString());
        } catch (Exception ex2) {
            fail("Unexpected Error " + ex2.toString());
        }
    }

    @Test
    @caseine.format.javajunit.Grade(1.000000)
    public void p000033000_checkMethodTrianglesetP3Point() {
        System.out.println("setP3");
        try {
            Class<?> theClass = ReflectUtilities.parseType("geom.Triangle");
            Method x = theClass.getDeclaredMethod("setP3", ReflectUtilities.parseType("geom.Point"));
            assertTrue("Fix setP3 (modifiers)", Modifier.isPrivate(1) == Modifier.isPrivate(x.getModifiers()));
            assertTrue("Fix setP3 (modifiers)", Modifier.isProtected(1) == Modifier.isProtected(x.getModifiers()));
            assertTrue("Fix setP3 (modifiers)", Modifier.isPublic(1) == Modifier.isPublic(x.getModifiers()));
            assertTrue("Fix setP3 (modifiers)", Modifier.isStatic(1) == Modifier.isStatic(x.getModifiers()));
            assertTrue("Fix setP3 (Return)", x.getReturnType().equals(ReflectUtilities.parseType("void")));
        } catch (NoSuchMethodException | SecurityException ex) {
            fail("Fix setP3");
        } catch (ClassNotFoundException ex3) {
            fail("Inexisting class " + ex3.toString());
        } catch (Exception ex2) {
            fail("Unexpected Error " + ex2.toString());
        }
    }

    @Test
    @caseine.format.javajunit.Grade(1.000000)
    public void p000034000_checkMethodTrianglegetPerimetre() {
        System.out.println("getPerimetre");
        try {
            Class<?> theClass = ReflectUtilities.parseType("geom.Triangle");
            Method x = theClass.getDeclaredMethod("getPerimetre");
            assertTrue("Fix getPerimetre (modifiers)", Modifier.isPrivate(1) == Modifier.isPrivate(x.getModifiers()));
            assertTrue("Fix getPerimetre (modifiers)", Modifier.isProtected(1) == Modifier.isProtected(x.getModifiers()));
            assertTrue("Fix getPerimetre (modifiers)", Modifier.isPublic(1) == Modifier.isPublic(x.getModifiers()));
            assertTrue("Fix getPerimetre (modifiers)", Modifier.isStatic(1) == Modifier.isStatic(x.getModifiers()));
            assertTrue("Fix getPerimetre (Return)", x.getReturnType().equals(ReflectUtilities.parseType("double")));
        } catch (NoSuchMethodException | SecurityException ex) {
            fail("Fix getPerimetre");
        } catch (ClassNotFoundException ex3) {
            fail("Inexisting class " + ex3.toString());
        } catch (Exception ex2) {
            fail("Unexpected Error " + ex2.toString());
        }
    }

    @Test
    @caseine.format.javajunit.Grade(2.000000)
    public void p000035000_testMethodTrianglegetPerimetre() {
        System.out.println("Test Method Triangle.getPerimetre");
        try {
            Class<?> classRef = cf.geom.Triangle.class;
            Class<?> classToTest = ReflectUtilities.parseType("geom.Triangle");
            for (int i = 0; i < 100; ++i) {
                StringBuilder msg = new StringBuilder("Fix Triangle.getPerimetre() --> ");
                boolean result = ReflectUtilities.sameResult(msg, classRef, classToTest, "getPerimetre");
                assertTrue(msg.toString(), result);
            }
        } catch (Exception ex) {
            fail("Fix Triangle.getPerimetre() ");
        }
    }

    @Test
    @caseine.format.javajunit.Grade(1.000000)
    public void p000036000_checkMethodTrianglegetBaryCentre() {
        System.out.println("getBaryCentre");
        try {
            Class<?> theClass = ReflectUtilities.parseType("geom.Triangle");
            Method x = theClass.getDeclaredMethod("getBaryCentre");
            assertTrue("Fix getBaryCentre (modifiers)", Modifier.isPrivate(1) == Modifier.isPrivate(x.getModifiers()));
            assertTrue("Fix getBaryCentre (modifiers)", Modifier.isProtected(1) == Modifier.isProtected(x.getModifiers()));
            assertTrue("Fix getBaryCentre (modifiers)", Modifier.isPublic(1) == Modifier.isPublic(x.getModifiers()));
            assertTrue("Fix getBaryCentre (modifiers)", Modifier.isStatic(1) == Modifier.isStatic(x.getModifiers()));
            assertTrue("Fix getBaryCentre (Return)", x.getReturnType().equals(ReflectUtilities.parseType("geom.Point")));
        } catch (NoSuchMethodException | SecurityException ex) {
            fail("Fix getBaryCentre");
        } catch (ClassNotFoundException ex3) {
            fail("Inexisting class " + ex3.toString());
        } catch (Exception ex2) {
            fail("Unexpected Error " + ex2.toString());
        }
    }

    @Test
    @caseine.format.javajunit.Grade(3.000000)
    public void p000037000_testMethodTrianglegetBaryCentre() {
        System.out.println("Test Method Triangle.getBaryCentre");
        try {
            Class<?> classRef = cf.geom.Triangle.class;
            Class<?> classToTest = ReflectUtilities.parseType("geom.Triangle");
            for (int i = 0; i < 100; ++i) {
                StringBuilder msg = new StringBuilder("Fix Triangle.getBaryCentre() --> ");
                boolean result = ReflectUtilities.sameResult(msg, classRef, classToTest, "getBaryCentre");
                assertTrue(msg.toString(), result);
            }
        } catch (Exception ex) {
            fail("Fix Triangle.getBaryCentre() ");
        }
    }

    @Test
    @caseine.format.javajunit.Grade(1.000000)
    public void p000038000_checkMethodTrianglegetSurface() {
        System.out.println("getSurface");
        try {
            Class<?> theClass = ReflectUtilities.parseType("geom.Triangle");
            Method x = theClass.getDeclaredMethod("getSurface");
            assertTrue("Fix getSurface (modifiers)", Modifier.isPrivate(1) == Modifier.isPrivate(x.getModifiers()));
            assertTrue("Fix getSurface (modifiers)", Modifier.isProtected(1) == Modifier.isProtected(x.getModifiers()));
            assertTrue("Fix getSurface (modifiers)", Modifier.isPublic(1) == Modifier.isPublic(x.getModifiers()));
            assertTrue("Fix getSurface (modifiers)", Modifier.isStatic(1) == Modifier.isStatic(x.getModifiers()));
            assertTrue("Fix getSurface (Return)", x.getReturnType().equals(ReflectUtilities.parseType("double")));
        } catch (NoSuchMethodException | SecurityException ex) {
            fail("Fix getSurface");
        } catch (ClassNotFoundException ex3) {
            fail("Inexisting class " + ex3.toString());
        } catch (Exception ex2) {
            fail("Unexpected Error " + ex2.toString());
        }
    }

    @Test
    @caseine.format.javajunit.Grade(4.000000)
    public void p000039000_testMethodTrianglegetSurface() {
        System.out.println("Test Method Triangle.getSurface");
        try {
            Class<?> classRef = cf.geom.Triangle.class;
            Class<?> classToTest = ReflectUtilities.parseType("geom.Triangle");
            for (int i = 0; i < 100; ++i) {
                StringBuilder msg = new StringBuilder("Fix Triangle.getSurface() --> ");
                boolean result = ReflectUtilities.sameResult(msg, classRef, classToTest, "getSurface");
                assertTrue(msg.toString(), result);
            }
        } catch (Exception ex) {
            fail("Fix Triangle.getSurface() ");
        }
    }

    @Test
    @caseine.format.javajunit.Grade(1.000000)
    public void p000040000_checkMethodTriangletoString() {
        System.out.println("toString");
        try {
            Class<?> theClass = ReflectUtilities.parseType("geom.Triangle");
            Method x = theClass.getDeclaredMethod("toString");
            assertTrue("Fix toString (modifiers)", Modifier.isPrivate(1) == Modifier.isPrivate(x.getModifiers()));
            assertTrue("Fix toString (modifiers)", Modifier.isProtected(1) == Modifier.isProtected(x.getModifiers()));
            assertTrue("Fix toString (modifiers)", Modifier.isPublic(1) == Modifier.isPublic(x.getModifiers()));
            assertTrue("Fix toString (modifiers)", Modifier.isStatic(1) == Modifier.isStatic(x.getModifiers()));
            assertTrue("Fix toString (Return)", x.getReturnType().equals(ReflectUtilities.parseType("java.lang.String")));
        } catch (NoSuchMethodException | SecurityException ex) {
            fail("Fix toString");
        } catch (ClassNotFoundException ex3) {
            fail("Inexisting class " + ex3.toString());
        } catch (Exception ex2) {
            fail("Unexpected Error " + ex2.toString());
        }
    }

    @Test
    @caseine.format.javajunit.Grade(2.000000)
    public void p000041000_testMethodTriangletoString() {
        System.out.println("Test Method Triangle.toString");
        try {
            Class<?> classRef = cf.geom.Triangle.class;
            Class<?> classToTest = ReflectUtilities.parseType("geom.Triangle");
            for (int i = 0; i < 100; ++i) {
                StringBuilder msg = new StringBuilder("Fix Triangle.toString() --> ");
                boolean result = ReflectUtilities.sameResult(msg, classRef, classToTest, "toString");
                assertTrue(msg.toString(), result);
            }
        } catch (Exception ex) {
            fail("Fix Triangle.toString() ");
        }
    }
}
