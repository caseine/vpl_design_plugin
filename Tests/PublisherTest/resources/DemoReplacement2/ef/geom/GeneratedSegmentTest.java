package geom;

import java.lang.reflect.*;
import org.junit.*;
import static org.junit.Assert.*;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;
import caseine.reflect.ReflectUtilities;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class GeneratedSegmentTest {

    @Test
    @caseine.format.javajunit.Grade(0.5882352941176471)
    public void p000020000_checkSegmentFieldP1() {
        System.out.println("Check attribute p1");
        try {
            Field x = ReflectUtilities.parseType("geom.Segment").getDeclaredField("p1");
            assertTrue("Fix  (modifiers)", Modifier.isPrivate(2) == Modifier.isPrivate(x.getModifiers()));
            assertTrue("Fix  (modifiers)", Modifier.isProtected(2) == Modifier.isProtected(x.getModifiers()));
            assertTrue("Fix  (modifiers)", Modifier.isPublic(2) == Modifier.isPublic(x.getModifiers()));
            assertTrue("Fix  (modifiers)", Modifier.isStatic(2) == Modifier.isStatic(x.getModifiers()));
            assertTrue("", x.getType().equals(ReflectUtilities.parseType("geom.Point")));
        } catch (Exception ex) {
            fail("");
        }
    }

    @Test
    @caseine.format.javajunit.Grade(0.5882352941176471)
    public void p000020000_checkSegmentFieldP2() {
        System.out.println("Check attribute p2");
        try {
            Field x = ReflectUtilities.parseType("geom.Segment").getDeclaredField("p2");
            assertTrue("Fix  (modifiers)", Modifier.isPrivate(2) == Modifier.isPrivate(x.getModifiers()));
            assertTrue("Fix  (modifiers)", Modifier.isProtected(2) == Modifier.isProtected(x.getModifiers()));
            assertTrue("Fix  (modifiers)", Modifier.isPublic(2) == Modifier.isPublic(x.getModifiers()));
            assertTrue("Fix  (modifiers)", Modifier.isStatic(2) == Modifier.isStatic(x.getModifiers()));
            assertTrue("", x.getType().equals(ReflectUtilities.parseType("geom.Point")));
        } catch (Exception ex) {
            fail("");
        }
    }

    @Test
    @caseine.format.javajunit.Grade(0.29411764705882354)
    public void p000021000_checkConstructorSegmentPointPoint() {
        System.out.println("Declaration of Constructor of geom.Segment");
        try {
            Constructor x = ReflectUtilities.parseType("geom.Segment").getDeclaredConstructor(ReflectUtilities.parseType("geom.Point"), ReflectUtilities.parseType("geom.Point"));
            assertTrue("Fix  (modifiers)", Modifier.isPrivate(1) == Modifier.isPrivate(x.getModifiers()));
            assertTrue("Fix  (modifiers)", Modifier.isProtected(1) == Modifier.isProtected(x.getModifiers()));
            assertTrue("Fix  (modifiers)", Modifier.isPublic(1) == Modifier.isPublic(x.getModifiers()));
            assertTrue("Fix  (modifiers)", Modifier.isStatic(1) == Modifier.isStatic(x.getModifiers()));
        } catch (Exception ex) {
            fail("Constructor of geom.Segment");
        }
    }

    @Test
    @caseine.format.javajunit.Grade(0.5882352941176471)
    public void p000022000_checkGetterSegmentP1() {
        System.out.println("Check getter p1");
        try {
            for (int i = 0; i < 100; ++i) {
                Object o = ReflectUtilities.randomValue(ReflectUtilities.parseType("geom.Segment"));
                Object attr = ReflectUtilities.getAttribut(ReflectUtilities.parseType("geom.Segment"), o, "p1");
                Object getAttr = ReflectUtilities.getFromMethod(ReflectUtilities.parseType("geom.Segment"), o, "getP1");
                assertTrue("Fix getP1", ReflectUtilities.equals(attr, getAttr));
            }
        } catch (Exception ex) {
            fail("Fix getter of p1");
        }
    }

    @Test
    @caseine.format.javajunit.Grade(0.5882352941176471)
    public void p000022000_checkGetterSegmentP2() {
        System.out.println("Check getter p2");
        try {
            for (int i = 0; i < 100; ++i) {
                Object o = ReflectUtilities.randomValue(ReflectUtilities.parseType("geom.Segment"));
                Object attr = ReflectUtilities.getAttribut(ReflectUtilities.parseType("geom.Segment"), o, "p2");
                Object getAttr = ReflectUtilities.getFromMethod(ReflectUtilities.parseType("geom.Segment"), o, "getP2");
                assertTrue("Fix getP2", ReflectUtilities.equals(attr, getAttr));
            }
        } catch (Exception ex) {
            fail("Fix getter of p2");
        }
    }

    @Test
    @caseine.format.javajunit.Grade(0.5882352941176471)
    public void p000023000_checkSetterSegmentp1() {
        System.out.println("Check setter p1");
        try {
            Method x = ReflectUtilities.parseType("geom.Segment").getDeclaredMethod("setP1", ReflectUtilities.parseType("geom.Point"));
            assertTrue("Fix setP1", x.getReturnType().equals(void.class));
            Object o = ReflectUtilities.randomValue(ReflectUtilities.parseType("geom.Segment"));
            for (int i = 0; i < 100; ++i) {
                Point v = (Point) ReflectUtilities.randomValue(Point.class);
                ReflectUtilities.getFromMethodTA(ReflectUtilities.parseType("geom.Segment"), o, "setP1", ReflectUtilities.parseType("geom.Point"), v);
                assertTrue("Fix setP1", ReflectUtilities.getAttribut(ReflectUtilities.parseType("geom.Segment"), o, "p1").equals(v));
            }
        } catch (Exception ex) {
            fail("Fix setP1");
        }
    }

    @Test
    @caseine.format.javajunit.Grade(0.5882352941176471)
    public void p000023000_checkSetterSegmentp2() {
        System.out.println("Check setter p2");
        try {
            Method x = ReflectUtilities.parseType("geom.Segment").getDeclaredMethod("setP2", ReflectUtilities.parseType("geom.Point"));
            assertTrue("Fix setP2", x.getReturnType().equals(void.class));
            Object o = ReflectUtilities.randomValue(ReflectUtilities.parseType("geom.Segment"));
            for (int i = 0; i < 100; ++i) {
                Point v = (Point) ReflectUtilities.randomValue(Point.class);
                ReflectUtilities.getFromMethodTA(ReflectUtilities.parseType("geom.Segment"), o, "setP2", ReflectUtilities.parseType("geom.Point"), v);
                assertTrue("Fix setP2", ReflectUtilities.getAttribut(ReflectUtilities.parseType("geom.Segment"), o, "p2").equals(v));
            }
        } catch (Exception ex) {
            fail("Fix setP2");
        }
    }

    @Test
    @caseine.format.javajunit.Grade(0.29411764705882354)
    public void p000024000_checkMethodSegmentgetLongueur() {
        System.out.println("getLongueur");
        try {
            Class<?> theClass = ReflectUtilities.parseType("geom.Segment");
            Method x = theClass.getDeclaredMethod("getLongueur");
            assertTrue("Fix getLongueur (modifiers)", Modifier.isPrivate(1) == Modifier.isPrivate(x.getModifiers()));
            assertTrue("Fix getLongueur (modifiers)", Modifier.isProtected(1) == Modifier.isProtected(x.getModifiers()));
            assertTrue("Fix getLongueur (modifiers)", Modifier.isPublic(1) == Modifier.isPublic(x.getModifiers()));
            assertTrue("Fix getLongueur (modifiers)", Modifier.isStatic(1) == Modifier.isStatic(x.getModifiers()));
            assertTrue("Fix getLongueur (Return)", x.getReturnType().equals(ReflectUtilities.parseType("double")));
        } catch (NoSuchMethodException | SecurityException ex) {
            fail("Fix getLongueur");
        } catch (ClassNotFoundException ex3) {
            fail("Inexisting class " + ex3.toString());
        } catch (Exception ex2) {
            fail("Unexpected Error " + ex2.toString());
        }
    }

    @Test
    @caseine.format.javajunit.Grade(0.8823529411764706)
    public void p000025000_testMethodSegmentgetLongueur() {
        System.out.println("Test Method Segment.getLongueur");
        try {
            Class<?> classRef = cf.geom.Segment.class;
            Class<?> classToTest = ReflectUtilities.parseType("geom.Segment");
            for (int i = 0; i < 100; ++i) {
                StringBuilder msg = new StringBuilder("Fix Segment.getLongueur() --> ");
                boolean result = ReflectUtilities.sameResult(msg, classRef, classToTest, "getLongueur");
                assertTrue(msg.toString(), result);
            }
        } catch (Exception ex) {
            fail("Fix Segment.getLongueur() ");
        }
    }

    @Test
    @caseine.format.javajunit.Grade(0.29411764705882354)
    public void p000026000_checkMethodSegmenttoString() {
        System.out.println("toString");
        try {
            Class<?> theClass = ReflectUtilities.parseType("geom.Segment");
            Method x = theClass.getDeclaredMethod("toString");
            assertTrue("Fix toString (modifiers)", Modifier.isPrivate(1) == Modifier.isPrivate(x.getModifiers()));
            assertTrue("Fix toString (modifiers)", Modifier.isProtected(1) == Modifier.isProtected(x.getModifiers()));
            assertTrue("Fix toString (modifiers)", Modifier.isPublic(1) == Modifier.isPublic(x.getModifiers()));
            assertTrue("Fix toString (modifiers)", Modifier.isStatic(1) == Modifier.isStatic(x.getModifiers()));
            assertTrue("Fix toString (Return)", x.getReturnType().equals(ReflectUtilities.parseType("java.lang.String")));
        } catch (NoSuchMethodException | SecurityException ex) {
            fail("Fix toString");
        } catch (ClassNotFoundException ex3) {
            fail("Inexisting class " + ex3.toString());
        } catch (Exception ex2) {
            fail("Unexpected Error " + ex2.toString());
        }
    }

    @Test
    @caseine.format.javajunit.Grade(0.8823529411764706)
    public void p000027000_testMethodSegmenttoString() {
        System.out.println("Test Method Segment.toString");
        try {
            Class<?> classRef = cf.geom.Segment.class;
            Class<?> classToTest = ReflectUtilities.parseType("geom.Segment");
            for (int i = 0; i < 100; ++i) {
                StringBuilder msg = new StringBuilder("Fix Segment.toString() --> ");
                boolean result = ReflectUtilities.sameResult(msg, classRef, classToTest, "toString");
                assertTrue(msg.toString(), result);
            }
        } catch (Exception ex) {
            fail("Fix Segment.toString() ");
        }
    }
}
