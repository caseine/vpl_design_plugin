package geom;

public class Triangle {

    private Segment s1, s2, s3;

    public Triangle(Point p1, Point p2, Point p3) {
        this.s1 = new Segment(p1, p2);
        this.s2 = new Segment(p2, p3);
        this.s3 = new Segment(p3, p1);
    }

    public Point getP1() {
        return s1.getP1();
    }

    public Point getP2() {
        return s2.getP1();
    }

    public Point getP3() {
        return s3.getP1();
    }

    public void setP1(Point p1) {
        this.s1 = new Segment(p1, s2.getP1());
        this.s3 = new Segment(s3.getP1(), p1);
    }

    public void setP2(Point p2) {
        this.s1 = new Segment(s1.getP1(), p2);
        this.s2 = new Segment(p2, s3.getP1());
    }

    public void setP3(Point p3) {
        this.s2 = new Segment(s2.getP1(), p3);
        this.s3 = new Segment(p3, s1.getP1());
    }

    public double getPerimetre() {
        return s1.getLongueur() + s2.getLongueur() + s3.getLongueur();
    }

    public Point getBaryCentre() {
        Point a = s1.getP1();
        Point b = s2.getP1();
        Point c = s3.getP1();
        return new Point((a.getX() + b.getX() + c.getX()) / 3, (a.getY() + b.getY() + c.getY()) / 3);
    }

    public double getSurface() {
        double a = s1.getLongueur();
        double b = s2.getLongueur();
        double c = s3.getLongueur();
        double p = (a + b + c) / 2;
        return Math.sqrt(p * (p - a) * (p - b) * (p - c));
    }

    @Override
    public String toString() {
        return "<" + s1 + ", " + s2 + ", " + s3 + ">";
    }
}
