/*
 * @author : Yvan Maillot (yvan.maillot@uha.fr)
 */
package edu.uha.miage;

import caseine.tags.*;
import static java.lang.Math.sqrt;
import java.util.Scanner;

/**
 *
 * @author yvan
 */
@RelativeEvaluation
@ClassTestPriority(1)
public class Main {

    /**
     * Petite procédure d'exemple qui, comme son nom l'indique, lit une valeur
     * de type int puis une valeur de type double, puis les affiche ainsi que
     * leur produit.
     *
     * L'objectif de cet exemple est de montrer quelques rudiments pour vous
     * aider à réaliser la suite de l'exercice.
     *
     * Vous pouvez exécuter le programme pour voir ce que ça donne.
     *
     * ATTENTION : Je me répète mais deux précautions valent mieux qu'une. Selon
     * la localisation du système (française ou anglaise par exemple) la lecture
     * au clavier d'un nombre de type double avec Scanner peut varier.
     *
     * - Si le système est anglais (comme c'est le cas sur la plateforme
     * CaseInE) alors les nombres de type double se lisent au clavier avec un
     * point par exemple 3.14
     *
     * - Si le système est français (comme c'est probablement le cas sur votre
     * machine) alors les nombres de type double se lisent au clavier avec une
     * virgule par exemple 3,14
     *
     * Donc si la saisie d'un nombre de type double entraine une erreur (de type
     * InputMismatchException) cela signifie que vous avez tapé un point à la
     * place d'une virgule ou inversement, selon votre système.
     */
    public static void lireUnEntierUnDoubleAuClavierPuisLesAfficherAinsiQueLeurProduit() {
        // Scanner pour lire des valeurs au clavier
        Scanner in = new Scanner(System.in);
        // Quelques déclarations de variables nécessaires au programme
        int a;
        double x;
        // Une invitation à entrer des valeurs au clavier dans un ordre bien déterminé
        System.out.print("Entrez un entier puis un double: ");
        // Lecture au clavier d'un entier mis dans la variable a
        a = in.nextInt();
        // Lecture au clavier d'un nombre de type double mis dans la variable x
        x = in.nextDouble();
        /*
         * ATTENTION : selon la localisation du système (française ou anglaise par exemple)
         * la lecture au clavier d'un nombre de type double avec Scanner peut varier.
         *   - Si le système est anglais (comme c'est le cas sur la plateforme CaseInE)
         *     alors les nombres de type double se lisent au clavier avec un point
         *     par exemple 3.14
         * 
         *   - Si le système est français (comme c'est probablement le cas sur votre machine)
         *     alors les nombres de type double se lisent au clavier avec une virgule
         *     par exemple 3,14
         * 
         * Donc si la saisie d'un nombre de type double entraine une erreur (de type
         * InputMismatchException) cela signifie que vous avez tapé un point à la place
         * d'une virgule ou inversement, selon votre système.
         */

        // Affichage des valeurs saisies et de leur produit.
        System.out.println("L’entier : " + a);
        System.out.println("Le double : " + x);
        System.out.println("Leur produit : " + a * x);
    }

    public static void main(String[] args) {
        // DEMO
        lireUnEntierUnDoubleAuClavierPuisLesAfficherAinsiQueLeurProduit();
    }

    /**
     * Procédure qui lit un entier (int) c qui représente une température en
     * Celcius et qui affiche ensuite sa correspondance en Farenheit sous la
     * forme d'un nombre à virgule à l'aide de la formule f = 1,8c + 32
     *
     * Exemple d'exécution :
     *
     * Entrez une température en degré Celsius : 10 
     * 
     * 10°C font 50.0°F
     */
    @ToDoIn("01\n\tÉcrire cette procédure qui "
            + "\n\t\t - lit un INT au clavier qui est une température en degré Celsius "
            + "\n\t\t - affiche cette température en Celsius et en Farenheit\n\t")
    public static void lireTemperatureEnCelsiusAfficherEnFarenheit() {
        Scanner in = new Scanner(System.in);
        System.out.print("Entrez une température en degré Celsius : ");
        int c = in.nextInt();
        System.out.println(c + "°C font " + (1.8 * c + 32) + "°F");
    }

    /**
     * Procédure qui lit un entier (int) c qui représente une température en
     * Celcius et qui affiche ensuite sa correspondance en Farenheit sous la
     * forme d'un nombre à virgule à l'aide de la formule c = 5(f - 32)/9
     *
     * ATTENTION AUX PIÈGES
     *
     * Exemple d'exécution :
     *
     * Entrez une température en degré Farenheit : 50 50°F font 10.0°C
     */
    @ToDoIn("02\n\tÉcrire cette procédure qui "
            + "\n\t\t - lit un INT au clavier qui est une température en degré Farenheit"
            + "\n\t\t - affiche cette température en Farenheit et en Celsius\n\t")
    public static void lireTemperatureEnFarenheitAfficherEnCelsius() {
        Scanner in = new Scanner(System.in);
        System.out.print("Entrez une température en degré Farenheit : ");
        int f = in.nextInt();
        System.out.println(f + "°F font " + (5 * (f - 32.0) / 9.0) + "°C");
    }

    /**
     * Procédure qui lit au clavier le rayon d'un cercle (un nombre à virgule)
     * et affiche son aire et son périmètre.
     *
     * Exemple d'exécution :
     *
     * Entrez le rayon d'un cercle : 3,2 Son aire : 32.169908772759484 Son
     * périmètre : 20.106192982974676
     *
     * Rappels : - PI se trouve dans la classe Math (lire le cours, si
     * nécessaire) - Trouvez vous-même les formules de l'aire et du périmètre
     * d'un cercle.
     */
    @ToDoIn("03\n\tÉcrire cette procédure qui "
            + "\n\t\t - lit au clavier le rayon d'un cercle et "
            + "\n\t\t - affiche son aire et son périmère\n\t")
    public static void lireLeRayonDunCercleAuClavierEtAfficherSonAireEtSonPerimetre() {
        Scanner in = new Scanner(System.in);
        System.out.print("Entrez le rayon d'un cercle : ");
        double r = in.nextDouble();
        System.out.println("Son aire : " + Math.PI * r * r);
        System.out.println("Son périmètre : " + 2 * Math.PI * r);
    }

    /**
     * Procédure qui lit au clavier la largeur et la longueur d'un rectangle
     * (deux nombres à virgule) affiche son aire et son périmètre.
     *
     * Exemple d'exécution :
     *
     * Entrez la largeur et la longueur d'un rectangle : 3,2 5,8 Son aire :
     * 18.56 Son périmètre : 18
     */
    @ToDoIn("04\n\tÉcrire cette procédure qui "
            + "\n\t\t - lit au clavier la largeur et la longueur d'un rectangle "
            + "\n\t\t - et affiche son aire et son périmère\n\t")
    public static void lireLargeurEtLongueurDunRectangleAuClavierEtAfficherSonAireEtSonPerimetre() {
        Scanner in = new Scanner(System.in);
        System.out.print("Entrez la largeur et la longueur d'un rectangle : ");
        double largeur = in.nextDouble();
        double longueur = in.nextDouble();
        System.out.println("Son aire : " + largeur * longueur);
        System.out.println("Son périmètre : " + 2 * (largeur + longueur));
    }

    /**
     * Procédure qui lit au clavier les longueurs des côtés d'un triangle puis
     * calcule son aire en appelant la fonction aireTriangle() et l'affiche.
     *
     * Il n'y a rien à changer dans cette procédure, mais c'est la fonction
     * aireTriangle() qu'il faut écrire.
     *
     */
    public static void lireCotesTriangleEtAfficherSonAire() {
        Scanner in = new Scanner(System.in);
        System.out.print("Entrez les longueurs des 3 côtés d'un triangle : ");
        double a = in.nextDouble();
        double b = in.nextDouble();
        double c = in.nextDouble();
        System.out.println("L'aire de ce triangle : " + aireTriangle(a, b, c));
    }

    /**
     * Fonction qui retourne l'aire d'un triangle à partir de la longueur de ses
     * 3 côtés à l'aide de la formule de Héron (
     * https://fr.wikipedia.org/wiki/Formule_de_Héron ).
     *
     * @param a longueur d'un côté
     * @param b longueur d'un côté
     * @param c longueur d'un côté
     * @return l'aire d'un triangle de côtés a, b, c.
     */
    @ToDoIn("05\n\tÉcrire cette fonction qui retourne"
            + "\n\tl'aire d'un triangle à partir des"
            + "\n\tlongueurs de ses 3 côtés.\n\t")
    @ToCompare(priority = 10, grade = 1)
    private static double aireTriangle(double a, double b, double c) {
        double dp = (a + b + c) / 2;
        return sqrt(dp * (dp - a) * (dp - b) * (dp - c));
    }

}
