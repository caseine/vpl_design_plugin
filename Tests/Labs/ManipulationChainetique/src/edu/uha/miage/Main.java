/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.uha.miage;

/**
 *
 * @author yvan
 */
public class Main {
    public static void main(String[] args) {
        // Ecrire vos propres tests dans edu.uha.miage.Main
        // Par exemple
        int l = ManipulationChainetique.longueur("Bonjour");
        if (l == 7) {
            System.out.println("OK : la longueur de \"Bonjour\" est bien 7");
        } else {
            System.err.println("Faux : la longueur de \"Bonjour\" devrait être 7 alors qu'on a " + l );
        }
    }
}
