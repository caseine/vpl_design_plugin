/*
 * @author : Yvan Maillot (yvan.maillot@uha.fr)
 */
package edu.uha.miage;

import caseine.tags.*;

/**
 *
 * @author yvan
 */

@ClassTestPriority(5)
public class Triangle {

    @ToCheck(priority = 30, grade = 1)
    @ToDo("3.01. Les trois segments du triangle : s1, s2, s3")
    private Segment s1, s2, s3;

    @ToCheck(priority = 31, grade = 5)
    @ToDo("3.02. Écrire le constructeur avec ses trois sommets")
    public Triangle(Point p1, Point p2, Point p3) {
        this.s1 = new Segment(p1, p2);
        this.s2 = new Segment(p2, p3);
        this.s3 = new Segment(p3, p1);
    }

    @ToCheck(priority = 32, grade = 2)
    @ToDo("3.03. getP1() qui retourne le premier sommet de ce triangle")
    public Point getP1() {
        return s1.getP1();
    }

    @ToDo("3.04. getP2() qui retourne le deuxième sommet de ce triangle")
    @ToCheck(priority = 32, grade = 2)
    public Point getP2() {
        return s2.getP1();
    }

    @ToDo("3.05. getP3() qui retourne le troisième sommet de ce triangle")
    @ToCheck(priority = 32, grade = 2)
    public Point getP3() {
        return s3.getP1();
    }

    @ToCheck(priority = 33, grade = 2)
    @ToDo("3.06. setP1() qui met à jour le premier sommet de ce triangle")
    public void setP1(Point p1) {
        this.s1 = new Segment(p1, s2.getP1());
        this.s3 = new Segment(s3.getP1(), p1);
    }

    @ToDo("3.07. setP2() qui met à jour le deuxième sommet de ce triangle")
    @ToCheck(priority = 33, grade = 2)
    public void setP2(Point p2) {
        this.s1 = new Segment(s1.getP1(), p2);
        this.s2 = new Segment(p2, s3.getP1());
    }

    @ToDo("3.08. setP3() qui met à jour le troisième sommet de ce triangle")
    @ToCheck(priority = 33, grade = 2)
    public void setP3(Point p3) {
        this.s2 = new Segment(s2.getP1(), p3);
        this.s3 = new Segment(p3, s1.getP1());
    }

    @ToDo("3.05. Retourner le périmètre de ce triangle")
    @ToCheck(value="Signature of Triangle.getPerimetre()", priority = 34, grade = 1)
    @ToCompare(value="Behavior of Triangle.getPerimetre()", priority = 35, grade = 5)
    public double getPerimetre() {
        return s1.getLongueur() + s2.getLongueur() + s3.getLongueur();
    }

    @ToCheck(value="Signature of Triangle.getSurface()", priority = 36, grade = 1)
    @ToDo("3.06. Retourner la surface de ce triangle")
    @ToCompare(value="Behavior of Triangle.getSurface()", priority = 37, grade = 5)
    public double getSurface() {
        double a = s1.getLongueur();
        double b = s2.getLongueur();
        double c = s3.getLongueur();
        double p = (a + b + c) / 2;
        return Math.sqrt(p * (p - a) * (p - b) * (p - c));
    }

    @ToDo("3.07. Retourner le barycentre de ce triangle")
    @ToCheck(value="Signature of Triangle.getBaryCentre()", priority = 38, grade = 1)
    @ToCompare(value="Behavior of Triangle.getBaryCentre()", priority = 39, grade = 5)
    public Point getBaryCentre() {
        Point a = s1.getP1();
        Point b = s2.getP1();
        Point c = s3.getP1();

        return new Point((a.getX() + b.getX() + c.getX()) / 3, (a.getY() + b.getY() + c.getY()) / 3);
    }

    @Override
    @ToCheck(priority = 40, grade = 1)
    @ToDo("3.08. La méthode toString()")
    public String toString() {
        return "<" + s1 + ", " + s2 + ", " + s3 + ">";
    }
}
