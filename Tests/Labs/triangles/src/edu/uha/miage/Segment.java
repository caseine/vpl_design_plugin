/*
 * @author : Yvan Maillot (yvan.maillot@uha.fr)
 */
package edu.uha.miage;

import caseine.tags.*;

/**
 *
 * @author yvan
 */
@ClassTestPriority(3)
public class Segment {
    @ToCheck(priority = 20, grade = 1)
    @GetterToCheck(priority = 26, grade = 1)
    @SetterToCheck(priority = 27, grade = 1)
    @ToDo("2.01. Déclarer les attributs p1 et p2, les extrémités de ce segment")
    private Point p1, p2;

    @ToCheck(priority = 21, grade = 1)
    @ToDo("2.02. Écrire le constructeur attendant les extrémités de ce segment")
    public Segment(Point p1, Point p2) {
        this.p1 = p1;
        this.p2 = p2;
    }

    @ToDo("2.03. Écrire un getter pour chaque extrémité")
    @ToCheck(priority = 22, grade = 1)
    public Point getP1() {
        return p1;
    }

    @ToDo
    @ToCheck(priority = 23, grade = 1)
    public void setP1(Point p1) {
        this.p1 = p1;
    }

    @ToDo("2.04. Écrire un setter pour chaque extrémité")
    @ToCheck(priority = 22, grade = 1)
    public Point getP2() {
        return p2;
    }

    @ToDo
    @ToCheck(priority = 23, grade = 1)
    public void setP2(Point p2) {
        this.p2 = p2;
    }
    
    @ToDo("2.05. Écrire getLongueur() qui retourne la longueur de ce segment")
    @ToCheck(value = "Signature getLongueur() of Segment", priority = 24, grade = 1)
    @ToCompare(value = "Behavior getLongueur() of Segment", priority = 28, grade = 10)
    public double getLongueur() {
        return Math.sqrt((p2.getX()-p1.getX())*(p2.getX()-p1.getX())+(p2.getY()-p1.getY())*(p2.getY()-p1.getY()));
    }
    
    @Override
    @ToCheck(priority = 25, grade = 1)
    @ToDo("2.06. Redéfinir toString() une représentation de ce segment comme demandé dans l'énoncé, Par exemple [(1.2, 3.4) ; (4.5, 5.6)]")
    public String toString() {
        return "["+p1+" ; "+p2+"]";
    }
}
