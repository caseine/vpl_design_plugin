package edu.uga.date;

import caseine.tags.ToDoIn;

public class Calendrier {

    /**
     * Indique si l'annee passee en parametre est bissextile.
     *
     * Une annee bissextile repond aux criteres suivants :
     * - annee est multiple de 4 (RG01) mais pas de 100 (RG02)
     * - SAUF si annee est multiple de 400 (RG03)
     *
     * REGLES : RG01, RG02, RG03
     *
     * @param annee
     * @return true si l'annee est bissextile et false sinon.
     */
	@ToDoIn
    public boolean isBissextile(final int annee) {

        // RG03
        if (annee % 400 == 0) {
            return true;
        }

        // RG02
        if (annee % 100 == 0) {
            return false;
        }

        // RG01
        if (annee % 4 == 0) {
            return true;
        }

        return false;
    }
}