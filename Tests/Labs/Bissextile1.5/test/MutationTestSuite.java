import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.stream.Stream;

import org.junit.jupiter.api.DynamicTest;
import org.junit.jupiter.api.TestFactory;

import caseine.extra.utils.mutations.DynamicTestExecutor;
import caseine.extra.utils.mutations.DynamicTestExecutorImpl5;
import caseine.extra.utils.mutations.Mutation;

public class MutationTestSuite {
	
    private static final String ERROR_NOT_DETECTED = "Error not detected: ";

	static String parameters[][];
	
	static {
		try {
			parameters = new Mutation("test",  new ArrayList<>()).getMutations();
		} catch (IOException | ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

	@TestFactory
    public Stream<DynamicTest> test_mutations() throws Exception {
		
		Stream<String[]> streamOfParameters = Arrays.stream(parameters);

		return streamOfParameters
			      .map(p -> DynamicTest.dynamicTest(
			    		  p[1], 
			          () -> {
			              DynamicTestExecutor dte = new DynamicTestExecutorImpl5(p[0], p[1]);
			              assertTrue(ERROR_NOT_DETECTED + p[2], dte.doTest());
			   		}
			    ));
    }
}