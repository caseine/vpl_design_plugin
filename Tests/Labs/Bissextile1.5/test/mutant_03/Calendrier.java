package mutant_03;

import caseine.extra.utils.mutations.Mutant;

@Mutant(testClass = "edu.uga.date.CalendrierTest", errorMessage = "RG03")
public class Calendrier {

    
    public boolean isBissextile(final int annee) {

        // RG03
        if (annee % 400 == 0) {
            return false;
        }

        // RG02
        if (annee % 100 == 0) {
            return false;
        }

        // RG01
        if (annee % 4 == 0) {
            return true;
        }

        return false;
    }

}