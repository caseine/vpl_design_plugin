/*
 * @author : Yvan Maillot (yvan.maillot@uha.fr)
 */
package edu.uha.miage;

import caseine.tags.RelativeEvaluation;
import caseine.tags.ToDoIn;

/**
 * Un plan cartésien est divisé en 9 régions, limitées par 
 * deux droites horizontales (y = Y1 et y = Y2, telles que Y1 &lt; Y2) et
 * deux droites verticales (x = X1 et x = X2, telles que X1 &lt; X2)
 * 
 * Voir l'image de l'énoncé.
 * 
 * Le but de l'exercice consistera à écrire des expressions booléennes qui 
 * identifieront si un point de ce plan de coordonnées x et y est dans telle 
 * ou telle région.
 * 
 * Écrivez le corps des méthodes de cette classe de sorte à respecter leur javadoc.
 * 
 * Remarque : les droites horizontales et verticales sont représentées par les
 * constantes X1, X2, Y1 et Y2 déclarées dans ce programme.
 * 
 * @author yvan
 */
@RelativeEvaluation
public class RegionsDuPlan {
    // Les limites verticales et horizontales    
    public final static double X1 = 0.2; // Attention il faut X1 < X2
    public final static double X2 = 0.8;// Attention il faut X1 < X2
    public final static double Y1 = 0.25;// Attention il faut Y1 < Y2
    public final static double Y2 = 0.75;// Attention il faut Y1 < Y2
    /* Vous apprendrez bientôt à comprendre la signification des mots clés public, 
       final, static. 
    
        En attendant, il suffit de comprendre que X1, X2, Y1 et Y2 sont des 
        constantes de type double utilisées pour définir les droites verticales
        et horizontales de l'exercice.
    */
    /**
     * Méthode qui retourne vrai si le point de coordonnées (x, y) se trouve 
     * strictement dans la région 1 .
     * 
     * Le "défi" est de le faire en une seule instruction : le "return"
     * 
     * @param x l'abscisse du point dont on veut savoir s'il est dans (1)
     * @param y l'ordonnée du point dont on veut savoir s'il est dans (1)
     * @return vrai si le point (x,y) est dans (1), faux sinon.
     */
    @ToDoIn("01"
            + "\n\t\tÉcrire estDans1() qui retourne vrai si le point de "
            + "\n\t\tcoordonnées (x, y) se trouve strictement dans la région (1)"
            + "\n\t\tet faux sinon.\n\t")
    public static boolean estDans1(double x, double y) {
        return x < X1 && y > Y2;
    }

    /**
     * Méthode qui retourne vrai si le point de coordonnées (x, y) se trouve 
     * strictement dans la région 2 .
     * 
     * Le "défi" est de le faire en une seule instruction : le "return"
     * 
     * @param x l'abscisse du point dont on veut savoir s'il est dans (2)
     * @param y l'ordonnée du point dont on veut savoir s'il est dans (2)
     * @return vrai si le point (x,y) est dans (2), faux sinon.
     */
    @ToDoIn("02"
            + "\n\t\tÉcrire estDans2() qui retourne vrai si le point de "
            + "\n\t\tcoordonnées (x, y) se trouve strictement dans la région (2)"
            + "\n\t\tet faux sinon.\n\t")
    public static boolean estDans2(double x, double y) {
        return x > X1 && x < X2 && y > Y2;
    }

    /**
     * Méthode qui retourne vrai si le point de coordonnées (x, y) se trouve 
     * strictement dans la région 3 .
     * 
     * Le "défi" est de le faire en une seule instruction : le "return"
     * 
     * @param x l'abscisse du point dont on veut savoir s'il est dans (3)
     * @param y l'ordonnée du point dont on veut savoir s'il est dans (3)
     * @return vrai si le point (x,y) est dans (3), faux sinon.
     */
    @ToDoIn("03"
            + "\n\t\tÉcrire estDans3() qui retourne vrai si le point de "
            + "\n\t\tcoordonnées (x, y) se trouve strictement dans la région (3)"
            + "\n\t\tet faux sinon.\n\t")
    public static boolean estDans3(double x, double y) {
        return x > X2 && y > Y2;
    }

    /**
     * Méthode qui retourne vrai si le point de coordonnées (x, y) se trouve 
     * strictement dans la région 4 .
     * 
     * Le "défi" est de le faire en une seule instruction : le "return"
     * 
     * @param x l'abscisse du point dont on veut savoir s'il est dans (4)
     * @param y l'ordonnée du point dont on veut savoir s'il est dans (4)
     * @return vrai si le point (x,y) est dans (4), faux sinon.
     */
    @ToDoIn("04"
            + "\n\t\tÉcrire estDans4() qui retourne vrai si le point de "
            + "\n\t\tcoordonnées (x, y) se trouve strictement dans la région (4)"
            + "\n\t\tet faux sinon.\n\t")
    public static boolean estDans4(double x, double y) {
        return x > X2 && y > Y1 && y < Y2;
    }

    /**
     * Méthode qui retourne vrai si le point de coordonnées (x, y) se trouve 
     * strictement dans la région 5 .
     * 
     * Le "défi" est de le faire en une seule instruction : le "return"
     * 
     * @param x l'abscisse du point dont on veut savoir s'il est dans (5)
     * @param y l'ordonnée du point dont on veut savoir s'il est dans (5)
     * @return vrai si le point (x,y) est dans (5), faux sinon.
     */
    @ToDoIn("05"
            + "\n\t\tÉcrire estDans5() qui retourne vrai si le point de "
            + "\n\t\tcoordonnées (x, y) se trouve strictement dans la région (5)"
            + "\n\t\tet faux sinon.\n\t")
    public static boolean estDans5(double x, double y) {
        return x > X2 && y < Y1;
    }

    /**
     * Méthode qui retourne vrai si le point de coordonnées (x, y) se trouve 
     * strictement dans la région 6 .
     * 
     * Le "défi" est de le faire en une seule instruction : le "return"
     * 
     * @param x l'abscisse du point dont on veut savoir s'il est dans (6)
     * @param y l'ordonnée du point dont on veut savoir s'il est dans (6)
     * @return vrai si le point (x,y) est dans (6), faux sinon.
     */
    @ToDoIn("06"
            + "\n\t\tÉcrire estDans6() qui retourne vrai si le point de "
            + "\n\t\tcoordonnées (x, y) se trouve strictement dans la région (6)"
            + "\n\t\tet faux sinon.\n\t")
    public static boolean estDans6(double x, double y) {
        return x > X1 && x < X2 && y < Y1;
    }

    /**
     * Méthode qui retourne vrai si le point de coordonnées (x, y) se trouve 
     * strictement dans la région 7 .
     * 
     * Le "défi" est de le faire en une seule instruction : le "return"
     * 
     * @param x l'abscisse du point dont on veut savoir s'il est dans (7)
     * @param y l'ordonnée du point dont on veut savoir s'il est dans (7)
     * @return vrai si le point (x,y) est dans (7), faux sinon.
     */
    @ToDoIn("07"
            + "\n\t\tÉcrire estDans7() qui retourne vrai si le point de "
            + "\n\t\tcoordonnées (x, y) se trouve strictement dans la région (7)"
            + "\n\t\tet faux sinon.\n\t")
    public static boolean estDans7(double x, double y) {
        return x < X1 && y < Y1;
    }

    /**
     * Méthode qui retourne vrai si le point de coordonnées (x, y) se trouve 
     * strictement dans la région 8 .
     * 
     * Le "défi" est de le faire en une seule instruction : le "return"
     * 
     * @param x l'abscisse du point dont on veut savoir s'il est dans (8)
     * @param y l'ordonnée du point dont on veut savoir s'il est dans (8)
     * @return vrai si le point (x,y) est dans (8), faux sinon.
     */
    @ToDoIn("08"
            + "\n\t\tÉcrire estDans8() qui retourne vrai si le point de "
            + "\n\t\tcoordonnées (x, y) se trouve strictement dans la région (8)"
            + "\n\t\tet faux sinon.\n\t")
    public static boolean estDans8(double x, double y) {
        return x < X1 && y > Y1 && y < Y2;  
    }

    /**
     * Méthode qui retourne vrai si le point de coordonnées (x, y) se trouve 
     * strictement dans la région 9 .
     * 
     * Le "défi" est de le faire en une seule instruction : le "return"
     * 
     * @param x l'abscisse du point dont on veut savoir s'il est dans (9)
     * @param y l'ordonnée du point dont on veut savoir s'il est dans (9)
     * @return vrai si le point (x,y) est dans (9), faux sinon.
     */
    @ToDoIn("09"
            + "\n\t\tÉcrire estDans9() qui retourne vrai si le point de "
            + "\n\t\tcoordonnées (x, y) se trouve strictement dans la région (9)"
            + "\n\t\tet faux sinon.\n\t")
    public static boolean estDans9(double x, double y) {
        return x > X1 && x < X2 && y > Y1 && y < Y2;
    }


    /**
     * Méthode qui retourne vrai si le point de coordonnées (x, y) ne se trouve 
     * strictement que dans l'une des régions de 1 à 8 ou sur l'une des 
     * horizontales (Y1 ou Y2) ou sur l'une des verticales (X1 ou X2), faux sinon.
     * 
     * Le "défi" est de le faire en une seule instruction : le "return"
     * 
     * @param x l'abscisse du point dont on veut savoir s'il est dans la région spécifiée
     * @param y l'ordonnée du point dont on veut savoir s'il est dans la région spécifiée
     * @return vrai si le point (x,y) est dans la région spécifiée, faux sinon.
     */
    @ToDoIn("10"
            + "\n\t\tÉcrire nEstQueDans1ou2ou3ou4ou5ou6ou7ou8() qui retourne vrai "
            + "\n\t\tsi le point de coordonnées (x, y) se trouve strictement"
            + "\n\t\t dans l'une des régions de (1) à (8)"
            + "\n\t\t ou sur X1"
            + "\n\t\t ou sur X2"
            + "\n\t\t ou sur Y1"
            + "\n\t\t ou sur Y2"
            + "\n\t\tet faux sinon.\n\t")
    public static boolean nEstQueDans1ou2ou3ou4ou5ou6ou7ou8ouSurX1ouSurX2ouSurY1ouSurY2(double x, double y) {
        return x < X1 || x > X2 || y < Y1 || y > Y2;
    }

    /**
     * Méthode qui retourne vrai si le point de coordonnées (x, y) se trouve 
     * strictement dans la région 3 ou la région 7.
     * 
     * Le "défi" est de le faire en une seule instruction : le "return"
     * 
     * @param x l'abscisse du point dont on veut savoir s'il est dans (3) ou (7)
     * @param y l'ordonnée du point dont on veut savoir s'il est dans (3) ou (7)
     * @return vrai si le point (x,y) est dans (3) ou (7), faux sinon.
     */
    @ToDoIn("11"
            + "\n\t\tÉcrire estDans3ou7() qui retourne vrai "
            + "\n\t\tsi le point de coordonnées (x, y) se trouve strictement"
            + "\n\t\t dans la région (3) ou la région (7)"
            + "\n\t\tet faux sinon.\n\t")
    public static boolean estDans3ou7(double x, double y) {
        return x > X2 && y > Y2 || x < X1 && y < Y1;
    }

    /**
     * Méthode qui retourne vrai si le point de coordonnées (x, y) n'est 
     *  - ni dans la région 1, 
     *  - ni dans la région 8, 
     *  - ni dans la région 7, 
     *  - ni sur la demi-droite qui est la partie de Y1 avant X1 (exclu), 
     *  - ni sur la demi-droite qui est la partie de Y2 avant X1 (exclu)
     * 
     * Le "défi" est de le faire en une seule instruction : le "return"
     * 
     * @param x l'abscisse du point dont on veut savoir s'il est dans la région spécifiée.
     * @param y l'ordonnée du point dont on veut savoir s'il est dans la région spécifiée
     * 
     * @return vrai si le point (x,y) est dans la région spécifiée, faux sinon.
     */
    @ToDoIn("12"
            + "\n\t\tÉcrire estNiDans1NiDans8NiDans7NiSurY1avantX1NiSurY2avantX1() qui retourne vrai "
            + "\n\t\tsi le point de coordonnées (x, y) se trouve"
            + "\n\t\t ni dans la région (1)"
            + "\n\t\t ni dans la région (8)"
            + "\n\t\t ni dans la région (7)"
            + "\n\t\t ni sur la demi-droite qui est la partie de Y1 avant X1 (exclu)"
            + "\n\t\t ni sur la demi-droite qui est la partie de Y2 avant X1 (exclu)"
            + "\n\t\tet faux sinon.\n\t")
    public static boolean estNiDans1NiDans8NiDans7NiSurY1avantX1NiSurY2avantX1(double x, double y) {
        return x >= X1;
    }

}
