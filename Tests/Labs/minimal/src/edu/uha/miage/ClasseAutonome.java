/*
 * @author : Yvan Maillot (yvan.maillot@uha.fr)
 */
package edu.uha.miage;

import caseine.tags.ClassTestPriority;
import caseine.tags.RelativeEvaluation;
import caseine.tags.ToCheck;
import caseine.tags.ToDo;

/**
 *
 * @author yvan
 */
@ToDo("Écrire LA classe autonome minimale.")
@RelativeEvaluation
@ToCheck(value = "???", grade = 1, priority = 1)
@ClassTestPriority(1)
public class ClasseAutonome { 
    
    @ToCheck(value = "votre classe. Elle n'est pas autonome", grade = 1, priority = 2)
    public static void main(String[] args) {
    }
}
