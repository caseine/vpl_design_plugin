package geom;

import caseine.tags.*;

@RelativeEvaluation
public class Point {

    @GetterToCheck(priority = 3, grade = 1)
    @SetterToCheck(priority = 4, grade = 1)
    private double x, y;
    @ToDo("2. Écrire le constructeur aux coordonnées cartésiennes")
    @ToCheck(priority = 2, grade = 1)
    public Point(double x, double y) {
        this.x = x;
        this.y = y;
    }

    @ToDo("3. Écrire un getter pour x")
    public double getX() {
        return x;
    }
    @ToDo("4. Écrire un getter pour y")
    public double getY() {
        return y;
    }

    @ToDo("5. Écrire un setter pour x")
    public void setX(double x) {
        this.x = x;
    }

    @ToDo("6. Écrire un setter pour y")
    public void setY(double y) {
        this.y = y;
    }


    @ToDo(value = "7. public toString() qui retourne par exemple (2.5, 3.2)")
    @ToCheck(priority = 5, grade = 1)
    @ToCompare(priority = 10, grade = 2)
    @Override
    public String toString() {
        return "(" + x + ", " + y + ")";
    }


    // #########################################################################

    @ToDo(
            value = "8. Ici, equals() est mal écrit. Le réécrire comme vous l'avez appris en cours.",
            replacement = "public boolean equals(Point p) { return x == p.x && y == p.y; }"
    )
    @ToCheck(priority = 11, grade = 5)
    @Override
    public boolean equals(Object p) {
        if (this == p) {
            return true;
        }
        if (p == null) {
            return false;
        }
        if (getClass() != p.getClass()) {
            return false;
        }
        final Point other = (Point) p;
        if (Double.doubleToLongBits(this.x) != Double.doubleToLongBits(other.x)) {
            return false;
        }
        if (Double.doubleToLongBits(this.y) != Double.doubleToLongBits(other.y)) {
            return false;
        }
        return true;
    }

    @ToDo
    @ToCheck(value = "La redéfinition de equals() nécessite encore quelque chose", priority = 12, grade = 2)
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 59 * hash + (int) (Double.doubleToLongBits(this.x) ^ (Double.doubleToLongBits(this.x) >>> 32));
        hash = 59 * hash + (int) (Double.doubleToLongBits(this.y) ^ (Double.doubleToLongBits(this.y) >>> 32));
        return hash;
    }

}

