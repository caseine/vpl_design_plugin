/*
 * @author : Yvan Maillot (yvan.maillot@uha.fr)
 */
package edu.uha.miage;

import caseine.tags.*;

/**
 * Pour tester les tags @CodeQualityToCheck, @OnlyOneStatementToCheck et @StatementCountToCheck
 * 
 * 
 * @author yvan
 */
@RelativeEvaluation
public class Bidon {

    private int a, b, c;
    @StatementCountToCheck(value = "Seulement 3 instructions", minStmt = 3, maxStmt = 3, priority = 1)
    @ToDo("Écrire le constructeur à 3 paramètres entiers en seulement 3 instructions")
    @CodeQualityToCheck(priority = 2, codeAnalyserMethodsName = "edu.uha.miage.BidonTest.codeQualityCheckForConstructor")
    public Bidon(int a, int b, int c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }

    @StatementCountToCheck(value = "Seulement 1 instruction", minStmt = 1, maxStmt = 1, priority = 2)
    @OnlyOneStatementToCheck
    @ToDo("Écrire le constructeur à 2 paramètres entiers, le premier est affecté à a, le second à b et c vaut 1 en seulement 1 instruction")
    @CodeQualityToCheck(priority = 3, codeAnalyserMethodsName = "edu.uha.miage.BidonTest.codeQualityCheckExplicitConstructorCall")
    public Bidon(int a, int b) {
        this(a, b, 0);
    }

    @StatementCountToCheck(value = "Seulement 1 instruction", minStmt = 1, maxStmt = 1, priority = 3)
    @OnlyOneStatementToCheck
    @CodeQualityToCheck(priority = 4, codeAnalyserMethodsName = "edu.uha.miage.BidonTest.codeQualityCheckExplicitConstructorCall")
    @ToDo("Écrire le constructeur à 1 paramètre entier, il est affecté à a, b et c valent 2, en seulement 1 instruction")
    public Bidon(int a) {
        this(a, 0, 0);
    }

    @StatementCountToCheck(value = "Pas d'instruction", minStmt = 0, maxStmt = 0, priority = 4)
    @ToDo("Écrire le constructeur par défaut sans instruction (les attributs sont initialisés par défaut)")
    public Bidon() {

    }

    @StatementCountToCheck(value = "Seulement 1 instruction", minStmt = 1, maxStmt = 1, priority = 5)
    @OnlyOneStatementToCheck
    @CodeQualityToCheck(priority = 6, codeAnalyserMethodsName = "edu.uha.miage.BidonTest.codeQualityCheckForGetter")
    @ToDo("Écrire tous les getters")
    public int getA() {
        return a;
    }
    @StatementCountToCheck(value = "Seulement 1 instruction", minStmt = 1, maxStmt = 1, priority = 5)
    @OnlyOneStatementToCheck
    @CodeQualityToCheck(priority = 6, codeAnalyserMethodsName = "edu.uha.miage.BidonTest.codeQualityCheckForGetter")
    @ToDo
    public int getB() {
        return b;
    }
    @StatementCountToCheck(value = "Seulement 1 instruction", minStmt = 1, maxStmt = 1, priority = 5)
    @OnlyOneStatementToCheck
    @CodeQualityToCheck(priority = 6, codeAnalyserMethodsName = "edu.uha.miage.BidonTest.codeQualityCheckForGetter")
    @ToDo
    public int getC() {
        return c;
    }


    @StatementCountToCheck(value = "Seulement 1 instruction", minStmt = 1, maxStmt = 1, priority = 9)
    @OnlyOneStatementToCheck
    @CodeQualityToCheck(priority = 7, codeAnalyserMethodsName = "edu.uha.miage.BidonTest.codeQualityCheckForGetSomme")
    @ToDo("Écrire getSomme(), une fonction à un paramètre entier qui retourne la somme des attributs et du paramètre")
    public int getSomme(int d) {
        return a + b + c + d;
    }
    @StatementCountToCheck(value = "Seulement 1 instruction", minStmt = 1, maxStmt = 1, priority = 10)
    @CodeQualityToCheck(priority = 7, codeAnalyserMethodsName = "edu.uha.miage.BidonTest.codeQualityCheckForGetSomme")
    @ToDo("Écrire getSomme(), une fonction à deux paramètres entiers qui retourne la somme des attributs et des paramètres")
    public int getSomme(int d, int e) {
        return a + b + c + d + e;
    }
    @StatementCountToCheck(value = "Seulement 1 instruction", minStmt = 1, maxStmt = 1, priority = 11)
    @CodeQualityToCheck(priority = 7, codeAnalyserMethodsName = "edu.uha.miage.BidonTest.codeQualityCheckForGetSomme")
    @ToDo("Écrire getSomme(), une fonction à trois paramètres entiers qui retourne la somme des attributs et des paramètres")
    public int getSomme(int d, int e, int f) {
        return a + b + c + d + e + f;
    }
    @StatementCountToCheck(value = "Pas d'instruction", minStmt = 0, maxStmt = 0, priority = 12)
    @ToDo("Écrire withNoStatement(), une fonction sans paramètre et sans instruction")
    public void withNoStatement() {

    }
    @StatementCountToCheck(value = "Entre 1 et 2 instructions", minStmt = 1, maxStmt = 2, priority = 13)
    @CodeQualityToCheck(priority = 8, codeAnalyserMethodsName = "edu.uha.miage.BidonTest.codeQualityCheckForWithAForStatement")
    @ToDo("Écrire withAForStatement(), une fonction sans paramètre et avec une boucle for, avec 1 ou 2 instructions")
    public void withAForStatement() {
        for (int i = 0; i < 10; ++i) {
            System.out.println(i);
        }
    }
    @StatementCountToCheck(value = "Entre 1 et 3 instructions", minStmt = 1, maxStmt = 3, priority = 14)
    @CodeQualityToCheck(priority = 9, codeAnalyserMethodsName = "edu.uha.miage.BidonTest.codeQualityCheckForWithAWhileStatement")
    @ToDo("Écrire withAWhileStatement(), une fonction avec un paramètre entier et avec une boucle while, avec 1 à 3 instructions")
    public void withAWhileStatement(int n) {
        while (n > 0) {
            System.out.println(n);
            --n;
        }
    }

    @StatementCountToCheck(value = "Entre 1 et 4 instructions", minStmt = 1, maxStmt = 4, priority = 16)
    @CodeQualityToCheck(priority = 9, codeAnalyserMethodsName = "edu.uha.miage.BidonTest.codeQualityCheckForWithIfStatements")
    @ToDo("Écrire withIfStatements(), une fonction avec trois paramètres entiers et avec un à trois if, avec 1 à 4 instructions, " +
            "qui retourne -1 si le premier paramètre est inférieur au deuxième, " +
            "1 si le premier paramètre est supérieur au troisième, " +
            "et 0 sinon," +
            "avec seulement des if")
    @ToCompare(priority = 8)
    public int withIfStatements(int x, int mini, int maxi) {
        if (x < mini) {
            return -1;
        } else if (x > maxi) {
            return 1;
        } else {
            return 0;
        }
    }
}
