/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.uha.miage;

/**
 *
 * @author yvan
 */
public class Main {
    public static void main(String[] args) {
        // Ecrire vos propres tests dans edu.uha.miage.Main
        // Par exemple
        int[] triple = {1, 2, 3};
        int taille = ManipulationDeTableaux.taille(triple);
        if (taille == 3) {
            System.out.println("OK : le tableau triple contient bien 3 éléments");
        } else {
            System.err.println("FAUX : le tableau triple ne contient pas " + taille + " éléments");
        }
    }
}
