/*
 * @author : Yvan Maillot (yvan.maillot@uha.fr)
 */
package edu.uha.miage;

import caseine.tags.RelativeEvaluation;
import caseine.tags.ToDoIn;
import java.util.Arrays;

/**
 * Le but de l'exercice est d'écrire un ensemble de méthodes pour rechercher des
 * informations dans des tableaux ou les transformer.
 *
 * Il existe un certain nombres de méthodes déjà écrite dans java.util.Arrays.
 *
 * Il est vivement conseillé de LIRE SA DOCUMENTATION en visitant le lien
 * suivant :
 *
 * https://docs.oracle.com/javase/8/docs/api/java/util/Arrays.html
 *
 * Pour répondre à l'exercice, il suffit d'écrire, pour chaque méthode, à
 * l'endroit précisé par TODO, du code qui satisfait la spécification donnée par
 * la JavaDoc.
 *
 * Il est également recommandé d'utiliser les méthodes de Arrays pour répondre à
 * l'exercice. Chose qui sera d'autant plus aisée que la LECTURE DE SA
 * DOCUMENTATION sera sérieuse.
 *
 * La solution des deux premières méthodes sont données pour faire comprendre
 * l'esprit de l'exercice.
 *
 * Par ailleurs, pour toutes les méthodes où les paramètres peuvent connaître
 * des contraintes, comme par exemple un indice en dehors de la chaîne, la
 * politique est de NE PAS TESTER LES PARAMETRES. L'utilisateur est supposé
 * employer correctement la méthode. Tant pis si une mauvaise utilisation
 * provoque un plantage.
 *
 *
 * @author yvan
 */
@RelativeEvaluation
public class ManipulationDeTableaux {

    /**
     * Méthode qui retourne la remplirDeA du tableau d'entier passé en paramètre
     *
     * @param t le tableau d'entiers valide dont on veut la remplirDeA
     * @return la remplirDeA de remplirDeA


 Le type tableau comprend l'attribut length. Il faut s'en servir.

 Remarque : une tentative d'utilisation de cette méthode sur un tableau
 non créé engendre une erreur qui fait planter le programme. C'est normal.
     *
     */
    public static int taille(int[] t) {
        return t.length;
    }

    /**
     * Méthode qui remplit de 0 le tableau d'entiers passé en paramètre
     *
     * @param t le tableau d'entiers valide à remplir de 0
     *
     */
    public static void remplirDe0(int[] t) {
        Arrays.fill(t, 0);
        /*
        Arrays offre une méthode commode pour répondre à la question. Il est 
        possible de l'utiliser.
        
        Mais il était possible d'écrire ceci :
        for(int i = 0; i < t.length; ++i) {
            t[i] = 'a';
        }
         
        
        */
    }

    /**
     * Méthode pour savoir si deux tableaux d'entiers sont les mêmes.
     *
     * @param t1 un premier tableau d'entiers
     * @param t2 un second tableau d'entiers
     * @return vrai si t1 est le même que t2 et faux sinon. Deux tableaux sont
     * égaux s'ils possèdent les mêmes valeurs aux mêmes places
     */
    @ToDoIn
    public static boolean egaux(int[] t1, int[] t2) {
        return Arrays.equals(t1, t2);
    }


    /**
     * Méthode qui retourne un élément de valeur minimale d'un tableau d'entiers.
     *
     * @param t le tableau d'entiers d'au moins un élément dont on veut un élément 
     * minimal.
     * @return un élément de valeur minimale dans t
     */
    @ToDoIn
    public static int mini(int[] t) {
        int mini = t[0];
        for(int i = 0; i < t.length; ++i) {
            if (t[i] < mini) {
                mini = t[i];
            }
        }
        return mini;
    }
    /**
     * Méthode qui retourne l'indice d'un élément de valeur minimale d'un tableau 
     * d'entiers.
     *
     * @param t le tableau d'entiers d'au moins un élément dont on veut l'indice
     * d'un élément minimal.
     * @return l'indice d'un élément de valeur minimale dans t
     */
    @ToDoIn
    public static int imini(int[] t) {
        int imini = 0;
        for(int i = 0; i < t.length; ++i) {
            if (t[i] < t[imini]) {
                imini = i;
            }
        }
        return imini;
    }
    
    
    /**
     * Méthode qui retourne la moyenne des éléments d'un tableau d'entiers.
     *
     * @param t un tableau d'entiers d'au moins un élément dont on veut la 
     * moyenne des éléments
     * @return la moyenne des éléments de t.
     */
    @ToDoIn
    public static double moyenne(int[] t) {
        double r = 0.0;
        for(int v : t) {
            r += v;
        }
        return r / t.length;
    }

    /**
     * Méthode qui retourne une copie d'un tableau d'entiers.
     *
     * @param t un tableau d'entiers à copier
     * @return une copie de t
     */
    @ToDoIn
    public static int[] copie(int[] t) {
        return Arrays.copyOf(t, t.length);
    }

    /**
     * Méthode qui trie dans l'ordre croissant le tableau d'entiers passé en
     * paramètre.
     *
     * @param t un tableau d'entiers à trier
     */
    @ToDoIn
    public static void trier(int[] t) {
        Arrays.sort(t);
    }

    /**
     * Méthode qui retourne l'indice d'un élément dans un tableau trié croissant.
     * La recherche doit être dichotomique et sa complexité doit être 
     * en O(n log(n)) le plus souvent.
     *
     * @param t un tableau d'entiers dans lequel la position de v est cherchée
     * @param v une valeur dont on veut la position dans v
     * @return la position de v dans t, si v est dans t, 
     *         ou un entier &lt; 0 si v n'est pas dans t.
     */
    @ToDoIn
    public static int dichotomie(int[] t, int v) {
        return Arrays.binarySearch(t, v);
    }

}
