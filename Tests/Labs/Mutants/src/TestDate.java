

import org.junit.Test;

import caseine.tags.ToDo;

public class TestDate {

	@Test
	public void Test1() {
		// pour executer la fonction vadiDate avec la date du 2/11/2020
		assert(Date.valiDate(2, 11, 2020)); 
	}
	
	@Test
	@ToDo
	public void Test_bissexiles() {
		//  1900 ne devrait pas etre bis
		assert(Date.valiDate(29, 02, 1900)==false); 
		assert(Date.valiDate(29, 02, 2000)); 
		assert(Date.valiDate(29, 02, 1984)); 
		assert(Date.valiDate(29, 02, -4)); 
		assert(Date.valiDate(29, 02, 100)==false); 
	}

	
	@Test
	@ToDo
	public void Test_moisLimites() {
		assert(Date.valiDate(15, 01, 2001)); 
		assert(Date.valiDate(28, 12, 1789)); 
	}
	
			
	@Test
	@ToDo
	public void Test_Jourlimites() {
		//  il faut tester un premier
		assert(Date.valiDate(1, 3, 400)); 
		assert(Date.valiDate(-1, 5, 1980) == false); 
	}

			
	@Test
	@ToDo
	public void Test_moisInvalides() {
		//  date invalide à cause du mois
		assert(Date.valiDate(1, 0, 1980) == false); 
		assert(Date.valiDate(1, 13, 1980) == false); 
	}

			
	@Test
	@ToDo
	public void Test_PasPlusDeJoursDansLeMois() {
		assert(Date.valiDate(32, 3, 1882) == false);  //mutant 19
		assert(Date.valiDate(31, 4, 1783) == false);  //mutant 20
   	    assert(Date.valiDate(32, 5, 1684) == false);  //mutant 21
 		assert(Date.valiDate(31, 6, 1586) == false);  //mutant 22
		assert(Date.valiDate(32, 7, 1487) == false);  //mutant 23
		assert(Date.valiDate(32, 8, 1388) == false);  //mutant 24
		assert(Date.valiDate(31, 9, 1290) == false);  //mutant 25
		assert(Date.valiDate(32, 10, 1191) == false); //mutant 26
		assert(Date.valiDate(31, 11, 994) == false);  //mutant 27
		assert(Date.valiDate(32, 12, 789) == false);  //mutant 28
		assert(Date.valiDate(30, 2, 1981) == false);  // test inutile
		assert(Date.valiDate(32, 1, 81) == false);    //mutant30
	}

	@Test
	@ToDo
	public void Test_derniersJoursDesMois() {
		assert(Date.valiDate(31, 3, 1982));  //mutant 9
		assert(Date.valiDate(30, 4, 1983));  //mutant 10
		assert(Date.valiDate(31, 5, 1984));  //mutant 11
		assert(Date.valiDate(30, 6, 1986));  //mutant 12
		assert(Date.valiDate(31, 7, 1987));  //mutant 13
		assert(Date.valiDate(31, 8, 1988));  //mutant 14
		assert(Date.valiDate(30, 9, 1990));  //mutant 15
		assert(Date.valiDate(31, 10, 1991)); //mutant 16
		assert(Date.valiDate(30, 11, 1994)); //mutant 17
		assert(Date.valiDate(31, 12, 1789)); //mutant 18 
	}
	





}
