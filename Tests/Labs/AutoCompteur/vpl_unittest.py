# Please do not change or remove
# It is used in order to allow the use of @grade and @timeout

### TODO : GPL 3.0 or later
### Author : F. Thiard

### Last modification 02/03/2020 : fixed test display for compatibility with Caseine Eclipse Plugin

"""
This module defines a small overlay to use the unittest (formerly pyUnit) framework for VPL evaluation.
For the documentation of unittest, a xUnit type test framework, see https://docs.python.org/3/library/unittest.html.

Main features are :
- output of tests in VPL comment format
- automatic and manual grading mode
- user defined timeouts (warning : no timeout by default)
- Ordering of tests according to order in the source code (vs. default unittest behavior : alphabetically). Possibilty to revert to default behavior
- protected tracebacks : in case of test failure, only the exception message is shown ; in case of test error, a protected traceback including only frames in authorized files (supposedly, student submitted files)

***
TODO :
- check for encoding issues (should be ok, but...)
NOT HANDLED :
- subtests (unittest >3.4 feature) : behavior may be inconsistent with grading... Not sure what the consistent behavior should be.
POSSIBLY? :
- for now, student output is redirected to an IO stream (closed after each test case). We might want to allow the user to provide a stream, or to redirect to /dev/null or a dummy object.

ISSUES :
- Defining a hierarchy of unittest.TestCase subclasses **where several levels are instantiated and loaded, and define test methods** may lead to ugly results :
  + because of test ordering according to line number (which we could deal with. Meanwhile, reverting to alphabetical order is an option)
  + because of unittest load_tests_from_module behavior (which we can't), see <https://docs.python.org/3/library/unittest.html#loading-and-running-tests>

  In short : don't. Superclasses (non-leaf) should be used to factor helper code, setup and teardown methods, not test methods.
  If you really need to, use alphabetical ordering, and make sure the modules listed in the casefile contain only leaf classes and classes which don't define any test_methods, or use the load_test protocol to control which TestCases are loaded.
***

# Usage :

## Test modules :

/!\ Test cases should not use attributes starting with _vpl_. (ugly, yeah, blame Python.)
test modules should import decorators grade and timeout to use these features (manual grading and timeouts)

## Running as a script :

This module provide a main evaluation program which can be run directly as a script.
Usage : python -m vpl_unittest [-a] [-g grade] [-s varname] casefile
casefile : the name (path) of the casefile, containing the names of the test modules (one per line), usually vpl_evaluate.cases
options :
-a, --alphabetical : reverts to unittest default alphabetical ordering of tests within each module and each test case (instead of ordering tests according to order of definition)
-g, --maxgrade grade : the maximum grade for the activity. Defaults to the value of the environment variable VPL_GRADEMAX, or 100 if it is not defined
-s, --subfilesvar varname : The name of an environment variable containing the list of the names of authorized filesfor tracebacks. Defaults to VPL_SUBFILES.

## From a client module :

This module provides :

+ Utilities like grade and timeout decorators, which can be used to specify timeouts and custom grades associated with tests method. These should be imported by test modules if needed
+ Custom Result, Runner and Loader classes :
  - VPLTestResult, a subclass of unittest.TestResult, in charge of formatting the test results, and maintaining the current grade if manual grades are defined. This is where most of the work is do ne.
  - VPLTestLoader, a subclass of unittest.TestLoader, which loads tests from the modules specified in a given casefile, and by default orders them by definition line number within each module
  - VPLTestRunner, a custom runner subclassing unittest.TextTestRunner, which uses an instance of VPLTestResult for result formatting, and computes/outputs the final grade in VPL format after running the tests.
+ A complete evaluation function
vpl_unittest.evaluate runs a complete evaluation given the adequate arguments (casefile, max grade, list of authorized (student) files, optional argument to alphabetize tests)
"""
import functools
import errno
import os
import signal

class TestTimeout(Exception):
    """ Timeout exception
    """
    pass

def timeout(seconds=30,error_message=os.strerror(errno.ETIME)):
    """Timeout function decorator
    Inspired from package timeout_decorator at https://pypi.org/project/timeout-decorator/ in order to reduce dependencies. If available, is is perfectly possible to use timeout_decorator.timeout instead.
    /!\\ this is the "simple" version using signals, thus :
    + inoperative on non *nix platforms (this code is supposed to be executed by caseine/VPL execution servers, so that's OK).
    + doesn't play well with multi-threading (can't send signal from child process ?). If this is needed, use timeoute_decorator.timeout with signals=False)
    + doesn't play well with nesting either (which is not a problem here, as  unit tests have absolutely no reason to be nested.
    """
    def timeout_decorator(func):
        def _handle_timeout(signum,frame):
            raise TestTimeout(error_message)
        @functools.wraps(func)
        def wrapper(*args,**kwargs):
            signal.signal(signal.SIGALRM, _handle_timeout)
            signal.alarm(seconds)
            try :
                result=func(*args,**kwargs)
            finally:
                signal.alarm(0)
            return result
        return wrapper
    return timeout_decorator

def grade(n):
    """ Decorator for attributing an explicit max grade to a test. If at least one manual grade is set, all grades are supposed to be set manually (if no grade is set for a test, it is assumed to be 0). It is the teacher's responsibility to ensure compatibility with the VPL's max grade
    """
    def grade_decorator(func):
        @functools.wraps(func)
        def wrapper(*args,**kwargs):
            #We ensure that the max grade is set *after* executing the test to avoid any manipulation by the student code.
            try:
                result=func(*args,**kwargs)
            finally:
                #Adds an instance attribute _vpl_testgrade (args[0] is self). This will be used by the testResult to update the current grade in case of success
                args[0]._vpl_testgrade=n
            return result
        return wrapper
    return grade_decorator
