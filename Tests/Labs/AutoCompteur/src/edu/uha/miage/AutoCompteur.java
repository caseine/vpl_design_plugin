/*
 * @author : Yvan Maillot (yvan.maillot@uha.fr)
 */
package edu.uha.miage;

import caseine.tags.ClassTestPriority;
import caseine.tags.RelativeEvaluation;
import caseine.tags.ToCheck;
import caseine.tags.ToDo;

/**
 * Écrire une classe AutoCompteur dont l’instance affiche à sa construction le 
 * nombre d’instances de AutoCompteur créées.
 * 
 * Chaque instance de cette classe doit être identifiée par le numéro d'ordre
 * de sa création. L'identifiant de la première créée est 1, la deuxième est 2,
 * la nième est n.
 * 
 * Cette classe devra possèder deux méthodes :
 * 
 *   1. getNbInstances() qui donne le nombre d'instances de cette classe déjà créées.
 *   2. getId() qui donne l'identifiant de l'instance sur laquelle elle s'applique.
 * 
 * @author yvan
 */
@RelativeEvaluation
@ClassTestPriority(2)
public class AutoCompteur {
    // Pour compter le nombre d'instances créées, un attribut de classe est nécessaire
    @ToDo("1. Déclarer le ou les attributs nécessaires.")
    private static int nbInstances;
    @ToDo
    private int id;

    @ToDo("2."
            + "\n\t\tÉcrire un constructeur par défaut de sorte qu'il affiche le nombre "
            + "\n\t\td'instances de AutoCompteur créées à chaque nouvelle instance "
            + "\n\t\tde AutoCompteur créée\n\t")
    public AutoCompteur() {
        System.out.println(id = ++nbInstances);
    }

    @ToDo("3."
            + "\n\t\tÉcrire une méthode getNbInstances() avec "
            + "\n\t\tune signature et des modificateurs adaptés"
            + "\n\t\tafin qu'elle retourne le nombre d'instances "
            + "de AutoCompteur déjà créée\n\t")
    @ToCheck(
            priority = 3,
            grade = 1
    )
    public static int getNbInstances() {
        return nbInstances;
    }

    @ToDo("4."
            + "\n\t\tÉcrire une méthode getId() avec "
            + "\n\t\tune signature et des modificateurs adaptés"
            + "\n\t\tafin qu'elle retourne l'identifiant de cette instance\n\t")
    @ToCheck(
            priority = 3,
            grade = 1
    )
    public int getId() {
        return id;
    }

}
