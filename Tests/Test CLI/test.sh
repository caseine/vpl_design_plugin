 #!/bin/bash 
TEST_DIRECTORY=test_output         
rm -rf $TEST_DIRECTORY

echo "//////////// Version of caseine:"
caseine -V

VPL_ID=66049

mkdir $TEST_DIRECTORY 
cd $TEST_DIRECTORY

echo "//////////// PYTHON TESTS ////////////"

echo "//////////// Generates a python project"
caseine --gen -l python -p PythonTest0
caseine --push -v $VPL_ID -p PythonTest0

caseine --gen -p=PythonTest1 -l python --vplId $VPL_ID
caseine --push -p=PythonTest1

echo "//////////// Scenario Yvan"
caseine --gen --lang python -p PythonTest3
cd PythonTest3 
caseine --push -v $VPL_ID
cd -

echo "//////////// JAVA TESTS ////////////"

echo "//////////// Scenario Yvan"
caseine --gen --lang java -p JavaTest0
cd JavaTest0 
caseine --push -v $VPL_ID
cd -

echo "//////////// Le coin JAVA 2"
caseine --lang=java -java-template-2 --gen -p exo1
cd exo1
caseine -t$CASEINE_TOKEN -v$VPL_ID --push
cd -

caseine -p exo2 -java-template-1 --gen --lang=java 
cd exo2
caseine -v$VPL_ID --push
cd -

# TODO
#unzip pour-illustrer-relativeevaluation-et-grade.zip 
#cd pour-illustrer-relativeevaluation-et-grade
#caseine --gen --lang=java
#caseine --push -v$VPL_ID

#echo "//////////// Scenario Yvan2"
#rm -rf DesSegmentsPlutotClasses/target
#caseine --push -p DesSegmentsPlutotClasses

echo "//////////// Generates a simple project template, vplId set at the end"
caseine --gen -p JavaTest1 -java-template-1
caseine --local --path JavaTest1
caseine --push --path JavaTest1  --vplId $VPL_ID

echo "//////////// Generates a full project template"
caseine --gen --path JavaTest2 --vplId $VPL_ID --lang java --ide eclipse -java-template-2
caseine --push --path JavaTest2

echo "//////////// Generates without path"
mkdir JavaTest3
cd JavaTest3
caseine --gen --ide eclipse --vplId $VPL_ID -java-template-1
caseine --push
cd -

echo "//////////// Generates a simple project template, vplId not set"
caseine --gen -p JavaTest_err1 -java-template-1
caseine --push --path JavaTest_err1 

echo "//////////// MAVEN TESTS ////////////"

echo "//////////// Generates a project template with a vplId"
caseine --gen -p JavaTest4 --mvn --vplId $VPL_ID  -java-template-3
cd JavaTest4
mvn deploy
cd -

echo "//////////// Generates a maven project without template"
caseine --gen -p JavaTest5 --mvn
cd JavaTest5
mvn deploy
cd -

echo "//////////// No push with a mvn project, it is better to launch mvn"
caseine --gen -p JavaTest6 --mvn --vplId $VPL_ID
caseine --push -p JavaTest6

echo "//////////// Push a project without settings"
caseine --gen -p JavaTest7 -java-template-1
caseine --push --path JavaTest1  --vplId $VPL_ID --no-settings

echo "//////////// CPP TESTS ////////////"

caseine --gen -p CPPTest0 -l cpp

caseine --gen -p CPPTest1 -l cpp -cpp-template-1 --vplId $VPL_ID

caseine --local --path CPPTest1

caseine --push --path CPPTest1 --no-settings

echo "//////////// DOC TESTS ////////////"
caseine --gen -p DocTest0 -l cpp -cpp-template-1 --vplId $VPL_ID
caseine --doc -p DocTest0

echo "//////////// NO LOCAL TESTS ////////////"
caseine --gen -p NoLocalTest0 -l cpp -cpp-template-1 --vplId $VPL_ID
caseine --push -p NoLocalTest0 --no-local

caseine --gen -p NoLocalTest1 -java-template-1
caseine --push --path NoLocalTest1  --vplId $VPL_ID --no-local

caseine --gen -p NoLocalTest2 -java-template-3
caseine --push --path NoLocalTest2  --vplId $VPL_ID --no-local

echo "//////////// MONKEY TESTS ////////////"

echo "> caseine"
caseine

echo "> caseine toto"
caseine toto

echo "> caseine --push --no-local --lang=java"
caseine --push --no-local --lang=java

echo "> caseine --no-local --lang=java"
caseine --no-local --lang=java

echo "> caseine --gen --lang=toto"
caseine --gen --lang=toto

#echo "> caseine --no-local --lang=java -cpp-template-1"
#caseine --no-local --lang=java -cpp-template-1




